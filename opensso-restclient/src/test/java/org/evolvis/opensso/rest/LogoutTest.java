package org.evolvis.opensso.rest;

import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.evolvis.opensso.rest.service.IsTokenValid;
import org.evolvis.opensso.rest.service.Logout;
import org.junit.Assert;
import org.junit.Test;

public class LogoutTest extends AbstractRestTest {

    @Test
    public void testRealmLogout() throws Exception {
        final String token = client.callService(new AuthenticateUser(TEST_REALM, TEST_UID, TEST_PWD)).getToken();
        Assert.assertNotNull("token must not null", token);

        client.callService(new Logout(token));

        final Boolean isValid = client.callService(new IsTokenValid(token)).valid();
        Assert.assertFalse("token must not valid", isValid);
    }

    @Test
    public void testSubrealmLogout() throws Exception {
        final String token = client.callService(new AuthenticateUser(TEST_SUBREALM, TEST_UID, TEST_PWD)).getToken();
        Assert.assertNotNull("token must not null", token);

        client.callService(new Logout(token));

        final Boolean isValid = client.callService(new IsTokenValid(token)).valid();
        Assert.assertFalse("token must not valid", isValid);
    }
}
