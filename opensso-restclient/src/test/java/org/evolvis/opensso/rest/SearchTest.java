package org.evolvis.opensso.rest;

import org.evolvis.opensso.rest.constant.UserAttribute;
import org.evolvis.opensso.rest.service.Search;
import org.junit.Assert;
import org.junit.Test;

public class SearchTest extends AbstractRestTest {

    @Test
    public void testNullRealmFailing() throws Exception {

        // search userUUID
        Exception exception = null;
        try {
            client.callService(new Search(null));
        }
        catch (final IllegalArgumentException iae) {
            exception = iae;
        }

        Assert.assertNotNull("IllegalArgumentException must be thrown", exception);
    }

    @Test
    public void testSearchByUserUuid() throws Exception {

        // search userUUID
        final Search search = client.callService(new Search(TEST_REALM).setAttr(UserAttribute.USER_UUID, TEST_USER_UUID));

        Assert.assertTrue("search must success", search.success());
        Assert.assertTrue("one uid must be found, uids: " + search.getUids(), search.getUids().size() == 1);
        Assert.assertEquals("found uid must match testuser uid", TEST_UID, search.getUid());
    }

    @Test
    public void testSearchSubrealmByUserUuid() throws Exception {

        // search userUUID
        final Search search = client.callService(new Search(TEST_SUBREALM).setAttr(UserAttribute.USER_UUID, TEST_USER_UUID));

        Assert.assertTrue("search must success", search.success());
        Assert.assertTrue("one uid must be found, uids: " + search.getUids(), search.getUids().size() == 1);
        Assert.assertEquals("found uid must match testuser uid", TEST_UID, search.getUid());
    }

    @Test
    public void testSearchByPasswordUuid() throws Exception {

        // search passwordUUID
        final Search search = client.callService(new Search(TEST_REALM).setAttr(UserAttribute.PASSWORD_UUID, TEST_PASSWORD_UUID));

        Assert.assertTrue("search must success", search.success());
        Assert.assertTrue("one uid must be found, uids: " + search.getUids(), search.getUids().size() == 1);
        Assert.assertEquals("found uid must match testuser uid", TEST_UID, search.getUid());
    }

    @Test
    public void testSearchByUidAndPasswordUuid() throws Exception {

        // search for uid & passwordUUID
        final Search search = client.callService(new Search(TEST_REALM)
            .setAttr(UserAttribute.UID, TEST_UID)
            .setAttr(UserAttribute.PASSWORD_UUID, TEST_PASSWORD_UUID)
        );

        Assert.assertTrue("search must success", search.success());
        Assert.assertTrue("one uid must be found, uids: " + search.getUids(), search.getUids().size() == 1);
        Assert.assertEquals("found uid must match testuser uid", TEST_UID, search.getUid());
    }

    @Test
    public void testSearchSubrealmByPasswordUuid() throws Exception {

        // search passwordUUID
        final Search search = client.callService(new Search(TEST_SUBREALM)
            .setAttr(UserAttribute.PASSWORD_UUID, TEST_PASSWORD_UUID)
        );

        Assert.assertTrue("search must success", search.success());
        Assert.assertTrue("one uid must be found, uids: " + search.getUids(), search.getUids().size() == 1);
        Assert.assertEquals("found uid must match testuser uid", TEST_UID, search.getUid());
    }

    @Test
    public void testSearchSubrealmByUidAndPasswordUuid() throws Exception {

        // search for uid & passwordUUID
        final Search search = client.callService(new Search(TEST_SUBREALM)
            .setAttr(UserAttribute.UID, TEST_UID)
            .setAttr(UserAttribute.PASSWORD_UUID, TEST_PASSWORD_UUID)
        );

        Assert.assertTrue("search must success", search.success());
        Assert.assertTrue("one uid must be found, uids: " + search.getUids(), search.getUids().size() == 1);
        Assert.assertEquals("found uid must match testuser uid", TEST_UID, search.getUid());
    }

    @Test
    public void testSearchByRegisterUuid() throws Exception {

        // search registerUUID
        final Search search = client.callService(new Search(TEST_REALM).setAttr(UserAttribute.REGISTER_UUID, TEST_REGISTER_UUID));

        Assert.assertTrue("search must success", search.success());
        Assert.assertTrue("one uid must be found, uids: " + search.getUids(), search.getUids().size() == 1);
        Assert.assertEquals("found uid must match testuser uid", TEST_UID, search.getUid());
    }

    @Test
    public void testSearchSubrealmByRegisterUuid() throws Exception {

        // search registerUUID
        final Search search = client.callService(new Search(TEST_SUBREALM).setAttr(UserAttribute.REGISTER_UUID,
                TEST_REGISTER_UUID));

        Assert.assertTrue("search must success", search.success());
        Assert.assertTrue("one uid must be found, uids: " + search.getUids(), search.getUids().size() == 1);
        Assert.assertEquals("found uid must match testuser uid", TEST_UID, search.getUid());
    }
    
    /**
     * Test for the bug, that for an Search without a result a List with one 'null' Element is returned.  
     */
    @Test
    public void testSearchBugfixListWithoutNullElement() throws Exception {
        
        // search for an empty result
        final Search search = client.callService(new Search(TEST_REALM)
            .setAttr(UserAttribute.LASTNAME, "1qayxsw23edcvfr45tgbnhz67ujmki89olpö0ßüä")
        );

        Assert.assertTrue("search must success", search.success());
        Assert.assertTrue("a uid must not be found and the list must be empty, uids: " + search.getUids(), search.getUids().isEmpty());
    }
}
