package org.evolvis.opensso.rest.jmeter;

import org.evolvis.opensso.rest.AbstractRestTest;
import org.evolvis.opensso.rest.RestClient;

/**
 * ABC for Tests intended to run in JMeter. Most subclasses of this class will
 * need to run in a certain order to be successful.
 * 
 * @author Fabian Bieker
 * 
 */
public abstract class AbstractJMeterRestTest extends AbstractRestTest {

	/**
	 * we need to keep the RestClient to run a bunch of tests in jmeter ...
	 */
	private static final ThreadLocal<RestClient> clientStore = new ThreadLocal<RestClient>();

	private static final ThreadLocal<String> sessionTokenStore = new ThreadLocal<String>();

	private static final ThreadLocal<String> userIdStore = new ThreadLocal<String>();

	public AbstractJMeterRestTest() {
		super();
	}

	@Override
	protected RestClient getRestClient() {
		if (clientStore.get() == null) {
			clientStore.set(super.getRestClient());
		}
		return clientStore.get();
	}

	protected void setUserId(String id) {
		userIdStore.set(id);
	}

	protected String getUserId() {
		return userIdStore.get();
	}

	protected void setSessionToken(String id) {
		sessionTokenStore.set(id);
	}

	protected String getSessionToken() {
		return sessionTokenStore.get();
	}
}