package org.evolvis.opensso.rest;

import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.evolvis.opensso.rest.service.IsTokenValid;
import org.junit.Assert;
import org.junit.Test;

public class IsTokenValidTest extends AbstractRestTest {

    @Test
    public void testRealmValidation() throws Exception {
        final String token = client.callService(new AuthenticateUser(TEST_REALM, TEST_UID, TEST_PWD)).getToken();
        Assert.assertNotNull("token is null", token);

        final Boolean isValid = client.callService(new IsTokenValid(token)).valid();
        Assert.assertTrue("token not valid", isValid);
    }

    @Test
    public void testSubrealmValidation() throws Exception {
        final String token = client.callService(new AuthenticateUser(TEST_SUBREALM, TEST_UID, TEST_PWD)).getToken();
        Assert.assertNotNull("token is null", token);

        final Boolean isValid = client.callService(new IsTokenValid(token)).valid();
        Assert.assertTrue("token not valid", isValid);
    }
}
