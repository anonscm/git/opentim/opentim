package org.evolvis.opensso.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.config.Configuration;
import org.evolvis.config.PropertyConfig;
import org.evolvis.opensso.rest.constant.RestConfig;
import org.junit.Before;

public abstract class AbstractRestTest {

    private static final Configuration config = PropertyConfig.getInstance();

    public static final String TEST_REALM = config.get("test.realm");

    public static final String TEST_SUBREALM = config.get("test.subrealm");

    public static final String TEST_UID = config.get("test.uid");

    public static final String TEST_PWD = config.get("test.pwd");

    public static final String TEST_USER_UUID = config.get("test.userUUID");

    public static final String TEST_PASSWORD_UUID = config.get("test.passwordUUID");

    public static final String TEST_REGISTER_UUID = config.get("test.registerUUID");

    protected final Log log = LogFactory.getLog(getClass());

    protected RestClient client;

    @Before
    public void setUp() throws Exception {
        client = getRestClient();
    }

    protected RestClient getRestClient() {
        final RestClient client = RestClient.getInstance();
        client.initialize(RestConfig.SERVICE_URL, RestConfig.SERVICE_LOGIN, RestConfig.SERVICE_PASSWD);
        return client;
    }
}
