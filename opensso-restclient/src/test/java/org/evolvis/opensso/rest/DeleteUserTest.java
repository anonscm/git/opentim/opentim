package org.evolvis.opensso.rest;

import org.evolvis.opensso.rest.constant.RestError;
import org.evolvis.opensso.rest.constant.UserStatus;
import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.evolvis.opensso.rest.service.CreateUser;
import org.evolvis.opensso.rest.service.DeleteUser;
import org.junit.Assert;
import org.junit.Test;

public class DeleteUserTest extends AbstractRestTest {

    @Test
    public void testDeleteUser() throws Exception {
        final String testId = String.valueOf(System.currentTimeMillis());
        final CreateUser createUser = client.callService(new CreateUser(TEST_REALM, testId, "test", testId + "_first", testId
                + "_last", testId + "_email", UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));
        Assert.assertTrue("createUser failed", createUser.success());
        Assert.assertNull("createUser failed with error", createUser.getError());

        final AuthenticateUser successAuth = client.callService(new AuthenticateUser(testId, "test"));
        Assert.assertTrue("successAuth failed", successAuth.success());
        Assert.assertNull("successAuth failed with error", successAuth.getError());
        Assert.assertNotNull("successAuth failed, token is null", successAuth.getToken());

        final DeleteUser deleteUser = client.callService(new DeleteUser(TEST_REALM, testId));
        Assert.assertTrue("deleteUser failed", deleteUser.success());
        Assert.assertNull("deleteUser failed with error", deleteUser.getError());

        final AuthenticateUser failedAuth = client.callService(new AuthenticateUser(testId, "test"));
        Assert.assertFalse("failedAuth success", failedAuth.success());
        Assert.assertNotNull("failedAuth failed without error", failedAuth.getError());
        Assert.assertEquals("failedAuth error must match", RestError.INVALID_CREDENTIALS, failedAuth.getError());
        Assert.assertNull("failedAuth failed, token is not null", failedAuth.getToken());
    }
}
