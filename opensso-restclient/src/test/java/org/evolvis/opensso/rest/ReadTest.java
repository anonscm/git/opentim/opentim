package org.evolvis.opensso.rest;

import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;
import org.evolvis.opensso.rest.constant.UserAttribute;
import org.evolvis.opensso.rest.constant.UserStatus;
import org.evolvis.opensso.rest.service.CreateUser;
import org.evolvis.opensso.rest.service.DeleteUser;
import org.evolvis.opensso.rest.service.Read;
import org.junit.Assert;
import org.junit.Test;

public class ReadTest extends AbstractRestTest {

    @Test
    public void testNullRealmFailing() throws Exception {

        Exception exception = null;
        try {
            client.callService(new Read(null, ""));
        }
        catch (final IllegalArgumentException iae) {
            exception = iae;
        }

        Assert.assertNotNull("IllegalArgumentException must be thrown", exception);
    }


    @Test
    public void testNullUidFailing() throws Exception {

        Exception exception = null;
        try {
            client.callService(new Read("", null));
        }
        catch (final IllegalArgumentException iae) {
            exception = iae;
        }

        Assert.assertNotNull("IllegalArgumentException must be thrown", exception);
    }


    @Test
    public void testNullRealmUidFailing() throws Exception {

        Exception exception = null;
        try {
            client.callService(new Read(null, null));
        }
        catch (final IllegalArgumentException iae) {
            exception = iae;
        }

        Assert.assertNotNull("IllegalArgumentException must be thrown", exception);
    }


    @Test
    public void testReadByUid() throws Exception {

        final String realm = TEST_REALM;
        final String uid = RandomStringUtils.randomAlphanumeric(8);
        final String passwd = RandomStringUtils.randomAlphanumeric(8);
        final String firstname = "aFirstname";
        final String surname = "aSurname";
        final String email = "aEmail";
        final String userStatus = UserStatus.ACTIVE;
        final String userUUID = UUID.randomUUID().toString();

        try {
            // create user
            final CreateUser createUser = client.callService(new CreateUser(realm, uid, passwd, firstname, surname, email, userStatus,
                    userUUID));
            Assert.assertTrue("createUser must success, error=" + createUser.getError(), createUser.success());

            // search userUUID
            final Read read = client.callService(new Read(realm, uid));
            Assert.assertTrue("read must success, error=" + read.getError(), read.success());

            // check for attributes
            Assert.assertFalse("attributes must be found, attribute: " + read.getAllAttributeValues(), read.getAllAttributeValues()
                    .isEmpty());

            // check every attribute

            Assert.assertTrue("uid must be equal, " + uid + " != " + read.getAttributeValue(UserAttribute.UID), uid.equals(read
                    .getAttributeValue(UserAttribute.UID)));

            Assert.assertTrue("firstname must be equal, " + firstname + " != " + read.getAttributeValue(UserAttribute.FIRSTNAME), firstname
                    .equals(read.getAttributeValue(UserAttribute.FIRSTNAME)));

            Assert.assertTrue("surname must be equal, " + surname + " != " + read.getAttributeValue(UserAttribute.LASTNAME), surname
                    .equals(read.getAttributeValue(UserAttribute.LASTNAME)));

            Assert.assertTrue("email must be equal, " + email + " != " + read.getAttributeValue(UserAttribute.EMAIL), email.equals(read
                    .getAttributeValue(UserAttribute.EMAIL)));

            Assert.assertTrue("userStatus must be equal, " + userStatus + " != " + read.getAttributeValue(UserAttribute.USERSTATUS),
                    userStatus.equals(read.getAttributeValue(UserAttribute.USERSTATUS)));

            Assert.assertTrue("userUUID must be equal, " + userUUID + " != " + read.getAttributeValue(UserAttribute.USER_UUID), userUUID
                    .equals(read.getAttributeValue(UserAttribute.USER_UUID)));
        }
        finally {
            client.callService(new DeleteUser(realm, uid));
        }
    }
}
