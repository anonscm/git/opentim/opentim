package org.evolvis.opensso.rest.jmeter;

import static org.junit.Assert.assertNotNull;

import org.evolvis.opensso.rest.service.DeleteUser;
import org.junit.Assert;
import org.junit.Test;

/**
 * 
 * @author Fabian Bieker
 *
 */
public class DeleteUserTest extends AbstractJMeterRestTest {

	@Test
    public void testDeleteUser() throws Exception {
		
		assertNotNull("userid not set - failure in prev. test?", getUserId());
		
        final DeleteUser deleteUser = client.callService(new DeleteUser(TEST_REALM, getUserId()));
        Assert.assertTrue("deleteUser failed", deleteUser.success());
        Assert.assertNull("deleteUser failed with error", deleteUser.getError());
        
        // avoid reuse of userid
        setUserId(null);
    }
}
