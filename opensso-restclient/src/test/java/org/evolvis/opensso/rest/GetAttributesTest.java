package org.evolvis.opensso.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.evolvis.opensso.rest.constant.UserAttribute;
import org.evolvis.opensso.rest.constant.UserStatus;
import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.evolvis.opensso.rest.service.CreateUser;
import org.evolvis.opensso.rest.service.DeleteUser;
import org.evolvis.opensso.rest.service.GetAttributes;
import org.junit.Test;

public class GetAttributesTest extends AbstractRestTest {

    private final String testId = String.valueOf(System.currentTimeMillis());

    @Test
    public void testRealmGetAttributes() throws Exception {
        final String token = client.callService(new AuthenticateUser(TEST_REALM, TEST_UID, TEST_PWD)).getToken();
        assertNotNull("token is null", token);

        final Map<String, List<String>> attributes = client.callService(new GetAttributes(token)).attributes();
        assertNotNull("attributes is null", attributes);
        assertFalse("attributes is empty", attributes.isEmpty());
    }

    @Test
    public void testSubrealmGetAttributes() throws Exception {
        final String token = client.callService(new AuthenticateUser(TEST_SUBREALM, TEST_UID, TEST_PWD)).getToken();
        assertNotNull("token is null", token);

        final Map<String, List<String>> attributes = client.callService(new GetAttributes(token)).attributes();
        assertNotNull("attributes is null", attributes);
        assertFalse("attributes is empty", attributes.isEmpty());
    }

    @Test
    public void testRealmGetAttributesFailing() throws Exception {
        final String token = client.callService(new AuthenticateUser(TEST_REALM, TEST_UID, TEST_PWD)).getToken();
        assertNotNull("token is null", token);

        final Map<String, List<String>> attributes = client.callService(new GetAttributes(token)).attributes();
        assertNotNull("attributes is null", attributes);
        assertFalse("attributes is empty", attributes.isEmpty());
    }
    
    @Test
    public void testGetAttributesAndCompare() throws Exception {

        final Map<String, String> attributes = new HashMap<String, String>();

        try {
            // fixtures
            attributes.put(UserAttribute.UID, "theID_" + testId);
            attributes.put(UserAttribute.PASSWORD, "thePassword");
            attributes.put(UserAttribute.FIRSTNAME, "theFirstName");
            attributes.put(UserAttribute.LASTNAME, "theLastName");
            attributes.put(UserAttribute.EMAIL, "theEmail");
            attributes.put(UserAttribute.USERSTATUS, UserStatus.ACTIVE.toString());
            attributes.put(UserAttribute.USER_UUID, java.util.UUID.randomUUID().toString());

            client.callService(new CreateUser(TEST_REALM, attributes.get(UserAttribute.UID), attributes
                    .get(UserAttribute.PASSWORD), attributes.get(UserAttribute.FIRSTNAME),
                    attributes.get(UserAttribute.LASTNAME), attributes.get(UserAttribute.EMAIL), attributes
                            .get(UserAttribute.USERSTATUS), attributes.get(UserAttribute.USER_UUID)));

            final AuthenticateUser authenticateUser = client.callService(new AuthenticateUser(attributes.get(UserAttribute.UID),
                    attributes.get(UserAttribute.PASSWORD)));

            assertNotNull("AuthenticateUser must not return null", authenticateUser);
            assertNotNull("User token is null but user is active", authenticateUser.getToken());

            final Map<String, List<String>> storedAttributes = client.callService(new GetAttributes(authenticateUser.getToken()))
                    .attributes();

            assertNotNull("Attributes map for user must not be null", storedAttributes);

            for (final String key : attributes.keySet()) {
                assertNotNull("Attribute missing: " + key, storedAttributes.get(key));
                assertEquals("Attribute " + key + " not set correctly!", attributes.get(key), storedAttributes.get(key).get(0));
            }
        }
        finally {
            // clean up
            client.callService(new DeleteUser(TEST_REALM, attributes.get(UserAttribute.UID)));
        }
    }
}
