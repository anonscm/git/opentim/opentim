package org.evolvis.opensso.rest.jmeter;

import static org.junit.Assert.assertNotNull;

import org.evolvis.opensso.rest.service.IsTokenValid;
import org.evolvis.opensso.rest.service.Logout;
import org.junit.Assert;
import org.junit.Test;


/**
 * 
 * @author Fabian Bieker
 *
 */
public class LogoutTest extends AbstractJMeterRestTest {
	
	@Test
    public void testLogout() throws Exception {
        
		assertNotNull("userid not set - failure in prev. test?", getUserId());
		assertNotNull("session token not set - failure in prev. test?", getSessionToken());
		
        Boolean valid = client.callService(new IsTokenValid(getSessionToken())).valid();
        Assert.assertTrue("token not valid", valid);

        client.callService(new Logout(getSessionToken()));

        valid = client.callService(new IsTokenValid(getSessionToken())).valid();
        Assert.assertFalse("token still valid", valid);
        
        // avoid reuse of this token
        setSessionToken(null);

    }
	
}
