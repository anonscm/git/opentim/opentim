package org.evolvis.opensso.rest;

import org.evolvis.opensso.rest.service.GetCookieName;
import org.junit.Assert;
import org.junit.Test;

public class GetCookieNameTest extends AbstractRestTest {

    @Test
    public void testGetCookieName() throws Exception {
        final GetCookieName getCookieName = client.callService(new GetCookieName());
        Assert.assertNotNull("success must be true", getCookieName.success());
        Assert.assertNotNull("tokenCookieName must be not null", getCookieName.getTokenCookieName());
    }
}
