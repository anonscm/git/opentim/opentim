package org.evolvis.opensso.rest;

import java.util.List;
import java.util.Map;

import org.evolvis.opensso.rest.constant.UserStatus;
import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.evolvis.opensso.rest.service.CreateUser;
import org.evolvis.opensso.rest.service.DeleteUser;
import org.evolvis.opensso.rest.service.GetAttributes;
import org.evolvis.opensso.rest.service.IsTokenValid;
import org.evolvis.opensso.rest.service.Logout;
import org.junit.Assert;
import org.junit.Test;

public class AllUserTests extends AbstractRestTest {

    @Test
    public void testAll() throws Exception {
        final String testId = String.valueOf(System.currentTimeMillis());

        client.callService(new CreateUser(TEST_REALM, testId, "test", testId + "_first", testId + "_last", testId + "_email",
                UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));

        final String token = client.callService(new AuthenticateUser(testId, "test")).getToken();
        Assert.assertNotNull("token is null", token);

        Boolean valid = client.callService(new IsTokenValid(token)).valid();
        Assert.assertTrue("token not valid", valid);

        final Map<String, List<String>> attributes = client.callService(new GetAttributes(token)).attributes();
        Assert.assertFalse("attributes is empty", attributes.isEmpty());

        client.callService(new Logout(token));

        valid = client.callService(new IsTokenValid(token)).valid();
        Assert.assertFalse("token still valid", valid);

        final DeleteUser deleteUser = client.callService(new DeleteUser(TEST_REALM, testId));
        Assert.assertTrue("deleteUser failed", deleteUser.success());
        Assert.assertNull("deleteUser failed with error", deleteUser.getError());
    }
}
