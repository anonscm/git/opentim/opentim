package org.evolvis.opensso.rest.jmeter;

import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Map;

import org.evolvis.opensso.rest.service.GetAttributes;
import org.evolvis.opensso.rest.service.IsTokenValid;
import org.junit.Assert;
import org.junit.Test;

/**
 * 
 * @author Fabian Bieker
 * 
 */
public class GetAttributesTest extends AbstractJMeterRestTest {

	@Test
	public void testGetAttr() throws Exception {

		assertNotNull("userid not set - failure in prev. test?", getUserId());
		assertNotNull("session token not set - failure in prev. test?", getSessionToken());
		
		Boolean valid = client.callService(new IsTokenValid(getSessionToken()))
				.valid();
		Assert.assertTrue("token not valid", valid);

		final Map<String, List<String>> attributes = client.callService(
				new GetAttributes(getSessionToken())).attributes();
		Assert.assertFalse("attributes is empty", attributes.isEmpty());

	}
}
