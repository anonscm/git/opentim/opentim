package org.evolvis.opensso.rest.jmeter;

import static org.junit.Assert.assertNotNull;

import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.evolvis.opensso.rest.service.IsTokenValid;
import org.junit.Assert;
import org.junit.Test;

/**
 * 
 * @author Fabian Bieker
 *
 */
public class LoginUserTest extends AbstractJMeterRestTest {

	@Test
    public void testLogin() throws Exception {
		
		assertNotNull("userid not set - failure in prev. test?", getUserId());
        
        final String token = client.callService(new AuthenticateUser(getUserId(), "test")).getToken();
        Assert.assertNotNull("token is null", token);

        Boolean valid = client.callService(new IsTokenValid(token)).valid();
        Assert.assertTrue("token not valid", valid);
        
        setSessionToken(token);

    }
}
