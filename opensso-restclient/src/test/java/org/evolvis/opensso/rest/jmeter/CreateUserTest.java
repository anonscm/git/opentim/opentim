/**
 * 
 */
package org.evolvis.opensso.rest.jmeter;

import java.io.IOException;
import java.util.Random;

import org.evolvis.opensso.rest.constant.UserStatus;
import org.evolvis.opensso.rest.service.CreateUser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Fabian Bieker
 * 
 */
public class CreateUserTest extends AbstractJMeterRestTest {

	private static final Random rnd = new Random();
	
	@Before
	public void clearState() {
		// when running the jmeter tests in a loop make sure, no old data from
		// prev. test runs is present (the prev. run might have failed ...)
		setUserId(null);
		setSessionToken(null);
	}

	@Test
	public void testCreateUser() throws IOException {
		final String testId = String.valueOf(System.currentTimeMillis()) + "-"
				+ Thread.currentThread().getId() + "-" + rnd.nextInt();

		final CreateUser createUser = client.callService(new CreateUser(
				TEST_REALM, testId, "test", testId + "_first",
				testId + "_last", testId + "_email", UserStatus.ACTIVE));

		Assert.assertTrue("createUser failed - " + createUser.getError(),
				createUser.success());

		setUserId(testId);
	}

}
