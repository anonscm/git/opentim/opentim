package org.evolvis.opensso.rest;

import org.evolvis.opensso.rest.constant.RestError;
import org.evolvis.opensso.rest.constant.UserStatus;
import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.evolvis.opensso.rest.service.CreateUser;
import org.evolvis.opensso.rest.service.DeleteUser;
import org.junit.Assert;
import org.junit.Test;

public class CreateUserTest extends AbstractRestTest {

    private final String testId = String.valueOf(System.currentTimeMillis());

    @Test
    public void testCreateActiveUser() throws Exception {
        try {
            final CreateUser createUser = client.callService(new CreateUser(TEST_REALM, testId, "test", testId + "_first", testId
                    + "_last", testId + "_email", UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));
            Assert.assertTrue("createUser must success", createUser.success());

            final AuthenticateUser authenticateUser = client.callService(new AuthenticateUser(TEST_REALM, testId, "test"));
            Assert.assertNotNull("token must not be null", authenticateUser.getToken());
            Assert.assertTrue("success must be true", authenticateUser.success());
            Assert.assertNull("error must be null", authenticateUser.getError());
        }
        // clean up
        finally {
            try {
                client.callService(new DeleteUser(TEST_REALM, testId));
            }
            catch (final Exception e) {
                log.warn("cleaning failed", e);
            }
        }
    }

    @Test
    public void testCreateActiveSubrealmUser() throws Exception {
        try {
            final CreateUser createUser = client.callService(new CreateUser(TEST_SUBREALM, testId, "test", testId + "_first",
                    testId + "_last", testId + "_email", UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));
            Assert.assertTrue("createUser must success", createUser.success());

            final AuthenticateUser authenticateUser = client.callService(new AuthenticateUser(TEST_SUBREALM, testId, "test"));
            Assert.assertNotNull("token must not be null", authenticateUser.getToken());
            Assert.assertTrue("success must be true", authenticateUser.success());
            Assert.assertNull("error must be null", authenticateUser.getError());
        }
        // clean up
        finally {
            try {
                client.callService(new DeleteUser(TEST_SUBREALM, testId));
            }
            catch (final Exception e) {
                log.warn("cleaning failed", e);
            }
        }
    }

    @Test
    public void testCreateInactiveUser() throws Exception {
        try {
            final CreateUser createUser = client.callService(new CreateUser(TEST_REALM, testId, "test", testId + "_first", testId
                    + "_last", testId + "_email", UserStatus.INACTIVE, java.util.UUID.randomUUID().toString()));
            Assert.assertTrue("createUser must success", createUser.success());

            final AuthenticateUser authenticateUser = client.callService(new AuthenticateUser(TEST_REALM, testId, "test"));
            Assert.assertNull("token must be null", authenticateUser.getToken());
            Assert.assertFalse("success must be false", authenticateUser.success());
            Assert.assertNotNull("error must be not null", authenticateUser.getError());
            Assert.assertEquals("error must equal constant", RestError.USER_NOT_FOUND, authenticateUser.getError());
        }
        // clean up
        finally {
            try {
                client.callService(new DeleteUser(TEST_REALM, testId));
            }
            catch (final Exception e) {
                log.warn("cleaning failed", e);
            }
        }
    }

    @Test
    public void testCreateInactiveSubrealmUser() throws Exception {
        try {
            final CreateUser createUser = client.callService(new CreateUser(TEST_SUBREALM, testId, "test", testId + "_first",
                    testId + "_last", testId + "_email", UserStatus.INACTIVE, java.util.UUID.randomUUID().toString()));
            Assert.assertTrue("createUser must success", createUser.success());

            final AuthenticateUser authenticateUser = client.callService(new AuthenticateUser(TEST_SUBREALM, testId, "test"));
            Assert.assertNull("token must be null", authenticateUser.getToken());
            Assert.assertFalse("success must be false", authenticateUser.success());
            Assert.assertNotNull("error must be not null", authenticateUser.getError());
            Assert.assertEquals("error must equal constant", RestError.USER_NOT_FOUND, authenticateUser.getError());
        }
        // clean up
        finally {
            try {
                client.callService(new DeleteUser(TEST_SUBREALM, testId));
            }
            catch (final Exception e) {
                log.warn("cleaning failed", e);
            }
        }
    }

    @Test
    public void testCreateDuplicateUser() throws Exception {
        try {
            // must succeed
            final CreateUser createUser = client.callService(new CreateUser(TEST_REALM, testId, "test", testId + "_first", testId
                    + "_last", testId + "_email", UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));
            Assert.assertTrue("createUser must succeed", createUser.success());
            Assert.assertNull("error must be null", createUser.getError());

            // must fail
            final CreateUser failedCreateUser = client.callService(new CreateUser(TEST_REALM, testId, "test", testId + "_first",
                    testId + "_last", testId + "_email", UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));
            Assert.assertFalse("createUser must not succeed", failedCreateUser.success());
            Assert.assertNotNull("error must not be null", failedCreateUser.getError());
            Assert.assertEquals("error must equal constant", RestError.LDAP_ENTRY_ALREADY_EXISTS, failedCreateUser.getError());
        }
        // clean up
        finally {
            try {
                client.callService(new DeleteUser(TEST_REALM, testId));
            }
            catch (final Exception e) {
                log.warn("cleaning failed", e);
            }
        }
    }

    @Test
    public void testCreateDuplicateSubrealmUser() throws Exception {
        try {
            // must succeed
            final CreateUser createUser = client.callService(new CreateUser(TEST_SUBREALM, testId, "test", testId + "_first",
                    testId + "_last", testId + "_email", UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));
            Assert.assertTrue("createUser must succeed", createUser.success());
            Assert.assertNull("error must be null", createUser.getError());

            // must fail
            final CreateUser failedCreateUser = client.callService(new CreateUser(TEST_SUBREALM, testId, "test", testId
                    + "_first", testId + "_last", testId + "_email", UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));
            Assert.assertFalse("createUser must not succeed", failedCreateUser.success());
            Assert.assertNotNull("error must not be null", failedCreateUser.getError());
            Assert.assertEquals("error must equal constant", RestError.LDAP_ENTRY_ALREADY_EXISTS, failedCreateUser.getError());
        }
        // clean up
        finally {
            try {
                client.callService(new DeleteUser(TEST_SUBREALM, testId));
            }
            catch (final Exception e) {
                log.warn("cleaning failed", e);
            }
        }
    }
}
