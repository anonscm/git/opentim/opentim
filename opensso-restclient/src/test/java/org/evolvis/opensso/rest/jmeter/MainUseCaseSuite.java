/**
 * 
 */
package org.evolvis.opensso.rest.jmeter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * 
 * A Testsuite to run the JMeter related tests for the "main" use case (Login,
 * Logout etc.) in the correct order ...
 * 
 * @author Fabian Bieker
 * 
 */
@RunWith(Suite.class)
@SuiteClasses( { CreateUserTest.class, LoginUserTest.class,
		GetAttributesTest.class, LogoutTest.class, DeleteUserTest.class })
public class MainUseCaseSuite {
	
	@Test
	public void testDummy() {
		// just to make maven happy ...
	}

}
