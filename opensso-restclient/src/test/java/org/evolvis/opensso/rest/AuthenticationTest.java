package org.evolvis.opensso.rest;

import org.evolvis.opensso.rest.constant.RestError;
import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.junit.Assert;
import org.junit.Test;

public class AuthenticationTest extends AbstractRestTest {

    @Test
    public void testAdminAuthentication() throws Exception {
        final AuthenticateUser authenticateUser = client.callService(new AuthenticateUser("amAdmin", "amAdmin!"));
        Assert.assertNotNull("token must not be null", authenticateUser.getToken());
        Assert.assertTrue("authenticateUser must success", authenticateUser.success());
        Assert.assertNull("error must be null", authenticateUser.getError());
    }

    @Test
    public void testUserAuthentication() throws Exception {
        final AuthenticateUser authenticateUser = client.callService(new AuthenticateUser(TEST_UID, TEST_PWD));
        Assert.assertNotNull("token must not be null", authenticateUser.getToken());
        Assert.assertTrue("authenticateUser must success", authenticateUser.success());
        Assert.assertNull("error must be null", authenticateUser.getError());
    }

    @Test
    public void testFailureAuthentication() throws Exception {
        final AuthenticateUser authenticateUser = client.callService(new AuthenticateUser("abc", "abc"));
        Assert.assertNull("token must be null", authenticateUser.getToken());
        Assert.assertFalse("authenticateUser must not success", authenticateUser.success());
        Assert.assertNotNull("error must not be null", authenticateUser.getError());
        Assert.assertEquals("error must equal constant", RestError.INVALID_CREDENTIALS, authenticateUser.getError());
    }

    @Test
    public void testRealmAuthentication() throws Exception {
        final AuthenticateUser authenticateUser = client.callService(new AuthenticateUser(TEST_SUBREALM, TEST_UID, TEST_PWD));
        Assert.assertNotNull("token must be not null", authenticateUser.getToken());
        Assert.assertTrue("success must be true", authenticateUser.success());
        Assert.assertNull("error must be null", authenticateUser.getError());
    }

    @Test
    public void testFailureRealmAuthentication() throws Exception {
        final AuthenticateUser authenticateUser = client.callService(new AuthenticateUser("/abc", "abc", "abc"));
        Assert.assertNull("token must be null", authenticateUser.getToken());
        Assert.assertFalse("authenticateUser must not success", authenticateUser.success());
        Assert.assertNotNull("error must not be null", authenticateUser.getError());
        Assert.assertEquals("error must equal constant", RestError.GENERAL_FAILURE, authenticateUser.getError());
    }
}
