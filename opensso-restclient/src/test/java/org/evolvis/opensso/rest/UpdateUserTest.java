package org.evolvis.opensso.rest;

import java.util.List;
import java.util.Map;

import org.evolvis.opensso.rest.constant.RestConfig;
import org.evolvis.opensso.rest.constant.RestError;
import org.evolvis.opensso.rest.constant.UserAttribute;
import org.evolvis.opensso.rest.constant.UserStatus;
import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.evolvis.opensso.rest.service.CreateUser;
import org.evolvis.opensso.rest.service.DeleteUser;
import org.evolvis.opensso.rest.service.GetAttributes;
import org.evolvis.opensso.rest.service.UpdateUser;
import org.junit.Assert;
import org.junit.Test;

public class UpdateUserTest extends AbstractRestTest {

    private final String testId = String.valueOf(System.currentTimeMillis());

    @Test
    public void testUpdateUserStatus() throws Exception {

        // create inactive user
        final CreateUser createUser = client.callService(new CreateUser(TEST_REALM, testId, "test", testId + "_first", testId
                + "_last", testId + "_email", UserStatus.INACTIVE, java.util.UUID.randomUUID().toString()));
        Assert.assertTrue("createUser must succeed", createUser.success());
        Assert.assertNull("error must be null", createUser.getError());

        // must fail
        final AuthenticateUser authInactiveUser = client.callService(new AuthenticateUser(testId, "test"));
        Assert.assertNull("token must be null", authInactiveUser.getToken());
        Assert.assertNotNull("error must not be null", authInactiveUser.getError());
        Assert.assertEquals("error must equal constant", RestError.USER_NOT_FOUND, authInactiveUser.getError());

        // update status
        final UpdateUser updateUser = client.callService(new UpdateUser(TEST_REALM, testId).setAttr(UserAttribute.USERSTATUS,
                UserStatus.ACTIVE));
        Assert.assertTrue("update wasn't successful", updateUser.success());
        Assert.assertNull("update indicated error", updateUser.getError());

        // must not fail
        final AuthenticateUser authActiveUser = client.callService(new AuthenticateUser(testId, "test"));
        Assert.assertNotNull("token must not be null", authActiveUser.getToken());
        Assert.assertNull("error must be null", authActiveUser.getError());

        // clean up
        client = getRestClient();
        client.callService(new DeleteUser(TEST_REALM, testId));
    }

    @Test
    public void testUpdateUserPassword() throws Exception {

        // create user
        final CreateUser createUser = client.callService(new CreateUser(TEST_REALM, testId, "testtest", testId + "_first", testId
                + "_last", testId + "_email", UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));
        Assert.assertTrue("createUser must succeed", createUser.success());
        Assert.assertNull("error must be null", createUser.getError());

        // must succeed
        final AuthenticateUser passOneUser = client.callService(new AuthenticateUser(testId, "testtest"));
        Assert.assertNotNull("token must not be null", passOneUser.getToken());
        Assert.assertNull("error must be null", passOneUser.getError());

        // update password
        final UpdateUser updateUser = client.callService(new UpdateUser(TEST_REALM, testId).setAttr(UserAttribute.PASSWORD,
                "test"));
        Assert.assertTrue("update wasn't successful", updateUser.success());
        Assert.assertNull("update has error", updateUser.getError());

        // must succeed
        final AuthenticateUser passSecondUser = client.callService(new AuthenticateUser(testId, "test"));
        Assert.assertNotNull("token must not be null", passSecondUser.getToken());
        Assert.assertNull("error must be null", passSecondUser.getError());

        // clean up
        client = getRestClient();
        client.callService(new DeleteUser(TEST_REALM, testId));
    }

    @Test
    public void testAdminAuthFailed() throws Exception {

        // create user
        final CreateUser createUser = client.callService(new CreateUser(TEST_REALM, testId, "test", testId + "_first", testId
                + "_last", testId + "_email", UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));
        Assert.assertTrue("createUser must success", createUser.success());
        Assert.assertNull("error must null", createUser.getError());

        client = RestClient.getInstance();
        client.initialize(RestConfig.SERVICE_URL);
        client.setAdminToken("abc1234");

        // must not work
        final UpdateUser updateUser = client.callService(new UpdateUser(TEST_REALM, testId).setAttr(UserAttribute.PASSWORD,
                "testtest"));
        Assert.assertFalse("update must not be successful", updateUser.success());
        Assert.assertNotNull("error must not null", updateUser.getError());
        Assert.assertEquals("error must equals constant", RestError.TOKEN_EXPIRED, updateUser.getError());

        // clean up
        client = getRestClient();
        client.callService(new DeleteUser(TEST_REALM, testId));
    }

    @Test
    public void testPasswordRegisterUUID() throws Exception {
        try {
            // create user
            final CreateUser createUser = client.callService(new CreateUser(TEST_REALM, testId, TEST_PWD, testId + "_first",
                    testId + "_last", testId + "_email", UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));
            Assert.assertTrue("createUser must succeed", createUser.success());
            Assert.assertNull("error must be null", createUser.getError());

            final String passwordUUID = java.util.UUID.randomUUID().toString();
            final String registerUUID = java.util.UUID.randomUUID().toString();

            // update UUIDs
            final UpdateUser updateUser = client.callService(new UpdateUser(TEST_REALM, testId).setAttr(
                    UserAttribute.PASSWORD_UUID, passwordUUID).setAttr(UserAttribute.REGISTER_UUID, registerUUID));
            Assert.assertTrue("update wasn't successful", updateUser.success());
            Assert.assertNull("update has error", updateUser.getError());

            // login
            final String userToken = client.callService(new AuthenticateUser(TEST_REALM, testId, TEST_PWD)).getToken();
            Assert.assertNotNull("token is null", userToken);

            // get attributes
            final Map<String, List<String>> attributes = client.callService(new GetAttributes(userToken)).attributes();

            // validate passwordUUID
            final List<String> passwordUUIDAttrs = attributes.get(UserAttribute.PASSWORD_UUID);
            Assert.assertNotNull("passwordUUIDAttrs must not be null", passwordUUIDAttrs);
            Assert.assertTrue("passwordUUID must exist ones", passwordUUIDAttrs.size() == 1);
            Assert.assertTrue("passwordUUID must match generated UUID", passwordUUID.equals(passwordUUIDAttrs.get(0)));
            
            // validate registerUUID
            final List<String> registerUUIDAttrs = attributes.get(UserAttribute.REGISTER_UUID);
            Assert.assertNotNull("registerUUIDAttrs must not be null", registerUUIDAttrs);
            Assert.assertTrue("registerUUID must exist ones", registerUUIDAttrs.size() == 1);
            Assert.assertTrue("registerUUID must match generated UUID", registerUUID.equals(registerUUIDAttrs.get(0)));
        }
        // clean up
        finally {
            client.callService(new DeleteUser(TEST_REALM, testId));
        }
    }

    @Test
    public void testSubrealmPasswordRegisterUUID() throws Exception {
        try {
            // create user
            final CreateUser createUser = client.callService(new CreateUser(TEST_SUBREALM, testId, TEST_PWD, testId + "_first",
                    testId + "_last", testId + "_email", UserStatus.ACTIVE, java.util.UUID.randomUUID().toString()));
            Assert.assertTrue("createUser must succeed", createUser.success());
            Assert.assertNull("error must be null", createUser.getError());

            final String passwordUUID = java.util.UUID.randomUUID().toString();
            final String registerUUID = java.util.UUID.randomUUID().toString();

            // update UUIDs
            final UpdateUser updateUser = client.callService(new UpdateUser(TEST_SUBREALM, testId).setAttr(
                    UserAttribute.PASSWORD_UUID, passwordUUID).setAttr(UserAttribute.REGISTER_UUID, registerUUID));
            Assert.assertTrue("update wasn't successful", updateUser.success());
            Assert.assertNull("update has error", updateUser.getError());

            // login
            final String userToken = client.callService(new AuthenticateUser(TEST_SUBREALM, testId, TEST_PWD)).getToken();
            Assert.assertNotNull("token is null", userToken);

            // get attributes
            final Map<String, List<String>> attributes = client.callService(new GetAttributes(userToken)).attributes();

            // validate passwordUUID
            final List<String> passwordUUIDAttrs = attributes.get(UserAttribute.PASSWORD_UUID);
            Assert.assertNotNull("passwordUUIDAttrs must not be null", passwordUUIDAttrs);
            Assert.assertTrue("passwordUUID must exist ones", passwordUUIDAttrs.size() == 1);
            Assert.assertTrue("passwordUUID must match generated UUID", passwordUUID.equals(passwordUUIDAttrs.get(0)));
            
            // validate registerUUID
            final List<String> registerUUIDAttrs = attributes.get(UserAttribute.REGISTER_UUID);
            Assert.assertNotNull("registerUUIDAttrs must not be null", registerUUIDAttrs);
            Assert.assertTrue("registerUUID must exist ones", registerUUIDAttrs.size() == 1);
            Assert.assertTrue("registerUUID must match generated UUID", registerUUID.equals(registerUUIDAttrs.get(0)));
        }
        // clean up
        finally {
            client.callService(new DeleteUser(TEST_SUBREALM, testId));
        }
    }
}
