package org.evolvis.opensso.config;

import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_ATTR_EMAIL;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_ATTR_FIRSTNAME;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_ATTR_FULLNAME;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_ATTR_LASTNAME;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_ATTR_PASSWORD;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_ATTR_PASSWORD_UUID;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_ATTR_REGISTER_UUID;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_ATTR_UID;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_ATTR_USERSTATUS;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_ATTR_USER_UUID;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_ENABLED;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_SERVICE_LOGIN;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_SERVICE_PASSWD;
import static org.evolvis.opensso.rest.constant.PropSSOKeys.TARENT_SSO_SERVICE_URL;

import org.evolvis.config.Configuration;
import org.evolvis.config.PropertyConfig;
import org.evolvis.opensso.rest.constant.PropRestKeys;
import org.evolvis.opensso.rest.constant.RestConfig;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Tino Rink
 */
public class ConfigTests {

    @Test
    public void testRestConfigKeys() {

        final Configuration config = PropertyConfig.getInstance();
        Assert.assertNotNull("config must not null", config);

        final String maxConnections = config.get(PropRestKeys.TARENT_SSO_MAX_CONNECTIONS);
        Assert.assertNotNull("maxConnections must not null", maxConnections);

        final String maxHostConnections = config.get(PropRestKeys.TARENT_SSO_MAX_HOST_CONNECTIONS);
        Assert.assertNotNull("maxHostConnections must not null", maxHostConnections);
    }

    @Test
    public void testSSOConfigKeys() {

        final Configuration config = PropertyConfig.getInstance();
        Assert.assertNotNull("config must not null", config);

        final String ssoEnabled = config.get(TARENT_SSO_ENABLED);
        Assert.assertNotNull("ssoEnabled must not null", ssoEnabled);

        final String ssoServiceUrl = config.get(TARENT_SSO_SERVICE_URL);
        Assert.assertNotNull("ssoServiceUrl must not null", ssoServiceUrl);

        final String ssoServiceLogin = config.get(TARENT_SSO_SERVICE_LOGIN);
        Assert.assertNotNull("ssoServiceLogin must not null", ssoServiceLogin);

        final String ssoServicePasswd = config.get(TARENT_SSO_SERVICE_PASSWD);
        Assert.assertNotNull("ssoServicePasswd must not null", ssoServicePasswd);

        final String ssoAttrUid = config.get(TARENT_SSO_ATTR_UID);
        Assert.assertNotNull("ssoAttrUid must not null", ssoAttrUid);

        final String ssoAttrPassword = config.get(TARENT_SSO_ATTR_PASSWORD);
        Assert.assertNotNull("ssoAttrPassword must not null", ssoAttrPassword);

        final String ssoAttrEmail = config.get(TARENT_SSO_ATTR_EMAIL);
        Assert.assertNotNull("ssoAttrEmail must not null", ssoAttrEmail);

        final String ssoAttrFirstname = config.get(TARENT_SSO_ATTR_FIRSTNAME);
        Assert.assertNotNull("ssoAttrFirstname must not null", ssoAttrFirstname);

        final String ssoAttrLastname = config.get(TARENT_SSO_ATTR_LASTNAME);
        Assert.assertNotNull("ssoAttrLastname must not null", ssoAttrLastname);

        final String ssoAttrFullname = config.get(TARENT_SSO_ATTR_FULLNAME);
        Assert.assertNotNull("ssoAttrFullname must not null", ssoAttrFullname);

        final String ssoAttrUserstatus = config.get(TARENT_SSO_ATTR_USERSTATUS);
        Assert.assertNotNull("ssoAttrUserstatus must not null", ssoAttrUserstatus);

        final String ssoAttrUserUuid = config.get(TARENT_SSO_ATTR_USER_UUID);
        Assert.assertNotNull("ssoAttrUserUuid must not null", ssoAttrUserUuid);

        final String ssoAttrPasswordUuid = config.get(TARENT_SSO_ATTR_PASSWORD_UUID);
        Assert.assertNotNull("ssoAttrPasswordUuid must not null", ssoAttrPasswordUuid);

        final String ssoAttrRegisterUuid = config.get(TARENT_SSO_ATTR_REGISTER_UUID);
        Assert.assertNotNull("ssoAttrRegisterUuid must not null", ssoAttrRegisterUuid);
    }

    @Test
    public void testRestConfig() {

        Assert.assertNotNull("SERVICE_URL must not null", RestConfig.SERVICE_URL);

        Assert.assertNotNull("SERVICE_LOGIN must not null", RestConfig.SERVICE_LOGIN);

        Assert.assertNotNull("SERVICE_PASSWD must not null", RestConfig.SERVICE_PASSWD);

        Assert.assertNotNull("MAX_CONNECTIONS must not null", RestConfig.MAX_CONNECTIONS);

        Assert.assertNotNull("MAX_HOST_CONNECTIONS must not null", RestConfig.MAX_HOST_CONNECTIONS);
    }
}
