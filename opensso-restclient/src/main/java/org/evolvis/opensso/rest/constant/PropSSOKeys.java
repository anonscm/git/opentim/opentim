package org.evolvis.opensso.rest.constant;

public class PropSSOKeys {

    public static final String TARENT_SSO_ENABLED = "tarent.sso.enabled";

    public static final String TARENT_SSO_SERVICE_URL = "tarent.sso.service.url";

    public static final String TARENT_SSO_SERVICE_LOGIN = "tarent.sso.service.login";

    public static final String TARENT_SSO_SERVICE_PASSWD = "tarent.sso.service.passwd";

    public static final String TARENT_SSO_ATTR_UID = "tarent.sso.attr.uid";

    public static final String TARENT_SSO_ATTR_PASSWORD = "tarent.sso.attr.password";

    public static final String TARENT_SSO_ATTR_EMAIL = "tarent.sso.attr.email";

    public static final String TARENT_SSO_ATTR_FIRSTNAME = "tarent.sso.attr.firstname";

    public static final String TARENT_SSO_ATTR_LASTNAME = "tarent.sso.attr.lastname";

    public static final String TARENT_SSO_ATTR_FULLNAME = "tarent.sso.attr.fullname";

    public static final String TARENT_SSO_ATTR_USERSTATUS = "tarent.sso.attr.userstatus";

    public static final String TARENT_SSO_ATTR_USER_UUID = "tarent.sso.attr.userUUID";

    public static final String TARENT_SSO_ATTR_PASSWORD_UUID = "tarent.sso.attr.passwordUUID";

    public static final String TARENT_SSO_ATTR_REGISTER_UUID = "tarent.sso.attr.registerUUID";
}
