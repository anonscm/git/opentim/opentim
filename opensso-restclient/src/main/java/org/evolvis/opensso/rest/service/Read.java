package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpStatus;
import org.evolvis.opensso.rest.constant.RestKey;
import org.evolvis.opensso.rest.constant.RestValue;

public class Read extends AbstractRestService {
    
    private final List<String> reqAttributes = new ArrayList<String>();

    private final Map<String, List<String>> resAttributes = new HashMap<String, List<String>>();

    private final String realm;
    
    private final String uid;

    public String getServicePath() {
        return "/identity/read";
    }

    public boolean needAdminPermission() {
        return true;
    }

    public Read(final String realm, final String uid) {
        if (null == realm) {
            throw new IllegalArgumentException("realm is null");
        }
        if (null == uid) {
            throw new IllegalArgumentException("uid is null");
        }
        this.realm = realm;
        this.uid = uid;
    }

    public Read setAttr(final String key) {
        reqAttributes.add(key);
        return this;
    }

    public List<String[]> getRequestParameters() {
        final List<String[]> paramList = new ArrayList<String[]>();
        paramList.add(new String[] { RestKey.ATTR_NAMES, RestKey.REALM });
        paramList.add(new String[] { RestKey.ATTR_VALUES + RestKey.REALM, realm });
        paramList.add(new String[] { RestKey.NAME, uid });
        for (String attributeName : reqAttributes) {
            paramList.add(new String[] { RestKey.ATTR_NAMES, attributeName });
        }
        return paramList;
    }

    public void processResponse(final int statusCode, final BufferedInputStream inputStream) throws IOException {
        final List<String> lines = readContentAsLines(inputStream);
        if (HttpStatus.SC_OK == statusCode) {
            success = true;
            String attributeName = null;
            List<String> attributeValues = null;
            // Response is in weird name/value format
            // lines with names start with USERDETAILS_NAME
            // lines with values start with USERDETAILS_VALUE
            // there can be multiple VALUES per NAME
            for (final String line : lines) {
                if (line.startsWith(RestValue.IDENTITYDETAILS_NAME)) {
                    // start of new name/value pair
                    if (null != attributeName) {
                        // save PREVIOUS attributeName and values
                        resAttributes.put(attributeName, attributeValues);
                    }
                    // initiate new attributeName
                    attributeName = readValue(RestValue.IDENTITYDETAILS_NAME, line);
                    attributeValues = new ArrayList<String>();
                }
                else if (line.startsWith(RestValue.IDENTITYDETAILS_VALUE)) {
                    attributeValues.add(readValue(RestValue.IDENTITYDETAILS_VALUE, line));
                }
            }
            if (null != attributeName) {
                resAttributes.put(attributeName, attributeValues);
            }
        }
        else {
            success = false;
            if (lines.size() > 0) {
                error = getError(lines.get(0));
            }
            if (null == error) {
                logger.warn("processResponse() got statusCode: " + statusCode + ", but couldn't determinate error, response: "
                        + lines);
            }
        }
    }


    /**
     * Return the first AttributeValue for the given AttributeKey or null if none exist
     */
    public String getAttributeValue(final String key) {
        final List<String> values = resAttributes.get(key);
        return values != null && !values.isEmpty() ? values.get(0) : null;
    }
    
    /**
     * Return a List of AttributeValues for the given AttributeKey or null if none exist
     */
    public List<String> getAttributeValues(final String key) {
        return resAttributes.get(key);
    }

    /**
     * Return all AttributeValue as Map of {AttributeKey -> List of AttributeValues}
     */
    public Map<String, List<String>> getAllAttributeValues() {
        return resAttributes;
    }
}