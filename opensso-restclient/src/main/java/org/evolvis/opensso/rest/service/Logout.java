package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;

public class Logout extends AbstractRestService {

    private final String token;

    public String getServicePath() {
        return "/identity/logout";
    }

    public boolean needAdminPermission() {
        return false;
    }

    public Logout(final String token) {
        if (null == token) {
            throw new IllegalArgumentException("token is null");
        }
        this.token = token;
    }

    public List<String[]> getRequestParameters() {
        final List<String[]> paramList = new ArrayList<String[]>();
        paramList.add(new String[] { "subjectid", token });
        return paramList;
    }

    public void processResponse(final int statusCode, final BufferedInputStream inputStream) throws IOException {
        final List<String> lines = readContentAsLines(inputStream);
        if (HttpStatus.SC_OK == statusCode) {
            success = true;
            // shouldn't happen
            if (lines.size() > 0) {
                logger.warn("processResponse() got more lines as aspected: " + lines);
            }
        }
        else {
            success = false;
            if (lines.size() > 0) {
                error = getError(lines.get(0));
            }
            if (null == error) {
                logger.error("processResponse() couldn't determinate error: " + lines);
            }
        }
    }
}
