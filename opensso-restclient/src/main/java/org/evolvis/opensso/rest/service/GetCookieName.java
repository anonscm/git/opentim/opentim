package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.evolvis.opensso.rest.constant.RestValue;

public class GetCookieName extends AbstractRestService {

    private String tokenCookieName;

    public String getServicePath() {
        return "/identity/getCookieNameForToken";
    }

    public boolean needAdminPermission() {
        return false;
    }

    public List<String[]> getRequestParameters() {
        return Collections.emptyList();
    }

    public void processResponse(final int statusCode, final BufferedInputStream inputStream) throws IOException {
        final List<String> lines = readContentAsLines(inputStream);
        if ((HttpStatus.SC_OK == statusCode) && (lines.size() != 0)) {
            success = true;
            for (final String line : lines) {
                if (line.startsWith(RestValue.STRING)) {
                    tokenCookieName = line.substring(RestValue.STRING.length());
                    break;
                }
            }
        }
        else {
            success = false;
            if (lines.size() > 0) {
                error = getError(lines.get(0));
            }
            if (null == error) {
                logger.error("processResponse() couldn't determinate error: " + lines);
            }
        }
    }

    public String getTokenCookieName() {
        return tokenCookieName;
    }
}
