package org.evolvis.opensso.rest.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * SSHA creates salted hashes to store OpenSSO user passwords.
 * 
 * @author Timo Pick
 *
 */
public class SSHA {

	protected final static Log logger = LogFactory.getLog(SSHA.class);

	/**
	 * Creates a <b>randomly</b> salted hash (SSHA token) that has the following structure:
	 * 
	 * {SSHA} + Base64Encode( SHA1(password + salt) + salt )
	 * 
	 * <li>the "{SSHA}" tag (cleartext)
	 * <li>SHA-1 hash of cleartext password and salt (hash)
	 * <li>salt (cleartext): It must be re-used to initialize the SHA-1 algorithm when a password is compared to the hash.
	 * <li>hash and salt are then Base64 encoded
	 * @param password
	 * @return 
	 */
	public static String hash(String password) {
		if (password == null) {
			logger.warn("Password was null. Returning null.");
			return null;
		}
		return hash(password.getBytes());
	}
	
	/**
	 * Creates a <b>randomly</b> salted hash (SSHA token) that has the following structure:
	 * 
	 * {SSHA} + Base64Encode( SHA1(password + salt) + salt )
	 * 
	 * <li>the "{SSHA}" tag (cleartext)
	 * <li>SHA-1 hash of cleartext password and salt (hash)
	 * <li>salt (cleartext): It must be re-used to initialize the SHA-1 algorithm when a password is compared to the hash.
	 * <li>hash and salt are then Base64 encoded
	 * @param password
	 * @return 
	 */
	public static String hash(byte[] password) {
		if (password == null) {
			logger.warn("Password was null. Returning null.");
			return null;
		}
		
		// Create 64 bits of salt
		byte[] salt = new byte[8];
		SecureRandom random = new SecureRandom();
		random.nextBytes(salt);
		return hash(password, salt);
	}

	/**
	 * Creates a salted hash (SSHA token) that has the following structure:
	 * 
	 * {SSHA} + Base64Encode( SHA1(password + salt) + salt )
	 * 
	 * <li>the "{SSHA}" tag (cleartext)
	 * <li>SHA-1 hash of cleartext password and salt (hash)
	 * <li>salt (cleartext): It must be re-used to initialize the SHA-1 algorithm when a password is compared to the hash.
	 * <li>hash and salt are then Base64 encoded
	 * @param password
	 * @param salt initialize the SHA-1 algorithm with this salt.
	 * @return 
	 */
	public static String hash(byte[] password, byte[] salt) {
		if (password == null) {
			logger.warn("Password was null. Returning null.");
			return null;
		}
		
		if (salt == null) {
			logger.warn("Salt was null. Returning null.");
			return null;
		}
		
		try {
			// Append salt to cleartext password
			byte[] passwordAndSalt = new byte[password.length + salt.length];
			System.arraycopy(password, 0, passwordAndSalt, 0, password.length);
			System.arraycopy(salt, 0, passwordAndSalt, password.length,
					salt.length);

			// Hash password+salt with salt
			MessageDigest digest = MessageDigest.getInstance("SHA-1");
			byte[] hashedPasswordAndSalt = digest.digest(passwordAndSalt);

			// Append salt to hashed password+salt
			byte[] digestAndSalt = new byte[hashedPasswordAndSalt.length
					+ salt.length];
			System.arraycopy(hashedPasswordAndSalt, 0, digestAndSalt, 0,
					hashedPasswordAndSalt.length);
			System.arraycopy(salt, 0, digestAndSalt,
					hashedPasswordAndSalt.length, salt.length);

			String result = "{SSHA}" + new String(Base64.encodeBase64(digestAndSalt));
			return result;
		} catch (Exception e) {
			logger.error("Could not hash password.", e);
			throw new RuntimeException("Could not hash password.", e);
		}
	}

	/**
	 * Compare a cleartext password with a SSHA token
	 * 
	 * @param sshaString
	 * @param password
	 * @return
	 */
	public static boolean compare(String sshaString, String password) {
		// Base64decode
		byte[] decodedHash1 = Base64.decodeBase64(sshaString.getBytes());
		// Save salt
		byte[] salt = new byte[8];
		System.arraycopy(decodedHash1, decodedHash1.length - 8, salt, 0, 8);
		// Use this salt to hash new password
		String hashedPw = hash(password.getBytes(), salt);
		// Compare
		return Arrays.equals(sshaString.getBytes(), hashedPw.getBytes());
	}
	
	public static void main(String[] args) {
		try {
			String hash1 = hash("passwort".getBytes("UTF-8"));
			System.out.println(hash1);
			System.out.println(compare(hash1, "passwort"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
