package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.evolvis.opensso.rest.constant.RestKey;
import org.evolvis.opensso.rest.constant.UserAttribute;
import org.evolvis.opensso.rest.util.SSHA;

/**
 * @author Tino Rink
 */
public class CreateUser extends AbstractRestService {

    private String realm;

    private String userid;

    private String firstname;

    private String lastname;

    private String passwd;

    private String email;

    private String userStatus;

    private String userUUID;

    public String getServicePath() {
        return "/identity/create";
    }

    public boolean needAdminPermission() {
        return true;
    }

    public CreateUser(final String realm, final String userid, final String passwd, final String firstname, final String surname,
            final String email, final String userStatus) {
        init(realm, userid, passwd, firstname, surname, email, userStatus, null);
    }

    public CreateUser(final String realm, final String userid, final String passwd, final String firstname, final String surname,
            final String email, final String userStatus, final String userUUID) {
        init(realm, userid, passwd, firstname, surname, email, userStatus, userUUID);
    }

    private void init(final String realm, final String userid, final String passwd, final String firstname,
            final String lastname, final String email, final String userStatus, final String userUUID) {
        if (null == realm) {
            throw new IllegalArgumentException("realm is null");
        }
        if (null == userid) {
            throw new IllegalArgumentException("userid is null");
        }
        if (null == passwd) {
            throw new IllegalArgumentException("passwd is null");
        }
        if (null == firstname) {
            throw new IllegalArgumentException("firstname is null");
        }
        if (null == lastname) {
            throw new IllegalArgumentException("lastname is null");
        }
        if (null == email) {
            throw new IllegalArgumentException("email is null");
        }
        if (null == userStatus) {
            throw new IllegalArgumentException("userStatus is null");
        }
        this.realm = realm;
        this.userid = userid;
		this.passwd = SSHA.hash(passwd); // Hash password
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.userStatus = userStatus;
        this.userUUID = userUUID;
    }

    public List<String[]> getRequestParameters() {

        final List<String[]> paramList = new ArrayList<String[]>();
        
        paramList.add(new String[] { RestKey.ID_REALM, realm });
        paramList.add(new String[] { RestKey.ID_TYPE, "user" });
        paramList.add(new String[] { RestKey.ID_NAME, userid });
        paramList.add(new String[] { RestKey.ID_ATTR_NAMES, UserAttribute.PASSWORD });
        paramList.add(new String[] { RestKey.ID_ATTR_VALUES + UserAttribute.PASSWORD, passwd });
        paramList.add(new String[] { RestKey.ID_ATTR_NAMES, UserAttribute.FIRSTNAME });
        paramList.add(new String[] { RestKey.ID_ATTR_VALUES + UserAttribute.FIRSTNAME, firstname });
        paramList.add(new String[] { RestKey.ID_ATTR_NAMES, UserAttribute.LASTNAME });
        paramList.add(new String[] { RestKey.ID_ATTR_VALUES + UserAttribute.LASTNAME, lastname });
        paramList.add(new String[] { RestKey.ID_ATTR_NAMES, UserAttribute.FULLNAME });
        paramList.add(new String[] { RestKey.ID_ATTR_VALUES + UserAttribute.FULLNAME, firstname + " " + lastname });
        paramList.add(new String[] { RestKey.ID_ATTR_NAMES, UserAttribute.EMAIL });
        paramList.add(new String[] { RestKey.ID_ATTR_VALUES + UserAttribute.EMAIL, email });
        paramList.add(new String[] { RestKey.ID_ATTR_NAMES, UserAttribute.USERSTATUS });
        paramList.add(new String[] { RestKey.ID_ATTR_VALUES + UserAttribute.USERSTATUS, userStatus });

        if (null != userUUID) {
            paramList.add(new String[] { RestKey.ID_ATTR_NAMES, UserAttribute.USER_UUID });
            paramList.add(new String[] { RestKey.ID_ATTR_VALUES + UserAttribute.USER_UUID, userUUID });
        }

        return paramList;
    }

    public void processResponse(final int statusCode, final BufferedInputStream inputStream) throws IOException {
        final List<String> lines = readContentAsLines(inputStream);
        if (HttpStatus.SC_OK == statusCode) {
            success = true;
            if (lines.size() > 0) {
                logger.warn("processResponse() got more lines as aspected: " + lines);
            }
        }
        else {
            success = false;
            if (lines.size() > 0) {
                error = getError(lines.get(0));
            }
            if (null == error) {
                logger.error("processResponse() couldn't determinate error: " + lines);
            }
        }
    }
}
