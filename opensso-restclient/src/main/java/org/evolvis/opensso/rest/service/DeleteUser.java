package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.evolvis.opensso.rest.constant.RestKey;

/**
 * @author Tino Rink
 */
public class DeleteUser extends AbstractRestService {

    private final String realm;

    private final String userid;

    public String getServicePath() {
        return "/identity/delete";
    }

    public boolean needAdminPermission() {
        return true;
    }

    public DeleteUser(final String realm, final String userid) {
        if (null == realm) {
            throw new IllegalArgumentException("realm is null");
        }
        if (null == userid) {
            throw new IllegalArgumentException("userid is null");
        }
        this.realm = realm;
        this.userid = userid;
    }

    public List<String[]> getRequestParameters() {
        final List<String[]> paramList = new ArrayList<String[]>();
        paramList.add(new String[] { RestKey.ID_REALM, realm });
        paramList.add(new String[] { RestKey.ID_TYPE, "user" });
        paramList.add(new String[] { RestKey.ID_NAME, userid });
        return paramList;
    }

    public void processResponse(final int statusCode, final BufferedInputStream inputStream) throws IOException {
        final List<String> lines = readContentAsLines(inputStream);
        if (HttpStatus.SC_OK == statusCode) {
            success = true;
            if (lines.size() > 0) {
                logger.warn("processResponse() got more lines as aspected: " + lines);
            }
        }
        else {
            success = false;
            if (lines.size() > 0) {
                error = getError(lines.get(0));
            }
            if (null == error) {
                logger.error("processResponse() couldn't determinate error: " + lines);
            }
        }
    }
}
