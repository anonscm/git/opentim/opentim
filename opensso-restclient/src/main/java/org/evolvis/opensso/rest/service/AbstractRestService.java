package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Tino Rink
 */
public abstract class AbstractRestService implements RestService {

    private static final String errorPrefix = "com.sun.identity.idsvcs.";

    private static final String ldapPrefix = "ldap exception.  ";

    protected final Log logger = LogFactory.getLog(getClass());

    protected Boolean success;

    protected String error;

    public boolean success() {
        return success;
    }

    public String getError() {
        return error;
    }

    /**
     * Produce a list of the lines in inStream
     * @param inStream
     * @return a List of the inStream's lines
     * @throws IOException
     */
    protected static List<String> readContentAsLines(final BufferedInputStream inStream) throws IOException {
        final List<String> lines = new ArrayList<String>();
        if (null != inStream) {
            final BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
            String line = null;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        }
        return lines;
    }

    /**
     * Parse out the value of a valueString of the form "valueType=value"
     * @param valueType the 'type=' definition
     * @param valueString the 'name=value' pair
     * @return the value part
     */
    protected static String readValue(final String valueType, final String valueString) {
        if (null == valueType) {
            throw new IllegalArgumentException("valueType is null");
        }
        if (null == valueString) {
            throw new IllegalArgumentException("valueString is null");
        }
        if (valueString.startsWith(valueType)) {
            return valueString.substring(valueType.length());
        }
        return null;
    }

    /**
     * Parse out error message from HTTP reply
     * @param msg The message string
     * @return the actual error message
     */
    protected static String getError(final String msg) {

        // check for error
        final int errorStartIdx = msg.indexOf(errorPrefix);
        if (errorStartIdx == -1) {
            return null;
        }

        // We consider the first whitespace after out prefix the end of the error
        final int errorEndIdx = msg.indexOf(" ", errorStartIdx + errorPrefix.length());

        // check for ldap error
        final int ldapStartIdx = msg.indexOf(ldapPrefix, errorEndIdx);
        if (ldapStartIdx > 0) {
            final int ldapEndIdx = msg.indexOf(":", ldapStartIdx + ldapPrefix.length());
            return msg.substring(ldapStartIdx + ldapPrefix.length(), ldapEndIdx);
        }
        return msg.substring(errorStartIdx + errorPrefix.length(), errorEndIdx);
    }
}
