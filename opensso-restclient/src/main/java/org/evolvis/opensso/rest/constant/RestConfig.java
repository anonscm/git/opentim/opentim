package org.evolvis.opensso.rest.constant;

import org.evolvis.config.Configuration;
import org.evolvis.config.PropertyConfig;

public class RestConfig {

    private static final Configuration config = PropertyConfig.getInstance();

    public static final String SERVICE_URL = config.get(PropSSOKeys.TARENT_SSO_SERVICE_URL);

    public static final String SERVICE_LOGIN = config.get(PropSSOKeys.TARENT_SSO_SERVICE_LOGIN);

    public static final String SERVICE_PASSWD = config.get(PropSSOKeys.TARENT_SSO_SERVICE_PASSWD);

    public static final Integer MAX_CONNECTIONS = Integer.valueOf(config.get(PropRestKeys.TARENT_SSO_MAX_CONNECTIONS, "100"));

    public static final Integer MAX_HOST_CONNECTIONS = Integer.valueOf(config.get(PropRestKeys.TARENT_SSO_MAX_HOST_CONNECTIONS,
            "100"));
    
    public static final String PROXY_HOST = config.get(PropRestKeys.TARENT_SSO_PROXY_HOST);
    
    public static final Integer PROXY_PORT = Integer.valueOf(config.get(PropRestKeys.TARENT_SSO_PROXY_PORT, "-1"));
}
