package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.httpclient.HttpStatus;
import org.evolvis.opensso.rest.constant.RestKey;
import org.evolvis.opensso.rest.constant.UserAttribute;
import org.evolvis.opensso.rest.util.SSHA;

/**
 * @author Tino Rink
 */
public class UpdateUser extends AbstractRestService {

    private final Map<String, String> attributeMap = new TreeMap<String, String>();

    private String realm = null;

    private String userid = null;

    public String getServicePath() {
        return "/identity/update";
    }

    public boolean needAdminPermission() {
        return true;
    }

    public UpdateUser(final String realm, final String userid) {
        if (null == realm) {
            throw new IllegalArgumentException("realm is null");
        }
        if (null == userid) {
            throw new IllegalArgumentException("userid is null");
        }
        this.realm = realm;
        this.userid = userid;
    }

    /* FIXME: UpdateUser allows the updating of userUUID. That must not be possible! (<f.thiel@tarent.de>)
     * FIXME: Updating the userUUID should be configurable (<t.rink@tarent.de>)
     */
    public UpdateUser(final String realm, final String userid, final Map<String, String> attributeMap) {
        if (null == realm) {
            throw new IllegalArgumentException("realm is null");
        }
        if (null == userid) {
            throw new IllegalArgumentException("userid is null");
        }
        if (null == attributeMap) {
            throw new IllegalArgumentException("attributeMap is null");
        }
        this.realm = realm;
        this.userid = userid;
        
        // User changed password? Hash it.
        String passwd = attributeMap.get(UserAttribute.PASSWORD);
        if (passwd != null) {
        	attributeMap.put(UserAttribute.PASSWORD, SSHA.hash(passwd));
        }
        
        this.attributeMap.putAll(attributeMap);
    }

    public UpdateUser setAttr(final String key, final String value) {
    	if (UserAttribute.PASSWORD.equals(key)) {
            attributeMap.put(key, SSHA.hash(value));
    	} else {
            attributeMap.put(key, value);
    	}
        return this;
    }

    public List<String[]> getRequestParameters() {
        final List<String[]> paramList = new ArrayList<String[]>();
        paramList.add(new String[] { RestKey.ID_REALM, realm });
        paramList.add(new String[] { RestKey.ID_NAME, userid });
        for (final Entry<String, String> entry : attributeMap.entrySet()) {
            paramList.add(new String[] { RestKey.ID_ATTR_NAMES, entry.getKey() });
            paramList.add(new String[] { RestKey.ID_ATTR_VALUES + entry.getKey(), entry.getValue() });
        }
        return paramList;
    }

    public void processResponse(final int statusCode, final BufferedInputStream inputStream) throws IOException {
        final List<String> lines = readContentAsLines(inputStream);
        if (HttpStatus.SC_OK == statusCode) {
            success = true;
            if (lines.size() > 0) {
                logger.warn("processResponse() got statusCode: " + statusCode + ", response: " + lines);
            }
        }
        else {
            success = false;
            if (lines.size() > 0) {
                error = getError(lines.get(0));
            }
            if ((lines.size() > 0) && (null == error)) {
                logger.warn("processResponse() got statusCode: " + statusCode + ", response: " + lines);
            }
        }
    }
}
