package org.evolvis.opensso.rest.constant;

public class RestKey {

    public static final String ADMIN_TOKEN_ID = "admin";

    public static final String ATTR_NAMES = "attributes_names";

    public static final String ATTR_VALUES = "attributes_values_";

    public static final String FILTER = "filter";

    public static final String ID_REALM = "identity_realm";

    public static final String ID_TYPE = "identity_type";

    public static final String ID_NAME = "identity_name";

    public static final String ID_ATTR_NAMES = "identity_attribute_names";

    public static final String ID_ATTR_VALUES = "identity_attribute_values_";

    public static final String REALM = "realm";

    public static final String NAME = "name";

}
