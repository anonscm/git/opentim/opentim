package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.List;

/**
 * @author Tino Rink
 */
public interface RestService {

    /**
     * Get the URL relative to the OpenSSO location for this REST service
     * @return relative URL string
     */
    public String getServicePath();

    /**
     * Does this service need admin permissions?
     * @return
     */
    public boolean needAdminPermission();

    /**
     * Get the name/value pairs for the HTTP query parameters for this service
     * @return list of name/value pairs
     */
    public List<String[]> getRequestParameters();

    /**
     * Procedure to process the REST response
     * @param statusCode the HTTP statusCode
     * @param inputStream the inputStream returned by the REST service
     * @throws IOException
     */
    public void processResponse(final int statusCode, final BufferedInputStream inputStream) throws IOException;

    /**
     * @return success of service call
     */
    public boolean success();

    /**
     * @return the error
     */
    public String getError();
}