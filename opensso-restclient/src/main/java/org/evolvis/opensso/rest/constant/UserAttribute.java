package org.evolvis.opensso.rest.constant;

import org.evolvis.config.Configuration;
import org.evolvis.config.PropertyConfig;

public class UserAttribute {

    private static final Configuration config = PropertyConfig.getInstance();

    public static final String UID = config.get(PropSSOKeys.TARENT_SSO_ATTR_UID, "uid");

    public static final String PASSWORD = config.get(PropSSOKeys.TARENT_SSO_ATTR_PASSWORD, "userpassword");

    public static final String FIRSTNAME = config.get(PropSSOKeys.TARENT_SSO_ATTR_FIRSTNAME, "mail");

    public static final String LASTNAME = config.get(PropSSOKeys.TARENT_SSO_ATTR_LASTNAME, "mail");

    public static final String FULLNAME = config.get(PropSSOKeys.TARENT_SSO_ATTR_FULLNAME, "sn");

    public static final String EMAIL = config.get(PropSSOKeys.TARENT_SSO_ATTR_EMAIL, "cn");

    public static final String USERSTATUS = config.get(PropSSOKeys.TARENT_SSO_ATTR_USERSTATUS, "inetuserstatus");

    public static final String USER_UUID = config.get(PropSSOKeys.TARENT_SSO_ATTR_USER_UUID, "useruuid");

    public static final String PASSWORD_UUID = config.get(PropSSOKeys.TARENT_SSO_ATTR_PASSWORD_UUID, "passworduuid");

    public static final String REGISTER_UUID = config.get(PropSSOKeys.TARENT_SSO_ATTR_REGISTER_UUID, "registeruuid");
}
