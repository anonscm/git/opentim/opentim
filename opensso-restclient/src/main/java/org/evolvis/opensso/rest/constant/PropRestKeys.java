package org.evolvis.opensso.rest.constant;

public class PropRestKeys {

    public static final String TARENT_SSO_MAX_CONNECTIONS = "tarent.sso.max.connections";

    public static final String TARENT_SSO_MAX_HOST_CONNECTIONS = "tarent.sso.max.host.connections";

    public static final String TARENT_SSO_PROXY_HOST = "tarent.sso.proxy.host";
    
    public static final String TARENT_SSO_PROXY_PORT = "tarent.sso.proxy.port";
}
