package org.evolvis.opensso.rest.constant;

public class UserStatus {

    public static final String ACTIVE = "Active";

    public static final String INACTIVE = "Inactive";
}
