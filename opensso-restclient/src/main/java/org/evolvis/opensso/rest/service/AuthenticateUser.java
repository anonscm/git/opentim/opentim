package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.evolvis.opensso.rest.constant.RestValue;

/**
 * @author Tino Rink
 */
public class AuthenticateUser extends AbstractRestService {

    private final String realm;

    private final String username;

    private final String password;

    private String token;

    public String getServicePath() {
        return "/identity/authenticate";
    }

    public boolean needAdminPermission() {
        return false;
    }

    public AuthenticateUser(final String username, final String password) {
        if (null == username) {
            throw new IllegalArgumentException("username is null");
        }
        if (null == password) {
            throw new IllegalArgumentException("password is null");
        }
        this.realm = "/";
        this.username = username;
        this.password = password;
    }

    public AuthenticateUser(final String realm, final String username, final String password) {
        if (null == realm) {
            throw new IllegalArgumentException("realm is null");
        }
        if (null == username) {
            throw new IllegalArgumentException("username is null");
        }
        if (null == password) {
            throw new IllegalArgumentException("password is null");
        }
        this.realm = realm;
        this.username = username;
        this.password = password;
    }

    public List<String[]> getRequestParameters() {
        final List<String[]> paramList = new ArrayList<String[]>();
        paramList.add(new String[] { "realm", realm });
        paramList.add(new String[] { "username", username });
        paramList.add(new String[] { "password", password });
        return paramList;
    }

    public void processResponse(final int statusCode, final BufferedInputStream inputStream) throws IOException {
        final List<String> lines = AbstractRestService.readContentAsLines(inputStream);
        if (HttpStatus.SC_OK == statusCode) {
            if (lines.size() == 1) {
                success = true;
                token = readValue(RestValue.TOKEN_ID, lines.get(0));
            }
            else {
                success = false;
                logger.warn("processResponse() couldn't read token, lines: " + lines);
            }
        }
        else {
            success = false;
            error = getError(lines.get(0));
            if (null == error) {
                logger.warn("processResponse() couldn't determinate error, lines: " + lines);
            }
        }
    }

    public String getToken() {
        return token;
    }
}
