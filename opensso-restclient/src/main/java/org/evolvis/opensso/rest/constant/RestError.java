package org.evolvis.opensso.rest.constant;

public class RestError {

    public static final String GENERAL_FAILURE = "GeneralFailure";

    public static final String INVALID_CREDENTIALS = "InvalidCredentials";

    public static final String INVALID_PASSWORD = "InvalidPassword";

    public static final String USER_NOT_FOUND = "UserNotFound";

    public static final String TOKEN_EXPIRED = "TokenExpired";

    /**
     * @see http://docs.sun.com/source/816-5618-10/netscape/ldap/LDAPException.html#ENTRY_ALREADY_EXISTS
     */
    public static final String LDAP_ENTRY_ALREADY_EXISTS = "LDAP Error 68";
}