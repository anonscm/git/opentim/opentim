package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpStatus;
import org.evolvis.opensso.rest.constant.RestValue;

public class GetAttributes extends AbstractRestService {

    private final Map<String, List<String>> attributes = new HashMap<String, List<String>>();

    private final String token;

    public String getServicePath() {
        return "/identity/attributes";
    }

    public boolean needAdminPermission() {
        return false;
    }

    public GetAttributes(final String token) {
        if (null == token) {
            throw new IllegalArgumentException("token is null");
        }
        this.token = token;
    }

    public List<String[]> getRequestParameters() {
        final List<String[]> paramList = new ArrayList<String[]>();
        paramList.add(new String[] { "subjectid", token });
        return paramList;
    }

    public void processResponse(final int statusCode, final BufferedInputStream inputStream) throws IOException {
        final List<String> lines = readContentAsLines(inputStream);
        if (HttpStatus.SC_OK == statusCode) {
            success = true;
            String attributeName = null;
            List<String> attributeValues = null;
            // Response is in weird name/value format
            // lines with names start with USERDETAILS_NAME
            // lines with values start with USERDETAILS_VALUE
            // there can be multiple VALUES per NAME
            for (final String line : lines) {
                if (line.startsWith(RestValue.USERDETAILS_NAME)) {
                    // start of new name/value pair
                    if (null != attributeName) {
                        // save PREVIOUS attributeName and values
                        attributes.put(attributeName, attributeValues);
                    }
                    // initiate new attributeName
                    attributeName = readValue(RestValue.USERDETAILS_NAME, line);
                    attributeValues = new ArrayList<String>();
                }
                else if (line.startsWith(RestValue.USERDETAILS_VALUE)) {
                    attributeValues.add(readValue(RestValue.USERDETAILS_VALUE, line));
                }
            }
            if (null != attributeName) {
                attributes.put(attributeName, attributeValues);
            }
        }
        else {
            success = false;
            if (lines.size() > 0) {
                error = getError(lines.get(0));
            }
            if (null == error) {
                logger.warn("processResponse() got statusCode: " + statusCode + ", but couldn't determinate error, response: "
                        + lines);
            }
        }
    }

    public List<String> attribute(final String key) {
        return attributes.get(key);
    }

    public Map<String, List<String>> attributes() {
        return attributes;
    }
}