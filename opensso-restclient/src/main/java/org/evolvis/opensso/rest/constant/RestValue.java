package org.evolvis.opensso.rest.constant;

public class RestValue {

    public static final String BOOLEAN = "boolean=";

    public static final String STRING = "string=";

    public static final String TOKEN_ID = "token.id=";

    public static final String USERDETAILS_NAME = "userdetails.attribute.name=";

    public static final String USERDETAILS_VALUE = "userdetails.attribute.value=";

    public static final String IDENTITYDETAILS_NAME = "identitydetails.attribute.name=";

    public static final String IDENTITYDETAILS_VALUE = "identitydetails.attribute.value=";
    
}
