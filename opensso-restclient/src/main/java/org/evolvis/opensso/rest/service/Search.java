package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import org.apache.commons.httpclient.HttpStatus;
import org.evolvis.opensso.rest.constant.RestKey;
import org.evolvis.opensso.rest.constant.RestValue;

/**
 * @author Tino Rink
 */
public class Search extends AbstractRestService {

    private final Map<String, String> attributeMap = new TreeMap<String, String>();

    private String realm = null;

    private List<String> uids = null;

    public String getServicePath() {
        return "/identity/search";
    }

    public boolean needAdminPermission() {
        return true;
    }

    public Search(final String realm) {
        if (null == realm) {
            throw new IllegalArgumentException("realm is null");
        }
        this.realm = realm;
    }

    public Search(final String realm, final Map<String, String> attributeMap) {
        if (null == realm) {
            throw new IllegalArgumentException("realm is null");
        }
        if (null == attributeMap) {
            throw new IllegalArgumentException("attributeMap is null");
        }
        this.realm = realm;
        this.attributeMap.putAll(attributeMap);
    }

    public Search setAttr(final String key, final String value) {
        attributeMap.put(key, value);
        return this;
    }

    public List<String[]> getRequestParameters() {
        final List<String[]> paramList = new ArrayList<String[]>();
        paramList.add(new String[] { RestKey.ATTR_NAMES, RestKey.REALM });
        paramList.add(new String[] { RestKey.ATTR_VALUES + RestKey.REALM, realm });
        for (final Entry<String, String> entry : attributeMap.entrySet()) {
            paramList.add(new String[] { RestKey.ATTR_NAMES, entry.getKey() });
            paramList.add(new String[] { RestKey.ATTR_VALUES + entry.getKey(), entry.getValue() });
        }
        return paramList;
    }

    public void processResponse(final int statusCode, final BufferedInputStream inputStream) throws IOException {
        final List<String> lines = readContentAsLines(inputStream);
        if (HttpStatus.SC_OK == statusCode) {
            success = true;
            if (lines.size() > 0) {
                uids = new ArrayList<String>(lines.size());
                for (final String line : lines) {
                    final String value = readValue(RestValue.STRING, line);
                    if (null != value) {
                        uids.add(value);
                    }
                }
            }
            else {
                uids = Collections.emptyList();
            }
        }
        else {
            success = false;
            if (lines.size() > 0) {
                error = getError(lines.get(0));
            }
            if ((lines.size() > 0) && (null == error)) {
                logger.warn("processResponse() got statusCode: " + statusCode + ", response: " + lines);
            }
        }
    }

    /**
     * return the first found uid or null
     */
    public String getUid() {
        return uids.size() > 0 ? uids.get(0) : null;
    }

    /**
     * return the found uids
     */
    public List<String> getUids() {
        return uids;
    }
}
