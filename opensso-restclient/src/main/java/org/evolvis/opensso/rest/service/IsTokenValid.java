package org.evolvis.opensso.rest.service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpStatus;
import org.evolvis.opensso.rest.constant.RestValue;

public class IsTokenValid extends AbstractRestService {

    private final String token;

    private boolean isValid;

    public String getServicePath() {
        return "/identity/isTokenValid";
    }

    public boolean needAdminPermission() {
        return false;
    }

    public IsTokenValid(final String token) {
        if (null == token) {
            throw new IllegalArgumentException("token is null");
        }
        this.token = token;
    }

    public List<String[]> getRequestParameters() {
        final List<String[]> paramList = new ArrayList<String[]>();
        paramList.add(new String[] { "tokenid", token });
        return paramList;
    }

    public void processResponse(final int statusCode, final BufferedInputStream inputStream) throws IOException {
        final List<String> lines = readContentAsLines(inputStream);
        if ((HttpStatus.SC_OK == statusCode) && (lines.size() == 1)) {
            success = true;
            isValid = Boolean.valueOf(readValue(RestValue.BOOLEAN, lines.get(0)));
        }
        else {
            success = false;
            isValid = false;
            if (lines.size() > 0) {
                error = getError(lines.get(0));
            }
            if (null == error) {
                logger.warn("processResponse() got statusCode: " + statusCode + ", but couldn't determinate error, response: "
                        + lines);
            }
        }
    }

    public boolean valid() {
        return isValid;
    }
}
