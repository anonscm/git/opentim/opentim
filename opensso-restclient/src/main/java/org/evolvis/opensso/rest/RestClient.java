package org.evolvis.opensso.rest;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.ProxyHost;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.PostMethod;
import org.evolvis.opensso.rest.constant.RestConfig;
import org.evolvis.opensso.rest.constant.RestKey;
import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.evolvis.opensso.rest.service.IsTokenValid;
import org.evolvis.opensso.rest.service.RestService;

/**
 * @author Tino Rink
 */
public class RestClient {

    private static final MultiThreadedHttpConnectionManager connectionManager;
    static {
        connectionManager = new MultiThreadedHttpConnectionManager();
        connectionManager.getParams().setMaxTotalConnections(RestConfig.MAX_CONNECTIONS);
        connectionManager.getParams().setDefaultMaxConnectionsPerHost(RestConfig.MAX_HOST_CONNECTIONS);
    }

    private static final HttpClient httpClient = new HttpClient(connectionManager);

    private String baseUrl;

    private String adminLogin;

    private String adminPasswd;

    private String adminToken;

    protected RestClient() {
    }

    public static RestClient getInstance() {
        return new RestClient();
    }

    /**
     * Initialize for User-Services only
     */
    synchronized public RestClient initialize(final String baseUrl) {
        return initialize(baseUrl, null);
    }
    
    /**
     * Initialize for User-Services only
     */
    synchronized public RestClient initialize(final String baseUrl, ProxyHost proxyHost) {
        if (null == baseUrl) {
            throw new IllegalArgumentException("baseUrl is null");
        }
        this.baseUrl = baseUrl;
        httpClient.getHostConfiguration().setProxyHost(proxyHost);
        return this;
    }
    
    /**
     * Initialize for User- & Admin-Services
     */
    synchronized public RestClient initialize(final String baseUrl, final String adminLogin, final String adminPasswd) {
        return initialize(baseUrl, adminLogin, adminPasswd, null);
    }

    /**
     * Initialize for User- & Admin-Services
     */
    synchronized public RestClient initialize(final String baseUrl, final String adminLogin, final String adminPasswd, ProxyHost proxyHost) {
        if (null == baseUrl) {
            throw new IllegalArgumentException("baseUrl is null");
        }
        if (null == adminLogin) {
            throw new IllegalArgumentException("adminLogin is null");
        }
        if (null == adminPasswd) {
            throw new IllegalArgumentException("adminPasswd is null");
        }
        this.baseUrl = baseUrl;
        this.adminLogin = adminLogin;
        this.adminPasswd = adminPasswd;
        httpClient.getHostConfiguration().setProxyHost(proxyHost);
        return this;
    }

    public boolean isUserServiceInitialized() {
        return null != baseUrl;
    }

    public boolean isAdminServiceInitialized() {
        return (null != baseUrl) && (null != adminLogin) && (null != adminPasswd);
    }

    public <T extends RestService> T callService(final T restService) throws IOException {
        HttpMethod method = null;
        BufferedInputStream inputStream = null;
        try {
            if (null == restService) {
                throw new IllegalArgumentException("restService is null");
            }
            if (null == restService.getServicePath()) {
                throw new IllegalArgumentException("servicePath is null");
            }

            final ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

            for (final String[] entry : restService.getRequestParameters()) {
                nameValuePair.add(new NameValuePair(entry[0], entry[1]));
            }

            if (restService.needAdminPermission()) {
                nameValuePair.add(new NameValuePair(RestKey.ADMIN_TOKEN_ID, getAdminToken()));
            }

            method = new PostMethod(baseUrl + restService.getServicePath());
            method.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
            method.getParams().setCookiePolicy(CookiePolicy.IGNORE_COOKIES);
            method.setQueryString(nameValuePair.toArray(new NameValuePair[nameValuePair.size()]));

            httpClient.executeMethod(method);

            inputStream = new BufferedInputStream(method.getResponseBodyAsStream());

            restService.processResponse(method.getStatusCode(), inputStream);
        }
        finally {
            if (null != inputStream) {
                inputStream.close();
            }
            if (null != method) {
                method.releaseConnection();
            }
        }
        return restService;
    }

    public void setAdminToken(final String token) {
        adminToken = token;
    }

    private String getAdminToken() throws IOException {
        if ((null != adminLogin) && (null != adminPasswd)) {
            if (null == adminToken) {
                adminToken = callService(new AuthenticateUser("/", adminLogin, adminPasswd)).getToken();
            }
            else if (!callService(new IsTokenValid(adminToken)).valid()) {
                adminToken = callService(new AuthenticateUser("/", adminLogin, adminPasswd)).getToken();
            }
        }
        return adminToken;
    }
}
