/**
 * 
 */
package org.evolvis.idm.test;

import org.evolvis.idm.em.agent.service.AgentManagementService;
import org.evolvis.idm.test.service.ServiceProvider;

/**
 * @author Yorka Neumann, tarent GmbH
 *
 */
public interface AgentServiceProvider extends ServiceProvider {

	AgentManagementService getAgentManagementService();

}
