package org.evolvis.idm.test;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.em.agent.model.AbstractAgent;
import org.evolvis.idm.em.agent.service.AgentManagementService;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.test.service.ServiceProviderLocal;

public class AgentServiceProviderLocal extends ServiceProviderLocal implements AgentServiceProvider {

	AgentManagementService agentManagementService; 
	
	@Override
	public AgentManagementService getAgentManagementService() {
		if (agentManagementService == null) {
			agentManagementService = (AgentManagementService) jndiLookup("agentManagementImplLocal");
		}
		return agentManagementService;
	}

	

}
