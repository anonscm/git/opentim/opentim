//package org.evolvis.idm.test;
//
//import junit.framework.Assert;
//
//import org.evolvis.idm.em.agent.liferay.LiferayAgent;
//import org.evolvis.idm.em.agent.service.AgentManagementService;
//import org.evolvis.idm.relation.multitenancy.model.Application;
//import org.evolvis.idm.relation.multitenancy.model.Client;
//import org.evolvis.idm.test.service.AbstractBaseServiceAbstractTest;
//import org.evolvis.idm.test.AgentServiceProvider;
//import org.junit.Test;
//
//public class AgentManagementAbstractTest extends AbstractBaseServiceAbstractTest<AgentManagementService>{
//
//	AgentServiceProvider serviceProvider;
//	
//	@Test
//	public void setAgent() throws Exception{
//		Client client = this.getCurrentClient(true);
//		Application application = this.getCurrentApplication(client, true);
//		
//		LiferayAgent agent = new LiferayAgent();
//		agent.setApplication(application);
//		
//		agent = (LiferayAgent) this.getService().setAgent(client.getName(), agent);
//		
//		Assert.assertNotNull(agent);
//		Assert.assertNotNull(agent.getId());
//		
//	}
//
//	@Override
//	protected AgentManagementService getService() {
//		return getServiceProvider().getAgentManagementService();
//	}
//
//	@Override
//	protected AgentServiceProvider getServiceProvider() {
//		if (serviceProvider == null)
//			serviceProvider = new AgentServiceProviderLocal();
//		return serviceProvider;
//	}
//	
//	
//}
