package org.evolvis.idm.test;

import java.net.URL;

import org.junit.Test;

import com.liferay.client.soap.portal.service.http.GroupServiceSoap;
import com.liferay.client.soap.portal.service.http.GroupServiceSoapService;
import com.liferay.client.soap.portal.service.http.GroupServiceSoapServiceLocator;

public class LiferayAgentTest {
	
	private static final String ENDPOINT_HOST_AND_PORT = "localhost:9999";
	
	@Test
	public void test1() throws Exception {
		GroupServiceSoapService serviceLocator = new GroupServiceSoapServiceLocator();
		
		URL endpointUrl = new URL(serviceLocator.getPortal_GroupServiceAddress().replaceFirst("localhost:8080", ENDPOINT_HOST_AND_PORT));
		
		GroupServiceSoap service = serviceLocator.getPortal_GroupService(endpointUrl);
	}
}
