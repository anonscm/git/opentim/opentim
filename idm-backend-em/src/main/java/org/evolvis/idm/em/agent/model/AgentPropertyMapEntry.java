/**
 * 
 */
package org.evolvis.idm.em.agent.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractMapEntry;
import org.evolvis.idm.common.util.ConfigurationUtil;

/**
 * @author Yorka Neumann, tarent GmbH
 *
 */
@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "AgentPropertyMapEntry", uniqueConstraints = @UniqueConstraint(columnNames = {"key","map_id"}))
@XmlType(name = "AgentPropertyMapEntry", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "agentPropertyMapEntry", namespace = "http://idm.evolvis.org")
public class AgentPropertyMapEntry extends AbstractMapEntry<AgentPropertyMap> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4522126895199082065L;

	
	public AgentPropertyMapEntry(){
		// nothing
	}
	
	public AgentPropertyMapEntry(String key, String value, AgentPropertyMap map){
		super(key, value, map);
	}

	@Override
	public String getClientName() {
		// TODO Auto-generated method stub
		return null;
	}
}
