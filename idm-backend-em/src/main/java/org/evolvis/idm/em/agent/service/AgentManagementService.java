/**
 * 
 */
package org.evolvis.idm.em.agent.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.em.agent.model.AbstractAgent;

/**
 * @author Yorka Neumann, tarent GmbH
 *
 */
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "AgentManagementService", name = "AgentManagementServicePT", portName = "AgentManagementServicePort")
public interface AgentManagementService extends AgentRetrievalService {
	
	@WebMethod
	@WebResult(name = "agent")
	public AbstractAgent setAgent(@WebParam(name = "clientName") String clientName, @WebParam(name = "agent")AbstractAgent agent) throws IllegalRequestException, BackendException;
	
	@WebMethod
	public void deleteAgent(@WebParam(name = "clientName") String clientName, @WebParam(name = "agentId") Long agentId) throws IllegalRequestException, BackendException;
	

}
