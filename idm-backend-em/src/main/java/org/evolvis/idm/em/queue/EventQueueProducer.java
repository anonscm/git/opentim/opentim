package org.evolvis.idm.em.queue;

import javax.jms.*;
import javax.naming.NamingException;

public class EventQueueProducer {

	public static void main(String[] args) {
		
		EventQueueProducer queueClient = new EventQueueProducer();
		
		try {
			queueClient.sendMessage();
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	
	private void sendMessage() throws NamingException, JMSException {
		ConnectionFactory connectionFactory = ContextUtil.getConnectionFactory();
		Connection connection = connectionFactory.createConnection();
		Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
		Queue queue = (Queue)ContextUtil.getNamingContext().lookup("queue/HelloQueue");
		MessageProducer sender = session.createProducer(queue);
		for (int count = 1; count <= 10; count++) {
			TextMessage message = session.createTextMessage();
			message.setStringProperty("subject", "Test");
			message.setLongProperty("sent", System.currentTimeMillis());
			message.setText("Textnachricht Nr.: " + count);
			sender.send(message);
//			sender.
			System.out.println("Sending message "+count);

			session.commit();
		}
		connection.close();
	}
}
