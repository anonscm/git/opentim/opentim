package org.evolvis.idm.em.agent.model;

import java.util.Map;

import org.evolvis.idm.em.core.model.Event;
import org.evolvis.idm.em.core.model.EventResult;
import org.evolvis.idm.identity.account.model.Account;

public interface Agent {
	
	public EventResult processAccountEvent(Event<Account> accountEvent);
	
	public void configure(Map<String, String> configuration);
}
