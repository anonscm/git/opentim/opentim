package org.evolvis.idm.em.agent.model;

import java.util.List;
import java.util.Map;

public interface RoleCapableAgent extends Agent {

	public List<String> getRoles();
	public Map<String,String> getManualRoleMapping();
	public List<String> getUnmappedRoles();
}
