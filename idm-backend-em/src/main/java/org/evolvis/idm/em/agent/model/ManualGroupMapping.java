/**
 * 
 */
package org.evolvis.idm.em.agent.model;

import javax.persistence.Entity;

import org.evolvis.idm.relation.permission.model.Group;

/**
 * @author Yorka Neumann, tarent GmbH
 *
 */
@Entity
public class ManualGroupMapping extends ManualMapping<Group> {
	
}
