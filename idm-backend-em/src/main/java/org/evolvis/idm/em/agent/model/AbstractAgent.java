/**
 * 
 */
package org.evolvis.idm.em.agent.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.relation.multitenancy.model.Application;

/**
 * @author Yorka Neumann, tarent GmbH
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "Agent")
@XmlType(name = "AbstractAgent", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "abstractAgent", namespace = "http://idm.evolvis.org")
public abstract class AbstractAgent extends AbstractEntity {

	@ManyToOne(optional = false)
	private Application application;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "agent")
	private AgentPropertyMap agentPropertyMap;
	
	
	
//	@OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.REMOVE}, fetch = FetchType.EAGER, mappedBy = "agent" )
//	private List<ManualMapping<?>> manualMappings;
	
//	@OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.REMOVE}, fetch = FetchType.EAGER, mappedBy = "agent" )
//	private List<ManualGroupMapping> manualGroupMappings;
//	
//	@OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.REMOVE}, fetch = FetchType.EAGER, mappedBy = "agent" )
//	private List<ManualRoleMapping> manualRoleMappings;
//	
//	@OneToMany(cascade = {CascadeType.PERSIST,CascadeType.REFRESH, CascadeType.REMOVE}, fetch = FetchType.EAGER, mappedBy = "agent" )
//	private List<ManualRoleMapping> manualPrivateDataMappings;
	
	
	/**
	 * 
	 */
	public AbstractAgent() {
		// nothing
	}

	/* (non-Javadoc)
	 * @see org.evolvis.idm.common.model.AbstractEntity#getClientName()
	 */
	@Override
	public String getClientName() {
		if(this.getApplication() != null)
			return this.getApplication().getClientName();
		return null;
	}

	/**
	 * @param application the application to set
	 */
	public void setApplication(Application application) {
		this.application = application;
	}

	/**
	 * @return the application
	 */
	public Application getApplication() {
		return application;
	}

	/**
	 * @param agentPropertyMap the agentPropertyMap to set
	 */
	public void setAgentPropertyMap(AgentPropertyMap agentPropertyMap) {
		this.agentPropertyMap = agentPropertyMap;
	}

	/**
	 * @return the agentPropertyMap
	 */
	public AgentPropertyMap getAgentPropertyMap() {
		return agentPropertyMap;
	}


	
//	/**
//	 * @param manualGroupMappings the manualGroupMappings to set
//	 */
//	protected void setManualGroupMappings(List<ManualGroupMapping> manualGroupMappings) {
//		this.manualGroupMappings = manualGroupMappings;
//	}
//
//	/**
//	 * @return the manualGroupMappings
//	 */
//	protected List<ManualGroupMapping> getManualGroupMappings() {
//		return manualGroupMappings;
//	}
//
//	/**
//	 * @param manualRoleMappings the manualRoleMappings to set
//	 */
//	protected void setManualRoleMappings(List<ManualRoleMapping> manualRoleMappings) {
//		this.manualRoleMappings = manualRoleMappings;
//	}
//
//	/**
//	 * @return the manualRoleMappings
//	 */
//	protected List<ManualRoleMapping> getManualRoleMappings() {
//		return manualRoleMappings;
//	}
//
//	/**
//	 * @param manualPrivateDataMappings the manualPrivateDataMappings to set
//	 */
//	protected void setManualPrivateDataMappings(List<ManualRoleMapping> manualPrivateDataMappings) {
//		this.manualPrivateDataMappings = manualPrivateDataMappings;
//	}
//
//	/**
//	 * @return the manualPrivateDataMappings
//	 */
//	protected List<ManualRoleMapping> getManualPrivateDataMappings() {
//		return manualPrivateDataMappings;
//	}	
}
