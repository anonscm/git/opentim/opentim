package org.evolvis.idm.em.agent.service;


import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.em.agent.model.AbstractAgent;

public interface AgentRetrievalService extends AbstractBaseService {

	@WebMethod
	@WebResult(name = "allAgents")
	public QueryResult<AbstractAgent> getAllAgents() throws IllegalRequestException, BackendException;
	
	@WebMethod
	@WebResult(name = "clientAgents")
	public QueryResult<AbstractAgent> getClientAgents(@WebParam(name = "clientName")String clientName) throws IllegalRequestException, BackendException;
	
	
	@WebMethod
	@WebResult(name = "applicationAgents")
	public QueryResult<AbstractAgent> getApplicationAgents(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName) throws IllegalRequestException, BackendException;
	
	@WebMethod
	@WebResult(name = "agent")
	public AbstractAgent getAgentByClientAndAgentId(@WebParam(name = "clientName") String clientName, @WebParam(name = "agentId") Long agentId) throws BackendException, IllegalRequestException;
	
}
