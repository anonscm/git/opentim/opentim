package org.evolvis.idm.em.core.model;

public enum EventType {
	CREATE, UPDATE, DELETE
}
