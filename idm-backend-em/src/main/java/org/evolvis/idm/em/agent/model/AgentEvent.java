package org.evolvis.idm.em.agent.model;

import java.io.Serializable;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.em.core.model.Event;
import org.evolvis.idm.em.core.model.EventType;

public class AgentEvent<T extends AbstractEntity> extends Event<T> implements Serializable {

	private static final long serialVersionUID = 7060634409437948470L;
	
	private String targetEntityIdentifier;
	
	public AgentEvent(Event<T> event) {
		super(event.getEntity(), event.getEventType());
	}
	
	public AgentEvent(T entity, EventType eventType) {
		super(entity, eventType);
	}
	
	public String getTargetEntityIdentifier() {
		return targetEntityIdentifier;
	}
	
	public void setTargetEntityIdentifier(String targetEntityIdentifier) {
		this.targetEntityIdentifier = targetEntityIdentifier;
	}
	
	public long getTargetEntityIdentitierAsLong() {
		return Long.valueOf(getTargetEntityIdentifier());
	}
}
