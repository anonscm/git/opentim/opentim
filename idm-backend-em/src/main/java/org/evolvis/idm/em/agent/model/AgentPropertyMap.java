/**
 * 
 */
package org.evolvis.idm.em.agent.model;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractMap;
import org.evolvis.idm.common.util.ConfigurationUtil;

/**
 * @author Yorka Neumann, tarent GmbH
 *
 */


@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "AgentPropertyMap", uniqueConstraints = @UniqueConstraint(columnNames = { "agent_id" }))
@XmlType(name = "AgentPropertyMap", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "agentPropertyMap", namespace = "http://idm.evolvis.org")
public class AgentPropertyMap extends AbstractMap<AgentPropertyMapEntry> {

	@OneToOne(optional = false, fetch = FetchType.EAGER)
	private AbstractAgent agent;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "map", fetch = FetchType.EAGER)
	private List<AgentPropertyMapEntry> mapEntries;
	
	
	@Override
	protected AgentPropertyMapEntry createMapEntryInstance(String key, String value) {
		return new AgentPropertyMapEntry(key, value, this);
	}

	@Override
	public List<AgentPropertyMapEntry> getMapEntries() {
		if (this.mapEntries == null)
			mapEntries = new LinkedList<AgentPropertyMapEntry>();
		return this.mapEntries;
	}

	@Override
	public void setMapEntries(List<AgentPropertyMapEntry> mapEntries) {
		this.mapEntries = mapEntries;
		
	}

	/**
	 * @param agent the agent to set
	 */
	public void setAgent(AbstractAgent agent) {
		this.agent = agent;
	}

	/**
	 * @return the agent
	 */
	public AbstractAgent getAgent() {
		return agent;
	}
	
	@Override
	public String getClientName() {
		// TODO Auto-generated method stub
		return null;
	}



}
