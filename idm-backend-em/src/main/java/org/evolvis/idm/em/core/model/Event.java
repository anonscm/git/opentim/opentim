package org.evolvis.idm.em.core.model;

import java.io.Serializable;

import org.evolvis.idm.common.model.AbstractEntity;

public class Event<T extends AbstractEntity> implements Serializable {
	
	private T entity;
	
	private EventType eventType;
	
	public Event() {
	}
	
	public Event(T entity, EventType eventType) {
		this.entity = entity;
		this.eventType = eventType;
	}

	public T getEntity() {
		return entity;
	}

	public void setEntity(T entity) {
		this.entity = entity;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

}
