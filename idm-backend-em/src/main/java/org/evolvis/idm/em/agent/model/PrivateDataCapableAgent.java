package org.evolvis.idm.em.agent.model;

import java.util.List;
import java.util.Map;

public interface PrivateDataCapableAgent {

	public List<String> getPrivateData();
	public List<String> getUnmappedPrivateData();
	public Map<String,String> getManualPrivateDataMapping();
}
