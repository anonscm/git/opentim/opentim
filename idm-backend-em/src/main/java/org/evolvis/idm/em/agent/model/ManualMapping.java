/**
 * 
 */
package org.evolvis.idm.em.agent.model;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;

/**
 * @author Yorka Neumann, tarent GmbH
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "ManualMapping")
public abstract class ManualMapping<T extends AbstractEntity> extends AbstractEntity {

	@ManyToOne(optional = false)
	private AbstractAgent agent;
	
	@OneToOne(optional = false)
	private T idmEntity;
	
	
	private Long foreinEntityId;
	
	/**
	 * 
	 */
	public ManualMapping() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.evolvis.idm.common.model.AbstractEntity#getClientName()
	 */
	@Override
	public String getClientName() {
		if(this.getIdmEntity() != null && this.getIdmEntity().getClientName() != null)
			return this.getIdmEntity().getClientName();
		if(this.getAgent() != null)
			return this.getAgent().getClientName();
		return null;
	}

	/**
	 * @param agent the agent to set
	 */
	public void setAgent(AbstractAgent agent) {
		this.agent = agent;
	}

	/**
	 * @return the agent
	 */
	public AbstractAgent getAgent() {
		return agent;
	}

	/**
	 * @param idmEntity the idmEntity to set
	 */
	public void setIdmEntity(T idmEntity) {
		this.idmEntity = idmEntity;
	}

	/**
	 * @return the idmEntity
	 */
	public T getIdmEntity() {
		return idmEntity;
	}

	/**
	 * @param foreinEntityId the foreinEntityId to set
	 */
	public void setForeinEntityId(Long foreinEntityId) {
		this.foreinEntityId = foreinEntityId;
	}

	/**
	 * @return the foreinEntityId
	 */
	public Long getForeinEntityId() {
		return foreinEntityId;
	}
	
}
