package org.evolvis.idm.em.queue;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

/**
 * Message-Driven Bean implementation class for: HelloMDBQueue
 * 
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/HelloQueue"),
		@ActivationConfigProperty(propertyName = "providerAdapterJNDI", propertyValue = "java:/DefaultJMSProvider")
})
public class EventQueue implements MessageListener {

	private Logger logger = Logger.getLogger(getClass());
	
	private int messageCount = 0;
	
	/**
	 * Default constructor.
	 */
	public EventQueue() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see MessageListener#onMessage(Message)
	 */
	public void onMessage(Message message) {
		
		logger.info("Message received: " + message + ", Received message count: "+ ++messageCount);
		if (message instanceof TextMessage) {
			TextMessage textMessage = (TextMessage) message;
			try {
				String payLoad = textMessage.getText();
				logger.info("Payload was: " + payLoad);
			} catch (JMSException e) {
				logger.error("Exception happened: ", e);
			}
		}

	}
}