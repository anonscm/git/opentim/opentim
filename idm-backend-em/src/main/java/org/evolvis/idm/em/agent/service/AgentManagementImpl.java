/**
 * 
 */
package org.evolvis.idm.em.agent.service;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryConfiguration;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;

import org.evolvis.idm.em.agent.model.AbstractAgent;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.jboss.wsf.spi.annotation.WebContext;

/**
 * @author Yorka Neumann, tarent GmbH
 * 
 */
@Stateless
@Local(AccountManagement.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "AgentManagementService", name = "AgentManagementServicePT", portName = "AgentManagementServicePort")
@WebContext(contextRoot = "idm-backend-em", urlPattern = "/AgentManagementService")
public class AgentManagementImpl extends AbstractBaseServiceImpl implements AgentManagementService {

	private String qlString = "SELECT agent FROM Agent agent";

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.evolvis.idm.em.agent.service.AgentRetrievalService#getAgentByClientAndAgentId(java.lang.String, java.lang.Long)
	 */
	@Override
	@WebMethod
	@WebResult(name = "agent")
	public AbstractAgent getAgentByClientAndAgentId(@WebParam(name = "clientName") String clientName, @WebParam(name = "agentId") Long agentId) throws BackendException, IllegalRequestException {
		if(clientName == null || agentId == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		String localQlString = qlString + " WHERE agent.id = :agentId AND agent.client.name = :clientName";
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("agentId", agentId);
		parameterMap.put("clientName", clientName);
		QueryConfiguration queryConfiguration = new QueryConfiguration(localQlString, parameterMap);
		QueryResult<AbstractAgent> queryResult = new QueryResult<AbstractAgent>();
		queryResult = this.executeSimpleQuery(localQlString, queryConfiguration, queryResult);
		if (queryResult.size() == 1)
			return queryResult.get(0);
		if (queryResult.size() == 0)
			return null;
		throw BackendException.UNKNOWN_ERROR_EXCEPTION;
	}

	private QueryResult<AbstractAgent> getAllAgents(String qlString, Map<String, Object> parameterMap) throws IllegalRequestException, BackendException {
		QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap);
		QueryResult<AbstractAgent> queryResult = new QueryResult<AbstractAgent>();
		return this.executeSimpleQuery(qlString, queryConfiguration, queryResult);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.evolvis.idm.em.agent.service.AgentRetrievalService#getAllAgents()
	 */
	@Override
	@WebMethod
	@WebResult(name = "allAgents")
	public QueryResult<AbstractAgent> getAllAgents() throws IllegalRequestException, BackendException {
		return this.getAllAgents(qlString, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.evolvis.idm.em.agent.service.AgentRetrievalService#getApplicationAgents(java.lang.String, java.lang.String)
	 */
	@Override
	@WebMethod
	@WebResult(name = "applicationAgents")
	public QueryResult<AbstractAgent> getApplicationAgents(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName) throws IllegalRequestException, BackendException {
		if(clientName == null || applicationName == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("clientName", clientName);
		parameterMap.put("applicationName", applicationName);
		return this.getAllAgents(qlString+" WHERE agent.application.name = :applicationName AND agent.client.name = :clientName ", parameterMap);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.evolvis.idm.em.agent.service.AgentRetrievalService#getClientAgents(java.lang.String)
	 */
	@Override
	@WebMethod
	@WebResult(name = "clientAgents")
	public QueryResult<AbstractAgent> getClientAgents(@WebParam(name = "clientName") String clientName) throws IllegalRequestException, BackendException {
		if(clientName == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("clientName", clientName);
		return this.getAllAgents(qlString+" WHERE agent.client.name = :clientName", parameterMap);		
	}

	@Override
	@WebMethod
	public void deleteAgent(@WebParam(name = "clientName") String clientName, @WebParam(name = "agentId") Long agentId) throws IllegalRequestException, BackendException {
		if(clientName == null || agentId == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		AbstractAgent agent = this.getAgentByClientAndAgentId(clientName, agentId);
		if(agent == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		this.remove(agent);
	}

	@Override
	@WebMethod
	@WebResult(name = "agent")
	public AbstractAgent setAgent(@WebParam(name = "clientName") String clientName, @WebParam(name = "agent") AbstractAgent agent) throws IllegalRequestException, BackendException {
		if(clientName == null || agent == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		if(agent.getId() == null){
			this.persist(agent);
		}
		else{
			agent = this.merge(agent);
		}
		return agent;
	}

}
