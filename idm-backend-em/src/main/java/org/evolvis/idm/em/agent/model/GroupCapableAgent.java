package org.evolvis.idm.em.agent.model;

import java.util.List;
import java.util.Map;

import org.evolvis.idm.em.core.model.Event;
import org.evolvis.idm.em.core.model.EventResult;
import org.evolvis.idm.relation.permission.model.Group;

public interface GroupCapableAgent extends Agent {

	public Map<String,String> getManualGroupMapping();

	public List<String> getUnmappedGroups();
	
	public EventResult processGroupEvent(Event<Group> groupEvent);
}
