------------------------------------------------------------------------
--- IdM postgresql database schema install script - created 26. Feb. 2010
--- for IdM backend version 1.3.3
---
--- Notes: 
--- Executing this script creates the idm database schema into the postgresql schema 'public'. 
--- If schema 'public' does not exist already, remove the comment in line 14.
--- If you want to use another postgresql schema than 'public', replace all occurences of 'public' with the schema name you want to use. 
--- If you want to change the owner of the idm database structure, remove comment from and edit the lines 595 - 620.
--- If you want to replace an already existing idm database structure, please execute the pg_drop_idm-db.sql before. 
------------------------------------------------------------------------


--- CREATE SCHEMA public;


------------------------------------------------------------------------
--- *** CREATE IDM-DB structure ***
------------------------------------------------------------------------



SET search_path = public, pg_catalog;


CREATE SEQUENCE HIBERNATE_SEQUENCE
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;




------------------------------------------------------------------------
--- Create IDM-Tables
------------------------------------------------------------------------

--------------------------------------------------------
--  DDL for Table IDM_ACCOUNT
--------------------------------------------------------
CREATE TABLE IDM_ACCOUNT (
    id bigint NOT NULL,
    creationdate date,
    deactivationdate date,
    displayname character varying(100) NOT NULL,
    softdelete boolean NOT NULL,
    username character varying(100) NOT NULL,
    uuid character varying(100) NOT NULL,
    client_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_APPLICATION
--------------------------------------------------------
CREATE TABLE IDM_APPLICATION (
    id bigint NOT NULL,
    certificationdata character varying(255),
    displayname character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    client_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_APPMETADATA
--------------------------------------------------------
CREATE TABLE IDM_APPMETADATA (
    id bigint NOT NULL,
    account_id bigint NOT NULL,
    application_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_APPMETADATAVALUE
--------------------------------------------------------
CREATE TABLE IDM_APPMETADATAVALUE (
    id bigint NOT NULL,
    key character varying(255),
    value character varying(255),
    map_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_APPROVAL
--------------------------------------------------------
CREATE TABLE IDM_APPROVAL (
    id bigint NOT NULL,
    account_id bigint NOT NULL,
    application_id bigint NOT NULL,
    approvedattributevalue_id bigint,
    approvedvalueset_id bigint
);



--------------------------------------------------------
--  DDL for Table IDM_ATTRIBUTE
--------------------------------------------------------
CREATE TABLE IDM_ATTRIBUTE (
    id bigint NOT NULL,
    additionaltypeconstraint character varying(256),
    displayname character varying(100) NOT NULL,
    displayorder integer,
    multiple boolean NOT NULL,
    name character varying(100) NOT NULL,
    syncoptions character varying(256),
    type integer NOT NULL,
    writeprotected boolean NOT NULL,
    attributegroup_id bigint,
    parentattribute_id bigint
);



--------------------------------------------------------
--  DDL for Table IDM_ATTRIBUTEGROUP
--------------------------------------------------------
CREATE TABLE IDM_ATTRIBUTEGROUP (
    id bigint NOT NULL,
    displayname character varying(100) NOT NULL,
    displayorder integer,
    multiple boolean NOT NULL,
    name character varying(100) NOT NULL,
    client_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_CLIENT
--------------------------------------------------------
CREATE TABLE IDM_CLIENT (
    id bigint NOT NULL,
    certificationdata character varying(255),
    displayname character varying(50) NOT NULL,
    name character varying(255) NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_CLIENTPROPERTYMAP
--------------------------------------------------------
CREATE TABLE IDM_CLIENTPROPERTYMAP (
    id bigint NOT NULL,
    client_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_CLIENTPROPERTYMAPENTRY
--------------------------------------------------------
CREATE TABLE IDM_CLIENTPROPERTYMAPENTRY (
    id bigint NOT NULL,
    key character varying(255),
    value character varying(255),
    map_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_GROUP
--------------------------------------------------------
CREATE TABLE IDM_GROUP (
    id bigint NOT NULL,
    displayname character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    writeprotected boolean NOT NULL,
    client_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_GROUPASSIGNMENT
--------------------------------------------------------
CREATE TABLE IDM_GROUPASSIGNMENT (
    id bigint NOT NULL,
    account_id bigint NOT NULL,
    group_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_INTERNALROLEASSIGNMENT
--------------------------------------------------------
CREATE TABLE IDM_INTERNALROLEASSIGNMENT (
    id bigint NOT NULL,
    type integer,
    account_id bigint
);



--------------------------------------------------------
--  DDL for Table IDM_INTERNALROLESCOPE
--------------------------------------------------------
CREATE TABLE IDM_INTERNALROLESCOPE (
    id bigint NOT NULL,
    group_id bigint NOT NULL,
    internalroleassignment_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_ORGUNIT
--------------------------------------------------------
CREATE TABLE IDM_ORGUNIT (
    id bigint NOT NULL,
    displayname character varying(255),
    name character varying(50) NOT NULL,
    group_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_ORGUNITMAP
--------------------------------------------------------
CREATE TABLE IDM_ORGUNITMAP (
    id bigint NOT NULL,
    orgunit_id bigint NOT NULL
);





--------------------------------------------------------
--  DDL for Table IDM_ORGUNITMAPENTRY
--------------------------------------------------------
CREATE TABLE IDM_ORGUNITMAPENTRY (
    id bigint NOT NULL,
    key character varying(255),
    value character varying(255),
    map_id bigint NOT NULL
);




--------------------------------------------------------
--  DDL for Table IDM_ROLE
--------------------------------------------------------
CREATE TABLE IDM_ROLE (
    id bigint NOT NULL,
    displayname character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    application_id bigint NOT NULL
);




--------------------------------------------------------
--  DDL for Table IDM_ROLEASSIGNMENT
--------------------------------------------------------
CREATE TABLE IDM_ROLEASSIGNMENT (
    id bigint NOT NULL,
    scope character varying(255),
    role_id bigint NOT NULL,
    account_id bigint,
    group_id bigint
);



--------------------------------------------------------
--  DDL for Table IDM_VALUE
--------------------------------------------------------
CREATE TABLE IDM_VALUE (
    id bigint NOT NULL,
    bytevalue oid,
    islocked boolean NOT NULL,
    sortorder integer,
    value character varying(255),
    attribute_id bigint NOT NULL,
    valueset_id bigint NOT NULL
);



--------------------------------------------------------
--  DDL for Table IDM_VALUESET
--------------------------------------------------------
CREATE TABLE IDM_VALUESET (
    id bigint NOT NULL,
    islocked boolean NOT NULL,
    sortorder integer,
    account_id bigint NOT NULL,
    attributegroup_id bigint NOT NULL
);




--------------------------------------------------------
-- *** Create constraints for IDM-Tables ***
--------------------------------------------------------


--------------------------------------------------------
--  Constraints for Table IDM_ACCOUNT
--------------------------------------------------------
ALTER TABLE ONLY IDM_ACCOUNT ADD CONSTRAINT IDM_ACCOUNT_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_ACCOUNT ADD CONSTRAINT IDM_ACCOUNT_UC_1 UNIQUE (uuid);
ALTER TABLE ONLY IDM_ACCOUNT ADD CONSTRAINT IDM_ACCOUNT_UC_2 UNIQUE (client_id, username);

--------------------------------------------------------
--  Constraints for Table IDM_APPLICATION
--------------------------------------------------------
ALTER TABLE ONLY IDM_APPLICATION ADD CONSTRAINT IDM_APPLICATION_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_APPLICATION ADD CONSTRAINT IDM_APPLICATION_UC_1 UNIQUE (name, client_id);

--------------------------------------------------------
--  Constraints for Table IDM_APPMETADATA
--------------------------------------------------------
ALTER TABLE ONLY IDM_APPMETADATA ADD CONSTRAINT IDM_APPMETADATA_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_APPMETADATA ADD CONSTRAINT IDM_APPMETADATA_UC_1 UNIQUE (account_id, application_id);

--------------------------------------------------------
--  Constraints for Table IDM_APPMETADATAVALUE
--------------------------------------------------------
ALTER TABLE ONLY IDM_APPMETADATAVALUE ADD CONSTRAINT IDM_APPMETADATAVALUE_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_APPMETADATAVALUE ADD CONSTRAINT IDM_APPMETADATAVALUE_UC_1 UNIQUE (key, map_id);

--------------------------------------------------------
--  Constraints for Table IDM_APPROVAL
--------------------------------------------------------
ALTER TABLE ONLY IDM_APPROVAL ADD CONSTRAINT IDM_APPROVAL_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_APPROVAL ADD CONSTRAINT IDM_APPROVAL_UC_1 UNIQUE (application_id, account_id, approvedvalueset_id, approvedattributevalue_id);
ALTER TABLE ONLY IDM_APPROVAL ADD CONSTRAINT IDM_APPROVAL_UC_2 UNIQUE (application_id, account_id, approvedattributevalue_id);
ALTER TABLE ONLY IDM_APPROVAL ADD CONSTRAINT IDM_APPROVAL_UC_3 UNIQUE (application_id, account_id, approvedvalueset_id);

--------------------------------------------------------
--  Constraints for Table IDM_ATTRIBUTE
--------------------------------------------------------
ALTER TABLE ONLY IDM_ATTRIBUTE ADD CONSTRAINT IDM_ATTRIBUTE_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_ATTRIBUTE ADD CONSTRAINT IDM_ATTRIBUTE_UC_1 UNIQUE (name, attributegroup_id);
ALTER TABLE ONLY IDM_ATTRIBUTE ADD CONSTRAINT IDM_ATTRIBUTE_UC_2 UNIQUE (name, parentattribute_id);

--------------------------------------------------------
--  Constraints for Table IDM_ATTRIBUTEGROUP
--------------------------------------------------------
ALTER TABLE ONLY IDM_ATTRIBUTEGROUP ADD CONSTRAINT IDM_ATTRIBUTEGROUP_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_ATTRIBUTEGROUP ADD CONSTRAINT IDM_ATTRIBUTEGROUP_UC_1 UNIQUE (name, client_id);

--------------------------------------------------------
--  Constraints for Table IDM_CLIENT
--------------------------------------------------------
ALTER TABLE ONLY IDM_CLIENT ADD CONSTRAINT IDM_CLIENT_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_CLIENT ADD CONSTRAINT IDM_CLIENT_UC_1 UNIQUE (name);

--------------------------------------------------------
--  Constraints for Table IDM_CLIENTPROPERTYMAP
--------------------------------------------------------
ALTER TABLE ONLY IDM_CLIENTPROPERTYMAP ADD CONSTRAINT IDM_CLIENTPROPERTYMAP_PK PRIMARY KEY (id);
ALTER TABLE ONLY idm_clientpropertymap ADD CONSTRAINT IDM_CLIENTPROPERTYMAP_UC_1 UNIQUE (client_id);

--------------------------------------------------------
--  Constraints for Table IDM_CLIENTPROPERTYMAPENTRY
--------------------------------------------------------
ALTER TABLE ONLY IDM_CLIENTPROPERTYMAPENTRY ADD CONSTRAINT IDM_CLIENTPROP_MAPENTRY_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_CLIENTPROPERTYMAPENTRY ADD CONSTRAINT IDM_CLIENTPROP_MAPENTRY_UC_1 UNIQUE (key, map_id);

--------------------------------------------------------
--  Constraints for Table IDM_GROUP
--------------------------------------------------------
ALTER TABLE ONLY IDM_GROUP ADD CONSTRAINT IDM_GROUP_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_GROUP ADD CONSTRAINT IDM_GROUP_UC_1 UNIQUE (client_id, name);

--------------------------------------------------------
--  Constraints for Table IDM_GROUPASSIGNMENT
--------------------------------------------------------
ALTER TABLE ONLY IDM_GROUPASSIGNMENT ADD CONSTRAINT IDM_GROUPASSIGNMENT_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_GROUPASSIGNMENT ADD CONSTRAINT IDM_GROUPASSIGNMENT_UC_1 UNIQUE (account_id, group_id);

--------------------------------------------------------
--  Constraints for Table IDM_INTERNALROLEASSIGNMENT
--------------------------------------------------------
ALTER TABLE ONLY IDM_INTERNALROLEASSIGNMENT ADD CONSTRAINT IDM_IROLEASSIGNMENT_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_INTERNALROLEASSIGNMENT ADD CONSTRAINT IDM_IROLEASSIGNMENT_UC_1 UNIQUE (account_id, type);

--------------------------------------------------------
--  Constraints for Table IDM_INTERNALROLESCOPE
--------------------------------------------------------
ALTER TABLE ONLY IDM_INTERNALROLESCOPE ADD CONSTRAINT IDM_INTERNALROLESCOPE_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_INTERNALROLESCOPE ADD CONSTRAINT IDM_INTERNALROLESCOPE_UC_1 UNIQUE (internalroleassignment_id, group_id);


--------------------------------------------------------
--  Constraints for Table IDM_ORGUNIT
--------------------------------------------------------
ALTER TABLE ONLY IDM_ORGUNIT ADD CONSTRAINT IDM_ORGUNIT_PK PRIMARY KEY (id);

--------------------------------------------------------
--  Constraints for Table IDM_ORGUNITMAP
--------------------------------------------------------
ALTER TABLE ONLY IDM_ORGUNITMAP ADD CONSTRAINT IDM_ORGUNITMAP_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_ORGUNITMAP ADD CONSTRAINT IDM_ORGUNITMAP_UC_1 UNIQUE (orgunit_id);

--------------------------------------------------------
--  Constraints for Table IDM_ORGUNITMAPENTRY
--------------------------------------------------------
ALTER TABLE ONLY IDM_ORGUNITMAPENTRY ADD CONSTRAINT IDM_ORGUNITMAPENTRY_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_ORGUNITMAPENTRY ADD CONSTRAINT IDM_ORGUNITMAPENTRY_UC_1 UNIQUE (key, map_id);

--------------------------------------------------------
--  Constraints for Table IDM_ROLE
--------------------------------------------------------
ALTER TABLE ONLY IDM_ROLE ADD CONSTRAINT IDM_ROLE_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_ROLE ADD CONSTRAINT IDM_ROLE_UC_1 UNIQUE (name, application_id);

--------------------------------------------------------
--  Constraints for Table IDM_ROLEASSIGNMENT
--------------------------------------------------------
ALTER TABLE ONLY IDM_ROLEASSIGNMENT ADD CONSTRAINT IDM_ROLEASSIGNMENT_PK PRIMARY KEY (id);
ALTER TABLE ONLY IDM_ROLEASSIGNMENT ADD CONSTRAINT IDM_ROLEASSIGNMENT_UC_1 UNIQUE (account_id, role_id, scope, group_id);

--------------------------------------------------------
--  Constraints for Table IDM_VALUE
--------------------------------------------------------
ALTER TABLE ONLY IDM_VALUE ADD CONSTRAINT IDM_VALUE_PK PRIMARY KEY (id);

--------------------------------------------------------
--  Constraints for Table IDM_VALUESET
--------------------------------------------------------
ALTER TABLE ONLY IDM_VALUESET ADD CONSTRAINT IDM_VALUESET_PK PRIMARY KEY (id);




--------------------------------------------------------
-- ** Ref Constraints for IDM-Tables **
--------------------------------------------------------

--------------------------------------------------------
--  Ref Constraints for Table IDM_ACCOUNT
--------------------------------------------------------
ALTER TABLE ONLY IDM_ACCOUNT ADD CONSTRAINT FK_ACCOUNT_TO_CLIENT FOREIGN KEY (client_id) REFERENCES IDM_CLIENT(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_APPLICATION
--------------------------------------------------------
ALTER TABLE ONLY IDM_APPLICATION ADD CONSTRAINT FK_APPLICATION_TO_CLIENT FOREIGN KEY (client_id) REFERENCES IDM_CLIENT(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_APPMETADATA
--------------------------------------------------------
ALTER TABLE ONLY IDM_APPMETADATA ADD CONSTRAINT FK_AMD_TO_ACCOUNT FOREIGN KEY (account_id) REFERENCES IDM_ACCOUNT(id) ON DELETE CASCADE;
ALTER TABLE ONLY IDM_APPMETADATA ADD CONSTRAINT FK_AMD_TO_APPLICATION FOREIGN KEY (application_id) REFERENCES IDM_APPLICATION(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_APPMETADATAVALUE
--------------------------------------------------------
ALTER TABLE ONLY IDM_APPMETADATAVALUE ADD CONSTRAINT FK_AMDVALUE_TO_AMD FOREIGN KEY (map_id) REFERENCES IDM_APPMETADATA(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_APPROVAL
--------------------------------------------------------
ALTER TABLE ONLY IDM_APPROVAL ADD CONSTRAINT FK_APPROVAL_TO_ACCOUNT FOREIGN KEY (account_id) REFERENCES IDM_ACCOUNT(id) ON DELETE CASCADE;
ALTER TABLE ONLY IDM_APPROVAL ADD CONSTRAINT FK_APPROVAL_TO_VALUESET FOREIGN KEY (approvedvalueset_id) REFERENCES idm_valueset(id) ON DELETE CASCADE;
ALTER TABLE ONLY IDM_APPROVAL ADD CONSTRAINT FK_APPROVAL_TO_VALUE FOREIGN KEY (approvedattributevalue_id) REFERENCES IDM_VALUE(id) ON DELETE CASCADE;
ALTER TABLE ONLY IDM_APPROVAL ADD CONSTRAINT FK_APPROVAL_TO_APPLICATION FOREIGN KEY (application_id) REFERENCES IDM_APPLICATION(id) ON DELETE CASCADE;
ALTER TABLE ONLY IDM_APPROVAL ADD CONSTRAINT FK_APPROVAL_TO_APPROVAL FOREIGN KEY (id) REFERENCES IDM_APPROVAL(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_ATTRIBUTE
--------------------------------------------------------
ALTER TABLE ONLY IDM_ATTRIBUTE ADD CONSTRAINT FK_ATTRIBUTE_TO_ATTRIBUTEGROUP FOREIGN KEY (attributegroup_id) REFERENCES IDM_ATTRIBUTEGROUP(id) ON DELETE CASCADE;
ALTER TABLE ONLY IDM_ATTRIBUTE ADD CONSTRAINT FK_ATTRIBUTE_TO_ATTRIBUTE FOREIGN KEY (parentattribute_id) REFERENCES IDM_ATTRIBUTE(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_ATTRIBUTEGROUP
--------------------------------------------------------
ALTER TABLE ONLY IDM_ATTRIBUTEGROUP ADD CONSTRAINT FK_ATTRIBUTEGROUP_TO_CLIENT FOREIGN KEY (client_id) REFERENCES IDM_CLIENT(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_CLIENTPROPERTYMAP
--------------------------------------------------------
ALTER TABLE ONLY IDM_CLIENTPROPERTYMAP ADD CONSTRAINT FK_CPROPMAP_TO_CLIENT FOREIGN KEY (client_id) REFERENCES IDM_CLIENT(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_CLIENTPROPERTYMAPENTRY
--------------------------------------------------------
ALTER TABLE ONLY IDM_CLIENTPROPERTYMAPENTRY ADD CONSTRAINT FK_CPROPMAPENTRY_TO_CPROPMAP FOREIGN KEY (map_id) REFERENCES idm_clientpropertymap(id) ON DELETE CASCADE;


--------------------------------------------------------
--  Ref Constraints for Table IDM_GROUP
--------------------------------------------------------
ALTER TABLE ONLY IDM_GROUP ADD CONSTRAINT FK_GROUP_TO_CLIENT FOREIGN KEY (client_id) REFERENCES IDM_CLIENT(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_GROUPASSIGNMENT
--------------------------------------------------------
ALTER TABLE ONLY IDM_GROUPASSIGNMENT ADD CONSTRAINT FK_GROUPASSIGNMENT_TO_ACCOUNT FOREIGN KEY (account_id) REFERENCES IDM_ACCOUNT(id) ON DELETE CASCADE;
ALTER TABLE ONLY IDM_GROUPASSIGNMENT ADD CONSTRAINT FK_GROUPASSIGNMENT_TO_GROUP FOREIGN KEY (group_id) REFERENCES IDM_GROUP(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_INTERNALROLEASSIGNMENT
--------------------------------------------------------
ALTER TABLE ONLY IDM_INTERNALROLEASSIGNMENT ADD CONSTRAINT FK_IROLEASSIGNMENT_TO_ACCOUNT FOREIGN KEY (account_id) REFERENCES IDM_ACCOUNT(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_INTERNALROLESCOPE
--------------------------------------------------------
ALTER TABLE ONLY IDM_INTERNALROLESCOPE ADD CONSTRAINT FK_IRSCOPE_TO_IROLEASSIGNMENT FOREIGN KEY (internalroleassignment_id) REFERENCES IDM_INTERNALROLEASSIGNMENT(id) ON DELETE CASCADE;
ALTER TABLE ONLY IDM_INTERNALROLESCOPE ADD CONSTRAINT FK_IRSCOPE_TO_GROUP FOREIGN KEY (group_id) REFERENCES idm_group(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_ORGUNIT
--------------------------------------------------------
ALTER TABLE ONLY IDM_ORGUNIT ADD CONSTRAINT FK_ORGUNIT_TO_GROUP FOREIGN KEY (group_id) REFERENCES IDM_GROUP(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_ORGUNITMAP
--------------------------------------------------------
ALTER TABLE ONLY IDM_ORGUNITMAP ADD CONSTRAINT FK_OUMAP_TO_OU FOREIGN KEY (orgunit_id) REFERENCES IDM_ORGUNIT(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_ORGUNITMAPENTRY
--------------------------------------------------------
ALTER TABLE ONLY IDM_ORGUNITMAPENTRY
    ADD CONSTRAINT FK_OUMAPENTRY_TO_OUMAP FOREIGN KEY (map_id) REFERENCES IDM_ORGUNITMAP(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_ROLE
--------------------------------------------------------
ALTER TABLE ONLY IDM_ROLE ADD CONSTRAINT FK_ROLE_TO_APPLICATION FOREIGN KEY (application_id) REFERENCES IDM_APPLICATION(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_ROLEASSIGNMENT
--------------------------------------------------------
ALTER TABLE ONLY IDM_ROLEASSIGNMENT ADD CONSTRAINT FK_ROLEASSIGNMENT_TO_ACCOUNT FOREIGN KEY (account_id) REFERENCES IDM_ACCOUNT(id) ON DELETE CASCADE;
ALTER TABLE ONLY IDM_ROLEASSIGNMENT ADD CONSTRAINT FK_ROLEASSIGNMENT_TO_GROUP FOREIGN KEY (group_id) REFERENCES IDM_GROUP(id) ON DELETE CASCADE;
ALTER TABLE ONLY IDM_ROLEASSIGNMENT ADD CONSTRAINT FK_ROLEASSIGNMENT_TO_ROLE FOREIGN KEY (role_id) REFERENCES IDM_ROLE(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_VALUE
--------------------------------------------------------
ALTER TABLE ONLY IDM_VALUE ADD CONSTRAINT FK_VALUE_TO_VALUESET FOREIGN KEY (valueset_id) REFERENCES IDM_VALUESET(id) ON DELETE CASCADE;
ALTER TABLE ONLY IDM_VALUE ADD CONSTRAINT FK_VALUE_TO_ATTRIBUTE FOREIGN KEY (attribute_id) REFERENCES IDM_ATTRIBUTE(id) ON DELETE CASCADE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_VALUESET
--------------------------------------------------------
ALTER TABLE ONLY IDM_VALUESET ADD CONSTRAINT FK_VALUESET_TO_ACCOUNT FOREIGN KEY (account_id) REFERENCES IDM_ACCOUNT(id) ON DELETE CASCADE;

ALTER TABLE ONLY IDM_VALUESET ADD CONSTRAINT FK_VALUESET_TO_ATTRIBUTEGROUP FOREIGN KEY (attributegroup_id) REFERENCES IDM_ATTRIBUTEGROUP(id) ON DELETE CASCADE;






--------------------------------------------------------
-- *** Create constraints for IDM-Views ***
--------------------------------------------------------
CREATE VIEW IDM_USER AS
SELECT a.id, a.creationdate, a.deactivationdate, a.displayname, a.softdelete, a.username, a.uuid, a.client_id, v1.value AS firstname, v2.value AS lastname 
FROM 
IDM_ACCOUNT a JOIN IDM_VALUESET vs ON a.id = vs.account_id
JOIN IDM_VALUE v1 ON v1.valueset_id = vs.id
JOIN IDM_ATTRIBUTE a1 ON v1.attribute_id = a1.id AND a1.name::text = 'firstname'::text
JOIN IDM_VALUE v2 ON v2.valueset_id = vs.id
JOIN IDM_ATTRIBUTE a2 ON v2.attribute_id = a2.id AND a2.name::text = 'lastname'::text 
JOIN IDM_ATTRIBUTEGROUP ag ON a2.attributegroup_id = ag.id AND ag.name::text = 'personalData'::text;


CREATE VIEW IDM_ROLEUSER AS
    SELECT DISTINCT u.id, u.creationdate, u.deactivationdate, u.displayname, u.softdelete, u.username, u.uuid, u.client_id, u.firstname, u.lastname, false AS directassignment, ra.role_id, ra.scope FROM (((idm_user u JOIN idm_groupassignment ga ON ((u.id = ga.account_id))) JOIN idm_group group1 ON ((ga.group_id = group1.id))) JOIN idm_roleassignment ra ON ((group1.id = ra.group_id))) WHERE (NOT (u.id IN (SELECT DISTINCT ui.id FROM (idm_user ui JOIN idm_roleassignment rai ON ((ui.id = rai.account_id)))))) UNION SELECT DISTINCT u.id, u.creationdate, u.deactivationdate, u.displayname, u.softdelete, u.username, u.uuid, u.client_id, u.firstname, u.lastname, true AS directassignment, ra.role_id, ra.scope FROM (idm_user u JOIN idm_roleassignment ra ON ((u.id = ra.account_id)));


CREATE VIEW IDM_CURRENTTIMESTAMP AS
    SELECT now.now AS "timestamp" FROM now() now(now);

---------------------------------------------------------
--- *** Change the owner of the idm database structure
---------------------------------------------------------
/*
ALTER SCHEMA public OWNER TO postgres;
ALTER TABLE public.HIBERNATE_SEQUENCE OWNER TO postgres;
ALTER TABLE public.IDM_ACCOUNT OWNER TO postgres;
ALTER TABLE public.IDM_APPLICATION OWNER TO postgres;
ALTER TABLE public.IDM_APPMETADATA OWNER TO postgres;
ALTER TABLE public.IDM_APPMETADATAVALUE OWNER TO postgres;
ALTER TABLE public.IDM_APPROVAL OWNER TO postgres;
ALTER TABLE public.IDM_ATTRIBUTE OWNER TO postgres;
ALTER TABLE public.IDM_ATTRIBUTEGROUP OWNER TO postgres;
ALTER TABLE public.IDM_CLIENT OWNER TO postgres;
ALTER TABLE public.IDM_CLIENTPROPERTYMAP OWNER TO postgres;
ALTER TABLE public.IDM_CLIENTPROPERTYMAPENTRY OWNER TO postgres;
ALTER TABLE public.IDM_GROUP OWNER TO postgres;
ALTER TABLE public.IDM_GROUPASSIGNMENT OWNER TO postgres;
ALTER TABLE public.IDM_INTERNALROLEASSIGNMENT OWNER TO postgres;
ALTER TABLE public.IDM_INTERNALROLESCOPE OWNER TO postgres;
ALTER TABLE public.IDM_ORGUNIT OWNER TO postgres;
ALTER TABLE public.IDM_ORGUNITMAP OWNER TO postgres;
ALTER TABLE public.IDM_ORGUNITMAPENTRY OWNER TO postgres;
ALTER TABLE public.IDM_ROLE OWNER TO postgres;
ALTER TABLE public.IDM_ROLEASSIGNMENT OWNER TO postgres;
ALTER TABLE public.IDM_VALUE OWNER TO postgres;
ALTER TABLE public.IDM_VALUESET OWNER TO postgres;
ALTER TABLE public.IDM_USER OWNER TO postgres;
ALTER TABLE public.idm_roleuser OWNER TO postgres;
ALTER TABLE public.idm_currenttimestamp OWNER TO postgres;
*/

--------------------------------------------------------------------
--- *** Adding default data ***
--------------------------------------------------------------------



---------------------------------------------------
--   DATA FOR TABLE IDM_CLIENT
--   FILTER = none used
---------------------------------------------------
--- REM INSERTING into IDM_CLIENT
Insert into IDM_CLIENT (ID,CERTIFICATIONDATA,DISPLAYNAME,NAME) values (1,null,'PUT_CLIENT_DISPLAYNAME_HERE','PUT_WEB_ID_HERE');

---------------------------------------------------
--   END DATA FOR TABLE IDM_CLIENT
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE IDM_CLIENTPROPERTYMAP
--   FILTER = none used
---------------------------------------------------
--- REM INSERTING into IDM_CLIENTPROPERTYMAP
INSERT into IDM_CLIENTPROPERTYMAP (ID, CLIENT_ID) VALUES (25,1);
---------------------------------------------------
--   END DATA FOR TABLE IDM_CLIENTPROPERTYMAP
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_CLIENTPROPERTYMAPENTRY
--   FILTER = none used
---------------------------------------------------
--- REM INSERTING into IDM_CLIENTPROPERTYMAPENTRY
-- opensso settings -- 
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (26,25,'openSso.serviceUrl','http://localhost:8080/opensso');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (27,25,'openSso.adminLogin','amAdmin');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (28,25,'openSso.adminPassword','');

-- cron job settings -- 
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (29,25,'system.dailyJobExecutionTime','02:00');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (30,25,'system.deactivatedAccountLifetimeInDays','30');

-- registration settings -- 
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (31,25,'registration.termsRequired.enabled','false');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (32,25,'registration.termsRequired.url','http://');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (33,25,'registration.displayNameGeneration.enabled','false');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (34,25,'registration.displayNameGeneration.pattern','%firstname% %lastname%');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (35,25,'registration.alternativeLink.enabled','false');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (36,25,'registration.alternativeLink.url','http://');

-- security settings --

INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (37,25,'security.password.pattern',E'^(?=.{6,74})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (38,25,'security.allowEmailExistenceHints.enabled','false');

-- ldap settings -- 
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (39,25,'ldapSync.enabled','false');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (40,25,'ldapSync.hostAndPort','@ldapSync.host@:@ldapSync.port@');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (41,25,'ldapSync.userDN','CN=Administrator,CN=Users,dc=@ldapSync.baseDN@');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (42,25,'ldapSync.password','@ldapSync.password@');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (43,25,'ldapSync.baseDN','@ldapSync.baseDN@');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (44,25,'ldapSync.searchDN','cn=Users');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (45,25,'ldapSync.searchFilter','(&(objectClass=person)(cn=LOGINNAME))');
--? INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (45,25,'LOGINNAME','');
-- search properties --
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (46,25,'search.pagingLimit','10');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (47,25,'search.fullPrivateDataSearch.enabled','true');

INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (48,25,'registration.successLink.enabled','false');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (49,25,'registration.successLink.url','http://');


-- ? INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (48,25,'internal.lastUpdateUsers','');
-- other properties -- 
-- ? INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (49,25,'other.allowDeleteUser.enabled','');


---------------------------------------------------
--   END DATA FOR TABLE IDM_CLIENTPROPERTYMAPENTRY
---------------------------------------------------



---------------------------------------------------
--   DATA FOR TABLE IDM_ATTRIBUTEGROUP
--   FILTER = none used
---------------------------------------------------
--- REM INSERTING into IDM_ATTRIBUTEGROUP
Insert into IDM_ATTRIBUTEGROUP (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,CLIENT_ID) values (2,'Name',1,false,'personalData',1);
Insert into IDM_ATTRIBUTEGROUP (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,CLIENT_ID) values (14,'Meldeadresse',2,false,'primaryAddress',1);
Insert into IDM_ATTRIBUTEGROUP (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,CLIENT_ID) values (21,'Kontaktdaten',3,false,'contactData',1);
Insert into IDM_ATTRIBUTEGROUP (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,CLIENT_ID) values (25,'Weitere Adressen',4,true,'furtherAddresses',1);

---------------------------------------------------
--   END DATA FOR TABLE IDM_ATTRIBUTEGROUP
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE IDM_ATTRIBUTE
--   FILTER = none used
---------------------------------------------------
--- REM INSERTING into IDM_ATTRIBUTE
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (3,'Anrede',1,false,'salutation',7,2,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED, ADDITIONALTYPECONSTRAINT) values (4,'Vorname',2,false,'firstname',6,2,true,E'[\\p{L}-\'`´ ]{2,40}');
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED, ADDITIONALTYPECONSTRAINT) values (5,'Nachname',3,false,'lastname',6,2,true,E'[\\p{L}-\'`´ ]{2,40}');
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (6,'Geburtsdatum',4,false,'birthdate',3,2,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (7,'Geschlecht',5,false,'gender',7,2,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (8,'Nationalität',6,false,'nationality',7,2,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (9,'Herr',1,false,'mr',0,3,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (10,'Frau',2,false,'mrs',0,3,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (11,'Fräulein',3,false,'miss',0,3,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (12,'männlich',1,false,'male',0,7,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (13,'weiblich',2,false,'female',0,7,false);

Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (15,'Straße',1,false,'streetName',0,14,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (16,'Hausnummer',2,false,'streetNumber',0,14,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (17,'Land',3,false,'country',7,14,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (18,'PLZ',4,false,'zipCode',0,14,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (19,'Ort',5,false,'city',0,14,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (20,'Bundesland',6,false,'state',0,14,false);

Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (22,'Telefon',1,true,'phone',0,21,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (23,'Telefax',2,true,'fax',0,21,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (24,'Mobil',3,true,'mobile',0,21,false);

Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (26,'Straße',1,false,'streetName',0,25,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (27,'Hausnummer',2,false,'streetNumber',0,25,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (28,'Land',3,false,'country',7,25,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (29,'PLZ',4,false,'zipCode',0,25,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (30,'Ort',5,false,'city',0,25,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (31,'Bundesland',6,false,'state',0,25,false);


/* subattributes for attribute 'nationality' of attribute group 'personal data' */
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (32,'deutsch',1,false,'DE',0,8,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (33,'britisch',2,false,'GB',0,8,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (34,'französisch',3,false,'FR',0,8,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (35,'polnisch',4,false,'PL',0,8,false);

/* subattributes for attribute 'country' of attribute group 'primaryAddress' */
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (36,'Deutschland',1,false,'DE',0,17,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (37,'Frankreich',2,false,'FR',0,17,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (38,'Polen',3,false,'PL',0,17,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (39,'Vereinigtes Königreich',4,false,'GB',0,17,false);

/*subattributes for attribute 'country' of attribute group 'furtherAddresses' */
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (40,'Deutschland',1,false,'DE',0,28,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (41,'Frankreich',2,false,'FR',0,28,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (42,'Polen',3,false,'PL',0,28,false);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (43,'Vereinigtes Königreich',4,false,'GB',0,28,false);


