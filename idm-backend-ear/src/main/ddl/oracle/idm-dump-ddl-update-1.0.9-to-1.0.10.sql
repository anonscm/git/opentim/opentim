--------------------------------------------------------
--  DDL for View IDM_ROLEUSER
--------------------------------------------------------

  	CREATE OR REPLACE VIEW "IDM_ROLEUSER" ("ID", "CREATIONDATE", "DEACTIVATIONDATE", "DISPLAYNAME", "SOFTDELETE", "USERNAME", "UUID", "CLIENT_ID", "DIRECTASSIGNMENT", "ROLE_ID", "SCOPE") AS 
 	SELECT DISTINCT account.*, 0 AS directAssignment, rA.role_id, ra.scope FROM idm_account account JOIN idm_groupassignment gA ON account.id = ga.account_id JOIN idm_group group1 ON ga.group_id = group1.id JOIN idm_roleassignment rA on group1.id = ra.group_id WHERE account.id NOT IN (SELECT DISTINCT accountI.id FROM idm_account accountI JOIN idm_roleassignment raI ON accountI.id = raI.account_id)  union SELECT DISTINCT account.* , 1 AS directAssignment, ra.role_id, ra.scope FROM idm_account account JOIN idm_roleassignment ra ON account.id = ra.account_id;

--------------------------------------------------------
--  Constraints for Table IDM_GROUPASSIGNMENT
--------------------------------------------------------
  	ALTER TABLE "IDM_GROUPASSIGNMENT" ADD CONSTRAINT "GA_UNIQUE_CONST_1" UNIQUE ("ACCOUNT_ID", "GROUP_ID") ENABLE;


--------------------------------------------------------
--  Constraints for Table IDM_ROLEASSIGNMENT
--------------------------------------------------------
	ALTER TABLE "IDM_ROLEASSIGNMENT" ADD CONSTRAINT "RA_UNIQUE_CONST_1" UNIQUE ("SCOPE", "ACCOUNT_ID", "GROUP_ID", "ROLE_ID") ENABLE;

