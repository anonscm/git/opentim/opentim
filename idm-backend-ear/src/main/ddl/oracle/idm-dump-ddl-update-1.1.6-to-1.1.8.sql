--------------------------------------------------------
--  Constraints for Table IDM_ACCOUNT
--------------------------------------------------------
  
  ALTER TABLE IDM_ACCOUNT DISABLE UNIQUE (CLIENT_ID, DISPLAYNAME);

--------------------------------------------------------
--  DDL for View IDM_CURRENTTIMESTAMP
--------------------------------------------------------

  CREATE OR REPLACE VIEW "IDM_CURRENTTIMESTAMP" ("TIMESTAMP") AS 
  SELECT  localtimestamp as timestamp FROM dual;

--------------------------------------------------------
--  DDL for View IDM_USER
--------------------------------------------------------

  CREATE OR REPLACE VIEW "IDM_USER" ("ID", "CREATIONDATE", "DEACTIVATIONDATE", "DISPLAYNAME", "SOFTDELETE", "USERNAME", "UUID", "CLIENT_ID", "FIRSTNAME", "LASTNAME") AS 
  SELECT a.*,v1.value AS firstname, v2.value AS lastname FROM idm_account a 
  JOIN idm_valueSet vs on a.id = vs.account_id 
  JOIN idm_value v1 on v1.valueset_id = vs.id 
  JOIN idm_attribute a1 on v1.attribute_id = a1.id AND a1.name = 'firstname'
  JOIN idm_value v2 on v2.valueset_id = vs.id
  JOIN idm_attribute a2 on v2.attribute_id = a2.id AND a2.name = 'lastname'
  JOIN idm_attributegroup ag on a2.attributegroup_id = ag.id AND ag.name = 'personalData';


--------------------------------------------------------
--  DDL for View IDM_ROLEUSER
--------------------------------------------------------

  CREATE OR REPLACE VIEW "IDM_ROLEUSER" ("ID", "CREATIONDATE", "DEACTIVATIONDATE", "DISPLAYNAME", "SOFTDELETE", "USERNAME", "UUID", "CLIENT_ID", "FIRSTNAME", "LASTNAME", "DIRECTASSIGNMENT", "ROLE_ID", "SCOPE") AS 
  SELECT DISTINCT u.*, 0 AS directAssignment, rA.role_id, ra.scope FROM idm_user u JOIN idm_groupassignment gA ON u.id = ga.account_id JOIN idm_group group1 ON ga.group_id = group1.id JOIN idm_roleassignment rA on group1.id = ra.group_id WHERE u.id NOT IN (SELECT DISTINCT uI.id FROM idm_user uI JOIN idm_roleassignment raI ON uI.id = raI.account_id)  union SELECT DISTINCT u.* , 1 AS directAssignment, ra.role_id, ra.scope FROM idm_user u JOIN idm_roleassignment ra ON u.id = ra.account_id;