------------------------------------------------------------------------
--- IdM oracle database schema update script - created 28. Jan. 2010
--- for updating IdM backend version [1.1.9 - 1.2.5] to 1.2.6
------------------------------------------------------------------------

ALTER TABLE "IDM_ATTRIBUTE" MODIFY ("ATTRIBUTEGROUP_ID" NULL );
ALTER TABLE "IDM_ATTRIBUTE" ADD "PARENTATTRIBUTE_ID" NUMBER(19,0);
ALTER TABLE "IDM_ATTRIBUTE" ADD CONSTRAINT "FK_ATTRIBUTE_TO_ATTRIBUTE" FOREIGN KEY ("PARENTATTRIBUTE_ID") REFERENCES "IDM_ATTRIBUTE" ("ID") ENABLE;
