------------------------------------------------------------------------
--- IdM oracle database update script - created 11. Dec.2009
--- for update from version 1.1.8 to 1.1.9 
------------------------------------------------------------------------


-----------------------------------------------------------------------
--- Unlock every attribute except of the attributes with name = 'firstname' or 'lastname' 
--- which are assigned to an attributegroup with name 'personalData'
-----------------------------------------------------------------------

UPDATE IDM_ATTRIBUTE SET WRITEPROTECTED = 0 WHERE id NOT IN 
(SELECT b.id 
 FROM IDM_ATTRIBUTE b 
 JOIN IDM_ATTRIBUTEGROUP ag ON b.attributegroup_id = ag.id 
 WHERE (b.name = 'firstname' OR b.name = 'lastname') AND ag.name = 'personalData');

----------------------------------------------------------------------
--- Change display name value of attribute with name 'primaryAddress' from 'Anschrift' to 'Meldeadresse'
--- and set remove multiple option from these attributes.
----------------------------------------------------------------------

UPDATE IDM_ATTRIBUTEGROUP SET displayname = 'Meldeadresse', multiple = 0 WHERE displayname = 'Anschrift' AND name = 'primaryAddress';

----------------------------------------------------------
--- Create a new attribute group 'furtherAddresses' for EACH client, 
--- filled with several attributes 
----------------------------------------------------------
DECLARE
  /* Output variables to hold the result of the query: */
  cid IDM_CLIENT.id%TYPE;
  /* Cursor declaration: */
  CURSOR clientIdCursor IS SELECT id FROM IDM_CLIENT FOR UPDATE;

BEGIN
  OPEN clientIdCursor;
  LOOP
    /* Retrieve each row of the result from the above query into PL/SQL variables: */
    FETCH clientIdCursor INTO cid;
    /* If there are no more rows to fetch, exit the loop: */
    EXIT WHEN clientIdCursor%NOTFOUND;
    /* Insert the reverse tuple: */

	---- Insert new attribute group with name 'furtherAddresses'
    Insert into IDM_ATTRIBUTEGROUP(ID, DISPLAYNAME, DISPLAYORDER, MULTIPLE, NAME, CLIENT_ID) values ( (SELECT max(id)+1 FROM idm_attributegroup),'Weitere Adressen',4,1,'furtherAddresses',cid);
    ---- Create attributes for that attribute group ---
	Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values ((SELECT max(id)+1 FROM IDM_ATTRIBUTE),'Straße',1,0,'streetName',0,(SELECT max(id) FROM IDM_ATTRIBUTEGROUP),0);
    Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values ((SELECT max(id)+1 FROM IDM_ATTRIBUTE),'Hausnummer',2,0,'streetNumber',0,(SELECT max(id) FROM IDM_ATTRIBUTEGROUP),0);
    Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values ((SELECT max(id)+1 FROM IDM_ATTRIBUTE),'Land',3,0,'country',0,(SELECT max(id) FROM IDM_ATTRIBUTEGROUP),0);
    Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values ((SELECT max(id)+1 FROM IDM_ATTRIBUTE),'PLZ',4,0,'zipCode',0,(SELECT max(id) FROM IDM_ATTRIBUTEGROUP),0);
    Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values ((SELECT max(id)+1 FROM IDM_ATTRIBUTE),'Ort',5,0,'city',0,(SELECT max(id) FROM IDM_ATTRIBUTEGROUP),0);
    Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values ((SELECT max(id)+1 FROM IDM_ATTRIBUTE),'Bundesland',6,0,'state',0,(SELECT max(id) FROM IDM_ATTRIBUTEGROUP),0);
  END LOOP;
  /* Free cursor used by the query. */
  CLOSE clientIdCursor;
END;