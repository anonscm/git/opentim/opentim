--------------------------------------------------------
--  Drop IDM_ORGUNITGROUPASSIGNMENT which is out of use
--------------------------------------------------------
DROP TABLE "IDM_ORGUNITGROUPASSIGNMENT" cascade constraints;
--------------------------------------------------------
--  Add attribute to IDM-VALUE to realize locking of values
--------------------------------------------------------
ALTER TABLE "IDM_VALUE" ADD ("ISLOCKED" NUMBER(1,0) DEFAULT 0 NOT NULL ENABLE);

--------------------------------------------------------
--  Add two attributes to table IDM_ATTRIBUTE
--------------------------------------------------------
ALTER TABLE "IDM_ATTRIBUTE" ADD "SYNCOPTIONS" VARCHAR2(256 BYTE);
ALTER TABLE "IDM_ATTRIBUTE" ADD "ADDITIONALTYPECONSTRAINT" VARCHAR2(256);

--------------------------------------------------------
--  Add unique constraint to table IDM_INTERNALROLEASSIGNMENT
--  in order to prevent multiple assignments of a user to a certain internal role
--------------------------------------------------------
ALTER TABLE "IDM_INTERNALROLEASSIGNMENT" ADD UNIQUE ("ACCOUNT_ID", "TYPE") ENABLE;

--------------------------------------------------------
--  DDL for Table IDM_CLIENTPROPERTYMAP
--------------------------------------------------------

  CREATE TABLE "IDM_CLIENTPROPERTYMAP" 
   (	"ID" NUMBER(19,0), 
	"CLIENT_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_CLIENTPROPERTYMAPENTRY
--------------------------------------------------------

  CREATE TABLE "IDM_CLIENTPROPERTYMAPENTRY" 
   (	"ID" NUMBER(19,0), 
	"KEY" VARCHAR2(255), 
	"VALUE" VARCHAR2(255), 
	"MAP_ID" NUMBER(19,0)
   ) ;

--------------------------------------------------------
--  Add unique constraint in order to prevent that an 
--  org unit has more than one org unit map.
--------------------------------------------------------
 ALTER TABLE "IDM_ORGUNITMAP" ADD UNIQUE ("ORGUNIT_ID") ENABLE;

--------------------------------------------------------
--  Constraints for Table IDM_CLIENTPROPERTYMAP
--------------------------------------------------------

  ALTER TABLE "IDM_CLIENTPROPERTYMAP" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAP" MODIFY ("CLIENT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAP" ADD PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAP" ADD UNIQUE ("CLIENT_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_CLIENTPROPERTYMAPENTRY
--------------------------------------------------------

  ALTER TABLE "IDM_CLIENTPROPERTYMAPENTRY" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAPENTRY" MODIFY ("MAP_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAPENTRY" ADD PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAPENTRY" ADD UNIQUE ("KEY", "MAP_ID") ENABLE;