------------------------------------------------------------------------
--- IdM oracle database schema install script - created 25. Feb. 2010
--- for IdM backend version 1.3.3
------------------------------------------------------------------------

------------------------------------------------------------------------
--- * Changed Attributes:
---   Attribute 'salutation','country' in attribute groups 'primaryAdress' and 'furtherAdresses' are now of type SELECTLIST
---   and got some subattributes.
--- * New Attributes: 'gender' and 'nationality' in attribute group 'personalData' of type SELECTLIST with some subattributes.
--- * Unique constraint 'IDM_ATTRIBUTE_UC_1' changed so that the combination of 'name','parentattribute_id' and 'attributegroup_id' has to be unique.
------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Only important if you want to replace the schema of an old idm-backend vers. <= 1.1.6
-- If so enable the following line 
-- ALTER TABLE IDM_ACCOUNT DROP UNIQUE (CLIENT_ID, DISPLAYNAME);
--------------------------------------------------------------------------------


  DROP TABLE "IDM_ACCOUNT" cascade constraints;
  DROP TABLE "IDM_APPLICATION" cascade constraints;
  DROP TABLE "IDM_APPMETADATA" cascade constraints;
  DROP TABLE "IDM_APPMETADATAVALUE" cascade constraints;
  DROP TABLE "IDM_APPROVAL" cascade constraints;
  DROP TABLE "IDM_ATTRIBUTE" cascade constraints;
  DROP TABLE "IDM_ATTRIBUTEGROUP" cascade constraints;
  DROP TABLE "IDM_CLIENT" cascade constraints;
  DROP TABLE "IDM_CLIENTPROPERTYMAP" cascade constraints;
  DROP TABLE "IDM_CLIENTPROPERTYMAPENTRY" cascade constraints;
  DROP TABLE "IDM_GROUP" cascade constraints;
  DROP TABLE "IDM_GROUPASSIGNMENT" cascade constraints;
  DROP TABLE "IDM_INTERNALROLEASSIGNMENT" cascade constraints;
  DROP TABLE "IDM_INTERNALROLESCOPE" cascade constraints;
  DROP TABLE "IDM_ORGUNIT" cascade constraints;
  DROP TABLE "IDM_ORGUNITMAP" cascade constraints;
  DROP TABLE "IDM_ORGUNITMAPENTRY" cascade constraints;
  DROP TABLE "IDM_ROLE" cascade constraints;
  DROP TABLE "IDM_ROLEASSIGNMENT" cascade constraints;
  DROP TABLE "IDM_VALUE" cascade constraints;
  DROP TABLE "IDM_VALUESET" cascade constraints;
  DROP SEQUENCE "HIBERNATE_SEQUENCE";
  DROP VIEW "IDM_ROLEUSER";
  DROP VIEW "IDM_USER";
  DROP VIEW "IDM_CURRENTTIMESTAMP";
 
--------------------------------------------------------
--  DDL for Sequence HIBERNATE_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "HIBERNATE_SEQUENCE"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 100 CACHE 20 NOORDER  NOCYCLE;
--------------------------------------------------------
--  DDL for Table IDM_ACCOUNT
--------------------------------------------------------

  CREATE TABLE "IDM_ACCOUNT" 
   (	"ID" NUMBER(19,0), 
	"CREATIONDATE" DATE, 
	"DEACTIVATIONDATE" DATE, 
	"DISPLAYNAME" VARCHAR2(100), 
	"SOFTDELETE" NUMBER(1,0), 
	"USERNAME" VARCHAR2(100), 
	"UUID" VARCHAR2(100), 
	"CLIENT_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_APPLICATION
--------------------------------------------------------

  CREATE TABLE "IDM_APPLICATION" 
   (	"ID" NUMBER(19,0), 
	"CERTIFICATIONDATA" VARCHAR2(255), 
	"DISPLAYNAME" VARCHAR2(50), 
	"NAME" VARCHAR2(50), 
	"CLIENT_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_APPMETADATA
--------------------------------------------------------

  CREATE TABLE "IDM_APPMETADATA" 
   (	"ID" NUMBER(19,0), 
	"ACCOUNT_ID" NUMBER(19,0), 
	"APPLICATION_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_APPMETADATAVALUE
--------------------------------------------------------

  CREATE TABLE "IDM_APPMETADATAVALUE" 
   (	"ID" NUMBER(19,0), 
	"KEY" VARCHAR2(255), 
	"VALUE" VARCHAR2(255), 
	"MAP_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_APPROVAL
--------------------------------------------------------

  CREATE TABLE "IDM_APPROVAL" 
   (	"ID" NUMBER(19,0), 
	"ACCOUNT_ID" NUMBER(19,0), 
	"APPLICATION_ID" NUMBER(19,0), 
	"APPROVEDATTRIBUTEVALUE_ID" NUMBER(19,0), 
	"APPROVEDVALUESET_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_ATTRIBUTE
--------------------------------------------------------

  CREATE TABLE "IDM_ATTRIBUTE" 
   (	"ID" NUMBER(19,0), 
	"ADDITIONALTYPECONSTRAINT" VARCHAR2(256), 
	"DISPLAYNAME" VARCHAR2(100), 
	"DISPLAYORDER" NUMBER(10,0), 
	"MULTIPLE" NUMBER(1,0), 
	"NAME" VARCHAR2(100), 
	"SYNCOPTIONS" VARCHAR2(256), 
	"TYPE" NUMBER(10,0), 
	"WRITEPROTECTED" NUMBER(1,0), 
	"ATTRIBUTEGROUP_ID" NUMBER(19,0),
	"PARENTATTRIBUTE_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_ATTRIBUTEGROUP
--------------------------------------------------------

  CREATE TABLE "IDM_ATTRIBUTEGROUP" 
   (	"ID" NUMBER(19,0), 
	"DISPLAYNAME" VARCHAR2(100), 
	"DISPLAYORDER" NUMBER(10,0), 
	"MULTIPLE" NUMBER(1,0), 
	"NAME" VARCHAR2(100), 
	"CLIENT_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_CLIENT
--------------------------------------------------------

  CREATE TABLE "IDM_CLIENT" 
   (	"ID" NUMBER(19,0), 
	"CERTIFICATIONDATA" VARCHAR2(255), 
	"DISPLAYNAME" VARCHAR2(50), 
	"NAME" VARCHAR2(50)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_CLIENTPROPERTYMAP
--------------------------------------------------------

  CREATE TABLE "IDM_CLIENTPROPERTYMAP" 
   (	"ID" NUMBER(19,0), 
	"CLIENT_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_CLIENTPROPERTYMAPENTRY
--------------------------------------------------------

  CREATE TABLE "IDM_CLIENTPROPERTYMAPENTRY" 
   (	"ID" NUMBER(19,0), 
	"KEY" VARCHAR2(255), 
	"VALUE" VARCHAR2(255), 
	"MAP_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_GROUP
--------------------------------------------------------

  CREATE TABLE "IDM_GROUP" 
   (	"ID" NUMBER(19,0), 
	"DISPLAYNAME" VARCHAR2(50), 
	"NAME" VARCHAR2(50), 
	"WRITEPROTECTED" NUMBER(1,0), 
	"CLIENT_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_GROUPASSIGNMENT
--------------------------------------------------------

  CREATE TABLE "IDM_GROUPASSIGNMENT" 
   (	"ID" NUMBER(19,0), 
	"ACCOUNT_ID" NUMBER(19,0), 
	"GROUP_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_INTERNALROLEASSIGNMENT
--------------------------------------------------------

  CREATE TABLE "IDM_INTERNALROLEASSIGNMENT" 
   (	"ID" NUMBER(19,0), 
	"TYPE" NUMBER(10,0), 
	"ACCOUNT_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_INTERNALROLESCOPE
--------------------------------------------------------

  CREATE TABLE "IDM_INTERNALROLESCOPE" 
   (	"ID" NUMBER(19,0), 
	"GROUP_ID" NUMBER(19,0), 
	"INTERNALROLEASSIGNMENT_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_ORGUNIT
--------------------------------------------------------

  CREATE TABLE "IDM_ORGUNIT" 
   (	"ID" NUMBER(19,0), 
	"DISPLAYNAME" VARCHAR2(255), 
	"NAME" VARCHAR2(50), 
	"GROUP_ID" NUMBER(19,0)
   ) ;

--------------------------------------------------------
--  DDL for Table IDM_ORGUNITMAP
--------------------------------------------------------

  CREATE TABLE "IDM_ORGUNITMAP" 
   (	"ID" NUMBER(19,0), 
	"ORGUNIT_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_ORGUNITMAPENTRY
--------------------------------------------------------

  CREATE TABLE "IDM_ORGUNITMAPENTRY" 
   (	"ID" NUMBER(19,0), 
	"KEY" VARCHAR2(255), 
	"VALUE" VARCHAR2(255), 
	"MAP_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_ROLE
--------------------------------------------------------

  CREATE TABLE "IDM_ROLE" 
   (	"ID" NUMBER(19,0), 
	"DISPLAYNAME" VARCHAR2(50), 
	"NAME" VARCHAR2(50), 
	"APPLICATION_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_ROLEASSIGNMENT
--------------------------------------------------------

  CREATE TABLE "IDM_ROLEASSIGNMENT" 
   (	"ID" NUMBER(19,0), 
	"SCOPE" VARCHAR2(255), 
	"ACCOUNT_ID" NUMBER(19,0), 
	"GROUP_ID" NUMBER(19,0), 
	"ROLE_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_VALUE
--------------------------------------------------------

  CREATE TABLE "IDM_VALUE" 
   (	"ID" NUMBER(19,0), 
	"BYTEVALUE" BLOB, 
	"ISLOCKED" NUMBER(1,0), 
	"SORTORDER" NUMBER(10,0), 
	"VALUE" VARCHAR2(255), 
	"ATTRIBUTE_ID" NUMBER(19,0), 
	"VALUESET_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table IDM_VALUESET
--------------------------------------------------------

  CREATE TABLE "IDM_VALUESET" 
   (	"ID" NUMBER(19,0), 
	"ISLOCKED" NUMBER(1,0), 
	"SORTORDER" NUMBER(10,0), 
	"ACCOUNT_ID" NUMBER(19,0), 
	"ATTRIBUTEGROUP_ID" NUMBER(19,0)
   ) ;



--------------------------------------------------------
--  Constraints for Table IDM_ACCOUNT
--------------------------------------------------------

  ALTER TABLE "IDM_ACCOUNT" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ACCOUNT" MODIFY ("DISPLAYNAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ACCOUNT" MODIFY ("SOFTDELETE" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ACCOUNT" MODIFY ("USERNAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ACCOUNT" MODIFY ("UUID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ACCOUNT" MODIFY ("CLIENT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ACCOUNT" ADD CONSTRAINT "IDM_ACCOUNT_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_ACCOUNT" ADD CONSTRAINT "IDM_ACCOUNT_UC_1" UNIQUE ("UUID") ENABLE;
 
  ALTER TABLE "IDM_ACCOUNT" ADD CONSTRAINT "IDM_ACCOUNT_UC_2" UNIQUE ("CLIENT_ID", "USERNAME") ENABLE;
  
--------------------------------------------------------
--  Constraints for Table IDM_APPLICATION
--------------------------------------------------------

  ALTER TABLE "IDM_APPLICATION" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPLICATION" MODIFY ("DISPLAYNAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPLICATION" MODIFY ("NAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPLICATION" MODIFY ("CLIENT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPLICATION" ADD CONSTRAINT "IDM_APPLICATION_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_APPLICATION" ADD CONSTRAINT "IDM_APPLICATION_UC_1" UNIQUE ("NAME", "CLIENT_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_APPMETADATA
--------------------------------------------------------

  ALTER TABLE "IDM_APPMETADATA" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPMETADATA" MODIFY ("ACCOUNT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPMETADATA" MODIFY ("APPLICATION_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPMETADATA" ADD CONSTRAINT "IDM_APPMETADATA_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_APPMETADATA" ADD CONSTRAINT "IDM_APPMETADATA_UC_1" UNIQUE ("ACCOUNT_ID", "APPLICATION_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_APPMETADATAVALUE
--------------------------------------------------------

  ALTER TABLE "IDM_APPMETADATAVALUE" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPMETADATAVALUE" MODIFY ("MAP_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPMETADATAVALUE" ADD CONSTRAINT "IDM_APPMETADATAVALUE_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_APPMETADATAVALUE" ADD CONSTRAINT "IDM_APPMETADATAVALUE_UC_1" UNIQUE ("KEY", "MAP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_APPROVAL
--------------------------------------------------------

  ALTER TABLE "IDM_APPROVAL" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPROVAL" MODIFY ("ACCOUNT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPROVAL" MODIFY ("APPLICATION_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_APPROVAL" ADD CONSTRAINT "IDM_APPROVAL_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_APPROVAL" ADD CONSTRAINT "IDM_APPROVAL_UC_1" UNIQUE ("APPLICATION_ID", "ACCOUNT_ID", "APPROVEDVALUESET_ID", "APPROVEDATTRIBUTEVALUE_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_ATTRIBUTE
--------------------------------------------------------

  ALTER TABLE "IDM_ATTRIBUTE" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ATTRIBUTE" MODIFY ("DISPLAYNAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ATTRIBUTE" MODIFY ("MULTIPLE" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ATTRIBUTE" MODIFY ("NAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ATTRIBUTE" MODIFY ("TYPE" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ATTRIBUTE" MODIFY ("WRITEPROTECTED" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ATTRIBUTE" ADD CONSTRAINT "IDM_ATTRIBUTE_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_ATTRIBUTE" ADD CONSTRAINT "IDM_ATTRIBUTE_UC_1" UNIQUE ("NAME", "ATTRIBUTEGROUP_ID","PARENTATTRIBUTE_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_ATTRIBUTEGROUP
--------------------------------------------------------

  ALTER TABLE "IDM_ATTRIBUTEGROUP" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ATTRIBUTEGROUP" MODIFY ("DISPLAYNAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ATTRIBUTEGROUP" MODIFY ("MULTIPLE" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ATTRIBUTEGROUP" MODIFY ("NAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ATTRIBUTEGROUP" MODIFY ("CLIENT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ATTRIBUTEGROUP" ADD CONSTRAINT "IDM_ATTRIBUTEGROUP_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_ATTRIBUTEGROUP" ADD CONSTRAINT "IDM_ATTRIBUTEGROUP_UC_1" UNIQUE ("NAME", "CLIENT_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_CLIENT
--------------------------------------------------------

  ALTER TABLE "IDM_CLIENT" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_CLIENT" MODIFY ("DISPLAYNAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_CLIENT" MODIFY ("NAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_CLIENT" ADD CONSTRAINT "IDM_CLIENT_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_CLIENT" ADD CONSTRAINT "IDM_CLIENT_UC_1" UNIQUE ("NAME") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_CLIENTPROPERTYMAP
--------------------------------------------------------

  ALTER TABLE "IDM_CLIENTPROPERTYMAP" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAP" MODIFY ("CLIENT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAP" ADD CONSTRAINT  "IDM_CLIENTPROPERTYMAP_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAP" ADD CONSTRAINT  "IDM_CLIENTPROPTERTYMAP_UC_1" UNIQUE ("CLIENT_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_CLIENTPROPERTYMAPENTRY
--------------------------------------------------------

  ALTER TABLE "IDM_CLIENTPROPERTYMAPENTRY" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAPENTRY" MODIFY ("MAP_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAPENTRY" ADD CONSTRAINT "IDM_CLIENTPROP_MAPENTRY_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_CLIENTPROPERTYMAPENTRY" ADD CONSTRAINT "IDM_CLIENTPROP_MAPENTRY_UC_1" UNIQUE ("KEY", "MAP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_GROUP
--------------------------------------------------------

  ALTER TABLE "IDM_GROUP" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_GROUP" MODIFY ("DISPLAYNAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_GROUP" MODIFY ("NAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_GROUP" MODIFY ("WRITEPROTECTED" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_GROUP" MODIFY ("CLIENT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_GROUP" ADD CONSTRAINT "IDM_GROUP_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_GROUP" ADD CONSTRAINT "IDM_GROUP_UC_1" UNIQUE ("CLIENT_ID", "NAME") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_GROUPASSIGNMENT
--------------------------------------------------------

  ALTER TABLE "IDM_GROUPASSIGNMENT" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_GROUPASSIGNMENT" MODIFY ("ACCOUNT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_GROUPASSIGNMENT" MODIFY ("GROUP_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_GROUPASSIGNMENT" ADD CONSTRAINT "IDM_GROUPASSIGNMENT_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_GROUPASSIGNMENT" ADD CONSTRAINT "IDM_GROUPASSIGNMENT_UC_1" UNIQUE ("ACCOUNT_ID", "GROUP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_INTERNALROLEASSIGNMENT
--------------------------------------------------------

  ALTER TABLE "IDM_INTERNALROLEASSIGNMENT" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_INTERNALROLEASSIGNMENT" ADD CONSTRAINT "IDM_IROLEASSIGNMENT_PK" PRIMARY KEY ("ID") ENABLE;

  ALTER TABLE "IDM_INTERNALROLEASSIGNMENT" ADD CONSTRAINT "IDM_IROLEASSIGNMENT_UC_1" UNIQUE ("ACCOUNT_ID", "TYPE") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_INTERNALROLESCOPE
--------------------------------------------------------

  ALTER TABLE "IDM_INTERNALROLESCOPE" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_INTERNALROLESCOPE" MODIFY ("GROUP_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_INTERNALROLESCOPE" MODIFY ("INTERNALROLEASSIGNMENT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_INTERNALROLESCOPE" ADD CONSTRAINT "IDM_INTERNALROLESCOPE_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_INTERNALROLESCOPE" ADD CONSTRAINT "IDM_INTERNALROLESCOPE_UC_1" UNIQUE ("INTERNALROLEASSIGNMENT_ID", "GROUP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_ORGUNIT
--------------------------------------------------------

  ALTER TABLE "IDM_ORGUNIT" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ORGUNIT" MODIFY ("NAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ORGUNIT" MODIFY ("GROUP_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ORGUNIT" ADD CONSTRAINT "IDM_ORGUNIT_PK" PRIMARY KEY ("ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_ORGUNITMAP
--------------------------------------------------------

  ALTER TABLE "IDM_ORGUNITMAP" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ORGUNITMAP" MODIFY ("ORGUNIT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ORGUNITMAP" ADD CONSTRAINT "IDM_ORGUNITMAP_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_ORGUNITMAP" ADD CONSTRAINT "IDM_ORGUNITMAP_UC_1" UNIQUE ("ORGUNIT_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_ORGUNITMAPENTRY
--------------------------------------------------------

  ALTER TABLE "IDM_ORGUNITMAPENTRY" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ORGUNITMAPENTRY" MODIFY ("MAP_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ORGUNITMAPENTRY" ADD CONSTRAINT "IDM_ORGUNITMAPENTRY_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_ORGUNITMAPENTRY" ADD CONSTRAINT "IDM_ORGUNITMAPENTRY_UC_1" UNIQUE ("KEY", "MAP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_ROLE
--------------------------------------------------------

  ALTER TABLE "IDM_ROLE" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ROLE" MODIFY ("DISPLAYNAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ROLE" MODIFY ("NAME" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ROLE" MODIFY ("APPLICATION_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ROLE" ADD CONSTRAINT "IDM_ROLE_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_ROLE" ADD CONSTRAINT "IDM_ROLE_UC_1" UNIQUE ("NAME", "APPLICATION_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_ROLEASSIGNMENT
--------------------------------------------------------

  ALTER TABLE "IDM_ROLEASSIGNMENT" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ROLEASSIGNMENT" MODIFY ("ROLE_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_ROLEASSIGNMENT" ADD CONSTRAINT "IDM_ROLEASSIGNMENT_PK" PRIMARY KEY ("ID") ENABLE;
 
  ALTER TABLE "IDM_ROLEASSIGNMENT" ADD CONSTRAINT "IDM_ROLEASSIGNMENT_UC_1" UNIQUE ("ACCOUNT_ID", "ROLE_ID", "SCOPE", "GROUP_ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_VALUE
--------------------------------------------------------

  ALTER TABLE "IDM_VALUE" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_VALUE" MODIFY ("ISLOCKED" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_VALUE" MODIFY ("ATTRIBUTE_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_VALUE" MODIFY ("VALUESET_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_VALUE" ADD CONSTRAINT "IDM_VALUE_PK" PRIMARY KEY ("ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table IDM_VALUESET
--------------------------------------------------------

  ALTER TABLE "IDM_VALUESET" MODIFY ("ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_VALUESET" MODIFY ("ISLOCKED" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_VALUESET" MODIFY ("ACCOUNT_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_VALUESET" MODIFY ("ATTRIBUTEGROUP_ID" NOT NULL ENABLE);
 
  ALTER TABLE "IDM_VALUESET" ADD CONSTRAINT "IDM_VALUESET_PK" PRIMARY KEY ("ID") ENABLE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_ACCOUNT
--------------------------------------------------------

  ALTER TABLE "IDM_ACCOUNT" ADD CONSTRAINT "FK_ACCOUNT_TO_CLIENT" FOREIGN KEY ("CLIENT_ID")
	  REFERENCES "IDM_CLIENT" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_APPLICATION
--------------------------------------------------------

  ALTER TABLE "IDM_APPLICATION" ADD CONSTRAINT "FK_APPLICATION_TO_CLIENT" FOREIGN KEY ("CLIENT_ID")
	  REFERENCES "IDM_CLIENT" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_APPMETADATA
--------------------------------------------------------

  ALTER TABLE "IDM_APPMETADATA" ADD CONSTRAINT "FK_AMD_TO_ACCOUNT" FOREIGN KEY ("ACCOUNT_ID")
	  REFERENCES "IDM_ACCOUNT" ("ID") ENABLE;
 
  ALTER TABLE "IDM_APPMETADATA" ADD CONSTRAINT "FK_AMD_TO_APPLICATION" FOREIGN KEY ("APPLICATION_ID")
	  REFERENCES "IDM_APPLICATION" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_APPMETADATAVALUE
--------------------------------------------------------

  ALTER TABLE "IDM_APPMETADATAVALUE" ADD CONSTRAINT "FK_AMDVALUE_TO_AMD" FOREIGN KEY ("MAP_ID")
	  REFERENCES "IDM_APPMETADATA" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_APPROVAL
--------------------------------------------------------

  ALTER TABLE "IDM_APPROVAL" ADD CONSTRAINT "FK_APPROVAL_TO_ACCOUNT" FOREIGN KEY ("ACCOUNT_ID")
	  REFERENCES "IDM_ACCOUNT" ("ID") ENABLE;
 
  ALTER TABLE "IDM_APPROVAL" ADD CONSTRAINT "FK_APPROVAL_TO_VALUESET" FOREIGN KEY ("APPROVEDVALUESET_ID")
	  REFERENCES "IDM_VALUESET" ("ID") ENABLE;
 
  ALTER TABLE "IDM_APPROVAL" ADD CONSTRAINT "FK_APPROVAL_TO_VALUE" FOREIGN KEY ("APPROVEDATTRIBUTEVALUE_ID")
	  REFERENCES "IDM_VALUE" ("ID") ENABLE;
 
  ALTER TABLE "IDM_APPROVAL" ADD CONSTRAINT "FK_APPROVAL_TO_APPLICATION" FOREIGN KEY ("APPLICATION_ID")
	  REFERENCES "IDM_APPLICATION" ("ID") ENABLE;
 
  ALTER TABLE "IDM_APPROVAL" ADD CONSTRAINT "FK_APPROVAL_TO_APPROVAL" FOREIGN KEY ("ID")
	  REFERENCES "IDM_APPROVAL" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_ATTRIBUTE
--------------------------------------------------------

  ALTER TABLE "IDM_ATTRIBUTE" ADD CONSTRAINT "FK_ATTRIBUTE_TO_ATTRIBUTEGROUP" FOREIGN KEY ("ATTRIBUTEGROUP_ID")
	  REFERENCES "IDM_ATTRIBUTEGROUP" ("ID") ENABLE;
 
  ALTER TABLE "IDM_ATTRIBUTE" ADD CONSTRAINT "FK_ATTRIBUTE_TO_ATTRIBUTE" FOREIGN KEY ("PARENTATTRIBUTE_ID")
	  REFERENCES "IDM_ATTRIBUTE" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_ATTRIBUTEGROUP
--------------------------------------------------------

  ALTER TABLE "IDM_ATTRIBUTEGROUP" ADD CONSTRAINT "FK_ATTRIBUTEGROUP_TO_CLIENT" FOREIGN KEY ("CLIENT_ID")
	  REFERENCES "IDM_CLIENT" ("ID") ENABLE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_CLIENTPROPERTYMAP
--------------------------------------------------------

  ALTER TABLE "IDM_CLIENTPROPERTYMAP" ADD CONSTRAINT "FK_CPROPMAP_TO_CLIENT" FOREIGN KEY ("CLIENT_ID")
	  REFERENCES "IDM_CLIENT" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_CLIENTPROPERTYMAPENTRY
--------------------------------------------------------

  ALTER TABLE "IDM_CLIENTPROPERTYMAPENTRY" ADD CONSTRAINT "FK_CPROPMAPENTRY_TO_CPROPMAP" FOREIGN KEY ("MAP_ID")
	  REFERENCES "IDM_CLIENTPROPERTYMAP" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_GROUP
--------------------------------------------------------

  ALTER TABLE "IDM_GROUP" ADD CONSTRAINT "FK_GROUP_TO_CLIENT" FOREIGN KEY ("CLIENT_ID")
	  REFERENCES "IDM_CLIENT" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_GROUPASSIGNMENT
--------------------------------------------------------

  ALTER TABLE "IDM_GROUPASSIGNMENT" ADD CONSTRAINT "FK_GROUPASSIGNMENT_TO_ACCOUNT" FOREIGN KEY ("ACCOUNT_ID")
	  REFERENCES "IDM_ACCOUNT" ("ID") ENABLE;
 
  ALTER TABLE "IDM_GROUPASSIGNMENT" ADD CONSTRAINT "FK_GROUPASSIGNMENT_TO_GROUP" FOREIGN KEY ("GROUP_ID")
	  REFERENCES "IDM_GROUP" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_INTERNALROLEASSIGNMENT
--------------------------------------------------------

  ALTER TABLE "IDM_INTERNALROLEASSIGNMENT" ADD CONSTRAINT "FK_IROLEASSIGNMENT_TO_ACCOUNT" FOREIGN KEY ("ACCOUNT_ID")
	  REFERENCES "IDM_ACCOUNT" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_INTERNALROLESCOPE
--------------------------------------------------------

  ALTER TABLE "IDM_INTERNALROLESCOPE" ADD CONSTRAINT "FK_IRSCOPE_TO_IROLEASSIGNMENT" FOREIGN KEY ("INTERNALROLEASSIGNMENT_ID")
	  REFERENCES "IDM_INTERNALROLEASSIGNMENT" ("ID") ENABLE;
 
  ALTER TABLE "IDM_INTERNALROLESCOPE" ADD CONSTRAINT "FK_IRSCOPE_TO_GROUP" FOREIGN KEY ("GROUP_ID")
	  REFERENCES "IDM_GROUP" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_ORGUNIT
--------------------------------------------------------

  ALTER TABLE "IDM_ORGUNIT" ADD CONSTRAINT "FK_ORGUNIT_TO_GROUP" FOREIGN KEY ("GROUP_ID")
	  REFERENCES "IDM_GROUP" ("ID") ENABLE;

--------------------------------------------------------
--  Ref Constraints for Table IDM_ORGUNITMAP
--------------------------------------------------------

  ALTER TABLE "IDM_ORGUNITMAP" ADD CONSTRAINT "FK_OUMAP_TO_OU" FOREIGN KEY ("ORGUNIT_ID")
	  REFERENCES "IDM_ORGUNIT" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_ORGUNITMAPENTRY
--------------------------------------------------------

  ALTER TABLE "IDM_ORGUNITMAPENTRY" ADD CONSTRAINT "FK_OUMAPENTRY_TO_OUMAP" FOREIGN KEY ("MAP_ID")
	  REFERENCES "IDM_ORGUNITMAP" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_ROLE
--------------------------------------------------------

  ALTER TABLE "IDM_ROLE" ADD CONSTRAINT "FK_ROLE_TO_APPLICATION" FOREIGN KEY ("APPLICATION_ID")
	  REFERENCES "IDM_APPLICATION" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_ROLEASSIGNMENT
--------------------------------------------------------

  ALTER TABLE "IDM_ROLEASSIGNMENT" ADD CONSTRAINT "FK_ROLEASSIGNMENT_TO_ACCOUNT" FOREIGN KEY ("ACCOUNT_ID")
	  REFERENCES "IDM_ACCOUNT" ("ID") ENABLE;
 
  ALTER TABLE "IDM_ROLEASSIGNMENT" ADD CONSTRAINT "FK_ROLEASSIGNMENT_TO_GROUP" FOREIGN KEY ("GROUP_ID")
	  REFERENCES "IDM_GROUP" ("ID") ENABLE;
 
  ALTER TABLE "IDM_ROLEASSIGNMENT" ADD CONSTRAINT "FK_ROLEASSIGNMENT_TO_ROLE" FOREIGN KEY ("ROLE_ID")
	  REFERENCES "IDM_ROLE" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_VALUE
--------------------------------------------------------

  ALTER TABLE "IDM_VALUE" ADD CONSTRAINT "FK_VALUE_TO_VALUESET" FOREIGN KEY ("VALUESET_ID")
	  REFERENCES "IDM_VALUESET" ("ID") ENABLE;
 
  ALTER TABLE "IDM_VALUE" ADD CONSTRAINT "FK_VALUE_TO_ATTRIBUTE" FOREIGN KEY ("ATTRIBUTE_ID")
	  REFERENCES "IDM_ATTRIBUTE" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table IDM_VALUESET
--------------------------------------------------------

  ALTER TABLE "IDM_VALUESET" ADD CONSTRAINT "FK_VALUESET_TO_ACCOUNT" FOREIGN KEY ("ACCOUNT_ID")
	  REFERENCES "IDM_ACCOUNT" ("ID") ENABLE;
 
  ALTER TABLE "IDM_VALUESET" ADD CONSTRAINT "FK_VALUESET_TO_ATTRIBUTEGROUP" FOREIGN KEY ("ATTRIBUTEGROUP_ID")
	  REFERENCES "IDM_ATTRIBUTEGROUP" ("ID") ENABLE;


-------------------------------------------------------
-- Create Views
-------------------------------------------------------


--------------------------------------------------------
--  DDL for View IDM_CURRENTTIMESTAMP
--------------------------------------------------------

  CREATE OR REPLACE VIEW "IDM_CURRENTTIMESTAMP" ("TIMESTAMP") AS 
  SELECT  localtimestamp as timestamp FROM dual;

--------------------------------------------------------
--  DDL for View IDM_USER
--------------------------------------------------------

  CREATE OR REPLACE VIEW "IDM_USER" ("ID", "CREATIONDATE", "DEACTIVATIONDATE", "DISPLAYNAME", "SOFTDELETE", "USERNAME", "UUID", "CLIENT_ID", "FIRSTNAME", "LASTNAME") AS 
  SELECT a.*,v1.value AS firstname, v2.value AS lastname FROM idm_account a 
  JOIN idm_valueSet vs on a.id = vs.account_id 
  JOIN idm_value v1 on v1.valueset_id = vs.id 
  JOIN idm_attribute a1 on v1.attribute_id = a1.id AND a1.name = 'firstname'
  JOIN idm_value v2 on v2.valueset_id = vs.id
  JOIN idm_attribute a2 on v2.attribute_id = a2.id AND a2.name = 'lastname'
  JOIN idm_attributegroup ag on a2.attributegroup_id = ag.id AND ag.name = 'personalData';


--------------------------------------------------------
--  DDL for View IDM_ROLEUSER
--------------------------------------------------------

  CREATE OR REPLACE VIEW "IDM_ROLEUSER" ("ID", "CREATIONDATE", "DEACTIVATIONDATE", "DISPLAYNAME", "SOFTDELETE", "USERNAME", "UUID", "CLIENT_ID", "FIRSTNAME", "LASTNAME", "DIRECTASSIGNMENT", "ROLE_ID", "SCOPE") AS 
  SELECT DISTINCT u.*, 0 AS directAssignment, rA.role_id, ra.scope 
  FROM idm_user u 
  JOIN idm_groupassignment gA ON u.id = ga.account_id 
  JOIN idm_group group1 ON ga.group_id = group1.id 
  JOIN idm_roleassignment rA on group1.id = ra.group_id WHERE u.id NOT IN (SELECT DISTINCT uI.id FROM idm_user uI JOIN idm_roleassignment raI ON uI.id = raI.account_id)  
  UNION
  SELECT DISTINCT u.* , 1 AS directAssignment, ra.role_id, ra.scope FROM idm_user u JOIN idm_roleassignment ra ON u.id = ra.account_id;






---------------------------------------------------
--   DATA FOR TABLE IDM_CLIENT
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_CLIENT
Insert into IDM_CLIENT (ID,CERTIFICATIONDATA,DISPLAYNAME,NAME) values (1,null,'PUT_CLIENT_DISPLAYNAME_HERE','PUT_WEB_ID_HERE');

---------------------------------------------------
--   END DATA FOR TABLE IDM_CLIENT
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE IDM_CLIENTPROPERTYMAP
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_CLIENTPROPERTYMAP
INSERT into IDM_CLIENTPROPERTYMAP (ID, CLIENT_ID) VALUES (32,1);
---------------------------------------------------
--   END DATA FOR TABLE IDM_CLIENTPROPERTYMAP
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_CLIENTPROPERTYMAPENTRY
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_CLIENTPROPERTYMAPENTRY
-- opensso settings -- 
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (33,32,'openSso.serviceUrl','http://localhost:8080/opensso');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (34,32,'openSso.adminLogin','amAdmin');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (35,32,'openSso.adminPassword','');

-- cron job settings -- 
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (36,32,'system.dailyJobExecutionTime','02:00');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (37,32,'system.deactivatedAccountLifetimeInDays','30');

-- registration settings -- 
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (38,32,'registration.termsRequired.enabled','false');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (39,32,'registration.termsRequired.url','http://');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (40,32,'registration.displayNameGeneration.enabled','false');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (42,32,'registration.displayNameGeneration.pattern','%firstname% %lastname%');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (43,32,'registration.alternativeLink.enabled','false');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (44,32,'registration.alternativeLink.url','http://');

-- security settings --
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (45,32,'security.password.pattern','^(?=.{8,74})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (46,32,'security.allowEmailExistenceHints.enabled','false');

-- ldap settings -- 
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (47,32,'ldapSync.enabled','false');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (48,32,'ldapSync.hostAndPort','@ldapSync.host@:@ldapSync.port@');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (49,32,'ldapSync.userDN','CN=Administrator,CN=Users,dc=@ldapSync.baseDN@');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (50,32,'ldapSync.password','@ldapSync.password@');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (51,32,'ldapSync.baseDN','@ldapSync.baseDN@');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (52,32,'ldapSync.searchDN','cn=Users');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (53,32,'ldapSync.searchFilter','(&(objectClass=person)(cn=LOGINNAME))');
--? INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (60,32,'LOGINNAME','');
-- search properties --
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (54,32,'search.pagingLimit','10');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (55,32,'search.fullPrivateDataSearch.enabled','true');

INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (56,32,'registration.successLink.enabled','false');
INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (57,32,'registration.successLink.url','http://');


-- ? INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (58,32,'internal.lastUpdateUsers','');
-- other properties -- 
-- ? INSERT INTO IDM_CLIENTPROPERTYMAPENTRY (ID, MAP_ID, KEY, VALUE) VALUES (59,32,'other.allowDeleteUser.enabled','');


---------------------------------------------------
--   END DATA FOR TABLE IDM_CLIENTPROPERTYMAPENTRY
---------------------------------------------------



---------------------------------------------------
--   DATA FOR TABLE IDM_ATTRIBUTEGROUP
--   FILTER = none used
---------------------------------------------------
/* default attribute groups for default client*/
Insert into IDM_ATTRIBUTEGROUP (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,CLIENT_ID) values (2,'Name',1,0,'personalData',1);
Insert into IDM_ATTRIBUTEGROUP (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,CLIENT_ID) values (14,'Meldeadresse',2,0,'primaryAddress',1);
Insert into IDM_ATTRIBUTEGROUP (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,CLIENT_ID) values (21,'Kontaktdaten',3,0,'contactData',1);
Insert into IDM_ATTRIBUTEGROUP (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,CLIENT_ID) values (25,'Weitere Adressen',4,1,'furtherAddresses',1);

---------------------------------------------------
--   END DATA FOR TABLE IDM_ATTRIBUTEGROUP
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE IDM_ATTRIBUTE
--   FILTER = none used
---------------------------------------------------
/* attributes of attribute group 'personalData' */
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (3,'Anrede',1,0,'salutation',7,2,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED, ADDITIONALTYPECONSTRAINT) values (4,'Vorname',2,0,'firstname',6,2,1,q'([\p{L}-'`´ ]{2,40})');
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED, ADDITIONALTYPECONSTRAINT) values (5,'Nachname',3,0,'lastname',6,2,1,q'([\p{L}-'`´ ]{2,40})');
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (6,'Geburtsdatum',4,0,'birthdate',3,2,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (7,'Geschlecht',5,0,'gender',7,2,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (8,'Nationalität',6,0,'nationality',7,2,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (9,'Herr',1,0,'mr',0,3,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (10,'Frau',2,0,'mrs',0,3,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (11,'Fräulein',3,0,'miss',0,3,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (12,'männlich',1,0,'male',0,7,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (13,'weiblich',2,0,'female',0,7,0);

/* attributes of attribute group 'primaryAddress' */
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (15,'Straße',1,0,'streetName',0,14,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (16,'Hausnummer',2,0,'streetNumber',0,14,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (17,'Land',3,0,'country',7,14,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (18,'PLZ',4,0,'zipCode',0,14,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (19,'Ort',5,0,'city',0,14,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (20,'Bundesland',6,0,'state',0,14,0);

/* attributes of attribute group 'contactData' */
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (22,'Telefon',1,1,'phone',0,21,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (23,'Telefax',2,1,'fax',0,21,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (24,'Mobil',3,1,'mobile',0,21,0);

/* attributes of attribute group 'furtherAddresses' */
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (26,'Straße',1,0,'streetName',0,25,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (27,'Hausnummer',2,0,'streetNumber',0,25,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (28,'Land',3,0,'country',7,25,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (29,'PLZ',4,0,'zipCode',0,25,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (30,'Ort',5,0,'city',0,25,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,ATTRIBUTEGROUP_ID, WRITEPROTECTED) values (31,'Bundesland',6,0,'state',0,25,0);


/* subattributes for attribute 'nationality' of attribute group 'personal data' */
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (32,'deutsch',1,0,'DE',0,8,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (33,'britisch',2,0,'GB',0,8,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (34,'französisch',3,0,'FR',0,8,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (35,'polnisch',4,0,'PL',0,8,0);
/* subattributes for attribute 'country' of attribute group 'primaryAddress' */
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (36,'Deutschland',1,0,'DE',0,17,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (37,'Frankreich',2,0,'FR',0,17,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (38,'Polen',3,0,'PL',0,17,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (39,'Vereinigtes Königreich',4,0,'GB',0,17,0);
/*subattributes for attribute 'country' of attribute group 'furtherAddresses' */
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (40,'Deutschland',1,0,'DE',0,28,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (41,'Frankreich',2,0,'FR',0,28,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (42,'Polen',3,0,'PL',0,28,0);
Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (43,'Vereinigtes Königreich',4,0,'GB',0,28,0);
---------------------------------------------------
--   END DATA FOR TABLE IDM_ATTRIBUTE
---------------------------------------------------



---------------------------------------------------
--   DATA FOR TABLE IDM_ACCOUNT
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_ACCOUNT

---------------------------------------------------
--   END DATA FOR TABLE IDM_ACCOUNT
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_APPLICATION
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_APPLICATION


---------------------------------------------------
--   END DATA FOR TABLE IDM_APPLICATION
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_APPMETADATA
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_APPMETADATA

---------------------------------------------------
--   END DATA FOR TABLE IDM_APPMETADATA
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_APPMETADATAVALUE
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_APPMETADATAVALUE

---------------------------------------------------
--   END DATA FOR TABLE IDM_APPMETADATAVALUE
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_APPROVAL
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_APPROVAL

---------------------------------------------------
--   END DATA FOR TABLE IDM_APPROVAL
---------------------------------------------------





---------------------------------------------------
--   DATA FOR TABLE IDM_GROUP
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_GROUP

---------------------------------------------------
--   END DATA FOR TABLE IDM_GROUP
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_GROUPASSIGNMENT
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_GROUPASSIGNMENT

---------------------------------------------------
--   END DATA FOR TABLE IDM_GROUPASSIGNMENT
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_INTERNALROLEASSIGNMENT
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_INTERNALROLEASSIGNMENT

---------------------------------------------------
--   END DATA FOR TABLE IDM_INTERNALROLEASSIGNMENT
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_INTERNALROLESCOPE
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_INTERNALROLESCOPE

---------------------------------------------------
--   END DATA FOR TABLE IDM_INTERNALROLESCOPE
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_ORGUNIT
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_ORGUNIT

---------------------------------------------------
--   END DATA FOR TABLE IDM_ORGUNIT
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_ORGUNITMAP
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_ORGUNITMAP

---------------------------------------------------
--   END DATA FOR TABLE IDM_ORGUNITMAP
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_ORGUNITMAPENTRY
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_ORGUNITMAPENTRY

---------------------------------------------------
--   END DATA FOR TABLE IDM_ORGUNITMAPENTRY
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_ROLE
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_ROLE

---------------------------------------------------
--   END DATA FOR TABLE IDM_ROLE
---------------------------------------------------


---------------------------------------------------
--   DATA FOR TABLE IDM_ROLEASSIGNMENT
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_ROLEASSIGNMENT

---------------------------------------------------
--   END DATA FOR TABLE IDM_ROLEASSIGNMENT
---------------------------------------------------

---------------------------------------------------
--   DATA FOR TABLE IDM_VALUESET
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_VALUESET

---------------------------------------------------
--   END DATA FOR TABLE IDM_VALUESET
---------------------------------------------------



---------------------------------------------------
--   DATA FOR TABLE IDM_VALUE
--   FILTER = none used
---------------------------------------------------
REM INSERTING into IDM_VALUE

---------------------------------------------------
--   END DATA FOR TABLE IDM_VALUE
---------------------------------------------------


