------------------------------------------------------------------------
--- IdM oracle database schema update script - created 19. Feb. 2010
--- for updating IdM backend version [1.1.9 - 1.3.0] to 1.3.1
------------------------------------------------------------------------

ALTER TABLE "IDM_ATTRIBUTE" MODIFY ("ATTRIBUTEGROUP_ID" NULL );
ALTER TABLE "IDM_ATTRIBUTE" ADD "PARENTATTRIBUTE_ID" NUMBER(19,0);
ALTER TABLE "IDM_ATTRIBUTE" ADD CONSTRAINT "FK_ATTRIBUTE_TO_ATTRIBUTE" FOREIGN KEY ("PARENTATTRIBUTE_ID") REFERENCES "IDM_ATTRIBUTE" ("ID") ENABLE;

-----------------------------------------------------------------------------------------------------
--- Update bug fix script - 18.02.2010 
--- This sql script fixes two problems:
--- 1) Initial created but deactivated users have no deactivation date which causes exceptions.
--- 2) Duplicate backslash in the password policy pattern requires that a password must contain a backslash.
--- 3) Updating attribute types and validation constraints for firstname and lastname values
-----------------------------------------------------------------------------------------------------

UPDATE idm_account SET deactivationdate = sysdate WHERE softdelete = 1 AND deactivationdate is null;

UPDATE idm_clientpropertymapentry cpm SET cpm.value = '^(?=.{8,74})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$'
WHERE cpm.key = 'security.password.pattern';

UPDATE IDM_ATTRIBUTE SET ADDITIONALTYPECONSTRAINT = q'([\p{L}-'`´ ]{2,40})', TYPE = 6 WHERE id IN 
(SELECT b.id 
 FROM IDM_ATTRIBUTE b 
 JOIN IDM_ATTRIBUTEGROUP ag ON b.attributegroup_id = ag.id 
 WHERE (b.name = 'firstname' OR b.name = 'lastname') AND ag.name = 'personalData');
 