------------------------------------------------------------------
------ Add additional attribute to realize locked attributes -----
------------------------------------------------------------------ 
ALTER TABLE "IDM_ATTRIBUTE" DROP COLUMN "WRITEPROTECTED";
ALTER TABLE "IDM_ATTRIBUTE" ADD ("WRITEPROTECTED" NUMBER(1,0) DEFAULT 0 NOT NULL ENABLE);
