UPDATE IDM_ATTRIBUTE SET ADDITIONALTYPECONSTRAINT = q'([\p{L}-'`´ ]{2,40})', TYPE = 6 WHERE id IN 
(SELECT b.id 
 FROM IDM_ATTRIBUTE b 
 JOIN IDM_ATTRIBUTEGROUP ag ON b.attributegroup_id = ag.id 
 WHERE (b.name = 'firstname' OR b.name = 'lastname') AND ag.name = 'personalData');
