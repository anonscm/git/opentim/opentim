UPDATE IDM_ATTRIBUTE SET ADDITIONALTYPECONSTRAINT = NULL, TYPE = 0 WHERE id IN 
(SELECT b.id 
 FROM IDM_ATTRIBUTE b 
 JOIN IDM_ATTRIBUTEGROUP ag ON b.attributegroup_id = ag.id 
 WHERE (b.name = 'firstname' OR b.name = 'lastname') AND ag.name = 'personalData');
