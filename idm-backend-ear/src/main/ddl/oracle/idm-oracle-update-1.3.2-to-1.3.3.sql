/* IdM oracle database schema update script - created 19. Feb. 2010 for updating IdM backend version 1.3.1 to 1.3.3
 * 
 * This script modifies the following default idm-attributes of each client:
 * - 'salutation' of attribute group 'personal data' : switched to attribute type 'SELECTLIST' and some subattributes added.
 * - 'country' of attribut group 'primaryAddress' and 'furtherAddresses': switched to attribute type 'SELECTLIST' and some subattributes added.
 * - attributes 'gender' and 'nationality' in attribute group 'personal Data' with some subattributes added.
 * - Unique constraint 'IDM_ATTRIBUTE_UC_1' changed so that the combination of 'name','parentattribute_id' and 'attributegroup_id' has to be unique.
*/


/**
 * Change unique constraint "IDM_ATTRIBUTE_UC_1".
 */
  ---if there is no constraint named "IDM_ATTRIBUTE_UC_1" (setup version was <= 1.1.6 ) uncomment the following line and comment the line after that.
  ---ALTER TABLE IDM_ATTRIBUTE DROP UNIQUE (name, attributegroup_id);
  ALTER TABLE "IDM_ATTRIBUTE" DROP CONSTRAINT "IDM_ATTRIBUTE_UC_1";
  ALTER TABLE "IDM_ATTRIBUTE" ADD CONSTRAINT "IDM_ATTRIBUTE_UC_1" UNIQUE ("NAME", "ATTRIBUTEGROUP_ID", "PARENTATTRIBUTE_ID") ENABLE;

DECLARE

  /* Output variable to hold the result of the query: */
  cid IDM_ATTRIBUTE.id%TYPE;
  
  
  /* --- Cursor declaration: --- */
  
  /* cursor to iterate through all 'salutation' attributes*/
  CURSOR attributeIdCursor IS SELECT attr.id FROM idm_attribute attr JOIN idm_attributegroup attrGroup ON attr.attributegroup_id = attrGroup.id WHERE attr.name = 'salutation' AND attrGroup.name = 'personalData' FOR UPDATE;
  
  /* cursor to iterate through all 'personalData' attributes */
  CURSOR attributeGroupIdCursor IS SELECT attrGroup.id FROM IDM_ATTRIBUTEGROUP attrGroup WHERE attrGroup.name = 'personalData' FOR UPDATE;
  
  /* cursor to iterate through all 'gender' attributes */
  CURSOR attributeGenderIdCursor IS SELECT attr.id FROM IDM_ATTRIBUTE attr JOIN idm_attributegroup attrGroup ON attr.attributegroup_id = attrgroup.id WHERE attrGroup.name = 'personalData' AND attr.name = 'gender' FOR UPDATE;
  
  /* cursor to iterate through all 'nationality' attributes*/
  CURSOR attributeNationalityIdCursor IS SELECT attr.id FROM IDM_ATTRIBUTE attr JOIN idm_attributegroup attrGroup ON attr.attributegroup_id = attrgroup.id WHERE attrGroup.name = 'personalData' AND attr.name = 'nationality' FOR UPDATE;

  /* cursor to iterate through all 'country' attributes of attribute group 'primaryAddress' AND 'furtherAddresses' */
  CURSOR attributeCountryIdCursor IS SELECT attr.id FROM IDM_ATTRIBUTE attr JOIN idm_attributegroup attrGroup ON attr.attributegroup_id = attrgroup.id WHERE (attrGroup.name = 'primaryAddress' OR attrGroup.name = 'furtherAddresses') AND attr.name = 'country' FOR UPDATE;

  
/* --- Modify data base state --- */
BEGIN

  /* Rename key of a clientpropertymapentry */
  UPDATE IDM_CLIENTPROPERTYMAPENTRY cpm SET cpm.key = 'security.allowEmailExistenceHints.enabled' WHERE cpm.key = 'security.requestPasswordFailMessage.enabled'; 

  /* --- Update the structure and data base state of the attributes 'salutation' that are associated to an attribute group with name 'personalData' --- */ 
  OPEN attributeIdCursor;
  LOOP
    FETCH attributeIdCursor INTO cid;
    EXIT WHEN attributeIdCursor%NOTFOUND;
    
	---- UPDATE the attribute type of the current attribute with name 'salutation'
    UPDATE IDM_ATTRIBUTE a SET a.type = 7 WHERE a.id = cid;
     
    ---- Create subattributes for each existing 'salutation' attribute ---
    Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE, parentattribute_id ,WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'Herr',1,0,'mr',0,cid,0);
    Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE, parentattribute_id ,WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'Frau',2,0,'mrs',0,cid,0);
    Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE, parentattribute_id ,WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'Fräulein',3,0,'miss',0,cid,0);
    
    --- Set every existing value that is associated to that salution attribute that does not match to a value of the new subattributes to NULL ---
    UPDATE IDM_VALUE v SET v.value = NULL WHERE v.attribute_id = cid AND NOT (v.value = 'mr' OR v.value = 'mrs' OR v.value = 'miss');
  END LOOP;
  CLOSE attributeIdCursor;


  /* --- Now add to new attributes to all existing attribute groups with name '' ---*/
  OPEN attributeGroupIdCursor;
  LOOP
    FETCH attributeGroupIdCursor INTO cid;
    EXIT WHEN attributeGroupIdCursor%NOTFOUND;
    
    ---- Insert attributes 'gender' and 'attribute group' to current attribute group ---
    INSERT INTO IDM_ATTRIBUTE (ID, DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE, ATTRIBUTEGROUP_ID,WRITEPROTECTED) VALUES (HIBERNATE_SEQUENCE.NEXTVAL,'Geschlecht',5,0,'gender',7,cid,0);
    INSERT INTO IDM_ATTRIBUTE (ID, DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE, ATTRIBUTEGROUP_ID,WRITEPROTECTED) VALUES (HIBERNATE_SEQUENCE.NEXTVAL,'Nationalität',6,0,'nationality',7,cid,0);     
  END LOOP;
  CLOSE attributeGroupIdCursor;


  /* --- Now add two subattributes to each attribute 'gender' --- */ 
  OPEN attributeGenderIdCursor;
  LOOP
  	FETCH attributeGenderIdCursor INTO cid;
    EXIT WHEN attributeGenderIdCursor%NOTFOUND;
  
    --- Insert subattributes 'male' and 'female' to current attribute 'gender' --- 
    Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE, parentattribute_id ,WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'männlich',1,0,'male',0,cid,0);
    Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE, parentattribute_id ,WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'weiblich',2,0,'female',0,cid,0);
  END LOOP;
  CLOSE attributeGenderIdCursor;

  
  /* --- Now add one subattribute to each attribute 'nationality' ---*/
  OPEN attributeNationalityIdCursor;
  LOOP
    FETCH attributeNationalityIdCursor INTO cid;
    EXIT WHEN attributeNationalityIdCursor%NOTFOUND;
    
    ---- UPDATE the subattribute 'DE' type of the current attribute with name 'nationality'
	Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'deutsch',1,0,'DE',0,cid,0);
	Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'britisch',2,0,'GB',0,cid,0);
	Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'französisch',3,0,'FR',0,cid,0);
	Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID,WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'polnisch',4,0,'PL',0,cid,0);
    
  END LOOP;
  CLOSE attributeNationalityIdCursor;


  /* --- Update the structure and data base state of the attributes 'country' that are associated to an attribute group with name 'primaryAddress' or name 'furtherAddresses' --- */ 
  OPEN attributeCountryIdCursor;
  LOOP
    FETCH attributeCountryIdCursor INTO cid;
  	EXIT WHEN attributeCountryIdCursor%NOTFOUND;
    
	---- UPDATE the attribute type of the current attribute with name 'country'
    UPDATE IDM_ATTRIBUTE a SET a.type = 7 WHERE a.id = cid;
     
    ---- Create subattributes for each existing 'country' attribute in attribute groups 'primaryAddress' or 'furtherAddresses' ---
	Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'Deutschland',1,0,'DE',0,cid,0);
	Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'Frankreich',2,0,'FR',0,cid,0);
	Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'Polen',3,0,'PL',0,cid,0);
	Insert into IDM_ATTRIBUTE (ID,DISPLAYNAME,DISPLAYORDER,MULTIPLE,NAME,TYPE,PARENTATTRIBUTE_ID, WRITEPROTECTED) values (HIBERNATE_SEQUENCE.NEXTVAL,'Vereinigtes Königreich',4,0,'GB',0,cid,0);
    
  END LOOP;
  CLOSE attributeCountryIdCursor;

END;
