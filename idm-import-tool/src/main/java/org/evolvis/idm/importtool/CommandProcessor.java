package org.evolvis.idm.importtool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleManagement;
import org.evolvis.idm.ws.InternalWsClientFactory;

/** class that contains the methods that process one command and call the 
 * web services of IdM
 * 
 * @author sischaar
 * @author Martin Pelzer, tarent GmbH
 * @since 16.10.2009
 */
public class CommandProcessor {
    
	private final String DEFAULT_ROLE_SCOPE = "default";
	
	
    private Logger logger = Logger.getLogger(CommandProcessor.class);
    
    Utils utils = new Utils();
    
    private String endpointURI;
    private String clientName;
    private String defaultApplication;
    
    private InternalWsClientFactory clientFactory = InternalWsClientFactory.getInstance();
    private UserManagement userAccountManagementService;
    private GroupAndRoleManagement groupAndRoleManagementService;
    private ClientManagement clientManagementService;
    private ApplicationManagement applicationManagementService;
    private OrganizationalUnitManagement orgUnitManagementService;
    private InternalRoleManagement internalRoleManagementService;
    
    private Client client;

    private Map<String,User> userCache = new HashMap<String,User>();
    private Map<String,Group> groupCache = new HashMap<String,Group>();
    private Map<String,Role> roleCache = new HashMap<String,Role>();

    
    public CommandProcessor(String endpoint, String clientName, String application) {
        this.endpointURI = endpoint;
        this.clientName = clientName;
        this.defaultApplication = application;
        
        userAccountManagementService = clientFactory.getUserManagementService(endpointURI);
        groupAndRoleManagementService = clientFactory.getGroupAndRoleManagementService(endpointURI);
        clientManagementService = InternalWsClientFactory.getInstance().getClientManagementService(endpointURI);
        applicationManagementService = InternalWsClientFactory.getInstance().getApplicationManagementService(endpointURI);
        orgUnitManagementService = InternalWsClientFactory.getInstance().getOrganizationalUnitManagementService(endpointURI);
        internalRoleManagementService = InternalWsClientFactory.getInstance().getInternalRoleManagementService(endpointURI);
    }
    
    
    public void init() {
    	try {
			this.client = clientManagementService.getClientByName(clientName);
		} catch (IllegalRequestException e) {
			logger.error("IllegalRequestException while accessing web service for receiving client: " + e.getMessage());
        	throw new RuntimeException();
		} catch (BackendException e) {
			logger.error("BackendException while accessing web service for receiving client: " + e.getMessage());
        	throw new RuntimeException();
		}
        if (client == null) {
        	logger.error("The given client is unknown.");
        	throw new RuntimeException();
        }
    }
    
    
    /**
     * Creates an account if no account exists for the given username.
     * The params list contains (in the given order):
     * mailAddress, displayName, firstName, lastName, password
     */
    public void createUser(List<String> params) throws Exception {
    	int index = 0;
        String userName = utils.getParam(params, index++, "mailAddress");
        String displayName = utils.getParam(params, index++, "DisplayName");
        String firstName = utils.getParam(params, index++, "FirstName");
        String lastName = utils.getParam(params, index++, "LastName");
        String password = utils.getParam(params, index++, "Password");
        utils.checkParamCount(params, index);
        
        User user = new User();
        user.setClient(this.client);
        user.setUsername(userName);
        user.setDisplayName(displayName);
        user.setFirstname(firstName);
        user.setLastname(lastName);
        user.setPassword(password);
        
        userAccountManagementService.createUser(user);
        logger.info("createUser(" + clientName + ", " + userName + ", " + displayName + ") done.");
    }
    

    /** creates a new group
     * The params list contains (in the given order):
     * displayName, name
     * 
     * @param params
     * @throws Exception
     */
    public void setGroup(List<String> params) throws Exception {
    	int index = 0;
        String displayName = utils.getParam(params, index++, "displayName");
        String name = utils.getParam(params, index++, "name");
        utils.checkParamCount(params, index);
        
    	// Check if group does already exist.
		Group group = this.getGroup(name);
		if (group != null) {
			logger.warn("Gruppe \"" + name + "\" existiert bereits.");
		} else {
			group = new Group();
			group.setClient(client);
			group.setName(name);
			group.setDisplayName(displayName);
			group = groupAndRoleManagementService.setGroup(clientName, group);
		}
		if (group != null) {
			if (group.getId() == null) {
				logger.warn("setGroup " + params + " lieferte keine ID.");
			}
		} else {
			logger.warn("setGroup " + params + " lieferte keine Gruppe.");
		}
		
		logger.info("setGroup(" + displayName + ", " + name + ") done.");

		// Put new group in group cache.
		this.groupCache.put(name, group);
    }
    
    
    /**
	 * creates a new role for the application given in the params (or for the
	 * default application if not application is given in the params) The params
	 * list contains (in the given order): displayName, name, [application]
	 * 
	 * @param params
	 * @throws Exception
	 */
    public void setRole(List<String> params) throws Exception {
    	int index = 0;
        String displayName = utils.getParam(params, index++, "displayName");
        String name = utils.getParam(params, index++, "name");
        String application = utils.getOptionalParam(params, index++, "application", defaultApplication);
        utils.checkParamCount(params, index);
    	
        Role role = new Role();
		Application app = applicationManagementService.getApplicationByName(clientName, application);
		if (app == null) {
			logger.error("setRole: application \"" + application + "\" unknown. Could not set role " + name + ".");
			return;
		}
		role.setApplication(app);
		role.setName(name);
		role.setDisplayName(displayName);
		role = groupAndRoleManagementService.setRole(clientName, role);

		if (role != null) {
			if (role.getId() == null) {
				logger.warn("setRole " + params + " lieferte keine ID.");
			}
		} else {
			logger.warn("setRole " + params + " lieferte keine Rolle.");
		}
		
		logger.info("setRole(" + displayName + ", " + name + ") done.");

		// Put new role in role cache.
		this.roleCache.put(name, role);
	}
    
    
    /**
	 * creates a new org unit for a group The params list contains (in the given
	 * order): name, displayName, groupName
	 * 
	 * If the group referenced by groupName does not exist an error is written
	 * to the log.
	 * 
	 * @param params
	 * @throws Exception
	 */
    public void setOrgUnit(List<String> params) throws Exception {
    	int index = 0;
        String name = utils.getParam(params, index++, "name");
        String displayName = utils.getParam(params, index++, "displayName");
        String groupName = utils.getParam(params, index++, "groupName");
        utils.checkParamCount(params, index);
        
        // get group
        Group group = this.getGroup(groupName);
       	if (group == null) {
       		logger.error("setOrgUnit: group \"" + groupName + "\" unknown. Could not add org unit " + name + ".");
   			return;
       	}
        
        // create org unit for group
        OrgUnit orgUnit = new OrgUnit();
        orgUnit.setDisplayName(displayName);
        orgUnit.setGroup(group);
        orgUnit.setName(name);
        
        this.orgUnitManagementService.setOrgUnit(clientName, orgUnit);
        
        logger.info("setOrgUnit(" + displayName + ", " + name + ") done.");
    }
    
    
    /** Creates a new key value pair for the given org unit. If a pair with the given key already exists
     * for the given org unit, it will be overridden.
     * The params list contains (in the given order):
     * orgUnitName, key, value
     * 
     * If the org unit referenced by orgUnitName does not exist an error is written to the log.
     * 
     * @param params
     * @throws Exception
     */
    public void addOrgUnitKeyValue(List<String> params) throws Exception {
    	int index = 0;
        String orgUnitName = utils.getParam(params, index++, "orgUnitName");
        String key = utils.getParam(params, index++, "key");
        String value = utils.getParam(params, index++, "value");
        utils.checkParamCount(params, index);
        
        // get orgUnit
        OrgUnit orgUnit = this.orgUnitManagementService.getOrgUnitByName(clientName, orgUnitName);
        if (orgUnit == null) {
        	logger.error("addOrgUnitKeyValue: orgUnit \"" + orgUnitName + "\" unknown. Could not add key " + key + ".");
			return;
        }
        
        // add new key/value pair to orgUnit
        this.orgUnitManagementService.addValueToOrgUnit(clientName, orgUnit.getId(), key, value);
        
        logger.info("addOrgUnitKeyValue(" + orgUnitName + ", " + key + ", " + value + ") done.");
    }
    
    
    /** Creates an association between a group and a role.
     * The params list contains (in the given order):
     * groupName, roleName, [applicationName], [scope]
     * 
     * If the group referenced by groupName or the role referenced by roleName do not exist
     * an error is written to the log.
     * 
     * @param params
     * @throws Exception
     */
    public void addGroupToRole(List<String> params) throws Exception {    	
    	int index = 0;
        String groupName = utils.getParam(params, index++, "groupName");
        String roleName = utils.getParam(params, index++, "roleName");
        String applicationName = utils.getOptionalParam(params, index++, "application", this.defaultApplication);
        String scope = utils.getOptionalParam(params, index++, "scope", DEFAULT_ROLE_SCOPE);
        utils.checkParamCount(params, index);
    	
    	// get group
        Group group = this.getGroup(groupName);
       	if (group == null) {
       		logger.error("addGroupToRole: group \"" + groupName + "\" unknown. Could not add role " + roleName + ".");
   			return;
       	}
        
        // get role
        Role role = this.getRole(roleName, applicationName);
       	if (role == null) {
       		logger.error("addGroupToRole: role \"" + roleName + "\" unknown for application " + applicationName + ". Could not add to group " + groupName + ".");
   			return;
       	}
        
        // add group to role
        groupAndRoleManagementService.addGroupToRole(clientName, group.getId(), role.getId(), scope);
        
        logger.info("addGroupToRole(" + groupName + ", " + roleName + ") done.");
    }
    
    
    /** Adds a user to a group.
     * The params list contains (in the given order):
     * userMailAddress,groupName
     * 
     * If the user referenced by userMailAddress or the group referenced by groupName do
     * not exist an error is written to the log.
     * 
     * @param params
     * @throws Exception
     */
    public void addUserToGroup(List<String> params) throws Exception {
    	int index = 0;
        String mailAddress = utils.getParam(params, index++, "mailAdress");
        String groupName = utils.getParam(params, index++, "groupName");
        utils.checkParamCount(params, index);
        
        // get group
        Group group = this.getGroup(groupName);
       	if (group == null) {
       		logger.error("addUserToGroup: group \"" + groupName + "\" unknown. Could not add user " + mailAddress + ".");
   			return;
       	}
        
        // get user
        User user = this.getUser(mailAddress);
       	if (user == null) {
       		logger.error("addUserToGroup: user \"" + mailAddress + "\" unknown. Could not add to group " + groupName + ".");
   			return;
       	}
        
        // add user to group
        this.groupAndRoleManagementService.addUserToGroup(user.getUuid(), group.getId());
        
        logger.info("addUserToGroup(" + mailAddress + ", " + groupName + ") done.");
    }
    
    
    /** Adds a user to a role of a given application.
     * The params list contains (in the given order):
     * userMailAddress, roleName, [applicationName], [scope]
     * 
     * If the user referenced by userMailAddress or the role referenced by roleName or the application
     * referenced by applicationName do not exist an error is written to the log.
     * 
     * @param params
     * @throws Exception
     */
    public void addUserToRole(List<String> params) throws Exception {
    	int index = 0;
        String mailAddress = utils.getParam(params, index++, "mailAddress");
        String roleName = utils.getParam(params, index++, "roleName");
        String applicationName = utils.getOptionalParam(params, index++, "applicationName", this.defaultApplication);
        String scope = utils.getOptionalParam(params, index++, "Scope", DEFAULT_ROLE_SCOPE);
        utils.checkParamCount(params, index);
        
        // get user
        User user = this.getUser(mailAddress);
       	if (user == null) {
       		logger.error("addUserToRole: user \"" + mailAddress + "\" unknown. Could not add to role " + roleName + ".");
   			return;
       	}
       	
        // get role
        Role role = this.getRole(roleName, applicationName);
       	if (role == null) {
       		logger.error("addUserToRole: role \"" + roleName + "\" unknown. Could not add to user " + mailAddress + ".");
   			return;
       	}
        
        groupAndRoleManagementService.addUserToRole(user.getUuid(), role.getId(), scope);
        
        logger.info("addUserToRole(" + mailAddress + ", " + roleName + ") done.");
    }
    
    
    /** Adds a user to an IdM internal role.
     * The params list contains (in the given order):
     * userMailAddress, internalRoleName
     * 
     * If the user referenced by userMailAddress or the role referenced by internalRoleName do not exist
     * an error is written to the log.
     * 
     * @param params
     * @throws Exception
     */
    public void addUserToInternalRole(List<String> params) throws Exception {
    	int index = 0;
        String mailAddress = utils.getParam(params, index++, "mailAddress");
        String internalRoleName = utils.getParam(params, index++, "internalRoleName");
        utils.checkParamCount(params, index);
    	
    	// get user
        User user = this.getUser(mailAddress);
       	if (user == null) {
       		logger.error("addUserToInternalRole: user \"" + mailAddress + "\" unknown. Could not add to internal role " + internalRoleName + ".");
   			return;
       	}
       	
       	// get internal role type
       	InternalRoleType roleType;
       	if ("user".equals(internalRoleName))
       		roleType = InternalRoleType.USER;
       	else if ("sectionAdmin".equals(internalRoleName))
       		roleType = InternalRoleType.SECTION_ADMIN;
       	else if ("clientAdmin".equals(internalRoleName))
       		roleType = InternalRoleType.CLIENT_ADMIN;
       	else if ("superAdmin".equals(internalRoleName))
       		roleType = InternalRoleType.SUPER_ADMIN;
       	else {
       		logger.error("addUserToInternalRole: internal role \"" + internalRoleName + "\" unknown. Could not add to user " + mailAddress + ".");
   			return;
       	}
       	
       	// assign user to internal role
       	this.internalRoleManagementService.addUserToInternalRole(user.getUuid(), roleType);
       	
       	logger.info("addUserToInternalRole(" + mailAddress + ", " + internalRoleName + ") done.");
    }
    
    
    /** Adds a group to the list of groups that a user who is section admin can administrate.
     *  The params list contains (in the given order):
     *  userMailAddress, groupName
     *  
     *  If the user referenced by userMailAddress or the group referenced by groupName do not exist
     *  an error is written to the log. An error is also written if the user is no section admin.
     * 
     * @param params
     * @throws Exception
     */
    public void addUserToAdminGroup(List<String> params) throws Exception {
    	int index = 0;
        String mailAddress = utils.getParam(params, index++, "mailAddress");
        String groupName = utils.getParam(params, index++, "groupName");
        utils.checkParamCount(params, index);
        
        // get user
        User user = this.getUser(mailAddress);
       	if (user == null) {
       		logger.error("addUserToAdminGroup: user \"" + mailAddress + "\" unknown. Could not add admin group " + groupName + ".");
   			return;
       	}
        
        // get group
        Group group = this.getGroup(groupName);
       	if (group == null) {
       		logger.error("addUserToAdminGroup: group \"" + groupName + "\" unknown. Could not add user " + mailAddress + ".");
   			return;
       	}
       	
       	// add assignment
       	this.internalRoleManagementService.setInternalRoleScope(user.getUuid(), InternalRoleType.SECTION_ADMIN, group.getId());
        
        logger.info("addUserToAdminGroup(" + mailAddress + ", " + groupName + ") done.");
    }
    
    
    /**
     * Loescht die Gruppe zum gegebenen <code>name</code> (falls existent).
     * @param params
     * @throws Exception
     * @since 28.10.2009
     */
    /*public void deleteGroup(List<String> params) throws Exception {
        int index = 0;
        String name = utils.getParam(params, index++, "GroupName");
        utils.checkParamCount(params, index);

        logger.debug("check for existing group for \"" + name + "\" ...");
        Group group = groupAndRoleManagementService.getGroupByName(clientName, name);
        if (group == null) {
        	logger.warn("Gruppe \"" + name + "\" existiert nicht.");
            return;
        }
        logger.debug("method-call: deleteGroup(" + group.getId() + ") ...");
        groupAndRoleManagementService.deleteGroup(clientName, group.getId());
        logger.debug("finished: deleteGroup(" + group.getId() + ").");
        logger.info("deleteGroupByName(" + name + ") done: " + group.getId());
    }*/

    
    /**
     * Loescht die Rolle zum gegebenen <code>name</code> (und ggf. <code>application</code>)
     * (falls existent).
     * @param params
     * @throws Exception
     * @since 28.10.2009
     */
    /*public void deleteRole(List<String> params) throws Exception {
        int index = 0;
        String name = utils.getParam(params, index++, "RoleName");
        String application = utils.getOptionalParam(params, index++, "Application", defaultApplication);
        utils.checkParamCount(params, index);

        logger.debug("check for existing role for \"" + name + "\" ...");
        Role role = groupAndRoleManagementService.getRoleByName(clientName, application, name);
        if (role == null) {
        	logger.warn("Rolle \"" + name + "\" existiert nicht.");
            return;
        }
        logger.debug("method-call: deleteRole(" + role.getId() + ") ...");
        groupAndRoleManagementService.deleteRole(clientName, role.getId());
        logger.debug("finished: deleteRole(" + role.getId() + ").");
        logger.info("deleteRoleByName(" + name + ") done: " + role.getId());
    }*/
 
    
    /*public void deleteGroupFromRole(List<String> params) throws Exception {
        int index = 0;
        String groupIdKey = utils.getParam(params, index++, "GroupId");
        String roleIdKey = utils.getParam(params, index++, "RoleId");
        String scope = utils.getOptionalParam(params, index++, "Scope", DEFAULT_ROLE_SCOPE);
        utils.checkParamCount(params, index);
        Long groupId = utils.getLongValue("GroupId", groupIdMap, groupIdKey);
        Long roleId = utils.getLongValue("RoleId", roleIdMap, roleIdKey);

        if (groupId == null || roleId == null) {
        	logger.error("deleteGroupFromRole(" + groupIdKey + "=" + groupId + ", " + roleIdKey + "=" + roleId + ") nicht moeglich");
            return;
        }
        
        logger.debug("method-call: deleteGroupFromRole(" + groupId + ", " + roleId + ", " + scope + ") ...");
        groupAndRoleManagementService.deleteGroupFromRole(clientName, groupId, roleId, scope);
        logger.debug("finished: deleteGroupFromRole(" + groupId + ", " + roleId + ", " + scope + ").");
    }*/
    
    
    /*public void deleteUserFromGroup(List<String> params) throws Exception {
        int index = 0;
        String uuidKey = utils.getParam(params, index++, "UUID");
        String groupIdKey = utils.getParam(params, index++, "GroupId");
        utils.checkParamCount(params, index);
        String uuid = utils.getStringValue("UUID", uuidMap, uuidKey);
        Long groupId = utils.getLongValue("GroupId", groupIdMap, groupIdKey);
        
        if (uuid == null || groupId == null) {
        	logger.error("deleteUserFromGroup(" + uuidKey + "=" + uuid + ", " + groupIdKey + "=" + groupId + ") nicht moeglich");
            return;
        }
        
        logger.debug("method-call: deleteUserFromGroup(" + uuid + ", " + groupId + ") ...");
        groupAndRoleManagementService.deleteUserFromGroup(uuid, groupId);
        logger.debug("finished: deleteUserFromGroup(" + uuid + ", " + groupId + ").");
    }*/
    
    
    /*public void deleteUserFromRole(List<String> params) throws Exception {
        int index = 0;
        String uuidKey = utils.getParam(params, index++, "UUID");
        String roleIdKey = utils.getParam(params, index++, "RoleId");
        String scope = utils.getOptionalParam(params, index++, "Scope", DEFAULT_ROLE_SCOPE);
        utils.checkParamCount(params, index);
        String uuid = utils.getStringValue("UUID", uuidMap, uuidKey);
        Long roleId = utils.getLongValue("RoleId", roleIdMap, roleIdKey);
        
        if (uuid == null || roleId == null) {
        	logger.error("deleteUserFromRole(" + uuidKey + "=" + uuid + ", " + roleIdKey + "=" + roleId + ") nicht moeglich");
            return;
        }
        
        logger.debug("method-call: deleteUserFromRole(" + uuid + ", " + roleId + ", " + scope + ") ...");
        groupAndRoleManagementService.deleteUserFromRole(uuid, roleId, scope);
        logger.debug("finished: deleteUserFromRole(" + uuid + ", " + roleId + ", " + scope + ").");
    }*/
    
    
    /** returns a group identified by its name fetched from
     * the cache or from the IdM backend
     * 
     * @param groupName
     * @return
     */
    private Group getGroup(String groupName) {
    	Group group = this.groupCache.get(groupName);
        if (group == null) {
        	// not cached -> get from IdM backend
        	try {
				group = this.groupAndRoleManagementService.getGroupByName(clientName, groupName);
			} catch (IllegalRequestException e) {
				// This happens if the group is not existent in IdM yet.
				return null;
			} catch (BackendException e) {
				logger.error("error while fetching group: " + e.toString() + ", " + e.getMessage());
				return null;
			}
        }
        return group;
    }
    
    
    /** returns a role identified by its name fetched from
     * the cache or from the IdM backend
     * 
     * @param groupName
     * @return
     */
    private Role getRole(String roleName, String applicationName) {
    	Role role = this.roleCache.get(roleName);
        if (role == null) {
        	// not cached -> get from IdM backend
        	try {
				role = this.groupAndRoleManagementService.getRoleByName(clientName, applicationName, roleName);
			} catch (IllegalRequestException e) {
				// This happens if the role is not existent in IdM yet.
				return null;
			} catch (BackendException e) {
				logger.error("error while fetching role: " + e.getMessage());
				return null;
			}
        }
        return role;
    }
    
    
    /** returns a user identified by its name fetched from
     * the cache or from the IdM backend
     * 
     * @param groupName
     * @return
     */
    private User getUser(String mailAddress) {
    	User user = this.userCache.get(mailAddress);
        if (user == null) {
        	// not cached -> get from IdM backend
        	try {
				user = this.userAccountManagementService.getUserByUsername(clientName, mailAddress);
			} catch (IllegalRequestException e) {
				logger.error("error while fetching group: " + e.getMessage());
				return null;
			} catch (BackendException e) {
				logger.error("error while fetching group: " + e.getMessage());
				return null;
			}
        }
        return user;
    }
    
}
