package org.evolvis.idm.importtool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/** utilities for IdM import tool for handling import files
 * 
 * @author sischaar
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class Utils {

	public static final String VAR_PREFIX = "$";
	
	
    /** checks if the parameters that are needed are given
     * 
     * @param args
     * @return
     */
	public static boolean hasAllParameters(String[] args) {
		// If parameter "help" is given everything is ok. The help page is shown.
		if (hasParameter("-help", args))
			return true;
		
		// If "help" is not given, at least "client", "endpoint", "application" and "f" are needed.
		if (hasParameter("-endpoint", args) &&
				hasParameter("-f", args) &&
				hasParameter("-client", args) &&
				hasParameter("-application", args))
			return true;
		
		return false;
	}
	
	
	/** checks for existence of a program parameter
     * 
     * @param name name of the parameter
     * @param args array of all program parameters
     * @return <code>true</code>, if the parameter was found
     */
	public static boolean hasParameter(String name, String[] args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals(name)) {
                return true;
            }
        }
        return false;
    }

    
    /** returns the value of a program parameter
     * 
     * @param name name of the parameter
     * @param args array of all program parameters
     * @return returns the value of the given parameter or <code>null</code>.
     */
	public static String getParameter(String name, String[] args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals(name)) {
                if (i+1 < args.length) {
                    return args[i+1];
                }
            }
        }
        return null;
    }
    
	
    public static List<List<String>> splitCommands(List<String> lines) {
        List<List<String>> commands = new ArrayList<List<String>>();
        for (String line : lines) {
            commands.add(splitCommand(line));
        }
        return commands;
    }
    
    private static List<String> splitCommand(String line) {
    	String [] command = line.split("\",\"");
		
		if (command.length > 0) {
        	command[0] = command[0].substring(1);
        	command[command.length - 1] = command[command.length - 1].substring(0, command[command.length - 1].length() - 1);
        }
    	        
        return Arrays.asList(command);
    }
    
    public String getParam(List<String> params, int index, String name) {
        if (params.size() <= index) {
            throw new ArrayIndexOutOfBoundsException("Missing parameter \"" + name + "\" at index " + index);
        }
        String param = params.get(index);
        if (param.length() == 0) {
            throw new IllegalArgumentException("Missing mandatory parameter \"" + name + "\" at index " + index);
        }
        return param;
    }
    
    public String getOptionalParam(List<String> params, int index, String name, String defaultValue) {
        String param = null;
        if (params.size() > index) {
            param = params.get(index);
            if (param.length() == 0) {
                param = null;
            }
        }
        if (param == null) {
            return defaultValue;
        }
        return param;
    }
    
    public void checkParamCount(List<String> params, int size) {
        if (params.size() > size) {
        	System.out.println("Unexpected number of parameters (" + size + " expected): " + params);
        }
    }
    
    public <T extends Object> void putVariable(String name, Map<String,T> map, String key, T value) {
        T old = map.put(key, value);
        if (old != null) {
        	System.out.println(name + " \"" + key + "\", Wert \"" + old + "\" durch \"" + value + "\" ersetzt.");
        }
        else {
        	System.out.println(name + " \"" + key + "\", Wert \"" + value + "\" gesetzt.");
        }
    }

    public <T extends Object> T getVariable(String name, Map<String,T> map, String key) {
        T value = map.get(key);
        if (value == null) {
        	System.out.println(name + " \"" + key + "\" nicht gesetzt.");
        }
        else {
        	System.out.println(name + " \"" + key + "\", geladen: " + value);
        }
        return value;
    }

    public String getStringValue(String name, Map<String,String> map, String key) {
        if (key.startsWith(VAR_PREFIX)) {
            return getVariable(name, map, key);
        }
        // evtl. ist gar keine Variable angegeben:
        return key;
    }

    public Long getLongValue(String name, Map<String,Long> map, String key) {
        Long value = null;
        if (key.startsWith(VAR_PREFIX)) {
            value = getVariable(name, map, key);
        }
        else {
            // evtl. ist ID direkt und keine Variable angegeben:
            value = new Long(key);
        }
        return value;
    }
	
}
