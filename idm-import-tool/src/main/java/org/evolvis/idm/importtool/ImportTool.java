package org.evolvis.idm.importtool;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;


/** main class of IdM import tool; contains the main method and controls the
 * workflow (read file, parse file, process commands)
 * 
 * @author sischaar
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class ImportTool {
	
	private static final String DEFAULT_ENCODING = "ISO-8859-1";
	
	private Logger logger = Logger.getLogger(ImportTool.class);

	
	public void startImport(String fileName, String encoding, String endpoint, String application, String client, String verbose) throws IOException {
		// Read file.
		ImportFileReader reader = new ImportFileReader();
		List<String> lines = reader.readFile(fileName, encoding != null ? encoding : DEFAULT_ENCODING);
		
		// Parse all lines of the file;
		List<List<String>> commands = Utils.splitCommands(lines);

		// Iterate over the list of commands and delegate each command to the
		// correct method in the command parser.
		CommandProcessor cmdProcessor = new CommandProcessor(endpoint, client, application);
		try {
			cmdProcessor.init();
		} catch (Exception e) {
			// something went wrong while initializing command processor -> exit
			System.err.println("Could not initialize command processor. Please check log.");
			throw new IOException();
		}
		for (List<String> cmd : commands) {
			// Get first String which is the command itself. The remaining
			// Strings in the list are data.
			String method = cmd.get(0);
			cmd = cmd.subList(1, cmd.size());
			
			try {
				if ("createUser".equals(method)) {
					cmdProcessor.createUser(cmd);
				} else if ("addGroup".equals(method)) {
					cmdProcessor.setGroup(cmd);
				} else if ("addRole".equals(method)) {
					cmdProcessor.setRole(cmd);
				} else if ("addOrgUnit".equals(method)) {
					cmdProcessor.setOrgUnit(cmd);
				} else if ("addOrgUnitKeyValue".equals(method)) {
					cmdProcessor.addOrgUnitKeyValue(cmd);
				} else if ("addGroupToRole".equals(method)) {
					cmdProcessor.addGroupToRole(cmd);
				} else if ("addUserToGroup".equals(method)) {
					cmdProcessor.addUserToGroup(cmd);
				} else if ("addUserToRole".equals(method)) {
					cmdProcessor.addUserToRole(cmd);
				} else if ("addUserToInternalRole".equals(method)) {
					cmdProcessor.addUserToInternalRole(cmd);
				} else if ("addUserToAdminGroup".equals(method)) {
					cmdProcessor.addUserToAdminGroup(cmd);
				}

				// will be implemented/corrected later
				/*
				 * else if ("deleteGroup".equals(method)) {
				 * importTool.deleteGroup(cmd); } else if
				 * ("deleteRole".equals(method)) { importTool.deleteRole(cmd); }
				 * else if ("deleteGroupFromRole".equals(method)) {
				 * importTool.deleteGroupFromRole(cmd); } else if
				 * ("deleteUserFromGroup".equals(method)) {
				 * importTool.deleteUserFromGroup(cmd); } else if
				 * ("deleteUserFromRole".equals(method)) {
				 * importTool.deleteUserFromRole(cmd); }
				 */
				else {
					logger.error("Unsupported operation: \"" + method + "\" " + cmd);
				}
			} catch (Throwable t) {
				logger.error("execution of command \"" + method + "\" " + cmd + " failed: " + t.getMessage());
			}
		}

	}
    
    

    
	private static void showHelp() {
        System.out.println();
        System.out.println("IdM import tool");
        System.out.println();
        System.out.println("-help               [optional]  Hilfe.");
        System.out.println();
        System.out.println("    oder");
        System.out.println();
        System.out.println("-f <file>           [mandatory] Datei mit Anweisungen");
        System.out.println("-endpoint <uri>     [optional]  Endpoint-URI.");
        System.out.println("-client <name>      [optional]  Client-Name.");
        System.out.println("-application <name> [optional]  Applikation.");
        System.out.println("-scope <name>       [optional]  Zweck.");
        System.out.println("-encoding <name>    [optional]  Encoding der Datei.");
        System.out.println();
    }
    
    
    public static void main(String[] args) throws IllegalRequestException, BackendException {
        // Check if enough parameters are given.
    	if (!Utils.hasAllParameters(args)) {
        	System.out.println("needed parameters: '-help' or '-f <file> -endpoint <url> -client <client> -application <application>' [-encoding <encoding>]");
        	System.exit(1);
        }
    	
        System.out.println("Starting IdM import tool.");

        // If parameter "help" is given show help page and exit.
        if (Utils.hasParameter("-help", args) || Utils.hasParameter("-h", args)) {
            showHelp();
            System.exit(0);
        }

        // Otherwise initialize ImportTool instance, read file and start import.
        
        String fileName = Utils.getParameter("-f", args);
		if (fileName == null) {
			System.out.println("please provide a file name: '-f <fileName>'");
			System.exit(1);
		} 
		
		String encoding = Utils.getParameter("-encoding", args);
		ImportTool importTool = new ImportTool();
		try {
			importTool.startImport(fileName, encoding, Utils.getParameter("-endpoint", args), Utils.getParameter("-application", args), Utils.getParameter("-client", args), Utils.getParameter("-verbose", args));
		} catch (IOException e) {
			System.err.println("error while importing file: " + e.getMessage());
			System.exit(1);
		}
			
        System.out.println("IdM import tool finished. Please check idm-import-tool.log for possible errors.");
    }
	
}
