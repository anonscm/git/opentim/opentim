package org.evolvis.idm.importtool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/** helper class for reading a file
 * 
 * @author sischaar
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class ImportFileReader {

	 /**
	 * imports the data in the given file
	 * 
	 * @param f
	 *            the file to import
	 * @param fileEncoding
	 *            encoding of the file
	 * @throws IOException
	 */
    public List<String> readFile(String fileName, String fileEncoding) throws IOException {
    	File f = new File(fileName);
		if (!f.exists() || !f.canRead() || f.isDirectory()) {
			System.out.println("The given file name is not valid.");
			throw new IOException();
		}
    	
    	List<String> lines = null;
        try {
            lines = this.readFile(f, fileEncoding);
        }
        catch (IOException e) {
            System.err.println("file reading error: " + e);
            throw new IOException(e);
        }
        if (lines == null || lines.size() == 0) {
            System.err.println("file is empty");
            throw new IOException();
        }
        
        return lines;
    }
    
    
    /** reads a text file with a given encoding and returns the lines of the file as a list of Strings
	 * 
	 * @param f the file to read
	 * @param encoding the encoding of the file
	 * @return a list of Strings containing the lines of the text file
	 * @throws IOException
	 */
    private List<String> readFile(File f, String encoding) throws IOException {
        FileInputStream fin = null;
        InputStreamReader inr = null;
        BufferedReader in = null;
        try {
            fin = new FileInputStream(f);
            inr = new InputStreamReader(fin, encoding);
            in = new BufferedReader(inr);
            ArrayList<String> lines = new ArrayList<String>();
            String line;
            while ((line = in.readLine()) != null) {
                line = line.trim();
                if (line.length() == 0 || line.charAt(0) == '#') {
                    continue;
                }
                lines.add(line);
            }
            return lines;
        }
        finally {
            if (in != null) {
                in.close();
            }
            if (inr != null) {
                inr.close();
            }
            if (fin != null) {
                fin.close();
            }
        }
    }
	
}
