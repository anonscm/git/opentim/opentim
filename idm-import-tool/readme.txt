IdM import tool
-------------------

The IdM import tool can import data into an IdM installation. It uses the web services
interface of the IdM installation to add data that has to be provided in a specific
format.

Usage of IdM import tool
--------------------------

You can start IdM import tool by unpacking the tar.gz, changing to the directory
"idm-import-tool" and then starting "java -jar idm-import-tool-$VERSION.jar.

You will see a help screen that gives some information about parameters for idm
import tool. For importing data the needed parameters are

-f <FILE> -endpoint <URL> -client <CLIENTNAME> -application <APPLICATIONNAME>

FILE is the name of the file containing the data you want to import

URL is the URL of the web service interface of the IdM installation, e.g.
    http://<SERVERNAME>:8080/idm-backend

CLIENTNAME is the name of the IdM client ("Mandant") that you want to import the
    data for.
    
APPLICATIONNAME is the name of the application ("Anwendung") to which the roles
    you want to import should be associated.


Data format
-------------

Please have a look at the file "example.import". This file lists all possible commands
and the needed parameters for each command.

If you give no encoding format as input parameter please make sure that your import files
are in ISO-8859-1 encoding. Otherwise there will be problems with special characters
like umlauts.
