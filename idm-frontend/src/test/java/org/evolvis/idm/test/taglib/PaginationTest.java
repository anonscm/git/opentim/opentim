package org.evolvis.idm.test.taglib;


import static org.junit.Assert.*;

import java.io.StringWriter;

import javax.portlet.PortletURL;

import org.evolvis.idm.taglib.PaginationHelper;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.portlet.MockPortalContext;
import org.springframework.mock.web.portlet.MockPortletURL;

public class PaginationTest {

	private PortletURL url;
	
	/** Count of all items. */
    Integer itemCount;

    /** Count of items per page. */
    Integer itemLimit;

    /** Offset of items. */
    Integer offset;

    /** Url for pages links. */
    PortletURL portletUrl;
	private PaginationHelper helper;
	private StringWriter out;

	@Before
	public void setUp() throws Exception {
		url = new MockPortletURL(new MockPortalContext(),MockPortletURL.URL_TYPE_RENDER );
		helper = new PaginationHelper();
		helper.setItemCount(45);
		helper.setItemLimit(10);
		helper.setPortletUrl(url);
		
		out = new StringWriter();
	}

	@Test
	public void testHelper_pageCount_00() throws Throwable{		
		helper.setItemCount(50);
		assertEquals(5,helper.getPageCount());
	}
	
	@Test
	public void testHelper_pageCount_01() throws Throwable{		
		helper.setItemCount(41);
		assertEquals(5,helper.getPageCount());
	}

	
	@Test
	public void testHelper_lastPageLink_00() throws Throwable{		
		helper.writeLastPageLink(out);
		assertEquals("<a href=\"http://localhost/mockportlet?urlType=render;param_offset=40\" >&gt;&gt;</a>&nbsp;&nbsp;",out.toString());
	}
	@Test
	public void testHelper_lastPageLink_01() throws Throwable{		
		helper.setItemCount(41);
		helper.writeLastPageLink(out);
		assertEquals("<a href=\"http://localhost/mockportlet?urlType=render;param_offset=40\" >&gt;&gt;</a>&nbsp;&nbsp;",out.toString());
	}
	
	@Test
	public void testHelper_firstPageLink_00() throws Throwable{		
		helper.writeFirstPageLink(out);
		assertEquals("<a href=\"http://localhost/mockportlet?urlType=render;param_offset=0\" >&lt;&lt;</a>&nbsp;&nbsp;",out.toString());
	}
	
	@Test
	public void testHelper_nextPageLink_00() throws Throwable{
		helper.setOffset(30);
		helper.writeNextPageLink(out);
		assertEquals("<a href=\"http://localhost/mockportlet?urlType=render;param_offset=40\" >&gt;</a>&nbsp;&nbsp;",out.toString());
	}

	@Test
	public void testHelper_prevPageLink_00() throws Throwable{
		helper.setOffset(30);
		helper.writePreviousPageLink(out);
		assertEquals("<a href=\"http://localhost/mockportlet?urlType=render;param_offset=20\" >&lt;</a>&nbsp;&nbsp;",out.toString());
	}
	
	@Test
	public void testHelper_currentPageLink_00() throws Throwable{
		helper.setOffset(20);
		helper.writePageNumberLink(out, 2);
		assertEquals("<a href=\"http://localhost/mockportlet?urlType=render;param_offset=20\" class=\"currentpage\">3</a>&nbsp;&nbsp;",out.toString());
	}

	@Test
	public void testHelper_otherPageLink_00() throws Throwable{
		helper.setOffset(0);
		helper.writePageNumberLink(out, 3);
		assertEquals("<a href=\"http://localhost/mockportlet?urlType=render;param_offset=30\" >4</a>&nbsp;&nbsp;",out.toString());
	}
	
	@Test 
	public void testTag_secondPage_00() throws Throwable{
		helper.setOffset(10);
		helper.writePaginationLinks(out);		
		String expected =
			"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=0\" >&lt;&lt;</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=0\" >&lt;</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=0\" >1</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=10\" class=\"currentpage\">2</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=20\" >3</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=30\" >4</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=40\" >5</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=20\" >&gt;</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=40\" >&gt;&gt;</a>&nbsp;&nbsp;";
		assertEquals(expected,out.toString());
	}
	
	@Test 
	public void testTag_firstPage_00() throws Throwable{
		helper.setOffset(0);
		helper.writePaginationLinks(out);		
		String expected =
			"&lt;&lt;&nbsp;&nbsp;"
			+"&lt;&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=0\" class=\"currentpage\">1</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=10\" >2</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=20\" >3</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=30\" >4</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=40\" >5</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=10\" >&gt;</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=40\" >&gt;&gt;</a>&nbsp;&nbsp;";
		assertEquals(expected,out.toString());
	}
	
	@Test 
	public void testTag_lastPage_00() throws Throwable{
		helper.setOffset(40);
		helper.writePaginationLinks(out);		
		String expected =
			"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=0\" >&lt;&lt;</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=30\" >&lt;</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=0\" >1</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=10\" >2</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=20\" >3</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=30\" >4</a>&nbsp;&nbsp;"
			+"<a href=\"http://localhost/mockportlet?urlType=render;param_offset=40\" class=\"currentpage\">5</a>&nbsp;&nbsp;"
			+"&gt;&nbsp;&nbsp;"
			+"&gt;&gt;&nbsp;&nbsp;";
		assertEquals(expected,out.toString());
	}

}
