//package org.evolvis.idm.test.util;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.springframework.validation.Errors;
//import org.springframework.validation.FieldError;
//import org.springframework.validation.ObjectError;
//
//@SuppressWarnings("unchecked")
//public class ErrorHolder implements Errors {
//
//    private final String objectName;
//
//    private List<ObjectError> globalErrors = new ArrayList<ObjectError>();
//
//    private Map<String, List<FieldError>> fieldErrorMap = new HashMap<String, List<FieldError>>();
//
//
//    public ErrorHolder(final Object obj) {
//        objectName = obj.getClass().getSimpleName();
//    }
//
//
//    @Override
//    public void addAllErrors(Errors errors) {
//        throw new RuntimeException("not implemented");
//
//    }
//
//
//    @Override
//    public List getAllErrors() {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    @Override
//    public int getErrorCount() {
//        return globalErrors.size() + getFieldErrorCount();
//    }
//
//
//    @Override
//    public FieldError getFieldError() {
//        return fieldErrorMap.isEmpty() ? null : fieldErrorMap.get(0).get(0);
//    }
//
//
//    @Override
//    public FieldError getFieldError(String field) {
//        return fieldErrorMap.containsKey(field) ? fieldErrorMap.get(field).get(0) : null;
//    }
//
//
//    @Override
//    public int getFieldErrorCount() {
//        int fieldErrorCounter = 0;
//        for (final List<FieldError> fieldErrors : fieldErrorMap.values()) {
//            fieldErrorCounter += fieldErrors.size();
//        }
//        return fieldErrorCounter;
//    }
//
//
//    @Override
//    public int getFieldErrorCount(String field) {
//        return fieldErrorMap.containsKey(field) ? fieldErrorMap.get(field).size() : 0;
//    }
//
//
//    @Override
//    public List getFieldErrors() {
//        final List<FieldError> allFieldErrors = new ArrayList<FieldError>();
//        for (final List<FieldError> fieldErrors : fieldErrorMap.values()) {
//            allFieldErrors.addAll(fieldErrors);
//        }
//        return allFieldErrors;
//    }
//
//
//    @Override
//    public List getFieldErrors(String field) {
//        return fieldErrorMap.containsKey(field) ? Collections.unmodifiableList(fieldErrorMap.get(field)) : Collections.EMPTY_LIST;
//    }
//
//
//    @Override
//    public Class getFieldType(String field) {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    @Override
//    public Object getFieldValue(String field) {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    @Override
//    public ObjectError getGlobalError() {
//        return globalErrors.isEmpty() ? null : globalErrors.get(0);
//    }
//
//
//    @Override
//    public int getGlobalErrorCount() {
//        return globalErrors.size();
//    }
//
//
//    @Override
//    public List getGlobalErrors() {
//        return Collections.unmodifiableList(globalErrors);
//    }
//
//
//    @Override
//    public String getNestedPath() {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    @Override
//    public String getObjectName() {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    @Override
//    public boolean hasErrors() {
//        return !globalErrors.isEmpty() || !fieldErrorMap.isEmpty();
//    }
//
//
//    @Override
//    public boolean hasFieldErrors() {
//        return !fieldErrorMap.isEmpty();
//    }
//
//
//    @Override
//    public boolean hasFieldErrors(String field) {
//        return fieldErrorMap.containsKey(field);
//    }
//
//
//    @Override
//    public boolean hasGlobalErrors() {
//        return !globalErrors.isEmpty();
//    }
//
//
//    @Override
//    public void popNestedPath() throws IllegalStateException {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    @Override
//    public void pushNestedPath(String subPath) {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    @Override
//    public void reject(String errorCode) {
//        globalErrors.add(new ObjectError(objectName, errorCode));
//    }
//
//
//    @Override
//    public void reject(String errorCode, String defaultMessage) {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    @Override
//    public void reject(String errorCode, Object[] errorArgs, String defaultMessage) {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    @Override
//    public void rejectValue(String field, String errorCode) {
//        final List<FieldError> fieldErrors = fieldErrorMap.containsKey(field) ? fieldErrorMap.get(field) : new ArrayList<FieldError>();
//        fieldErrors.add(new FieldError(objectName, field, errorCode));
//        fieldErrorMap.put(field, fieldErrors);
//    }
//
//
//    @Override
//    public void rejectValue(String field, String errorCode, String defaultMessage) {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    @Override
//    public void rejectValue(String field, String errorCode, Object[] errorArgs, String defaultMessage) {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    @Override
//    public void setNestedPath(String nestedPath) {
//        throw new RuntimeException("not implemented");
//    }
//
//
//    public Map<String, List<FieldError>> getFieldErrorMap() {
//        return fieldErrorMap;
//    }
//
//
//    public void setFieldErrorMap(Map<String, List<FieldError>> fieldErrorMap) {
//        this.fieldErrorMap = fieldErrorMap;
//    }
//
//
//    public void setGlobalErrors(List<ObjectError> globalErrors) {
//        this.globalErrors = globalErrors;
//    }
//
//
//    @Override
//    public int hashCode() {
//        final int prime = 31;
//        int result = 1;
//        result = prime * result + ((fieldErrorMap == null) ? 0 : fieldErrorMap.hashCode());
//        result = prime * result + ((globalErrors == null) ? 0 : globalErrors.hashCode());
//        result = prime * result + ((objectName == null) ? 0 : objectName.hashCode());
//        return result;
//    }
//
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj)
//            return true;
//        if (obj == null)
//            return false;
//        if (getClass() != obj.getClass())
//            return false;
//        ErrorHolder other = (ErrorHolder) obj;
//        if (fieldErrorMap == null) {
//            if (other.fieldErrorMap != null)
//                return false;
//        }
//        else if (!fieldErrorMap.equals(other.fieldErrorMap))
//            return false;
//        if (globalErrors == null) {
//            if (other.globalErrors != null)
//                return false;
//        }
//        else if (!globalErrors.equals(other.globalErrors))
//            return false;
//        if (objectName == null) {
//            if (other.objectName != null)
//                return false;
//        }
//        else if (!objectName.equals(other.objectName))
//            return false;
//        return true;
//    }
//
//
//    @Override
//    public String toString() {
//        return "ErrorHolder [objectName=" + objectName + ", globalErrors=" + globalErrors + ", fieldErrorMap=" + fieldErrorMap + "]";
//    }
//    
//    
//}
