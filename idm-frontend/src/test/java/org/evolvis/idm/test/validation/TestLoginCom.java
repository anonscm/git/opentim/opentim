package org.evolvis.idm.test.validation;

import org.evolvis.idm.common.validation.JSR303Validator;
import org.evolvis.idm.loginregister.command.LoginCom;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

/**
 * @author Tino Rink
 * @author Dmytro Mayster
 * @author Jens Neumaier
 * 
 * This is a test for LoginCom and spring validator implementation.
 * 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"TestValidation-context.xml"})
public class TestLoginCom {

    @Autowired
    protected JSR303Validator validator;

    @Test
    public void testValidLogin() {

        final LoginCom command = new LoginCom();
        command.setLogin("login");
        command.setPassword("passwort");

        Assert.assertTrue("LoginCom must be supported", validator.supports(command.getClass()));
      
        Errors errors = new BeanPropertyBindingResult(command, "command");       
        validator.validate(command, errors);

        Assert.assertFalse("LoginCom must have no errors ", errors.hasErrors());
        Assert.assertFalse("LoginCom must have no global errors ", errors.hasGlobalErrors());
        Assert.assertFalse("LoginCom must have no field errors ", errors.hasFieldErrors());
    }


    @Test
    public void testInvalidLogin1() {

        final LoginCom command = new LoginCom();
        command.setLogin(null);
        command.setPassword("passwort");

        Assert.assertTrue("LoginCom must be supported", validator.supports(command.getClass()));

        Errors errors = new BeanPropertyBindingResult(command, "command");       
        validator.validate(command, errors);

        Assert.assertTrue("LoginCom must have errors ", errors.hasErrors());
        Assert.assertFalse("LoginCom must have no global errors ", errors.hasGlobalErrors());
        Assert.assertTrue("LoginCom must have field errors ", errors.hasFieldErrors());
        Assert.assertTrue("LoginCom must have login field errors ", errors.hasFieldErrors("login"));
        
        Assert.assertTrue("LoginCom must have login field errors ", "violation.notNull".equals(errors.getFieldError("login")
                .getDefaultMessage()));
    }


    @Test
    public void testInvalidLogin2() {

        final LoginCom command = new LoginCom();
        command.setLogin("");
        command.setPassword("passwort");

        Assert.assertTrue("LoginCom must be supported", validator.supports(command.getClass()));

        Errors errors = new BeanPropertyBindingResult(command, "command");       
        validator.validate(command, errors);

        Assert.assertTrue("LoginCom must have errors ", errors.hasErrors());
        Assert.assertFalse("LoginCom must have no global errors ", errors.hasGlobalErrors());
        Assert.assertTrue("LoginCom must have field errors ", errors.hasFieldErrors());
        Assert.assertTrue("LoginCom must have login field errors ", errors.hasFieldErrors("login"));
        
        Assert.assertTrue("LoginCom must have login field errors ", "violation.notEmpty".equals(errors.getFieldError("login")
                .getDefaultMessage()));
    }


    @Test
    public void testInvalidPasswort1() {

        final LoginCom command = new LoginCom();
        command.setLogin("login");
        command.setPassword(null);

        Assert.assertTrue("LoginCom must be supported", validator.supports(command.getClass()));

        Errors errors = new BeanPropertyBindingResult(command, "command");       
        validator.validate(command, errors);
        
        Assert.assertTrue("LoginCom must have errors ", errors.hasErrors());
        Assert.assertFalse("LoginCom must have no global errors ", errors.hasGlobalErrors());
        Assert.assertTrue("LoginCom must have field errors ", errors.hasFieldErrors());
        Assert.assertTrue("LoginCom must have login field errors ", errors.hasFieldErrors("password"));
        
        Assert.assertTrue("LoginCom must have login field errors ", "violation.notNull".equals(errors.getFieldError("password")
                .getDefaultMessage()));
    }


    @Test
    public void testInvalidPasswort2() {

        final LoginCom command = new LoginCom();
        command.setLogin("login");
        command.setPassword("");

        Assert.assertTrue("LoginCom must be supported", validator.supports(command.getClass()));

        Errors errors = new BeanPropertyBindingResult(command, "command");       
        validator.validate(command, errors);

        Assert.assertTrue("LoginCom must have errors ", errors.hasErrors());
        Assert.assertFalse("LoginCom must have no global errors ", errors.hasGlobalErrors());
        Assert.assertTrue("LoginCom must have field errors ", errors.hasFieldErrors());
        Assert.assertTrue("LoginCom must have login field errors ", errors.hasFieldErrors("password"));
        
        Assert.assertTrue("LoginCom must have login field errors ", "violation.notEmpty".equals(errors.getFieldError("password")
                .getDefaultMessage()));
    }
}
