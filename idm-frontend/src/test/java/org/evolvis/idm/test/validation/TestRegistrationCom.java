package org.evolvis.idm.test.validation;

import org.evolvis.idm.common.validation.JSR303Validator;
import org.evolvis.idm.loginregister.command.RegistrationCom;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

/**
 * @author Tino Rink
 * @author Dmytro Mayster
 * @author Jens Neumaier
 * 
 * This is a test for RegistrationCom and spring validator implementation.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"TestValidation-context.xml"})
public class TestRegistrationCom {

    @Autowired
    protected JSR303Validator validator;

    @Test
    public void testValidLogin() {

        final RegistrationCom command = new RegistrationCom();
        command.setFirstname("firstname");
        command.setLastname("lastname");
        command.setDisplayName("displayname");
        command.setLogin("email@test.de");
        command.setPassword("Test1234+");
        command.setPasswordRepeat("Test1234+");

        Assert.assertTrue("RegistrationCom must be supported", validator.supports(command.getClass()));

        Errors errors = new BeanPropertyBindingResult(command, "command");
        validator.validate(command, errors);

        Assert.assertFalse("RegistrationCom must have no errors ", errors.hasErrors());
        Assert.assertFalse("RegistrationCom must have no global errors ", errors.hasGlobalErrors());
        Assert.assertFalse("RegistrationCom must have no field errors ", errors.hasFieldErrors());
    }
    
    @Test
    public void testInvalidInput() {

        final RegistrationCom command = new RegistrationCom();
        command.setFirstname("firstname");
        command.setLastname("l");
        command.setDisplayName("displayname");
        command.setLogin("email@test.de");
        command.setPassword("asasas");
        command.setPasswordRepeat("asasas1");

        Assert.assertTrue("RegistrationCom must be supported", validator.supports(command.getClass()));

        Errors errors = new BeanPropertyBindingResult(command, "command");
        validator.validate(command, errors);

        Assert.assertTrue("RegistrationCom must have errors ", errors.hasErrors());
    }
    
}
