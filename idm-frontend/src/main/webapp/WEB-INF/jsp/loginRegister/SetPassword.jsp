<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0" version="2.0"
>

<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" />

<s:htmlEscape defaultHtmlEscape="true" />

<h3><s:message code="header.recovery.do" /></h3>

<jsp:include page="../common/_msg.jsp" />

<portlet:actionURL var="setPasswordUrl">
	<portlet:param name="ctx" value="SetPassword" />
</portlet:actionURL>

<form:form action="${setPasswordUrl}" method="post"
	modelAttribute="command" cssClass="login_form">

	<div>
		<div style="text-align: left; float: left"><s:message code="label.setNewPasswordNotice" /></div>
	</div>

	<div>
		<form:label path="password"><s:message code="label.password" /></form:label>
		<form:password id="password" path="password" cssErrorClass="field-error" />
	</div>
	
	<div>
		<form:label path="passwordRepeat"><s:message code="label.password_repeat" /></form:label>
		<form:password id="passwordRepeat" path="passwordRepeat" cssErrorClass="field-error" />
	</div>
	
	<div class="button">
		<button type="submit"><s:message code="button.save" /></button>
	</div>

</form:form>

<br />

<c:if test="${skipNavigationLinks == null}">
	<div style="clear:both">
		<portlet:renderURL var="loginUrl">
			<portlet:param name="ctx" value="Login" />
		</portlet:renderURL>
		<a href="${loginUrl}"><s:message code="to.login" /></a>
		|
		<c:if test="${!altRegistration}">
			<portlet:renderURL var="registrationUrl">
				<portlet:param name="ctx" value="Registration" />
			</portlet:renderURL>
		</c:if>
		<a href="${registrationUrl}"><s:message code="to.register" /></a>
	</div>
</c:if>

</jsp:root>