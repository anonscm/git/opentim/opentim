<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	version="2.0"
>

<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" />

<h3><s:message code="header.registration" /></h3>

<jsp:include page="../common/_msg.jsp" />

<portlet:actionURL var="registrationUrl">
	<portlet:param name="ctx" value="Registration" />
</portlet:actionURL>

<c:if test="${msg_success == null}">
	<form:form action="${registrationUrl}" method="post"
		modelAttribute="command" cssClass="login_form">
	
		<form:hidden path="activationURL"/>
	
		<div>
			<form:label path="firstname"><s:message code="label.firstname" /></form:label>
			<form:input path="firstname" cssErrorClass="field-error" />
		</div>
		<div>
			<form:label path="lastname"><s:message code="label.lastname" /></form:label>
			<form:input path="lastname" cssErrorClass="field-error" />
		</div>
		<div>
			<form:label path="login"><s:message code="label.login" /></form:label>
			<form:input path="login" cssErrorClass="field-error" />
		</div>
		<c:choose>
			<c:when test="${displayNameGenerationEnabled}">
				<form:hidden path="displayName"  />
			</c:when>
			<c:otherwise>
				<div>
					<form:label path="displayName"><s:message code="label.displayName" /></form:label> 
					<form:input path="displayName" cssErrorClass="field-error" />
				</div>
			</c:otherwise>
		</c:choose>
		<div>
			<form:label path="password"><s:message code="label.password" /></form:label>
			<form:password path="password" cssErrorClass="field-error" />
		</div>
		<div>
			<form:label path="passwordRepeat"><s:message code="label.password_repeat" /></form:label>
			<form:password path="passwordRepeat" cssErrorClass="field-error" />
		</div>
		<c:if test="${termsRequired}">
			<div>
				<form:checkbox path="declarationOfApproval" id="declarationOfApproval" cssStyle="margin-right: 5px; float: left;"/>
				<form:label path="declarationOfApproval" cssStyle="width: 340px; max-width: 340px;">
					<s:message code="label.declarationOfApprovalText" /><a style="margin-left: 5px;" href="${hyperlink}"><s:message code="label.linkDeclarationOfApproval" /></a>
				</form:label>
			</div>
		</c:if>
		<div class="button">
			<button type="submit"><s:message code="button.create"/></button>
		</div>
	
	</form:form>
</c:if>

<br />

<div style="clear:both;">
	<portlet:renderURL var="loginUrl">
		<portlet:param name="ctx" value="Login" />
	</portlet:renderURL>
	<a href="${loginUrl}"><s:message code="to.login" /></a>
	|
	<portlet:renderURL var="reqRecoveryUrl">
		<portlet:param name="ctx" value="ReqRecovery" />
	</portlet:renderURL>
	<a href="${reqRecoveryUrl}"><s:message code="to.recovery" /></a>
</div>

</jsp:root>