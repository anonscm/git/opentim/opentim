<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	version="2.0"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<c:if test="${skipHeader == null}">
	<h3><s:message code="header.login" /></h3>
</c:if>

<jsp:include page="../common/_msg.jsp" />

<portlet:actionURL var="loginUrl">
	<portlet:param name="ctx" value="Login" />
	<portlet:param name="action" value="Login" />
</portlet:actionURL>

<form:form action="${loginUrl}" method="post" modelAttribute="command" cssClass="login_form">

	<div>
		<form:label path="login"><s:message code="label.login" /></form:label>
		<form:input path="login" cssClass="focus" cssErrorClass="field-error" />
	</div>
	<div>
		<form:label path="password"><s:message code="label.password" /></form:label>
		<form:password path="password" cssClass="focus" cssErrorClass="field-error" />
	</div>
	<div class="button">
		<button type="submit"><s:message code="button.login" /></button>
	</div>
	
</form:form>

<br />

<c:if test="${skipNavigationLinks == null}">
	<div style="clear:both;">
	<c:if test="${!altRegistration}">
		<portlet:renderURL var="registrationUrl">
			<portlet:param name="ctx" value="Registration" />
		</portlet:renderURL>
	</c:if>
		<a href="${registrationUrl}"><s:message code="to.register" /></a>
		|
		<portlet:renderURL var="reqRecoveryUrl">
			<portlet:param name="ctx" value="ReqRecovery" />
		</portlet:renderURL>
		<a href="${reqRecoveryUrl}"><s:message code="to.recovery" /></a>
	</div>
</c:if>

</jsp:root>