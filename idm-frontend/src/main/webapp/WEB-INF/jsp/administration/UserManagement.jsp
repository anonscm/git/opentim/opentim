<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:sec="http://www.springframework.org/security/tags"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:idm="idm-taglib"
	version="2.0">

<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<div id="benutzerverwaltung">

<jsp:include page="common/_init.jsp" />
<jsp:include page="common/_nav.jsp" />

<jsp:include page="../common/_msg.jsp" />

<portlet:resourceURL id="/img/bin.png" var="img_bin_png" />
<portlet:resourceURL id="/img/tick.png" var="img_active_png" />
<portlet:resourceURL id="/img/cross.png" var="img_inactive_png" />
<portlet:resourceURL id="/img/delete.png" var="img_delete_png" />
<portlet:resourceURL id="/img/user_edit.png" var="img_user_edit_png" />
<portlet:resourceURL id="/img/group_add.png" var="img_group_add_png" />
<portlet:resourceURL id="/img/key_add.png" var="img_key_add_png" />
<portlet:resourceURL id="/img/arrow_down_white.png" var="img_arrow_down_png" />
<portlet:resourceURL id="/img/arrow_up_white.png" var="img_arrow_up_png" />
<portlet:resourceURL id="/img/view_refresh.png" var="user_sync_png" />

<s:message code="label.pages.ascending" var="ascending" />
<s:message code="label.pages.descending" var="descending" />
<s:message code="label.users.active" var="active" />
<s:message code="label.users.inactive" var="inactive" />
<s:message code="label.users.all" var="all" />
<s:message code="label.users.sync" var="msg_sync"/>


<div>


<!-- create new user form -->

<div id="user">
	<sec:authorize ifAnyGranted="ROLE_SUPER_ADMIN, ROLE_CLIENT_ADMIN">
		<portlet:actionURL var="addUrl">
			<portlet:param name="ctx" value="${ctx}" />
			<portlet:param name="action" value="Add" />
		    <portlet:param name="statusFilter" value="${statusFilter}" />
		    <portlet:param name="keywords" value="${fn:escapeXml(keywords)}" />
			<portlet:param name="sortOrder" value="${sortOrder}" />
			<portlet:param name="page" value="${page}" />
		</portlet:actionURL>
		<form:form action="${addUrl}" modelAttribute="command">
			<fieldset>
				<legend><s:message code="label.users.createnew" /></legend>
				
			  	<div>
			  		<form:label path="login"><s:message code="label.username" /></form:label>
			  		<form:input path="login" cssErrorClass="field-error" />
			  	</div>
			  	
			  	<c:choose>
					<c:when test="${displayNameGenerationEnabled}">
						<form:hidden path="displayName"  />
					</c:when>
					<c:otherwise>
						<div>
							<form:label path="displayName"><s:message code="label.displayName" /></form:label> 
							<form:input path="displayName" cssErrorClass="field-error" />
						</div>
					</c:otherwise>
				</c:choose>
  	
				<div>
					<form:label path="firstname"><s:message code="label.firstname" /></form:label>
  					<form:input path="firstname" cssErrorClass="field-error" />
				</div>
  	
				<div>
					<form:label path="lastname"><s:message code="label.lastname" /></form:label>
					<form:input path="lastname" cssErrorClass="field-error" />
				</div>
				
				
				<div>
					<form:label path="password"><s:message code="label.password" /></form:label>
					<form:password path="password" cssErrorClass="field-error" autocomplete="off" />
				</div>
				 	
				<div>
					<form:label path="passwordRepeat"><s:message code="label.repeat" /></form:label>
					<form:password path="passwordRepeat" cssErrorClass="field-error" autocomplete="off" />
				</div>
				
				
				<div>
					<button type="submit"><s:message code="label.save" /></button>
				</div>
			</fieldset>
		</form:form>
	</sec:authorize>
</div>



<!-- search form -->

<div id="search-field" class="${search eq '' ? '' : 'activesearch'}">
	<portlet:renderURL var="searchUrl">
		<portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="sortOrder" value="${sortOrder}" />
		<portlet:param name="page" value="${page}" />
	</portlet:renderURL>
	<form action="${searchUrl}" method="post">
		<s:message code="label.search" var="search" />
		<fieldset id="left">
			<legend>${search}</legend>
			<div>
				<label for="keywords">${search}</label>
				<input id="keywords" type="text" maxlength="50" name="keywords" style="margin-bottom: 5px;" value="${fn:escapeXml(keywords)}" />
			</div>
			<div>
				<label for="statusFilter"><s:message code="label.user.statusfilter" /></label>
				<select name="statusFilter" id="statusFilter">
					<c:choose>
						<c:when test="${statusFilter eq 'All'}">
							<option value="All" selected="selected">${all}</option>
						</c:when>
						<c:otherwise>
							<option value="All">${all}</option>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${statusFilter eq 'Active'}">
							<option value="Active" selected="selected">${active}</option>
						</c:when>
						<c:otherwise>
							<option value="Active">${active}</option>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${statusFilter eq 'Inactive'}">
							<option value="Inactive" selected="selected">${inactive}</option>
						</c:when>
						<c:otherwise>
							<option value="Inactive">${inactive}</option>
						</c:otherwise>
					</c:choose>
				</select>
			</div>
			<div>
				<button type="submit">${search}</button>
			</div>
		</fieldset>
	</form>
</div>

</div>


<!-- inactivate confirmation -->

<c:if test="${confirmation eq 'ChangeStatus'}">
	<portlet:actionURL var="confirmationUrl" copyCurrentRenderParameters="true">
		<portlet:param name="action" value="ChangeStatus" />
		<portlet:param name="userId" value="${changeStatusUser.id}" />
		<portlet:param name="status" value="Inactive" />
	</portlet:actionURL>
	<s:message code="label.abort" var="nay" />
	<s:message code="label.yes.deactivate" var="yea" />
	<idm:confirmation nayText="${nay}" yeaText="${yea}" actionUrl="${confirmationUrl}">
		<s:message code="question.users.deactivate" arguments="${changeStatusUser.label}" argumentSeparator="%" />
	</idm:confirmation>
</c:if>


<!-- delete confirmation -->

<c:if test="${confirmation eq 'Delete'}">
	<portlet:actionURL var="confirmationUrl" copyCurrentRenderParameters="true">
		<portlet:param name="action" value="Delete" />
		<portlet:param name="userId" value="${changeStatusUser.id}" />
	</portlet:actionURL>
	<s:message code="label.abort" var="nay" />
	<s:message code="label.yes.delete" var="yea" />
	<idm:confirmation nayText="${nay}" yeaText="${yea}" actionUrl="${confirmationUrl}">
		<s:message code="question.users.delete" arguments="${deleteUser.label}" argumentSeparator="%" />
	</idm:confirmation>
</c:if>


<!-- users table -->

<table>
<thead>
	<tr class="portlet-section-header results-header">
		<th>
			<s:message code="label.username" />
			<idm:sorting currentSortingOrder="${currentSortingOrder}" baseUrl="${sortUrl}"
				field="username" ascLabel="${ascending}" descLabel="${descending}"
				ascImgSrc="${img_arrow_up_png}" descImgSrc="${img_arrow_down_png}" />
		</th>
		<th>
			<s:message code="label.lastname" />, <s:message code="label.firstname" />
			<idm:sorting currentSortingOrder="${currentSortingOrder}" baseUrl="${sortUrl}"
				field="lastname" ascLabel="${ascending}" descLabel="${descending}"
				ascImgSrc="${img_arrow_up_png}" descImgSrc="${img_arrow_down_png}" />
			
		</th>
		<th class="statuscell">
			<s:message code="label.status" />
			<idm:sorting currentSortingOrder="${currentSortingOrder}" baseUrl="${sortUrl}"
				field="softDelete" ascLabel="${ascending}" descLabel="${descending}"
				ascImgSrc="${img_arrow_up_png}" descImgSrc="${img_arrow_down_png}" />
		</th>
		<th class="edit"><s:message code="label.actions" /></th>
	</tr>
</thead>

<tbody>
	<s:message code="label.users.isactive" var="msg_isactive" />
	<s:message code="label.users.edit" var="msg_edit" />
	<s:message code="label.users.delete" var="msg_delete" />
	<c:forEach var="user" items="${users}" varStatus="loopStatus">
		<fmt:formatDate value="${user.deactivationDate.time}" type="date" var="formattedDeactivationDate" />
		<s:message code="label.users.isinactive" var="msg_isinactive" arguments="${formattedDeactivationDate}" argumentSeparator="%" />
		<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
			<td>
				<portlet:renderURL var="editUrl">
					<portlet:param name="ctx" value="${ctx}" />
					<portlet:param name="subctx" value="UserDetail" />
					<portlet:param name="uuid" value="${user.uuid}" />
				</portlet:renderURL>
				<a href="${editUrl}">${user.username}</a>
			</td>
			<td>${user.lastname}, ${user.firstname}</td>
			<td class="statuscell">
				<sec:authorize ifAnyGranted="ROLE_SUPER_ADMIN, ROLE_CLIENT_ADMIN">
					<c:choose>
						<c:when test="${!user.softDelete}">
							<portlet:actionURL var="changeStatusUrl">
								<portlet:param name="ctx" value="${ctx}" />
								<portlet:param name="action" value="ChangeStatus" />
								<portlet:param name="uuid" value="${user.uuid}" />
								<portlet:param name="status" value="Inactive" />
								<portlet:param name="username" value="${user.username}" />
							    <portlet:param name="statusFilter" value="${statusFilter}" />
							    <portlet:param name="keywords" value="${fn:escapeXml(keywords)}" />
								<portlet:param name="sortOrder" value="${sortOrder}" />
								<portlet:param name="page" value="${page}" />
							</portlet:actionURL>
							<a href="${changeStatusUrl}"><img src="${img_active_png}" title="${msg_isactive}" alt="${msg_isactive}" /></a>
						</c:when>
						<c:otherwise>
							<portlet:actionURL var="changeStatusUrl">
								<portlet:param name="ctx" value="${ctx}" />
								<portlet:param name="action" value="ChangeStatus" />
								<portlet:param name="uuid" value="${user.uuid}" />
								<portlet:param name="status" value="Active" />
							    <portlet:param name="statusFilter" value="${statusFilter}" />
							    <portlet:param name="keywords" value="${fn:escapeXml(keywords)}" />
								<portlet:param name="sortOrder" value="${sortOrder}" />
								<portlet:param name="page" value="${page}" />
							</portlet:actionURL>
							<a href="${changeStatusUrl}"><img src="${img_inactive_png}" title="${msg_isinactive}" alt="${msg_isinactive}" /></a>
						</c:otherwise>
			  		</c:choose>
				</sec:authorize>
				<sec:authorize ifAllGranted="ROLE_SECTION_ADMIN">
					<c:choose>
						<c:when test="${!user.softDelete}">
							<img src="${img_active_png}" title="${msg_isactive}" alt="${msg_isactive}" />
						</c:when>
						<c:otherwise>
			  				<img src="${img_inactive_png}" title="${msg_isinactive}" alt="${msg_isinactive}" />
			  			</c:otherwise>
			  		</c:choose>
				</sec:authorize>
			</td>
			<td class="edit">
				<a href="${editUrl}"><img src="${img_user_edit_png}" title="${msg_edit}" alt="${msg_edit}" /></a>
				<sec:authorize ifAnyGranted="ROLE_SUPER_ADMIN">
					<portlet:actionURL var="deleteUrl">
						<portlet:param name="ctx" value="${ctx}" />
						<portlet:param name="action" value="Delete" />
						<portlet:param name="uuid" value="${user.uuid}" />
					    <portlet:param name="statusFilter" value="${statusFilter}" />
					    <portlet:param name="keywords" value="${fn:escapeXml(keywords)}" />
						<portlet:param name="sortOrder" value="${sortOrder}" />
						<portlet:param name="page" value="${page}" />
					</portlet:actionURL>
					<c:if test="${allowDeleteUser}">
						<a href="${deleteUrl}"><img src="${img_bin_png}" title="${msg_delete}" alt="${msg_delete}" /></a>
					</c:if>
					<c:if test="${ldap_sync}">
						<portlet:actionURL var="syncUserUrl">
							<portlet:param name="ctx" value="${ctx}" />
							<portlet:param name="action" value="SynchronizeUser" />
							<portlet:param name="uuid" value="${user.uuid}" />
						    <portlet:param name="statusFilter" value="${statusFilter}" />
						    <portlet:param name="keywords" value="${fn:escapeXml(keywords)}" />
							<portlet:param name="sortOrder" value="${sortOrder}" />
							<portlet:param name="page" value="${page}" />
						</portlet:actionURL>
						<a href="${syncUserUrl}"><img src="${user_sync_png}" title="${msg_sync}" alt="${msg_sync}" /></a>
					</c:if>
				</sec:authorize>
			</td>
		</tr>
	</c:forEach>
</tbody>
</table>

<idm:paging itemLimit="${itemLimit}" backwardLimit="5" forwardLimit="10" currentPage="${page}"
	baseUrl="${pageUrl}" itemCount="${itemCount}" />

</div>

</jsp:root>