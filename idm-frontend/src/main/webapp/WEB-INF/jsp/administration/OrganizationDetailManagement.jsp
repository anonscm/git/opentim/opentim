<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:idm="idm-taglib"
	version="2.0">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />


<portlet:resourceURL id="/img/page_edit.png" var="url_page_edit_png" />
<portlet:resourceURL id="/img/group_edit.png" var="url_group_edit_png" />
<portlet:resourceURL id="/img/bin.png" var="url_bin_png" />
<portlet:resourceURL id="/img/bin.png" var="img_bin_png" />
<portlet:resourceURL id="/img/arrow_down_white.png" var="url_arrow_down_png" />
<portlet:resourceURL id="/img/arrow_up_white.png" var="url_arrow_up_png" />

<s:message code="label.search" var="search" />
<s:message code="label.save" var="save" />
<s:message code="label.group" var="groupname"/>
<s:message code="label.orgunit.field.delete" var="delete" />
<s:message code="label.orgunit.detail" var="editOrgUnit"/>
<s:message code="label.orgunitname" var="orgunitName"/>
<s:message code="label.displayName" var="displayName"/>
<s:message code="label.actions" var="actions"/>
<s:message code="label.pages.ascending" var="ascending" />
<s:message code="label.pages.descending" var="descending" />
<s:message code="label.orgunit.key" var="key"/>
<s:message code="label.orgunit.addkey" var="addkey"/>
<s:message code="label.orgunit.value" var="value"/>
<s:message code="label.update.yes" var="yes" />
<s:message code="label.abort" var="abort" />

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<jsp:include page="../common/_msg.jsp" />


<!-- confirmation for update the organisation unit's name -->

<c:if test="${confirmation eq 'UpdateOrgUnit'}">
	<portlet:actionURL var="confirmationUrl">
	    <portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="updateOrgUnit" />
	</portlet:actionURL>
	<s:message code="label.abort" var="nay" />
	<s:message code="label.update.yes" var="yea" />
	<idm:confirmation nayText="${nay}" yeaText="${yea}" actionUrl="${confirmationUrl}">
		<s:message code="question.orgunit.update" />
	</idm:confirmation>
</c:if>


<!-- edit organisation unit form -->

<portlet:actionURL var="updateOrgUnitUrl">
	<portlet:param name="ctx" value="${ctx}" />
	<portlet:param name="orgUnitId" value="${command.id}" />
	<portlet:param name="sortOrder" value="${sortOrder}" />
	<portlet:param name="action" value="updateOrgUnit" />
</portlet:actionURL>

<div id="user">
<form:form action="${updateOrgUnitUrl}" modelAttribute="command">
	<fieldset>
		<legend>${editOrgUnit}</legend>  
    	<div>
    		<form:label path="name">${orgunitName}</form:label>
    		<form:input path="name" cssErrorClass="field-error" />
   		</div>
	    <div>
	    	<form:label path="displayName">${displayName}</form:label>
	    	<form:input path="displayName" cssErrorClass="field-error" />
    	</div>
	    <div>
	    	<form:label path="group">${groupname}</form:label>
	    	<form:select path="group" cssErrorClass="field-error">
	        	<form:options items="${groups}" itemValue="id" itemLabel="label" />
	    	</form:select>
    	</div>
	    <div>
	    	<button type="submit" style="margin-top: 10px;">${save}</button>
		</div>
	</fieldset>
</form:form>
</div>

<!-- add key form -->

<div id="right-field">
<portlet:actionURL var="addKeyUrl">
	<portlet:param name="ctx" value="${ctx}" />
	<portlet:param name="orgUnitId" value="${command.id}" />
	<portlet:param name="sortOrder" value="${sortOrder}" />
	<portlet:param name="action" value="addKey" />
</portlet:actionURL>
<form action="${addKeyUrl}" method="post">
	<fieldset id="left">
		<legend>${addkey}</legend>
		<div>
			<label for="key">${fn:escapeXml(key)}</label>
			<input id="key" name="key" type="text" maxlength="100" />
		</div>
		<div>
			<label for="value">${fn:escapeXml(value)}</label>
			<input id="value" name="value" type="text" maxlength="100" />
		</div>
		<div>
			<button type="submit">${save}</button>
		</div>
   	</fieldset>
</form>
</div>


<div style="clear:both;"></div>


<!-- confirmation for removing a key value pair -->

<c:if test="${confirmation eq 'RemoveKey'}">
	<portlet:actionURL var="confirmationUrl">
	    <portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="key" value="${keyToRemove}" />
		<portlet:param name="action" value="removeKey" />
	</portlet:actionURL>
	<s:message code="label.abort" var="nay" />
	<s:message code="label.yes.delete" var="yea" />
	<idm:confirmation nayText="${nay}" yeaText="${yea}" actionUrl="${confirmationUrl}">
		<s:message code="question.orgunitkey.delete" arguments="${keyToRemove}" argumentSeparator="%" />
	</idm:confirmation>
</c:if>


<!-- organisation units table -->

<form:form action="${updateOrgUnitUrl}" modelAttribute="command">
<form:hidden path="name" />
<form:hidden path="displayName" />
<form:hidden path="group" />
	
<table>
<thead>
	<tr class="portlet-section-header results-header">
		<th>
			${key}
			<idm:sorting currentSortingOrder="${currentSortingOrder}" baseUrl="${sortUrl}"
				field="key" ascLabel="${ascending}" descLabel="${descending}"
				ascImgSrc="${url_arrow_up_png}" descImgSrc="${url_arrow_down_png}" />
		</th>
		<th class="value">${value}</th>
		<th class="edit">${actions}</th>
	</tr>
</thead>

<tbody>
<c:forEach var="orgUnitEntry" items="${command.orgUnitMap.mapEntries}" varStatus="loopStatus">
	<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
		<td><form:label path="orgUnitMap.mapEntries[${loopStatus.index}].value">${fn:escapeXml(orgUnitEntry.key)}</form:label></td>
		<td class="value">
			<form:input path="orgUnitMap.mapEntries[${loopStatus.index}].value" cssErrorClass="field-error" maxlength="100" />
		</td>
		<td class="edit">
			<portlet:actionURL var="removeKeyUrl">
				<portlet:param name="ctx" value="${ctx}" />
				<portlet:param name="action" value="removeKey" />
				<portlet:param name="key" value="${orgUnitEntry.key}" />
				<portlet:param name="sortOrder" value="${sortOrder}" />
				<portlet:param name="orgUnitId" value="${command.id}" />
			</portlet:actionURL>
			<a href="${removeKeyUrl}"><img alt="${delete}" title="${delete}" src="${img_bin_png}" /></a>
		</td>
	</tr>
</c:forEach>
</tbody>

</table>
<div id="save">
	<button type="submit">${save}</button>
</div>

</form:form>

<!--<idm:paging itemLimit="${itemLimit}" backwardLimit="5" forwardLimit="10" currentPage="${page}"
	baseUrl="${pageUrl}" itemCount="${itemCount}" />-->

</div>

</jsp:root>