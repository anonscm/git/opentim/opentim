<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:sec="http://www.springframework.org/security/tags"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:idm="idm-taglib"
	version="2.0">

<jsp:directive.page language="java"	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"	isELIgnored="false" />

<portlet:resourceURL id="/img/bin.png" var="img_bin_png" />
<portlet:resourceURL id="/img/tick.png" var="img_tick_png" />
<portlet:resourceURL id="/img/user_edit.png" var="img_user_edit_png" />

<s:message code="label.groups.detail" var="groupDetail"/>
<s:message code="label.groups.roles" var="groupRoles"/>
<s:message code="label.groups.user" var="groupUsers"/>
<s:message code="label.groups.createnew" var="createNewGroup" />
<s:message code="label.groups.createrole" var="addRoleLabel" />
<s:message code="label.groupname" var="groupName"/>
<s:message code="label.client" var="client"/>
<s:message code="label.rolename" var="rolename"/>
<s:message code="label.role" var="roleLabel"/>
<s:message code="label.application" var="application"/>
<s:message code="label.write-protected" var="writeProtected" />
<s:message code="label.save" var="save"/>
<s:message code="label.users.adduser" var="addUser" />
<s:message code="label.users.user" var="userLabel" />
<s:message code="label.users.isactive" var="userActive" />
<s:message code="label.users.edit" var="userEdit" />
<s:message code="label.add" var="add" />
<s:message code="label.displayName" var="displayName" />
<s:message code="label.username" var="userName" />
<s:message code="label.firstname" var="firstname" />
<s:message code="label.lastname" var="lastname" />
<s:message code="label.status" var="status" />
<s:message code="label.actions" var="actions" />
<s:message code="label.pages.page" var="page" />
<s:message code="label.pages.first" var="first" />
<s:message code="label.pages.last" var="last" />
<s:message code="label.pages.next" var="next" />
<s:message code="label.pages.previous" var="previous" />
<s:message code="label.pages.of" var="of" />
<s:message code="label.update.yes" var="yes" />
<s:message code="label.abort" var="abort" />
<s:message code="label.groups.removerole" var="removeRoleAction" />
<s:message code="label.groups.removeuser" var="removeUser" />

<!-- TODO css umbenennen -->   
<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<jsp:include page="../common/_msg.jsp" />


<!-- confirmation for update the role's name -->

<c:if test="${confirmation eq 'UpdateGroup'}">
	<portlet:actionURL var="confirmationUrl">
	    <portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="updateGroup" />
	</portlet:actionURL>
	<s:message code="label.abort" var="nay" />
	<s:message code="label.update.yes" var="yea" />
	<idm:confirmation nayText="${nay}" yeaText="${yea}" actionUrl="${confirmationUrl}">
		<s:message code="question.groups.update"  arguments="${command.label}" argumentSeparator="%" />
	</idm:confirmation>
</c:if>


<div>
	<portlet:actionURL var="updateGroupUrl">
		<portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="updateGroup" />
	</portlet:actionURL>
	<form:form action="${updateGroupUrl}" modelAttribute="command">
		<fieldset style="width: 70%; min-width: 450px;" id="user_left">
			<legend>${groupDetail}</legend>
			<div>
				<form:label path="name">${groupName}</form:label>
				<form:input cssStyle="width: 440px" path="name" cssErrorClass="field-error" />
			</div>
			<div>
				<form:label path="displayName">${displayName}</form:label>
				<form:input cssStyle="width: 440px" path="displayName" cssErrorClass="field-error" />
			</div>
			<div>
				<button type="submit" style="margin-top: 10px;">${save}</button>
			</div>
		</fieldset>
	</form:form>
</div>

<div style="clear:both;">
<div id="left">

<!-- add user to group form -->

<h3>${groupUsers}</h3>
<div id="user">
	<portlet:actionURL var="addUserUrl">
	    <portlet:param name="ctx" value="GroupDetailManagement" />
	    <portlet:param name="action" value="addUser" />
	</portlet:actionURL>
	<form action="${addUserUrl}" method="post">
    	<label for="userId">${addUser}</label>
		<select id="userId" name="userId" class="add_new">
	        <c:forEach var="user" items="${allAccounts}">
	            <option value="${user.uuid}">${user.label}</option>
	        </c:forEach>
		</select>
		<button type="submit" class="add_new">${add}</button>
	</form>
</div>


<!-- remove user form group table -->

<table>
<thead>
	<tr class="portlet-section-header results-header">
		<th>${userName}</th>
		<th>${lastname}, ${firstname}</th>
		<th class="edit">${actions}</th>
	</tr>
</thead>

<tbody>
<c:forEach var="user" items="${accounts}" varStatus="loopStatus">
	<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
		<portlet:renderURL var="redirectUserUrl">
			<portlet:param name="ctx" value="UserManagement" />
			<portlet:param name="subctx" value="UserDetail" />
			<portlet:param name="uuid" value="${user.uuid}" />
		</portlet:renderURL>
		<td><a href="${redirectUserUrl}">${user.username}</a></td>
		<td>${user.lastname}, ${user.firstname}</td>
		<td class="edit">
			<portlet:actionURL var="removeUrl">
				<portlet:param name="ctx" value="GroupDetailManagement" />
				<portlet:param name="action" value="removeUser" />
				<portlet:param name="userId" value="${user.uuid}" />
			</portlet:actionURL>
			<a href="${removeUrl}"><img alt="${removeUser}" title="${removeUser}" src="${img_bin_png}" /></a>
		</td>
	</tr>
</c:forEach>
</tbody>
</table>
<!-- TODO add paging -->
</div>


<div id="right">

<!-- add role to group form -->

<h3>${groupRoles}</h3>
<div id="user">
	<sec:authorize ifAnyGranted="ROLE_SUPER_ADMIN, ROLE_CLIENT_ADMIN">
		<portlet:actionURL var="addRoleUrl">
		    <portlet:param name="ctx" value="GroupDetailManagement" />
		    <portlet:param name="action" value="addRole" />
		</portlet:actionURL>
		<form action="${addRoleUrl}" method="post">
			<label for="roleId">${addRoleLabel}</label>
			<select name="roleId" id="roleId" class="add_new">
				<c:forEach var="role" items="${allRoles}">
					<option value="${role.id}">${role.label}</option>
				</c:forEach>
			</select>
	    	<button type="submit" class="add_new">${add}</button>
		</form>
	</sec:authorize>
</div>


<!-- remove role from group table -->

<table>
<thead>
	<tr class="portlet-section-header results-header">
		<th>${displayName} (${rolename})</th>
		<th class="edit">${actions}</th>
	</tr>
</thead>

<tbody>
<c:forEach var="role" items="${roles}" varStatus="loopStatus">
	<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
		<portlet:renderURL var="redirectRoleUrl">
			<portlet:param name="ctx" value="RoleAssociation" />
			<portlet:param name="roleId" value="${role.id}" />
			<portlet:param name="appId" value="${role.application.id}" />
		</portlet:renderURL>
		<td><a href="${redirectRoleUrl}">${role.label}</a></td>
		<td class="edit">
			<sec:authorize ifAnyGranted="ROLE_SUPER_ADMIN, ROLE_CLIENT_ADMIN">
				<portlet:actionURL var="removeRoleUrl">
					<portlet:param name="ctx" value="GroupDetailManagement" />
					<portlet:param name="action" value="removeRole" />
					<portlet:param name="roleId" value="${role.id}" />
				</portlet:actionURL>
				<a href="${removeRoleUrl}"><img alt="${removeRoleAction}" title="${removeRoleAction}" src="${img_bin_png}" /></a>
			</sec:authorize>
	    </td>
	</tr>
</c:forEach>
</tbody>
</table>
<!-- TODO add paging -->
</div>
</div>
</div>

</jsp:root>