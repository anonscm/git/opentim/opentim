<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:sec="http://www.springframework.org/security/tags"
	xmlns:portlet="http://java.sun.com/portlet_2_0" version="2.0">
<jsp:directive.page import="java.util.List"/>
	
<jsp:directive.page import="java.util.Calendar"/>
<jsp:directive.page import="java.text.SimpleDateFormat"/>
<jsp:directive.page import="org.evolvis.idm.identity.account.model.User"/>

<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<div id="benutzerverwaltung">

<jsp:include page="common/_init.jsp" />
<jsp:include page="common/_nav.jsp" />

<jsp:include page="../common/_msg.jsp" />

<portlet:resourceURL id="/img/bin.png" var="url_bin_png" />
<portlet:resourceURL id="/img/tick.png" var="url_tick_png" />
<portlet:resourceURL id="/img/user_edit.png" var="url_user_edit_png" />
<portlet:resourceURL id="/img/group_add.png" var="url_group_add_png" />
<portlet:resourceURL id="/img/key_add.png" var="url_key_add_png" />

<div id="user">
	<portlet:actionURL var="editUserDetailUrl">
		<portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="subctx" value="${subctx}" />
		<portlet:param name="uuid" value="${uuid}" />
		<portlet:param name="action" value="editUserAccount" />
	</portlet:actionURL>
	<form:form action="${editUserDetailUrl}" modelAttribute="command">
		<fieldset>
			<legend><s:message code="label.users.userdetails"/></legend>
			
			<div>
				<div class="label"><s:message code="label.users.username" /></div>
				<div class="input">${command.breakableLogin}</div>
			</div>
				
			<div>
				<div class="label"><s:message code="label.firstname" /></div>
				<div class="input">${command.firstname}</div>
			</div>
			
			<div>
				<div class="label"><s:message code="label.lastname" /></div>
				<div class="input">${command.lastname}</div>
			</div>
			
			<!--<sec:authorize ifAnyGranted="ROLE_SUPER_ADMIN, ROLE_CLIENT_ADMIN">
				<div>
					<s:message code="label.save" var="msg" />
					<button type="submit">${msg}</button>
				</div>
			</sec:authorize>-->
		</fieldset>
		
		<br />
		
		<fieldset style="clear:both; margin-top: 10px;">
			<legend><s:message code="label.users.accountinfo"/></legend>
			
			<jsp:scriptlet>SimpleDateFormat fm = new SimpleDateFormat("dd.MM.yyyy");
 				User user = (User)request.getAttribute("command");</jsp:scriptlet>
			
			<div>
				<label><s:message code="label.users.creationdate"/>:</label>
				<jsp:expression>fm.format(user.getCreationDate().getTime())</jsp:expression>
			</div>
			
			<jsp:scriptlet>
				if ( user.getDeactivationDate() != null ) { 
					Calendar deletedate = (Calendar)user.getDeactivationDate().clone();
					deletedate.add(Calendar.MONTH, 1);
			</jsp:scriptlet> 
 			
 			<div>
 				<label><s:message code="label.users.deactivationdate"/>:</label>
 				<jsp:expression>fm.format(user.getDeactivationDate().getTime())</jsp:expression>
			</div>
			
			<div>
				<label><s:message code="label.users.deletedate"/>:</label>
 				<jsp:expression>fm.format(deletedate.getTime())</jsp:expression>
			</div>
			
			<jsp:scriptlet> } else { </jsp:scriptlet>
			
			<div>
				<label><s:message code="label.users.deactivationdate"/>:</label>
 				&#160;
			</div>
			<div>
				<label><s:message code="label.users.deletedate"/>:</label>
 				&#160;
			</div>
			
			<jsp:scriptlet> } </jsp:scriptlet>
		</fieldset>
	</form:form>
</div>


<fieldset id="user_right">
	<legend><s:message code="label.users.autorisation"/></legend>
	
	<div>
		<portlet:actionURL var="changeInternalRoleUrl">
			<portlet:param name="ctx" value="${ctx}" />
			<portlet:param name="subctx" value="${subctx}" />
			<portlet:param name="uuid" value="${uuid}" />
			<portlet:param name="action" value="ChangeInternalRole" />
		</portlet:actionURL>
		<form action="${changeInternalRoleUrl}" method="post">
			<div>
				<label for="internalRole"><s:message code="label.users.internalrole"/></label>
				<select name="internalRole" id="internalRole">
					<c:forEach var="role" items="${all_internal_roles}">
						<c:choose>
							<c:when	test="${target_internal_role == role}">
								<option value="${role}" selected="selected"><s:message code="${role}"/></option>
							</c:when>
							<c:otherwise>
								<option value="${role}" ><s:message code="${role}"/></option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			
				<button type="submit"><s:message code="label.save" /></button>
			</div>
		</form>
	</div>
	
	<c:if test="${target_internal_role == 'SECTION_ADMIN'}">
	
		<h3><s:message code="label.users.administratedgroups" /></h3>
		
		<portlet:actionURL var="addUrl">
			<portlet:param name="ctx" value="${ctx}" />
			<portlet:param name="subctx" value="${subctx}" />
			<portlet:param name="uuid" value="${uuid}" />
			<portlet:param name="action" value="AddInternalGroup" />
		</portlet:actionURL>
		<div>
			<form action="${addUrl}" method="post">
				<div>
					<label for="groupId"><s:message code="label.users.addgroup" /></label>
					<select name="groupId">
						<!-- list of groups for which the administrator may grant admin rights -->
						<c:forEach var="group" items="${target_assignable_groups_for_administration}">
							<option value="${group.id}">${group.label}</option>
						</c:forEach>
					</select>
					<button type="submit"><s:message code="label.add" /></button>
				</div>
			</form>
		</div>
		
		<!-- list of groups for which the target already has admin rights -->
		<!-- it must be checked for each group if the editor is allowed to remove admin rights -->
		<table>
		<thead>
	        <tr class="portlet-section-header results-header">
				<th><s:message code="label.displayName" /> (<s:message code="label.groupname" />)</th>
				<th><s:message code="label.actions" /></th>
			</tr>
		</thead>
		<tbody>
		<c:forEach var="group" items="${target_groups_with_admin_rights}" varStatus="loopStatus">
			<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
				<td>${group.breakableLabel}</td>   
				<!-- check if editor has rights to change admin status -->
				<!-- if this right is granted place an URL -->
				<jsp:scriptlet>
					List editorGroupsWithAdminRights = (List)pageContext.findAttribute("editor_groups_with_admin_rights");
					Object group = pageContext.findAttribute("group");
					boolean editorHasAdminRightsOnGroup = editorGroupsWithAdminRights.contains(group);
					pageContext.setAttribute("editorHasAdminRightsOnGroup", editorHasAdminRightsOnGroup);
				</jsp:scriptlet>
				<c:choose>
					<c:when test="${editorHasAdminRightsOnGroup}">  
						<td class="edit">
							<portlet:actionURL var="deleteUrl">
								<portlet:param name="ctx" value="${ctx}" />
								<portlet:param name="subctx" value="${subctx}" />
								<portlet:param name="uuid" value="${uuid}" />
								<portlet:param name="action" value="DeleteInternalGroup" />
								<portlet:param name="groupId" value="${group.id}" />
							</portlet:actionURL>
							<s:message code="label.groups.delete" var="msg" />
							<a href="${deleteUrl}"><img src="${url_bin_png}" alt="${msg}" title="${msg}" /></a>
						</td>
					</c:when>
					<c:otherwise>			
						<td class="edit"> - </td>
					</c:otherwise>
				</c:choose>                     
			</tr>
		</c:forEach>
		</tbody>
		</table>
			
	</c:if>
</fieldset>

<div style="clear:both;"></div>

<div id="left">
	
	<h3><s:message code="label.users.groups" /></h3>
	
	<!-- add group form -->
	
	<div id="user">
	
		<div>
			<portlet:actionURL var="addUrl">
				<portlet:param name="ctx" value="${ctx}" />
				<portlet:param name="subctx" value="${subctx}" />
				<portlet:param name="uuid" value="${uuid}" />
				<portlet:param name="action" value="AddGroup" />
			</portlet:actionURL>
			<form action="${addUrl}" method="post" >
				<label for="groupId"><s:message code="label.users.addgroup" /></label>
				<select name="groupId" id="groupId" class="add_new">
					<!-- groups the admin is allowed to assign to user -->
					<c:forEach var="group" items="${editor_groups_with_admin_rights}">
						<option value="${group.id}">${group.label}</option>
					</c:forEach>
				</select>
				<s:message code="label.add" var="msg" />
				<button type="submit" class="add_new">${msg}</button>
			</form>
		</div>
		
	</div>

	<!-- groups table -->

	<table>
	<thead>
		<tr class="portlet-section-header results-header">
			<th><s:message code="label.displayName" /> (<s:message code="label.groupname" />)</th>
			<th><s:message code="label.actions" /></th>
		</tr>
	</thead>
	<tbody>
	<c:forEach var="user_group" items="${target_assigned_groups}" varStatus="loopStatus">
		<jsp:scriptlet>
			List editorGroupsWithAdminRights = (List)pageContext.findAttribute("editor_groups_with_admin_rights");
			Object group = pageContext.findAttribute("user_group");
			boolean editorHasAdminRightsOnGroup = editorGroupsWithAdminRights.contains(group);
			pageContext.setAttribute("editorHasAdminRightsOnGroup", editorHasAdminRightsOnGroup);
		</jsp:scriptlet>
		<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
			<c:choose>
				<c:when test="${editorHasAdminRightsOnGroup}">  
					<portlet:renderURL var="redirectGroupUrl">
					    <portlet:param name="ctx" value="GroupDetailManagement" />
					    <portlet:param name="groupId" value="${user_group.id}" />
					</portlet:renderURL>
					<td><a href="${redirectGroupUrl}">${user_group.label}</a></td>
					<!-- check if editor has rights to change group status -->
					<!-- if this right is granted place an URL -->
					<td class="edit">
						<portlet:actionURL var="deleteUrl">
							<portlet:param name="ctx" value="${ctx}" />
							<portlet:param name="subctx" value="${subctx}" />
							<portlet:param name="uuid" value="${uuid}" />
							<portlet:param name="action" value="DeleteGroup" />
							<portlet:param name="groupId" value="${user_group.id}" />
						</portlet:actionURL>
						<s:message code="label.groups.delete" var="msg" />
						<a href="${deleteUrl}" title="${msg}"><img src="${url_bin_png}" alt="${msg}" /></a>
					</td>
				</c:when>
				<c:otherwise>
					<td>${user_group.label}</td>
					<td class="edit"> - </td>	
				</c:otherwise>
			</c:choose>            
		</tr>
    </c:forEach>
    </tbody>
	</table>
</div>


<div id="right">

	<h3><s:message code="label.users.roles" /></h3>
	
	<!-- add role form -->
	
	<div id="user">
		<sec:authorize ifAnyGranted="ROLE_SUPER_ADMIN, ROLE_CLIENT_ADMIN">
			<div>
				<portlet:actionURL var="addUrl">
					<portlet:param name="ctx" value="${ctx}" />
					<portlet:param name="subctx" value="${subctx}" />
					<portlet:param name="uuid" value="${uuid}" />
					<portlet:param name="action" value="AddRole" />
				</portlet:actionURL>
				<form action="${addUrl}" method="post">
					<label for="roleId"><s:message code="label.users.addrole" /></label>
					<select name="roleId" id="roleId" class="add_new">
						<c:forEach var="role" items="${unused_roles}">
							<option value="${role.id}">${role.label}</option>
						</c:forEach>
					</select>
					<s:message code="label.add" var="msg" />
					<button type="submit" class="add_new">${msg}</button>
				</form>
			</div>
		</sec:authorize>
	</div>
  
	<!-- roles table -->
  
	<table>
    <thead>
		<tr class="portlet-section-header results-header">
			<th><s:message code="label.displayName" /> (<s:message code="label.rolename" />)</th>
			<th><s:message code="label.actions" /></th>
		</tr>
	</thead>

	<tbody>
	<c:forEach var="user_role" items="${user_roles}" varStatus="loopStatus">
		<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
			<portlet:renderURL var="redirectRoleUrl">
				<portlet:param name="ctx" value="RoleAssociation" />
				<portlet:param name="roleId" value="${user_role.id}" />
				<portlet:param name="appId" value="${user_role.application.id}" />
			</portlet:renderURL>
			<td><a href="${redirectRoleUrl}">${user_role.label}</a></td>   
			<td class="edit">
				<portlet:actionURL var="deleteUrl">
					<portlet:param name="ctx" value="${ctx}" />
					<portlet:param name="subctx" value="${subctx}" />
					<portlet:param name="uuid" value="${uuid}" />
					<portlet:param name="action" value="DeleteRole" />
					<portlet:param name="roleId" value="${user_role.id}" />
				</portlet:actionURL>
				<s:message code="label.roles.delete" var="msg" />
				<sec:authorize ifAnyGranted="ROLE_SUPER_ADMIN, ROLE_CLIENT_ADMIN">
					<a href="${deleteUrl}" title="${msg}"><img src="${url_bin_png}" alt="${msg}" /></a>
				</sec:authorize>
				<sec:authorize ifAllGranted="ROLE_SECTION_ADMIN"> - </sec:authorize>
			</td>            
		</tr>
	</c:forEach>
	</tbody>
	</table>
</div>

</div>
</jsp:root>