<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:idm="idm-taglib"
	version="2.0">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<portlet:resourceURL id="/img/page_edit.png" var="url_page_edit_png" />
<portlet:resourceURL id="/img/group_edit.png" var="url_group_edit_png" />
<portlet:resourceURL id="/img/bin.png" var="url_bin_png" />
<portlet:resourceURL id="/img/bin.png" var="img_bin_png" />
<portlet:resourceURL id="/img/cross_gray.png" var="img_cross_gray_png" />
<portlet:resourceURL id="/img/arrow_down.png" var="url_arrow_down_png" />
<portlet:resourceURL id="/img/arrow_up.png" var="url_arrow_up_png" />
<portlet:resourceURL id="/img/multiple.png" var="url_multiple_png" />

<s:message code="label.save" var="save" />
<s:message code="label.displayName" var="displayName"/>
<s:message code="label.actions" var="actions"/>
<s:message code="label.order" var="order"/>
<s:message code="label.name" var="name"/>
<s:message code="label.attributegroup" var="attributegroup"/>
<s:message code="label.attributegroupname" var="attributegroupname"/>
<s:message code="label.attributename" var="attributename"/>
<s:message code="label.datatype" var="datatype"/>
<s:message code="label.additionaldatatypeconstraint" var="additionaldatatypeconstraint"/>
<s:message code="label.attribute.directoryservicename" var="directoryservicename"/>
<s:message code="label.multiple" var="multiple"/>
<s:message code="label.shiftup" var="shiftup" />
<s:message code="label.shiftdown" var="shiftdown" />
<s:message code="label.attributegroup.addattribute" var="addAttribute"/>
<s:message code="label.attributegroup.create" var="createAttributeGroup"/>
<s:message code="label.attributegroup.delete" var="deleteAttributeGroup" />
<s:message code="label.attribute.delete" var="deleteAttribute" />
<s:message code="label.attributegroup.protected" var="protectedAttributeGroup" />
<s:message code="label.attribute.protected" var="protectedAttribute" />
<s:message code="label.attribute.edit" var="editAttribute"/>

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<jsp:include page="../common/_msg.jsp" />


<!-- create new attribute group form -->

<div id="user">
	<portlet:actionURL var="createAttributeGroupUrl">
		<portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="createAttributeGroup" />
	</portlet:actionURL>
	<form:form action="${createAttributeGroupUrl}" modelAttribute="command" >    
		<fieldset>
			<legend>${createAttributeGroup}</legend>  
			
			<div>
				<form:label path="attributeGroup.name">${attributegroupname}</form:label>
	    		<form:input path="attributeGroup.name" cssClass="name" cssErrorClass="field-error"/>
	   		</div>
			
			<div>
				<form:label path="attributeGroup.displayName">${displayName}</form:label>
	    		<form:input path="attributeGroup.displayName" cssClass="displayName" cssErrorClass="field-error"/>
	   		</div>
	   		
			<div>
				<form:label path="attributeGroup.displayOrder">${order}</form:label>
				<form:select path="attributeGroup.displayOrder" cssErrorClass="field-error">
					<c:forEach begin="1" end="${command.countAttributeGroups}" step="1" var="index"> 
						<form:option value="${index}"><s:message code="label.previousToPosition" arguments="${index}" /></form:option>
					</c:forEach>
					<form:option value="${command.countAttributeGroups +1}"><s:message code="label.atTheEnd" arguments="${index}" /></form:option>
				</form:select>
			</div>
			
			<div>
				<form:label path="attributeGroup.multiple">${multiple}</form:label>
				<form:checkbox path="attributeGroup.multiple" id="attributeGroup.multiple" cssClass="multiple" />
			</div>
			
			<div>
				<button type="submit">${save}</button>
			</div>
		</fieldset>
	</form:form>
</div>


<!-- create new attribute form -->

<div id="right-field">
	<portlet:actionURL var="addAttributeUrl">
		<portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="addAttribute" />
	</portlet:actionURL>
	<form:form action="${addAttributeUrl}" modelAttribute="command">
		<fieldset id="left">
			<legend>${addAttribute}</legend>
			
			<div>
				<form:label path="attribute.name">${attributename}</form:label>
     			<form:input path="attribute.name" cssErrorClass="field-error" />
			</div>
			
			<div>
				<form:label path="attribute.displayName">${displayName}</form:label>
				<form:input path="attribute.displayName" cssErrorClass="field-error" />
			</div>
			
			<div>
				<form:label path="attribute.type">${datatype}</form:label>
      			<form:select  path="attribute.type"
      				items="${command.attributeTypes}" itemValue="value"
      				itemLabel="i18nName" cssErrorClass="field-error" />
     		</div>
			
			<div>
				<form:label path="attribute.attributeGroup">${attributegroup}</form:label>
				<form:select  path="attribute.attributeGroup"
					items="${command.attributeGroups}" itemValue="id"
					itemLabel="label" cssErrorClass="field-error" />
			</div>
			
			<div>
				<form:label path="attribute.displayOrder">${order}</form:label>
				<form:select path="attribute.displayOrder">
					<c:forEach begin="1" end="${command.countAttributes}" step="1" var="index"> 
						<form:option value="${index}"><s:message code="label.previousToPosition" arguments="${index}" /></form:option>
					</c:forEach>
					<form:option value="${command.countAttributes +1}"><s:message code="label.atTheEnd" arguments="${index}" /></form:option>
				</form:select>
			</div>
			
			<div>
				<form:label path="attribute.multiple">${multiple}</form:label>
				<form:checkbox path="attribute.multiple" id="attribute.multiple" cssClass="multiple" />
			</div>
			
			<div>
				<button type="submit">${save}</button>
			</div>
		</fieldset>
	</form:form>
</div>


<div style="clear:both;"></div>


<!-- delete attribute group confirmation -->

<c:if test="${confirmation eq 'DeleteAttributeGroup'}">
	<portlet:actionURL var="confirmationUrl">
		<portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="DeleteAttributeGroup" />
		<portlet:param name="attributeGroupId" value="${attributeGroupToDelete.id}" />
	</portlet:actionURL>
	<s:message code="label.abort" var="nay" />
	<s:message code="label.yes.delete" var="yea" />
	<idm:confirmation nayText="${nay}" yeaText="${yea}" actionUrl="${confirmationUrl}">
		<s:message code="question.attributegroup.delete" arguments="${attributeGroupToDelete.label}" argumentSeparator="%" />
	</idm:confirmation>
</c:if>

<!-- remove attribute confirmation -->

<c:if test="${confirmation eq 'RemoveAttribute'}">
	<portlet:actionURL var="confirmationUrl">
		<portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="RemoveAttribute" />
		<portlet:param name="attributeId" value="${attributeToRemove.id}" />
		<portlet:param name="attributeGroupId" value="${attributeGroupToRemoveAttributeFrom.id}" />
	</portlet:actionURL>
	<s:message code="label.abort" var="nay" />
	<s:message code="label.yes.delete" var="yea" />
	<idm:confirmation nayText="${nay}" yeaText="${yea}" actionUrl="${confirmationUrl}">
		<s:message code="question.attribute.remove" arguments="${attributeToRemove.name}%${attributeGroupToRemoveAttributeFrom.label}" argumentSeparator="%" />
	</idm:confirmation>
</c:if>

<!-- attribute groups table -->

<portlet:actionURL var="updateAttributeGroupsUrl">
    <portlet:param name="ctx" value="${ctx}" />
    <portlet:param name="action" value="updateAttributeGroups" />
</portlet:actionURL>

<form:form action="${updateAttributeGroupsUrl}" modelAttribute="command" > 
<table>
<thead>
	<tr class="portlet-section-header results-header">
		<th scope="col" id="orderCol">${order}</th>
		<th scope="col" id="nameCol">${name}</th>
		<th scope="col" id="displayNameCol">${displayName}</th>
		<th scope="col" id="datatypeCol">${datatype}</th>
		<th scope="col" id="actionsCol" class="edit">${actions}</th>
	</tr>
</thead>

<tbody>
<c:forEach var="attributeGroup" items="${command.attributeGroups}" varStatus="grStatus">
	<tr class="top_level">
		<td class="order" headers="orderCol">
			<form:input path="attributeGroups[${grStatus.index}].displayOrder" size="2" cssErrorClass="field-error"/>
			<portlet:actionURL var="shiftupAttributeGroupUrl">
				<portlet:param name="ctx" value="${ctx}" />
				<portlet:param name="action" value="shiftAttributeGroup" />
				<portlet:param name="attributeGroupId" value="${attributeGroup.id}" />
				<portlet:param name="shiftOffset" value="-1" />
			</portlet:actionURL>
			<a href="${shiftupAttributeGroupUrl}" title="${shiftup}"><img src="${url_arrow_up_png}" title="${shiftup}" alt="${shiftup}" /></a>
			<portlet:actionURL var="shiftdownAttributeGroupUrl">
				<portlet:param name="ctx" value="${ctx}" />
				<portlet:param name="action" value="shiftAttributeGroup" />
				<portlet:param name="attributeGroupId" value="${attributeGroup.id}" />
				<portlet:param name="shiftOffset" value="1" />
			</portlet:actionURL>
			<a href="${shiftdownAttributeGroupUrl}" title="${shiftdown}"><img src="${url_arrow_down_png}" title="${shiftdown}" alt="${shiftdown}" /></a>
		</td>
		<td headers="nameCol">
			${attributeGroup.breakableName}
		</td>
		<td headers="displayNameCol"><form:input path="attributeGroups[${grStatus.index}].displayName" cssErrorClass="field-error"/></td>
		<td headers="datatypeCol">${attributegroup} <c:if test="${attributeGroup.multiple}"><img src="${url_multiple_png}" alt="${multiple}" title="${multiple}" /></c:if></td>
		<td headers="actionsCol" class="edit">
			<portlet:actionURL var="deleteAttributeGroupUrl">
				<portlet:param name="ctx" value="${ctx}" />
				<portlet:param name="action" value="DeleteAttributeGroup" />
				<portlet:param name="attributeGroupId" value="${attributeGroup.id}" />
			</portlet:actionURL>
			<c:choose>
				<c:when test="${attributeGroup.writeProtected}">
					<img alt="${protectedAttributeGroup}" title="${protectedAttributeGroup}" src="${img_cross_gray_png}" />
				</c:when>
				<c:otherwise>
					<a href="${deleteAttributeGroupUrl}"><img alt="${deleteAttributeGroup}" title="${deleteAttributeGroup}" src="${img_bin_png}" /></a>
				</c:otherwise>
			</c:choose>
		</td>
	</tr>

	<c:forEach var="attribute" items="${attributeGroup.attributes}" varStatus="attStatus">
		<tr class="sub_level">
			<td class="order">
				<form:input path="attributeGroups[${grStatus.index}].attributes[${attStatus.index}].displayOrder" size="2" cssErrorClass="field-error"/>
				<portlet:actionURL var="shiftupAttributeUrl">
					<portlet:param name="ctx" value="${ctx}" />
					<portlet:param name="action" value="shiftAttribute" />
					<portlet:param name="attributeGroupId" value="${attributeGroup.id}" />
					<portlet:param name="attributeId" value="${attribute.id}" />
					<portlet:param name="shiftOffset" value="-1" />
				</portlet:actionURL>
				<a href="${shiftupAttributeUrl}" title="${shiftup}"><img src="${url_arrow_up_png}" title="${shiftup}" alt="${shiftup}" /></a>
				<portlet:actionURL var="shiftdownAttributeUrl">
					<portlet:param name="ctx" value="${ctx}" />
					<portlet:param name="action" value="shiftAttribute" />
					<portlet:param name="attributeGroupId" value="${attributeGroup.id}" />
					<portlet:param name="attributeId" value="${attribute.id}" />
					<portlet:param name="shiftOffset" value="1" />
				</portlet:actionURL>
				<a href="${shiftdownAttributeUrl}" title="${shiftdown}"><img src="${url_arrow_down_png}" title="${shiftdown}" alt="${shiftdown}" /></a>
			</td>
			<td>
				<portlet:renderURL var="privateDataDetailConfiguration">
					<portlet:param name="ctx" value="PrivateDataDetailConfiguration"/>
					<portlet:param name="attributeId" value="${attribute.id}"/>
					<portlet:param name="attributeGroupId" value="${attributeGroup.id}"/>
				</portlet:renderURL>
				<a href="${privateDataDetailConfiguration}">
					${attribute.breakableName}
				</a>
			</td>
			<td><form:input path="attributeGroups[${grStatus.index}].attributes[${attStatus.index}].displayName" cssErrorClass="field-error"/></td>
			<td><s:message code="${attribute.type}" var="type" />${type} <c:if test="${attribute.multiple}"><img src="${url_multiple_png}" alt="${multiple}" title="${multiple}" /></c:if></td>
			<td class="edit">
				<portlet:renderURL var="viewPrivateDataUrl">
					<portlet:param name="ctx" value="PrivateDataDetailConfiguration"/>
					<portlet:param name="attributeId" value="${attribute.id}"/>
					<portlet:param name="attributeGroupId" value="${attributeGroup.id}"/>
				</portlet:renderURL>
				<a href="${viewPrivateDataUrl}"><img alt="${editAttribute}" title="${editAttribute}" src="${url_page_edit_png}" /></a>
				<c:choose>
					<c:when test="${not attribute.writeProtected}">
						<portlet:actionURL var="removeAttributeUrl">
							<portlet:param name="ctx" value="${ctx}" />
							<portlet:param name="action" value="RemoveAttribute" />
							<portlet:param name="attributeGroupId" value="${attributeGroup.id}" />
							<portlet:param name="attributeId" value="${attribute.id}" />
						</portlet:actionURL>
						<a href="${removeAttributeUrl}"><img alt="${deleteAttribute}" title="${deleteAttribute}" src="${img_bin_png}" /></a>
					</c:when>
					<c:otherwise>
						<img alt="${protectedAttribute}" title="${protectedAttribute}" src="${img_cross_gray_png}" />
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
	</c:forEach>
</c:forEach>
</tbody>
</table>
<div id="save">
	<button type="submit">${save}</button>
</div>
</form:form>

<!--<idm:paging itemLimit="${itemLimit}" backwardLimit="5" forwardLimit="10" currentPage="${page}" baseUrl="${pagingUrl}" itemCount="${itemCount}" />-->

</div>

</jsp:root>