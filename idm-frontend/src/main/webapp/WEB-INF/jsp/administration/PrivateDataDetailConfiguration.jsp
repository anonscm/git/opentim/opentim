<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:idm="idm-taglib"
	version="2.0">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<portlet:resourceURL id="/img/add.png" var="img_add_png" />
<portlet:resourceURL id="/img/bin.png" var="img_bin_png" />
<portlet:resourceURL id="/img/help.png" var="helpIcon" />

<s:message code="label.attribute.detail" var="attributeDetails"/>
<s:message code="label.attribute.dataTypeRestrictions" var="dataTypeRestrictions"/>
<s:message code="label.attribute.directoryservicename" var="syncoptions"/>
<s:message code="label.displayName" var="displayname"/>
<s:message code="label.order" var="order"/>
<s:message code="label.attributegroup" var="attributegroup"/>
<s:message code="label.name" var="name"/>
<s:message code="label.attribute.directoryservicename" var="directoryservicename"/>

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<jsp:include page="../common/_msg.jsp" />

<portlet:actionURL var="updateAttributeUrl">
	<portlet:param name="ctx" value="PrivateDataDetailConfiguration" />
	<portlet:param name="action" value="updateAttribute" />
</portlet:actionURL>

<form:form modelAttribute="command" action="${updateAttributeUrl}">

	<fieldset id="user_left">
		<legend>${attributeDetails}</legend>

    	<form:hidden path="attribute.id" />

		<div>
			<form:label path="attribute.name">${name}</form:label>
    		<form:input path="attribute.name" cssErrorClass="field-error" readonly="true" />
		</div>
		
		<div>
			<form:label path="attribute.displayName">${displayname}</form:label>
			<form:input path="attribute.displayName" cssErrorClass="field-error" />
		</div>

		<c:if test="${command.attribute.type eq 'VALIDATABLE'}">
			<div>
				<form:label path="attribute.additionalTypeConstraint">
					<s:message code="label.attribute.pattern" />
				</form:label>
	     		<form:input path="attribute.additionalTypeConstraint" cssErrorClass="field-error" />
			</div>
		</c:if>
		
		<c:if test="${command.attribute.type eq 'BLOB'}">
			<div>
				<s:message code="help.attribute.fileSize" var="help_fileSize" />
				<idm:helpLabel path="maxFileSize" helpText="${help_fileSize}" iconSrc="${helpIcon}">
					<s:message code="label.attribute.fileSize" />
				</idm:helpLabel>
				<form:input path="maxFileSize" title="${help_fileSize}" cssErrorClass="field-error" />
			</div>
			<div>
				<form:label path="allowedFileExtensions">
					<s:message code="label.attribute.fileExtensions" />
				</form:label>
	     		<form:input path="allowedFileExtensions" cssErrorClass="field-error" />
			</div>
		</c:if>
		
		<c:if test="${command.attribute.type eq 'SELECTLIST'}">
			<div>
				<fieldset style="width: auto;">
					<legend>
						<s:message code="label.attribute.subattributes" />
						<!-- add button to add more sub attributes -->
						<portlet:actionURL var="addSubAttributeUrl">
							<portlet:param name="ctx" value="${ctx}" />
							<portlet:param name="action" value="addSubAttribute" />
							<portlet:param name="attributeId" value="${command.attribute.id}" />
						</portlet:actionURL>
						<a href="${addSubAttributeUrl}"><img src="${img_add_png}" class="add_button" alt="label.attribute.subattribute.add" /></a>
					</legend>
					
					<c:forEach items="${command.attribute.subAttributes}" var="subAttribute" varStatus="subAttributesLoop">
						<div>
							<div class="label" style="clear: both; width: 100%;">
								<s:message code="label.attribute.subattribute" arguments="${subAttributesLoop.index + 1}" />
								<portlet:actionURL var="removeSubAttributeUrl">
									<portlet:param name="ctx" value="${ctx}" />
									<portlet:param name="action" value="removeSubAttribute" />
									<portlet:param name="attributeId" value="${command.attribute.id}" />
									<portlet:param name="subAttributeIndex" value="${subAttributesLoop.index}" />
								</portlet:actionURL>
								<a href="${removeSubAttributeUrl}" style="vertical-align: middle;"><img src="${img_bin_png}" alt="label.attribute.subattribute.remove" /></a>
							</div>
							
							<form:label cssStyle="width: 210px; float: left;" path="attribute.subAttributes[${subAttributesLoop.index}].displayName">
								<s:message code="label.attribute.subattribute.displayname" />
							</form:label>
							<form:input cssStyle="float: left;" path="attribute.subAttributes[${subAttributesLoop.index}].displayName" cssErrorClass="field-error" />
							
							<form:label cssStyle="width: 210px; float: left;" path="attribute.subAttributes[${subAttributesLoop.index}].name">
								<s:message code="label.attribute.subattribute.name" />
							</form:label>
							<form:input cssStyle="float: left;" path="attribute.subAttributes[${subAttributesLoop.index}].name" cssErrorClass="field-error" />
							
						</div>
					</c:forEach>
				</fieldset>
			</div>
		</c:if>
		
		<c:if test="${!command.attribute.attributeGroup.multiple and !command.attribute.multiple and (command.attribute.type eq 'STRING' or command.attribute.type eq 'VALIDATABLE')}">
			<div>
				<form:label path="attribute.syncOptions">
					<s:message code="label.attribute.directoryservicename" />
				</form:label>
	     		<form:input path="attribute.syncOptions" cssErrorClass="field-error" />
			</div>
		</c:if>
		
		<div>
			<button type="submit"><s:message code="label.save" /></button>
		</div>
	</fieldset>

</form:form>

</div>

</jsp:root>