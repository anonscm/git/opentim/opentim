<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:idm="idm-taglib"
	version="2.0"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<jsp:include page="../common/_msg.jsp" />

<portlet:resourceURL id="/img/arrow_down_white.png" var="url_arrow_down_png" />
<portlet:resourceURL id="/img/arrow_up_white.png" var="url_arrow_up_png" />

<s:message code="label.client" var="client" />
<s:message code="label.displayName" var="displayName"/>
<s:message code="label.actions" var="actions"/>
<s:message code="label.activateClient" var="activateClient"/>
<s:message code="label.pages.ascending" var="ascending" />
<s:message code="label.pages.descending" var="descending" />

<portlet:actionURL var="activateClientURL">
	<portlet:param name="ctx" value="${ctx}" />
	<portlet:param name="action" value="clientActivation" />
</portlet:actionURL>
<form action="${activateClientURL}" method="post">

<table>
<thead>
	<tr class="portlet-section-header results-header">
		<th>
			${displayName} (${client})
			<idm:sorting currentSortingOrder="${currentSortingOrder}" baseUrl="${sortUrl}"
				field="displayName" ascLabel="${ascending}" descLabel="${descending}"
				ascImgSrc="${url_arrow_up_png}" descImgSrc="${url_arrow_down_png}" />
		</th>
		<th class="edit">${actions}</th>
	</tr>
</thead>

<tbody>
<c:forEach var="client" items="${clients}" varStatus="loopStatus">
	<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
		<td>${client.label}</td>
		<td class="edit">
			<c:choose>
				<c:when test="${client.name eq clientname}">
					<input type="radio" name="client" value="${client.name}" checked="checked" />
				</c:when>
				<c:otherwise>
					<input type="radio" name="client" value="${client.name}" />
				</c:otherwise>
			</c:choose>
		</td>
	</tr>
</c:forEach>
</tbody>

<tfoot>
	<tr class="choose">
		<td colspan="3">
			<button type="submit">${activateClient}</button>
			<!-- <a href="${activateClientURL}">${activateClient}</a> -->
		</td>
	</tr>
</tfoot>
</table>
</form>

<idm:paging itemLimit="${itemLimit}" backwardLimit="5" forwardLimit="10" currentPage="${page}"
	baseUrl="${pageUrl}" itemCount="${itemCount}" />


</div>

</jsp:root>