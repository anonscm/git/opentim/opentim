<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:sec="http://www.springframework.org/security/tags"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:idm="idm-taglib"
	version="2.0">

<jsp:directive.page language="java"	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"	isELIgnored="false" />


<portlet:resourceURL id="/img/bin.png" var="img_bin_png" /> 
<portlet:resourceURL id="/img/group_edit.png" var="img_group_edit_png" /> 
<portlet:resourceURL id="/img/arrow_down_white.png" var="url_arrow_down_png" /> 
<portlet:resourceURL id="/img/arrow_up_white.png" var="url_arrow_up_png" />

<s:message code="label.search" var="search" />
<s:message code="label.groups.createnew" var="createNew"/>
<s:message code="label.groupname" var="groupName"/>
<s:message code="label.client" var="clientLabel"/>
<s:message code="label.groups.delete" var="delete" />
<s:message code="label.groups.edit" var="edit" />
<s:message code="label.actions" var="actions"/>
<s:message code="label.displayName" var="displayName"/>
<s:message code="label.write-protected" var="writeProtected" />
<s:message code="label.save" var="save" />
<s:message code="label.pages.ascending" var="ascending" />
<s:message code="label.pages.descending" var="descending" />
 
<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<jsp:include page="../common/_msg.jsp" />


<!-- create new group form -->

<div id="user">
<sec:authorize ifAnyGranted="ROLE_SUPER_ADMIN, ROLE_CLIENT_ADMIN">
	<portlet:actionURL var="createUrl">
	    <portlet:param name="ctx" value="${ctx}" />
	    <portlet:param name="action" value="Create" />
	    <portlet:param name="keywords" value="${fn:escapeXml(keywords)}" />
		<portlet:param name="sortOrder" value="${sortOrder}" />
		<portlet:param name="page" value="${page}" />
	</portlet:actionURL>
	<form:form action="${createUrl}" modelAttribute="command">
		<fieldset>
		    <legend>${createNew}</legend>
			<div>
				<form:label path="name">${groupName}</form:label>
				<form:input path="name" cssErrorClass="field-error" />
			</div>
			<div>
				<form:label path="displayName">${displayName}</form:label>
				<form:input path="displayName" cssErrorClass="field-error" />
			</div>
			<div>
				<button type="submit" style="margin-top: 10px;">${save}</button>
			</div>
		</fieldset>
	</form:form>
</sec:authorize>
</div>


<!-- search form -->

<div id="search-field">
	<portlet:renderURL var="searchUrl">
		<portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="sortOrder" value="${sortOrder}" />
		<portlet:param name="page" value="${page}" />
	</portlet:renderURL>
	<form action="${searchUrl}" method="post">
		<s:message code="label.search" var="search" />
		<fieldset id="left">
			<legend>${search}</legend>
			<div>
				<label for="keywords">${search}</label>
				<input id="keywords" type="text" maxlength="50" name="keywords" style="margin-bottom: 5px;" value="${fn:escapeXml(keywords)}" />
				<button type="submit">${search}</button>
			</div>
		</fieldset>
	</form>
</div>	


<!-- delete confirmation -->

<c:if test="${confirmation eq 'Delete'}">
	<portlet:actionURL var="confirmationUrl" copyCurrentRenderParameters="true">
		<portlet:param name="action" value="Delete" />
		<portlet:param name="groupId" value="${deleteGroup.id}" />
	</portlet:actionURL>
	<s:message code="label.abort" var="nay" />
	<s:message code="label.yes.delete" var="yea" />
	<idm:confirmation nayText="${nay}" yeaText="${yea}" actionUrl="${confirmationUrl}">
		<s:message code="question.groups.delete" arguments="${deleteGroup.label}" argumentSeparator="%" />
	</idm:confirmation>
</c:if>


<!-- groups table -->

<table>
<thead>
	<tr class="portlet-section-header results-header">
		<th>
			<s:message code="label.displayName" /> (${groupName})
			<idm:sorting currentSortingOrder="${currentSortingOrder}" baseUrl="${sortUrl}"
				field="displayName" ascLabel="${ascending}" descLabel="${descending}"
				ascImgSrc="${url_arrow_up_png}" descImgSrc="${url_arrow_down_png}" />

		</th>
		<th class="edit">${actions}</th>
	</tr>
</thead>

<tbody>
<c:forEach var="group" items="${groups}" varStatus="loopStatus">
	<portlet:renderURL var="editUrl">
		<portlet:param name="ctx" value="GroupDetailManagement" />
		<portlet:param name="groupId" value="${group.id}" />
	</portlet:renderURL>
	
	<portlet:actionURL var="deleteUrl">
		<portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="Delete" />
		<portlet:param name="groupId" value="${group.id}" />
	    <portlet:param name="keywords" value="${fn:escapeXml(keywords)}" />
		<portlet:param name="sortOrder" value="${sortOrder}" />
		<portlet:param name="page" value="${page}" />
	</portlet:actionURL>
	
	<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
		<td>
			<portlet:renderURL var="editUrl">
				<portlet:param name="ctx" value="GroupDetailManagement" />
				<portlet:param name="groupId" value="${group.id}" />
			</portlet:renderURL>
			<a href="${editUrl}">${group.label}</a>
		</td>
		<td class="edit">
			<a href="${editUrl}"><img alt="${edit}" title="${edit}" src="${img_group_edit_png}" /></a> 
			<sec:authorize ifAnyGranted="ROLE_SUPER_ADMIN, ROLE_CLIENT_ADMIN">
				<a href="${deleteUrl}"><img alt="${delete}" title="${delete}" src="${img_bin_png}" /></a>
			</sec:authorize>
		</td>
	</tr>
</c:forEach>
</tbody>
</table>

<idm:paging itemLimit="${itemLimit}" backwardLimit="5" forwardLimit="10" currentPage="${page}"
	baseUrl="${pageUrl}" itemCount="${itemCount}" />

</div>

</jsp:root>