<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:idm="idm-taglib"
	version="2.0">

<!-- load img resources -->
<portlet:resourceURL id="/img/page_edit.png" var="url_page_edit_png" />
<portlet:resourceURL id="/img/group_edit.png" var="url_group_edit_png" />
<portlet:resourceURL id="/img/bin.png" var="url_bin_png" />
<portlet:resourceURL id="/img/bin.png" var="img_bin_png" />
<portlet:resourceURL id="/img/cross_gray.png" var="img_cross_gray_png" />

<jsp:directive.page language="java"	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"	isELIgnored="false" />

<s:message code="label.roles.detail" var="editRole" />
<s:message code="label.roles.users" var="roleUsers"/>
<s:message code="label.roles.groups" var="roleGroups"/>
<s:message code="label.roles.adduser" var="addUser" />
<s:message code="label.roles.addgroup" var="addGroup" />
<s:message code="label.groups.delete" var="removeGroup" />
<s:message code="label.users.delete" var="removeUser" />
<s:message code="label.username" var="userName" />
<s:message code="label.firstname" var="firstname" />
<s:message code="label.lastname" var="lastname" />
<s:message code="label.actions" var="actions" />
<s:message code="label.save" var="save" />
<s:message code="label.add" var="add" />
<s:message code="label.rolename" var="rolename" />
<s:message code="label.displayName" var="displayName"/>
<s:message code="label.pages.ascending" var="ascending" />
<s:message code="label.pages.descending" var="descending" />
<s:message code="label.application" var="applicationLabel"/>
<s:message code="label.update.yes" var="yes" />
<s:message code="label.abort" var="abort" />

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<jsp:include page="../common/_msg.jsp" />


<!-- confirmation for update the role's name -->

<c:if test="${confirmation eq 'UpdateRole'}">
	<portlet:actionURL var="confirmationUrl">
	    <portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="updateRole" />
	</portlet:actionURL>
	<s:message code="label.abort" var="nay" />
	<s:message code="label.update.yes" var="yea" />
	<idm:confirmation nayText="${nay}" yeaText="${yea}" actionUrl="${confirmationUrl}">
		<s:message code="question.roles.update" />
	</idm:confirmation>
</c:if>


<!-- edit role form -->

<div>
	<portlet:actionURL var="updateRoleUrl">
		<portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="updateRole" />
	</portlet:actionURL>
	<form:form action="${updateRoleUrl}" modelAttribute="command">
		<fieldset style="width: 70%">
			<legend>${editRole}</legend>
			<div>
				<form:label path="application">${applicationLabel}</form:label>
				<form:select cssStyle="width: 440px;" path="application" cssErrorClass="field-error">
					<form:options itemValue="id" itemLabel="label"  items="${applications}" />
				</form:select>
			</div>
			<div>
				<form:label path="name">${rolename}</form:label>
				<form:input cssStyle="width: 440px" path="name" cssErrorClass="field-error" />
			</div>
			<div>
				<form:label path="displayName">${displayName}</form:label>
				<form:input cssStyle="width: 440px" path="displayName" cssErrorClass="field-error" />
			</div>
			<div>
				<button type="submit">${save}</button>
			</div>
		</fieldset>
	</form:form>
</div>


<!-- add user to role form -->
<div style="clear:both">
<div id="left">

	<h3>${roleUsers}</h3>

	<div id="user">
	
	<portlet:actionURL var="addUserUrl">
		<portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="addUser" />
	</portlet:actionURL>
	<form action="${addUserUrl}" method="post">
	    <div>
	    	<label for="userId">${addUser}</label>
		    <select name="userId" id="userId" cssErrorClass="field-error" class="add_new">
		        <c:forEach var="user" items="${allAccounts}">
		            <option value="${user.uuid}">${user.label}</option>
		        </c:forEach>
		    </select>
	    	<button type="submit" class="add_new">${add}</button>
	    </div>
	</form>
	
	</div>
	
	
	<!-- users in role table -->
	
	<table>
	<thead>
		<tr class="portlet-section-header results-header">
			<th>${userName}</th>
			<th>${lastname}, ${firstname}</th>
			<th class="edit">${actions}</th>
		</tr>
	</thead>
	
	<tbody>
	<c:forEach var="user" items="${accounts}" varStatus="loopStatus">
		<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
			<portlet:renderURL var="redirectUserUrl">
				<portlet:param name="ctx" value="UserManagement" />
				<portlet:param name="subctx" value="UserDetail" />
				<portlet:param name="uuid" value="${user.uuid}" />
			</portlet:renderURL>
			<td><a href="${redirectUserUrl}">${user.username}</a></td>
			<td>${user.lastname}, ${user.firstname}</td>
			<td class="edit">
				<c:choose>
					<c:when test="${user.directAssignment}">
						<portlet:actionURL var="removeUrl">
							<portlet:param name="ctx" value="${ctx}" />
							<portlet:param name="action" value="removeUser" />
							<portlet:param name="userId" value="${user.uuid}" />
						</portlet:actionURL>
						<a href="${removeUrl}"><img alt="${removeUser}" title="${removeUser}" src="${img_bin_png}" /></a>
					</c:when>
					<c:otherwise>
						<s:message code="label.roles.indirectassigned" var="indirectAssigned" />
						<img alt="${indirectAssigned}" title="${indirectAssigned}" src="${img_cross_gray_png}" />
					</c:otherwise>
				</c:choose>
			</td>
		</tr>
	</c:forEach>
	</tbody>
	</table>
	
</div>


<div id="right">

	<h3>${roleGroups}</h3>
	
	<!-- add group to role form -->
	
	<div id="user">
		<portlet:actionURL var="addUrl">
			<portlet:param name="ctx" value="${ctx}" />
			<portlet:param name="action" value="addGroup" />
		</portlet:actionURL>
		<form action="${addUrl}" method="post">
			<div>
				<label for="groupId">${addGroup}</label>
				<select name="groupId" id="groupId" class="add_new">
					<c:forEach var="group" items="${to_use_groups}">
						<option value="${group.id}">${group.label}</option>
					</c:forEach>
				</select>
				<button type="submit" class="add_new">${add}</button>
			</div>
		</form>
	</div>
	
	<!-- groups in role table -->
	
	<table>
    <thead>
		<tr class="portlet-section-header results-header">
			<th>
				<s:message code="label.displayName" /> (<s:message code="label.groupname" />)
			</th>
			<th><s:message code="label.actions" /></th>
		</tr>
	</thead>

    <tbody>
    <c:forEach var="role_group" items="${role_groups}" varStatus="loopStatus">
		<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
			<portlet:renderURL var="redirectGroupUrl">
			    <portlet:param name="ctx" value="GroupDetailManagement" />
			    <portlet:param name="groupId" value="${role_group.id}" />
			</portlet:renderURL>
			<td><a href="${redirectGroupUrl}">${role_group.label}</a></td>
			<td class="edit">
				<portlet:actionURL var="removeGroupUrl">
					<portlet:param name="ctx" value="${ctx}" />
					<portlet:param name="action" value="removeGroup" />
					<portlet:param name="groupId" value="${role_group.id}" />
				</portlet:actionURL>
				<a href="${removeGroupUrl}"><img alt="${removeGroup}" title="${removeGroup}" src="${img_bin_png}" /></a>
			</td>            
         </tr>
    </c:forEach>
    </tbody>
	</table>
</div>
</div>
</div>

</jsp:root>