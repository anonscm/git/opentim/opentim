<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:idm="idm-taglib"
	version="2.0"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<jsp:include page="../common/_msg.jsp" />

<portlet:resourceURL id="/img/help.png" var="helpIcon" />

<portlet:actionURL var="updateClientPropertyMap">
	<portlet:param name="ctx" value="${ctx}" />
	<portlet:param name="action" value="updateClientPropertyMap" />
</portlet:actionURL>

<form:form action="${updateClientPropertyMap}" modelAttribute="command">
	
	<div class="column">
		
		<fieldset>
			<legend><s:message code="label.conf.openSso.sectionLabel" /></legend>
			<div style="text-align: left; float: left"><s:message code="label.conf.externalPropertyNotice" /></div>
			
			<div>
				<form:label path="openSsoServiceUrl"><s:message code="label.conf.openSso.serviceUrl" /></form:label>
				<form:input path="openSsoServiceUrl" readonly="true" cssErrorClass="field-error" />
			</div>
			
			<div>
				<form:label path="openSsoAdminLogin"><s:message code="label.conf.openSso.adminLogin" /></form:label>
				<form:input path="openSsoAdminLogin" readonly="true" cssErrorClass="field-error" />
			</div>
			
			<div>
				<form:label path="openSsoAdminPassword"><s:message code="label.conf.openSso.adminPassword" /></form:label>
				<form:password path="openSsoAdminPassword" readonly="true" showPassword="true" cssErrorClass="field-error" />
			</div>
			
		</fieldset>
		
		<fieldset>
			<legend><s:message code="label.conf.security.sectionLabel" /></legend>
			
			<div>
				<s:message code="help.conf.security.password.pattern" var="help_security_password_pattern" />
				<idm:helpLabel path="securityPasswordPattern" helpText="${help_security_password_pattern}" iconSrc="${helpIcon}">
					<s:message code="label.conf.security.password.pattern" />
				</idm:helpLabel>
				<form:input path="securityPasswordPattern" title="${help_security_password_pattern}" cssErrorClass="field-error" />
			</div>

			<div>
				<form:label path="securityAllowEmailExistenceHintsEnabled"><s:message code="label.conf.security.securityAllowEmailExistenceHints.enabled" /></form:label>
				<form:checkbox path="securityAllowEmailExistenceHintsEnabled" id="securityAllowEmailExistenceHintsEnabled" value="true" />
			</div>
		</fieldset>	
	
		<fieldset>
			<legend><s:message code="label.conf.system.sectionLabel" /></legend>
			
			<div>
				<s:message code="help.conf.system.dailyJobExecutionTime" var="help_system_dailyJobExecutionTime" />
				<idm:helpLabel path="systemDailyJobExecutionTime" helpText="${help_system_dailyJobExecutionTime}" iconSrc="${helpIcon}">
					<s:message code="label.conf.system.dailyJobExecutionTime" />
				</idm:helpLabel>
				<form:input path="systemDailyJobExecutionTime" title="${help_system_dailyJobExecutionTime}" cssErrorClass="field-error" />
			</div>
			
			<div>
				<form:label path="systemDeactivatedAccountLifetimeInDays"><s:message code="label.conf.system.deactivatedAccountLifetimeInDays" /></form:label>
				<form:input path="systemDeactivatedAccountLifetimeInDays" cssErrorClass="field-error" />
			</div>
		</fieldset>
			
		<fieldset>
			<legend><s:message code="label.conf.search.sectionLabel" /></legend>
			
			<div>
				<s:message code="help.conf.search.pagingLimit" var="help_search_pagingLimit" />
				<idm:helpLabel path="searchPagingLimit" helpText="${help_search_pagingLimit}" iconSrc="${helpIcon}">
					<s:message code="label.conf.search.pagingLimit" />
				</idm:helpLabel>
				<form:input path="searchPagingLimit" title="${help_search_pagingLimit}" cssErrorClass="field-error" />
			</div>
			
			<div>
				<s:message code="help.conf.search.fullPrivateDataSearch.enabled" var="help_search_fullPrivateDataSearch_enabled" />
				<idm:helpLabel path="searchFullPrivateDataSearchEnabled" helpText="${help_search_fullPrivateDataSearch_enabled}" iconSrc="${helpIcon}">
					<s:message code="label.conf.search.fullPrivateDataSearch.enabled" />
				</idm:helpLabel>
				<form:checkbox path="searchFullPrivateDataSearchEnabled" id="searchFullPrivateDataSearchEnabled" value="true" title="${help_search_fullPrivateDataSearch_enabled}" />
			</div>
		</fieldset>
		
		<fieldset>
			<legend><s:message code="label.conf.display.sectionLabel" /></legend>
			
			<div>
				<form:label path="displayPrivateDataManagementColumns">
					<s:message code="label.conf.display.privateDataManagement.columns" />
				</form:label>
				<form:input path="displayPrivateDataManagementColumns" cssErrorClass="field-error" />
			</div>
			
		</fieldset>
		
	</div>
	
	<div class="column">
	
		<fieldset>
			<legend><s:message code="label.conf.ldapSync.sectionLabel" /></legend>
			
			<div>
				<form:label path="ldapSyncEnabled"><s:message code="label.conf.ldapSync.enabled" /></form:label>
				<form:checkbox path="ldapSyncEnabled" id="ldapSyncEnabled" value="true" />
			</div>
			
			<div>
				<form:label path="ldapSyncHostAndPort"><s:message code="label.conf.ldapSync.hostAndPort" /></form:label>
				<form:input path="ldapSyncHostAndPort" cssErrorClass="field-error" />
			</div>
			
			<div>
				<form:label path="ldapSyncUserDN"><s:message code="label.conf.ldapSync.userDN" /></form:label>
				<form:input path="ldapSyncUserDN" cssErrorClass="field-error" />
			</div>
			
			<div>
				<form:label path="ldapSyncPassword"><s:message code="label.conf.ldapSync.password" /></form:label>
				<form:password path="ldapSyncPassword" showPassword="true" cssErrorClass="field-error" />
			</div>
			
			<div>
				<form:label path="ldapSyncBaseDN"><s:message code="label.conf.ldapSync.baseDN" /></form:label>
				<form:input path="ldapSyncBaseDN" cssErrorClass="field-error" />
			</div>
			
			<div>
				<form:label path="ldapSyncSearchDN"><s:message code="label.conf.ldapSync.searchDN" /></form:label>
				<form:input path="ldapSyncSearchDN" cssErrorClass="field-error" />
			</div>
			
			<div>
				<s:message code="help.conf.ldapSync.searchFilter" var="help_ldapSync_searchFilter" />
				<idm:helpLabel path="ldapSyncSearchFilter" helpText="${help_ldapSync_searchFilter}" iconSrc="${helpIcon}">
					<s:message code="label.conf.ldapSync.searchFilter" />
				</idm:helpLabel>
				<form:input path="ldapSyncSearchFilter" title="${help_ldapSync_searchFilter}" />
			</div>
		</fieldset>

		<fieldset>
			<legend><s:message code="label.conf.registration.sectionLabel" /></legend>
			
			<div>
				<s:message code="help.conf.registration.displayNameGeneration.enabled" var="help_registration_displayNameGeneration_enabled" />
				<idm:helpLabel path="registrationDisplayNameGenerationEnabled" helpText="${help_registration_displayNameGeneration_enabled}" iconSrc="${helpIcon}">
					<s:message code="label.conf.registration.displayNameGeneration.enabled" />
				</idm:helpLabel>
				<form:checkbox path="registrationDisplayNameGenerationEnabled" id="registrationDisplayNameGenerationEnabled" title="${help_registration_displayNameGeneration_enabled}" value="true" />
			</div>
		
			<div>
				<s:message code="help.conf.registration.displayNameGeneration.pattern" var="help_registration_displayNameGeneration_pattern" />
				<idm:helpLabel path="registrationDisplayNameGenerationPattern" helpText="${help_registration_displayNameGeneration_pattern}" iconSrc="${helpIcon}">
					<s:message code="label.conf.registration.displayNameGeneration.pattern" />
				</idm:helpLabel>
				<form:input path="registrationDisplayNameGenerationPattern" title="${help_registration_displayNameGeneration_pattern}" cssErrorClass="field-error" />
			</div>
					
			<div>
				<form:label path="registrationAlternativeLinkEnabled"><s:message code="label.conf.registration.alternativeLink.enabled" /></form:label>
				<form:checkbox path="registrationAlternativeLinkEnabled" id="registrationAlternativeLinkEnabled" value="true" />
			</div>
			
			<div>
				<form:label path="registrationAlternativeLinkUrl"><s:message code="label.conf.registration.alternativeLink.url" /></form:label>
				<form:input path="registrationAlternativeLinkUrl" cssErrorClass="field-error" />
			</div>
					
			<div>
				<form:label path="registrationSuccessLinkEnabled"><s:message code="label.conf.registration.registrationSuccessLink.enabled" /></form:label>
				<form:checkbox path="registrationSuccessLinkEnabled" id="registrationSuccessLinkEnabled" value="true" />
			</div>
			
			<div>
				<form:label path="registrationSuccessLinkUrl"><s:message code="label.conf.registration.registrationSuccessLink.url" /></form:label>
				<form:input path="registrationSuccessLinkUrl" cssErrorClass="field-error" />
			</div>
			
			<div>
				<form:label path="activationSuccessLinkEnabled"><s:message code="label.conf.registration.activationSuccessLink.enabled" /></form:label>
				<form:checkbox path="activationSuccessLinkEnabled" id="activationSuccessLinkEnabled" value="true" />
			</div>
			
			<div>
				<form:label path="activationSuccessLinkUrl"><s:message code="label.conf.registration.activationSuccessLink.url" /></form:label>
				<form:input path="activationSuccessLinkUrl" cssErrorClass="field-error" />
			</div>
			
			<div>
				<s:message code="help.conf.registration.termsRequired.enabled" var="help_registration_termsRequired_enabled" />
				<idm:helpLabel path="registrationTermsRequiredEnabled" helpText="${help_registration_termsRequired_enabled}" iconSrc="${helpIcon}">
					<s:message code="label.conf.registration.termsRequired.enabled" />
				</idm:helpLabel>
				<form:checkbox path="registrationTermsRequiredEnabled" id="registrationTermsRequiredEnabled" title="${help_registration_termsRequired_enabled}" value="true" />
			</div>
			
			<div>
				<s:message code="help.conf.registration.termsRequired.url" var="help_registration_termsRequired_url" />
				<idm:helpLabel path="registrationTermsRequiredUrl" helpText="${help_registration_termsRequired_url}" iconSrc="${helpIcon}">
					<s:message code="label.conf.registration.termsRequired.url" />
				</idm:helpLabel>
				<form:input path="registrationTermsRequiredUrl" title="${help_registration_termsRequired_url}" cssErrorClass="field-error" />
			</div>
		</fieldset>
	
		<fieldset>
			<legend><s:message code="label.conf.other.sectionLabel" /></legend>
			
			<div>
				<s:message code="help.conf.other.allowDeleteUser.enabled" var="help_other_allowDeleteUser_enabled" />
				<idm:helpLabel path="otherAllowDeleteUserEnabled" helpText="${help_other_allowDeleteUser_enabled}" iconSrc="${helpIcon}">
					<s:message code="label.conf.other.allowDeleteUser.enabled" />
				</idm:helpLabel>
				<form:checkbox path="otherAllowDeleteUserEnabled" id="otherAllowDeleteUserEnabled" title="${help_other_allowDeleteUser_enabled}" value="true" />
			</div>
		</fieldset>
	
	</div>

	<div class="submit">
		<button type="submit"><s:message code="label.save" /></button>
	</div>
	
</form:form>

</div>

</jsp:root>