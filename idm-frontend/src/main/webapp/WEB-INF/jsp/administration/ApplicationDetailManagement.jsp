<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:idm="idm-taglib"
	version="2.0">

<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<s:message code="label.appname" var="applicationName"/>
<s:message code="label.save" var="save"/>
<s:message code="label.displayName" var="displayName" />

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<jsp:include page="../common/_msg.jsp" />


<!-- confirmation for update the role's name -->

<c:if test="${confirmation eq 'UpdateApplication'}">
	<portlet:actionURL var="confirmationUrl">
	    <portlet:param name="ctx" value="${ctx}" />
		<portlet:param name="action" value="updateApplication" />
	</portlet:actionURL>
	<s:message code="label.abort" var="nay" />
	<s:message code="label.update.yes" var="yea" />
	<idm:confirmation nayText="${nay}" yeaText="${yea}" actionUrl="${confirmationUrl}">
		<s:message code="question.groups.update" arguments="${command.label}" argumentSeparator="%" />
	</idm:confirmation>
</c:if>


<portlet:actionURL var="UpdateApplicationUrl">
	<portlet:param name="ctx" value="ApplicationDetailManagement" />
	<portlet:param name="action" value="updateApplication" />
</portlet:actionURL>

<form:form action="${UpdateApplicationUrl}" modelAttribute="command">
	<fieldset id="user_left">
		<legend><s:message code="label.application.detail"/></legend>
		<div>
			<form:label path="name">${applicationName}</form:label>
			<form:input path="name" cssErrorClass="field-error" />
		</div>
		<div>
			<form:label path="displayName">${displayName}</form:label>
			<form:input path="displayName" cssErrorClass="field-error" />
		</div>
		<div>
			<button type="submit">${save}</button>
		</div>
	</fieldset>
</form:form>

</div>
</jsp:root>