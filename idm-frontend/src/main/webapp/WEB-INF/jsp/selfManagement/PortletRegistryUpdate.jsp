<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	version="2.0"
>

<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" />

<h3><s:message code="label.portletRegistryUpdate" /></h3>
<jsp:include page="../common/_msg.jsp" />

<dl>
	<dt>portletId</dt>
	<dd>${registryItem.portletId}</dd>
	<dt>serviceName</dt>
	<dd>${registryItem.serviceName}</dd>
	<dt>baseUrl</dt>
	<dd>${registryItem.baseUrl}</dd>
	<dt>context</dt>
	<dd>${registryItem.context}</dd>
</dl>

</jsp:root>