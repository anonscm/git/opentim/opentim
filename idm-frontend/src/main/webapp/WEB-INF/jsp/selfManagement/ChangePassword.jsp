<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	version="2.0">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<s:message code="label.password.old" var="password_old"/>
<s:message code="label.password.new" var="password_new"/>
<s:message code="label.password.new.repeat" var="password_repeat" />
<s:message code="label.back" var="back"/>
<s:message code="label.save" var="save" />

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<!-- <h2><s:message code="label.password.change" /></h2> -->

<portlet:actionURL var="changePasswordUrl">
	<portlet:param name="ctx" value="ChangePassword" />
	<portlet:param name="action" value="changePassword" />
</portlet:actionURL>

<!-- all errors bound to the model attribute 'command' will be  introduce on this place -->
<!-- success message will be introduce on this place -->
<jsp:include page="../common/_msg.jsp" />

<div id="user" class="my_account">
<form:form action="${changePasswordUrl}" modelAttribute="command">
	<fieldset>
		<legend><s:message code="label.password.change" /></legend>
	
        <div>
        	<form:label path="oldPassword">${password_old}</form:label>
        	<form:password path="oldPassword" cssErrorClass="field-error"/>
        </div>
        <div>
        	<form:label path="password">${password_new}</form:label>
        	<form:password path="password" cssErrorClass="field-error"/>
        </div>
      	<div>
      		<form:label path="passwordRepeat">${password_repeat}</form:label>
        	<form:password path="passwordRepeat" cssErrorClass="field-error"/>
       	</div>
        <div>
        	<button type="submit">${save}</button>
       	</div>
    </fieldset>
</form:form>
</div>

<!-- <div style="clear:both;"></div> -->

<div style="clear:both;">
	<portlet:renderURL var="backUrl">
	    <portlet:param name="ctx" value="AccountSummary" />
	</portlet:renderURL>
	<a href="${backUrl}">${back}</a>
</div>

</div>
</jsp:root>