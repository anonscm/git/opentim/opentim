<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:sec="http://www.springframework.org/security/tags"
	xmlns:portlet="http://java.sun.com/portlet_2_0" version="2.0"
	xmlns:idm="idm-taglib">
<jsp:directive.page import="org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap"/>
<jsp:directive.page import="org.evolvis.idm.security.SecurityContextHolder"/>
<jsp:directive.page import="org.evolvis.idm.identity.privatedata.model.Attribute"/>

<jsp:directive.page import="java.util.List"/>
<jsp:directive.page	import="org.evolvis.idm.identity.privatedata.model.ValueSet" />

<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" />

<portlet:resourceURL id="/img/bin.png" var="img_bin_png" />
<portlet:resourceURL id="/img/clear.png" var="img_clear_png" />
<portlet:resourceURL id="/img/calendar.png" var="img_calendar_png" />
<portlet:resourceURL id="/img/add.png" var="img_add_png" />

<s:message code="label.myaccount" var="myAccount" />
<s:message code="label.user.privatedata" var="privateData" />
<s:message code="label.save" var="save" />
<s:message code="label.delete" var="delete" />
<s:message code="label.group.delete" var="deleteGroup" />
<s:message code="label.group.clear" var="clearGroup" />
<s:message code="label.date" var="date" />
<s:message code="label.time" var="time" />
<s:message code="label.file.upload" var="uploadFile" />
<s:message code="label.file.overwrite" var="overwriteFile" />
<s:message code="label.file.saved" var="savedFile" />

<jsp:scriptlet>
	String columns = SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_DISPLAY_PRIVATEDATAMANAGEMENT_COLUMNS);
	pageContext.setAttribute("columns", columns);
</jsp:scriptlet>

<div id="benutzerverwaltung">

	<jsp:include page="common/_nav.jsp" />
	
	
	<!-- show messages -->
	<jsp:include page="../common/_msg.jsp" />


	<!-- delete confirmation -->
	
	<c:if test="${confirmation eq 'Delete'}">
		<portlet:actionURL var="confirmationUrl" copyCurrentRenderParameters="true">
			<portlet:param name="action" value="deleteValues" />
		    <portlet:param name="valueSetIndex" value="${valueSetToDeleteIndex}" />
		</portlet:actionURL>
		<s:message code="label.abort" var="nay" />
		<s:message code="label.yes.delete" var="yea" />
		<idm:confirmation nayText="${nay}" yeaText="${yea}" actionUrl="${confirmationUrl}">
			<s:message code="question.valueSet.delete" arguments="${valueSetToDelete.name}" argumentSeparator="%" />
		</idm:confirmation>
	</c:if>
	

	<!-- show value sets editing forms -->
	
	<div id="attributes">
	
		<portlet:actionURL var="updateValueSetsUrl">
			<portlet:param name="ctx" value="${ctx}" />
			<portlet:param name="action" value="updateValueSets" />
		</portlet:actionURL>
		<form:form action="${updateValueSetsUrl}" modelAttribute="command">
	
			<!-- value sets loop -->
			<c:forEach var="valueSet" items="${command.valueSets}" varStatus="valueSetLoop">
			
				<!-- set counter for value index -->
				<c:set var="valueIndex" value="0" />
				
				<fieldset class="multi_group">
					<portlet:actionURL var="addValueSetUrl">
						<portlet:param name="ctx" value="${ctx}" />
						<portlet:param name="attributeGroupId" value="${valueSet.attributeGroup.id}" />
						<portlet:param name="addIndex" value="${valueSetLoop.index + 1}" />
						<portlet:param name="action" value="addValueSet" />
					</portlet:actionURL>
					
					<legend>
						${valueSet.attributeGroup.displayName}
						<!-- add button for multiple attribute groups -->
						<c:if test="${valueSet.attributeGroup.multiple}">
							<a href="${addValueSetUrl}"><img src="${img_add_png}" class="add_button" /></a>
						</c:if>
					</legend>
					
					<!-- attributes -->
					<c:forEach var="attribute" items="${valueSet.attributeGroup.attributes}" varStatus="attributeLoop">
						
						<!-- start a new row if number of columns is reached -->
						<div class="column" style="${attributeLoop.index mod columns eq 0 ? 'clear: left' : ''}">
					
							<!-- get all values of the same current attribute (all of them
							     should be displayed with the same label) -->
							<jsp:scriptlet>
								Attribute attribute = (Attribute) pageContext.getAttribute("attribute");
								List attributeValues = ((ValueSet) pageContext.getAttribute("valueSet")).getValuesByAttribute(attribute.getName());
								pageContext.setAttribute("attributeValuesSize", attributeValues.size());
								pageContext.setAttribute("attributeValues", attributeValues);
							</jsp:scriptlet>
								
							<!-- label -->
							<portlet:actionURL var="addValueUrl">
								<portlet:param name="ctx" value="${ctx}" />
								<portlet:param name="action" value="addValue" />
								<portlet:param name="valueSetIndex" value="${valueSetLoop.index}" />
								<portlet:param name="attributeId" value="${attribute.id}" />
								<portlet:param name="addIndex" value="${valueIndex + attributeValuesSize}" />
							</portlet:actionURL>
							<form:label path="valueSets[${valueSetLoop.index}].values[${valueIndex}].value">
								${attribute.displayName}
								<!-- add button for multiple value attributes -->
								<c:if test="${attribute.multiple}">
									<a href="${addValueUrl}"><img src="${img_add_png}" class="add_button" /></a>
								</c:if>
							</form:label>
							
							<!-- input field(s) -->
							<div class="fields">
								<c:forEach var="value" items="${attributeValues}">
									<div>
									
										<portlet:actionURL var="deleteValueUrl">
											<portlet:param name="ctx" value="${ctx}" />
											<portlet:param name="action" value="deleteValue" />
											<portlet:param name="valueSetIndex" value="${valueSetLoop.index}" />
											<portlet:param name="valueIndex" value="${valueIndex}" />
										</portlet:actionURL>
										
										<c:choose>
										
											<!-- file up/download(s) -->
											<c:when test="${attribute.type eq 'BLOB'}">
												<portlet:actionURL var="uploadFileUrl">
													<portlet:param name="ctx" value="${ctx}" />
													<portlet:param name="action" value="uploadFile" />
													<portlet:param name="valueSetIndex" value="${valueSetLoop.index}" />
													<portlet:param name="valueIndex" value="${valueIndex}" />
												</portlet:actionURL>
												<portlet:resourceURL id="downloadAttributeFile" var="downloadFileUrl">
													<portlet:param name="valueSetId" value="${valueSet.id}" />
													<portlet:param name="valueId" value="${value.id}" />
												</portlet:resourceURL>
												<span class="upload_file">
													<c:choose>
														<c:when test="${value.value ne null and value.value ne ''}">
															<!--${savedFile}:-->
															<a href="${downloadFileUrl}" target="_blank">${value.value}</a>
															<a href="${deleteValueUrl}">
																<img src="${img_bin_png}" width="12px" style="vertical-align: -2px;" />
															</a>
														</c:when>
														<c:otherwise>
															<a href="${uploadFileUrl}">${uploadFile}</a>
														</c:otherwise>
													</c:choose>
												</span>
											</c:when>
											
											<!-- text input -->
											<c:otherwise>
												<idm:input valueObject="${value}"
													path="valueSets[${valueSetLoop.index}].values[${valueIndex}]"
													locked="${valueSet.locked or value.locked}" />
												<c:if test="${attribute.type eq 'DATE'}"><img src="${img_calendar_png}" /></c:if>
											</c:otherwise>
											
										</c:choose>
										
										<!-- delete button -->
										<c:if test="${attribute.multiple eq true and attribute.type ne 'BLOB' and not (valueSet.locked or valueSet.lockedRecursive)}">
											<a href="${deleteValueUrl}"><img src="${img_bin_png}" /></a>
										</c:if>
										
									</div>
									
									<!-- update value index counter -->
									<c:set var="valueIndex" value="${valueIndex + 1}" />
								
								</c:forEach> <!-- attribute values loop -->
							</div> <!-- fields -->
							
						</div>
								
					</c:forEach> <!-- attributes loop -->
					
					<!-- delete button if valueSet is not empty or locked -->
					<c:if test="${not empty valueSet.values and valueSet.id ne null and not valueSet.locked and not valueSet.lockedRecursive}">
						<div class="button">
							<!-- delete link -->
							<portlet:actionURL var="deleteValuesUrl">
								<portlet:param name="ctx" value="${ctx}" />
								<portlet:param name="action" value="deleteValues" />
								<portlet:param name="valueSetIndex" value="${valueSetLoop.index}" />
							</portlet:actionURL>
							<a href="${deleteValuesUrl}">
								<c:choose>
									<c:when test="${valueSet.attributeGroup.multiple}">
										<img alt="${deleteGroup}" title="${deleteGroup}" src="${img_bin_png}" style="vertical-align: bottom;" />
										${deleteGroup}
									</c:when>
									<c:otherwise>
										<img alt="${clearGroup}" title="${clearGroup}" src="${img_clear_png}" style="vertical-align: bottom;" />
										${clearGroup}
									</c:otherwise>
								</c:choose>
							</a> 
						</div>
					</c:if>
				</fieldset>
							
			</c:forEach> <!-- value sets loop -->
						
			<!-- save button -->
			<div class="button">
				<button type="submit">${save}</button>
			</div>
		
		</form:form>
	
	</div>

</div>

</jsp:root>