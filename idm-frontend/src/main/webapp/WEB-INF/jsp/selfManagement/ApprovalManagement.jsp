<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:sec="http://www.springframework.org/security/tags"
	xmlns:portlet="http://java.sun.com/portlet_2_0" version="2.0"
	xmlns:idm="idm-taglib">
<jsp:directive.page import="org.evolvis.idm.identity.privatedata.model.Attribute"/>

<jsp:directive.page import="java.util.List"/>
<jsp:directive.page	import="org.evolvis.idm.identity.privatedata.model.ValueSet" />

<jsp:directive.page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false" />

<portlet:resourceURL id="/img/bin.png" var="img_bin_png" />
<portlet:resourceURL id="/img/calendar.png" var="img_calendar_png" />
<portlet:resourceURL id="/img/add.png" var="img_add_png" />

<s:message code="label.myaccount" var="myAccount" />
<s:message code="label.user.privatedata" var="privateData" />
<s:message code="label.save" var="save" />
<s:message code="label.delete" var="delete" />
<s:message code="label.group.delete" var="deleteGroup" />
<s:message code="label.date" var="date" />
<s:message code="label.time" var="time" />
<s:message code="label.file.upload" var="uploadFile" />
<s:message code="label.file.overwrite" var="overwriteFile" />
<s:message code="label.file.saved" var="savedFile" />

<div id="benutzerverwaltung">
	
	<!-- show navigation -->
	<jsp:include page="common/_nav.jsp" />
	
	<!-- show messages -->
	<jsp:include page="../common/_msg.jsp" />
	
	<!-- approvals table -->
	<table>
	<thead>
	    <tr class="portlet-section-header results-header">
			<tr class="portlet-section-header results-header">
				<th>
					<s:message code="label.application" />
					<idm:sorting currentSortingOrder="${currentSortingOrder}" baseUrl="${sortUrl}"
						field="displayName" ascLabel="${ascending}" descLabel="${descending}"
						ascImgSrc="${url_arrow_up_png}" descImgSrc="${url_arrow_down_png}" />
				</th>
				<th><s:message code="label.approval" /></th>
				<th class="edit">${actions}</th>
			</tr>
	    </tr>
	</thead>
		
	<tbody>
	<c:forEach var="application" items="${applications}" varStatus="applicationsLoop">
		<tr class="portlet-section-body results-row${loopStatus.index mod 2 eq 0 ? '-alt' : ''}">
			<td>
				<portlet:renderURL var="editUrl">
					<portlet:param name="ctx" value="ApprovalDetailManagement" />
					<portlet:param name="applicationId" value="${application.id}" />
				</portlet:renderURL>  
				<a href="${editUrl}">${application.label}</a>
			</td>
			<td>
				<!-- TODO show number of approved values and valueSets for this application -->
			</td>
	      	<td class="edit">
				<s:message code="edit" var="msg" />
				<a href="${editUrl}"><img src="${url_page_edit_png}" alt="${msg}" title="${msg}" /></a>
			</td>
		</tr>
	</c:forEach>
	</tbody>
	</table>
	
	<idm:paging itemLimit="${itemLimit}" backwardLimit="5" forwardLimit="10" currentPage="${page}"
		baseUrl="${pageUrl}" itemCount="${itemCount}" />
		
</div>

</jsp:root>