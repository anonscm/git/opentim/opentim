<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	version="2.0">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<s:message code="label.login.new" var="login_new"/>
<s:message code="label.back" var="back"/>

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<portlet:actionURL var="requestLoginChangeUrl">
	<portlet:param name="ctx" value="RequestLoginChange" />
	<portlet:param name="action" value="requestLoginChange" />
</portlet:actionURL>

<jsp:include page="../common/_msg.jsp" />

<c:if test="${msg_success == null}">
	<div id="user" class="my_account">
		<form:form action="${requestLoginChangeUrl}" modelAttribute="command">
			<form:hidden path="activationURL"/>
			<fieldset>
				<legend><s:message code="label.login.change" /></legend>
			
		       	<div>
		        	<form:label path="login">${login_new}</form:label>
		        	<form:input path="login" cssErrorClass="field-error"/>
		        </div>
		        <div class="button">
					<button type="submit"><s:message code="button.save" /></button>
				</div>
		    </fieldset>
		</form:form>
	</div>
</c:if>

<div style="clear:both;">
	<portlet:renderURL var="backUrl">
	    <portlet:param name="ctx" value="AccountSummary" />
	</portlet:renderURL>
	<a href="${backUrl}">${back}</a>
</div>

</div>
</jsp:root>