<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	version="2.0">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<div class="lfr-portlet-toolbar">
	<c:forEach var="navPoint" items="${navPoints}">
	    
	    <portlet:renderURL var="navPointUrl">
	        <portlet:param name="ctx" value="${navPoint.ctx}" />
	    </portlet:renderURL>
	   
	    <c:choose>
	        <c:when test='${(navPoint.ctx eq ctx) or (navPoint.ctx eq "AccountSummary" and (ctx eq "ChangePassword" or ctx eq "AccountDeactivation")) or (navPoint.ctx eq "PrivateDataManagement" and ctx eq "UploadFile")}'>
	            <span class="lfr-toolbar-button ${navPoint.cssClass} current">
	                <a href="${navPointUrl}"><s:message code="${navPoint.name}" /></a>
	            </span>
	        </c:when>
	        <c:otherwise>
	            <span class="lfr-toolbar-button ${navPoint.cssClass}">
                    <a href="${navPointUrl}"><s:message code="${navPoint.name}" /></a>
                </span>
	        </c:otherwise>
        </c:choose>
	    
	</c:forEach>
</div>

</jsp:root>