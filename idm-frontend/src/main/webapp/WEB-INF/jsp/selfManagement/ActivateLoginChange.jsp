<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	version="2.0">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<s:message code="label.login.new" var="login_new"/>
<s:message code="label.back" var="back"/>
<s:message code="label.save" var="save" />

<div id="benutzerverwaltung">

<portlet:actionURL var="confirmCredentialsUrl">
	<portlet:param name="ctx" value="ActivateLoginChange" />
	<portlet:param name="action" value="confirmCredentials" />
</portlet:actionURL>

<jsp:include page="../common/_msg.jsp" />

<c:if test="${msg_success == null and msg_error == null}">
	<div id="user" class="my_account">
		<form:form action="${confirmCredentialsUrl}" modelAttribute="command">
			<form:hidden path="activationKey"/>
			<fieldset>
				<legend><s:message code="label.login.change.confirmpassword" /></legend>
			
			    <div>
					<form:label path="password"><s:message code="label.password" /></form:label>
					<form:password path="password" cssClass="focus" cssErrorClass="field-error" />
				</div>
		        <div class="button">
					<button type="submit"><s:message code="button.confirm" /></button>
				</div>
		    </fieldset>
		</form:form>
	</div>
</c:if>

</div>
</jsp:root>