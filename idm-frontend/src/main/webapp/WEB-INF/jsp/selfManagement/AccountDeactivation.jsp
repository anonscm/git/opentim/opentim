<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:idm="idm-taglib"
	version="2.0">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<s:message code="request.account.delete" var="request_delete"/>
<s:message code="label.account.delete" var="account_delete"/>
<s:message code="label.abort" var="abort"/>
<s:message code="label.delete" var="delete" />

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<portlet:renderURL var="abordUrl">
    <portlet:param name="ctx" value="AccountSummary" />
</portlet:renderURL>
	
<portlet:actionURL var="accountDeactivationUrl">
	<portlet:param name="ctx" value="AccountDeactivation" />
	<portlet:param name="action" value="deactivateAccount" />
</portlet:actionURL>

<!-- all errors bound to the model attribute 'command' will be  introduce on this place -->
<!-- success message will be introduce on this place -->
<jsp:include page="../common/_msg.jsp" />

	<div>
		<idm:confirmation nayText="${abort}" yeaText="${delete}" actionUrl="${accountDeactivationUrl}">
			${request_delete}
		</idm:confirmation>
	</div>

</div>
</jsp:root>