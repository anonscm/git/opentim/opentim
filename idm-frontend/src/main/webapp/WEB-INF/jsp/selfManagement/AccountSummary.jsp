<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:sec="http://www.springframework.org/security/tags"
	xmlns:portlet="http://java.sun.com/portlet_2_0" version="2.0">

<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<portlet:resourceURL id="/img/document-properties.png" var="document_properties_png" />
<portlet:resourceURL id="/img/contact-new.png" var="contact_new_png" />
<portlet:resourceURL id="/img/application-certificate.png" var="application_certificate_png" />
<portlet:resourceURL id="/img/dialog-error.png" var="dialog_error_png" />

<s:message code="label.myaccount" var="myAccount" />
<s:message code="label.account.delete" var="deleteAccount" />
<s:message code="text.account.delete" var="deleteAccountText" />
<s:message code="label.user.privatedata" var="privateData" />
<s:message code="text.user.privatedata" var="privateDataText" />
<s:message code="label.accountData" var="changeAccountData" />
<s:message code="text.password.change" var="changePasswordText" />
<s:message code="label.login.change" var="changeLoginLabel" />
<s:message code="text.login.change" var="changeLoginText" />
<s:message code="text.email.change" var="changeEmailText" />
<s:message code="label.edit" var="edit" />
<s:message code="label.approve" var="approve" />
<s:message code="label.abrogate" var="abrogate" />
<s:message code="label.certificates" var="certificates" />
<s:message code="text.certificates" var="certificatesText" />

<portlet:renderURL var="privateDataEditUrl">
	<portlet:param name="ctx" value="PrivateDataManagement" />
</portlet:renderURL>

<portlet:renderURL var="changePasswordUrl">
	<portlet:param name="ctx" value="ChangePassword" />
</portlet:renderURL>

<portlet:renderURL var="changeLoginUrl">
	<portlet:param name="ctx" value="RequestLoginChange" />
</portlet:renderURL>
	   
<portlet:renderURL var="accountDeactivationUrl">
    <portlet:param name="ctx" value="AccountDeactivation" />
</portlet:renderURL>

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<div class="account">

<jsp:include page="../common/_msg.jsp" />

<div class="change_password">
	<fieldset>
		<legend>${changeAccountData}</legend>
		<img src="${contact_new_png}"/>
		<a href="${changePasswordUrl}">${changePasswordText}</a><br />
		<a href="${changeLoginUrl}">${changeLoginText}</a>
	</fieldset>
</div>

<div class="master_data">
	<fieldset>
		<legend>${privateData}</legend>
		<img src="${document_properties_png}"/>${privateDataText}<br />
		<a href="${privateDataEditUrl}">${edit}</a><br />
		<!-- <a href="">${approve}</a><br /> -->
		<!-- <a href="">${abrogate}</a> -->
	</fieldset>
</div>

<!-- <div class="certificates">
	<fieldset>
		<legend>${certificates}</legend>
		<img src="${application_certificate_png}"/>
		<a href="">${certificatesText}</a>
	</fieldset>
</div> -->

<div class="delete_account">
	<fieldset>
		<legend>${deleteAccount}</legend>
		<img src="${dialog_error_png}"/>
		<a href="${accountDeactivationUrl}">${deleteAccountText}</a>
	</fieldset>
</div>

</div>

</div>

</jsp:root>