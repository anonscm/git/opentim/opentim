<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:sec="http://www.springframework.org/security/tags"
	xmlns:portlet="http://java.sun.com/portlet_2_0" version="2.0">

<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<s:message code="label.back" var="back"/>
<s:message code="label.file.upload" var="uploadFile" />

<div id="benutzerverwaltung">

<jsp:include page="common/_nav.jsp" />

<h2>${uploadFile}</h2>

<!-- all errors bound to the model attribute 'command' will be  introduce on this place -->
<!-- success message will be introduce on this place -->
<jsp:include page="../common/_msg.jsp" />

<portlet:actionURL var="uploadFileUrl">
	<portlet:param name="ctx" value="UploadFile" />
	<portlet:param name="action" value="uploadFile" />
	<portlet:param name="valueSetIndex" value="${valueSetIndex}" />
	<portlet:param name="valueIndex" value="${valueIndex}" />
</portlet:actionURL>

<div class="user" style="width: 50%">

<!-- set help message to show maximum possible file size and allowed file extensions -->
<c:if test="${not empty fileSizeLimit and not empty allowedFileExtensions}">
	<s:message code="help.filenameSizeAndExtensions" arguments="${fileSizeLimit}%${allowedFileExtensions}" argumentSeparator="%" var="help_filename" />
</c:if>
<c:if test="${not empty fileSizeLimit and empty allowedFileExtensions}">
	<s:message code="help.filenameSize" arguments="${fileSizeLimit}" var="help_filename" />
</c:if>
<c:if test="${empty fileSizeLimit and not empty allowedFileExtensions}">
	<s:message code="help.filenameExtensions" arguments="${allowedFileExtensions}" var="help_filename" />
</c:if>

<form method="post" action="${uploadFileUrl}" enctype="multipart/form-data">
	<div>
		<label for="file">
			<s:message code="label.filename" />
		</label>
		<input type="file" name="file" title="${help_filename}" />
	</div>
	
	<div>${help_filename}</div>
	
	<div class="button">
		<button type="submit"><s:message code="button.save" /></button>
	</div>
</form>

<p>
<portlet:renderURL var="backUrl">
    <portlet:param name="ctx" value="PrivateDataManagement" />
</portlet:renderURL>
<a href="${backUrl}">${back}</a>
</p>
</div>

</div>

</jsp:root>