<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	version="2.0">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<s:message code="label.password" var="password"/>
<s:message code="label.password_repeat" var="password_repeat" />
<s:message code="label.save" var="save" />

<div id="benutzerverwaltung">

<h2><s:message code="label.password.enter" /></h2>

<portlet:actionURL var="enterPasswordUrl">
	<portlet:param name="ctx" value="EnterPassword" />
	<portlet:param name="action" value="enterPassword" />
</portlet:actionURL>

<!-- all errors bound to the model attribute 'command' will be  introduce on this place -->
<!-- success message will be introduce on this place -->
<jsp:include page="../common/_msg.jsp" />

<div id="user">
<form:form action="${enterPasswordUrl}" modelAttribute="command">
	<form:hidden path="registerKey" />

	<c:choose>
		<c:when test="${displayNameGenerationEnabled}">
			<form:hidden path="displayName"  />
		</c:when>
		<c:otherwise>
			<div>
				<form:label path="displayName"><s:message code="label.displayName" /></form:label> 
				<form:input path="displayName" cssErrorClass="field-error" />
			</div>
		</c:otherwise>
	</c:choose>
	<div>
		<form:label path="firstname"><s:message code="label.firstname" /></form:label>
		<form:input path="firstname"  disabled='${empty command.firstname ? "" : "disabled"}' cssErrorClass="field-error"/>
	</div>
	<div>
		<form:label path="lastname"><s:message code="label.lastname" /></form:label>
		<form:input path="lastname" disabled='${empty command.lastname ? "" : "disabled"}' cssErrorClass="field-error"/>
	</div>
	<div>
		<form:label path="password">${password}</form:label>
		<form:password path="password" cssErrorClass="field-error"/>
	</div>
	<div>
		<form:label path="passwordRepeat">${password_repeat}</form:label>
		<form:password path="passwordRepeat" cssErrorClass="field-error"/>
	</div>

	<div>
		<button type="submit">${save}</button>
	</div>
</form:form>
</div>

</div>
</jsp:root>