<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	version="2.0"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<s:htmlEscape defaultHtmlEscape="true" />

<c:set var="skipNavigationLinks" value="true" scope="request" />
<jsp:include page="../loginRegister/SetPassword.jsp" />

<div style="clear: both;">
	<portlet:renderURL var="loginUrl">
		<portlet:param name="ctx" value="Login" />
	</portlet:renderURL>
    <a href="${loginUrl}"><s:message code="to.login" /></a>
    |
	<portlet:renderURL var="registerUrl">
		<portlet:param name="ctx" value="EmployeeActivation" />
	</portlet:renderURL>
    <a href="${registerUrl}"><s:message code="to.employeeactivation" /></a>
</div>

</jsp:root>