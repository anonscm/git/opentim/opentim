<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	version="2.0">
	
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<s:message code="label.employee.activate" var="employee_activate"/>
<s:message code="label.emailrequest" var="email_request"/>
<s:message code="label.employee.email" var="email"/>
<s:message code="label.setup" var="setup" />

<div id="benutzerverwaltung">

<portlet:actionURL var="activateEmployeeUrl">
	<portlet:param name="ctx" value="EmployeeActivation" />
	<portlet:param name="action" value="activateEmployee" />
</portlet:actionURL>

<div id="user">

<h3>${employee_activate}</h3>

<!-- all errors bound to the model attribute 'command' will be  introduce on this place -->
<!-- success message will be introduce on this place -->
<jsp:include page="../common/_msg.jsp" />

<form:form action="${activateEmployeeUrl}" modelAttribute="command">

<div style="text-align:left !important;">${email_request}</div>

<div>
	<form:label path="email" cssStyle="width:auto !important; margin-right:1em !important;">${email}</form:label>
	<form:input path="email" cssErrorClass="field-error" size="30" cssStyle="float:left !important; margin-right: 1em;"/>
	<button type="submit" style="clear: right; float: left;">${setup}</button>
</div>

</form:form>
</div>

<div style="clear:both;" >
	<portlet:renderURL var="homeUrl">
		<portlet:param name="portletMode" value="view" />
	</portlet:renderURL>
	<a href="${homeUrl}"><s:message code="to.login" /></a>
	|
	<portlet:renderURL var="reqRecoveryUrl">
		<portlet:param name="ctx" value="ReqRecovery" />
	</portlet:renderURL>
	<a href="${reqRecoveryUrl}"><s:message code="to.recovery" /></a>
</div>

</div>
</jsp:root>