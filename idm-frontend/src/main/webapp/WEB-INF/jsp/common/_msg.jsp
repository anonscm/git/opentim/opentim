<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:sec="http://www.springframework.org/security/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	version="2.0"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />

<!-- all errors bound to the model attribute 'command' -->
<form:form modelAttribute="command">
	<form:errors path="*" cssClass="portlet-msg-error" />
</form:form>

<!-- general error messages (detected manually, not via validator) will be shown here -->
<c:if test="${msg_error != null}">
	<div class="portlet-msg-error">
		${msg_error}
	</div>
</c:if>


<!-- success message will be shown here -->
<c:if test="${msg_success != null}">
	<div class="portlet-msg-success">
		${msg_success}
	</div>
</c:if>

<!-- warnings or hints will be shown here -->
<c:if test="${msg_warning != null}">
	<div class="portlet-msg-warning">
		${msg_warning}
	</div>
</c:if>

<!-- search status will be shown here -->
<c:if test="${msg_searchstatus != null}">
	<div class="portlet-msg-searchstatus">
		${msg_searchstatus}
	</div>
</c:if>

</jsp:root>