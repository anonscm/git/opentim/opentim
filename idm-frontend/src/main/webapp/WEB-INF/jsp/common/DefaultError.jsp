<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:sec="http://www.springframework.org/security/tags"
	xmlns:portlet="http://java.sun.com/portlet_2_0" version="2.0">

<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" />


<h1><s:message code="exception.generalerror.title"/></h1>

<p>${exception.localizedMessage == null ? exception : exception.localizedMessage }<br/>
<s:message code="exception.contact.admin"/></p>

<p>${exception.class}</p>

<p>
<portlet:renderURL var="homeUrl">
    <portlet:param name="portletMode" value="view" />
</portlet:renderURL>
<a href="${homeUrl}"><s:message code="label.home"/></a>
</p>

</jsp:root>