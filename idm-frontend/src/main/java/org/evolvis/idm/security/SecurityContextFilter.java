package org.evolvis.idm.security;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.RenderFilter;
import javax.portlet.filter.ResourceFilter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * PortletFilter for setting SecurityContext in Action- and Render-Phase.
 * 
 * @author Tino Rink
 */
public class SecurityContextFilter implements ActionFilter, RenderFilter, ResourceFilter {

	private final Log logger = LogFactory.getLog(getClass());

	@Override
	public void doFilter(ActionRequest request, ActionResponse response, FilterChain chain) throws IOException, PortletException {
		createSecurityContext(request);
		chain.doFilter(request, response);
	}

	@Override
	public void doFilter(RenderRequest request, RenderResponse response, FilterChain chain) throws IOException, PortletException {
		createSecurityContext(request);
		chain.doFilter(request, response);
	}
	
	@Override
	public void doFilter(ResourceRequest request, ResourceResponse response, FilterChain chain) throws IOException, PortletException {
		createSecurityContext(request);
		chain.doFilter(request, response);
	}
	
	private void createSecurityContext(PortletRequest request) {
		if (logger.isTraceEnabled()) {
			logger.trace("create and set SessionContext");
		}
		try {
			SecurityContextHolder.setContext(SecurityContextFactory.getBy(request));
		}
		catch (Exception e) {
			logger.warn("Create and set SessionContext failed", e);
		}
	}
	
	@Override
	public void destroy() {
		logger.info("destroy Filter");
	}

	@Override
	public void init(FilterConfig filterConfig) {
		logger.info("init Filter");
	}
}
