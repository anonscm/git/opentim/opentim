package org.evolvis.idm.security;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.evolvis.idm.relation.permission.service.InternalRoleManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.Authentication;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UsernameNotFoundException;

import com.liferay.portal.model.Role;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * Service that allows for retrieving a UserDetails object based on 
 * an <tt>Authentication</tt> object.
 * 
 * @author Dmytro Mayster
 *
 */
public class IdmUserDetailsService implements AuthenticationUserDetailsService {
	private final Log logger = LogFactory.getLog(getClass());
	
	private final String rolePrefix = "ROLE_";
	
	@Autowired
	protected AccountManagement accountManagement;
	
	@Autowired
	protected InternalRoleManagement internalRoleManagement;

	/**
	 *	Creates an {@link SpringSecurityUser} object and populates it with user detail from IDM
	 *
	 * @param authentication The pre-authenticated authentication token
	 * @return UserDetails for the given authentication token, never null.
	 * @throws UsernameNotFoundException
	 *             if no user details can be found for the given authentication
	 *             token
	 */
	@Override
	public UserDetails loadUserDetails(Authentication authentication)
			throws UsernameNotFoundException {
		String username = authentication.getName();
		logger.info("loadUserDetails() for user: " + username);
		String uuid = new String();
		
		SpringSecurityUser springSecurityUser = null;
		Account account;
		try {
			uuid = UserLocalServiceUtil.getUser(Long.valueOf(username)).getUuid();
			String password = authentication.getCredentials().toString();
			account = loadUserByUsername(uuid);
			if (account != null ) {
				springSecurityUser = new SpringSecurityUser(account.getUuid(), account.getUsername(), password, true, true, true, true, getAuthorities(uuid));
				logger.info("loadIdmUserDetails(), uuid: " + uuid);
			} else {
			//TODO: workaround for a liferay user that is not present in IDM
				com.liferay.portal.model.User liferayUser = UserLocalServiceUtil.getUserByUuid(uuid);
				List<Role> roles = RoleLocalServiceUtil.getUserRoles(liferayUser.getUserId());
				for (Role role : roles) {
					if (role.getName().equalsIgnoreCase("administrator")) {
						springSecurityUser = new SpringSecurityUser(uuid, liferayUser.getLogin(), password, true, true, true, true, 
								new GrantedAuthority[] {new GrantedAuthorityImpl(rolePrefix + InternalRoleType.SUPER_ADMIN)});
					}
				}
			//End To do
			}
		} catch (Exception e) {
			logger.warn("Error while retrieving user roles", e);
		}
		
		if(springSecurityUser == null )
			throw new UsernameNotFoundException("For user " + username + " was no authentication found.");
		
		return springSecurityUser;
	}

	private Account loadUserByUsername(String uuid)	throws BackendException {
		try {
			return accountManagement.getAccountByUuid(uuid);
		} catch (BackendException e) {
			// return null if user not present
			if (e.equals(IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION))
				return null;
			else
				throw e;
		}
	}
	
	private GrantedAuthority[] getAuthorities(String uuid) {
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
		try {
			InternalRoleType internalRoleType = internalRoleManagement.getInternalRoleForUser(uuid);
			authList.add(new GrantedAuthorityImpl(rolePrefix + internalRoleType.name()));
			logger.info("getAuthorities() entered, internalRole = " + internalRoleType.name());
		} catch (BackendException e) {
			logger.error("Error while retrieving user roles", e);
		}
		
		return authList.toArray(new GrantedAuthority[] {});
	}

}
