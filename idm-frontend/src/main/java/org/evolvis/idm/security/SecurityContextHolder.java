package org.evolvis.idm.security;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.exception.Errors;
import org.evolvis.idm.common.exception.ServiceException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.util.BackendServiceUtil;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.relation.permission.model.InternalRoleType;

public class SecurityContextHolder {

	private static final Log logger = LogFactory.getLog(SecurityContextHolder.class);
	
	private static final Map<String, ClientPropertyMap> CLIENT_PROPERTY_MAP_CACHE = new HashMap<String, ClientPropertyMap>();

	private static final ThreadLocal<SecurityContext> tlSecurityContext = new ThreadLocal<SecurityContext>();

	public static void setContext(final SecurityContext securityContext) {
		tlSecurityContext.set(securityContext);
	}

	private static SecurityContext getContext() {
		if (tlSecurityContext.get() != null) {
			if (tlSecurityContext.get().getActiveClient() == null) {
				throw new ServiceException(Errors.TENANT_NOT_AVAILABLE);
			}
			return tlSecurityContext.get();
		}
		else {
			throw new ServiceException(Errors.BACKEND_NOT_AVAILABLE);
		}
	}

	public static Long getLiferayCompanyId() {
		final SecurityContext context = getContext();
		return context != null ? context.getLiferayCompanyId() : null;
	}

	public static String getClientName() {
		final SecurityContext context = getContext();
		return context != null ? context.getClientName() : null;
	}

	public static String getClientHost() {
		final SecurityContext context = getContext();
		return context != null ? context.getClientHost() : null;
	}

	public static String getActiveClientRealm() {
		final SecurityContext context = getContext();
		return context != null ? context.getActiveClientRealm() : null;
	}

	/**
	 * @return the original client of the user even if an administrator changes
	 *         his active client or the employee activation forces another
	 *         client manually.
	 * 
	 *         This client is needed for activities like password recovery which
	 *         needs to be executed on the original client of the user no matter
	 *         if the client has been changed by the user or system.
	 */
	public static String getUserClientRealm() {
		final SecurityContext context = getContext();
		return context != null ? context.getUserClientRealm() : null;
	}

	public static String getUUID() {
		final SecurityContext context = getContext();
		return context != null ? context.getUUID() : null;
	}

	public static Client getClient() {
		final SecurityContext context = getContext();
		return context != null ? context.getActiveClient() : null;
	}

	public static void setClient(Client client) {
		getContext().setActiveClient(client);
	}
	
	public static void setUserClientRealm(String realm) {
		getContext().setUserClientRealm(realm);
	}

	public static InternalRoleType getInternalRole() {
		org.springframework.security.context.SecurityContext securityContext = org.springframework.security.context.SecurityContextHolder
				.getContext();
		if (securityContext == null || securityContext.getAuthentication() == null)
			throw new org.springframework.security.AuthenticationCredentialsNotFoundException(
					"No valid security context found.");

		return ((SpringSecurityUser) securityContext.getAuthentication().getPrincipal()).getInternalRole();
	}
	
	public static int getPagingLimit() {
		final SecurityContext context = getContext();
		if (context == null)
			return 10;
		else {
			return Integer.valueOf(getClientPropertyMap().get(ClientPropertyMap.KEY_SEARCH_PAGINGLIMIT)).intValue();
		}
	}
	
	public static ClientPropertyMap getClientPropertyMap() {
		final SecurityContext context = getContext();
		if (context == null)
			return null;
		else if (CLIENT_PROPERTY_MAP_CACHE.get(getClientName()) == null) {
			try {
				ClientPropertyMap clientPropertyMap = BackendServiceUtil.getClientManagementService().getClientPropertyMap(getClientName());
				CLIENT_PROPERTY_MAP_CACHE.put(getClientName(), clientPropertyMap);
			} catch (BackendException e) {
				logger.warn("ClientPropertyMap could not be retrieved", e);
			}
		}
		return CLIENT_PROPERTY_MAP_CACHE.get(getClientName());
	}

	public static void setClientPropertyMap(ClientPropertyMap clientPropertyMap) {
		CLIENT_PROPERTY_MAP_CACHE.put(getClientName(), clientPropertyMap);
	}
}
