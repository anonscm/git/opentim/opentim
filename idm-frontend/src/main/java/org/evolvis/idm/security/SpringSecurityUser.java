package org.evolvis.idm.security;

import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.springframework.security.GrantedAuthority;

/**
 * Models core user information.
 *
 * @author Dmytro Mayster
 */
public class SpringSecurityUser extends org.springframework.security.userdetails.User {
	private static final long serialVersionUID = 1L;
	
	private String uuid;

	public SpringSecurityUser(String uuid, String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked, GrantedAuthority[] authorities)
			throws IllegalArgumentException {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.setUuid(uuid);
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * Determines the higher role of user.
	 *  
	 * @return InternalRoleType the higher role of user
	 */
	public InternalRoleType getInternalRole() {
		List<GrantedAuthority> authorities = Arrays.asList(getAuthorities());
		
		SortedSet<InternalRoleType> internalRoles = new TreeSet<InternalRoleType>();
		for (GrantedAuthority grantedAuthority : authorities) {
		    if(grantedAuthority.equals("ROLE_SUPER_ADMIN")) {
		    	internalRoles.add(InternalRoleType.SUPER_ADMIN);
		    } else if(grantedAuthority.equals("ROLE_CLIENT_ADMIN")) {
		    	internalRoles.add(InternalRoleType.CLIENT_ADMIN);
		    } else if(grantedAuthority.equals("ROLE_SECTION_ADMIN")) {
		    	internalRoles.add(InternalRoleType.SECTION_ADMIN);
		    } else if(grantedAuthority.equals("ROLE_USER")) {
		    	internalRoles.add(InternalRoleType.USER);
		    }
		}
		
		return internalRoles.first();
    }

}
