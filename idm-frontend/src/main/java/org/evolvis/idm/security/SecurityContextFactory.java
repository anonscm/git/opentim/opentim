package org.evolvis.idm.security;

import java.io.IOException;

import javax.portlet.PortletRequest;
import javax.xml.ws.WebServiceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.exception.Errors;
import org.evolvis.idm.common.exception.ServiceException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.util.BackendServiceUtil;
import org.evolvis.idm.common.util.TenantResolver;
import org.evolvis.idm.relation.multitenancy.model.Client;

import com.liferay.portal.model.Company;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;

public class SecurityContextFactory {

	private static final Log logger = LogFactory.getLog(SecurityContextFactory.class);
	
	static SecurityContext getEmpty() {
	    return new SecurityContext(null, null, null, null, null);
	}
	
	static SecurityContext getBy(PortletRequest request) throws ServiceException {		
		try {
			Company company = PortalUtil.getCompany(request);
			User user = request.getRemoteUser() == null ? null : PortalUtil.getUser(request);
			// use saved client in portlet session if present
			// only used for client changes in the administration portlet
			Client client = null;
			if (request.getPortletSession(false) != null && request.getPortletSession().getAttribute("client") != null) {
				client = (Client) request.getPortletSession().getAttribute("client");
				logger.info("Received administrative client call on switched client \""+client.getClientName()+"\" from uuid \""+user.getUuid()+"\"");
			}
			SecurityContext securityContext = createServiceContext(company, user, client);
			
			return securityContext;
		} catch (Exception e) {
			logger.error("Creation of ServiceContext failed", e);
			if (e instanceof ServiceException)
				throw (ServiceException) e;
			else
				throw new ServiceException(Errors.SECURITY_CONTEXT_CREATION_FAILED);
		}
	}
	
	public static SecurityContext createServiceContext(Company company, User user, Client client) throws Exception {
		String activeTenant = "NOTFOUND";
		
		if (client == null) {
			// try to find client by uuid
			if (user != null) {			
				try {
					client = BackendServiceUtil.getAccountManagementService().getClientByUuid(user.getUuid());
				} catch (BackendException e1) {
					logger.warn("Error while retrieving client by uuid="+user.getUuid(), e1);
					logger.debug("Error while retrieving client by uuid="+user.getUuid()+", details: ", e1);
				}				
			}
			// try to find client by tenant configuration in case local liferay admin tries to login
			if (client == null) {
				try {
					activeTenant = TenantResolver.getActiveTenantName();
					client = BackendServiceUtil.getClientManagementService().getClientByName(activeTenant);
				} catch (BackendException be) {
					return getEmpty();
				} catch (WebServiceException wse) {
					if (wse.getCause() instanceof IOException)
						throw new ServiceException(Errors.BACKEND_NOT_AVAILABLE);
					else
						throw wse;
				} catch (Exception e1) {
					logger.error("Error while retrieving client by configuration", e1);
					String uuid = user == null ? "NONE" : user.getUuid();
					logger.error("No client has been found with name: \""+activeTenant+"\" and user \""+uuid+"\"");
					throw e1;
				}
			}
		}
		
		return new SecurityContext(
				company != null ? company.getCompanyId() : null,
				// company.getName() retrieves the initial set name of the company which is
				// not changed on portal instance updates. Therefore the WebId will represent the
				// client/company name.
				// old method: company != null ? company.getName() : null,
				//company != null ? company.getWebId() : null,
				company != null ? company.getVirtualHost() : null, 
				client != null ? "/" + client.getName() : null,
				user != null ? user.getUuid() : null,
				client
		);
	}
}
