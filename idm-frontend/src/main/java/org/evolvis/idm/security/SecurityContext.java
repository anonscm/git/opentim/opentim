package org.evolvis.idm.security;

import org.evolvis.idm.relation.multitenancy.model.Client;

public class SecurityContext {
	
	private Client activeClient;

    private Long liferayCompanyId;

    private String clientHost;
    
    private String userClientRealm;
	
	private String uuid;
	
	SecurityContext(Long liferayCompanyId, String clientHost, String userClientRealm, String uuid, Client client) {
		this.liferayCompanyId = liferayCompanyId;
	    this.clientHost = clientHost;
	    this.userClientRealm = userClientRealm;
		this.uuid = uuid;
		this.activeClient = client;
	}
	
	public Long getLiferayCompanyId() {
        return liferayCompanyId;
    }
	
	public String getClientName() {
		return activeClient == null ? null : activeClient.getName();
	}

	public String getClientHost() {
        return clientHost;
    }
	
	public String getUserClientRealm() {
		return userClientRealm;
	}
	
	public void setUserClientRealm(String userClientRealm) {
		this.userClientRealm = userClientRealm;
	}
	
	public String getActiveClientRealm() {
		return activeClient == null ? null : "/" + activeClient.getName();
	}
	
	public String getUUID() {
		return uuid;
	}
	
	public boolean hasUserContext() {
		return uuid != null;
	}

    public Client getActiveClient() {
		return activeClient;
	}

	public void setActiveClient(Client client) {
		this.activeClient = client;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
        result = prime * result + ((clientHost == null) ? 0 : clientHost.hashCode());
        result = prime * result + ((liferayCompanyId == null) ? 0 : liferayCompanyId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SecurityContext other = (SecurityContext) obj;
        if (uuid == null) {
            if (other.uuid != null)
                return false;
        } else if (!uuid.equals(other.uuid))
            return false;
        if (clientHost == null) {
            if (other.clientHost != null)
                return false;
        } else if (!clientHost.equals(other.clientHost))
            return false;
        if (liferayCompanyId == null) {
            if (other.liferayCompanyId != null)
                return false;
        } else if (!liferayCompanyId.equals(other.liferayCompanyId))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SecurityContext [callerId=" + uuid + ", clientHost=" + clientHost + ", clientId=" + liferayCompanyId + ", clientName=" + getClientName() + ", clientRealm=" + getUserClientRealm() + "]";
    }
}
