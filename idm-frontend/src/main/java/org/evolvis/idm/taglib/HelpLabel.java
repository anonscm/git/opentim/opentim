/**
 * 
 */
package org.evolvis.idm.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.web.servlet.tags.form.LabelTag;

/**
 * @author Michael Kutz, tarent GmbH
 */
public class HelpLabel extends TagSupport {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4378527912764899540L;

	private static final String HELPICON = " <img src=\"%1$s\" title=\"%2$s\" alt=\"%2$s\" />";

	/** Standard attribute of spring spring label tag. */
	private String path;

	/** The additional help text, used as title attribute value. */
	private String helpText;
	
	/** The URL of the image file to be used as indicator. */
	private String iconSrc;
	
	/** The original spring label tag used by this tag. */
	private LabelTag springTag;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int doStartTag() throws JspException {
		springTag = new LabelTag();
		
		springTag.setPageContext(pageContext);
		springTag.setPath(path);
		springTag.setTitle(helpText);

		return springTag.doStartTag();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int doEndTag() throws JspException {
		JspWriter out = pageContext.getOut();
		
		try {
			out.append(String.format(HELPICON, new Object[] {getIconSrc(), getHelpText()}));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return springTag.doEndTag();
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getPath() {
		return this.path;
	}

	/**
	 * @return the helpText
	 */
	public String getHelpText() {
		return helpText;
	}

	/**
	 * @param helpText
	 *            the helpText to set
	 */
	public void setHelpText(String helpText) {
		this.helpText = helpText;
	}

	/**
	 * @return the iconSrc
	 */
	public String getIconSrc() {
		return iconSrc;
	}

	/**
	 * @param iconSrc the iconSrc to set
	 */
	public void setIconSrc(String iconSrc) {
		this.iconSrc = iconSrc;
	}

}
