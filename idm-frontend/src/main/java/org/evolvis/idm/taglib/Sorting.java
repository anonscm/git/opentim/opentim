/**
 * 
 */
package org.evolvis.idm.taglib;

import java.io.IOException;

import javax.portlet.PortletURL;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * @author Michael Kutz, tarent GmbH
 */
public class Sorting extends TagSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5253082441203851743L;

	/**
	 * The current sorting order. String consists of a field name and a direction separated by a
	 * ".", e.g. "name.ASC" or "status.DESC"
	 */
	private String currentSortingOrder;

	/** The name of the data field that will be used for sorting. */
	private String field;

	/** The base URL. */
	private PortletURL baseUrl;

	private String ascLabel;

	private String descLabel;

	private String ascImgSrc;

	private String descImgSrc;

	/**
	 * The format string for a link to a page. First argument is the PortletURL of the page, second
	 * is the title (tool tip) for the link and the third argument is the link's text.
	 */
	private static final String SORTLINK = "<li class=\"%3$s\"><a href=\"%1$s\"><img src=\"%4$s\" title=\"%2$s\" alt=\"%2$s\" /></a></li>";
	private static final String CURRENTSORTLINK = "<li class=\"%3$s\"><img src=\"%4$s\" title=\"%2$s\" alt=\"%2$s\" /></li>";

	private static final String STARTTAG = "<ul class=\"sorting\">";

	private static final String ENDTAG = "</ul>";

	private static final String ASCCLASS = "asc";
	private static final String DESCCLASS = "desc";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();

		try {
			/*
			 * append opening tag
			 */
			out.append(STARTTAG);

			/*
			 * append sorting links
			 */
			if (currentSortingOrder.equals(field + ".ASC")) {
				out.append(String.format(CURRENTSORTLINK, new Object[] {
						getSortUrl(field + ".ASC"), ascLabel, ASCCLASS, ascImgSrc }));
			} else {
				out.append(String.format(SORTLINK, new Object[] { getSortUrl(field + ".ASC"),
						ascLabel, ASCCLASS, ascImgSrc }));
			}
			if (currentSortingOrder.equals(field + ".DESC")) {
				out.append(String.format(CURRENTSORTLINK, new Object[] {
						getSortUrl(field + ".DESC"), descLabel, DESCCLASS, descImgSrc }));
			} else {
				out.append(String.format(SORTLINK, new Object[] { getSortUrl(field + ".DESC"),
						descLabel, DESCCLASS, descImgSrc }));
			}

			/*
			 * append closing tag
			 */
			out.append(ENDTAG);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return super.doStartTag();
	}

	/**
	 * Helper method generating a URL to a certain page.
	 * 
	 * @param page
	 *            number of the page the URL should be referencing.
	 * @return a URL referencing the specified page.
	 */
	private String getSortUrl(String sortOrder) {
		baseUrl.setParameter("sortOrder", sortOrder);
		return baseUrl.toString();
	}

	/**
	 * @param baseUrl
	 *            a PortletURL used as base URL. Should therefore not contain any paging arguments.
	 */
	public void setBaseUrl(PortletURL baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}

	/**
	 * @param field
	 *            the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}

	/**
	 * @return the currentSortingOrder
	 */
	public String getCurrentSortingOrder() {
		return currentSortingOrder;
	}

	/**
	 * @param currentSortingOrder
	 *            the currentSortingOrder to set
	 */
	public void setCurrentSortingOrder(String currentSortingOrder) {
		this.currentSortingOrder = currentSortingOrder;
	}

	public String toString() {
		return String.format("Sorting: field=%1$s, currentSortingOrder=%2$s, baseUrl=%3$s",
				new Object[] { this.field, this.currentSortingOrder, this.baseUrl });
	}

	public void setAscLabel(String ascLabel) {
		this.ascLabel = ascLabel;
	}

	public void setDescLabel(String descLabel) {
		this.descLabel = descLabel;
	}
	
	public void setAscImgSrc(String ascImgSrc) {
		this.ascImgSrc = ascImgSrc;
	}

	public void setDescImgSrc(String descImgSrc) {
		this.descImgSrc = descImgSrc;
	}

}
