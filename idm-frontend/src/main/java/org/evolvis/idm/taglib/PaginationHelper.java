package org.evolvis.idm.taglib;

import java.io.IOException;
import java.io.Writer;

import javax.portlet.PortletURL;

public class PaginationHelper {

	private static final String CSS_CLASS_CURRENTPAGE = "currentpage";
	/** Offset parameter key. */
	static final String OFFSET_PARAMETER_KEY = "offset";
	/** Constant for gap between page links. */
	static final String SPACER = "&nbsp;&nbsp;";
	private PortletURL portletUrl;
	private Integer offset;
	private Integer itemLimit;
	private Integer itemCount;

	public void writeFirstPageLink(final Writer out)
			throws IOException {
		
		portletUrl.setParameter(PaginationHelper.OFFSET_PARAMETER_KEY, "0");
		PaginationHelper.appendLink(out, portletUrl, "&lt;&lt;");
	}

	public void writeLastPageLink(final Writer out)
			throws IOException {
		int lastPageOffset = (getPageCount() - 1) * itemLimit;
		portletUrl.setParameter(PaginationHelper.OFFSET_PARAMETER_KEY, String
		        .valueOf(lastPageOffset));
		PaginationHelper.appendLink(out, portletUrl, "&gt;&gt;");
	}

	public void writeNextPageLink( final Writer out)
			throws IOException {
		portletUrl.setParameter(PaginationHelper.OFFSET_PARAMETER_KEY, String
		        .valueOf(offset + itemLimit));
		PaginationHelper.appendLink(out, portletUrl, "&gt;");
	}

	public void writePageNumberLink(final Writer out, int pageNumber) throws IOException {
		portletUrl.setParameter(PaginationHelper.OFFSET_PARAMETER_KEY, String.valueOf((pageNumber)
		        * itemLimit));
		final String cssClass = pageNumber == getCurrentPage() ? CSS_CLASS_CURRENTPAGE
		        : null;
		PaginationHelper.appendLink(out, portletUrl, String.valueOf(pageNumber + 1), cssClass);
	}

	public void writePreviousPageLink( final Writer out) throws IOException {
		portletUrl.setParameter(PaginationHelper.OFFSET_PARAMETER_KEY, String
		        .valueOf(offset - itemLimit));
		PaginationHelper.appendLink(out, portletUrl, "&lt;");
	}

	void writeOutputForSinglePage(final Writer out)
			throws IOException {
		out.append("&lt;&lt;" + PaginationHelper.SPACER);
		out.append("&lt;" + PaginationHelper.SPACER);
		out.append("<span class=\""+CSS_CLASS_CURRENTPAGE+"\">1</span>" + PaginationHelper.SPACER);
		out.append("&gt;" + PaginationHelper.SPACER);
		out.append("&gt;&gt;" + PaginationHelper.SPACER);
	}

	/**
	 * Returns whether pagination is needed or not.
	 * 
	 * @param pagination TODO
	 * @return true if paging is needed
	 */
	boolean isPagingNeeded() {
	    // if item count is less then limit we don't need pagination
	    return itemCount > itemLimit;
	}

	/**
	 * Returns whether the current page is first page.
	 * 
	 * @param pagination TODO
	 * @return true if current page is first page
	 */
	boolean isFirstPage() {
	    return offset == 0;
	}

	/**
	 * Returns whether the current page is last page.
	 * 
	 * @param pagination TODO
	 * @return true if current page is last page
	 */
	boolean isLastPage() {
	    return getCurrentPage() + 1 == getPageCount();
	}

	/**
	 * Returns the count of pages for the paging navigation.
	 * 
	 * @param pagination TODO
	 * @return count of pages
	 */
	public int getPageCount() {
	    return (itemCount % itemLimit) == 0 ? itemCount
	            / itemLimit : itemCount / itemLimit + 1;
	}

	/**
	 * Returns the current page index beginning with 0.
	 * 
	 * @param pagination TODO
	 * @return current page index
	 */
	int getCurrentPage() {
	    return (offset + 1) / itemLimit;
	}

	/**
	 * Appends a html link tag to the given JspWriter.
	 * 
	 * @param out
	 *            JspWriter to append the link
	 * @param url
	 *            URL for the href attribute
	 * @param linkText
	 *            linkt text
	 * @param cssClass
	 *            css class name
	 * 
	 * @throws IOException
	 *             thrown if appending to JspWriter failed
	 */
	static void appendLink(final Writer out, final PortletURL url,
	        final String linkText, final String cssClass) throws IOException {
	    out.append("<a ");
	    out.append("href=\"" + url.toString() + "\" ");
	    if (cssClass != null) {
	        out.append("class=\"" + cssClass + "\"");
	    }
	    out.append(">");
	    out.append(linkText);
	    out.append("</a>" + SPACER);
	}

	/**
	 * Appends a html link tag to the given JspWriter.
	 * 
	 * @param out
	 *            JspWriter to append the link
	 * @param url
	 *            URL for the href attribute
	 * @param linkText
	 *            linkt text
	 * 
	 * @throws IOException
	 *             thrown if appending to JspWriter failed
	 */
	static void  appendLink(final Writer out, final PortletURL url,
	        final String linkText) throws IOException {
	    PaginationHelper.appendLink(out, url, linkText, null);
	}
	 /**
     * Sets the item limit per page.
     * 
     * @param pItemLimit
     *            the item limit
     */
    public void setItemLimit(final Integer pItemLimit) {
        this.itemLimit = pItemLimit;
    }

    /**
     * Sets the current offset.
     * 
     * @param pOffset
     *            the offset
     */
    public void setOffset(final Integer pOffset) {
        this.offset = pOffset;
    }

    /**
     * Sets the initial portlet url for the page links.
     * 
     * @param url
     *            the url
     */
    public void setPortletUrl(final PortletURL url) {
        this.portletUrl = url;
    }

	public void setItemCount(Integer pItemCount) {
		this.itemCount=pItemCount;
		
	}

	public void writePaginationLinks(final Writer out) throws IOException {
		if (!isPagingNeeded()) {
		    // create paging without links
		    writeOutputForSinglePage(out);
		} else {
		    
			if (isFirstPage()) {
		        // no links needed
		        out.append("&lt;&lt;" + PaginationHelper.SPACER);
		        out.append("&lt;" + PaginationHelper.SPACER);
		    } else {
		        writeFirstPageLink(out);
		        writePreviousPageLink(out);
		    }
	
		    // pages links
		    final int pageCount = getPageCount();
		    for (int i = 0; i < pageCount; i++) {
		    	writePageNumberLink( out, i);
		    }
		    
		    if (isLastPage()) {
		        // no links needed
		        out.append("&gt;" + PaginationHelper.SPACER);
		        out.append("&gt;&gt;" + PaginationHelper.SPACER);
		    } else {
		        
		    	writeNextPageLink( out);
	
		        // 'goto last page' link (last offset = (pagecount-1) *
		        // itemLimit)
		    	writeLastPageLink(out);
		    }
		}
	}
}
