/**
 * 
 */
package org.evolvis.idm.taglib;

import java.io.IOException;

import javax.portlet.PortletURL;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * This is a simple pager providing direct links to a configurable number of pages and relative
 * links to the first, last, previous and next pages.
 * 
 * @author Michael Kutz, tarent GmbH
 */
public class Paging extends TagSupport {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7167164507611093369L;

	/** The page currently displayed. */
	private int currentPage;

	/** The total number of items. */
	private int itemCount;

	/** The maximum number of items displayed on one page. */
	private int itemLimit;

	/** The base URL. */
	private PortletURL baseUrl;

	/** The maximum number of direct links to pages before the current one displayed by this. */
	private int backwardLinkLimit;

	/** The maximum number of direct links to pages after the current one displayed by this. */
	private int forwardLinkLimit;

	/**
	 * Format string for a link to a page. First argument is the PortletURL of the page, second is
	 * the title (tool tip) for the link and the third argument is the link's text.
	 */
	private static final String PAGELINK = "<li><a href=\"%1$s\" title=\"%2$s\">%3$s</a></li>";

	/** Format string for the current page display. The number of the page is the only argument. */
	private static final String CURRENTPAGE = "<li>%1$s</li>";

	/** The start tag for the paging. */
	private static final String STARTTAG = "<ul class=\"paging\">";

	/** The end tag for the paging. */
	private static final String ENDTAG = "</ul>";

	/** A string that is used to indicate there are more pages not represented by a link. */
	private static final String MOREPAGESPLACEHOLDER = "<li>\u2026</li>";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		int pageCount = getPageCount();

		if (pageCount < 2)
			return super.doStartTag();

		try {
			/*
			 * calculate first and last page that will be represented with a direct link
			 */
			int firstLinkNumber = Math.max(currentPage - backwardLinkLimit, 1);
			int lastLinkNumber = Math.min(currentPage + forwardLinkLimit, pageCount);
			
			/*
			 * append opening tag
			 */
			out.append(STARTTAG);

			/*
			 * append link to previous page (if there is one)
			 */
			if (currentPage > 1) {
				out.append(String.format(PAGELINK, new Object[] { getPageUrl(currentPage - 1),
						currentPage - 1, "<" }));
			}

			/*
			 * append absolute link to first page and place holder if there are pages left out
			 */
			if (firstLinkNumber - 1 > 0) {
				out.append(String.format(PAGELINK, new Object[] { getPageUrl(1), 1, "1" }));
				if (firstLinkNumber - 1 > 1) {
					out.append(MOREPAGESPLACEHOLDER);
				}
			}

			/*
			 * append direct links to pages before the current one
			 */
			for (int page = firstLinkNumber; page < currentPage; page++) {
				out.append(String.format(PAGELINK, new Object[] { getPageUrl(page), page, page }));
			}

			/*
			 * append current page number
			 */
			out.append(String.format(CURRENTPAGE, new Object[] { currentPage }));

			/*
			 * append direct links pages after the current one
			 */
			for (int page = currentPage + 1; page <= lastLinkNumber; page++) {
				out.append(String.format(PAGELINK, new Object[] { getPageUrl(page), page, page }));
			}
			
			/*
			 * append place holder and absolute link to last page if there are pages left out
			 */
			if (pageCount - lastLinkNumber > 0) {
				if(pageCount - lastLinkNumber > 1) {
					out.append(MOREPAGESPLACEHOLDER);
				}
				out.append(String.format(PAGELINK, new Object[] { getPageUrl(pageCount), pageCount, pageCount }));
			}

			/*
			 * append link to next page (if there is one)
			 */
			if (currentPage < pageCount) {
				out.append(String.format(PAGELINK, new Object[] { getPageUrl(currentPage + 1),
						currentPage + 1, ">" }));
			}

			/*
			 * append closing tag
			 */
			out.append(ENDTAG);
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		return super.doStartTag();
	}

	/**
	 * Helper method generating a URL to a certain page.
	 * 
	 * @param page
	 *            number of the page the URL should be referencing.
	 * @return a URL referencing the specified page.
	 */
	private String getPageUrl(int page) {
		baseUrl.setParameter("page", Integer.toString(page));
		return baseUrl.toString();
	}

	/**
	 * @return the number of pages needed to display all items.
	 */
	public int getPageCount() {
		return itemCount / itemLimit + (itemCount % itemLimit > 0 ? 1 : 0);
	}

	/**
	 * @param itemCount
	 *            the total number of items to be displayed.
	 */
	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}

	/**
	 * @param currentPage
	 *            the page currently displayed.
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	/**
	 * @param itemLimit
	 *            the maximum number of items displayed on one page.
	 */
	public void setItemLimit(int itemLimit) {
		this.itemLimit = itemLimit;
	}

	/**
	 * @param baseUrl
	 *            a PortletURL used as base URL. Should therefore not contain any paging arguments.
	 */
	public void setBaseUrl(PortletURL baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * @param linkLimit
	 *            the maximum number of page links to pages before the current one displayed by
	 *            this.
	 */
	public void setBackwardLimit(int backwardLinkLimit) {
		this.backwardLinkLimit = backwardLinkLimit;
	}

	/**
	 * @param linkLimit
	 *            the maximum number of page links to pages after the current one displayed by this.
	 */
	public void setForwardLimit(int forwardLinkLimit) {
		this.forwardLinkLimit = forwardLinkLimit;
	}
}
