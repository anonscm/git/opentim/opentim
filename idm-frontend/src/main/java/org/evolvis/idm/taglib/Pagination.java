package org.evolvis.idm.taglib;

import java.io.IOException;

import javax.portlet.PortletURL;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * The Class Pagination.
 */
public class Pagination extends TagSupport {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8230236639205053066L;

    
    PaginationHelper helper = new PaginationHelper();
    
    /** {@inheritDoc} */
    @Override
    public int doStartTag() throws JspException {

        final JspWriter out = pageContext.getOut();

        try {
            helper.writePaginationLinks(out);
        } catch (final IOException e) {
            throw new JspException(e);
        }

        return super.doStartTag();
    }

	/**
     * Sets the item count.
     * 
     * @param pItemCount
     *            the item count
     */
    public void setItemCount(final Integer pItemCount) {
        helper.setItemCount( pItemCount);
    }

    /**
     * Sets the item limit per page.
     * 
     * @param pItemLimit
     *            the item limit
     */
    public void setItemLimit(final Integer pItemLimit) {
        helper.setItemLimit(pItemLimit);
    }

    /**
     * Sets the current offset.
     * 
     * @param pOffset
     *            the offset
     */
    public void setOffset(final Integer pOffset) {
        helper.setOffset(pOffset);
    }

    /**
     * Sets the initial portlet url for the page links.
     * 
     * @param url
     *            the url
     */
    public void setPortletUrl(final PortletURL url) {
        helper.setPortletUrl(url);
    }
    
    
    
}
