/**
 * 
 */
package org.evolvis.idm.taglib;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeType;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.springframework.web.servlet.tags.form.AbstractHtmlInputElementTag;
import org.springframework.web.servlet.tags.form.CheckboxTag;
import org.springframework.web.servlet.tags.form.InputTag;
import org.springframework.web.servlet.tags.form.SelectTag;

/**
 * @author Michael Kutz, tarent GmbH
 */
public class Input extends TagSupport {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4378527912764899540L;

	/** Standard attribute of string input tag. */
	private String path;
	
	private Value valueObject;

	/** If true the input field is locked. */
	private boolean locked = false;

	private static final String DISABLEDINPUT = "<input disabled=\"disabled\" type=\"text\" value=\"${%1$s}\" />";

	private static final String CSSERRORCLASS = "field-error";
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int doStartTag() throws JspException {
		
		try {
			/*
			 * unlocked attributes
			 */
			if (!locked) {
				
				AbstractHtmlInputElementTag springTag;
				
				/*
				 * check boxes
				 */
				if (valueObject.getAttribute().getType().equals(AttributeType.BOOLEAN)) {
					CheckboxTag checkboxTag = new CheckboxTag();
					checkboxTag.setValue(Boolean.parseBoolean(valueObject.getValue()));
					springTag = checkboxTag;
				}
				
				/*
				 * select boxes
				 */
				else if (valueObject.getAttribute().getType().equals(AttributeType.SELECTLIST)) {
					SelectTag selectTag = new SelectTag();
					List<Attribute> subAttributes = valueObject.getAttribute().getSubAttributes();
					List<Attribute> items = new LinkedList<Attribute>();
					items.add(new Attribute("", ""));
					if (subAttributes != null) items.addAll(subAttributes);
					selectTag.setItems(items);
					selectTag.setItemLabel("displayName");
					selectTag.setItemValue("name");
					
					springTag = selectTag;
				}
				
				/*
				 * text fields
				 */
				else {
					InputTag inputTag = new InputTag();
					
					/*
					 * date
					 */
					if (valueObject.getAttribute().getType().equals(AttributeType.DATE)) {
						Locale locale = pageContext.getRequest().getLocale();
						
						inputTag.setCssClass("cal_date datepicker");
						inputTag.setMaxlength("10");
						inputTag.setOnfocus("new Control.DatePicker(this, { 'locale': '" + locale + "' })");
					}
					
					/*
					 * time
					 */
					else if (valueObject.getAttribute().getType().equals(AttributeType.DATETIME)) {
						inputTag.setCssClass("cal_date datepicker");
						inputTag.setMaxlength("5");
					}
					
					springTag = inputTag;
				}
				
				/*
				 * set common attributes
				 */
				springTag.setCssErrorClass(CSSERRORCLASS);
				springTag.setPageContext(pageContext);
				springTag.setPath(path + ".value");
				
				return springTag.doStartTag();
			}
			
			/*
			 * locked attributes (just display the displayName and value)
			 */
			else {
				JspWriter out = pageContext.getOut();
				out.append(String.format(DISABLEDINPUT, new Object[] {path + ".value"}));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return super.doStartTag();
	}

	/**
	 * @param value the value to set
	 */
	public void setValueObject(Value value) {
		this.valueObject = value;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @param locked the locked to set
	 */
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
	
}
