/**
 * 
 */
package org.evolvis.idm.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * @author Michael Kutz, tarent GmbH
 */
public class Confirmation extends TagSupport {

	private static final long serialVersionUID = 5463221067040378940L;

	private String actionUrl;
	
	private String yeaText;
	
	private String nayText;
	
	private static final String STARTTAG = 
			"<div class=\"portlet-msg-question\">";

	private static final String ENDTAG = 
			"<form action=\"%1$s\" method=\"post\" id=\"confirmation\">" +
			"<button type=\"submit\" name=\"denied\" value=\"denied\">%2$s</button>" +
			"<button  type=\"submit\" name=\"confirmed\" value=\"confirmed\">%3$s</button>" +
			"</form>" +
			"</div>";
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int doStartTag() throws JspException {
		final JspWriter out = pageContext.getOut();
		
		try {
			out.append(String.format(STARTTAG, new Object[] {}));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return EVAL_BODY_INCLUDE;
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public int doEndTag() throws JspException {
		final JspWriter out = pageContext.getOut();
		
		try {
			out.append(String.format(ENDTAG, new Object[] {actionUrl, nayText, yeaText}));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return super.doEndTag();
	}

	/**
	 * @return the actionUrl
	 */
	public String getActionUrl() {
		return actionUrl;
	}

	/**
	 * @param actionUrl the actionUrl to set
	 */
	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}

	/**
	 * @return the yeaText
	 */
	public String getYeaText() {
		return yeaText;
	}

	/**
	 * @param yeaText the yeaText to set
	 */
	public void setYeaText(String yeaText) {
		this.yeaText = yeaText;
	}

	/**
	 * @return the nayText
	 */
	public String getNayText() {
		return nayText;
	}

	/**
	 * @param nayText the nayText to set
	 */
	public void setNayText(String nayText) {
		this.nayText = nayText;
	}
	
}
