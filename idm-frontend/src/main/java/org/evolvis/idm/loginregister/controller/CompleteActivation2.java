/**
 * 
 */
package org.evolvis.idm.loginregister.controller;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.constant.PortletConfig;
import org.evolvis.idm.common.service.AuthenticationService;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.loginregister.command.CompleteActivationCom;
import org.evolvis.idm.loginregister.command.LoginCom;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * @author Michael Kutz, tarent GmbH
 */
@Controller
@RequestMapping(value = "view", params = "ctx=CompleteActivation2")
@SessionAttributes( { CompleteActivation2.COMMAND })
public class CompleteActivation2 {

	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected Validator validator;

	@Autowired
	protected UserManagement userManagement;

	@Autowired
	protected AuthenticationService authenticationService;

	@Autowired
	protected Login loginController;

	@ModelAttribute(COMMAND)
	public CompleteActivationCom getCompleteActivationCom(
			@RequestParam(value = PortletConfig.REGISTER_CONFIRM_KEY, required = false) String registerKeyBase64,
			ModelMap model) {
		/*
		 * if no register key is given there has to be a CompleteActivationCom in the model to
		 * return
		 */
		if (registerKeyBase64 == null) {
			return (CompleteActivationCom) model.get(COMMAND);
		}
		
		CompleteActivationCom completeActivationCom = new CompleteActivationCom();
		try {
			/*
			 * decode registration key
			 */
			final String registerKey = new String(Base64.decodeBase64(registerKeyBase64.getBytes()));
	
			/*
			 * check if there is a user for the given registration key
			 */
			String uuid = authenticationService.getUUIDForRegistrationKey(registerKey);
			if (uuid != null) {
				User user = userManagement.getUserByUuid(uuid);
	
				logger.info("Found user uuid=" + user.getUuid() + ", username="
						+ user.getUsername() + " for registerKey=" + registerKey);
	
				completeActivationCom.setRegisterKey(registerKeyBase64);
	
				boolean displayNameGenerationEnabled = SecurityContextHolder.getClientPropertyMap()
						.getBoolean(
								ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED);
				if (displayNameGenerationEnabled) {
					completeActivationCom.setDisplayName("------");
				} else {
					completeActivationCom.setDisplayName(user.getDisplayName());
				}
	
				completeActivationCom.setFirstname(user.getFirstname());
				completeActivationCom.setLastname(user.getLastname());
			}
		} catch (Exception e) {
			logger.warn(e);
		}
	
		return completeActivationCom;
	}

	@ActionMapping
	public void handleCompleteActivationAction(
			@ModelAttribute(COMMAND) @Valid CompleteActivationCom command, Errors errors,
			ActionRequest request, ActionResponse response) {
		logger.info("handleCompleteActivationAction() entered");
		try {

			String passwordPattern = SecurityContextHolder.getClientPropertyMap().get(
					ClientPropertyMap.KEY_SECURITY_PASSWORT_PATTERN);
			if (!passwordPattern.isEmpty() && !command.getPassword().matches(passwordPattern)) {
				errors.rejectValue("password", "violation.password.password");
			}

			/*
			 * return if errors exist
			 */
			if (errors.hasErrors())
				return;
			
			/*
			 * check if there is a not yet activated user with the requested registration key
			 */
			String registerKey = new String(Base64
					.decodeBase64(command.getRegisterKey().getBytes()));
			String uuid = authenticationService.getUUIDForRegistrationKey(registerKey);

			if (uuid == null) {
				errors.reject("error.completeactivation.registerkey");
				return;
			}
			/*
			 * get the user account
			 */
			User user = userManagement.getUserByUuid(uuid);

			/*
			 * update user password
			 */
			if (!authenticationService.updatePassword(user.getUsername(), command.getPassword())) {
				errors.reject("error.completeactivation.password");
				return;
			}

			/*
			 * update user's names
			 */
			userManagement.setUserNames(uuid, command.getFirstname(), command.getLastname());

			/*
			 * user status will be updated in the login controller
			 */
			loginController.handleConfirmRegistrationAction(request, response, null, errors,
					command.getRegisterKey());

			/*
			 * show success message if no errors have been reported
			 */
			if (!errors.hasErrors())
				request.setAttribute("msg_success", "success.completeactivation");
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.completeactivation");
		}
	}

	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse response, Model model) {
		/*
		 * if additional data has been added and account has been activated successfully show login
		 * form
		 */
		if (request.getAttribute("activationSuccessful") != null) {
			model.addAttribute(COMMAND, new LoginCom());
			return "Login";
		}

		/*
		 * check if the client configuration has display name generation enabled (and therefore
		 * prohibits free choice of display names)
		 */
		boolean displayNameGenerationEnabled = Boolean.valueOf(SecurityContextHolder
				.getClientPropertyMap().get(
						ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED));
		request.setAttribute("displayNameGenerationEnabled", displayNameGenerationEnabled);

		return "CompleteActivation2";
	}

}
