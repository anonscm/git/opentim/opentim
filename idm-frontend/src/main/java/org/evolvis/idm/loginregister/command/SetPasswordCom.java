package org.evolvis.idm.loginregister.command;

import java.io.Serializable;

import javax.validation.constraints.Size;

import org.evolvis.idm.common.validation.FieldEquality;
import org.evolvis.idm.common.validation.NotNull;

@FieldEquality(message = "violation.equalPasswords", value = { "password", "passwordRepeat" })
public class SetPasswordCom implements Serializable {

    private static final long serialVersionUID = 1422063113432672432L;

    @NotNull
    @Size(min = 8, max = 74, message = "violation.changepassword.size")
    private String password;

    @NotNull
    private String passwordRepeat;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((passwordRepeat == null) ? 0 : passwordRepeat.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SetPasswordCom other = (SetPasswordCom) obj;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (passwordRepeat == null) {
            if (other.passwordRepeat != null)
                return false;
        } else if (!passwordRepeat.equals(other.passwordRepeat))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SetPasswordCom [password=*****, password_repeat=*****]";
    }
}
