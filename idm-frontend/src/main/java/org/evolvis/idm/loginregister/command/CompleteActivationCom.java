package org.evolvis.idm.loginregister.command;

import javax.validation.constraints.Size;

import org.evolvis.idm.common.validation.CivilName;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.common.validation.FieldEquality;

/**
 * Command object used by CompleteActivation controller
 * 
 * @author Michael Kutz
 * 
 */
@FieldEquality(message = "violation.account.equalPasswords", value = { "password", "passwordRepeat" })
public class CompleteActivationCom {

	private String registerKey;
	
	@Size(min = 2, max = 40, message = "violation.account.size")
	@CivilName
	private String firstname;

	@Size(min = 2, max = 40, message = "violation.account.size")
	@CivilName
	private String lastname;

	@Size(min = 6, max = 40, message = "violation.account.size")
	@DisplayName
	private String displayName;

	@Size(min = 8, max = 74, message = "violation.changepassword.size")
	private String password;

	private String passwordRepeat;

	public String getRegisterKey() {
		return registerKey;
	}

	public void setRegisterKey(String registerKey) {
		this.registerKey = registerKey;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String newPassword) {
		this.password = newPassword;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

}
