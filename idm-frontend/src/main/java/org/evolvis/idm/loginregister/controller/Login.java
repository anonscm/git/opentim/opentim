package org.evolvis.idm.loginregister.controller;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.constant.MessageKeys;
import org.evolvis.idm.common.constant.PortletConfig;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.service.AuthenticationService;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.loginregister.command.LoginCom;
import org.evolvis.idm.loginregister.constant.SessionKeys;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.security.SecurityContextHolder;
import org.evolvis.opensso.rest.constant.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;

import com.liferay.portal.util.PortalUtil;

/**
 * @author Tino Rink
 * @author Frederic Eßer
 * @author Jens Neumaier
 */
@Controller
@RequestMapping(value = "view")
public class Login {
	public static final String COMMAND = "command";
	
    public static final String LIFERAY_SHARED_OPENSSO_TOKEN = "LIFERAY_SHARED_OPENSSO_TOKEN";

    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    protected AuthenticationService authenticationService;

    @Autowired
    protected Validator validator;

    @Autowired
    protected MessageSource resources;
    
    @Autowired
    protected UserManagement userManagement;

    @RequestMapping
    public ModelAndView handleRenderRequest(RenderRequest request) {
        ModelAndView modelAndView;
        
        if (request.getRemoteUser() == null) {
    		
    		modelAndView = new ModelAndView("Login");
    		
    		if (!modelAndView.getModelMap().containsAttribute(COMMAND))
    			modelAndView.addObject(COMMAND, new LoginCom());
    		
    		Login.alternativeRegistration(modelAndView);
    		
			return modelAndView;
        } else {
            modelAndView = new ModelAndView("SignedIn");
            try {
                modelAndView.addObject("user", PortalUtil.getUser(request));
            }
            catch (Exception e) {
                logger.warn("handleRenderRequest() couln't get user for RemoteUser=" + request.getRemoteUser());
            }
        }
        
        return modelAndView;
    }
	        
	@RequestMapping(params = "action=Login")
    public void handleLoginAction(ActionRequest request, ActionResponse response, ModelMap model, @Valid @ModelAttribute(COMMAND) LoginCom loginCom, Errors errors) {

        logger.info("doLogin() entered, loginCom=" + loginCom);
        
        // return if errors exists
        if (errors.hasErrors()) {
            return;
        }

        // try login and redirect if successful
        try {
            String token = authenticationService.login(loginCom.getLogin(), loginCom.getPassword());
            if (null != token) {
                request.getPortletSession().setAttribute(LIFERAY_SHARED_OPENSSO_TOKEN, token, PortletSession.APPLICATION_SCOPE);
                // redirect to PortletConfigKeys.TARENT_SSO_LOGIN_REDIRECT config param
                // this redirect will be overridden by the OpenSSOFilter if the config param TARENT_SSO_LOGIN_AUTOREDIRECT is enabled
                response.sendRedirect(PortletConfig.LOGIN_REDIRECT);
                return;
            }
        }
        catch (Exception e) {
            // shuldn't happen
            logger.error("doLogin() authentication failed", e);
        }

        // try onetime login
        try {
            String uuid = authenticationService.onetimeLogin(loginCom.getLogin(), loginCom.getPassword());
            if (null != uuid) {
                request.getPortletSession(true).setAttribute(SessionKeys.RECOVERY_USER_UID, uuid);
                request.setAttribute("ctx", "SetPassword");
                model.clear();
                return;
            }
        }
        catch (Exception e) {
            // shuldn't happen
            logger.error("doLogin() authentication failed", e);
        }

        // set context and default error
        errors.reject(MessageKeys.ERROR_INCORRECT_LOGIN_PASSWD);
    }
	
	@RequestMapping(params = "action=ConfirmRegistration")
	public void handleConfirmRegistrationAction(ActionRequest request, ActionResponse response, @ModelAttribute(COMMAND) LoginCom loginCom, Errors errors, @RequestParam(value = PortletConfig.REGISTER_CONFIRM_KEY) String key) {
		// decode
        String registerKey = new String(Base64.decodeBase64(key.getBytes()));

        // update user status
        String uuid = authenticationService.getUUIDForRegistrationKey(registerKey);
		if (uuid == null) {
	        logger.warn("Could not find user for register key = " + registerKey);
	        errors.reject("error.registration.activation.keyExpired");
	        return;
	    }
		try {
			userManagement.setUserStatus(uuid, UserStatus.ACTIVE);
		} catch (BackendException e) {
			logger.error("User registration confirmation failed with unknown reason.", e);
			errors.reject("error.registration.activation.unknown");
			return;
		}
		
		/*
		 * redirect to custom success site (if configured in ClinetPropertyMap)
		 */
		if (Boolean.parseBoolean(SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_REGISTRATION_ACTIVATIONSUCCESSLINK_ENABLED))) {
			try {
				response.sendRedirect(SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_REGISTRATION_ACTIVATIONSUCCESSLINK_URL));
			} catch (IOException e) {
				logger.error("Error while redirecting to success site", e);
			}
		}
		
        request.setAttribute("msg_success", resources.getMessage(
				"success.registration.activation", null, request.getLocale()));
	}
	
	public static void alternativeRegistration(ModelAndView modelAndView) {
		
		boolean alternativeRegistrationEnabled = Boolean.valueOf(SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_REGISTRATION_ALTERNATIVELINK_ENABLED));

		if (alternativeRegistrationEnabled)
		{
			modelAndView.addObject("altRegistration", true);
			modelAndView.addObject("registrationUrl", SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_REGISTRATION_ALTERNATIVELINK_URL));
		} 
		else {
			modelAndView.addObject("altRegistration", false);
		}
	}
}
