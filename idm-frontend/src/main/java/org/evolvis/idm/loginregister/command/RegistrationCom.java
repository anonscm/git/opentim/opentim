package org.evolvis.idm.loginregister.command;

import java.io.Serializable;

import javax.validation.constraints.Size;

import org.evolvis.idm.common.validation.CivilName;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.common.validation.Email;
import org.evolvis.idm.common.validation.FieldEquality;
import org.evolvis.idm.common.validation.NotNull;

@FieldEquality(message = "violation.equalPasswords", value = { "password", "passwordRepeat" })
public class RegistrationCom implements Serializable {

	private static final long serialVersionUID = 4783512496846168415L;

	@NotNull
	@CivilName
	@Size(min=2, max=40, message="violation.size")
	private String firstname;

	@NotNull
	@CivilName
	@Size(min=2, max=40, message="violation.size")
	private String lastname;

	@NotNull
	@Email
	private String login;

	@NotNull
	@Size(min=6, max=81, message="violation.size")
	@DisplayName
	private String displayName;

	@NotNull
	@Size(min=8, max=74, message="violation.size")
//	@Password
	private String password;

	@NotNull
//	@Password
	private String passwordRepeat;

	private boolean declarationOfApproval;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}
	
	private String activationURL;

	public void setActivationURL(String activationURL) {
		this.activationURL = activationURL;
	}

	public String getActivationURL() {
		return activationURL;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((passwordRepeat == null) ? 0 : passwordRepeat.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistrationCom other = (RegistrationCom) obj;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (passwordRepeat == null) {
			if (other.passwordRepeat != null)
				return false;
		} else if (!passwordRepeat.equals(other.passwordRepeat))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RegistrationCom [firstname=" + firstname + ", lastname=" + lastname + ", displayName=" + displayName + ", login=" + login + ", password=*****, passwordRepeat=*****]";
	}

	public void setDeclarationOfApproval(boolean declarationOfApproval) {
		this.declarationOfApproval = declarationOfApproval;
	}

	public boolean getDeclarationOfApproval() {
		return declarationOfApproval;
	}
}
