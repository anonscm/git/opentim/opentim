/**
 * 
 */
package org.evolvis.idm.loginregister.controller;

import java.net.URL;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.evolvis.portal.portletutils.portletregistry.PortletRegistry;
import org.evolvis.portal.portletutils.portletregistry.util.URLUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Michael Kutz, tarent GmbH
 */
@Controller
@RequestMapping(value = "edit")
public class PortletRegistyUpdate {

	/** The portlet registry JNDI name. */
	private String portletRegistryJNDIName = "PortletRegistryEJB3Impl/local";

	/**
	 * The service name. TODO get this out of backend api
	 */
	private static final String SERVICENAME = "org.evolvis.idm.loginregister";

	/** Message source, provides access to the i18n-property file's contents. */
	private MessageSource resources;

	@Autowired
	public void setMessageSource(MessageSource resources) {
		this.resources = resources;
	}

	@RequestMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse response, ModelMap model) {
		try {
			Context context = new InitialContext();

			/*
			 * get registry item and add it to the model
			 */
			PortletRegistry registry = (PortletRegistry) context.lookup(portletRegistryJNDIName);

			/*
			 * get the portlet id from liferay container
			 */
			String portletId = (String) request.getAttribute("PORTLET_ID");

			/*
			 * determine the portlet's base URL
			 */
			String baseUrl = URLUtil.extractBaseURL(request.getProperty("referer"));
			URL url = new URL(baseUrl);
			String path = url.getPath();

			/*
			 * register portlet in registry
			 */
			registry.registerPortletInstance(registry.createRegistryItem(baseUrl, portletId,
					SERVICENAME, path));
			
			/*
			 * add success message to action response
			 */
			request.setAttribute("msg_success", resources.getMessage(
					"success.portletregistry.update", null, request.getLocale()));
			
			model.addAttribute("registryItem", registry.getRegistryItemForService(SERVICENAME));

		} catch (Exception e) {
			request.setAttribute("msg_warning", resources.getMessage(
					"error.portletregistry.update", null, request.getLocale()));
			e.printStackTrace();
		}

		/*
		 * return the JSP's name
		 */
		return "PortletRegistryUpdate";
	}

}
