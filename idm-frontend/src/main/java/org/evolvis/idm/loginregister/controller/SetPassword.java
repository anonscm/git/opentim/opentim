package org.evolvis.idm.loginregister.controller;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.evolvis.idm.common.service.AuthenticationService;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.loginregister.command.SetPasswordCom;
import org.evolvis.idm.loginregister.constant.SessionKeys;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;

/**
 * @author Tino Rink
 * @author Jens Neumaier
 */
@Controller
@RequestMapping(value = "view", params = "ctx=SetPassword")
public class SetPassword {
	public static final String COMMAND = "command";
	
	@Autowired
    protected AuthenticationService authenticationService;
	
	@Autowired
    protected MessageSource resources;
	
	@Autowired
	protected UserManagement userManagement;
	
	@Autowired
    protected Validator validator;
    
    @RequestMapping
    public ModelAndView handleRenderRequest(RenderRequest request,
    		RenderResponse response) throws Exception 
    {
    	ModelAndView modelAndView = new ModelAndView("SetPassword");
    	modelAndView.addObject(COMMAND, new SetPasswordCom());
    	
    	Login.alternativeRegistration(modelAndView);
    	
    	return modelAndView;
    }
    
    @RequestMapping
    public void handleDefaultAction(ActionRequest request, ActionResponse response, ModelMap model, @ModelAttribute(COMMAND) @Valid SetPasswordCom setPasswordCom, Errors errors) throws Exception {

		String passwordPattern = SecurityContextHolder.getClientPropertyMap().get(
				ClientPropertyMap.KEY_SECURITY_PASSWORT_PATTERN);
		if (!passwordPattern.isEmpty() && !setPasswordCom.getPassword().matches(passwordPattern)) {
			errors.rejectValue("password", "violation.password.password");
		}

		if (errors.hasErrors()) {
			return;
		}
		
        // re-retrieving username from SecurityContext
        String uuid = (String) request.getPortletSession().getAttribute(SessionKeys.RECOVERY_USER_UID);
        User user = userManagement.getUserByUuid(uuid);
        authenticationService.updatePassword(user.getUsername(), setPasswordCom.getPassword());
        
        request.setAttribute("ctx", "Login");
        model.clear();
        request.setAttribute("msg_success", resources.getMessage(
				"msg.success_pw_recovery_done", null, request.getLocale()));
    }
}
