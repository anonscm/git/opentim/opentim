package org.evolvis.idm.loginregister.constant;

public class PortletParameter {

    public static final String EMAIL = "email";

    public static final String FIRSTNAME = "firstname";

    public static final String LOGIN = "login";

    public static final String PASSWORD = "password";

    public static final String PASSWORD_REPEAT = "password_repeat";
    
    public static final String SURNAME = "lastname";

}
