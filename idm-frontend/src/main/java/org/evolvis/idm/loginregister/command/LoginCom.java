package org.evolvis.idm.loginregister.command;

import java.io.Serializable;

import org.evolvis.idm.common.validation.NotEmpty;
import org.evolvis.idm.common.validation.NotNull;

public class LoginCom implements Serializable {

    private static final long serialVersionUID = 7671925327514609362L;

    @NotNull
    @NotEmpty
    private String login;

    @NotNull
    @NotEmpty
	private String password;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoginCom other = (LoginCom) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LoginCom [login=" + login + ", password=*****]";
	}
}
