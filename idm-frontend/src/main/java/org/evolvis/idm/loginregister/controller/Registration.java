package org.evolvis.idm.loginregister.controller;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.evolvis.idm.common.constant.PortletConfig;
import org.evolvis.idm.common.exception.ServiceException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.service.EmailService;
import org.evolvis.idm.common.util.BackendServiceUtil;
import org.evolvis.idm.employeeactivation.controller.EmployeeActivation;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.loginregister.command.RegistrationCom;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;

/**
 * @author Jens Neumaier
 */
@Controller
@RequestMapping(value = "view", params = "ctx=Registration")
public class Registration {
	public static final String COMMAND = "command";
	
	private final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	protected UserManagement userManagement;

	@Autowired
    protected EmailService emailService;
    
	@Autowired
    protected Validator validator;
    
    @Autowired
    protected MessageSource resources;
    
    @RequestMapping
    public ModelAndView handleRenderRequest(RenderRequest request,
    		RenderResponse response) throws Exception {
    	ModelAndView modelAndView = new ModelAndView("Registration");
    	
    	if (!modelAndView.getModelMap().containsAttribute(COMMAND))
			modelAndView.addObject(COMMAND, formBackingObject(response));

    	request.setAttribute("termsRequired", Boolean.valueOf(SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_REGISTRATION_TERMSREQUIRED_ENABLED)));
    	request.setAttribute("hyperlink", SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_REGISTRATION_TERMSREQUIRED_URL));
    	request.setAttribute("displayNameGenerationEnabled", Boolean.valueOf(SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED)));
    	
    	return modelAndView;
    }

    @RequestMapping
    public void handleDefaultAction(ActionRequest request, ActionResponse response,
    		@ModelAttribute(COMMAND) @Valid RegistrationCom registrationCom,
            Errors errors) {
    	
        if (logger.isDebugEnabled()) {
            logger.debug("onSubmitAction() entered, command=" + registrationCom);
        }
        
        String passwordPattern = SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_SECURITY_PASSWORT_PATTERN);
        if (!passwordPattern.isEmpty() && !registrationCom.getPassword().matches(passwordPattern)) {
        	errors.rejectValue("password", "violation.password.password");
        }
        
        /*
         * return if errors exists
         */
        if (errors.hasErrors()) 
        	return;
        
        // check if user is present in an employee client
        try {
        	if (userManagement.isUserExisting(SecurityContextHolder.getClientName(), registrationCom.getLogin())) {
        		errors.rejectValue("login", "error.user.duplicateUser");
        		return;
        	}
			Client employeeClient = EmployeeActivation.getEmployeeClient(BackendServiceUtil.getClientManagementService().getClients(null).getResultList());
			if (employeeClient != null && userManagement.isUserExisting(employeeClient.getName(), registrationCom.getLogin())) {
				errors.rejectValue("login", "error.user.duplicateUserEmployee");
				return;
        	}
        } catch (BackendException e) {
			logger.error("Error while checking for existing users.", e);
			throw new ServiceException(org.evolvis.idm.common.exception.Errors.BACKEND_NOT_AVAILABLE);
		}
        
        // allow registration if approval of registration terms are not required or given by the user
        boolean registerTermsRequired = Boolean.valueOf(SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_REGISTRATION_TERMSREQUIRED_ENABLED));
		
    	if(!registerTermsRequired || registrationCom.getDeclarationOfApproval()) {

    		String activationKey;
    		try {
    			activationKey = userManagement.registerUser(SecurityContextHolder.getClientName(), registrationCom.getLogin(), registrationCom.getPassword(), registrationCom
    					.getDisplayName(), registrationCom.getFirstname(), registrationCom.getLastname(), request.getLocale().toString());
    		} catch (BackendException e) {
    			logger.warn("registerAccount() failed for command=" + registrationCom + ", error=" + e.getMessage());
    			// TODO jneuma fix after refactoring (previously getError())
    			errors.reject(e.getMessage());
    			return;
    		}

			String registerKeyB64 = new String(Base64.encodeBase64(activationKey.getBytes()));
			String activationLink = registrationCom.getActivationURL();
			activationLink = activationLink.replaceAll(PortletConfig.REGISTER_CONFIRM_VALUE_PLACEHOLDER, registerKeyB64);

			try {
				emailService.sendSelfRegistrationMail(request.getServerName(), request.getLocale(), registrationCom.getLogin(), registrationCom.getFirstname() + " " + registrationCom.getLastname(), activationLink);
			}
			catch (EmailException e) {
				logger.error("sendRegisterMail() failed for email=" + registrationCom.getLogin() + ", activationKey="
						+ activationKey + ", activationLink="
						+ activationLink, e);
				errors.reject("error.emailSendingFailedRegistration");
				
				// rollback user creation
				try {
					userManagement.deleteUser(userManagement.getUserByUsername(SecurityContextHolder.getClientName(), registrationCom.getLogin()).getUuid());
				} catch (BackendException be) {
					// do nothing if rollback fails
					logger.error("Error while executing user registration rollback", be);
				}
				
				return;
			}
			
			/*
			 * redirect to custom success site (if configured in ClinetPropertyMap)
			 */
			if (Boolean.parseBoolean(SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_REGISTRATION_REGISTRATIONSUCCESSLINK_ENABLED))) {
				try {
					response.sendRedirect(SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_REGISTRATION_REGISTRATIONSUCCESSLINK_URL));
				} catch (IOException e) {
					logger.error("Error while redirecting to success site", e);
				}
			}
			
			request.setAttribute("msg_success", resources.getMessage(
					"msg.register_congratulate", null, request.getLocale()));
    	} else {
			errors.reject("error.registration.agreement");
		}
    }
    
    protected RegistrationCom formBackingObject(RenderResponse response) throws Exception {
    	RegistrationCom registrationCom = new RegistrationCom();
    	boolean displayNameGenerationEnabled = Boolean.valueOf(SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED));
    	if (displayNameGenerationEnabled)
    		registrationCom.setDisplayName("------");
    	
    	// generate activation url
		PortletURL activationURL = response.createActionURL();
		activationURL.setParameter("ctx", "Login");
		activationURL.setParameter("action", "ConfirmRegistration");
		activationURL.setParameter(PortletConfig.REGISTER_CONFIRM_KEY, PortletConfig.REGISTER_CONFIRM_VALUE_PLACEHOLDER);

		registrationCom.setActivationURL(activationURL.toString());
		
    	return registrationCom;
    }
}
