package org.evolvis.idm.loginregister.controller;

import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.evolvis.idm.common.service.AuthenticationService;
import org.evolvis.idm.common.service.EmailService;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.loginregister.command.ReqRecoveryCom;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;

import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * @author Tino Rink
 * @author Jens Neumaier
 */
@Controller
@RequestMapping(value = "view", params = "ctx=ReqRecovery")
public class ReqRecovery {
	public static final String COMMAND = "command";
	
	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
    protected AuthenticationService authenticationService;

	@Autowired
    protected EmailService emailService;

	@Autowired
    protected UserManagement userManagement;
	
	@Autowired
    protected MessageSource resources;
    
    @RequestMapping
    public void handleDefaultAction(ActionRequest request, ActionResponse response, ModelMap model, @ModelAttribute(COMMAND) @Valid ReqRecoveryCom reqRecoveryCom,
            Errors errors) {

    	boolean showFailureMessages = Boolean.valueOf(SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_SECURITY_ALLOWEMAILEXISTENCEHINTS_ENABLED));
    	
    	/*
    	 * try to get the user by mail address return if the user didn't exist but only show error
    	 * message if showFailureMessages is set
    	 */
        String email = reqRecoveryCom.getEmail();
        User user = null;
        try {
            user = userManagement.getUserByUsername(SecurityContextHolder.getClientName(), email);
        } catch (Exception e) {
            if (showFailureMessages) {
            	errors.reject("error.requestPassword.invalidEmail");
            }
        	logger.info("reqRecovery failed, user not found, email=" + email);
        }
        
        /*
         * return if any validation errors were detected
         */
        if (errors.hasErrors()){
        	return;
        }
    	
    	/*
    	 * set success message in case no failures will be shown anyway
    	 */
    	if (!showFailureMessages) {
    		request.setAttribute("msg_success", resources.getMessage(
					"msg.success_pw_recovery_request", null, request.getLocale()));
    	}
    	
        /*
         * check user
         */
        if (null == user) {
            return;
        }

        /*
         * try get onetime passwd
         */
        String onetimePW = authenticationService.createOnetimePassword(user.getUsername());

        /*
         * check onetime passwd
         */
        if (null == onetimePW) {
            return;
        }

        /*
         * try sending recovery mail
         */
        try {
        	// TODO fix locale retrieval
        	Locale locale = null;
        	try {
        		locale = UserLocalServiceUtil.getUserByEmailAddress(SecurityContextHolder.getLiferayCompanyId(), email).getLocale();
        	} catch (Exception e) {
        		locale = Locale.getDefault();
        	}
        	
            emailService.sendPassowortRecoveryMail(request.getServerName(), locale, user.getUsername(), user
                    .getFirstname() + " " + user.getLastname(), onetimePW);
        }
        catch (EmailException e) {
        	logger.error("sendPasswordRecoveryMail() failed for email=" + user.getUsername() + 
    				", onetimePW=" + onetimePW, e);
        	if (showFailureMessages) {
				errors.reject("error.emailSendingFailedRecovery");
				return;
        	}
        }
        
        request.setAttribute("ctx", "Login");
        model.clear();
        request.setAttribute("msg_success", resources.getMessage(
				"msg.success_pw_recovery_request", null, Locale.getDefault()));
    }
    
    @RequestMapping
    public ModelAndView handleRenderRequest(RenderRequest request,
    		RenderResponse response) throws Exception 
    {
    	ModelAndView modelAndView = new ModelAndView("ReqRecovery");
    	
    	if (!modelAndView.getModelMap().containsAttribute(COMMAND))
			modelAndView.addObject(COMMAND, new ReqRecoveryCom());
    	
    	Login.alternativeRegistration(modelAndView);
    	
    	return modelAndView;
    }
}
