package org.evolvis.idm.loginregister.constant;

public class ServletConfigKeys {

    public static final String TARENT_LOSTPW_CONFIRM_URL   = "tarent.lostpw.confirm.url";
    
    public static final String TARENT_LOSTPW_CONFIRM_PROTO = "tarent.lostpw.confirm.proto"; // http://
   
    public static final String TARENT_LOSTPW_CONFIRM_PORT  = "tarent.lostpw.confirm.port";  // :8080
}
