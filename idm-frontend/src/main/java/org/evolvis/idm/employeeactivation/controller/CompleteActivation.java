package org.evolvis.idm.employeeactivation.controller;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.constant.PortletConfig;
import org.evolvis.idm.common.service.AuthenticationService;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.loginregister.command.CompleteActivationCom;
import org.evolvis.idm.loginregister.command.LoginCom;
import org.evolvis.idm.loginregister.controller.Login;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.security.SecurityContextHolder;
import org.evolvis.idm.selfmanagement.command.ChangePasswordCom;
import org.evolvis.opensso.rest.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

/**
 * Allowed registered (but not yet activated) employee users to enter personal data and choose their
 * password.
 * 
 * @author Dmytro Mayster
 * @author Jens Neumaier
 */

@Controller
@RequestMapping(value = "view", params = "ctx=CompleteActivation")
@SessionAttributes( { CompleteActivation.COMMAND })
public class CompleteActivation {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected Validator validator;

	@Autowired
	protected UserManagement userManagement;

	@Autowired
	protected AuthenticationService authenticationService;

	@Autowired
	protected Login loginController;

	/**
	 * Handles an <code>CompleteActivation</code> action request by use a {@link RestClient} for connect
	 * to a <code>OpenSSO</code> server.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param command
	 *            The command object {@link ChangePasswordCom} contains the file to upload
	 * @param errors
	 *            Errors object to collect from errors
	 * 
	 */
	@RequestMapping
	public void handleEnterPasswordAction(ActionRequest actionRequest,
			ActionResponse actionResponse, @ModelAttribute(COMMAND) @Valid CompleteActivationCom command,
			Errors errors) {

		logger.info("handleEnterPasswordAction() entered");

		try {

			String passwordPattern = SecurityContextHolder.getClientPropertyMap().get(
					ClientPropertyMap.KEY_SECURITY_PASSWORT_PATTERN);
			if (!passwordPattern.isEmpty() && !command.getPassword().matches(passwordPattern)) {
				errors.rejectValue("password", "violation.password.password");
			}

			/*
			 * return if errors exist
			 */
			if (errors.hasErrors())
				return;
			
			/*
			 * check if there is a not yet activated user with the requested registration key
			 */
			String registerKey = new String(Base64
					.decodeBase64(command.getRegisterKey().getBytes()));
			String uuid = authenticationService.getUUIDForRegistrationKey(registerKey);

			if (uuid == null) {
				errors.reject("error.employee.registerkey");
				return;
			}
			/*
			 * get the user account
			 */
			User user = userManagement.getUserByUuid(uuid);

			/*
			 * update user password
			 */
			if (!authenticationService.updatePassword(user.getUsername(), command.getPassword())) {
				errors.reject("error.employee.password");
				return;
			}

			// update user's names
			userManagement.setUserNames(uuid, command.getFirstname(), command.getLastname());

			// user status will be updated in the login controller
			loginController.handleConfirmRegistrationAction(actionRequest, actionResponse, null,
					errors, command.getRegisterKey());

			// show success message if no errors have been reported
			if (!errors.hasErrors())
				actionRequest.setAttribute("activationSuccessful", true);
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.employee.activate");
		}
	}

	/**
	 * Handles a render request and returns a {@link ModelAndView} object with a CompleteActivation view.
	 * The {@link ModelAndView} contains following objects: <br/>
	 * <br/>
	 * <i>{@link ChangePasswordCom}</i> command object stored under the <code>command</code> name. <br/>
	 * 
	 * @return modelAndView the name of the CompleteActivation view
	 */
	@RequestMapping
	public String handleRenderRequest(
			RenderRequest request,
			RenderResponse response,
			Model model,
			@RequestParam(value = PortletConfig.REGISTER_CONFIRM_KEY, required = false) String registerKey) {

		/*
		 * if additional data has been added and account has been activated successfully show login
		 * form
		 */
		if (request.getAttribute("activationSuccessful") != null) {
			model.addAttribute(COMMAND, new LoginCom());
			return "Login";
		}

		// otherwise show activation form

		boolean displayNameGenerationEnabled = Boolean.valueOf(SecurityContextHolder
				.getClientPropertyMap().get(
						ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED));

		request.setAttribute("displayNameGenerationEnabled", displayNameGenerationEnabled);

		CompleteActivationCom completeActivationCom;
		if (registerKey != null) {
			completeActivationCom = formBackingObject(registerKey);

			PortletURL redirectURL = response.createRenderURL();
			redirectURL.setParameter("ctx", "Login");
			redirectURL.setParameter("action", "ConfirmRegistration");

			model.addAttribute(COMMAND, completeActivationCom);
		} else {
			completeActivationCom = (CompleteActivationCom) model.asMap().get(COMMAND);
		}
		return "CompleteActivation";
	}

	private CompleteActivationCom formBackingObject(String registerKeyBase64) {
		CompleteActivationCom completeActivationCom = new CompleteActivationCom();
		try {
			/*
			 * decode registration key
			 */
			final String registerKey = new String(Base64.decodeBase64(registerKeyBase64.getBytes()));

			/*
			 * check if there is a user for the given registration key
			 */
			String uuid = authenticationService.getUUIDForRegistrationKey(registerKey);
			if (uuid != null) {
				/*
				 * set a new password command and fill it with data form user account replace
				 * generated place holders with empty strings -- the user has to overwrite
				 */
				User user = userManagement.getUserByUuid(uuid);

				logger.info("Found user uuid=" + user.getUuid() + ", username="
						+ user.getUsername() + " for registerKey=" + registerKey);

				completeActivationCom.setRegisterKey(registerKeyBase64);

				boolean displayNameGenerationEnabled = SecurityContextHolder.getClientPropertyMap()
						.getBoolean(
								ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED);
				if (displayNameGenerationEnabled) {
					completeActivationCom.setDisplayName("------");
				} else {
					completeActivationCom.setDisplayName(user.getDisplayName().startsWith(
							EmployeeActivation.GENERATEDPREFIX) ? "" : user.getDisplayName());
				}

				completeActivationCom.setFirstname(user.getFirstname().startsWith(
						EmployeeActivation.GENERATEDPREFIX) ? "" : user.getFirstname());
				completeActivationCom.setLastname(user.getLastname().startsWith(
						EmployeeActivation.GENERATEDPREFIX) ? "" : user.getLastname());
			}

		} catch (Exception e) {
			logger.warn(e);
		}

		return completeActivationCom;
	}

}
