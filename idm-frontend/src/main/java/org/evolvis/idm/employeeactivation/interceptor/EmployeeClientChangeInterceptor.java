package org.evolvis.idm.employeeactivation.interceptor;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.evolvis.idm.common.util.BackendServiceUtil;
import org.evolvis.idm.employeeactivation.controller.EmployeeActivation;
import org.springframework.web.portlet.handler.HandlerInterceptorAdapter;

public class EmployeeClientChangeInterceptor extends HandlerInterceptorAdapter {

	@Override
	protected boolean preHandle(PortletRequest request, PortletResponse response, Object handler) throws Exception {
		EmployeeActivation.updateClient(BackendServiceUtil.getClientManagementService());
		return true;
	}
}
