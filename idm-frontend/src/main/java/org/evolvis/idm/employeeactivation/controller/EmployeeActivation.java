package org.evolvis.idm.employeeactivation.controller;

import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletURL;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.constant.PortletConfig;
import org.evolvis.idm.common.exception.ServiceException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.EmailService;
import org.evolvis.idm.common.util.SynchronizingRestClientUtil;
import org.evolvis.idm.employeeactivation.command.ActivationCom;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.evolvis.idm.synchronization.service.DirectoryServiceSynchronization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

/**
 * Manages a employee activation.
 * 
 * @author Dmytro Mayster
 * @author Michael Kutz
 * @author Jens Neumaier
 */
@Controller
@RequestMapping(value = "view", params = "ctx=EmployeeActivation")
@SessionAttributes( { EmployeeActivation.COMMAND })
public class EmployeeActivation {

	public static final String COMMAND = "command";
	
	public static final String GENERATEDPREFIX = "GENERATED-";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected Validator validator;

	@Autowired
	protected UserManagement userManagement;

	@Autowired
	protected DirectoryServiceSynchronization directoryServiceSynchronization;

	@Autowired
	protected EmailService emailService;

	@Autowired
	protected ClientManagement clientManagement;

	/**
	 * Handles an <code>activateEmployee</code> action request by use a
	 * {@link EmailService} and sends a e-mail to a employee by use a
	 * {@link UserManagement}.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param command
	 *            ActivationCom object with a input data
	 * @param errors
	 *            Errors object to collect from errors
	 */
	@RequestMapping(params = "action=activateEmployee")
	public void handleActivateEmployeeAction(ActionRequest actionRequest,
			ActionResponse actionResponse, @ModelAttribute(COMMAND) @Valid ActivationCom command,
			Errors errors) {

		if (logger.isDebugEnabled()) {
			logger.debug("handleActivateEmployeeAction() entered, command=" + command);
		}
		
		/*
		 * Synchronize local SSO configuration with ClientPropertyMap in case this did not happen before
		 */
		SynchronizingRestClientUtil.getClient(SecurityContextHolder.getClientPropertyMap());

		/*
		 * return if errors exists
		 */
		if (errors.hasErrors())
			return;
		
		/*
		 * get user data form
		 */
		User user = new User();
		try {
			/*
			 * set default generated values
			 */
			user.setUsername(command.getEmail());
			Date date = new Date();
			user.setPassword("employABC123§$%"+date.getTime());
			user.setDisplayName(GENERATEDPREFIX + "displayName" + date.getTime());
			user.setFirstname(GENERATEDPREFIX + "firstname");
			user.setLastname(GENERATEDPREFIX + "lastname");

			if (userManagement.isUserExisting(SecurityContextHolder.getClientName(), user.getUsername())) {
				errors.reject("error.user.duplicateUserEmployee");
				return;
			}
			Client citizenClient = getCitizenClient(clientManagement.getClients(null).getResultList());
			if (citizenClient != null && userManagement.isUserExisting(citizenClient.getName(), user.getUsername())) {
				errors.reject("error.user.duplicateUserCitizen");
				return;
			}
			
			String activationKey = userManagement.registerUser(SecurityContextHolder.getClientName(),
					user.getUsername(), user.getPassword(), user.getDisplayName(), user
							.getFirstname(), user.getLastname(), actionRequest.getLocale()
							.toString());

			// TODO remove quick fix for retrieving uuid - should be
			// returned by registerAccount
			user = userManagement.getUserByUsername(
					SecurityContextHolder.getClient().getName(), user.getUsername());

			if (null != activationKey) {
				/*
				 * encode register key and generate activation link
				 */
				String registerKeyB64 = new String(Base64
						.encodeBase64(activationKey.getBytes()));
				String activationLink = command.getRedirectURL();
				activationLink = activationLink.replace(PortletConfig.REGISTER_CONFIRM_VALUE_PLACEHOLDER, registerKeyB64);

				logger.info("Sendig activation link via mail " + activationLink.toString());

				/*
				 * send activation link via mail to the employee user
				 */
				String fullName = user.getFirstname() + " " + user.getLastname();
				if (fullName.startsWith(GENERATEDPREFIX))
					fullName = resources.getMessage("name.employee.nonamefound", null, actionRequest.getLocale());	
				emailService.sendSelfRegistrationMail(actionRequest.getServerName(), actionRequest.getLocale(), user.getUsername(),
						fullName, activationLink.toString());

				actionRequest.setAttribute("msg_success", resources.getMessage(
						"success.employee.setup", null, actionRequest.getLocale()));
			} else {
				/*
				 * generating activation key failed
				 */
				errors.reject("error.employee.setup");
				return;
			}
		} catch (Exception e) {
			logger.error(e);
			// TODO better exception handling
			try {
				User userToDelete = userManagement.getUserByUsername(SecurityContextHolder
						.getClient().getName(), command.getEmail());
				if (userToDelete != null)
					userManagement.deleteUser(userToDelete.getUuid());
			} catch (BackendException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			errors.reject("error.employee.setup");
			return;
		}
	}

	/**
	 * Handles a render request and returns {@link ModelAndView} with a name of
	 * the EmployeeActivation view. The {@link ModelAndView} contains following
	 * objects: <br/> <br/><i>{@link ActivationCom}</i> command object stored
	 * under the <code>command</code> name.
	 * 
	 * @return modelAndView The {@link ModelAndView} object
	 */
	@RequestMapping
	public ModelAndView handleRenderRequest(ModelMap model, RenderResponse renderResponse) {
		ModelAndView modelAndView = new ModelAndView("EmployeeActivation");
		modelAndView.addObject(COMMAND, formBackingObject(renderResponse));

		return modelAndView;
	}

	public ActivationCom formBackingObject(RenderResponse renderResponse) {
		ActivationCom command = new ActivationCom();
		// generate url to redirect after activation
		PortletURL redirectURL = renderResponse.createRenderURL();
		redirectURL.setParameter("ctx", "CompleteActivation");
		redirectURL.setParameter(PortletConfig.REGISTER_CONFIRM_KEY, PortletConfig.REGISTER_CONFIRM_VALUE_PLACEHOLDER);

		command.setRedirectURL(redirectURL.toString());

		return command;
	}

	public static void updateClient(ClientManagement clientManagement) {
		Client employeeClient = null;
		try {
			// TODO jneuma check for allowed client names
			/*
			 * find out the employee client name (base client name +
			 * "-employee" or any other extension)
			 */
			List<Client> clients = clientManagement.getClients(null).getResultList();
			employeeClient = getEmployeeClient(clients);

			/*
			 * if no employee client for the current base client exists return
			 * with error message
			 */
			if (employeeClient == null) {
				if (PortletConfig.REGISTER_EMPLOYEE_FORCEMULTICLIENTSETUP) {
					throw new ServiceException(org.evolvis.idm.common.exception.Errors.TENANT_NOT_AVAILABLE);
				} else {
					employeeClient = SecurityContextHolder.getClient();
				}
			}

			/*
			 * set current client to the employee client
			 */
			SecurityContextHolder.setClient(employeeClient);
			SecurityContextHolder.setUserClientRealm("/" + employeeClient.getName());
			// TODO jneuma fix user client ...
			// logger.debug(
			// "Activated the following client for employee activation: clientName="
			// + SecurityContextHolder.getClientName() + ", baseClientRealm="
			// + SecurityContextHolder.getUserClientRealm().replace("/", ""));
		} catch (IllegalRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BackendException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Client getEmployeeClient(List<Client> clients) {
		for (Client client : clients) {
			String baseClientName = client.getName().length() < SecurityContextHolder
					.getClientName().length() ? client.getName() : SecurityContextHolder
					.getClientName();
			if (client.getName().indexOf(baseClientName) == 0
					&& client.getName().length() > baseClientName.length()) {
				return client;
			}
		}
		return null;
	}
	
	public static Client getCitizenClient(List<Client> clients) {
		for (Client client : clients) {
			String extendedClientName = client.getName().length() > SecurityContextHolder
					.getClientName().length() ? client.getName() : SecurityContextHolder
					.getClientName();
			if (extendedClientName.indexOf(client.getName()) == 0
					&& client.getName().length() < extendedClientName.length()) {
				return client;
			}
		}
		return null;
	}
}
