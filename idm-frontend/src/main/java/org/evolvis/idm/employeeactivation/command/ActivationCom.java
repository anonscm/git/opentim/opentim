package org.evolvis.idm.employeeactivation.command;

import org.evolvis.idm.common.validation.Email;

/**
 * Command object used by EmployeeActivation controller
 * 
 * @author Dmytro Mayster
 *
 */
public class ActivationCom {

	//@Pattern(message = "violation.employeeactivation.pattern", 
	//		regexp = "[a-zA-Z0-9_+-]{1}[a-zA-Z0-9._+-]{0,57}@[a-zA-Z0-9.-]{2,255}\\.[a-zA-Z]{2,6}")
	@Email
    private String email;
	
	private String redirectURL;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	public String getRedirectURL() {
		return redirectURL;
	}
}
