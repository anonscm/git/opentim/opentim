package org.evolvis.idm.administration.controller;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.administration.model.ClientPropertyMapWrapper;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;

/**
 * @author Frederic Eßer
 * @author Michael Kutz
 * @author Tino Rink
 * @author Jens Neumaier
 */
@Controller
@RequestMapping(value = "view", params = "ctx=ClientConfiguration")
public class ClientConfiguration {

	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected Validator validator;
	
	@Autowired
	protected ClientManagement clientManagement;

	/**
	 * Handler updates the clientPropertyMap of the current client.
	 * 
	 * @param actionRequest
	 * @param clientPropertyMap
	 * @param errors
	 */
	@Secured({"ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN"})
	@RequestMapping(params = "action=updateClientPropertyMap")
	public void handleUpdateClientPropertyMap(ActionRequest actionRequest,
			@ModelAttribute(COMMAND) @Valid ClientPropertyMapWrapper clientPropertyMapWrapper, Errors errors) {
		/*
		 * return in case of validation errors
		 */
		if (errors.hasErrors()) return;
		
		/*
		 * unwrap the ClientPropertyMap
		 */
		ClientPropertyMap clientPropertyMap = clientPropertyMapWrapper.getClientPropertyMap();
		
		/*
		 * store valid data
		 */
		try {
			logger.debug("Saving ClientPropertyMap in session for clientName \""+SecurityContextHolder.getClientName()+"\" and uuid \""+SecurityContextHolder.getUUID()+"\"");
			
			// try to save map clone - do not tamper static map cache before successful saving
			ClientPropertyMap clientPropertyMapCloneWithIds = SecurityContextHolder.getClientPropertyMap().clone();
			// add changed entries to cloned map to override map values and keep entry IDs
			clientPropertyMapCloneWithIds.putAll(clientPropertyMap);
			clientPropertyMap = clientManagement.setClientPropertyMap(SecurityContextHolder.getClientName(), clientPropertyMapCloneWithIds);
			// save successful changes to static cache
			SecurityContextHolder.setClientPropertyMap(clientPropertyMap);
		} catch (Exception e) {
			logger.error("An unknown error occurred while saving client properties, ", e);
			errors.reject("error.setclientpropertymap");
			return;
		}
		
		/*
		 * add success message to action response
		 */
		actionRequest.setAttribute("msg_success", resources.getMessage("success.clientproperties.update", null, actionRequest.getLocale()));
	}

	@Secured({"ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN"})
	@RequestMapping
	public ModelAndView handleRenderRequest(RenderRequest request) {

		logger.info("handleRenderRequest() entered, request=" + request);

		ModelAndView modelAndView = new ModelAndView("ClientConfiguration");
		
		/*
		 * add property map wrapper as command to model
		 * 
		 * TODO handle exceptions
		 */
		try {
			ClientPropertyMapWrapper wrapper = new ClientPropertyMapWrapper(SecurityContextHolder.getClientPropertyMap()); 
			modelAndView.addObject(COMMAND, wrapper);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		/*
		 * add nav points to model
		 */
		modelAndView.addObject("navPoints", AdminNavPoints.getAll());
		
		/*
		 * add clientname to model
		 */
		modelAndView.addObject("clientname", SecurityContextHolder.getClientName());

		return modelAndView;
	}
}