package org.evolvis.idm.administration.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.command.PrivateDataConfig;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.administration.util.DisplayOrderComparator;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.AbstractBean;
import org.evolvis.idm.common.propertyeditor.AttributeGroupPropertyEditor;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * Handle private data configuration.
 * 
 * @author Dmytro Mayster
 */
@Controller
@RequestMapping(value = "view", params = "ctx=PrivateDataConfiguration")
@SessionAttributes( { PrivateDataConfiguration.COMMAND })
public class PrivateDataConfiguration {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected org.evolvis.idm.identity.privatedata.service.PrivateDataConfiguration privateDataConfiguration;

	@Autowired
	protected Validator validator;

	@Autowired
	protected AttributeGroupPropertyEditor attributeGroupPropertyEditor;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(AttributeGroup.class, attributeGroupPropertyEditor);
		binder.setAllowedFields(new String[] { "attribute.name", "attribute.type",
				"attribute.attributeGroup", "attribute.multiple",
				"attribute.additionalTypeConstraint", "attributeGroup.name",
				"attributeGroup.multiple", "*.displayOrder", "*.displayName" });
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=createAttributeGroup")
	public void handleCreateAttributeGroupAction(ActionRequest actionRequest,
			@ModelAttribute(COMMAND) PrivateDataConfig command, Errors errors, ModelMap model) {
		logger.info("handleCreateAttributeGroupAction() entered, name = "
				+ command.getAttributeGroup().getName());

		String clientName = SecurityContextHolder.getClient().getName();

		try {
			// validate and return if errors exists
			errors.pushNestedPath("attributeGroup");
			validator.validate(command.getAttributeGroup(), errors);
			errors.popNestedPath();
			if (errors.hasErrors()) {
				return;
			}

			if (command.getAttributeGroup().getDisplayOrder().compareTo(
					command.getCountAttributeGroups()) > 0) {
				privateDataConfiguration.setAttributeGroup(clientName, command.getAttributeGroup());
			} else {
				List<AttributeGroup> groups = command.getAttributeGroups();
				groups.add(command.getAttributeGroup().getDisplayOrder() - 1, command
						.getAttributeGroup());
				List<AttributeGroup> correctedList = correctDisplayOrder(groups, false);
				privateDataConfiguration.setAttributeGroups(clientName, correctedList);
			}

			actionRequest.setAttribute("msg_success", resources.getMessage(
					"success.attributegroup.createnew", null, actionRequest.getLocale()));

			/*
			 * clear model after successful creation
			 */
			model.clear();
		} catch (BackendException e) {
			/*
			 * show error message if the there already is a attribute group with the new name
			 */
			if (e.getErrorCode().equals(IllegalRequestException.CODE_INVALID_DUPLICATE)) {
				errors.rejectValue("attributeGroup.name", "error.attributegroup.existed");
			} else {
				logger.warn(e);
				errors.reject("error.attributegroup.create");
			}
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=addAttribute")
	public void handleAddAttributeAction(ActionRequest actionRequest,
			@ModelAttribute(COMMAND) PrivateDataConfig command, Errors errors, ModelMap model) {
		logger.info("handleAddAttributeAction() entered, attribute = "
				+ command.getAttribute().getName() + ", group = "
				+ command.getAttribute().getAttributeGroup().getName());

		String clientName = SecurityContextHolder.getClient().getName();

		try {
			/*
			 * validate return if validation errors exists
			 */
			errors.pushNestedPath("attribute");
			validator.validate(command.getAttribute(), errors);
			errors.popNestedPath();
			if (errors.hasErrors()) {
				return;
			}

			AttributeGroup attributeGroup = command.getAttribute().getAttributeGroup();
			if (attributeGroup.getAttributes() == null) {
				command.getAttribute().setDisplayOrder(1);
				attributeGroup.addAttribute(command.getAttribute());
			} else if (command.getAttribute().getDisplayOrder().compareTo(
					attributeGroup.getAttributes().size()) > 0) {
				command.getAttribute().setDisplayOrder(attributeGroup.getAttributes().size() + 1);
				attributeGroup.addAttribute(command.getAttribute());
			} else {
				List<Attribute> attributes = attributeGroup.getAttributes();
				attributes
						.add(command.getAttribute().getDisplayOrder() - 1, command.getAttribute());
				List<Attribute> correctedList = correctDisplayOrder(attributes, false);
				attributeGroup.setAttributes(correctedList);
			}
			privateDataConfiguration.setAttributeGroup(clientName, attributeGroup);
			actionRequest.setAttribute("msg_success", resources.getMessage(
					"success.attributegroup.addattribute", null, actionRequest.getLocale()));

			/*
			 * clear command object after successful add
			 */
			model.clear();
		} catch (BackendException e) {
			/*
			 * show error message if the there already is a attribute group with the new name
			 */
			if (e.getErrorCode().equals(IllegalRequestException.CODE_INVALID_DUPLICATE)) {
				errors.rejectValue("attribute.name", "error.attribute.existed");
			} else {
				logger.warn(e);
				errors.reject("error.attributegroup.addattribute");
			}
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=updateAttributeGroups")
	public void handleUpdateAttributeGroupsAction(ActionRequest actionRequest,
			@ModelAttribute(COMMAND) PrivateDataConfig command, Errors errors) {
		logger.info("handleUpdateAttributeGroupsAction() entered");

		String clientName = SecurityContextHolder.getClient().getName();

		try {
			// validate and return if errors exists
			int i = 0;
			for (AttributeGroup attributeGroup : command.getAttributeGroups()) {
				errors.pushNestedPath("attributeGroups[" + i + "]");
				validator.validate(attributeGroup, errors);
				if (attributeGroup.getAttributes() != null) {
					// an attribute has a group, it has already been validated
					// ((JSR303Validator)validator).setRecursive(false);
					int j = 0;
					for (Attribute attribute : attributeGroup.getAttributes()) {
						errors.pushNestedPath("attributes[" + j + "]");
						validator.validate(attribute, errors);
						errors.popNestedPath();
						j++;
					}
					// ((JSR303Validator)validator).setRecursive(true);
				}
				errors.popNestedPath();
				i++;
			}
			if (errors.hasErrors()) {
				return;
			}

			List<AttributeGroup> groups = command.getAttributeGroups();
			sortBeanList(groups);
			List<AttributeGroup> correctedList = correctDisplayOrder(groups, true);
			privateDataConfiguration.setAttributeGroups(clientName, correctedList);

			actionRequest.setAttribute("msg_success", resources.getMessage(
					"success.attributegroups.update", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.attributegroups.update");
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=RemoveAttribute", "!confirmed", "!denied" })
	public void handleUnconfirmedRemoveAttributeAction(
			@RequestParam(value = "attributeId") final Long attributeId,
			@RequestParam(value = "attributeGroupId") final AttributeGroup attributeGroup,
			final ActionRequest request) {
		logger.info("handleUnconfirmedRemoveAttributeAction() entered, attributeId = "
				+ attributeId + ", attributeGroupId = " + attributeGroup.getId());

		/*
		 * ask for confirmation and return
		 */
		request.setAttribute("attributeGroupToRemoveAttributeFrom", attributeGroup);
		request.setAttribute("attributeToRemove", attributeGroup.getAttribute(attributeId));
		request.setAttribute("confirmation", "RemoveAttribute");
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=RemoveAttribute", "denied" })
	public void handleDeniedRemoveAttributeAction(final ActionRequest request) {
		logger.info("handleDeniedRemoveAttributeAction() entered");

		/*
		 * remove confirmation request if confirmation was denied
		 */
		request.removeAttribute("removeAttribute");
		request.removeAttribute("removeAttributeFromAttributeGroup");
		request.removeAttribute("confirmation");
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=RemoveAttribute", "confirmed" })
	public void handleConfirmedRemoveAttributeAction(
			@RequestParam(value = "attributeId") final Long attributeId,
			@RequestParam(value = "attributeGroupId") final AttributeGroup attributeGroup,
			final ActionRequest request, final Locale locale) {
		logger.info("handleConfirmedRemoveAttributeAction() entered, attributeId = " + attributeId
				+ ", attributeGroupId = " + attributeGroup.getId());

		/*
		 * delete the Attribute
		 */
		try {
			attributeGroup.getAttributes().remove(attributeGroup.getAttribute(attributeId));
			List<Attribute> correctedAttributes = correctDisplayOrder(attributeGroup
					.getAttributes(), false);
			attributeGroup.setAttributes(correctedAttributes);
			privateDataConfiguration.setAttributeGroup(SecurityContextHolder.getClientName(),
					attributeGroup);
			request.setAttribute("msg_success", resources.getMessage(
					"success.attributegroup.removeattribute", null, locale));
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_success", resources.getMessage(
					"error.attributegroup.removeattribute", null, locale));
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=DeleteAttributeGroup", "!confirmed", "!denied" })
	public void handleUnconfirmedDeleteAttributeGroupAction(
			@RequestParam("attributeGroupId") final AttributeGroup attributeGroup,
			final ActionRequest request) {
		logger.info("handleUnconfirmedDeleteAttributeGroupAction() entered, attributeGroupId="
				+ attributeGroup.getId());

		/*
		 * ask for confirmation and return
		 */
		request.setAttribute("attributeGroupToDelete", attributeGroup);
		request.setAttribute("confirmation", "DeleteAttributeGroup");
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=DeleteAttributeGroup", "denied" })
	public void handleDeniedDeleteAttributeGroupAction(final ActionRequest request) {
		logger.info("handleConfirmedAttributeGroupAction() entered");

		/*
		 * remove confirmation request if confirmation was denied
		 */
		request.removeAttribute("deleteAttributeGroup");
		request.removeAttribute("confirmation");
		return;
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=DeleteAttributeGroup", "confirmed" })
	public void handleConfirmedDeleteAttributeGroupAction(
			@RequestParam(value = "attributeGroupId") final AttributeGroup attributeGroup,
			@ModelAttribute(COMMAND) PrivateDataConfig privateDataConfig, final Errors errors,
			final ActionRequest request, final Locale locale) {
		logger.info("handleConfirmedAttributeGroupAction() entered, attributeGroupId="
				+ attributeGroup.getId());

		/*
		 * delete the AttributeGroup
		 */
		try {
			privateDataConfiguration.deleteAttributeGroup(SecurityContextHolder.getClientName(),
					attributeGroup);
			privateDataConfig.getAttributeGroups().remove(attributeGroup);
			List<AttributeGroup> correctedList = correctDisplayOrder(privateDataConfig
					.getAttributeGroups(), false);
			privateDataConfiguration.setAttributeGroups(SecurityContextHolder.getClientName(),
					correctedList);

			request.setAttribute("msg_success", resources.getMessage(
					"success.attributegroup.delete", null, locale));
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_error", resources.getMessage("error.attributegroup.delete",
					null, locale));
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=shiftAttributeGroup")
	public void handleShiftupAttributeGroupAction(ActionRequest actionRequest,
			@RequestParam("attributeGroupId") final Long attributeGroupId,
			@RequestParam("shiftOffset") final int shiftOffset,
			@ModelAttribute(COMMAND) PrivateDataConfig command, Errors errors) {
		logger.info("handleShiftupAttributeGroupAction() entered, attributeGroupId = "
				+ attributeGroupId + ", shiftOffset = " + shiftOffset);

		String clientName = SecurityContextHolder.getClient().getName();

		try {
			AttributeGroup attributeGroup = privateDataConfiguration.getAttributeGroupById(
					SecurityContextHolder.getClientName(), attributeGroupId);

			List<AttributeGroup> correctedList = shiftListEntry(command.getAttributeGroups(),
					attributeGroup, shiftOffset);
			privateDataConfiguration.setAttributeGroups(clientName, correctedList);

			actionRequest.setAttribute("msg_success", resources.getMessage(
					"success.attributegroup.shift", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.attributegroup.shift");
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=shiftAttribute")
	public void handleShiftupAttributeAction(ActionRequest actionRequest,
			@RequestParam("attributeGroupId") final Long attributeGroupId,
			@RequestParam("attributeId") final Long attributeId,
			@RequestParam("shiftOffset") final int shiftOffset,
			@ModelAttribute(COMMAND) PrivateDataConfig command, Errors errors) {
		logger.info("handleShiftupAttributeAction() entered, attributeGroupId = "
				+ attributeGroupId + "attributeId = " + attributeId + ", shiftOffset = "
				+ shiftOffset);

		String clientName = SecurityContextHolder.getClient().getName();

		try {
			AttributeGroup attributeGroup = privateDataConfiguration.getAttributeGroupById(
					SecurityContextHolder.getClientName(), attributeGroupId);

			List<Attribute> correctedList = shiftListEntry(attributeGroup.getAttributes(),
					attributeGroup.getAttribute(attributeId), shiftOffset);

			attributeGroup.setAttributes(correctedList);
			privateDataConfiguration.setAttributeGroup(clientName, attributeGroup);

			actionRequest.setAttribute("msg_success", resources.getMessage(
					"success.attribute.shift", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.attribute.shift");
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping
	public String handleRenderRequest(ModelMap model, RenderRequest request, RenderResponse response) {
		logger.info("handleRenderRequest() entered");

		String clientName = SecurityContextHolder.getClientName();
		PrivateDataConfig privateDataConfig;

		Object command = model.get(COMMAND);
		/*
		 * add new empty attribute and attribute group objects if there are non yet
		 */
		if (command == null || !(command instanceof PrivateDataConfig)) {
			privateDataConfig = new PrivateDataConfig(resources);
			model.addAttribute(COMMAND, formBackingObject(clientName, privateDataConfig));
		} else {
			privateDataConfig = (PrivateDataConfig) model.get(COMMAND);
		}

		/*
		 * add all yet existing groups to the command object
		 */
		try {
			privateDataConfig.setAttributeGroups(privateDataConfiguration
					.getAttributeGroups(clientName));
		} catch (BackendException e) {
			logger.warn(e);
		}
		model.addAllAttributes(referenceData(clientName));
		model.addAttribute("navPoints", AdminNavPoints.getAll());

		return "PrivateDataConfiguration";
	}

	private PrivateDataConfig formBackingObject(String clientName,
			PrivateDataConfig privateDataConfig) {
		try {
			privateDataConfig.setAttribute(new Attribute());
			privateDataConfig.setAttributeGroup(new AttributeGroup());
		} catch (Exception e) {
			logger.warn(e);
		}

		return privateDataConfig;
	}

	private Map<String, Object> referenceData(String clientName) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map.put("clientname", clientName);
		} catch (Exception e) {
			logger.warn(e);
		}

		return map;
	}

	private <T extends AbstractBean> List<T> sortBeanList(List<T> list) {
		if (list != null) {
			Collections.sort(list, new DisplayOrderComparator());
			for (AbstractBean bean : list) {
				if (bean instanceof AttributeGroup) {
					sortBeanList(((AttributeGroup) bean).getAttributes());
				}
			}
		}
		return list;
	}

	private <T extends AbstractBean> List<T> correctDisplayOrder(List<T> list, boolean recursive) {
		int orderId = 1;
		if (list != null) {
			for (AbstractBean bean : list) {
				if (bean instanceof AttributeGroup) {
					((AttributeGroup) bean).setDisplayOrder(orderId);
					if (recursive)
						correctDisplayOrder(((AttributeGroup) bean).getAttributes(), false);
				} else if (bean instanceof Attribute)
					((Attribute) bean).setDisplayOrder(orderId);
				orderId++;
			}
		}
		return list;
	}

	private <T extends AbstractBean> List<T> shiftListEntry(List<T> list, T bean, int shiftOffset) {
		int actualIndex = list.indexOf(bean);
		int shiftToIndex = actualIndex + shiftOffset;
		// Prevent IndexOutOfBoundsException and shift in the circle:
		if (shiftToIndex < 0)
			shiftToIndex = list.size() - 1;
		else if (shiftToIndex >= list.size())
			shiftToIndex = 0;

		if (actualIndex == shiftToIndex) {
			// Do nothing here
		} else {
			bean = list.remove(list.indexOf(bean));
			list.add(shiftToIndex, bean);
			correctDisplayOrder(list, false);
		}

		return list;
	}

}
