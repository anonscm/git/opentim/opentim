package org.evolvis.idm.administration.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.administration.model.AttributeWrapper;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.model.NavPoint;
import org.evolvis.idm.common.propertyeditor.AttributeGroupPropertyEditor;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.AttributeType;
import org.evolvis.idm.identity.privatedata.service.PrivateDataConfiguration;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * @author Michael Kutz
 */
@Controller
@RequestMapping(value = "view", params = "ctx=PrivateDataDetailConfiguration")
@SessionAttributes(PrivateDataDetailConfiguration.COMMAND)
public class PrivateDataDetailConfiguration {
	private final Log logger = LogFactory.getLog(getClass());

	public static final String COMMAND = "command";

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected PrivateDataConfiguration privateDataConfiguration;

	@Autowired
	protected AttributeGroupPropertyEditor attributeGroupPropertyEditor;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(AttributeGroup.class, attributeGroupPropertyEditor);
	}

	@ModelAttribute("clientname")
	protected String getClientName() {
		return SecurityContextHolder.getClientName();
	}

	@ModelAttribute("navPoints")
	protected List<NavPoint> getNavPoints() {
		return AdminNavPoints.getAll();
	}

	/**
	 * Adds a new sub attribute to the command object using {@link addSubAttributeToAttribute}
	 * method.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=addSubAttribute")
	public void handleAddSubAttributeAction(@ModelAttribute(COMMAND) final AttributeWrapper attributeWrapper,
			final Errors errors) {
		logger.info("handleAddSubAttributeAction() entered, attributeId=" + attributeWrapper.getAttribute().getId());

		/*
		 * create a new sub attribute (always of type string and named after the parent)
		 */
		addSubAttributeToAttribute(attributeWrapper.getAttribute());
	}

	/**
	 * Removes an existing sub attribute form the command object.
	 * 
	 * @param subAttributeIndex
	 *            the index the sub attribute that should be removed.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=removeSubAttribute")
	public void handleRemoveSubAttributeAction(@ModelAttribute(COMMAND) final AttributeWrapper attributeWrapper,
			final Errors errors, @RequestParam("subAttributeIndex") final int subAttributeIndex) {
		logger.info("handleRemoveSubAttributeAction() entered, attributeId=" + attributeWrapper.getAttribute().getId()
				+ " sub attribute with index " + subAttributeIndex + " to be removed.");

		Attribute subAttribute = attributeWrapper.getAttribute().getSubAttributes().remove(subAttributeIndex);
		
		try {
			privateDataConfiguration.deleteAttribute(SecurityContextHolder.getClientName(), subAttribute);
		} catch (BackendException e) {
			errors.reject("error.attribute.update");
		}
	}

	/**
	 * Persists all changes made to the command object using the PrivateDataConfiguration service.
	 * 
	 * This also performs a validation on the command object and will not persist any changes if it
	 * fails.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=updateAttribute")
	public void handleUpdateAttributeAction(
			@ModelAttribute(COMMAND) @Valid final AttributeWrapper attributeWrapper,
			final Errors errors, final ActionRequest request, final Locale locale) {
		logger.info("handleUpdateAttributeAction() entered, attributeId=" + attributeWrapper.getAttribute().getId());

		/*
		 * return if validation failed
		 */
		if (errors.hasErrors()) {
			return;
		}

		/*
		 * persist changes
		 */
		try {
			privateDataConfiguration.setAttribute(SecurityContextHolder.getClientName(), attributeWrapper.getAttribute());
			request.setAttribute("msg_success", resources.getMessage("success.attribute.edit",
					new String[] {attributeWrapper.getAttribute().getDisplayName()}, locale));
		} catch (BackendException e) {
			errors.reject("error.attribute.update");
		}
	}

	/**
	 * Handles all <code>render</code> requests.
	 * 
	 * @param attributeId
	 *            a valid attribute ID. Should be set for the first call, afterwards the command
	 *            object is used.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RenderMapping
	public String handleRenderRequest(
			@RequestParam(value = "attributeId", required = false) final Long attributeId,
			@RequestParam(value = "attributeGroupId", required = false) final AttributeGroup attributeGroup,
			final ModelMap model) {
		/*
		 * fetch command object form service if it's not yet in the model
		 */
		if (!model.containsAttribute(COMMAND) || !(model.get(COMMAND) instanceof AttributeWrapper)) {
			AttributeWrapper attributeWrapper = new AttributeWrapper(attributeGroup.getAttribute(attributeId));
			// attribute = privateDataConfiguration.getAttributeById(SecurityContextHolder
			// .getClientName(), attributeId);

			/*
			 * create sub attributes list with at least one entry
			 */
			if (attributeWrapper.getAttribute().getType().equals(AttributeType.SELECTLIST)) {
				List<Attribute> subAttributes = attributeWrapper.getAttribute().getSubAttributes();
				if (subAttributes == null || subAttributes.isEmpty()) {
					subAttributes = new ArrayList<Attribute>();
					attributeWrapper.getAttribute().setSubAttributes(subAttributes);
					addSubAttributeToAttribute(attributeWrapper.getAttribute());
				}
			}

			model.addAttribute(COMMAND, attributeWrapper);
		}
		return "PrivateDataDetailConfiguration";
	}

	/**
	 * Creates a new attribute of {@link AttributeType} STRING, sets its name to the given parent's
	 * name combined with a counter (e.g. "parent[2]" if parent has already two sub attributes),
	 * sets the parent property to the given parent attribute and finally adds it to the parent's
	 * sub attributes.
	 * 
	 * This does not persist anything, it just changes the sub attribute list of the given attribute
	 * in place.
	 * 
	 * @param parent
	 *            The attribute that should receive a new sub attribute.
	 */
	private void addSubAttributeToAttribute(Attribute parent) {
		Attribute subAttribute = new Attribute();
		subAttribute.setParentAttribute(parent);
		subAttribute.setType(AttributeType.STRING);

		parent.addSubAttribute(subAttribute);
	}

}
