package org.evolvis.idm.administration.validators;

import org.evolvis.idm.administration.command.CreateRole;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class RoleValidator implements Validator {

	@SuppressWarnings("unchecked")
	@Override
	public boolean supports(Class clazz) {
		return CreateRole.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "application", "application.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.empty");
	}

}
