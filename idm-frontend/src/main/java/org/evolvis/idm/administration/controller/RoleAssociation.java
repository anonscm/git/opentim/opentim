package org.evolvis.idm.administration.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.administration.util.CachedUserList;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.propertyeditor.ApplicationPropertyEditor;
import org.evolvis.idm.common.propertyeditor.RolePropertyEditor;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.ApplicationPropertyFields;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.model.RoleUser;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * @author d.ball@tarent.de
 * @author Dmytro Mayster
 * 
 */
@Controller
@RequestMapping(value = "view", params = "ctx=RoleAssociation")
@SessionAttributes(RoleAssociation.COMMAND)
public class RoleAssociation {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected Validator validator;

	@Autowired
	protected ApplicationManagement applicationManagement;

	@Autowired
	protected GroupAndRoleManagement groupAndRoleManagement;

	@Autowired
	protected ApplicationPropertyEditor applicationPropertyEditor;

	@Autowired
	protected RolePropertyEditor rolePropertyEditor;

	@Autowired
	protected CachedUserList cachedUserList;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Application.class, applicationPropertyEditor);
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=updateRole")
	public void handleUpdateRoleAction(@ModelAttribute(COMMAND) @Valid final Role role,
			final Errors errors,
			@RequestParam(value = "confirmed", required = false) final String confirmed,
			@RequestParam(value = "denied", required = false) final String denied,
			final Locale locale, final ActionRequest request, final ModelMap model) {

		Client client = SecurityContextHolder.getClient();

		logger.info("handleUpdateRoleAction() entered, clientId=" + client.getId() + ", name="
				+ role.getName());

		/*
		 * return if errors exist
		 */
		if (errors.hasErrors()) {
			return;
		}

		/*
		 * if the name of the Role should be changed, ask for confirmation
		 */
		try {
			Role preUpdateRole = groupAndRoleManagement.getRole(client.getName(), role.getId());
			if (!preUpdateRole.getName().equals(role.getName())) {

				/*
				 * show confirmation request
				 */
				if (confirmed == null && denied == null) {
					request.setAttribute("confirmation", "UpdateRole");
					return;
				}
				/*
				 * remove confirmation request if it was denied and reset the form
				 */
				else if (denied != null) {
					request.removeAttribute("confirmation");
					model.remove(COMMAND);
					model.addAttribute(COMMAND, preUpdateRole);
					return;
				}

			}
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.role.edit");
		}

		/*
		 * update the Role
		 */
		try {
			groupAndRoleManagement.setRole(SecurityContextHolder.getClientName(), role);
			request.setAttribute("msg_success", resources.getMessage("success.role.edit", null,
					locale));
		} catch (BackendException e) {
			/*
			 * show error message if the there already is a Role with the new name
			 */
			if (e.getErrorCode().equals(IllegalRequestException.CODE_INVALID_DUPLICATE)) {
				errors.rejectValue("name", "error.role.existed");
			} else {
				logger.warn(e);
				errors.reject("error.role.edit");
			}
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=addUser")
	public void handleAddUserAction(ActionRequest actionRequest,
			@RequestParam("userId") final String uuid, @ModelAttribute(COMMAND) final Role role,
			Errors errors) {
		logger.info("handleAddUserAction() entered, groleId=" + role.getId());

		try {
			groupAndRoleManagement.addUserToRole(uuid, role.getId(), "default");
			actionRequest.setAttribute("msg_success", resources.getMessage("success.user.add",
					null, actionRequest.getLocale()));

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.user.add");
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=removeUser")
	public void handleRemoveUserAction(ActionRequest actionRequest,
			@RequestParam("userId") final String uuid, @ModelAttribute(COMMAND) final Role role,
			Errors errors) {
		logger.info("handleRemoveUserAction() entered, userId=" + uuid);

		try {
			groupAndRoleManagement.deleteUserFromRole(uuid, role.getId(), "default");
			actionRequest.setAttribute("msg_success", resources.getMessage("success.user.remove",
					null, actionRequest.getLocale()));

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.user.delete");
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=addGroup")
	public void handleAddGroupAction(ActionRequest actionRequest,
			@RequestParam("groupId") final Long groupId, @ModelAttribute(COMMAND) final Role role,
			Errors errors) {
		logger
				.info("handleAddGroupAction entered, groupId=" + groupId + ", roleId="
						+ role.getId());

		try {
			groupAndRoleManagement.addGroupToRole(SecurityContextHolder.getClientName(), groupId,
					role.getId(), "default");
			actionRequest.setAttribute("msg_success", resources.getMessage("success.group.add",
					null, actionRequest.getLocale()));

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.group.add");
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=removeGroup")
	public void handleRemoveGroupAction(ActionRequest actionRequest,
			@RequestParam("groupId") final Long groupId, @ModelAttribute(COMMAND) final Role role,
			Errors errors) {
		logger.info("handleRemoveGroupAction() entered, groupId=" + groupId + ", roleId="
				+ role.getId());

		try {
			groupAndRoleManagement.deleteGroupFromRole(SecurityContextHolder.getClientName(),
					groupId, role.getId(), "default");
			actionRequest.setAttribute("msg_success", resources.getMessage("success.group.delete",
					null, actionRequest.getLocale()));

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.group.delete");
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse response, Model model,
			@RequestParam(value = "roleId", required = false) Long roleId,
			@RequestParam(value = "appId", required = false) Long appId) {
		logger.info("handleRenderRequest() entered, roleId=" + roleId);

		Role role;
		if (roleId != null) {
			role = formBackingObject(roleId, appId);
			model.addAttribute(COMMAND, role);
		} else {
			role = (Role) model.asMap().get(COMMAND);
		}

		model.addAllAttributes(referenceData(request, role));
		model.addAttribute("navPoints", AdminNavPoints.getAll());

		return "RoleAssociation";
	}

	private Role formBackingObject(Long roleId, Long appId) {
		Role role = new Role();
		try {
			role = groupAndRoleManagement.getRole(SecurityContextHolder.getClientName(), roleId);
			role.setApplication(applicationManagement.getApplicationById(SecurityContextHolder
					.getClientName(), appId));
		} catch (Exception e) {
			logger.warn(e);
		}

		return role;
	}

	private Map<String, Object> referenceData(RenderRequest request, Role role) {
		Map<String, Object> map = new HashMap<String, Object>();

		String clientName = SecurityContextHolder.getClientName();

		map.put("clientname", clientName);

		try {
			QueryDescriptor applicationsQueryDescriptor = new QueryDescriptor();
			applicationsQueryDescriptor.addOrderProperty(new BeanOrderProperty(Application.class,
					ApplicationPropertyFields.FIELD_PATH_DISPLAYNAME, true));
			map.put("applications", applicationManagement.getApplications(clientName,
					applicationsQueryDescriptor).getResultList());

			map.put("allAccounts", cachedUserList.getUsers(request));

			List<RoleUser> roleUsers = groupAndRoleManagement.getRoleUsers(SecurityContextHolder
					.getClientName(), role.getId(), "default");
			map.put("accounts", roleUsers);

			// TODO sort lists of groups
			List<Group> toUseGroups = groupAndRoleManagement.getGroups(clientName, null)
					.getResultList();
			List<Group> roleGroups = groupAndRoleManagement.getGroupsByRoleId(SecurityContextHolder
					.getClientName(), role.getId(), "default");
			toUseGroups.removeAll(roleGroups);

			map.put("to_use_groups", toUseGroups);
			map.put("role_groups", roleGroups);

		} catch (Exception e) {
			logger.warn(e);
		}

		return map;
	}

}
