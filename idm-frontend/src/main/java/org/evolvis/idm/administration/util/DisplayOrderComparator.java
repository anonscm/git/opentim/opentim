package org.evolvis.idm.administration.util;

import java.util.Comparator;

import org.evolvis.idm.common.model.AbstractBean;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;


public class DisplayOrderComparator implements Comparator<AbstractBean> {

	public int compare(AbstractBean arg0, AbstractBean arg1) {
		if (arg0 instanceof AttributeGroup && ((AttributeGroup) arg0).getDisplayOrder() != null && ((AttributeGroup) arg1).getDisplayOrder() != null) {
			if (((AttributeGroup) arg0).getDisplayOrder() != null && ((AttributeGroup) arg1).getDisplayOrder() != null)
				return ((AttributeGroup) arg0).getDisplayOrder() - ((AttributeGroup) arg1).getDisplayOrder();
			else if (((AttributeGroup) arg0).getDisplayOrder() == null)
				return ((AttributeGroup) arg1).getDisplayOrder() == null ? 0 : -1;
			else
				return 1;
		}
		else if (arg0 instanceof Attribute) {
			if (((Attribute) arg0).getDisplayOrder() != null && ((Attribute) arg1).getDisplayOrder() != null)
				return ((Attribute) arg0).getDisplayOrder() - ((Attribute) arg1).getDisplayOrder();
			else if (((Attribute) arg0).getDisplayOrder() == null)
				return ((Attribute) arg1).getDisplayOrder() == null ? 0 : -1;
			else
				return 1;
		}
		else if (arg0 instanceof ValueSet) {
			if (((ValueSet) arg0).getSortOrder() != null && ((ValueSet) arg1).getSortOrder() != null)
				return ((ValueSet) arg0).getSortOrder() - ((ValueSet) arg1).getSortOrder();
			else if (((ValueSet) arg0).getSortOrder() == null)
				return ((ValueSet) arg1).getSortOrder() == null ? 0 : -1;
			else
				return 1;
		}
		else if (arg0 instanceof Value) {
			if (((Value) arg0).getSortOrder() != null && ((Value) arg1).getSortOrder() != null)
				return ((Value) arg0).getSortOrder() - ((Value) arg1).getSortOrder();
			else if (((Value) arg0).getSortOrder() == null)
				return ((Value) arg1).getSortOrder() == null ? 0 : -1;
			else
				return 1;
		}
		return 0;
	}
}
