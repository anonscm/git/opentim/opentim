package org.evolvis.idm.administration.command;

import java.io.Serializable;

public class CreateRole implements Serializable {

	// TODO use generated
	private static final long	serialVersionUID	= 1L;

	private Long application;
	private String name;
	private String displayName;

	public Long getApplication() {
		return application;
	}
	public void setApplication(Long application) {
		this.application = application;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((application == null) ? 0 : application.hashCode());
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (!(obj instanceof CreateRole)) return false;
		CreateRole other = (CreateRole) obj;
		if (application == null) {
			if (other.application != null) return false;
		}
		else if (!application.equals(other.application)) return false;
		if (displayName == null) {
			if (other.displayName != null) return false;
		}
		else if (!displayName.equals(other.displayName)) return false;
		if (name == null) {
			if (other.name != null) return false;
		}
		else if (!name.equals(other.name)) return false;
		return true;
	}
	@Override
	public String toString() {
		return "CreateRole [application=" + application + ", displayName=" + displayName + ", name=" + name + "]";
	}

}
