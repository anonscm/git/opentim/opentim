package org.evolvis.idm.administration.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.propertyeditor.ApplicationPropertyEditor;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.ApplicationPropertyFields;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

/**
 * This controller class handles the {@link Application} management.
 * 
 * @author Frederic Eßer
 * @author Dmytro Mayster
 */
@Controller
@RequestMapping(value = "view", params = "ctx=ApplicationManagement")
@SessionAttributes(ApplicationManagement.COMMAND)
public class ApplicationManagement {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected Validator validator;

	@Autowired
	protected ApplicationPropertyEditor applicationPropertyEditor;

	@Autowired
	protected org.evolvis.idm.relation.multitenancy.service.ApplicationManagement applicationManagement;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Application.class, applicationPropertyEditor);
		binder.setAllowedFields(new String[] { "name", "displayName" });
	}

	@RequestMapping
	public ModelAndView handleRenderRequest(
			@RequestParam(value = "keywords", required = false) String search,
			@RequestParam(value = "sortOrder", defaultValue = "displayName.ASC") String sortOrder,
			@RequestParam(value = "page", defaultValue = "1") Integer page,
			RenderRequest request, RenderResponse response) {
		logger.info("handleRenderRequest() entered, request=" + request);

		Application application = new Application();
		application.setClient(SecurityContextHolder.getClient()); // TODO this should not be necessary!
		ModelAndView modelAndView = new ModelAndView("ApplicationManagement", COMMAND,
				application);
		modelAndView.addAllObjects(referenceData(search, sortOrder, request, response, page));
		modelAndView.addObject("navPoints", AdminNavPoints.getAll());

		return modelAndView;
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=Create")
	public void handleCreateAction(@ModelAttribute(COMMAND) @Valid Application application,
			Errors errors, ActionRequest actionRequest, ModelMap model) {
		Client client = SecurityContextHolder.getClient();

		logger.info("handleCreateAction() entered, clientId=" + client.getId() + ", name="
				+ application.getName());

		try {
			application.setClient(client);

			/*
			 * return if there are any validation errors
			 */
			if (errors.hasErrors()) {
				return;
			}

			applicationManagement
					.setApplication(SecurityContextHolder.getClientName(), application);
			actionRequest.setAttribute("msg_success", resources.getMessage(
					"success.applications.create", null, actionRequest.getLocale()));
			// clear the form after success submit
			model.clear();
		} catch (BackendException e) {
			/*
			 * show error message if the there already is a application with the new name
			 */
			if (e.getErrorCode().equals(IllegalRequestException.CODE_INVALID_DUPLICATE)) {
				errors.rejectValue("name", "error.application.existed");
			} else {
				logger.warn(e);
				errors.reject("error.application.create");
			}
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=Delete", "!confirmed", "!denied" })
	public void handleUnconfirmedDeleteAction(
			@RequestParam("applicationId") Application application,
			ActionRequest request) {
		logger.info("handleUnconfirmedDeleteAction() entered, applicationId = "+ application.getId());

		/*
		 * ask for confirmation and return
		 */
		request.setAttribute("deleteApplication", application);
		request.setAttribute("confirmation", "Delete");
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=Delete", "denied" })
	public void handleDeniedDeleteAction(final ActionRequest request) {
		logger.info("handleDeniedDeleteAction() entered");
		
		/*
		 * return if the user did not confirm the action
		 */
		request.removeAttribute("deleteApplication");
		request.removeAttribute("confirmation");
	}
	
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=Delete", "confirmed" })
	public void handleConfirmedDeleteAction(
			@RequestParam("applicationId") Application application,
			ActionRequest request,
			Locale locale) {
		logger.info("handleConfirmedDeleteAction() entered, applicationId=" + application.getId());

		/*
		 * delete the application
		 */
		try {
			applicationManagement.deleteApplication(SecurityContextHolder.getClientName(),
					application);
			request.setAttribute("msg_success", resources.getMessage("success.applications.delete",
					null, request.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_error", resources.getMessage("error.applications.delete",
					null, locale));
		}
	}

	private Map<String, Object> referenceData(String search, String sortOrder,
			RenderRequest renderRequest, RenderResponse renderResponse, Integer page) {
		Map<String, Object> map = new HashMap<String, Object>();

		/*
		 * remove illegal characters from search and set warning
		 */
		if (search != null && !search.matches(DisplayName.PATTERN)) {
			search = search.replaceAll(DisplayName.PATTERN_INVERSE_ONECHAR, "");
			renderRequest.setAttribute("msg_warning", resources.getMessage(
					"warning.search.removedillegalchars", null, renderResponse.getLocale()));
		}

		map.put("clientname", SecurityContextHolder.getClientName());
		map.put("keywords", search);

		/*
		 * prepare the query descriptor
		 */
		QueryDescriptor queryDescriptor = new QueryDescriptor();

		/*
		 * add order
		 */
		BeanOrderProperty orderProperty = new BeanOrderProperty(Application.class, sortOrder
				.split("\\.")[0], "ASC".equals(sortOrder.split("\\.")[1]));
		queryDescriptor.addOrderProperty(orderProperty);

		/*
		 * add paging
		 */
		queryDescriptor.setLimit(SecurityContextHolder.getPagingLimit());
		queryDescriptor.setOffSet((page - 1) * queryDescriptor.getLimit());

		/*
		 * add search
		 */
		if (search != null) {
			BeanSearchProperty searchPropertyName = new BeanSearchProperty(Application.class,
					ApplicationPropertyFields.FIELD_PATH_NAME, search);
			queryDescriptor.addSearchProperty(searchPropertyName);

			BeanSearchProperty searchPropertyDisplayName = new BeanSearchProperty(
					Application.class, ApplicationPropertyFields.FIELD_PATH_DISPLAYNAME, search);
			queryDescriptor.addSearchProperty(searchPropertyDisplayName);
		}

		try {
			QueryResult<Application> applicationResults = applicationManagement.getApplications(
					SecurityContextHolder.getClientName(), queryDescriptor);

			/*
			 * correct page number (might be greater than last page due to searching)
			 */
			page = applicationResults.getPageNumber();

			/*
			 * generate base URLs for use in tag libs
			 */
			PortletURL pageUrl = renderResponse.createRenderURL();
			pageUrl.setParameter("ctx", "ApplicationManagement");
			pageUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			pageUrl.setParameter("sortOrder", sortOrder);
			pageUrl.setParameter("keywords", search);
			map.put("pageUrl", pageUrl);
			PortletURL sortUrl = renderResponse.createRenderURL();
			sortUrl.setParameter("ctx", "ApplicationManagement");
			sortUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			sortUrl.setParameter("page", Integer.toString(page));
			sortUrl.setParameter("keywords", search);
			map.put("sortUrl", sortUrl);

			map.put("applications", applicationResults.getResultList());
			map.put("page", page);
			map.put("itemCount", applicationResults.getTotalSize());
			map.put("itemLimit", queryDescriptor.getLimit());
			map.put("sortUrl", sortUrl);
			map.put("currentSortingOrder", sortOrder);

			/*
			 * set message for search
			 */
			if (search != null && !"".equals(search)) {
				renderRequest.setAttribute("msg_searchstatus", resources.getMessage(
						"success.search",
						new Object[] { search, applicationResults.getTotalSize() }, renderResponse
								.getLocale()));
			}
		} catch (Exception e) {
			logger.warn(e);
		}

		return map;
	}
}