/**
 * 
 */
package org.evolvis.idm.administration.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.administration.util.GroupPermissionChecker;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.propertyeditor.GroupPropertyEditor;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.organization.model.OrgUnitMapEntry;
import org.evolvis.idm.relation.organization.model.OrgUnitMapEntryQueryResult;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.GroupPropertyFields;
import org.evolvis.idm.relation.permission.model.InternalRoleScope;
import org.evolvis.idm.relation.permission.model.InternalRoleScopePropertyFields;
import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * @author Michael Kutz, tarent GmbH
 */
@Controller
@RequestMapping(value = "view", params = "ctx=OrganizationDetailManagement")
@SessionAttributes(OrganizationDetailManagement.COMMAND)
public class OrganizationDetailManagement {

	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected GroupAndRoleManagement groupAndRoleManagement;

	@Autowired
	protected Validator validator;

	@Autowired
	protected OrganizationalUnitManagement organizationalUnitManagement;

	@Autowired
	protected GroupPropertyEditor groupPropertyEditor;

	@Autowired
	protected GroupPermissionChecker groupPermissionChecker;

	@Autowired
	protected InternalRoleScopeManagement internalRoleScopeManagement;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Group.class, groupPropertyEditor);
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=updateOrgUnit")
	public void handleUpdateOrgUnitAction(
			@ModelAttribute(COMMAND) @Valid final OrgUnit orgUnit, final Errors errors,
			@RequestParam(value = "confirmed", required = false) final String confirmed,
			@RequestParam(value = "denied", required = false) final String denied,
			final Locale locale, final ActionRequest request, final ModelMap model) {

		Client client = SecurityContextHolder.getClient();

		logger.info("handleUpdateOrgUnitAction() entered, orgUnitId=" + orgUnit.getId()
				+ " clientId=" + client.getId() + ", name=" + orgUnit.getName());

		/*
		 * return if any errors exists
		 */
		if (errors.hasErrors()) {
			return;
		}

		/*
		 * if the name of the OrgUnit should be changed, ask for confirmation
		 */
		try {
			OrgUnit preUpdateOrgUnit = organizationalUnitManagement.getOrgUnit(client.getName(),
					orgUnit.getId());

			if (!preUpdateOrgUnit.getName().equals(orgUnit.getName())) {

				/*
				 * show confirmation request
				 */
				if (confirmed == null && denied == null) {
					request.setAttribute("confirmation", "UpdateOrgUnit");
					return;
				}
				/*
				 * remove confirmation request if it was denied and reset the form
				 */
				else if (denied != null) {
					request.removeAttribute("confirmation");

					model.remove(COMMAND);
					model.addAttribute(COMMAND, preUpdateOrgUnit);
					return;
				}

			}
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.orgunit.edit");
		}

		groupPermissionChecker.checkUserGroupPermission(orgUnit.getGroup());

		/*
		 * update the OrgUnit
		 */
		try {
			if (orgUnit.getGroup() == null) {
				orgUnit.setGroup(organizationalUnitManagement.getGroupByOrgUnitId(client
						.getClientName(), orgUnit.getId()));
			}

			orgUnit.getOrgUnitMap().setOrgUnit(orgUnit);

			organizationalUnitManagement.setOrgUnit(SecurityContextHolder.getClientName(), orgUnit);
			request.setAttribute("msg_success", resources.getMessage("success.orgunit.edit", null,
					locale));
		} catch (BackendException e) {
			/*
			 * show error message if the there already is a OrgUnit with the new name
			 */
			if (e.getErrorCode().equals(IllegalRequestException.CODE_INVALID_DUPLICATE)) {
				errors.rejectValue("name", "error.orgunit.existed");
			} else {
				logger.warn(e);
				errors.reject("error.orgunit.edit");
			}
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=addKey")
	public void handleAddKeyAction(ActionRequest actionRequest,
			@RequestParam("key") final String key, @RequestParam("value") final String value,
			@ModelAttribute(COMMAND) OrgUnit orgUnit, Errors errors) {
		logger.info("handleAddKeyAction() entered, orgUnitId=" + orgUnit.getId() + ", key=" + key
				+ ", value=" + value);

		/*
		 * check if key is already in use, empty or contains illegal characters and return in such a
		 * case
		 */
		if (orgUnit.getOrgUnitMap().containsKey(key))
			errors.reject("error.orgunit.existedkey");
		if (key.isEmpty())
			errors.reject("error.orgunit.emptykey");
		if (!key.matches("[\\p{L}\\-+#_.:,\"'`´0-9\\(\\)\\[\\]{}\\$&§\\\\/<> ]*"))
			errors.reject("error.orgunit.invalidkey");
		if (key.length() > 100)
			errors.reject("error.orgunit.keysize");
		if (value.length() > 100)
			errors.reject("error.orgunit.valuesize");
		if (!value.matches("[\\p{L}\\-+#_.:,\"'`´0-9\\(\\)\\[\\]{}\\$&§\\\\/<> ]*"))
			errors.reject("error.orgunit.invalidvalue");

		if (errors.hasErrors())
			return;

		/*
		 * store validated data
		 */
		try {
			OrgUnit orgUnitPersistent = organizationalUnitManagement.getOrgUnit(
					SecurityContextHolder.getClientName(), orgUnit.getId());
			groupPermissionChecker.checkUserGroupPermission(orgUnitPersistent.getGroup());

			organizationalUnitManagement.addValueToOrgUnit(SecurityContextHolder.getClientName(),
					orgUnit.getId(), key, value);
			orgUnit.getOrgUnitMap().put(key, value);
			actionRequest.setAttribute("msg_success", resources.getMessage(
					"success.orgunit.addkey", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.orgunit.addkey");
		}
	}


	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = { "action=removeKey", "!confirmed", "!denied" })
	public void handleUnconfirmedRemoveKeyAction(@RequestParam("key") final String key,
			final ActionRequest request) {
		logger.info("handleUnconfirmedRemoveKeyAction() entered," +
				"key=" + key);

		/*
		 * ask for confirmation and return
		 */
		request.setAttribute("keyToRemove", key);
		request.setAttribute("confirmation", "RemoveKey");
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = { "action=removeKey", "denied" })
	public void handleDeniedRemoveKeyAction(final ActionRequest request) {
		logger.info("handleDeniedRemoveKeyAction() entered");

		/*
		 * remove confirmation request if confirmation was denied
		 */
		request.removeAttribute("keyToRemove");
		request.removeAttribute("confirmation");
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = { "action=removeKey", "confirmed" })
	public void handleConfirmedRemoveKeyAction(@ModelAttribute(COMMAND) final OrgUnit orgUnit,
			final Errors errors, @RequestParam("key") final String key,
			final ActionRequest request, final Locale local) {
		logger.info("handleConfirmedRemoveKeyAction() entered, orgUnitId=" + orgUnit.getId() + ", " +
				"key=" + key);
		/*
		 * remove the key
		 */
		try {
			OrgUnit orgUnitPersistent = organizationalUnitManagement.getOrgUnit(
					SecurityContextHolder.getClientName(), orgUnit.getId());
			groupPermissionChecker.checkUserGroupPermission(orgUnitPersistent.getGroup());

			organizationalUnitManagement.removeValueFromOrgUnit(SecurityContextHolder
					.getClientName(), orgUnit.getId(), key);
			orgUnit.getOrgUnitMap().remove(key);
			request.setAttribute("msg_success", resources.getMessage(
					"success.orgunit.removekey", null, local));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.orgunit.removekey");
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping
	public String handleRenderRequest(
			@RequestParam(value = "orgUnitId", required = false) Long orgUnitId,
			@RequestParam(value = "sortOrder", defaultValue = "key.ASC") final String sortOrder,
			@RequestParam(value = "page", defaultValue = "1") Integer page, RenderRequest request,
			RenderResponse response, ModelMap model) {
		logger.info("handleRenderRequest() entered, orgUnitId = " + orgUnitId);

		model.addAllAttributes(referenceData(orgUnitId, page, sortOrder, response, model));
		model.addAttribute("ctx", "OrganizationDetailManagement");
		model.addAttribute("navPoints", AdminNavPoints.getAll());

		return "OrganizationDetailManagement";
	}

	private Map<String, Object> referenceData(Long orgUnitId, Integer page, String sortOrder,
			RenderResponse response, ModelMap model) {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("clientname", SecurityContextHolder.getClientName());

		/*
		 * prepare the query descriptor
		 */
		QueryDescriptor queryDescriptor = new QueryDescriptor();

		/*
		 * add order
		 */
		String orderField = sortOrder.replaceFirst("(.*)\\.(ASC|DESC)", "$1");
		BeanOrderProperty orderProperty = new BeanOrderProperty(OrgUnitMapEntry.class, orderField,
				sortOrder.endsWith("ASC"));
		queryDescriptor.addOrderProperty(orderProperty);

		/*
		 * add paging
		 */
		// queryDescriptor.setLimit(SecurityContextHolder.getPagingLimit());
		// queryDescriptor.setOffSet((page - 1) * queryDescriptor.getLimit());

		try {
			OrgUnit orgUnit = null;
			OrgUnitMapEntryQueryResult orgUnitMapEntryQueryResult = null;
			if (orgUnitId == null) {
				orgUnit = (OrgUnit) model.get(COMMAND);
				orgUnitMapEntryQueryResult = organizationalUnitManagement.getOrgUnitMapEntries(
						SecurityContextHolder.getClientName(), orgUnit.getId(), queryDescriptor);
				orgUnit.setOrgUnitMap(orgUnitMapEntryQueryResult.getOrgUnitMap());
			} else {
				orgUnitMapEntryQueryResult = organizationalUnitManagement.getOrgUnitMapEntries(
						SecurityContextHolder.getClientName(), orgUnitId, queryDescriptor);
				orgUnit = orgUnitMapEntryQueryResult.getOrgUnit();
			}

			/*
			 * correct page number (might be greater than last page due to searching)
			 */
			page = orgUnitMapEntryQueryResult.getPageNumber();

			/*
			 * generate base URL for use in tag libs
			 */
			PortletURL pageUrl = response.createRenderURL();
			pageUrl.setParameter("ctx", "OrganizationDetailManagement");
			pageUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			pageUrl.setParameter("sortOrder", sortOrder);
			pageUrl.setParameter("orgUnitId", Long.toString(orgUnit.getId()));
			map.put("pageUrl", pageUrl);
			PortletURL sortUrl = response.createRenderURL();
			sortUrl.setParameter("ctx", "OrganizationDetailManagement");
			sortUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			sortUrl.setParameter("page", Integer.toString(page));
			sortUrl.setParameter("orgUnitId", Long.toString(orgUnit.getId()));
			map.put("sortUrl", sortUrl);

			map.put(COMMAND, orgUnit);
			map.put("page", page);
			map.put("itemCount", orgUnitMapEntryQueryResult.getTotalSize());
			map.put("itemLimit", queryDescriptor.getLimit());
			map.put("currentSortingOrder", sortOrder);

			groupPermissionChecker.checkUserGroupPermission(orgUnit.getGroup());

			/*
			 * add groups
			 */
			InternalRoleType internalRoleType = SecurityContextHolder.getInternalRole();

			if (internalRoleType.equals(InternalRoleType.SUPER_ADMIN)
					|| internalRoleType.equals(InternalRoleType.CLIENT_ADMIN)) {
				QueryDescriptor groupsQueryDescriptor = new QueryDescriptor();
				groupsQueryDescriptor.addOrderProperty(new BeanOrderProperty(Group.class,
						GroupPropertyFields.FIELD_PATH_DISPLAYNAME, true));
				map.put("groups", groupAndRoleManagement.getGroups(
						SecurityContextHolder.getClientName(), groupsQueryDescriptor)
						.getResultList());
			}
			if (internalRoleType.equals(InternalRoleType.SECTION_ADMIN)) {
				QueryDescriptor groupsQueryDescriptor = new QueryDescriptor();
				groupsQueryDescriptor.addOrderProperty(new BeanOrderProperty(
						InternalRoleScope.class,
						InternalRoleScopePropertyFields.FIELD_PATH_GROUP_PREFIX
								+ GroupPropertyFields.FIELD_PATH_DISPLAYNAME, true));
				List<Group> groups = internalRoleScopeManagement.getInternalRoleScopesForAccount(
						SecurityContextHolder.getUUID(), InternalRoleType.SECTION_ADMIN,
						groupsQueryDescriptor);

				map.put("groups", groups);
			}

		} catch (Exception e) {
			logger.warn(e);
		}

		return map;
	}

}
