/**
 * 
 */
package org.evolvis.idm.administration.command;

import java.io.Serializable;

import javax.validation.constraints.Size;

import org.evolvis.idm.common.validation.SearchQuery;

/**
 * This is a general command class for searching in any administration data base
 * table.
 * 
 * @author Michael Kutz, tarent GmbH
 */
public class SearchCom implements Serializable {

	private static final long serialVersionUID = -148454391599596294L;
	
	@Size(max = 100) // 100 characters at most
	@SearchQuery
	private String query;
	
	/**
	 * 
	 */
	public SearchCom() {
		this.query = "";
	}
	
	public SearchCom(String query) {
		this.query = query;
	}

	/**
	 * @return the query string
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * @param query the query string to look for
	 */
	public void setQuery(String query) {
		this.query = query;
	}
	

}
