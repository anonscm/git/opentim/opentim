package org.evolvis.idm.administration.model;

import org.evolvis.idm.identity.privatedata.model.Attribute;

public class AttributeWrapper {

	private static final long serialVersionUID = 1092732986744775286L;
	
	/**
	 * Pattern to extract max file size and allowed file size out of additionalTypeConstaint string.
	 * First capturing group is max file size, second is allowed file extensions.
	 */
	private static final String FILECONSTRAINTPATTERN = "^([0-9]*)[ ]?((?:[0-9a-zA-Z]+[|]?)*)$";

	private Attribute attribute;

	public AttributeWrapper() {
		super();
	}
	
	public AttributeWrapper(Attribute attribute) {
		this.attribute = attribute;
	}
	
	/**
	 * @return the attribute
	 */
	public Attribute getAttribute() {
		return attribute;
	}

	/**
	 * @param attribute the attribute to set
	 */
	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public void setAllowedFileExtensions(String fileExtensions) {
		String additionalTypeConstraint = attribute.getAdditionalTypeConstraint().replaceFirst(
				FILECONSTRAINTPATTERN, "$1 " + fileExtensions);
		attribute.setAdditionalTypeConstraint(additionalTypeConstraint);
	}

	public String getAllowedFileExtensions() {
		if (attribute.getAdditionalTypeConstraint() == null) return null;
		return attribute.getAdditionalTypeConstraint().replaceFirst(FILECONSTRAINTPATTERN, "$2");
	}

	public void setMaxFileSize(String maxFileSize) {
		String additionalTypeConstraint = attribute.getAdditionalTypeConstraint().replaceFirst(
				FILECONSTRAINTPATTERN, maxFileSize + " $2");
		attribute.setAdditionalTypeConstraint(additionalTypeConstraint);
	}

	public String getMaxFileSize() {
		if (attribute.getAdditionalTypeConstraint() == null) return null;
		return attribute.getAdditionalTypeConstraint().replaceFirst(FILECONSTRAINTPATTERN, "$1");
	}
}
