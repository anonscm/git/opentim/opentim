package org.evolvis.idm.administration.controller;

import java.util.Locale;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.util.SynchronizingRestClientUtil;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;

/**
 * @author Frederic Eßer
 * @author Tino Rink
 * @author Dmytro Mayster
 * 
 *         This is the default controller which is called if no context is given. Let this
 *         controller extend the controller you want to load as default context.
 * 
 */
@Controller
@RequestMapping(value = "view", params = "!ctx")
public class Default extends UserManagement {

	private final Log logger = LogFactory.getLog(getClass());

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping
	public ModelAndView handleRenderRequest(
			@RequestParam(value = "keywords", required = false) final String search,
			@RequestParam(value = "statusFilter", defaultValue = "All") final String statusFilter,
			@RequestParam(value = "sortOrder", defaultValue = "username.ASC") final String sortOrder,
			@RequestParam(value = "page", defaultValue = "1") final Integer page,
			Locale locale, RenderRequest request, RenderResponse response) {
		logger.info("handleRenderRequest() entered, request=" + request);
		
		/*
		 * synchronise local SSO configuration with ClientPropertyMap in case this did not happen
		 * before
		 */
		SynchronizingRestClientUtil.getClient(SecurityContextHolder.getClientPropertyMap());

		request.setAttribute("ctx", "UserManagement");

		return super.handleRenderRequest(search, statusFilter, sortOrder, page, locale, request,
				response);
	}
}
