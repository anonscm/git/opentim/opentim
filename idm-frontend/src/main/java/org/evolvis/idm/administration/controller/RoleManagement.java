package org.evolvis.idm.administration.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.propertyeditor.ApplicationPropertyEditor;
import org.evolvis.idm.common.propertyeditor.RolePropertyEditor;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.model.RolePropertyFields;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

/**
 * @author Frederic Eßer
 * @author Tino Rink
 * @author Dmytro Mayster
 * 
 */
@Controller
@RequestMapping(value = "view", params = "ctx=RoleManagement")
@SessionAttributes(RoleManagement.COMMAND)
public class RoleManagement {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected GroupAndRoleManagement groupAndRoleManagement;

	@Autowired
	protected ApplicationManagement applicationManagement;

	@Autowired
	protected Validator validator;

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected ApplicationPropertyEditor applicationPropertyEditor;

	@Autowired
	protected RolePropertyEditor rolePropertyEditor;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Application.class, applicationPropertyEditor);
		binder.registerCustomEditor(Role.class, rolePropertyEditor);
		binder.setAllowedFields(new String[] { "application", "name", "displayName" });
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=CreateRole")
	public void handleCreateAction(@ModelAttribute(COMMAND) @Valid Role role, Errors errors,
			ActionRequest actionRequest, ModelMap model) {
		Client client = SecurityContextHolder.getClient();
		logger.info("handleCreateAction() entered, clientId=" + client.getId() + ", name="
				+ role.getName());

		try {
			groupAndRoleManagement.getRoleByName(client.getName(), role.getApplication().getName(),
					role.getName());
			errors.reject("error.role.existed");
		} catch (BackendException e) {
			// Do nothing in case no role with the new name exists
		}

		try {
			if (errors.hasErrors()) {
				return;
			}

			groupAndRoleManagement.setRole(SecurityContextHolder.getClientName(), role);
			actionRequest.setAttribute("msg_success", resources.getMessage("success.role.create",
					null, actionRequest.getLocale()));
			// clear the form after success submit
			model.clear();
		} catch (BackendException e) {
			/*
			 * show error message if the there already is a role with the new name
			 */
			if (e.getErrorCode().equals(IllegalRequestException.CODE_INVALID_DUPLICATE)) {
				errors.rejectValue("name", "error.role.existed");
			} else {
				logger.warn(e);
				errors.reject("error.role.create");
			}
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=DeleteRole", "!confirmed", "!denied" })
	public void handleUnconfirmedDeleteAction(@RequestParam(value = "roleId") final Role role,
			final ActionRequest request) {
		logger.info("handleUnconfirmedDeleteAction() entered, roleId=" + role.getId());

		/*
		 * ask for confirmation and return
		 */
		request.setAttribute("deleteRole", role);
		request.setAttribute("confirmation", "Delete");
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=Delete", "denied" })
	public void handleConfirmedDeleteAction(final ActionRequest request) {
		logger.info("handleDeniedDeleteAction() entered");
		
		/*
		 * remove confirmation request if confirmation was denied
		 */
		request.removeAttribute("deleteRole");
		request.removeAttribute("confirmation");
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=Delete", "confirmed" })
	public void handleConfirmedDeleteAction(@RequestParam(value = "roleId") final Role role,
			final ActionRequest request, final Locale locale) {
		logger.info("handleConfirmedDeleteAction() entered, roleId=" + role.getId());

		/*
		 * delete the Role
		 */
		try {
			groupAndRoleManagement.deleteRole(SecurityContextHolder.getClientName(), role.getId());
			request.setAttribute("msg_success", resources.getMessage("success.role.delete", null,
					locale));
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_errors", resources.getMessage("error.role.delete", null,
					locale));
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping
	public ModelAndView handleRenderRequest(
			@RequestParam(value = "keywords", required = false) final String search,
			@RequestParam(value = "sortOrder", defaultValue = "displayName.ASC") final String sortOrder,
			@RequestParam(value = "page", defaultValue = "1") final Integer page,
			final RenderRequest request, final RenderResponse response) {
		logger.info("handleRenderRequest() entered, request=" + request);

		ModelAndView modelAndView = new ModelAndView("RoleManagement", COMMAND, new Role());
		modelAndView.addAllObjects(referenceData(search, sortOrder, request, response, page));
		modelAndView.addObject("navPoints", AdminNavPoints.getAll());

		return modelAndView;
	}

	private Map<String, Object> referenceData(String search, String sortOrder,
			RenderRequest renderRequest, RenderResponse renderResponse, Integer page) {
		Map<String, Object> map = new HashMap<String, Object>();

		/*
		 * remove illegal characters from search and set warning
		 */
		if (search != null && !search.matches(DisplayName.PATTERN)) {
			search = search.replaceAll(DisplayName.PATTERN_INVERSE_ONECHAR, "");
			renderRequest.setAttribute("msg_warning", resources.getMessage(
					"warning.search.removedillegalchars", null, renderResponse.getLocale()));
		}

		String clientName = SecurityContextHolder.getClientName();
		map.put("clientname", clientName);
		map.put("keywords", search);

		/*
		 * prepare the query descriptor
		 */
		QueryDescriptor queryDescriptor = new QueryDescriptor();

		/*
		 * add order
		 */
		String orderField = sortOrder.replaceFirst("(.*)\\.(ASC|DESC)", "$1");
		BeanOrderProperty orderProperty = new BeanOrderProperty(Role.class, orderField, sortOrder
				.endsWith("ASC"));
		queryDescriptor.addOrderProperty(orderProperty);

		/*
		 * add paging
		 */
		queryDescriptor.setLimit(SecurityContextHolder.getPagingLimit());
		queryDescriptor.setOffSet((page - 1) * queryDescriptor.getLimit());

		/*
		 * add search
		 */
		if (search != null) {
			BeanSearchProperty searchPropertyName = new BeanSearchProperty(Role.class,
					RolePropertyFields.FIELD_PATH_NAME, search);
			queryDescriptor.addSearchProperty(searchPropertyName);

			BeanSearchProperty searchPropertyDisplayName = new BeanSearchProperty(Role.class,
					RolePropertyFields.FIELD_PATH_DISPLAYNAME, search);
			queryDescriptor.addSearchProperty(searchPropertyDisplayName);
		}

		try {
			QueryResult<Role> roleResults = groupAndRoleManagement.getRolesByClient(clientName,
					queryDescriptor);

			/*
			 * correct page number (might be greater than last page due to searching)
			 */
			page = roleResults.getPageNumber();

			/*
			 * generate base URL for use in tag libs
			 */
			PortletURL pageUrl = renderResponse.createRenderURL();
			pageUrl.setParameter("ctx", "RoleManagement");
			pageUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			pageUrl.setParameter("sortOrder", sortOrder);
			pageUrl.setParameter("keywords", search);
			map.put("pageUrl", pageUrl);
			PortletURL sortUrl = renderResponse.createRenderURL();
			sortUrl.setParameter("ctx", "RoleManagement");
			sortUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			sortUrl.setParameter("page", Integer.toString(page));
			sortUrl.setParameter("keywords", search);
			map.put("sortUrl", sortUrl);

			map.put("roles", roleResults.getResultList());
			map.put("page", page);
			map.put("itemCount", roleResults.getTotalSize());
			map.put("itemLimit", queryDescriptor.getLimit());
			map.put("currentSortingOrder", sortOrder);

			List<Application> applications = applicationManagement
					.getApplications(clientName, null).getResultList();
			map.put("applications", applications);

			/*
			 * set message for search
			 */
			if (search != null && !"".equals(search)) {
				renderRequest.setAttribute("msg_searchstatus", resources.getMessage(
						"success.search", new Object[] { search, roleResults.getTotalSize() },
						renderResponse.getLocale()));
			}
		} catch (Exception e) {
			logger.warn(e);
		}

		return map;
	}
}