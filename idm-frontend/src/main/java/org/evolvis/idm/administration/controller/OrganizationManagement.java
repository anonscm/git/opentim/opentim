package org.evolvis.idm.administration.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.administration.util.GroupPermissionChecker;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.model.ValueType;
import org.evolvis.idm.common.propertyeditor.GroupPropertyEditor;
import org.evolvis.idm.common.propertyeditor.OrgUnitPropertyEditor;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.organization.model.OrgUnitSearchableFields;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.GroupSearchableFields;
import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

/**
 * @author Tino Rink
 * @author Dmytro Mayster
 */

@Controller
@RequestMapping(value = "view", params = "ctx=OrganizationManagement")
@SessionAttributes(OrganizationManagement.COMMAND)
public class OrganizationManagement {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected Validator validator;

	@Autowired
	protected GroupAndRoleManagement groupAndRoleManagement;

	@Autowired
	protected OrganizationalUnitManagement organizationalUnitManagement;

	@Autowired
	protected InternalRoleScopeManagement internalRoleScopeManagement;

	@Autowired
	protected GroupPropertyEditor groupPropertyEditor;

	@Autowired
	protected OrgUnitPropertyEditor orgUnitPropertyEditor;

	@Autowired
	protected GroupPermissionChecker groupPermissionChecker;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Group.class, groupPropertyEditor);
		binder.registerCustomEditor(OrgUnit.class, orgUnitPropertyEditor);
		binder.setAllowedFields(new String[] { "name", "group", "displayName" });
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=CreateOrgUnit")
	public void handleAddAction(ActionRequest actionRequest,
			@ModelAttribute(COMMAND) @Valid OrgUnit orgUnit, Errors errors, ModelMap model) {
		logger.info("handleAddAction() entered, groupId=" + orgUnit.getGroup().getId()
				+ ", orgunitname=" + orgUnit.getName());

		groupPermissionChecker.checkUserGroupPermission(orgUnit.getGroup());

		if (errors.hasErrors()) {
			return;
		}

		try {
			organizationalUnitManagement.setOrgUnit(SecurityContextHolder.getClientName(), orgUnit);
			actionRequest.setAttribute("msg_success", resources.getMessage(
					"success.orgunit.create", null, actionRequest.getLocale()));
			// clear the form after success submit
			model.clear();
		} catch (BackendException e) {
			/*
			 * show error message if the there already is a orgunit with the new name
			 */
			if (e.getErrorCode().equals(IllegalRequestException.CODE_INVALID_DUPLICATE)) {
				errors.rejectValue("name", "error.orgunit.existed");
			} else {
				logger.warn(e);
				errors.reject("error.orgunit.create");
			}
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = { "action=Delete", "!confirmed", "!denied" })
	public void handleUnconfirmedDeleteAction(
			@RequestParam(value = "orgUnitId") final OrgUnit orgUnit, final ActionRequest request) {
		logger.info("handleUnconfirmedDeleteAction() entered, orgUnitId=" + orgUnit.getId());

		/*
		 * ask for confirmation and return
		 */
		request.setAttribute("deleteOrgUnit", orgUnit);
		request.setAttribute("confirmation", "Delete");
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = { "action=Delete", "denied" })
	public void handleDeniedDeleteAction(final ActionRequest request) {
		logger.info("handleDeniedDeleteAction() entered");
		
		/*
		 * remove confirmation request if confirmation was denied
		 */
		request.removeAttribute("deleteOrgUnit");
		request.removeAttribute("confirmation");
	}
	
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = { "action=Delete", "confirmed" })
	public void handleConfirmedDeleteAction(
			@RequestParam(value = "orgUnitId") final OrgUnit orgUnit,
			final ActionRequest request, final Locale locale) {
		logger.info("handleDeniedDeleteAction() entered");
		
		/*
		 * delete the OrgUnit
		 */
		try {
			groupPermissionChecker.checkUserGroupPermission(orgUnit.getGroup());
			organizationalUnitManagement.deleteOrgUnit(SecurityContextHolder.getClientName(),
					orgUnit);
			request.setAttribute("msg_success", resources.getMessage("success.orgunit.delete",
					null, locale));
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_error", resources.getMessage("error.orgunit.delete", null,
					locale));
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping
	public ModelAndView handleRenderRequest(
			@RequestParam(value = "keywords", required = false) final String search,
			@RequestParam(value = "sortOrder", defaultValue = "displayName.ASC") final String sortOrder,
			@RequestParam(value = "page", defaultValue = "1") Integer page,
			final RenderRequest renderRequest, final RenderResponse renderResponse) {
		logger.info("handleRenderRequest() entered, request=" + renderRequest);

		ModelAndView modelAndView = new ModelAndView("OrganizationManagement", COMMAND,
				new OrgUnit());
		modelAndView.addAllObjects(referenceData(search, sortOrder, renderRequest, renderResponse,
				page));
		modelAndView.addObject("navPoints", AdminNavPoints.getAll());

		return modelAndView;
	}

	private Map<String, Object> referenceData(String search, String sortOrder,
			RenderRequest request, RenderResponse response, Integer page) {
		Map<String, Object> map = new HashMap<String, Object>();

		/*
		 * remove illegal characters from search and set warning
		 */
		if (search != null && !search.matches(DisplayName.PATTERN)) {
			search = search.replaceAll(DisplayName.PATTERN_INVERSE_ONECHAR, "");
			request.setAttribute("msg_warning", resources.getMessage(
					"warning.search.removedillegalchars", null, response.getLocale()));
		}

		map.put("keywords", search);

		/*
		 * prepare the query descriptor
		 */
		QueryDescriptor queryDescriptor = new QueryDescriptor();

		/*
		 * add order
		 */
		String orderField = sortOrder.replaceFirst("(.*)\\.(ASC|DESC)", "$1");
		BeanOrderProperty orderProperty = new BeanOrderProperty(OrgUnit.class, orderField,
				sortOrder.endsWith("ASC"));
		queryDescriptor.addOrderProperty(orderProperty);

		/*
		 * add paging
		 */
		queryDescriptor.setLimit(SecurityContextHolder.getPagingLimit());
		queryDescriptor.setOffSet((page - 1) * queryDescriptor.getLimit());

		/*
		 * add search
		 */
		if (search != null) {
			BeanSearchProperty searchPropertyName = new BeanSearchProperty(OrgUnit.class,
					OrgUnitSearchableFields.FIELD_PATH_NAME, search);
			queryDescriptor.addSearchProperty(searchPropertyName);

			BeanSearchProperty searchPropertyDisplayName = new BeanSearchProperty(OrgUnit.class,
					OrgUnitSearchableFields.FIELD_PATH_DISPLAYNAME, search);
			queryDescriptor.addSearchProperty(searchPropertyDisplayName);
		}

		try {
			String clientName = SecurityContextHolder.getClientName();
			map.put("clientname", clientName);

			InternalRoleType internalRoleType = SecurityContextHolder.getInternalRole();

			if (internalRoleType.equals(InternalRoleType.SUPER_ADMIN)
					|| internalRoleType.equals(InternalRoleType.CLIENT_ADMIN)) {
				map.put("groups", groupAndRoleManagement.getGroups(clientName, null)
						.getResultList());
			}
			if (internalRoleType.equals(InternalRoleType.SECTION_ADMIN)) {
				List<Group> groups = internalRoleScopeManagement.getInternalRoleScopesForAccount(
						SecurityContextHolder.getUUID(), InternalRoleType.SECTION_ADMIN, null);
				map.put("groups", groups);

				for (Group group : groups) {
					BeanSearchProperty searchPropertyGroupId = new BeanSearchProperty(
							OrgUnit.class, OrgUnitSearchableFields.FIELD_PATH_GROUP_PREFIX
									+ GroupSearchableFields.FIELD_PATH_DISPLAYNAME,
							group.getId().toString(), ValueType.LONG);
					queryDescriptor.addSearchProperty(searchPropertyGroupId);
				}
			}

			QueryResult<OrgUnit> organizationResults = organizationalUnitManagement.getOrgUnits(
					clientName, queryDescriptor);

			/*
			 * correct page number (might be greater than last page due to searching)
			 */
			page = organizationResults.getPageNumber();

			/*
			 * generate base URL for use in tag libs
			 */
			PortletURL pageUrl = response.createRenderURL();
			pageUrl.setParameter("ctx", "OrganizationManagement");
			pageUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			pageUrl.setParameter("sortOrder", sortOrder);
			pageUrl.setParameter("keywords", search);
			map.put("pageUrl", pageUrl);
			PortletURL sortUrl = response.createRenderURL();
			sortUrl.setParameter("ctx", "OrganizationManagement");
			sortUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			sortUrl.setParameter("page", Integer.toString(page));
			sortUrl.setParameter("keywords", search);
			map.put("sortUrl", sortUrl);

			map.put("orgUnits", organizationResults.getResultList());
			map.put("page", page);
			map.put("itemCount", organizationResults.getTotalSize());
			map.put("itemLimit", queryDescriptor.getLimit());
			map.put("currentSortingOrder", sortOrder);

			/*
			 * set message for search
			 */
			if (search != null && !"".equals(search)) {
				request.setAttribute("msg_searchstatus", resources.getMessage("success.search",
						new Object[] { search, organizationResults.getTotalSize() }, response
								.getLocale()));
			}
		} catch (Exception e) {
			logger.warn(e);
		}

		return map;
	}
}