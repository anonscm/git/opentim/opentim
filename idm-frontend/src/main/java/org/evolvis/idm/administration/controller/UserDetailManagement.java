package org.evolvis.idm.administration.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.administration.util.GroupPermissionChecker;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.NavPoint;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.GroupPropertyFields;
import org.evolvis.idm.relation.permission.model.InternalRoleScope;
import org.evolvis.idm.relation.permission.model.InternalRoleScopePropertyFields;
import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.model.RolePropertyFields;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement;
import org.evolvis.idm.security.SpringSecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * Manages user's details and internal scope.
 * 
 * @author Tino Rink
 * @author Dmytro Mayster
 * 
 */
@Controller
@RequestMapping(value = "view", params = { "ctx=UserManagement", "subctx=UserDetail" })
@SessionAttributes(UserDetailManagement.COMMAND)
public class UserDetailManagement {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected UserManagement userManagement;

	@Autowired
	protected InternalRoleManagement internalRoleManagement;

	@Autowired
	protected InternalRoleScopeManagement internalRoleScopeManagement;

	@Autowired
	protected GroupAndRoleManagement groupAndRoleManagement;

	@Autowired
	protected ApplicationManagement applicationManagement;

	@Autowired
	protected Validator validator;
	
	@Autowired
	protected GroupPermissionChecker groupPermissionChecker;

	/**
	 * Handles the <code>editUserAccount</code> action request by invoking the
	 * <code>setUserNames</code> of injected {@link UserManagement}.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param user
	 *            SpringSecurityUser object with the input data
	 * @param errors
	 *            Errors object to collect from errors
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=editUserAccount")
	public void handleEditUserAccount(@ModelAttribute(COMMAND) @Valid User user, Errors errors, ActionRequest actionRequest) {
		String uuid = user.getUuid();
		logger.info("handleEditUserAccount() entered, uuid=" + uuid);

		// return if errors exists
		if (errors.hasErrors()) {
			return;
		}

		try {
			userManagement.setUserNames(uuid, user.getFirstname(), user.getLastname());
			actionRequest.setAttribute("msg_success", resources.getMessage("success.userdetail.edit", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.userdetail.edit");
		}
	}

	/**
	 * Handles the <code>ChangeInternalRole</code> action request by invoking a
	 * <code>addUserToInternalRole</code> or
	 * <code>removeUserFromInternalRole</code> of injected
	 * {@link InternalRoleManagement}.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param internalRole
	 *            The new internal role that is assigned
	 * @param uuid
	 *            The uuid of user who gets a new role
	 * @param user
	 *            SpringSecurityUser object with the input data
	 * @param errors
	 *            Errors object to collect from errors
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=ChangeInternalRole")
	public void handleChangeInternalRolel(ActionRequest actionRequest, @RequestParam("internalRole") final InternalRoleType internalRole, @RequestParam("uuid") final String uuid, @ModelAttribute(COMMAND) User user, Errors errors) {
		logger.info("handleChangeInternalRolel() entered, uuid=" + uuid);
		SpringSecurityUser editor = (SpringSecurityUser) org.springframework.security.context.SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		try {
			InternalRoleType editorInternalRole = editor.getInternalRole();
			InternalRoleType currentInternalRole = internalRoleManagement.getInternalRoleForUser(uuid);

			// The editor must be at least the same role as the user
			// and he must not assign the higher role than he has himself.
			if (internalRole.compareTo(editorInternalRole) < 0 || currentInternalRole.compareTo(editorInternalRole) < 0) {
				errors.reject("error.internalrole.toohigh");
				return;
			}

			// assign the higher role
			if (internalRole.compareTo(currentInternalRole) < 0) {
				internalRoleManagement.addUserToInternalRole(uuid, internalRole);
			} else {
				// assign the lower role and remove the internal scopes
				while (internalRole.compareTo(currentInternalRole) > 0) {
					if (editorInternalRole.equals(InternalRoleType.SECTION_ADMIN)) {
						List<Group> editorInternalScopes = internalRoleScopeManagement.getInternalRoleScopesForAccount(editor.getUsername(), InternalRoleType.SECTION_ADMIN, null);
						List<Group> internalUserScopes = internalRoleScopeManagement.getInternalRoleScopesForAccount(uuid, currentInternalRole, null);
						// user has groups yet
						if (internalUserScopes != null) {
							internalUserScopes.removeAll(editorInternalScopes);
							// these groups are out of the editor's internal
							// scopes
							if (!internalUserScopes.isEmpty()) {
								errors.reject("error.internalrole.change.outofscope");
								return;
							}
						}
					}
					internalRoleManagement.removeUserFromInternalRole(uuid, currentInternalRole);
					for (Group group : internalRoleScopeManagement.getInternalRoleScopesForAccount(uuid, currentInternalRole, null)) {
						internalRoleScopeManagement.deleteInternalRoleScope(uuid, currentInternalRole, group.getId());
					}
					currentInternalRole = internalRoleManagement.getInternalRoleForUser(uuid);
				}
			}
			actionRequest.setAttribute("msg_success", resources.getMessage("success.internalrole.change", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.internalrole.change");
		}
	}

	/**
	 * Handles the <code>AddInternalGroup</code> action request by invoking the
	 * <code>setInternalRoleScope</code> of injected
	 * {@link InternalRoleScopeManagement}.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param groupId
	 *            The id of group. A given user is added to that group.
	 * @param uuid
	 *            The uuid of user that get a given group as internal scope
	 * @param user
	 *            SpringSecurityUser object with the input data
	 * @param errors
	 *            Errors object to collect from errors
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=AddInternalGroup")
	public void handleInternalGroupAdd(final ActionRequest actionRequest, @RequestParam(value = "groupId", required = false) final Long groupId, @RequestParam("uuid") final String uuid, @ModelAttribute(COMMAND) User user, Errors errors) {
		logger.info("handleInternalGroupAdd() entered, groupId=" + groupId + ", uuid=" + uuid);

		// SpringSecurityUser did not select group
		if (groupId == null)
			return;

		try {
			groupPermissionChecker.checkUserGroupPermission(groupId);
			InternalRoleType internalRole = internalRoleManagement.getInternalRoleForUser(uuid);
			internalRoleScopeManagement.setInternalRoleScope(uuid, internalRole, groupId);
			actionRequest.setAttribute("msg_success", resources.getMessage("success.internalgroup.add", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.internalgroup.add");
		}
	}

	/**
	 * Handles the <code>DeleteInternalGroup</code> action request by invoking
	 * the <code>deleteInternalRoleScope</code> of injected
	 * {@link InternalRoleScopeManagement}.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param groupId
	 *            The id of group. A given user is deleted from that group
	 *            scope.
	 * @param uuid
	 *            The uuid of user that is deleted from a given internal scope
	 * @param user
	 *            SpringSecurityUser object with the input data
	 * @param errors
	 *            Errors object to collect from errors
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=DeleteInternalGroup")
	public void handleInternalGroupDelete(final ActionRequest actionRequest, @RequestParam("groupId") final Long groupId, @RequestParam("uuid") final String uuid, @ModelAttribute(COMMAND) User user, Errors errors) {
		logger.info("handleInternalGroupDelete() entered, groupId=" + groupId + ", uuid=" + uuid);

		try {
			groupPermissionChecker.checkUserGroupPermission(groupId);
			InternalRoleType internalRole = internalRoleManagement.getInternalRoleForUser(uuid);
			internalRoleScopeManagement.deleteInternalRoleScope(uuid, internalRole, groupId);
			actionRequest.setAttribute("msg_success", resources.getMessage("success.internalgroup.delete", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.internalgroup.delete");
		}
	}

	/**
	 * Handles the <code>DeleteRole</code> action request by invoking the
	 * <code>deleteUserFromRole</code> of injected
	 * {@link GroupAndRoleManagement}.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param roleId
	 *            The id of role. A given user is deleted from that role.
	 * @param uuid
	 *            The uuid of user that is deleted from a given role
	 * @param user
	 *            SpringSecurityUser object with the input data
	 * @param errors
	 *            Errors object to collect from errors
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=DeleteRole")
	public void handleRoleDelete(final ActionRequest actionRequest, @RequestParam("roleId") final Long roleId, @RequestParam("uuid") final String uuid, @ModelAttribute(COMMAND) User user, Errors errors) {
		logger.info("handleRoleDelete() entered, roleId=" + roleId + ", uuid=" + uuid);

		try {
			groupAndRoleManagement.deleteUserFromRole(uuid, roleId, "default");
			actionRequest.setAttribute("msg_success", resources.getMessage("success.role.delete", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.role.delete");
		}
	}

	/**
	 * Handles the <code>AddRole</code> action request by invoking the
	 * <code>addUserToRole</code> of injected {@link GroupAndRoleManagement}.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param roleId
	 *            The id of role. A given user is added to that role.
	 * @param uuid
	 *            The uuid of user that is added to a given role
	 * @param user
	 *            SpringSecurityUser object with the input data
	 * @param errors
	 *            Errors object to collect from errors
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=AddRole")
	public void handleRoleAdd(final ActionRequest actionRequest, @RequestParam(value = "roleId", required = false) final Long roleId, @RequestParam("uuid") final String uuid, @ModelAttribute(COMMAND) User user, Errors errors) {
		logger.info("handleRoleAdd() entered, roleId=" + roleId + ", uuid=" + uuid);

		// SpringSecurityUser did not select role
		if (roleId == null)
			return;

		try {
			groupAndRoleManagement.addUserToRole(uuid, roleId, "default");
			actionRequest.setAttribute("msg_success", resources.getMessage("success.role.add", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.role.add");
		}
	}

	/**
	 * Handles the <code>DeleteGroup</code> action request by invoking the
	 * <code>deleteUserFromGroup</code> of injected
	 * {@link GroupAndRoleManagement}.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param groupId
	 *            The id of group. A given user is deleted from that group.
	 * @param uuid
	 *            The uuid of user that is deleted from a given group
	 * @param user
	 *            SpringSecurityUser object with the input data
	 * @param errors
	 *            Errors object to collect from errors
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=DeleteGroup")
	public void handleGroupDelete(final ActionRequest actionRequest, @RequestParam("groupId") final Long groupId, @RequestParam("uuid") final String uuid, @ModelAttribute(COMMAND) User user, Errors errors) {
		logger.info("handleGroupDelete() entered, groupId=" + groupId + ", uuid=" + uuid);

		try {
			groupPermissionChecker.checkUserGroupPermission(groupId);
			groupAndRoleManagement.deleteUserFromGroup(uuid, groupId);
			actionRequest.setAttribute("msg_success", resources.getMessage("success.group.delete", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.group.delete");
		}
	}

	/**
	 * Handles the <code>AddGroup</code> action request by invoking the
	 * <code>addUserToGroup</code> of injected {@link GroupAndRoleManagement}.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param groupId
	 *            The id of group. A given user is added to that group.
	 * @param uuid
	 *            The uuid of user that is added to a given group
	 * @param user
	 *            SpringSecurityUser object with the input data
	 * @param errors
	 *            Errors object to collect from errors
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=AddGroup")
	public void handleGroupAdd(final ActionRequest actionRequest, @RequestParam(value = "groupId", required = false) final Long groupId, @RequestParam("uuid") final String uuid, @ModelAttribute(COMMAND) User user, Errors errors) {
		logger.info("handleGroupAdd() entered, groupId=" + groupId + ", uuid=" + uuid);

		// SpringSecurityUser did not select group
		if (groupId == null)
			return;

		try {
			groupPermissionChecker.checkUserGroupPermission(groupId);
			
			for(Group g : groupAndRoleManagement.getGroupsByUser(uuid))
			{
				if(groupId.equals(g.getId()))
				{
					errors.reject("error.group.alreadyadded");
				}
			}
			
			groupAndRoleManagement.addUserToGroup(uuid, groupId);
			actionRequest.setAttribute("msg_success", resources.getMessage("success.group.add", null, actionRequest.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.group.add");
		}
	}

	/**
	 * Handles the render request and returns a {@link ModelAndView} object with
	 * a UserManagement view. The model of the {@link ModelAndView} contains
	 * following objects: <br/>
	 * <br/>
	 * <i>{@link SpringSecurityUser}</i> A command object stored under the
	 * <code>command</code> name. <br/>
	 * <i>clientname</i> The name of a current client. <br/>
	 * <i>navPoints</i> List of all {@link NavPoint}.
	 * 
	 * @param request
	 *            RenderRequest object
	 * @return {@link ModelAndView} object
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping
	public ModelAndView handleRenderRequest(@RequestParam("uuid") final String uuid, Model model) {
		logger.info("handleRenderRequest() entered, uuid=" + uuid);

		User user = formBackingObject(uuid);
		ModelAndView modelAndView = new ModelAndView("UserDetailManagement", COMMAND, user);
		modelAndView.addAllObjects(referenceData(uuid));
		modelAndView.addObject("navPoints", AdminNavPoints.getAll());

		return modelAndView;
	}

	private User formBackingObject(String uuid) {
		User user = null;
		try {
			user = userManagement.getUserByUuid(uuid);
		} catch (Exception e) {
			logger.warn(e);
		}

		return user;
	}

	private Map<String, Object> referenceData(String uuid) {
		Map<String, Object> map = new HashMap<String, Object>();
		String clientName = org.evolvis.idm.security.SecurityContextHolder.getClientName();
		SpringSecurityUser editor = (SpringSecurityUser) org.springframework.security.context.SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		map.put("clientname", clientName);

		try {
			InternalRoleType editorInternalRole = editor.getInternalRole();

			InternalRoleType targetInternalRole = internalRoleManagement.getInternalRoleForUser(uuid);
			map.put("all_internal_roles", InternalRoleType.values());
			map.put("target_internal_role", targetInternalRole);

			QueryDescriptor groupsQueryDescriptor = new QueryDescriptor();
			groupsQueryDescriptor.addOrderProperty(new BeanOrderProperty(Group.class, GroupPropertyFields.FIELD_PATH_DISPLAYNAME, true));
			List<Group> allGroups = groupAndRoleManagement.getGroups(clientName, groupsQueryDescriptor).getResultList();

			List<Group> targetAssignedGroups = groupAndRoleManagement.getGroupsByUser(uuid);
			/*
			 * sort all groups by display name
			 */
			Comparator<Group> groupsComparator = new Comparator<Group>() {
				@Override
				public int compare(Group g1, Group g2) {
					return String.CASE_INSENSITIVE_ORDER.compare(g1.getDisplayName(), g2.getDisplayName());
				}
			};
			Collections.sort(targetAssignedGroups, groupsComparator);
			map.put("target_assigned_groups", targetAssignedGroups);

			List<Group> editorGroupsWithAdminRights = new ArrayList<Group>();
			// list of groups which the editor may assign to a section admin
			// groups which are already assigned to the section admin will be
			// removed for selection convenience
			List<Group> targetAssignableGroupsForAdministration = new ArrayList<Group>();
			
			QueryDescriptor internalRoleScopesQueryDescriptor = new QueryDescriptor();
			internalRoleScopesQueryDescriptor.addOrderProperty(new BeanOrderProperty(InternalRoleScope.class, InternalRoleScopePropertyFields.FIELD_PATH_GROUP_PREFIX+GroupPropertyFields.FIELD_PATH_DISPLAYNAME, true));
			List<Group> targetGroupsWithAdminRights = internalRoleScopeManagement.getInternalRoleScopesForAccount(uuid, targetInternalRole, internalRoleScopesQueryDescriptor);
			map.put("target_groups_with_admin_rights", targetGroupsWithAdminRights);

			// add only groups with admin rights for section admin
			if (editorInternalRole.equals(InternalRoleType.SECTION_ADMIN)) {
				// retrieve editors groups with admin rights
				editorGroupsWithAdminRights = internalRoleScopeManagement.getInternalRoleScopesForAccount(editor.getUuid(), InternalRoleType.SECTION_ADMIN, internalRoleScopesQueryDescriptor);
				// add these groups to list of assignable groups
				targetAssignableGroupsForAdministration.addAll(editorGroupsWithAdminRights);

				// add all groups for super and client admin
			} else {
				// super and client admin may grant administrative rights for
				// all groups
				editorGroupsWithAdminRights.addAll(allGroups);
				targetAssignableGroupsForAdministration.addAll(allGroups);
			}

			// remove groups which are already assign as administrative groups
			targetAssignableGroupsForAdministration.removeAll(internalRoleScopeManagement.getInternalRoleScopesForAccount(uuid, InternalRoleType.SECTION_ADMIN, null));

			// put determined lists to jsp map
			map.put("editor_groups_with_admin_rights", editorGroupsWithAdminRights);
			map.put("target_assignable_groups_for_administration", targetAssignableGroupsForAdministration);

			// TODO: in den BackEnd verlagern:
			List<Application> applications = applicationManagement.getApplications(clientName, null).getResultList();
			List<Role> userRoles = new ArrayList<Role>();
			for (Application app : applications) {
				List<Role> rolesByUserAndApp = groupAndRoleManagement.getRolesByUserAndApplication(uuid, app.getId(), "default");
				for (Role roleForApp : rolesByUserAndApp)
					roleForApp.setApplication(app);
				userRoles.addAll(rolesByUserAndApp);
			}
			
			/*
			 * sort the roles by display name
			 */
			Comparator<Role> rolesComparator = new Comparator<Role>() {
				@Override
				public int compare(Role r1, Role r2) {
					return String.CASE_INSENSITIVE_ORDER.compare(r1.getDisplayName(), r2.getDisplayName());
				}
			};
			Collections.sort(userRoles, rolesComparator);

			map.put("user_roles", userRoles);

			QueryDescriptor rolesQueryDescriptor = new QueryDescriptor();
			rolesQueryDescriptor.addOrderProperty(new BeanOrderProperty(Role.class, RolePropertyFields.FIELD_PATH_DISPLAYNAME, true));
			List<Role> roles = groupAndRoleManagement.getRolesByClient(clientName, rolesQueryDescriptor).getResultList();
			roles.removeAll(userRoles);
			
			map.put("unused_roles", roles);
		} catch (Exception e) {
			logger.warn(e);
		}

		return map;
	}
}