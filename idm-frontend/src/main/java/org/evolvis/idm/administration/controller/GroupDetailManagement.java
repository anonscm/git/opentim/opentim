package org.evolvis.idm.administration.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.administration.util.CachedUserList;
import org.evolvis.idm.administration.util.GroupPermissionChecker;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.NavPoint;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.model.UserPropertyFields;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.model.RolePropertyFields;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.evolvis.idm.security.SpringSecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

/**
 * Handle detailed Group Management, like adding Users to a Group.
 * 
 * @author Fabian Bieker
 * @author Thomas Schmitz
 * @author Dmytro Mayster
 */
@Controller
@RequestMapping(value = "view", params = "ctx=GroupDetailManagement")
@SessionAttributes(GroupDetailManagement.COMMAND)
public class GroupDetailManagement {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected CachedUserList cachedUserList;

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected GroupPermissionChecker groupPermissionChecker;

	@Autowired
	protected GroupAndRoleManagement groupAndRoleManagement;

	@Autowired
	protected Validator validator;

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=updateGroup")
	public void handleUpdateGroupAction(
			@ModelAttribute(COMMAND) @Valid final Group group, Errors errors,
			@RequestParam(value = "confirmed", required = false) final String confirmed,
			@RequestParam(value = "denied", required = false) final String denied,
			final ModelMap model, final ActionRequest request, final Locale locale) {

		Client client = SecurityContextHolder.getClient();

		logger.info("handleUpdateGroupAction() entered, groupId=" + group.getId() + " clientId="
				+ client.getId() + ", name=" + group.getName());

		/*
		 * return if any errors exists
		 */
		if (errors.hasErrors()) {
			return;
		}

		/*
		 * if the name of the Group should be changed, ask for confirmation
		 */
		try {
			Group preUpdateGroup = groupAndRoleManagement.getGroup(client.getName(), group.getId());

			if (!preUpdateGroup.getName().equals(group.getName())) {

				/*
				 * show confirmation request
				 */
				if (confirmed == null && denied == null) {
					request.setAttribute("confirmation", "UpdateGroup");
					return;
				}
				/*
				 * remove confirmation request if it was denied and reset the form
				 */
				else if (denied != null) {
					request.removeAttribute("confirmation");

					model.remove(COMMAND);
					model.addAttribute(COMMAND, preUpdateGroup);
					return;
				}
			}

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.group.edit");
		}

		/*
		 * update the Group
		 */
		try {
			groupPermissionChecker.checkUserGroupPermission(group);

			groupAndRoleManagement.setGroup(SecurityContextHolder.getClientName(), group);
			request.setAttribute("msg_success", resources.getMessage("success.group.edit",
					null, locale));
		} catch (BackendException e) {
			/*
			 * show error message if the there already is a group with the new name
			 */
			if (e.getErrorCode().equals(IllegalRequestException.CODE_INVALID_DUPLICATE)) {
				errors.rejectValue("name", "error.group.existed");
			} else {
				logger.warn(e);
				errors.reject("error.group.edit");
			}
		}
	}

	/**
	 * Handles the addUser action request by invoking the <code>addUserToGroup</code> of injected
	 * {@link GroupAndRoleManagement}.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param uuid
	 *            id of user to add
	 * @param group
	 *            Group - the command object
	 * @param errors
	 *            Errors object
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=addUser")
	public void handleAddUserAction(ActionRequest actionRequest,
			@RequestParam("userId") final String uuid, @ModelAttribute(COMMAND) final Group group,
			Errors errors) {
		logger.info("handleAddUserAction() entered, groupId=" + group.getId());

		try {
			groupPermissionChecker.checkUserGroupPermission(group);
			groupAndRoleManagement.addUserToGroup(uuid, group.getId());
			actionRequest.setAttribute("msg_success", resources.getMessage("success.user.add",
					null, actionRequest.getLocale()));

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.user.add");
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=addRole")
	public void handleAddToRoleAction(ActionRequest actionRequest,
			@RequestParam("roleId") final Long roleId, @ModelAttribute(COMMAND) final Group group,
			Errors errors) {
		logger.info("handleAddRoleAction entered, groupId=" + group.getId() + ", roleId=" + roleId);

		try {
			if (groupAndRoleManagement.isGroupInRole(SecurityContextHolder.getClientName(), group
					.getId(), roleId, "default")) {
				errors.reject("error.group.roleexists");
				return;
			}

			groupAndRoleManagement.addGroupToRole(SecurityContextHolder.getClientName(), group
					.getId(), roleId, "default");
			actionRequest.setAttribute("msg_success", resources.getMessage("success.role.add",
					null, actionRequest.getLocale()));

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.role.add");
		}
	}

	/**
	 * Handles the addUser action request by invoking the <code>deleteUserFromGroup</code> of
	 * injected {@link GroupAndRoleManagement}.
	 * 
	 * @param actionResponse
	 *            ActionResponse object
	 * @param userId
	 *            id of user to remove
	 * @param group
	 *            Group - the command object
	 * @param errors
	 *            Errors object
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=removeUser")
	public void handleRemoveUserAction(ActionRequest actionRequest,
			@RequestParam("userId") final String uuid, @ModelAttribute(COMMAND) final Group group,
			Errors errors) {
		logger.info("handleRemoveUserAction() entered, groupId=" + group.getId());

		try {
			groupPermissionChecker.checkUserGroupPermission(group);
			groupAndRoleManagement.deleteUserFromGroup(uuid, group.getId());
			actionRequest.setAttribute("msg_success", resources.getMessage("success.user.remove",
					null, actionRequest.getLocale()));

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.user.delete");
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=removeRole")
	public void handleRemoveRoleAction(ActionRequest actionRequest,
			@RequestParam("roleId") final Long roleId, @ModelAttribute(COMMAND) final Group group,
			Errors errors) {
		logger.info("handleRemoveRoleAction() entered, groupId=" + group.getId() + ", roleId="
				+ roleId);

		try {
			groupPermissionChecker.checkUserGroupPermission(group);
			groupAndRoleManagement.deleteGroupFromRole(SecurityContextHolder.getClientName(), group
					.getId(), roleId, "default");
			actionRequest.setAttribute("msg_success", resources.getMessage("success.role.delete",
					null, actionRequest.getLocale()));

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.role.delete");
		}
	}

	/**
	 * Handles the render request and returns a {@link ModelAndView} object to the
	 * GroupDetailManagement view. The ModelAndView object contains following objects:
	 * 
	 * <br/>
	 * <br/>
	 * <i>navPoints</i> List of all {@link NavPoint NavPoints}. <br/>
	 * <i>group</i> {@link Group} object of this request. <br/>
	 * <i>accounts</i> List of all {@link SpringSecurityUser UserAccounts} that are in the given
	 * group. <br/>
	 * <i>allAccounts</i> Collection of all available {@link SpringSecurityUser UserAccounts}. <br/>
	 * <i>allRoles</i> List of all {@link Role Roles} that are associated to the client of current
	 * portal user.
	 * 
	 * @param groupId
	 *            id of requested group detail view
	 * @return ModelAndView object
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse response, Model model,
			@RequestParam(value = "groupId", required = false) Long groupId) {
		logger.info("handleRenderRequest() entered, groupId =" + groupId);

		Group group;
		if (groupId != null) {
			group = formBackingObject(groupId);
			model.addAttribute(COMMAND, group);
		} else {
			group = (Group) model.asMap().get(COMMAND);
		}

		groupPermissionChecker.checkUserGroupPermission(group);

		model.addAllAttributes(referenceData(request, group));
		model.addAttribute("navPoints", AdminNavPoints.getAll());

		return "GroupDetailManagement";
	}

	private Group formBackingObject(Long groupId) {
		Group group = new Group();
		try {
			group = groupAndRoleManagement.getGroup(SecurityContextHolder.getClientName(), groupId);

		} catch (Exception e) {
			logger.warn(e);
		}

		return group;
	}

	private Map<String, Object> referenceData(RenderRequest request, Group group) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			String clientName = SecurityContextHolder.getClientName();

			map.put("clientname", clientName);

			QueryDescriptor usersQueryDescriptor = new QueryDescriptor();
			usersQueryDescriptor.addOrderProperty(new BeanOrderProperty(User.class,
					UserPropertyFields.FIELD_PATH_USERNAME, true));
			List<User> users = groupAndRoleManagement.getUserAccountsByGroupId(
					SecurityContextHolder.getClientName(), group.getId(), usersQueryDescriptor)
					.getResultList();
			map.put("accounts", users);

			QueryDescriptor rolesQueryDescriptor = new QueryDescriptor();
			rolesQueryDescriptor.addOrderProperty(new BeanOrderProperty(Role.class,
					RolePropertyFields.FIELD_PATH_DISPLAYNAME, true));
			map.put("roles", groupAndRoleManagement.getRolesByGroup(clientName, group.getName(),
					null));

			List<User> allUsers = cachedUserList.getUsers(request);
			map.put("allAccounts", allUsers);

			List<Role> roles = groupAndRoleManagement.getRolesByClient(clientName,
					rolesQueryDescriptor).getResultList();
			map.put("allRoles", roles);

		} catch (Exception e) {
			logger.warn(e);
		}

		return map;
	}
}
