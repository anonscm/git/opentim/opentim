/**
 * 
 */
package org.evolvis.idm.administration.model;

import javax.validation.constraints.Pattern;

import org.evolvis.idm.common.validation.Regex;
import org.evolvis.idm.common.validation.Url;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;

/**
 * @author Michael Kutz, tarent GmbH
 * 
 *         JSR303 validatable wrapper for the ClientPropertyMap. This is needed to keep the Entity
 *         Bean ClientPropertyMap (and thereby the database) flexible.
 */
public class ClientPropertyMapWrapper {

	private ClientPropertyMap clientPropertyMap;

	public ClientPropertyMapWrapper() {
		super();
		this.clientPropertyMap = new ClientPropertyMap();
	}

	public ClientPropertyMapWrapper(ClientPropertyMap clientPropertyMap) {
		super();
		this.clientPropertyMap = clientPropertyMap;
	}

	/**
	 * @return the wrapped ClientPropertyMap.
	 */
	public ClientPropertyMap getClientPropertyMap() {
		return clientPropertyMap;
	}

	public void getClientPropertyMap(ClientPropertyMap clientPropertyMap) {
		this.clientPropertyMap = clientPropertyMap;
	}

	public String getInternalLastUpdateUsers() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_INTERNAL_LASTUPDATEUSERS);
	}

	public void setInternalLastUpdateUsers(String internalLastUpdateUsers) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_INTERNAL_LASTUPDATEUSERS,
				internalLastUpdateUsers);
	}

	@Url(message = "violation.clientPropertyMap.url")
	public String getOpenSsoServiceUrl() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_SERVICEURL);
	}

	public void setOpenSsoServiceUrl(String openSsoServiceUrl) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_OPENSSO_SERVICEURL, openSsoServiceUrl);
	}

	public String getOpenSsoAdminLogin() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_ADMINLOGIN);
	}

	public void setOpenSsoAdminLogin(String openSsoAdminLogin) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_OPENSSO_ADMINLOGIN, openSsoAdminLogin);
	}

	public String getOpenSsoAdminPassword() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_ADMINPASSWORD);
	}

	public void setOpenSsoAdminPassword(String openSsoAdminPassword) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_OPENSSO_ADMINPASSWORD,
				openSsoAdminPassword);
	}

	@Pattern(regexp = "((0|1)?[0-9]|2[0-3]):([0-5][0-9])", message = "violation.clientPropertyMap.pattern")
	public String getSystemDailyJobExecutionTime() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_SYSTEM_DAILYJOBEXECUTIONTIME);
	}

	public void setSystemDailyJobExecutionTime(String systemDailyJobExecutionTime) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_SYSTEM_DAILYJOBEXECUTIONTIME,
				systemDailyJobExecutionTime);
	}

	@Pattern(regexp = "\\d+", message = "violation.clientPropertyMap.pattern")
	public String getSystemDeactivatedAccountLifetimeInDays() {
		return this.clientPropertyMap
				.get(ClientPropertyMap.KEY_SYSTEM_DEACTIVATEDACCOUNTLIFETIMEINDAYS);
	}

	public void setSystemDeactivatedAccountLifetimeInDays(
			String systemDeactivatedAccountLifetimeInDays) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_SYSTEM_DEACTIVATEDACCOUNTLIFETIMEINDAYS,
				systemDeactivatedAccountLifetimeInDays);
	}

	public String getRegistrationTermsRequiredEnabled() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_REGISTRATION_TERMSREQUIRED_ENABLED);
	}

	public void setRegistrationTermsRequiredEnabled(String registrationTermsRequiredEnabled) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_REGISTRATION_TERMSREQUIRED_ENABLED,
				registrationTermsRequiredEnabled);
	}

	@Url(message = "violation.clientPropertyMap.url")
	public String getRegistrationTermsRequiredUrl() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_REGISTRATION_TERMSREQUIRED_URL);
	}

	public void setRegistrationTermsRequiredUrl(String registrationTermsRequiredUrl) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_REGISTRATION_TERMSREQUIRED_URL,
				registrationTermsRequiredUrl);
	}

	public String getRegistrationDisplayNameGenerationEnabled() {
		return this.clientPropertyMap
				.get(ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED);
	}

	public void setRegistrationDisplayNameGenerationEnabled(
			String registrationDisplayNameGenerationEnabled) {
		this.clientPropertyMap.put(
				ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED,
				registrationDisplayNameGenerationEnabled);
	}

	@Pattern(regexp = "(.*(%[a-zA-Z0-9]+%).*)+", message = "violation.clientPropertyMap.pattern")
	public String getRegistrationDisplayNameGenerationPattern() {
		return this.clientPropertyMap
				.get(ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_PATTERN);
	}

	public void setRegistrationDisplayNameGenerationPattern(
			String registrationDisplayNameGenerationPattern) {
		this.clientPropertyMap.put(
				ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_PATTERN,
				registrationDisplayNameGenerationPattern);
	}

	public String getRegistrationAlternativeLinkEnabled() {
		return this.clientPropertyMap
				.get(ClientPropertyMap.KEY_REGISTRATION_ALTERNATIVELINK_ENABLED);
	}

	public void setRegistrationAlternativeLinkEnabled(String registrationAlternativeLinkEnabled) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_REGISTRATION_ALTERNATIVELINK_ENABLED,
				registrationAlternativeLinkEnabled);
	}

	@Url(message = "violation.clientPropertyMap.url")
	public String getRegistrationAlternativeLinkUrl() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_REGISTRATION_ALTERNATIVELINK_URL);
	}

	public void setRegistrationAlternativeLinkUrl(String registrationAlternativeLinkUrl) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_REGISTRATION_ALTERNATIVELINK_URL,
				registrationAlternativeLinkUrl);
	}

	public String getRegistrationSuccessLinkEnabled() {
		return this.clientPropertyMap
				.get(ClientPropertyMap.KEY_REGISTRATION_REGISTRATIONSUCCESSLINK_ENABLED);
	}

	public void setRegistrationSuccessLinkEnabled(String registrationSuccessLinkEnabled) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_REGISTRATION_REGISTRATIONSUCCESSLINK_ENABLED,
				registrationSuccessLinkEnabled);
	}

	@Url(message = "violation.clientPropertyMap.url")
	public String getRegistrationSuccessLinkUrl() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_REGISTRATION_REGISTRATIONSUCCESSLINK_URL);
	}

	public void setRegistrationSuccessLinkUrl(String registrationSuccessLinkUrl) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_REGISTRATION_REGISTRATIONSUCCESSLINK_URL,
				registrationSuccessLinkUrl);
	}

	public String getActivationSuccessLinkEnabled() {
		return this.clientPropertyMap
				.get(ClientPropertyMap.KEY_REGISTRATION_ACTIVATIONSUCCESSLINK_ENABLED);
	}

	public void setActivationSuccessLinkEnabled(String registrationSuccessLinkEnabled) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_REGISTRATION_ACTIVATIONSUCCESSLINK_ENABLED,
				registrationSuccessLinkEnabled);
	}

	@Url(message = "violation.clientPropertyMap.url")
	public String getActivationSuccessLinkUrl() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_REGISTRATION_ACTIVATIONSUCCESSLINK_URL);
	}

	public void setActivationSuccessLinkUrl(String activationSuccessLinkUrl) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_REGISTRATION_ACTIVATIONSUCCESSLINK_URL,
				activationSuccessLinkUrl);
	}

	@Regex(message = "violation.clientPropertyMap.regex")
	public String getSecurityPasswordPattern() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_SECURITY_PASSWORT_PATTERN);
	}

	public void setSecurityPasswordPattern(String securityPasswordPattern) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_SECURITY_PASSWORT_PATTERN,
				securityPasswordPattern);
	}

	public String getSecurityAllowEmailExistenceHintsEnabled() {
		return this.clientPropertyMap
				.get(ClientPropertyMap.KEY_SECURITY_ALLOWEMAILEXISTENCEHINTS_ENABLED);
	}

	public void setSecurityAllowEmailExistenceHintsEnabled(
			String securityAllowEmailExistenceHintsEnabled) {
		this.clientPropertyMap.put(
				ClientPropertyMap.KEY_SECURITY_ALLOWEMAILEXISTENCEHINTS_ENABLED,
				securityAllowEmailExistenceHintsEnabled);
	}

	public String getldapSyncEnabled() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_LDAP_SYNC_ENABLED);
	}

	public void setldapSyncEnabled(String ldapSyncEnabled) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_ENABLED, ldapSyncEnabled);
	}

	public String getLdapSyncHostAndPort() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_LDAP_SYNC_HOSTANDPORT);
	}

	public void setLdapSyncHostAndPort(String ldapSyncHostAndPort) {
		this.clientPropertyMap
				.put(ClientPropertyMap.KEY_LDAP_SYNC_HOSTANDPORT, ldapSyncHostAndPort);
	}

	public String getLdapSyncUserDN() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_LDAP_SYNC_USERDN);
	}

	public void setLdapSyncUserDN(String ldapSyncUserDN) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_USERDN, ldapSyncUserDN);
	}

	public String getLdapSyncPassword() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_LDAP_SYNC_PASSWORD);
	}

	public void setLdapSyncPassword(String ldapSyncPassword) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_PASSWORD, ldapSyncPassword);
	}

	public String getLdapSyncBaseDN() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_LDAP_SYNC_BASEDN);
	}

	public void setLdapSyncBaseDN(String ldapSyncBaseDN) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_BASEDN, ldapSyncBaseDN);
	}

	public String getLdapSyncSearchDN() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHDN);
	}

	public void setLdapSyncSearchDN(String ldapSyncSearchDN) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHDN, ldapSyncSearchDN);
	}

	public String getLdapSyncSearchFilter() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHFILTER);
	}

	public void setLdapSyncSearchFilter(String ldapSyncSearchFilter) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHFILTER,
				ldapSyncSearchFilter);
	}

	@Pattern(regexp = "\\d+", message = "violation.clientPropertyMap.pattern")
	public String getSearchPagingLimit() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_SEARCH_PAGINGLIMIT);
	}

	public void setSearchPagingLimit(String searchPagingLimit) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_SEARCH_PAGINGLIMIT, searchPagingLimit);
	}

	public String getSearchFullPrivateDataSearchEnabled() {
		return this.clientPropertyMap
				.get(ClientPropertyMap.KEY_SEARCH_FULLPRIVATEDATASEARCH_ENABLED);
	}

	public void setSearchFullPrivateDataSearchEnabled(String searchFullPrivateDataSearchEnabled) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_SEARCH_FULLPRIVATEDATASEARCH_ENABLED,
				searchFullPrivateDataSearchEnabled);
	}

	@Pattern(regexp = "\\d+", message = "violation.clientPropertyMap.pattern")
	public String getDisplayPrivateDataManagementColumns() {
		return this.clientPropertyMap
				.get(ClientPropertyMap.KEY_DISPLAY_PRIVATEDATAMANAGEMENT_COLUMNS);
	}

	public void setDisplayPrivateDataManagementColumns(String displayPrivateDataManagementColumns) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_DISPLAY_PRIVATEDATAMANAGEMENT_COLUMNS,
				displayPrivateDataManagementColumns);
	}

	public String getOtherAllowDeleteUserEnabled() {
		return this.clientPropertyMap.get(ClientPropertyMap.KEY_OTHER_ALLOWDELETEUSER_ENABLED);
	}

	public void setOtherAllowDeleteUserEnabled(String otherAllowDeleteUserEnabled) {
		this.clientPropertyMap.put(ClientPropertyMap.KEY_OTHER_ALLOWDELETEUSER_ENABLED,
				otherAllowDeleteUserEnabled);
	}

}
