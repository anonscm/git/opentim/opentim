package org.evolvis.idm.administration.constant;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.idm.common.model.NavPoint;
import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.evolvis.idm.security.SecurityContextHolder;

/**
 * @author Tino Rink
 * @author Dmytro Mayster
 */
public class AdminNavPoints {

    public static final NavPoint USER_MANAGEMENT = new NavPoint("UserManagement", "nav.user_management", "user-button");

    public static final NavPoint GROUP_MANAGEMENT = new NavPoint("GroupManagement", "nav.group_management", "groups-button");

    public static final NavPoint ROLE_MANAGEMENT = new NavPoint("RoleManagement", "nav.role_management", "roles-button");

    public static final NavPoint ORGANISATION_MANAGEMENT = new NavPoint("OrganizationManagement", "nav.organisation_management",
            "organisations-button");
    
    public static final NavPoint PRIVATEDATA_CONFIGURATION = new NavPoint("PrivateDataConfiguration", "nav.privatedata_configuration",
    "privatedataconfiguration-button");

    public static final NavPoint APPLICATION_MANAGEMENT = new NavPoint("ApplicationManagement", "nav.application_management",
            "applications-button");

    public static final NavPoint CLIENT_MANAGEMENT = new NavPoint("ClientManagement", "nav.client_management",
            "admin-button");

    public static final NavPoint CLIENT_CONFIGURATION = new NavPoint("ClientConfiguration", "nav.client_configuration",
            "admin-button");


    public static List<NavPoint> getAll() {
    	List<NavPoint> tmpList = new ArrayList<NavPoint>();
    	InternalRoleType internalRole = SecurityContextHolder.getInternalRole();
    	tmpList.add(USER_MANAGEMENT);
        tmpList.add(GROUP_MANAGEMENT);
        if(internalRole.equals(InternalRoleType.SUPER_ADMIN)
        		|| internalRole.equals(InternalRoleType.CLIENT_ADMIN)) {
    		tmpList.add(ROLE_MANAGEMENT);
        }
        tmpList.add(ORGANISATION_MANAGEMENT);
        if(internalRole.equals(InternalRoleType.SUPER_ADMIN)
        		|| internalRole.equals(InternalRoleType.CLIENT_ADMIN)) {        
    		tmpList.add(APPLICATION_MANAGEMENT);
    		tmpList.add(PRIVATEDATA_CONFIGURATION);
    		tmpList.add(CLIENT_CONFIGURATION);
    	}
    	if (internalRole.equals(InternalRoleType.SUPER_ADMIN)) {
    		tmpList.add(CLIENT_MANAGEMENT);
    	}
    	
    	return tmpList;
    }
}
