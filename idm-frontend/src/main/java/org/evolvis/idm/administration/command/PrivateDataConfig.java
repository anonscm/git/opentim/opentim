package org.evolvis.idm.administration.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.AttributeType;
import org.springframework.context.MessageSource;

/**
 * Command object for PrivateDataConfig controller
 * 
 * @author mayster
 *
 */
public class PrivateDataConfig {

	private AttributeGroup attributeGroup;
	
	private Attribute attribute;
	
	private List<AttributeGroup> attributeGroups;
	
	private List<AttributeTypeWrapper> attributeTypes;
	
	private MessageSource resources;

	public PrivateDataConfig(MessageSource resources) {
		this.resources = resources;
		this.attributeGroup = new AttributeGroup();
		this.attribute = new Attribute();
		this.attributeGroups = new ArrayList<AttributeGroup>();
		this.attributeTypes = new ArrayList<AttributeTypeWrapper>();
		for (AttributeType attributeType : AttributeType.values()) {
			this.attributeTypes.add(new AttributeTypeWrapper(attributeType));
		}
	}

	public AttributeGroup getAttributeGroup() {
		return attributeGroup;
	}

	public void setAttributeGroup(AttributeGroup attributeGroup) {
		this.attributeGroup = attributeGroup;
	}

	public Attribute getAttribute() {
		return attribute;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public List<AttributeGroup> getAttributeGroups() {
		return attributeGroups;
	}

	public void setAttributeGroups(List<AttributeGroup> attributeGroups) {
		this.attributeGroups = attributeGroups;
	}
	
	public int getCountAttributeGroups() {
		return attributeGroups.size();
	}
	
	public int getCountAttributes() {
		int maxSize=1;
		for (AttributeGroup attributeGroup : attributeGroups) {
			if(attributeGroup.getAttributes() != null)
				maxSize = Math.max(maxSize, attributeGroup.getAttributes().size());
		}
		return maxSize;
	}
	
	public List<AttributeTypeWrapper> getAttributeTypes() {
		return attributeTypes;
	}

	public void setAttributeTypes(List<AttributeTypeWrapper> attributeTypes) {
		this.attributeTypes = attributeTypes;
	}

	public class AttributeTypeWrapper {
		AttributeType attributeType;
		
		public AttributeTypeWrapper(AttributeType attributeType) {
			this.attributeType = attributeType;
		}
		
		public String getName() {
			return attributeType.name();
		}
		
		public String getI18nName() {
			// TODO fix i18n, get locale from request in some way
			return resources.getMessage(attributeType.name(), null, Locale.getDefault());
		}
		
		public AttributeType getValue() {
			return AttributeType.valueOf(getName());
		}
	}
}
