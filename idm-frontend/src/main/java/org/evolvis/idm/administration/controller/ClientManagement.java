package org.evolvis.idm.administration.controller;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.util.SynchronizingRestClientUtil;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientQueryResult;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Jens Neumaier
 * @author Frederic Eßer
 */
@Controller
@RequestMapping(value = "view", params = "ctx=ClientManagement")
public class ClientManagement {
	public static final String COMMAND = "command";

	public static final String SUFFIX_SECOND_CLIENT_IN_ONE_LIFERAY_INSTANCE = "-shared-liferay";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected org.evolvis.idm.relation.multitenancy.service.ClientManagement clientManagement;

	@RequestMapping
	public String handleRenderRequest(
			@RequestParam(value = "sortOrder", defaultValue = "displayName.ASC") final String sortOrder,
			@RequestParam(value = "page", defaultValue = "1") Integer page,
			final RenderRequest request, final RenderResponse response, ModelMap model) {
		logger.info("handleRenderRequest() entered, request=" + request);

		model.addAllAttributes(referenceData(sortOrder, response, page));
		model.addAttribute("navPoints", AdminNavPoints.getAll());

		return "ClientManagement";
	}

	private Map<String, Object> referenceData(String sortOrder, RenderResponse response,
			Integer page) {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("clientname", SecurityContextHolder.getClientName());
		
		/*
		 * prepare the query descriptor
		 */
		QueryDescriptor queryDescriptor = new QueryDescriptor();

		/*
		 * add order
		 */
		String orderField = sortOrder.replaceFirst("(.*)\\.(ASC|DESC)", "$1");
		BeanOrderProperty orderProperty = new BeanOrderProperty(Client.class, orderField, sortOrder
				.endsWith("ASC"));
		queryDescriptor.addOrderProperty(orderProperty);

		/*
		 * add paging
		 */
		queryDescriptor.setLimit(SecurityContextHolder.getPagingLimit());
		queryDescriptor.setOffSet((page - 1) * queryDescriptor.getLimit());

		try {
			ClientQueryResult clientResults = clientManagement.getClients(queryDescriptor);
			
			/*
			 * correct page number (might be greater than last page due to searching)
			 */
			page = clientResults.getPageNumber();

			/*
			 * generate base URL for use in tag libs
			 */
			PortletURL pageUrl = response.createRenderURL();
			pageUrl.setParameter("ctx", "ClientManagement");
			pageUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			pageUrl.setParameter("sortOrder", sortOrder);
			map.put("pageUrl", pageUrl);
			PortletURL sortUrl = response.createRenderURL();
			sortUrl.setParameter("ctx", "ClientManagement");
			sortUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			sortUrl.setParameter("page", Integer.toString(page));
			map.put("sortUrl", sortUrl);

			map.put("clients", clientResults.getResultList());
			map.put("page", page);
			map.put("itemCount", clientResults.getTotalSize());
			map.put("itemLimit", queryDescriptor.getLimit());
			map.put("currentSortingOrder", sortOrder);
		} catch (BackendException e) {
			logger.warn(e);
		}

		return map;
	}

	@Secured( { "ROLE_SUPER_ADMIN" })
	@RequestMapping(params = "action=clientActivation")
	public void handleActivationAction(ActionRequest actionRequest, ModelMap model) {
		logger.info("handleActivationAction() entered, id=" + actionRequest.getParameter("client"));
		try {
			Client client = clientManagement.getClientByName(String.valueOf(actionRequest
					.getParameter("client")));
			// only do something if clientName has changed
			if (!client.getClientName().equals(SecurityContextHolder.getClientName())) {
				SecurityContextHolder.setClient(client);
				actionRequest.getPortletSession().setAttribute("client", client);
				actionRequest.setAttribute("msg_success", resources.getMessage(
						"success.client.changeactive", null, actionRequest.getLocale()));
				logger.info("handleActivationAction() successfully changed active client, uuid="
						+ SecurityContextHolder.getUUID() + ", id="
						+ actionRequest.getParameter("client"));

				// synchronize OpenSSO Rest-Client-Properties if client is newly created
				SynchronizingRestClientUtil.getClient(SecurityContextHolder.getClientPropertyMap());
			}
		} catch (BackendException e) {
			logger.warn(e);
			actionRequest.setAttribute("msg_warning", resources.getMessage(
					"error.client.changeactive", null, actionRequest.getLocale()));
		}
	}
}