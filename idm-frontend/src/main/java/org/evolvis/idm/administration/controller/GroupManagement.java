package org.evolvis.idm.administration.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.model.ValueType;
import org.evolvis.idm.common.propertyeditor.GroupPropertyEditor;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.GroupPropertyFields;
import org.evolvis.idm.relation.permission.model.GroupSearchableFields;
import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

/**
 * @author Frederic Eßer
 * @author Tino Rink
 * @author Dmytro Mayster
 * @author Michael Kutz
 */

@Controller
@RequestMapping(value = "view", params = "ctx=GroupManagement")
@SessionAttributes(GroupManagement.COMMAND)
public class GroupManagement {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected GroupAndRoleManagement groupAndRoleManagement;

	@Autowired
	protected InternalRoleScopeManagement internalRoleScopeManagement;

	@Autowired
	protected Validator validator;

	@Autowired
	protected GroupPropertyEditor groupPropertyEditor;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setAllowedFields(new String[] { "name", "displayName" });
		binder.registerCustomEditor(Group.class, groupPropertyEditor);
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=Create")
	public void handleCreateAction(@ModelAttribute(COMMAND) final Group group, final Errors errors,
			final ActionRequest request, final ModelMap model) {
		Client client = SecurityContextHolder.getClient();
		logger.info("handleCreateAction() entered, clientId=" + client.getId() + ", name="
				+ group.getName());

		try {
			group.setClient(client);
			group.setWriteProtected(false);

			/*
			 * validate and return if errors exists
			 */
			validator.validate(group, errors);
			if (errors.hasErrors()) {
				return;
			}

			groupAndRoleManagement.setGroup(SecurityContextHolder.getClientName(), group);
			request.setAttribute("msg_success", resources.getMessage("success.group.create", null,
					request.getLocale()));
			// clear the form after success submit
			model.clear();
		} catch (BackendException e) {
			/*
			 * show error message if the there already is a group with the new name
			 */
			if (e.getErrorCode().equals(IllegalRequestException.CODE_INVALID_DUPLICATE)) {
				errors.rejectValue("name", "error.group.existed");
			} else {
				logger.warn(e);
				errors.reject("error.group.create");
			}
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=Delete", "!confirmed", "!denied" })
	public void handleUnconfirmedDeleteAction(@RequestParam(value = "groupId") final Group group,
			final ActionRequest request) {
		logger.info("handleUnconfirmedDeleteAction() entered, groupId=" + group.getId());

		/*
		 * ask for confirmation and return
		 */
		request.setAttribute("deleteGroup", group);
		request.setAttribute("confirmation", "Delete");
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=Delete", "denied" })
	public void handleConfirmedDeleteAction(final ActionRequest request) {
		logger.info("handleDeniedDeleteAction() entered");

		/*
		 * remove confirmation request if confirmation was denied
		 */
		request.removeAttribute("deleteGroup");
		request.removeAttribute("confirmation");
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = { "action=Delete", "confirmed" })
	public void handleConfirmedDeleteAction(@RequestParam(value = "groupId") Group group,
			final ActionRequest request, final Locale locale) {
		logger.info("handleConfirmedDeleteAction() entered");
		/*
		 * delete the user
		 */
		try {
			groupAndRoleManagement
					.deleteGroup(SecurityContextHolder.getClientName(), group.getId());
			request.setAttribute("msg_success", resources.getMessage("success.group.delete", null,
					request.getLocale()));
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_error", resources.getMessage("error.group.delete", null,
					locale));
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping
	public ModelAndView handleRenderRequest(
			@RequestParam(value = "keywords", required = false) final String search,
			@RequestParam(value = "sortOrder", defaultValue = "displayName.ASC") final String sortOrder,
			@RequestParam(value = "page", defaultValue = "1") final Integer page,
			final Locale locale, final RenderRequest request, final RenderResponse response) {
		logger.info("handleRenderRequest() entered, request=" + request);

		ModelAndView modelAndView = new ModelAndView("GroupManagement", COMMAND, new Group());
		modelAndView.addAllObjects(referenceData(search, sortOrder, request, response, page));
		modelAndView.addObject("navPoints", AdminNavPoints.getAll());

		return modelAndView;
	}

	private Map<String, Object> referenceData(String search, String sortOrder,
			RenderRequest renderRequest, RenderResponse renderResponse, Integer page) {
		Map<String, Object> map = new HashMap<String, Object>();

		/*
		 * remove illegal characters from search and set warning
		 */
		if (search != null && !search.matches(DisplayName.PATTERN)) {
			search = search.replaceAll(DisplayName.PATTERN_INVERSE_ONECHAR, "");
			renderRequest.setAttribute("msg_warning", resources.getMessage(
					"warning.search.removedillegalchars", null, renderResponse.getLocale()));
		}

		map.put("clientname", SecurityContextHolder.getClientName());
		map.put("keywords", search);

		/*
		 * prepare the query descriptor
		 */
		QueryDescriptor queryDescriptor = new QueryDescriptor();

		/*
		 * add order
		 */
		String orderField = sortOrder.replaceFirst("(.*)\\.(ASC|DESC)", "$1");
		BeanOrderProperty orderProperty = new BeanOrderProperty(Group.class, orderField, sortOrder
				.endsWith("ASC"));
		queryDescriptor.addOrderProperty(orderProperty);

		/*
		 * add paging
		 */
		queryDescriptor.setLimit(SecurityContextHolder.getPagingLimit());
		queryDescriptor.setOffSet((page - 1) * queryDescriptor.getLimit());

		/*
		 * add search
		 */
		if (search != null) {
			BeanSearchProperty searchPropertyName = new BeanSearchProperty(Group.class,
					GroupPropertyFields.FIELD_PATH_NAME, search);
			queryDescriptor.addSearchProperty(searchPropertyName);

			BeanSearchProperty searchPropertyDisplayName = new BeanSearchProperty(Group.class,
					GroupPropertyFields.FIELD_PATH_DISPLAYNAME, search);
			queryDescriptor.addSearchProperty(searchPropertyDisplayName);
		}

		InternalRoleType internalRoleType = SecurityContextHolder.getInternalRole();

		try {
			if (internalRoleType.equals(InternalRoleType.SECTION_ADMIN)) {
				List<Group> groups = internalRoleScopeManagement.getInternalRoleScopesForAccount(
						SecurityContextHolder.getUUID(), InternalRoleType.SECTION_ADMIN, null);
				map.put("groups", groups);

				// search for something resulting in an empty list if section admin has no group
				// assignments yet
				if (groups.size() == 0) {
					BeanSearchProperty searchPropertyGroupNameNonExistent = new BeanSearchProperty(
							Group.class, GroupSearchableFields.FIELD_PATH_NAME,
							"SECTION-ADMIN-SHOULD-NOT-RECEIVE-ANY-GROUPS-HERE");
					queryDescriptor.addSearchProperty(searchPropertyGroupNameNonExistent);
				}

				for (Group group : groups) {
					BeanSearchProperty searchPropertyGroupId = new BeanSearchProperty(Group.class,
							GroupSearchableFields.FIELD_PATH_ID, group.getId().toString(),
							ValueType.LONG);
					queryDescriptor.addSearchProperty(searchPropertyGroupId);
				}
			}

			QueryResult<Group> groupResults = groupAndRoleManagement.getGroups(
					SecurityContextHolder.getClientName(), queryDescriptor);

			/*
			 * correct page number (might be greater than last page due to searching)
			 */
			page = groupResults.getPageNumber();

			/*
			 * generate base URL for use in tag libs
			 */
			PortletURL pageUrl = renderResponse.createRenderURL();
			pageUrl.setParameter("ctx", "GroupManagement");
			pageUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			pageUrl.setParameter("sortOrder", sortOrder);
			pageUrl.setParameter("keywords", search);
			map.put("pageUrl", pageUrl);
			PortletURL sortUrl = renderResponse.createRenderURL();
			sortUrl.setParameter("ctx", "GroupManagement");
			sortUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			sortUrl.setParameter("page", Integer.toString(page));
			sortUrl.setParameter("keywords", search);
			map.put("sortUrl", sortUrl);

			map.put("groups", groupResults.getResultList());
			map.put("page", page);
			map.put("itemCount", groupResults.getTotalSize());
			map.put("itemLimit", queryDescriptor.getLimit());
			map.put("currentSortingOrder", sortOrder);

			/*
			 * set message for search
			 */
			if (search != null && !"".equals(search)) {
				renderRequest.setAttribute("msg_searchstatus", resources.getMessage(
						"success.search", new Object[] { search, groupResults.getTotalSize() },
						renderResponse.getLocale()));
			}
		} catch (Exception e) {
			logger.error(e);
		}

		return map;
	}
}