package org.evolvis.idm.administration.util;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class GroupPermissionChecker {

	private final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	protected InternalRoleScopeManagement internalRoleScopeManagement;

	@Autowired
	protected GroupAndRoleManagement groupAndRoleManagement;
	
	public void checkUserGroupPermission(Group group) {
		try {
			// TODO check in backend!!!
			// check if user is section admin and has sufficient permissions to edit this group
			InternalRoleType internalRoleType = SecurityContextHolder.getInternalRole();
			if (internalRoleType.equals(InternalRoleType.SECTION_ADMIN)) {
				List<Group> groups = internalRoleScopeManagement.getInternalRoleScopesForAccount(SecurityContextHolder.getUUID(), InternalRoleType.SECTION_ADMIN, null);

				if (!groups.contains(group)) {
					String errorMsg = "WARNING! User with uuid="+SecurityContextHolder.getUUID()+" tried to edit a group with insufficient permissions.";
					logger.error(errorMsg);
					throw new AccessDeniedException(errorMsg);
				}
			}
		} catch (Exception e) {
			// throw runtime exception because this exception is only expected on hacking attempts
			throw new RuntimeException(e);
		}
	}
	
	public void checkUserGroupPermission(long groupId) {
		try {
			Group group = groupAndRoleManagement.getGroup(SecurityContextHolder.getClientName(), groupId);
			checkUserGroupPermission(group);
		} catch (Exception e) {
			// throw runtime exception because this exception is only expected on hacking attempts
			throw new RuntimeException(e);
		}
	}
}
