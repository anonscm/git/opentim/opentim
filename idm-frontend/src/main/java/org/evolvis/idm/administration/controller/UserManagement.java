package org.evolvis.idm.administration.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.common.propertyeditor.UserPropertyEditor;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalProperty;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.NavPoint;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.util.BackendServiceUtil;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.employeeactivation.controller.EmployeeActivation;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.model.UserPropertyFields;
import org.evolvis.idm.identity.account.model.UserSearchableFields;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.evolvis.idm.relation.permission.service.InternalRoleManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.evolvis.idm.security.SpringSecurityUser;
import org.evolvis.idm.synchronization.service.DirectoryServiceSynchronization;
import org.evolvis.opensso.rest.constant.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

/**
 * @author Tino Rink
 * @author Dmytro Mayster
 * @author Michael Kutz
 */
@Controller
@RequestMapping(value = "view", params = "ctx=UserManagement")
@SessionAttributes(UserManagement.COMMAND)
public class UserManagement {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected org.evolvis.idm.identity.account.service.UserManagement userManagement;

	@Autowired
	protected InternalRoleManagement internalRoleManagement;

	@Autowired
	protected Validator validator;

	@Autowired
	protected DirectoryServiceSynchronization directoryServiceSynchronization;

	@Autowired
	protected UserPropertyEditor userPropertyEditor;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setAllowedFields(new String[] { "login", "displayName", "firstname", "lastname",
				"password", "passwordRepeat" });
		binder.registerCustomEditor(User.class, userPropertyEditor);
	}
	
	/**
	 * Handles the <code>Add</code> action request by invoking the <code>createAccount</code> of
	 * injected {@link UserManagement}.
	 * 
	 * @param request
	 *            ActionRequest object
	 * @param user
	 *            UserDetail object with the input data
	 * @param errors
	 *            Errors object to collect from errors
	 * @param model
	 *            the holder of model attributes
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=Add")
	public void handleAddAction(@ModelAttribute(COMMAND) User user, final Errors errors,
			final ModelMap model, final Locale locale, final ActionRequest request) {
		logger.info("handleAddAction() entered,  username=" + user.getUsername() + ", firstname="
				+ user.getFirstname() + ", lastname=" + user.getLastname() + ", displayName="
				+ user.getDisplayName());

		try {
			if (userManagement.isUserExisting(SecurityContextHolder.getClientName(), user
					.getUsername())) {
				errors.rejectValue("login", "error.user.duplicateUser");
				return;
			}
			Client employeeClient = EmployeeActivation.getEmployeeClient(BackendServiceUtil
					.getClientManagementService().getClients(null).getResultList());
			if (employeeClient != null
					&& userManagement.isUserExisting(employeeClient.getName(), user.getUsername())) {
				errors.rejectValue("login", "error.user.duplicateUserEmployee");
				return;
			}
			Client citizenClient = EmployeeActivation.getCitizenClient(BackendServiceUtil
					.getClientManagementService().getClients(null).getResultList());
			if (citizenClient != null
					&& userManagement.isUserExisting(citizenClient.getName(), user.getUsername())) {
				errors.rejectValue("login", "error.user.duplicateUserCitizen");
				return;
			}

			user.setClient(SecurityContextHolder.getClient());

			user.setLocaleString(locale.toString());

			// validate and return if errors exists
			validator.validate(user, errors);

			String passwordPattern = SecurityContextHolder.getClientPropertyMap().get(
					ClientPropertyMap.KEY_SECURITY_PASSWORT_PATTERN);
			if (!passwordPattern.isEmpty() && !user.getPassword().matches(passwordPattern)) {
				errors.rejectValue("password", "violation.password.password");
			}

			if (errors.hasErrors()) {
				return;
			}

			userManagement.createUser(user);
			request.setAttribute("msg_success", resources.getMessage("success.user.create", null,
					locale));
			// clear the form after success submit
			model.clear();
		} catch (BackendException e) {
			if (e.getErrorCode().equals(IllegalRequestException.CODE_VALIDATION_ERROR)) {
				IllegalRequestException ire = (IllegalRequestException) e;
				List<IllegalProperty> illeagalProperties = ire.getIllegalProperties();
				for (IllegalProperty property : illeagalProperties) {
					errors.rejectValue(property.getPropertyName(), "error.user.validation");
				}
			} else {
				logger.warn(e);
				errors.reject("error.user.create");
			}
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=SynchronizeUser")
	public void handleSynchronizeUserAction(@RequestParam("uuid") final User user,
			final ModelMap model, final Locale locale, final ActionRequest request) {
		try {

			List<Value> rejectedValues = this.directoryServiceSynchronization.synchroniseAccount(user.getUuid());

			request.setAttribute("msg_success", resources.getMessage("success.user.synchronize",
					null, locale));
			
			// show rejected values in an additional warning message
			if (rejectedValues.size() > 0) {
				StringBuilder detailWarning = new StringBuilder();
				for (Value rejectedValue : rejectedValues) {
					String[] detailParams = new String[] { 
							rejectedValue.getAttribute().getAttributeGroup().getDisplayName(),
							rejectedValue.getAttribute().getDisplayName(),
							rejectedValue.getValue() };
					detailWarning.append(resources.getMessage("warning.user.synchronize.detail",
							detailParams, locale));
				}
				
				request.setAttribute("msg_warning", resources.getMessage("warning.user.synchronize",
						new String[] { detailWarning.toString() }, locale));
			}
			
			// clear the form after successful submit
			model.clear();
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_error", resources.getMessage("error.user.synchronize", null,
					locale));
		}
	}

	/**
	 * Handles the <code>ChangeStatus</code> action request by invoking the
	 * <code>setAccountStatus</code> of injected {@link UserManagement}.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN" })
	@RequestMapping(params = "action=ChangeStatus")
	public void handleChangeStatus(@RequestParam("uuid") final User user,
			@RequestParam("status") final String status,
			@RequestParam(value = "confirmed", required = false) final String confirmed,
			@RequestParam(value = "denied", required = false) final String denied,
			final Locale locale, final ActionRequest request) {

		logger.info("handleChangeStatus() entered, user=" + user + ", userStatus=" + status);
		
		SpringSecurityUser editor = (SpringSecurityUser) org.springframework.security.context.SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		try {
			InternalRoleType internalRole = internalRoleManagement.getInternalRoleForUser(user
					.getUuid());
			InternalRoleType editorInternalRole = editor.getInternalRole();

			/*
			 * check if the current user is allowed to perform the action
			 */
			if (internalRole.compareTo(editorInternalRole) < 0) {
				request.setAttribute("msg_error", resources.getMessage(
						"error.internalrole.toohigh", null, locale));
				return;
			}

			/*
			 * if a user should be deactivated return and ask the user for confirmation
			 */
			if (status.equals(UserStatus.INACTIVE) && confirmed == null && denied == null) {
				request.setAttribute("changeStatusUser", user);
				request.setAttribute("confirmation", "ChangeStatus");
				return;
			}
			
			/*
			 * remove confirmation if confirmation was denied
			 */
			if (denied != null) {
				request.removeAttribute("confirmation");
				return;
			}

			/*
			 * perform the requested action
			 */
			userManagement.setUserStatus(user.getUuid(), status);
			if (status.equals(UserStatus.ACTIVE)) {
				request.setAttribute("msg_success", resources.getMessage("success.user.activate",
						null, locale));
			} else {
				request.setAttribute("msg_success", resources.getMessage("success.user.deactivate",
						null, locale));
			}
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_error", resources.getMessage("error.user.changestatus", null,
					locale));
		}
	}

	/**
	 * Handles a unconfirmed <code>Delete</code> action request by invoking by asking for
	 * confirmation.
	 */
	@Secured( { "ROLE_SUPER_ADMIN" })
	@RequestMapping(params = { "action=Delete", "!confirmed", "!denied" })
	public void handleUnconfirmedDeleteAction(@RequestParam("uuid") final User user,
			final ActionRequest request) {

		logger.info("handleUnconfirmedDeleteAction() entered, user=" + user);

		/*
		 * return if deletion of users is allowed by client configuration
		 */
		if (!SecurityContextHolder.getClientPropertyMap().getBoolean(
				ClientPropertyMap.KEY_OTHER_ALLOWDELETEUSER_ENABLED)) {
			if (logger.isInfoEnabled()) {
				logger.info("handleDeleteAction(): delete of users is not enabled!");
			}
			return;
		}

		/*
		 * ask for confirmation
		 */
		request.setAttribute("deleteUser", user);
		request.setAttribute("confirmation", "Delete");
	}
	
	@Secured( { "ROLE_SUPER_ADMIN" })
	@RequestMapping(params = { "action=Delete", "denied" })
	public void handleDeniedDeleteAction(final ActionRequest request) {
		logger.info("handleDeniedDeleteAction() entered");
		
		/*
		 * remove confirmation request if confirmation was denied
		 */
		request.removeAttribute("confirmation");
	}

	/**
	 * Handles the <code>Delete</code> action request by invoking the <code>deleteAccount</code> of
	 * injected {@link UserManagement}.
	 */
	@Secured( { "ROLE_SUPER_ADMIN" })
	@RequestMapping(params = { "action=Delete", "confirmed" })
	public void handleConfirmedDeleteAction(@RequestParam("uuid") final User user,
			final Locale locale, final ActionRequest request) {
		logger.info("handleConfirmedDeleteAction() entered, user=" + user);
		
		/*
		 * return if deletion of users is not allowed by client configuration
		 */
		if (!SecurityContextHolder.getClientPropertyMap().getBoolean(
				ClientPropertyMap.KEY_OTHER_ALLOWDELETEUSER_ENABLED)) {
			if (logger.isInfoEnabled()) {
				logger.info("handleDeleteAction(): delete of users is not enabled!");
			}
			return;
		}

		/*
		 * delete the user
		 */
		try {
			userManagement.deleteUser(user.getUuid());
			request.setAttribute("msg_success", resources.getMessage("success.user.delete", null,
					locale));
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_error", resources.getMessage("error.user.delete", null,
					locale));
		}
	}

	/**
	 * Handles the render request and returns a {@link ModelAndView} object with a UserManagement
	 * view. The model of the {@link ModelAndView} contains following objects: <br/>
	 * <br/>
	 * <i>{@link UserDetail}</i> A command object stored under the <code>command</code> name. <br/>
	 * <i>clientname</i> The name of a current client. <br/>
	 * <i>users</i> List of <code>UserAccounts</code> got by invoking the
	 * <code>getAllAccounts</code> of injected {@link UserManagement}. <br/>
	 * <i>navPoints</i> List of all {@link NavPoint}.
	 * 
	 * @param request
	 *            RenderRequest object
	 * @return {@link ModelAndView} object
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping
	public ModelAndView handleRenderRequest(
			@RequestParam(value = "keywords", required = false) final String search,
			@RequestParam(value = "statusFilter", defaultValue = "All") final String statusFilter,
			@RequestParam(value = "sortOrder", defaultValue = "username.ASC") final String sortOrder,
			@RequestParam(value = "page", defaultValue = "1") final Integer page,
			final Locale locale, final RenderRequest request, final RenderResponse response) {
		logger.info("handleRenderRequest() entered, request=" + request);

		boolean displayNameGenerationEnabled = SecurityContextHolder.getClientPropertyMap()
				.getBoolean(ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED);
		User newUser = new User();
		if (displayNameGenerationEnabled)
			newUser.setDisplayName("------");

		ModelAndView modelAndView = new ModelAndView("UserManagement", COMMAND, newUser);
		modelAndView.addObject("displayNameGenerationEnabled", displayNameGenerationEnabled);
		modelAndView.addAllObjects(referenceData(search, statusFilter, sortOrder, request,
				response, page, locale));
		modelAndView.addObject("navPoints", AdminNavPoints.getAll());

		return modelAndView;
	}

	private Map<String, Object> referenceData(String search, String statusFilter, String sortOrder,
			RenderRequest request, RenderResponse response, Integer page, Locale locale) {
		Map<String, Object> map = new HashMap<String, Object>();

		/*
		 * remove illegal characters from search and set warning
		 */
		if (search != null && !search.matches("[" + DisplayName.CHARACTER_CLASS_BASE + "@+]*")) {
			search = search.replaceAll("[^" + DisplayName.CHARACTER_CLASS_BASE + "@+]", "");
			request.setAttribute("msg_warning", resources.getMessage(
					"warning.search.removedillegalchars", null, locale));
		}

		String clientName = SecurityContextHolder.getClientName();
		map.put("clientname", clientName);
		map.put("keywords", search);
		map.put("statusFilter", statusFilter);
		map.put("sortOrder", sortOrder);
		// see portal-tarent.properties:
		map.put("allowDeleteUser", SecurityContextHolder.getClientPropertyMap().getBoolean(
				ClientPropertyMap.KEY_OTHER_ALLOWDELETEUSER_ENABLED));

		/*
		 * prepare the query descriptor
		 */
		QueryDescriptor queryDescriptor = new QueryDescriptor();

		/*
		 * add order
		 */
		BeanOrderProperty orderProperty = new BeanOrderProperty(User.class,
				sortOrder.split("\\.")[0], "ASC".equals(sortOrder.split("\\.")[1]));
		queryDescriptor.addOrderProperty(orderProperty);

		/*
		 * add paging
		 */
		queryDescriptor.setLimit(SecurityContextHolder.getPagingLimit());
		queryDescriptor.setOffSet((page - 1) * queryDescriptor.getLimit());

		/*
		 * add search
		 */
		if (search != null) {
			/*
			 * if allowed by client configuration, search all private data fields
			 */
			if (SecurityContextHolder.getClientPropertyMap().getBoolean(
					ClientPropertyMap.KEY_SEARCH_FULLPRIVATEDATASEARCH_ENABLED)) {
				BeanSearchProperty searchPropertyAll = new BeanSearchProperty(User.class,
						UserPropertyFields.ALL, search);
				queryDescriptor.addSearchProperty(searchPropertyAll);
			}
			/*
			 * otherwise stick to some less private data fields
			 */
			else {
				// BeanSearchProperty searchPropertyUUID = new BeanSearchProperty(User.class,
				// UserSearchableFields.FIELD_PATH_UUID, search);
				// queryDescriptor.addSearchProperty(searchPropertyUUID);
				BeanSearchProperty searchPropertyUsername = new BeanSearchProperty(User.class,
						UserSearchableFields.FIELD_PATH_USERNAME, search);
				queryDescriptor.addSearchProperty(searchPropertyUsername);
				BeanSearchProperty searchPropertyDisplayName = new BeanSearchProperty(User.class,
						UserSearchableFields.FIELD_PATH_DISPLAYNAME, search);
				queryDescriptor.addSearchProperty(searchPropertyDisplayName);
				BeanSearchProperty searchPropertyFirstname = new BeanSearchProperty(User.class,
						UserSearchableFields.FIELD_PATH_FIRSTNAME, search);
				queryDescriptor.addSearchProperty(searchPropertyFirstname);
				BeanSearchProperty searchPropertyLastname = new BeanSearchProperty(User.class,
						UserSearchableFields.FIELD_PATH_LASTNAME, search);
				queryDescriptor.addSearchProperty(searchPropertyLastname);
			}
		}

		/*
		 * add status filter
		 */
		if (!"All".equals(statusFilter)) {
			BeanSearchProperty searchPropertyStatus = new BeanSearchProperty(User.class,
					UserPropertyFields.FIELD_PATH_SOFTDELETE, UserStatus.ACTIVE
							.equals(statusFilter) ? "false" : "true");
			queryDescriptor.addSearchProperty(searchPropertyStatus);
		}

		try {
			QueryResult<User> accountResults = userManagement.getUsers(clientName, queryDescriptor);

			/*
			 * correct page number (might be greater than last page due to searching)
			 */
			page = accountResults.getPageNumber();

			/*
			 * generate base URL for use in tag libs
			 */
			PortletURL pageUrl = response.createRenderURL();
			pageUrl.setParameter("ctx", "UserManagement");
			pageUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			pageUrl.setParameter("statusFilter", statusFilter);
			pageUrl.setParameter("sortOrder", sortOrder);
			pageUrl.setParameter("keywords", search);
			map.put("pageUrl", pageUrl);
			PortletURL sortUrl = response.createRenderURL();
			sortUrl.setParameter("ctx", "UserManagement");
			sortUrl.setParameter("itemLimit", Integer.toString(queryDescriptor.getLimit()));
			sortUrl.setParameter("page", Integer.toString(page));
			sortUrl.setParameter("statusFilter", statusFilter);
			sortUrl.setParameter("keywords", search);
			map.put("sortUrl", sortUrl);

			map.put("users", accountResults.getResultList());
			map.put("page", page);
			map.put("itemCount", accountResults.getTotalSize());
			map.put("itemLimit", queryDescriptor.getLimit());
			map.put("currentSortingOrder", sortOrder);

			Boolean ldapSyncEnabled = SecurityContextHolder.getClientPropertyMap().getBoolean(
					ClientPropertyMap.KEY_LDAP_SYNC_ENABLED);
			map.put("ldap_sync", ldapSyncEnabled);

			/*
			 * set message for search and filter
			 */
			if (search != null && !"".equals(search) || statusFilter != null
					&& !"All".equals(statusFilter)) {
				String filterString = resources.getMessage("label.users."
						+ statusFilter.toLowerCase(), null, locale);
				request.setAttribute("msg_searchstatus", resources.getMessage(
						"success.searchAndFilter", new Object[] { search, filterString,
								accountResults.getTotalSize() }, locale));
			}
		} catch (Exception e) {
			logger.warn(e);
		}

		return map;
	}
}