/**
 * 
 */
package org.evolvis.idm.administration.controller;

import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.administration.constant.AdminNavPoints;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * @author Frederic Eßer
 * 
 */
@Controller
@RequestMapping(value = "view", params = "ctx=ApplicationDetailManagement")
@SessionAttributes(ApplicationDetailManagement.COMMAND)
public class ApplicationDetailManagement {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected ApplicationManagement applicationManagement;

	@Autowired
	protected Validator validator;

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping(params = "action=updateApplication")
	public void handleUpdateApplicationAction(
			@ModelAttribute(COMMAND) @Valid final Application application, final Errors errors,
			@RequestParam(value = "confirmed", required = false) final String confirmed,
			@RequestParam(value = "denied", required = false) final String denied,
			final ModelMap model, final ActionRequest request, final Locale locale) {

		Client client = SecurityContextHolder.getClient();

		logger.info("handleUpdateApplicationAction() entered, applicationId=" + application.getId()
				+ " clientId=" + client.getId() + ", name=" + application.getName());

		/*
		 * return if any errors exists
		 */
		if (errors.hasErrors()) {
			return;
		}
		
		/*
		 * if the name of the Application should be changed, ask for confirmation
		 */
		try {
			Application preUpdateApplication = applicationManagement.getApplicationById(client.getName(),
					application.getId());
			preUpdateApplication.setClient(client);

			if (!preUpdateApplication.getName().equals(application.getName())) {

				/*
				 * show confirmation request
				 */
				if (confirmed == null && denied == null) {
					request.setAttribute("confirmation", "UpdateApplication");
					return;
				}
				/*
				 * remove confirmation request if it was denied and reset the form
				 */
				else if (denied != null) {
					request.removeAttribute("confirmation");
					
					model.remove(COMMAND);
					model.addAttribute(COMMAND, preUpdateApplication);
					return;
				}

			}

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.application.edit");
		}

		/*
		 * update the Application
		 */
		try {
			applicationManagement
					.setApplication(SecurityContextHolder.getClientName(), application);
			request.setAttribute("msg_success", resources.getMessage(
					"success.application.edit", null, locale));
		} catch (BackendException e) {
			/*
			 * show error message if the there already is a application with the new name
			 */
			if (e.getErrorCode().equals(IllegalRequestException.CODE_INVALID_DUPLICATE)) {
				errors.rejectValue("name", "error.application.existed");
			} else {
				logger.warn(e);
				errors.reject("error.application.edit");
			}
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN" })
	@RequestMapping
	public String handleRenderRequest(
			@RequestParam(value = "applicationId", required = false) Long applicationId,
			final ModelMap model) {
		logger.info("handleRenderRequest() entered, applicationId =" + applicationId);

		Application application;
		if (applicationId != null) {
			application = formBackingObject(applicationId);
			model.addAttribute(COMMAND, application);
		} else {
			application = (Application) model.get(COMMAND);
		}

		model.addAttribute("clientname", SecurityContextHolder.getClientName());
		model.addAttribute("navPoints", AdminNavPoints.getAll());

		return "ApplicationDetailManagement";
	}

	private Application formBackingObject(Long applicationId) {
		Application application = new Application();

		try {
			application = applicationManagement.getApplicationById(SecurityContextHolder
					.getClientName(), applicationId);
			application.setClient(SecurityContextHolder.getClient());
		} catch (Exception e) {
			logger.warn(e);
		}

		return application;
	}
}
