package org.evolvis.idm.administration.util;

import java.util.List;

import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.model.UserPropertyFields;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class CachedUserList {
	
	public static final String KEY_USER_LIST = "userListCache";
	
	public static final String KEY_LAST_UPDATE_CACHE = "lastUpdateUserListCache";
	
	private final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	protected ClientManagement clientManagement;
	
	@Autowired
	protected UserManagement userManagement;

	@SuppressWarnings("unchecked")
	public List<User> getUsers(RenderRequest request) throws IllegalRequestException, BackendException {
		/*
		 * determine last update and the user list cached in the session
		 */
		PortletSession portletSession = request.getPortletSession(true);
		Long lastUpdateCache = (Long) portletSession.getAttribute(KEY_LAST_UPDATE_CACHE);
		Long lastUpdateServer = clientManagement.getLastUserListChangeTimeMillis(SecurityContextHolder.getClientName());
		List<User> users = (List<User>) portletSession.getAttribute(KEY_USER_LIST);
		
		/*
		 * if no cached user list exists or the server's user list is newer than the cached one
		 * refetch the user list again
		 */
		if (users == null || lastUpdateCache == null || lastUpdateServer > lastUpdateCache) {
			portletSession.setAttribute(KEY_LAST_UPDATE_CACHE, lastUpdateServer);
			
			/*
			 * use ascending order by last name
			 */
			QueryDescriptor queryDescriptor = new QueryDescriptor();
			queryDescriptor.addOrderProperty(new BeanOrderProperty(User.class, UserPropertyFields.FIELD_PATH_LASTNAME, true));
			users = userManagement.getUsers(SecurityContextHolder.getClientName(), queryDescriptor).getResultList();
			
			/*
			 * put fetched list into session
			 */
			portletSession.setAttribute(KEY_USER_LIST, users);
			
			logger.info("Updated user list session cache for uuid="+SecurityContextHolder.getUUID());
		}
		return users;
	}

}
