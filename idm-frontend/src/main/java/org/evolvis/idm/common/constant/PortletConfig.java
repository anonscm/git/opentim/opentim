package org.evolvis.idm.common.constant;

import static org.evolvis.idm.common.constant.PortletConfigKeys.TARENT_IDM_BACKEND_ENDPOINT;
import static org.evolvis.idm.common.constant.PortletConfigKeys.TARENT_REGISTER_CONFIRM_REDIRECT;
import static org.evolvis.idm.common.constant.PortletConfigKeys.TARENT_REGISTER_CONFIRM_URL;
import static org.evolvis.idm.common.constant.PortletConfigKeys.TARENT_REGISTER_DOUBLE_OPT_IN;
import static org.evolvis.idm.common.constant.PortletConfigKeys.TARENT_REGISTER_EMPLOYEE_FORCEMULTICLIENTSETUP;
import static org.evolvis.idm.common.constant.PortletConfigKeys.TARENT_REGISTER_MAIL_HOST;
import static org.evolvis.idm.common.constant.PortletConfigKeys.TARENT_REGISTER_MAIL_LOGIN;
import static org.evolvis.idm.common.constant.PortletConfigKeys.TARENT_REGISTER_MAIL_PASSWD;
import static org.evolvis.idm.common.constant.PortletConfigKeys.TARENT_REGISTER_MAIL_SENDER;
import static org.evolvis.idm.common.constant.PortletConfigKeys.TARENT_SSO_LOGIN_AUTOREDIRECT;
import static org.evolvis.idm.common.constant.PortletConfigKeys.TARENT_SSO_LOGIN_REDIRECT;

import org.evolvis.config.Configuration;
import org.evolvis.config.PropertyConfig;

public class PortletConfig {

    private static final Configuration config = PropertyConfig.getInstance();

    public static final String LOGIN_REDIRECT = config.get(TARENT_SSO_LOGIN_REDIRECT, "/");
    
    public static final boolean LOGIN_AUTOREDIRECT = Boolean.valueOf(config.get(TARENT_SSO_LOGIN_AUTOREDIRECT, "false"));

    public static final boolean REGISTER_DOUBLE_OPT_IN = Boolean.valueOf(config.get(TARENT_REGISTER_DOUBLE_OPT_IN));
    
    public static final boolean REGISTER_EMPLOYEE_FORCEMULTICLIENTSETUP = Boolean.valueOf(config.get(TARENT_REGISTER_EMPLOYEE_FORCEMULTICLIENTSETUP, "false"));

    public static final String REGISTER_MAIL_HOST = config.get(TARENT_REGISTER_MAIL_HOST);

    public static final String REGISTER_MAIL_LOGIN = config.get(TARENT_REGISTER_MAIL_LOGIN);

    public static final String REGISTER_MAIL_PASSWD = config.get(TARENT_REGISTER_MAIL_PASSWD);

    public static final String REGISTER_MAIL_SENDER = config.get(TARENT_REGISTER_MAIL_SENDER);

    public static final String REGISTER_ID_SEPERATOR = "_";

    public static final String REGISTER_CONFIRM_KEY = "registerKey";
    
    public static final String REGISTER_CONFIRM_VALUE_PLACEHOLDER = "registerValue";

    public static final String REGISTER_CONFIRM_URL = config.get(TARENT_REGISTER_CONFIRM_URL);

    public static final String REGISTER_CONFIRM_REDIRECT = config.get(TARENT_REGISTER_CONFIRM_REDIRECT);
    
    public static final String IDM_BACKEND_ENDPOINT = config.get(TARENT_IDM_BACKEND_ENDPOINT);
}
