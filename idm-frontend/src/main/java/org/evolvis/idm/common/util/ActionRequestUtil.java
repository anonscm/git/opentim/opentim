package org.evolvis.idm.common.util;

import javax.portlet.ActionRequest;

/**
 * @author Tino Rink
 */
public class ActionRequestUtil {

    /**
     * Copy the parameter-values for the given parameter-names to the attribute of the ActionRequest object
     */
    public static void copyParametersToAttributes(final ActionRequest request, final String... parameters) {
        for (final String parameter : parameters) {
            request.setAttribute(parameter, request.getParameter(parameter));
        }
    }
    
}
