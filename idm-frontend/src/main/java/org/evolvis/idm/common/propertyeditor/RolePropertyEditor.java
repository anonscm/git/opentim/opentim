package org.evolvis.idm.common.propertyeditor;

import java.beans.PropertyEditorSupport;

import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Michael Kutz
 */

@Component
public class RolePropertyEditor extends PropertyEditorSupport {

	@Autowired
	protected GroupAndRoleManagement groupAndRoleManagement;

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		Role role;

		try {
			Long id = Long.parseLong(text);

			role = groupAndRoleManagement.getRole(SecurityContextHolder.getClientName(), id);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Not a valid number entered");
		} catch(Exception e) {
			throw new IllegalArgumentException("Not a valid id entered: " + text);
		}

		this.setValue(role);
	}

	@Override
	public String getAsText() {
		Long id = new Long(-1);

		Role role = (Role)this.getValue();

		if (role != null) {
			id = role.getId();
		}

		return id.toString();
	}

}
