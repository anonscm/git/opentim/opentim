package org.evolvis.idm.common.model;

public class NavPoint {
	private final String ctx;
	
	private final String name;

    private final String cssClass;
	
	public NavPoint(final String ctx, final String name, final String cssClass) {
		this.ctx = ctx;
		this.name = name;
        this.cssClass = cssClass;
	}

    public String getCtx() {
        return ctx;
    }

    public String getName() {
        return name;
    }

    public String getCssClass() {
        return cssClass;
    }
}
