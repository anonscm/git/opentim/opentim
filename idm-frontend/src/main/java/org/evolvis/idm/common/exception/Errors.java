package org.evolvis.idm.common.exception;

public class Errors {
	
	public static final String BACKEND_NOT_AVAILABLE = "idm_backend_not_available";

	public static final String SECURITY_CONTEXT_CREATION_FAILED = "security_context_creation_failed";

    public static final String EMAIL_SENDING_FAILED = "email_sending_failed";

	public static final String TENANT_NOT_AVAILABLE = "idm_tenant_not_available";
}
