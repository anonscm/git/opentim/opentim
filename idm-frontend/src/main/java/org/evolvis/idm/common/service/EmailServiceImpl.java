package org.evolvis.idm.common.service;

import static org.evolvis.idm.common.constant.PortletConfig.REGISTER_MAIL_HOST;
import static org.evolvis.idm.common.constant.PortletConfig.REGISTER_MAIL_LOGIN;
import static org.evolvis.idm.common.constant.PortletConfig.REGISTER_MAIL_PASSWD;
import static org.evolvis.idm.common.constant.PortletConfig.REGISTER_MAIL_SENDER;

import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import com.liferay.portal.kernel.util.StringPool;

public class EmailServiceImpl implements EmailService {

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Override
	public void sendPassowortRecoveryMail(String clientHost, Locale locale, String emailAddress, String name, String recoveryPassword) throws EmailException {
		if (logger.isDebugEnabled()) {
			logger.debug("sendPassowortRecoveryMail() entered, clientHost=" + clientHost + ", locale=" + locale + ", emailAddress=" + emailAddress + ", name=" + name);
		}

		/*
		 * define content
		 */
		String subject = resources.getMessage("mail.passwordrecovery.subject", new String[] { clientHost }, locale);
		String message = resources.getMessage("mail.passwordrecovery.message", new String[] { name, clientHost, recoveryPassword }, locale);

		/*
		 * send mail
		 */
		sendEmail(REGISTER_MAIL_HOST, REGISTER_MAIL_LOGIN, REGISTER_MAIL_PASSWD, REGISTER_MAIL_SENDER, REGISTER_MAIL_SENDER, emailAddress, name, subject, message);
	}

	@Override
	public void sendSelfRegistrationMail(String clientHost, Locale locale, String emailAddress, String name, String activationLink) throws EmailException {
		if (logger.isDebugEnabled()) {
			logger.debug("sendSelfRegistrationMail() entered, clientHost=" + clientHost + ", locale=" + locale + ", emailAddress=" + emailAddress + ", name=" + name);
		}

		/*
		 * define content
		 */
		String subject = resources.getMessage("mail.selfregistration.subject", new String[] { clientHost }, locale);
		String message = resources.getMessage("mail.selfregistration.message", new String[] { name, clientHost, activationLink }, locale);

		/*
		 * send mail
		 */
		sendEmail(REGISTER_MAIL_HOST, REGISTER_MAIL_LOGIN, REGISTER_MAIL_PASSWD, REGISTER_MAIL_SENDER, REGISTER_MAIL_SENDER, emailAddress, name, subject, message);
	}

	@Override
	public void sendAdminRegistrationMail(String clientHost, Locale locale, String emailAddress, String name, String activationLink) throws EmailException {
		if (logger.isDebugEnabled()) {
			logger.debug("sendAdminRegistrationMail() entered, clientHost=" + clientHost + ", locale=" + locale + ", emailAddress=" + emailAddress + ", name=" + name);
		}

		/*
		 * define content
		 */
		String subject = resources.getMessage("mail.adminregistration.subject", new String[] { clientHost }, locale);
		String message = resources.getMessage("mail.adminregistration.message", new String[] { name, clientHost, activationLink }, locale);

		/*
		 * send mail
		 */
		sendEmail(REGISTER_MAIL_HOST, REGISTER_MAIL_LOGIN, REGISTER_MAIL_PASSWD, REGISTER_MAIL_SENDER, REGISTER_MAIL_SENDER, emailAddress, name, subject, message);
	}
	
	@Override
	public void sendEmailChangeRequestMail(String clientHost, Locale locale, String newEmailAddress, String name, String activationLink) throws EmailException {
		if (logger.isDebugEnabled()) {
			logger.debug("sendEmailChangeRequestMail() entered, clientHost=" + clientHost + ", locale=" + locale + ", emailAddress=" + newEmailAddress + ", name=" + name);
		}
		
		/*
		 * define content
		 */
		String subject = resources.getMessage("mail.requestemailchange.subject", new String[] { clientHost }, locale);
		String message = resources.getMessage("mail.requestemailchange.message", new String[] { name, clientHost, activationLink }, locale);

		/*
		 * send mail
		 */
		sendEmail(REGISTER_MAIL_HOST, REGISTER_MAIL_LOGIN, REGISTER_MAIL_PASSWD, REGISTER_MAIL_SENDER, REGISTER_MAIL_SENDER, newEmailAddress, name, subject, message);
	}

	@Override
	public void sendEmailChangeRequestFailureMail(String clientHost, Locale locale, String emailAddress, String name) throws EmailException {
		if (logger.isDebugEnabled()) {
			logger.debug("sendAdminRegistrationMail() entered, clientHost=" + clientHost + ", locale=" + locale + ", emailAddress=" + emailAddress + ", name=" + name);
		}
		
		/*
		 * define content
		 */
		String subject = resources.getMessage("mail.requestemailchangefail.subject", new String[] { clientHost }, locale);
		String message = resources.getMessage("mail.requestemailchangefail.message", new String[] { name, clientHost }, locale);

		/*
		 * send mail
		 */
		sendEmail(REGISTER_MAIL_HOST, REGISTER_MAIL_LOGIN, REGISTER_MAIL_PASSWD, REGISTER_MAIL_SENDER, REGISTER_MAIL_SENDER, emailAddress, name, subject, message);
	}

	/**
	 * Helper method to send a simple email.
	 * 
	 * @param host
	 *            the mail client host to be used.
	 * @param login
	 *            login name for the mail client host.
	 * @param password
	 *            password for the mail client host.
	 * @param fromAddress
	 *            email address of the sender.
	 * @param fromName
	 *            name of the sender.
	 * @param toAddress
	 *            email address of the receiver.
	 * @param toName
	 *            name of the receiver.
	 * @param subject
	 *            subject for the message.
	 * @param body
	 *            the message's content.
	 * @throws EmailException
	 *             if sending of the mail fails.
	 */
	private void sendEmail(String host, String login, String password, String fromAddress, String fromName, String toAddress, String toName, String subject, String body) throws EmailException {
		/*
		 * setup empty mail
		 */
		SimpleEmail email = new SimpleEmail();
		email.setCharset("UTF-8");
		email.setHostName(host);

		if ((null != login) && (null != password) && !StringPool.BLANK.equalsIgnoreCase(login) && !StringPool.BLANK.equalsIgnoreCase(password)) {
			email.setAuthentication(login, password);
		} else {
			email.setAuthenticator(null);
		}

		email.setFrom(fromAddress, fromName);

		/*
		 * set mail content
		 */
		email.addTo(toAddress, toName);
		email.setSubject(subject);
		email.setMsg(body);

		/*
		 * send
		 */
		if (logger.isDebugEnabled())
			logger.debug("sending email:" + email);
		email.send();
	}
}