package org.evolvis.idm.common.interceptor;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.handler.HandlerInterceptorAdapter;

/**
 * Spring-Portlet-Interceptor for ParameterMapping with support of Redirection
 * 
 * @author Tino Rink
 */
public class ParameterMappingInterceptor extends HandlerInterceptorAdapter {

    public static final String DEFAULT_PARAMETER_NAME = "action";

    private String parameterName = DEFAULT_PARAMETER_NAME;
    
    /**
     * Set the name of the parameter used for mapping.
     */
    public void setParameterName(String parameterName) {
        this.parameterName = (parameterName != null ? parameterName : DEFAULT_PARAMETER_NAME);
    }

    @Override
    public void afterActionCompletion(final ActionRequest request,final ActionResponse response,final Object handler,final Exception ex) throws Exception {
        // copy parameter value
        try {
            final Object attributeValue = request.getAttribute(this.parameterName);
            if (null != attributeValue) {
                response.setRenderParameter(parameterName, String.valueOf(attributeValue));
                return;
            }
            final String parameterValue = request.getParameter(this.parameterName);
            if (null != parameterValue) {
                response.setRenderParameter(parameterName, parameterValue);
                return;
            }
        }
        catch (IllegalStateException ise) {
            // Probably sendRedirect called... no need to handle this exception
        }
    }
    
    @Override
    public void postHandleRender(final RenderRequest request, final RenderResponse response, final Object handler, final ModelAndView modelAndView)
            throws Exception {
        final Object attributeValue = request.getAttribute(this.parameterName);
        if (null != attributeValue) {
            modelAndView.addObject(parameterName, attributeValue);
            return;
        }
        final String parameterValue = request.getParameter(this.parameterName);
        if (null != parameterValue) {
            modelAndView.addObject(parameterName, parameterValue);
            return;
        }
    }
}
