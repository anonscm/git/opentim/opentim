package org.evolvis.idm.common.portletfilter;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.RenderFilter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import edu.emory.mathcs.backport.java.util.Arrays;

@SuppressWarnings("unchecked")
public class TrimRequestParamsPortletFilter implements ActionFilter, RenderFilter {
	public static String[] EXCLUDED_PARAMETERS = { "password", "passwordRepeat" };
	public static List<String> EXCLUDED_PARAMETERS_LIST = Arrays.asList(EXCLUDED_PARAMETERS);
	
	private static Log log = LogFactory.getLog(TrimRequestParamsPortletFilter.class.getName());

	@Override
	public void init(FilterConfig filterConfig) throws PortletException {
	}
	
	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(RenderRequest request, RenderResponse response, FilterChain chain) throws IOException, PortletException {
		TrimRenderRequestWrapper requestWrapper = new TrimRenderRequestWrapper(request);
		chain.doFilter(requestWrapper, response);
	}
	
	@Override
	public void doFilter(ActionRequest request, ActionResponse response, FilterChain chain) throws IOException, PortletException {
		TrimActionRequestWapper requestWrapper = new TrimActionRequestWapper(request);
		chain.doFilter(requestWrapper, response);
	}
	
	public static String trim(String name, String string) {
		if (EXCLUDED_PARAMETERS_LIST.contains(name))
			return string;
		if (log.isTraceEnabled())
			log.trace("Parameter "+name+" before trimming: \""+string+"\"");
		if (string != null)
			return string.trim();
		if (log.isTraceEnabled())
			log.trace("Parameter "+name+" after trimming: \""+string+"\"");
		return null;
	}
	
	public static void trim(String name, String[] strings) {
		if (strings != null)
			for (int i = 0; i < strings.length; i++)
				strings[i] = trim(name, strings[i]);
	}
	
	public static void trim(Map<String, String[]> strings) {
		for (Map.Entry<String, String[]> entry : strings.entrySet())
			trim(entry.getKey(), entry.getValue());
	}
}
