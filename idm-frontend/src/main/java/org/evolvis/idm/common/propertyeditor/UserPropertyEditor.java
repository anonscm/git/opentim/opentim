package org.evolvis.idm.common.propertyeditor;

import java.beans.PropertyEditorSupport;

import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Michael Kutz
 */

@Component
public class UserPropertyEditor extends PropertyEditorSupport {

	@Autowired
	protected UserManagement userManagement;

	/**
	 * {@inheritDoc} Returns a <code>User</code> object specified by its UUID.
	 * 
	 * @param uuid
	 *            the <code>User</code> object's UUID.
	 */
	@Override
	public void setAsText(String uuid) throws IllegalArgumentException {
		User user;

		try {
			/*
			 * get the user object
			 */
			user = userManagement.getUserByUuid(uuid);
		} catch (Exception e) {
			/*
			 * throw exception if no user was returned for the given UUID
			 */
			throw new IllegalArgumentException("Not a valid uuid entered: " + uuid);
		}

		this.setValue(user);
	}

	/**
	 * {@inheritDoc} Returns the UUID of the User or null.
	 */
	@Override
	public String getAsText() {
		User user = (User) this.getValue();
		if (user != null) {
			return user.getUuid();
		}
		return null;
	}

}
