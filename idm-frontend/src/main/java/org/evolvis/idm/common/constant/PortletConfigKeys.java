package org.evolvis.idm.common.constant;

public class PortletConfigKeys {

    public static final String TARENT_SSO_LOGIN_REDIRECT = "tarent.sso.service.redirect";
    
    /* Do not change the value of this key while it is not correctly referenced from OpenSSOFilter */
    public static final String TARENT_SSO_LOGIN_AUTOREDIRECT = "tarent.sso.service.autoRedirect";

    public static final String TARENT_REGISTER_DOUBLE_OPT_IN = "tarent.register.double_opt_in";

    public static final String TARENT_REGISTER_MAIL_HOST = "tarent.register.mail.host";

    public static final String TARENT_REGISTER_MAIL_LOGIN = "tarent.register.mail.login";

    public static final String TARENT_REGISTER_MAIL_PASSWD = "tarent.register.mail.passwd";

    public static final String TARENT_REGISTER_MAIL_SENDER = "tarent.register.mail.sender";

    public static final String TARENT_REGISTER_CONFIRM_URL = "tarent.register.confirm.url";

    public static final String TARENT_REGISTER_CONFIRM_REDIRECT = "tarent.register.confirm.redirect";
    
    public static final String TARENT_IDM_BACKEND_ENDPOINT = "tarent.idm.backend.endpoint";
    
    public static final String TARENT_REGISTER_EMPLOYEE_FORCEMULTICLIENTSETUP = "tarent.register.employee.forceMultiClientSetup";
}
