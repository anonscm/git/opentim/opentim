package org.evolvis.idm.common.propertyeditor;

import java.beans.PropertyEditorSupport;

import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Michael Kutz
 */

@Component
public class OrgUnitPropertyEditor extends PropertyEditorSupport {

	@Autowired
	protected OrganizationalUnitManagement organizationalUnitManagement;

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		OrgUnit orgUnit;

		try {
			Long id = Long.parseLong(text);

			orgUnit = organizationalUnitManagement.getOrgUnit(SecurityContextHolder.getClientName(), id);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Not a valid number entered");
		} catch(Exception e) {
			throw new IllegalArgumentException("Not a valid id entered: " + text);
		}

		this.setValue(orgUnit);
	}

	@Override
	public String getAsText() {
		Long id = new Long(-1);

		OrgUnit orgUnit = (OrgUnit)this.getValue();

		if (orgUnit != null) {
			id = orgUnit.getId();
		}

		return id.toString();
	}

}
