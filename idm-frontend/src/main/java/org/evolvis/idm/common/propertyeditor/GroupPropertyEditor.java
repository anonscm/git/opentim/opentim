package org.evolvis.idm.common.propertyeditor;

import java.beans.PropertyEditorSupport;

import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Dmytro Mayster
 * 
 * This PropertyEditor is used to effect the conversion between an 
 * Group's Object and an Group's Id. It is used in 
 * SpringMVC controllers.
 *
 */

@Component
public class GroupPropertyEditor extends PropertyEditorSupport {

	@Autowired
	protected GroupAndRoleManagement groupAndRoleManagement;

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		Group group;

		try {
			Long id = Long.parseLong(text);

			group = groupAndRoleManagement.getGroup(SecurityContextHolder.getClientName(), id);
			group.setClient(SecurityContextHolder.getClient());

		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Not a valid number entered");
		} catch(Exception e) {
			throw new IllegalArgumentException("Not a valid id entered: " + text);
		}

		this.setValue(group);
	}

	@Override
	public String getAsText() {
		Long id = new Long(-1);

		Group group = (Group)this.getValue();

		if (group != null) {
			id = group.getId();
		}

		return id.toString();
	}

}
