package org.evolvis.idm.common.propertyeditor;

import java.beans.PropertyEditorSupport;

import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.service.PrivateDataConfiguration;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Dmytro Mayster
 * 
 * This PropertyEditor is used to effect the conversion between an 
 * AttributeGroup's Object and an AttributeGroup's Id. It is used in 
 * SpringMVC controllers.
 *
 */

@Component
public class AttributeGroupPropertyEditor extends PropertyEditorSupport {

	@Autowired
	protected PrivateDataConfiguration privateDataConfiguration;

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		AttributeGroup attributeGroup;

		try {
			Long id = Long.parseLong(text);

			attributeGroup = privateDataConfiguration.getAttributeGroupById(SecurityContextHolder.getClientName(), id);

		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Not a valid number entered");
		} catch(Exception e) {
			throw new IllegalArgumentException("Not a valid id entered: " + text);
		}

		this.setValue(attributeGroup);
	}

	@Override
	public String getAsText() {
		Long id = new Long(-1);

		AttributeGroup attributeGroup = (AttributeGroup)this.getValue();

		if (attributeGroup != null) {
			id = attributeGroup.getId();
		}

		return id.toString();
	}

}
