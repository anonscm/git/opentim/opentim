package org.evolvis.idm.common.service;

import java.util.Locale;

import org.apache.commons.mail.EmailException;
import org.evolvis.idm.common.exception.ServiceException;

public interface EmailService {

	/**
	 * Sends a password recovery message via the specified client to the specified user. This kind
	 * of message is used to allow a used to reset her/his forgotten password.
	 * 
	 * @param clientHost
	 *            mail client host for sending.
	 * @param locale
	 *            the locale to be used for the message (e.g. 'de', 'en').
	 * @param emailAddress
	 *            the user's email address.
	 * @param firstName
	 *            the user's first name.
	 * @param lastName
	 *            the user's last name.
	 * @param recoveryPassword
	 *            the recovery password.
	 * @throws ServiceException
	 *             if message sending fails.
	 */
	public abstract void sendPassowortRecoveryMail(String clientHost, Locale locale,
			String emailAddress, String name,
			String recoveryPassword) throws EmailException;

	/**
	 * Sends a self registration message via the specified client to the specified user. This kind
	 * of message should verify the mail address used in the registration process.
	 * 
	 * @param clientHost
	 *            mail client host for sending.
	 * @param locale
	 *            the locale to be used for the message (e.g. 'de', 'en').
	 * @param emailAddress
	 *            the user's email address.
	 * @param firstName
	 *            the user's first name.
	 * @param lastName
	 *            the user's last name.
	 * @param activationLink
	 *            link to activate the new account.
	 * @throws ServiceException
	 *             if message sending fails.
	 */
	public abstract void sendSelfRegistrationMail(String clientHost, Locale locale,
			String emailAddress, String name,
			String activationLink) throws EmailException;

	/**
	 * Sends an admin registration message via the specified client to the specified user. This kind
	 * of message is necessary after a admin user created a new user.
	 * 
	 * @param clientHost
	 *            mail client host for sending.
	 * @param locale
	 *            the locale to be used for the message (e.g. 'de', 'en').
	 * @param emailAddress
	 *            the user's email address.
	 * @param name
	 *            the user's name (for addressing).
	 * @param activationLink
	 *            link to activate the new account.
	 * @throws ServiceException
	 *             if message sending fails.
	 */
	public abstract void sendAdminRegistrationMail(String clientHost, Locale locale,
			String emailAddress, String name,
			String activationLink) throws EmailException;
	
	/**
	 * Sends an email change request to the new email address.
	 * 
	 * @param clientHost
	 *            mail client host for sending.
	 * @param locale
	 *            the locale to be used for the message (e.g. 'de', 'en').
	 * @param newEmailAddress
	 *            the user's new email address.
	 * @param name
	 *            the user's name (for addressing).
	 * @param activationLink
	 *            link to activate the new email.
	 * @throws ServiceException
	 *             if message sending fails.
	 */
	public abstract void sendEmailChangeRequestMail(String clientHost, Locale locale,
			String newEmailAddress, String name,
			String activationLink) throws EmailException;
	
	/**
	 * Sends a notification to an email address that is already in use to inform
	 * the owner that an email address change to this address has been requested but is not possible.
	 * 
	 * @param clientHost
	 *            mail client host for sending.
	 * @param locale
	 *            the locale to be used for the message (e.g. 'de', 'en').
	 * @param emailAddress
	 *            the user's new email address.
	 * @param name
	 *            the user's name (for addressing).
	 * @param activationLink
	 *            link to activate the new email.
	 * @throws ServiceException
	 *             if message sending fails.
	 */
	public abstract void sendEmailChangeRequestFailureMail(String clientHost, Locale locale,
			String emailAddress, String name) throws EmailException;
}
