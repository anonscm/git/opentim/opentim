package org.evolvis.idm.common.service;

import java.io.IOException;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.util.SynchronizingRestClientUtil;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.security.SecurityContextHolder;
import org.evolvis.opensso.rest.RestClient;
import org.evolvis.opensso.rest.constant.UserAttribute;
import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.evolvis.opensso.rest.service.Read;
import org.evolvis.opensso.rest.service.Search;
import org.evolvis.opensso.rest.service.UpdateUser;

public class AuthenticationServiceImpl implements AuthenticationService {

    protected final Log logger = LogFactory.getLog(getClass());

    @Override
    public String login(String username, String password) {
        RestClient client = getRestClient();
        String realm = SecurityContextHolder.getActiveClientRealm();

        String token = null;
        try {
            AuthenticateUser authenticateUser = client.callService(new AuthenticateUser(realm, username, password));
            if (authenticateUser.success() && null != authenticateUser.getToken()) {
                token = authenticateUser.getToken();
            }
            else {
                logger.debug("username() failed with authentication-error=" + authenticateUser.getError());
            }
        }
        catch (IOException ioe) {
            logger.error("username() failed with IOException", ioe);
        }
        return token;
    }

    @Override
    public String onetimeLogin(String username, String password) {

        // TODO: Check Permission

        RestClient client = getRestClient();
        String realm = SecurityContextHolder.getUserClientRealm();

        try {
            Search search = client.callService(new Search(realm).setAttr(UserAttribute.UID, username).setAttr(
                    UserAttribute.PASSWORD_UUID, password));

            // check if success
            if (search.success() && !search.getUids().isEmpty()) {

                // get userUid
                String userUid = search.getUid();

                // update user
                UpdateUser updateUser = client.callService(new UpdateUser(realm, userUid).setAttr(UserAttribute.PASSWORD_UUID, null));

                return updateUser.success() ? getUUIDForUsername(userUid) : null;
            }
            else {
                return null;
            }
        }
        catch (IOException ioe) {
            logger.error("onetimeusername() failed with IOException", ioe);
            return null;
        }
    }

    @Override
    public String createOnetimePassword(String username) {

        // TODO: Check Permission

        RestClient restClient = getRestClient();

        try {
            // create & set recovery password
        	// try to create password until password is policy compliant
        	String recoveryPassword = null;
        	String passwordPattern = SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_SECURITY_PASSWORT_PATTERN);
        	
        	for (int i = 0; i < 10; i++) {
        		recoveryPassword = RandomStringUtils.random(8, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!§$%&/=?+-#");
        		recoveryPassword += RandomStringUtils.random(1, "0123456789");
        		recoveryPassword += RandomStringUtils.random(1, "!§$%&/=?+-#");
    	        if (passwordPattern.isEmpty() || recoveryPassword.matches(passwordPattern)) 
    	        	break;    	        	
        	}
        	
        	if (recoveryPassword == null) {
        		logger.error("Tried to create 10 random passwords but could not reach client's password policy. Using alphanumeric random password with length 10.");
        		recoveryPassword = RandomStringUtils.randomAlphanumeric(10);
        	}
             
            UpdateUser updateUser = restClient.callService(new UpdateUser(SecurityContextHolder.getUserClientRealm(), username).setAttr(
                    UserAttribute.PASSWORD_UUID, recoveryPassword));

            // check success
            if (updateUser.success()) {
                return recoveryPassword;
            }
            else {
                if (logger.isWarnEnabled()) {
                    logger.warn("createOnetimePassword() failed for username=" + username + ", restclient error: " + updateUser.getError());
                }
            }

        }
        catch (Exception e) {
            logger.error("createOnetimePassword() failed, username=" + username, e);
        }

        return null;
    }

    @Override
    public boolean updatePassword(String username, String password) {

    	String realm = SecurityContextHolder.getUserClientRealm();
    	
    	try {
    		logger.debug("Updating passwort for username="+username+".");
    		    	
            UpdateUser updateUser = getRestClient().callService(new UpdateUser(realm, username).setAttr(UserAttribute.PASSWORD,
                    password));

            if (!updateUser.success()) {
                return false;
            }
        }
        catch (IOException e) {
            logger.error("updatePasswort() failed, realm=" + realm + ", username=" + username, e);
            return false;
        }
        return true;
    }

	@Override
	public String getUUIDForRegistrationKey(String registrationKey) {
		Search search = null;
		try {
			search = getRestClient().callService(new Search(SecurityContextHolder.getActiveClientRealm()).setAttr(UserAttribute.REGISTER_UUID, registrationKey));
			
	        if (search.success() && search.getUids().size() == 1) {
	        	Read readAttributes = getRestClient().callService(new Read(SecurityContextHolder.getActiveClientRealm(), search.getUid()));
		        String uuid = readAttributes.getAttributeValue(UserAttribute.USER_UUID);
		        return uuid;
	        }
		} catch (Exception e) {
			logger.error("getUsernameForRegistrationKey() failed, realm=" + SecurityContextHolder.getActiveClientRealm() + ", registrationKey=" + registrationKey, e);
		}
		return null;
	}
	
	@Override
	public String getUUIDForUsername(String username) {
		Search search = null;
		try {
			search = getRestClient().callService(new Search(SecurityContextHolder.getActiveClientRealm()).setAttr(UserAttribute.UID, username));
			
	        if (search.success() && search.getUids().size() == 1) {
	        	Read readAttributes = getRestClient().callService(new Read(SecurityContextHolder.getActiveClientRealm(), search.getUid()));
		        String uuid = readAttributes.getAttributeValue(UserAttribute.USER_UUID);
		        return uuid;
	        }
		} catch (Exception e) {
			logger.error("getUUIDForUsername() failed, realm=" + SecurityContextHolder.getActiveClientRealm() + ", username=" + username, e);
		}
		return null;
	}

	private RestClient getRestClient() {
		ClientPropertyMap clientPropertyMap = SecurityContextHolder.getClientPropertyMap();
		return SynchronizingRestClientUtil.getClient(clientPropertyMap);
	}
}
