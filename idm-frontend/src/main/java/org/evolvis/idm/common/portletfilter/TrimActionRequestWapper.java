package org.evolvis.idm.common.portletfilter;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.filter.ActionRequestWrapper;

public class TrimActionRequestWapper extends ActionRequestWrapper {

	public TrimActionRequestWapper(ActionRequest request) {
		super(request);
	}
	
	@Override
	public String getParameter(String name) {
		String returnString = super.getParameter(name);
		TrimRequestParamsPortletFilter.trim(name, returnString);
		return returnString;
	}
	
	@Override
	public String[] getParameterValues(String name) {
		String returnStrings[] = super.getParameterValues(name);
		TrimRequestParamsPortletFilter.trim(name, returnStrings);
		return returnStrings;
	}
	
	@Override
	public Map<String, String[]> getParameterMap() {
		Map<String, String[]> returnStrings = super.getParameterMap();
		TrimRequestParamsPortletFilter.trim(returnStrings);
		return returnStrings;
	}
	
	@Override
	public Map<String, String[]> getPrivateParameterMap() {
		Map<String, String[]> returnStrings = super.getPrivateParameterMap();
		TrimRequestParamsPortletFilter.trim(returnStrings);
		return returnStrings;
	}
	
	@Override
	public Map<String, String[]> getPublicParameterMap() {
		Map<String, String[]> returnStrings = super.getPublicParameterMap();
		TrimRequestParamsPortletFilter.trim(returnStrings);
		return returnStrings;
	}
}
