package org.evolvis.idm.common.validation;

import java.lang.annotation.Annotation;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.MessageInterpolator;

import org.hibernate.validator.engine.MessageInterpolatorContext;
import org.hibernate.validator.engine.ResourceBundleMessageInterpolator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * 
 * TODO jneuma comment
 * @author Jens Neumaier
 * 
 */
public class JSR303Validator extends LocalValidatorFactoryBean {
	
	private static final String EMPTY_STRING = "";

	protected MessageInterpolator messageInterpolator = new ResourceBundleMessageInterpolator();

	@Autowired
	protected MessageSource resources;

	@Override
	public void validate(final Object target, final Errors errors) {
		// copied original code from super class
		// only redirected "validate"-call to superclass and changed message resolving strategy

		Set<ConstraintViolation<Object>> result = validate(target);

		for (ConstraintViolation<Object> violation : result) {
			String field = violation.getPropertyPath().toString();
			FieldError fieldError = errors.getFieldError(field);

			if (fieldError == null || !fieldError.isBindingFailure()) {

				/*
				 * Trying to find a accurate violation message using the
				 * following custom strategy:
				 * 
				 * 1. Search for message key "originalTemplate.fieldPathFirstChild.rootBeanClassSimpleName" 
				 * 2. Search for message key "originalTemplate.fieldPathLastChild.leafBeanClassSimpleName"
				 * 3. Search for message key "originalTemplate.fieldPathFirstChild" 
				 * 4. Use original message template
				 * 
				 * The root bean represents the root bean that is validated
				 * while the leaf been represents the child bean the constraint
				 * violation has been triggered on.
				 * 
				 * The highest priority message key found will be interpolated using the spring locale holder
				 * and the hibernate resource interpolator.
				 * 
				 */
				
				String customMessageTemplate = null;

				// TODO jneuma comment additional variables mechanism
				String originalTemplate = violation.getMessageTemplate();

				String[] originalTemplateParts = originalTemplate.split("//");
				originalTemplate = originalTemplateParts[0];
				
				if (violation.getPropertyPath() != null) {
					// resolving key parts
					String fieldPathFull = violation.getPropertyPath().toString();
					String rootBeanClassSimpleName = violation.getRootBeanClass().getSimpleName();
					String leafBeanClassSimpleName = violation.getLeafBean().getClass().getSimpleName();
					String fieldPathFirstChild = fieldPathFull;
					String fieldPathLastChild = null;
					if (fieldPathFull.indexOf('.') != -1) {
						fieldPathFirstChild = fieldPathFull.substring(0, fieldPathFull.indexOf('.'));
						fieldPathLastChild = fieldPathFull.substring(fieldPathFull.lastIndexOf('.') + 1);
					}

					// building message keys in reverse order ...
					// storing message keys in a String array where 0 is the highest priority
					String[] messageKeys = new String[4]; 
					StringBuffer messageKeyBuffer = new StringBuffer(2 + originalTemplate.length() + fieldPathFirstChild.length() + rootBeanClassSimpleName.length());

					// building "originalTemplate.fieldPathFull"
					messageKeyBuffer.append(originalTemplate).append('.').append(fieldPathFirstChild);
					// assinging value 2 with priority 3 as "3 - 1" (and so on) to avoid mistakes
					messageKeys[3 - 1] = messageKeyBuffer.toString();

					// building "originalTemplate.fieldPathFull.rootBeanClassSimpleName"
					messageKeyBuffer.append('.').append(rootBeanClassSimpleName);
					messageKeys[1 - 1] = messageKeyBuffer.toString();

					// building "originalTemplate.fieldPathLastChild.leafBeanClassSimpleName"
					if (fieldPathLastChild != null) {
						messageKeyBuffer = new StringBuffer(2 + originalTemplate.length() + fieldPathLastChild.length() + leafBeanClassSimpleName.length());
						messageKeyBuffer.append(originalTemplate).append('.').append(fieldPathLastChild).append('.').append(leafBeanClassSimpleName);
						messageKeys[2 - 1] = messageKeyBuffer.toString();
					}
					
					// saving default message as priority 4 key 
					messageKeys[4 - 1] = originalTemplate;

					// try resolving messages in priority order
					for (int priority = 0; priority < messageKeys.length && customMessageTemplate == null; priority++) {
						if (messageKeys[priority] != null) {
							// search for messages in resources
							String foundMessage = resources.getMessage(messageKeys[priority], null, EMPTY_STRING, LocaleContextHolder.getLocale());
							// test if message has been found (if not the default EMPTY_STRING is returned)
							if (!foundMessage.equals(EMPTY_STRING))
								customMessageTemplate = foundMessage;
						}
					}
				}
				
				// interpolate originalTemplate if no customMessageTemplate could be resolved
				if (customMessageTemplate == null)
					customMessageTemplate = originalTemplate;
				
				// replace variables using hibernate message interpolator if message could be resolved
				String resolvedMessage = messageInterpolator.interpolate(customMessageTemplate, 
							new MessageInterpolatorContext( buildCustomConstraintDescriptor(violation.getConstraintDescriptor(), originalTemplateParts), target));
				
				errors.rejectValue(field, violation.getConstraintDescriptor().getAnnotation().annotationType().getSimpleName(), getArgumentsForConstraint(errors.getObjectName(), field, violation.getConstraintDescriptor()), resolvedMessage);
			}
		}
	}
	
	public <T extends Annotation> javax.validation.metadata.ConstraintDescriptor<T> buildCustomConstraintDescriptor(javax.validation.metadata.ConstraintDescriptor<T> currentConstraintDescriptor, String[] originalTemplateParts) {
		ContraintDescriptor<T> customContraintDescriptor = new ContraintDescriptor<T>(currentConstraintDescriptor);
		
		for (int i = 1; i < originalTemplateParts.length; i++) {
			String[] attributeParts = originalTemplateParts[i].split("==");
			if (attributeParts.length == 2)
				customContraintDescriptor.addAttribute(attributeParts[0], attributeParts[1]);
		}
		
		return customContraintDescriptor;
	}
}
