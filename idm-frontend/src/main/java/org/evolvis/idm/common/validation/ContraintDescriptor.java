package org.evolvis.idm.common.validation;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.Payload;

public class ContraintDescriptor<T extends Annotation> implements javax.validation.metadata.ConstraintDescriptor<T> {

	private static final long serialVersionUID = -1908409717571607119L;
	
	javax.validation.metadata.ConstraintDescriptor<T> parentConstraintDescriptor;
	
	Map<String, Object> additionalAttributes = new HashMap<String, Object>(4);
	
	public ContraintDescriptor(javax.validation.metadata.ConstraintDescriptor<T> parentConstraintDescriptor) {
		this.parentConstraintDescriptor = parentConstraintDescriptor;
	}

	@Override
	public T getAnnotation() {
		return parentConstraintDescriptor.getAnnotation();
	}

	@Override
	public Map<String, Object> getAttributes() {
		Map<String, Object> originalAttributes = parentConstraintDescriptor.getAttributes();
		Map<String, Object> mergedAttributes = new HashMap<String, Object>(originalAttributes.size() + additionalAttributes.size());
		mergedAttributes.putAll(originalAttributes);
		mergedAttributes.putAll(additionalAttributes);
		return mergedAttributes;
	}
	
	public void addAttribute(String key, Object value) {
		additionalAttributes.put(key, value);
	}

	@Override
	public Set<javax.validation.metadata.ConstraintDescriptor<?>> getComposingConstraints() {
		return parentConstraintDescriptor.getComposingConstraints();
	}

	@Override
	public List<Class<? extends ConstraintValidator<T, ?>>> getConstraintValidatorClasses() {
		return parentConstraintDescriptor.getConstraintValidatorClasses();
	}

	@Override
	public Set<Class<?>> getGroups() {
		return parentConstraintDescriptor.getGroups();
	}

	@Override
	public Set<Class<? extends Payload>> getPayload() {
		return parentConstraintDescriptor.getPayload();
	}

	@Override
	public boolean isReportAsSingleViolation() {
		return parentConstraintDescriptor.isReportAsSingleViolation();
	}

}
