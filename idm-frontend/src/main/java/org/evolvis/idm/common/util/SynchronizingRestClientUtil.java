package org.evolvis.idm.common.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.opensso.rest.RestClient;
import org.evolvis.opensso.rest.constant.RestConfig;

public class SynchronizingRestClientUtil {
	
	private static final Log logger = LogFactory.getLog(SynchronizingRestClientUtil.class);

	private static final Map<String, RestClient> restClientCache = new HashMap<String, RestClient>();

	public static RestClient getClient(ClientPropertyMap clientPropertyMap) {
		if (restClientCache.get(clientPropertyMap.getClientName()) == null) {
			// TODO jneuma find a better solution for OpenSSO property sync
			if (clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_SERVICEURL) == null
					|| !clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_SERVICEURL).equals(
							RestConfig.SERVICE_URL.trim())
					|| clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_ADMINLOGIN) == null
					|| !clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_ADMINLOGIN).equals(
							RestConfig.SERVICE_LOGIN.trim())
					|| clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_ADMINPASSWORD) == null
					|| !clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_ADMINPASSWORD).equals(
							RestConfig.SERVICE_PASSWD.trim())) {
				
				clientPropertyMap.put(ClientPropertyMap.KEY_OPENSSO_SERVICEURL, RestConfig.SERVICE_URL.trim());
				clientPropertyMap.put(ClientPropertyMap.KEY_OPENSSO_ADMINLOGIN, RestConfig.SERVICE_LOGIN.trim());
				clientPropertyMap.put(ClientPropertyMap.KEY_OPENSSO_ADMINPASSWORD, RestConfig.SERVICE_PASSWD.trim());

				try {
					BackendServiceUtil.getClientManagementService().setClientPropertyMap(
							clientPropertyMap.getClientName(), clientPropertyMap);
				} catch (BackendException e) {
					logger.error("Error while synchronizing ClientPropertyMap for clientName="+clientPropertyMap.getClientName()+" with local OpenSSO configuration.", e);
				}
			}
			
			restClientCache.put(clientPropertyMap.getClientName(), RestClient.getInstance().initialize(
					clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_SERVICEURL),
					clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_ADMINLOGIN),
					clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_ADMINPASSWORD)));
		}
		return restClientCache.get(clientPropertyMap.getClientName());
	}
}
