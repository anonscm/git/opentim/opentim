package org.evolvis.idm.common.constant;


public class MessageKeys {

    // error msg keys
    
    public static final String ERROR_EMAIL_ALREADY_USED = "error_email_already_used";
 
    public static final String ERROR_EMAIL_INVALID = "error_email_invalid";
    
    public static final String ERROR_INCORRECT_LOGIN_PASSWD = "error_incorrect_login_passwd";
    
    public static final String ERROR_LOGIN_ALREADY_USED = "error_login_already_used";
    
    public static final String ERROR_UNKNOWN_SYSTEM_ERROR = "error_unknown_system_error";
    
    public static final String ERROR_USER_NOT_AUTHENTICATED = "error_user_not_authenticated";
    
    // success msg keys
    
    public static final String SUCCESS_PW_RECOVERY_DONE = "success_pw_recovery_done";
    
    public static final String SUCCESS_PW_RECOVERY_REQUEST = "success_pw_recovery_request";
}
