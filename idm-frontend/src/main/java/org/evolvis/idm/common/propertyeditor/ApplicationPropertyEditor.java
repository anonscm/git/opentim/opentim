package org.evolvis.idm.common.propertyeditor;

import java.beans.PropertyEditorSupport;

import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Dmytro Mayster
 * 
 * This PropertyEditor is used to effect the conversion between an 
 * Application's Object and an Application's Id. It is used in 
 * SpringMVC controllers.
 *
 */

@Component
public class ApplicationPropertyEditor extends PropertyEditorSupport {

	@Autowired
	protected ApplicationManagement applicationManagement;

	@Override
	public void setAsText(String idString) throws IllegalArgumentException {
		Application application;

		try {
			Long id = Long.parseLong(idString);

			application = applicationManagement.getApplicationById(SecurityContextHolder.getClientName(), id);

		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Not a valid number entered");
		} catch(Exception e) {
			throw new IllegalArgumentException("Not a valid id entered: " + idString);
		}

		this.setValue(application);
	}

	@Override
	public String getAsText() {
		Long id = new Long(-1);

		Application application = (Application)this.getValue();

		if (application != null) {
			id = application.getId();
		}

		return id.toString();
	}

}
