package org.evolvis.idm.common.util;

import org.evolvis.idm.common.constant.PortletConfig;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.identity.privatedata.service.PrivateDataConfiguration;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement;
import org.evolvis.idm.synchronization.service.DirectoryServiceSynchronization;
import org.evolvis.idm.ws.InternalWsClientFactory;

public class BackendServiceUtil {

	public static AccountManagement getAccountManagementService() {
		return InternalWsClientFactory.getInstance().getAccountManagementService(
				PortletConfig.IDM_BACKEND_ENDPOINT);
	}

	public static ApplicationManagement getApplicationManagementService() {
		return InternalWsClientFactory.getInstance().getApplicationManagementService(
				PortletConfig.IDM_BACKEND_ENDPOINT);
	}

	public static ClientManagement getClientManagementService() {
		return InternalWsClientFactory.getInstance().getClientManagementService(
				PortletConfig.IDM_BACKEND_ENDPOINT);
	}

	public static DirectoryServiceSynchronization getDirectoryServiceSynchronizationService() {
		return InternalWsClientFactory.getInstance().getDirectoryServiceSynchronizationService(
				PortletConfig.IDM_BACKEND_ENDPOINT);
	}

	public static GroupAndRoleManagement getGroupAndRoleManagementService() {
		return InternalWsClientFactory.getInstance().getGroupAndRoleManagementService(
				PortletConfig.IDM_BACKEND_ENDPOINT);
	}

	public static InternalRoleManagement getInternalRoleManagementService() {
		return InternalWsClientFactory.getInstance().getInternalRoleManagementService(
				PortletConfig.IDM_BACKEND_ENDPOINT);
	}

	public static InternalRoleScopeManagement getInternalRoleScopeManagementService() {
		return InternalWsClientFactory.getInstance().getInternalRoleScopeManagementService(
				PortletConfig.IDM_BACKEND_ENDPOINT);
	}

	public static OrganizationalUnitManagement getOrganisationalUnitManagementService() {
		return InternalWsClientFactory.getInstance().getOrganizationalUnitManagementService(
				PortletConfig.IDM_BACKEND_ENDPOINT);
	}

	public static PrivateDataConfiguration getPrivateDataConfigurationService() {
		return InternalWsClientFactory.getInstance().getPrivateDataConfigurationService(
				PortletConfig.IDM_BACKEND_ENDPOINT);
	}

	public static PrivateDataManagement getPrivateDataManagementService() {
		return InternalWsClientFactory.getInstance().getPrivateDataManagementService(
				PortletConfig.IDM_BACKEND_ENDPOINT);
	}
	
	public static UserManagement getUserManagementService() {
		return InternalWsClientFactory.getInstance().getUserManagementService(
				PortletConfig.IDM_BACKEND_ENDPOINT);
	}
}
