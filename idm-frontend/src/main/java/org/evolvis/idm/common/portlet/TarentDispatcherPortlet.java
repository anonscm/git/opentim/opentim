package org.evolvis.idm.common.portlet;

import java.io.IOException;
import java.io.InputStream;
import java.net.FileNameMap;
import java.net.URLConnection;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.portlet.DispatcherPortlet;

import com.liferay.portal.service.UserLocalServiceUtil;

public class TarentDispatcherPortlet extends DispatcherPortlet {

	FileNameMap fileNameMap = URLConnection.getFileNameMap();

	@Override
	public void serveResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {

		if (logger.isDebugEnabled()) {
			logger.debug("resourceID=" + request.getResourceID());
		}

		String resourceID = request.getResourceID();

		if (resourceID != null && resourceID.equals("downloadAttributeFile")) {
			sendAttributeFile(request, response);
		} else {
			final InputStream inputStream = getClass().getResourceAsStream(resourceID);
			if (null != inputStream) {
				response.setContentType(fileNameMap.getContentTypeFor(request.getResourceID()));
				FileCopyUtils.copy(inputStream, response.getPortletOutputStream());
			}
		}
	}

	private void sendAttributeFile(ResourceRequest request, ResourceResponse response) {
		try {
			String uuid = UserLocalServiceUtil.getUser(Long.valueOf(request.getRemoteUser())).getUuid();
			
			if (uuid.equals(SecurityContextHolder.getUUID())) {
				Long valueId = Long.valueOf(request.getParameter("valueId"));
				
				PrivateDataManagement privateDataManagement = (PrivateDataManagement) getPortletApplicationContext()
						.getBean("PrivateDataManagement");
				Value value = privateDataManagement.getValueWithBinaryAttachmentById(uuid, valueId);
				String fileName = value.getValue();
				byte[] byteValue = value.getByteValue();
				if(byteValue != null) {
					response.setContentType(fileNameMap.getContentTypeFor(fileName));
					response.setContentLength(byteValue.length);
					response.setProperty("Content-disposition", "attachment; filename=\"" + fileName + "\"");
					FileCopyUtils.copy(byteValue, response.getPortletOutputStream());
					response.getPortletOutputStream().flush();
					response.getPortletOutputStream().close();
				} else {
					response.getWriter().println("<html><body>File could not be downloaded!</body></html>"); // TODO i18n message
				}
			} else {
				response.getWriter().println("<html><body>Access denied ("+ uuid +" != "+ SecurityContextHolder.getUUID() +")</body></html>");
				// TODO throw access denied exception
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
