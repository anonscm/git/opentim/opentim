package org.evolvis.idm.common.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.model.Company;
import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.service.persistence.CompanyUtil;

public class TenantResolver {
	
	private static final Log logger = LogFactory.getLog(TenantResolver.class);
	
	public static final String PORTAL_EXT_PROPERTY_KEY_IDM_TENANT = "tarent.idm.activeTenant";
	
	public static String getActiveTenantName() throws Exception {
		// get active tenant from portal-*.properties company property
		String propertyConfigTenantName = PropsUtil.get(PORTAL_EXT_PROPERTY_KEY_IDM_TENANT);
		
		if (propertyConfigTenantName != null) {
			logger.debug("Retrieved active tenant for user from portal properties: "+propertyConfigTenantName);
			return propertyConfigTenantName;
		}
		
		// fallback to liferay web id configuration
		Company company = CompanyUtil.findByPrimaryKey(CompanyThreadLocal.getCompanyId());
		String liferayWebIdTenantName = company.getWebId();
		
		logger.debug("Retrieved active tenant for user from liferay company web id: "+liferayWebIdTenantName);
		return liferayWebIdTenantName;
	}
	
	public static String getActiveTenantSSORealm() throws Exception {
		return "/" + getActiveTenantName();
	}

}
