package org.evolvis.idm.common.exception;

public class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 64227538513770395L;

	private final String errorCode;
	
	public ServiceException(final String error) {
		this.errorCode = error;
	}
	
	public String getErrorCode() {
		return errorCode;
	}
}
