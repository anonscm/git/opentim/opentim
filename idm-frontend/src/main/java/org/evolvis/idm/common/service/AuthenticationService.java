package org.evolvis.idm.common.service;


public interface AuthenticationService {

	/**
	 * username against IdM
	 * @return SecurityToken or null
	 */
	public abstract String login(String username, String password);

	/**
	 * Onetime username against IdM. If successful, password will be reset.
	 * @return UUID of the user or null
	 */
	public abstract String onetimeLogin(String username, String password);
	
	/**
	 * Create a onetime password for the given username
	 * @return created password
	 */
	public abstract String createOnetimePassword(String username);
	
	/**
	 * Set the new password for given user
	 * @return boolean succes
	 */
	public abstract boolean updatePassword(String username, String password);
	
	public abstract String getUUIDForRegistrationKey(String registrationKey);
	
	public abstract String getUUIDForUsername(String username);
}
