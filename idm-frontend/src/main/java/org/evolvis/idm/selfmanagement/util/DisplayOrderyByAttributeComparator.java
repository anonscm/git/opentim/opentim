package org.evolvis.idm.selfmanagement.util;

import java.util.Comparator;

import org.evolvis.idm.common.model.AbstractBean;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;


/**
 * Provides a method to compare {@link AttributeGroup} and {@link Attribute} 
 * by <code>displayOrder</code>. {@link ValueSet} and {@link Value} are compared on 
 * basis of the <code>displayOrder</code> of their {@link AttributeGroup} and {@link Attribute}.  
 * 
 * @author Dmytro Mayster
 *
 */
public class DisplayOrderyByAttributeComparator implements Comparator<AbstractBean> {

	public int compare(AbstractBean arg0, AbstractBean arg1) {
		if (arg0 instanceof AttributeGroup && ((AttributeGroup) arg0).getDisplayOrder() != null && ((AttributeGroup) arg1).getDisplayOrder() != null) {
			if (((AttributeGroup) arg0).getDisplayOrder() != null && ((AttributeGroup) arg1).getDisplayOrder() != null)
				return ((AttributeGroup) arg0).getDisplayOrder() - ((AttributeGroup) arg1).getDisplayOrder();
			else if (((AttributeGroup) arg0).getDisplayOrder() == null)
				return ((AttributeGroup) arg1).getDisplayOrder() == null ? 0 : -1;
			else
				return 1;
		}
		else if (arg0 instanceof Attribute) {
			if (((Attribute) arg0).getDisplayOrder() != null && ((Attribute) arg1).getDisplayOrder() != null)
				return ((Attribute) arg0).getDisplayOrder() - ((Attribute) arg1).getDisplayOrder();
			else if (((Attribute) arg0).getDisplayOrder() == null)
				return ((Attribute) arg1).getDisplayOrder() == null ? 0 : -1;
			else
				return 1;
		}
		else if (arg0 instanceof ValueSet) {
			if (((ValueSet) arg0).getAttributeGroup().getDisplayOrder() != null && ((ValueSet) arg1).getAttributeGroup().getDisplayOrder() != null)
				return ((ValueSet) arg0).getAttributeGroup().getDisplayOrder() - ((ValueSet) arg1).getAttributeGroup().getDisplayOrder();
			else if (((ValueSet) arg0).getAttributeGroup().getDisplayOrder() == null)
				return ((ValueSet) arg1).getAttributeGroup().getDisplayOrder() == null ? 0 : -1;
			else
				return 1;
		}
		else if (arg0 instanceof Value) {
			Value value1 = (Value) arg0;
			Value value2 = (Value) arg1;
			if (value1.getSortOrder() != null && value2.getSortOrder() != null)
				return value1.getSortOrder() - value2.getSortOrder();
			else if (value1.getAttribute().getDisplayOrder() != null && value2.getAttribute().getDisplayOrder() != null)
				return value1.getAttribute().getDisplayOrder() - value2.getAttribute().getDisplayOrder();
			else if (value1.getAttribute().getDisplayOrder() == null)
				return value2.getAttribute().getDisplayOrder() == null ? 0 : -1;
			else
				return 1;
		}
		return 0;
	}
}
