package org.evolvis.idm.selfmanagement.controller;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.mail.EmailException;
import org.evolvis.idm.common.constant.PortletConfig;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.service.EmailService;
import org.evolvis.idm.common.util.BackendServiceUtil;
import org.evolvis.idm.employeeactivation.controller.EmployeeActivation;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.security.SecurityContextHolder;
import org.evolvis.idm.selfmanagement.command.RequestLoginChangeCom;
import org.evolvis.idm.selfmanagement.constant.UserNavPoints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

/**
 * Manages an email change
 * 
 * @author Jens Neumaier
 * 
 */
@Controller
@RequestMapping(value = "view", params = "ctx=RequestLoginChange")
@SessionAttributes( { RequestLoginChange.COMMAND })
public class RequestLoginChange {
	public static final String COMMAND = "command";

	@Autowired
	protected Validator validator;

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected UserManagement userManagement;
	
	@Autowired
    protected EmailService emailService;

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = "action=requestLoginChange")
	public void handleChangeLoginAction(ActionRequest actionRequest, ActionResponse actionResponse,
			@ModelAttribute(COMMAND) @Valid RequestLoginChangeCom command, Errors errors) {

		/*
		 * return if there are any errors
		 */
		if (errors.hasErrors()) return;
		
		// TODO jneuma comment & externalize user existence check
		
		try {
			boolean showFailureMessages = Boolean.valueOf(SecurityContextHolder.getClientPropertyMap().get(ClientPropertyMap.KEY_SECURITY_ALLOWEMAILEXISTENCEHINTS_ENABLED));
			
			String activationKey = null;
			String errorCode = null;
			
			User user = userManagement.getUserByUuid(SecurityContextHolder.getUUID());
			
			if (userManagement.isUserExisting(SecurityContextHolder.getClientName(), command.getLogin())) {
				errorCode = "error.user.duplicateUser";
			}
			Client employeeClient = EmployeeActivation.getEmployeeClient(BackendServiceUtil
					.getClientManagementService().getClients(null).getResultList());
			if (employeeClient != null
					&& userManagement.isUserExisting(employeeClient.getName(), command.getLogin())) {
				errorCode = "error.user.duplicateUserEmployee";
			}
			Client citizenClient = EmployeeActivation.getCitizenClient(BackendServiceUtil
					.getClientManagementService().getClients(null).getResultList());
			if (citizenClient != null
					&& userManagement.isUserExisting(citizenClient.getName(), command.getLogin())) {
				errorCode = "error.user.duplicateUserCitizen";
			}
			
			activationKey = userManagement.requestUsernameChange(SecurityContextHolder.getUUID(), command.getLogin());
		
		
			if (errorCode != null) {
				if (showFailureMessages) {
					errors.rejectValue("login", errorCode);
					return;
				}
				else {
					// TODO fix wrong user name!!!
					emailService.sendEmailChangeRequestFailureMail(actionRequest.getServerName(), actionRequest.getLocale(), command.getLogin(), 
								user.getFirstname() + " " + user.getLastname());
					actionRequest.setAttribute("msg_success", resources.getMessage(
							"success.login.requestChange", null, actionRequest.getLocale()));
					return;
				}
			}
			
			String registerKeyB64 = new String(Base64.encodeBase64(activationKey.getBytes()));
			String activationLink = command.getActivationURL();
			activationLink = activationLink.replaceAll(PortletConfig.REGISTER_CONFIRM_VALUE_PLACEHOLDER, registerKeyB64);
			
			emailService.sendEmailChangeRequestMail(actionRequest.getServerName(), actionRequest.getLocale(), command.getLogin(), 
								user.getFirstname() + " " + user.getLastname(), activationLink);
					
			actionRequest.setAttribute("msg_success", resources.getMessage(
						"success.login.requestChange", null, actionRequest.getLocale()));
			
		} catch (BackendException e) {
			errors.reject("error_unknown_system_error");
			return;
		} catch (EmailException e) {
			errors.reject("error.emailSendingFailedLoginChange");
			return;
		}
	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping
	public ModelAndView handleRenderRequest(RenderRequest request, RenderResponse response) {

		RequestLoginChangeCom requestLoginChangeCom = new RequestLoginChangeCom();
		ModelAndView modelAndView = new ModelAndView("RequestLoginChange", COMMAND, requestLoginChangeCom);
		modelAndView.addObject("navPoints", UserNavPoints.getAll());
		
		// generate activation url
		PortletURL activationURL = response.createActionURL();
		activationURL.setParameter("ctx", "ActivateLoginChange");
		activationURL.setParameter("action", "activateLoginChange");
		activationURL.setParameter(PortletConfig.REGISTER_CONFIRM_KEY, PortletConfig.REGISTER_CONFIRM_VALUE_PLACEHOLDER);

		requestLoginChangeCom.setActivationURL(activationURL.toString());

		return modelAndView;
	}

}
