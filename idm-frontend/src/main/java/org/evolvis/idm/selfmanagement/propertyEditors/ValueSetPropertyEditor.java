package org.evolvis.idm.selfmanagement.propertyEditors;

import java.beans.PropertyEditorSupport;

import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.security.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Michael Kutz
 */

@Component
public class ValueSetPropertyEditor extends PropertyEditorSupport {

	@Autowired
	protected PrivateDataManagement privateDataManagement;

	/**
	 * {@inheritDoc} Returns a <code>ValueSet</code> object specified by its ID.
	 * 
	 * @param valueSetId
	 *            the <code>ValueSet</code> object's ID.
	 */
	@Override
	public void setAsText(String valueSetId) throws IllegalArgumentException {
		ValueSet valueSet;

		try {
			/*
			 * get the user object
			 */
			valueSet = privateDataManagement.getValuesById(SecurityContextHolder.getUUID(),
					Long.valueOf(valueSetId));
		} catch (Exception e) {
			/*
			 * throw exception if no valueSet was returned for the given ID
			 */
			throw new IllegalArgumentException("No valueSet found for ID " + valueSetId);
		}

		this.setValue(valueSet);
	}

	/**
	 * {@inheritDoc} Returns the ID of the ValueSet or null.
	 */
	@Override
	public String getAsText() {
		ValueSet valueSet = (ValueSet) this.getValue();
		if (valueSet != null) {
			return Long.toString(valueSet.getId());
		}
		return null;
	}

}
