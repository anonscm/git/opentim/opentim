package org.evolvis.idm.selfmanagement.propertyEditors;

import java.beans.PropertyEditorSupport;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;

import org.evolvis.idm.selfmanagement.constant.DatePatterns;
import org.springframework.stereotype.Component;

/**
 * This PropertyEditor is used to effect the conversion between a form and a
 * command object. It is used in SpringMVC controllers.
 * 
 * @author Dmytro Mayster
 * 
 */

@Component
public class DatePropertyEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) {
		if (text != null && !text.isEmpty()) {
			SimpleDateFormat longDateFormat = new SimpleDateFormat(DatePatterns.LONG_DATE);
			SimpleDateFormat shortDateFormat = new SimpleDateFormat(DatePatterns.SHORT_DATE);
			// round up the date will be turned off:
			longDateFormat.setLenient(false);
			shortDateFormat.setLenient(false);
			// check if the value is a valid date:
			boolean isValid = false;
			if (text.length() == DatePatterns.LONG_DATE.length() || text.length() == DatePatterns.SHORT_DATE.length()) {
				ParsePosition parsePosition = new ParsePosition(0);
				longDateFormat.parse(text, parsePosition);
				// the entire string 'text' must be parsed without errors
				if (text.length() == DatePatterns.LONG_DATE.length() && parsePosition.getIndex() == text.length()) {
					isValid = true;
				} else {
					parsePosition.setIndex(0);
					shortDateFormat.parse(text, parsePosition);
					if (text.length() == DatePatterns.SHORT_DATE.length() && parsePosition.getIndex() == text.length()) {
						isValid = true;
					}
				}
			}
			
			if(!isValid) {
				throw new IllegalArgumentException("Not a valid date format entered: " + text + ", must be either "
						+ DatePatterns.LONG_DATE + " or " + DatePatterns.SHORT_DATE);
			}
		}

		// the value is a valid date:
		this.setValue(text);
	}

	@Override
	public String getAsText() {
		if(this.getValue() != null)
			return (String)this.getValue();
		
		return "";
	}

}
