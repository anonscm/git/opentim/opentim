package org.evolvis.idm.selfmanagement.constant;

public class DatePatterns {
	public static final String LONG_DATE = "dd.MM.yyyy";
	public static final String SHORT_DATE = "dd.MM.yy";
	public static final String TIME = "HH:mm";
}
