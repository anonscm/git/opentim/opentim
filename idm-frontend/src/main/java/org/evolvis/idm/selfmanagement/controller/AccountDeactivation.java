package org.evolvis.idm.selfmanagement.controller;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.model.NavPoint;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.security.SpringSecurityUser;
import org.evolvis.idm.selfmanagement.constant.UserNavPoints;
import org.evolvis.opensso.rest.constant.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * Manages a deactivation of user's account by user himself
 * 
 * @author Dmytro Mayster
 * 
 */

@Controller
@RequestMapping(value = "view", params = "ctx=AccountDeactivation")
@SessionAttributes( { AccountDeactivation.COMMAND })
public class AccountDeactivation {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected UserManagement userManagement;

	/**
	 * Handles an <code>deactivateAccount</code> action request by invoking the
	 * <code>setAccountStatus</code> of injected {@link UserManagement}.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param command
	 *            Command object to bind errors
	 * @param errors
	 *            Errors object to collect form errors
	 * 
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = { "action=deactivateAccount", "!denied" })
	public void handleConfirmedDeactivateAccountAction(ActionResponse actionResponse,
			@ModelAttribute(COMMAND) Object command, Errors errors) {
		logger.info("handleConfirmedDeactivateAccountAction() entered");

		SpringSecurityUser springSecurityUser = (SpringSecurityUser) org.springframework.security.context.SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		try {
			userManagement.setUserStatus(springSecurityUser.getUuid(), UserStatus.INACTIVE);

			// redirect to loguot:
			actionResponse.sendRedirect("/c/portal/logout");

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.account.delete");
		}

	}

	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = { "action=deactivateAccount", "denied" })
	public void handleDeniedDeactivateAccountAction(final @ModelAttribute(COMMAND) Object command,
			final Errors errors, final ActionRequest request, final ActionResponse response) {
		logger.info("handleDeniedDeactivateAccountAction() entered");
		request.setAttribute("ctx", "AccountSummary");
	}

	/**
	 * Handles a render request and returns a name of a <code>AccountDeactivation</code> view. The
	 * {@link ModelMap} contains following objects: <br/>
	 * <br/>
	 * <i>navPoints</i> List of all {@link NavPoint}.
	 * 
	 * 
	 * @return String The name of the <code>AccountDeactivation</code> view
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping
	public String handleRenderRequest(ModelMap model) {
		// Command object to bind errors
		if (!model.containsKey(COMMAND))
			model.addAttribute(COMMAND, new Object());

		model.addAttribute("navPoints", UserNavPoints.getAll());

		return "AccountDeactivation";
	}

}
