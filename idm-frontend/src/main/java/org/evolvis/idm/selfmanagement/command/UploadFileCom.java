/**
 * 
 */
package org.evolvis.idm.selfmanagement.command;

/**
 * @author Michael Kutz, tarent GmbH
 *
 */
public class UploadFileCom {

	private byte[] file;
	
	public UploadFileCom() {
	}
	
	public UploadFileCom(byte[] file) {
		this.setFile(file);
	}
	
	public void setFile(byte[] file) { 
		this.file = file; 
	} 
	
	public byte[] getFile() { 
		return file; 
	}
	
	/**
	 * @return the file size limit in kB.
	 */
	public int getFileSizeLimit() {
		return file.length / 1024;
	}
	
}
