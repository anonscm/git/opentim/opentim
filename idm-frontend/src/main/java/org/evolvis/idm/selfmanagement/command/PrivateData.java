package org.evolvis.idm.selfmanagement.command;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.ValueSet;

/**
 * Command object used by PrivateDataManagement controller
 * 
 * @author Dmytro Mayster
 *
 */
public class PrivateData {
	
	@Valid
	private List<ValueSet> valueSets;
	
	private byte[] file;

	public PrivateData() {
		new ArrayList<ValueSet>();
	}
	
	public PrivateData(List<ValueSet> valueSets) {
		this.valueSets = valueSets;
	}
	
	public PrivateData(Account account, List<AttributeGroup> attributeGroups) {
		valueSets = new ArrayList<ValueSet>();
		for (AttributeGroup attributeGroup : attributeGroups) {
			valueSets.add(new ValueSet(account, attributeGroup));
		}
	}
	
	public List<ValueSet> getValueSets() {
		return valueSets;
	}

	public void setValueSets(List<ValueSet> valueSets) {
		this.valueSets = valueSets;
	}
	
	public void setFile(byte[] file) {
		this.file = file;
	}
	
	public byte[] getFile() {
		return file;
	}
	
}
