package org.evolvis.idm.selfmanagement.controller;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.constant.PortletConfig;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AuthenticationService;
import org.evolvis.idm.common.service.EmailService;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.loginregister.controller.Login;
import org.evolvis.idm.selfmanagement.command.ActivateLoginChangeCom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

/**
 * Manages an email change
 * 
 * @author Jens Neumaier
 * 
 */
@Controller
@RequestMapping(value = "view", params = "ctx=ActivateLoginChange")
@SessionAttributes( { ActivateLoginChange.COMMAND })
public class ActivateLoginChange {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected Validator validator;

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected UserManagement userManagement;
	
	@Autowired
    protected EmailService emailService;
	
	@Autowired
    protected AuthenticationService authenticationService;

	@RequestMapping(params = "action=activateLoginChange")
	public void handleChangeLoginAction(ActionRequest actionRequest, ActionResponse actionResponse,
			@RequestParam(value = PortletConfig.REGISTER_CONFIRM_KEY) String activationKey) {

		// decode
		activationKey = new String(Base64.decodeBase64(activationKey.getBytes()));

        String uuid = authenticationService.getUUIDForRegistrationKey(activationKey);
		if (uuid == null) {
	        logger.warn("Could not find user for activation key = " + activationKey);
	        
	        // TODO jneuma create new key
	        actionRequest.setAttribute("msg_error", resources.getMessage(
					"error.registration.activation.keyExpired", null, actionRequest.getLocale()));
	        
	        return;
	    }
		
		actionRequest.setAttribute("activationKey", activationKey);
	}
	
	@RequestMapping(params = "action=confirmCredentials")
	public void handleChangeLoginAction(ActionRequest actionRequest, ActionResponse actionResponse,
			@ModelAttribute(COMMAND) @Valid ActivateLoginChangeCom command, Errors errors) {

        String uuid = authenticationService.getUUIDForRegistrationKey(command.getActivationKey());
		if (uuid == null) {
	        logger.warn("Could not find user for activation key = " + command.getActivationKey());
	        
	        // TODO jneuma create new key
	        actionRequest.setAttribute("msg_error", resources.getMessage(
					"error.registration.activation.keyExpired", null, actionRequest.getLocale()));
	        
	        return;
	    }
		
		String newUsername = command.getActivationKey().split(UserManagement.DELIMITER_USERNAME_CHANGE_SPLIT_ACTIVATIONID_EMAIL)[1];
		
		try {
			String ssoToken = userManagement.changeUsername(uuid, newUsername, command.getPassword());
			
			if (actionRequest.getRemoteUser() == null) {
				actionRequest.setAttribute("msg_success", resources.getMessage(
						"success.login.activateChange.autoLogin", null, actionRequest.getLocale()));
			} 
			else {
				actionRequest.setAttribute("msg_success", resources.getMessage(
						"success.login.activateChange.loginActive", null, actionRequest.getLocale()));				
			}
			
			actionRequest.getPortletSession().setAttribute(Login.LIFERAY_SHARED_OPENSSO_TOKEN, ssoToken, PortletSession.APPLICATION_SCOPE);
			actionRequest.setAttribute("ctx", "AccountSummary");
		} catch (BackendException e) {
			if (e.getErrorCode().equals(IllegalRequestException.CODE_INVALID_CREDENTIALS))
				errors.reject("error.login.invalidPassword");
			else
				errors.reject("error_unknown_system_error");
			return;
		}
	}

	@RequestMapping
	public ModelAndView handleRenderRequest(RenderRequest request, RenderResponse response) {
		ActivateLoginChangeCom activateLoginChangeCom = new ActivateLoginChangeCom();
			
		if (request.getAttribute("activationKey") != null)
			activateLoginChangeCom.setActivationKey((String) request.getAttribute("activationKey"));
				
		ModelAndView modelAndView = new ModelAndView("ActivateLoginChange", COMMAND, activateLoginChangeCom);
		
		return modelAndView;
	}

}
