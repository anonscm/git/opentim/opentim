package org.evolvis.idm.selfmanagement.command;

import javax.validation.constraints.Size;

import org.evolvis.idm.common.validation.FieldEquality;

/**
 * Command object used by ChangePassword controller
 * 
 * @author Dmytro Mayster
 *
 */
@FieldEquality(message = "violation.changepassword.equalPasswords", value = {"password", "passwordRepeat"})
public class ChangePasswordCom {

    private String oldPassword;
	
	@Size(min = 8, max = 74, message="violation.changepassword.size")
    private String password;

    private String passwordRepeat;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String newPassword) {
		this.password = newPassword;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

}
