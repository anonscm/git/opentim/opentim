package org.evolvis.idm.selfmanagement.controller;

import org.evolvis.idm.common.model.NavPoint;
import org.evolvis.idm.selfmanagement.constant.UserNavPoints;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Provides an access to a management of user's account 
 * @author Dmytro Mayster
 *
 */
@Controller
@RequestMapping(value = "view", params = "ctx=AccountSummary")
public class AccountSummary {
	
	/**
     * Handles a render request and returns a name of
     * the AccountSummary view. The {@link ModelMap} contains
     * following objects:
     * <br/>
     * <br/><i>navPoints</i> List of all {@link NavPoint}.
     * 
     * @param model the holder of model attributes
     * @return String as name of the AccountSummary view
     */
	@Secured({"ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER"})
	@RequestMapping
	protected String handleRenderRequest(ModelMap model)	{
		model.addAttribute("navPoints", UserNavPoints.getAll());
		return "AccountSummary";
	}
}