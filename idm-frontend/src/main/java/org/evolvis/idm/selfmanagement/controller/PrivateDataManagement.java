package org.evolvis.idm.selfmanagement.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.model.NavPoint;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.security.SecurityContextHolder;
import org.evolvis.idm.selfmanagement.command.PrivateData;
import org.evolvis.idm.selfmanagement.constant.UserNavPoints;
import org.evolvis.idm.selfmanagement.propertyEditors.ValueSetPropertyEditor;
import org.evolvis.idm.selfmanagement.util.DisplayOrderyByAttributeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * Manages a private user data. The command object needs to be up to date with the persistence unit
 * at any time!
 * 
 * @author Michael Kutz
 */
@Controller
@RequestMapping(value = "view", params = "ctx=PrivateDataManagement")
@SessionAttributes(PrivateDataManagement.COMMAND)
public class PrivateDataManagement {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected org.evolvis.idm.identity.privatedata.service.PrivateDataManagement privateDataManagement;

	@Autowired
	protected ValueSetPropertyEditor valueSetPropertyEditor;

	@Autowired
	protected Validator validator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(ValueSet.class, valueSetPropertyEditor);
	}

	/**
	 * @return The {@link NavPoint}s for the SelfManagement portlet navigation.
	 */
	@ModelAttribute("navPoints")
	protected List<NavPoint> getNavPoints() {
		return UserNavPoints.getAll();
	}

	/**
	 * @return A List of {@link ValueSet}s that may be edited in the View. There will be at least
	 *         one {@link ValueSet} for each {@link AttributeGroup} even if there is no
	 *         {@link ValueSet} in the database yet.
	 */
	@ModelAttribute(COMMAND)
	protected PrivateData getPrivateData() {
		List<ValueSet> valueSets = new ArrayList<ValueSet>();
		List<AttributeGroup> attributeGroups = new ArrayList<AttributeGroup>();

		try {
			/*
			 * get already existing ValueSets form the data base
			 */
			valueSets = privateDataManagement.getValues(SecurityContextHolder.getUUID());

			/*
			 * get all AttributeGroups
			 */
			attributeGroups = privateDataManagement.getAttributeGroups(SecurityContextHolder
					.getClientName());

			/*
			 * fill up all already existing ValueSets according to their AttributeGroups
			 */
			for (ValueSet valueSet : valueSets) {
				/*
				 * remove the corresponding AttributeGroup
				 */
				AttributeGroup attributeGroup = valueSet.getAttributeGroup();
				attributeGroups.remove(attributeGroup);

				/*
				 * create missing Values
				 */
				for (Attribute attribute : attributeGroup.getAttributes()) {
					if (valueSet.getValue(attribute) == null) {
						valueSet.addValue(new Value(attribute, null));
					}
				}

				/*
				 * sort the ValueSet's Values
				 */
				Collections.sort(valueSet.getValues(), new DisplayOrderyByAttributeComparator());
			}

			/*
			 * create missing ValueSets
			 */
			Account account = privateDataManagement.getAccount(SecurityContextHolder.getUUID());
			for (AttributeGroup attributeGroup : attributeGroups) {
				ValueSet valueSet = new ValueSet(account, attributeGroup);
				valueSets.add(valueSet);

				/*
				 * sort the ValueSet's Values
				 */
				Collections.sort(valueSet.getValues(), new DisplayOrderyByAttributeComparator());
			}

			/*
			 * sort the ValueSets
			 */
			Collections.sort(valueSets, new DisplayOrderyByAttributeComparator());

		} catch (BackendException e) {
			logger.warn(e);
		}

		return new PrivateData(valueSets);
	}

	/**
	 * Handles an <code>updateAllValueSets</code> action request by updating all {@link ValueSet}s
	 * of the current user.
	 * 
	 * @param valueSets
	 *            a {@link PrivateData} command object containing the updated {@link ValueSet}s.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = "action=updateValueSets")
	public void handleUpdateValueSetsAction(
			@ModelAttribute(COMMAND) @Valid PrivateData privateData, final Errors errors,
			ModelMap model, final ActionRequest request, final ActionResponse response,
			final Locale locale) {
		logger.info("handleUpdateValueSetsAction() entered");
		/*
		 * return if any errors were detected
		 */
		if (errors.hasErrors()) {
			response.setRenderParameter("hasErrors", Boolean.toString(true));
			return;
		}

		/*
		 * persist all ValueSets
		 */
		for (ValueSet valueSet : privateData.getValueSets()) {
			try {
				privateDataManagement.setValues(SecurityContextHolder.getUUID(), valueSet, false);
			} catch (Exception e) {
				errors.reject("error.valueset.update");
				logger.warn(e);
			}
		}

		/*
		 * clear the form after successful submit and set message
		 */
		model.remove(COMMAND);
		model.addAttribute(COMMAND, getPrivateData());
		request.setAttribute("msg_success", resources.getMessage("success.valueset.update", null,
				locale));
	}

	/**
	 * Handles the <code>addValueSet</code> action request by creating a new {@link ValueSet} based
	 * on the specified {@link AttributeGroup}.
	 * 
	 * @param attributeGroupId
	 *            the ID of the {@link AttributeGroup} the new {@link ValueSet} should be based on.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = "action=addValueSet")
	public void handleAddValueSetAction(@ModelAttribute(COMMAND) PrivateData privateData,
			@RequestParam("attributeGroupId") final long attributeGroupId, ModelMap model,
			final ActionRequest request, final Locale locale) {
		logger.info("handleUpdateValueSetAction() entered");

		String uuid = SecurityContextHolder.getUUID();

		try {
			Account account = privateDataManagement.getAccount(uuid);
			AttributeGroup attributeGroup = privateDataManagement.getAttributeGroupById(
					SecurityContextHolder.getClientName(), attributeGroupId);
			List<ValueSet> valueSets = privateData.getValueSets();

			/*
			 * create the new ValueSet
			 */
			ValueSet valueSet = new ValueSet(account, attributeGroup);

			/*
			 * persist changes
			 */
			valueSet = privateDataManagement.setValues(SecurityContextHolder.getUUID(), valueSet,
					false);
			valueSets.add(valueSet);

			/*
			 * clear the model and set success message
			 */
			model.remove(COMMAND);
			model.addAttribute(COMMAND, getPrivateData());
			request.setAttribute("msg_success", resources.getMessage("success.valueset.add", null,
					locale));
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_error", resources.getMessage("error.valueset.add", null,
					locale));
		}
	}

	/**
	 * Handles the an unconfirmed <code>deleteValues</code> action by displaying a confirmation
	 * request to the user.
	 * 
	 * @param valueSetIndex
	 *            the index of the {@link ValueSet} in the list of all {@link ValueSet}s in the
	 *            {@link PrivateData} object.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = { "action=deleteValues", "!confirmed", "!denied" })
	public void handleUnconfirmedDeleteValuesAction(
			@ModelAttribute(COMMAND) PrivateData privateData,
			@RequestParam("valueSetIndex") final int valueSetIndex, final ActionRequest request,
			final Locale locale) {
		logger.info("handleDeniedDeleteValuesAction() entered " + valueSetIndex);
		ValueSet valueSet = privateData.getValueSets().get(valueSetIndex);

		/*
		 * return if the ValueSet is locked
		 */
		if (valueSet.isLocked() || valueSet.isLockedRecursive()) {
			logger.info("handleUnconfirmedDeleteValueSetAction(): this ValueSet is locked and can not be deleted!");
			
			request.setAttribute("msg_error", resources.getMessage("error.value.locked", null,
					locale));
			return;
		}

		/*
		 * ask for confirmation
		 */
		request.setAttribute("valueSetToDeleteIndex", valueSetIndex);
		request.setAttribute("valueSetToDelete", valueSet);
		request.setAttribute("confirmation", "Delete");
	}

	/**
	 * Handles a confirmed <code>deleteValues</code> action request by invoking the
	 * <code>deleteValues</code> of injected {@link PrivateDataManagement}.
	 * 
	 * @param valueSetIndex
	 *            the index of the {@link ValueSet} in the list of all {@link ValueSet}s in the
	 *            {@link PrivateData} object.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = { "action=deleteValues", "denied" })
	public void handleDeniedDeleteValuesAction(final ActionRequest request) {
		logger.info("handleDeniedDeleteValuesAction() entered");
	
		/*
		 * remove the confirmation request
		 */
		request.removeAttribute("valueSetToDeleteIndex");
		request.removeAttribute("valueSetToDelete");
		request.removeAttribute("confirmation");
	}

	/**
	 * Handles a confirmed <code>deleteValues</code> action request by invoking the
	 * <code>deleteValues</code> of injected {@link PrivateDataManagement}.
	 * 
	 * @param valueSetIndex
	 *            the index of the {@link ValueSet} in the list of all {@link ValueSet}s in the
	 *            {@link PrivateData} object.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = { "action=deleteValues", "confirmed" })
	public void handleConfirmedDeleteValuesAction(@ModelAttribute(COMMAND) final PrivateData privateData,
			@RequestParam("valueSetIndex") final int valueSetIndex, final ModelMap model,
			final ActionRequest request, final Locale locale) {
		logger.info("handleConfirmedDeleteValuesAction() entered, valueSet's index is " + valueSetIndex);
	
		/*
		 * delete the ValueSet
		 */
		try {
			List<ValueSet> valueSets = privateData.getValueSets();
			ValueSet valueSet = valueSets.get(valueSetIndex);
			
			/*
			 * keep values of write protected Attributes
			 */
			if (valueSet.getAttributeGroup().hasWriteProtectedAttributes()) {
				List<Value> previousValues = valueSet.getValues();
				valueSet.setValues(new ArrayList<Value>());
				for (Value value : previousValues) {
					/*
					 * if the value is write protected write it to the new values list, otherwise
					 * create a new empty value
					 */
					if (value.getAttribute().isWriteProtected())
						valueSet.addValue(value);
					else
						valueSet.addValue(new Value(value.getAttribute(), null));
				}
				privateDataManagement.setValues(SecurityContextHolder.getUUID(), valueSet, false);
	
				/*
				 * set success message
				 */
				request.setAttribute("msg_success", resources.getMessage(
						"success.valueset.clear.preservedLocked", null, locale));
			} else {
				/*
				 * perform deletion (if possible)
				 */
				privateDataManagement
						.deleteValues(SecurityContextHolder.getUUID(), valueSet, false);
				/*
				 * set success message
				 */
				String messageCode = "success.valueset.clear";
				if (valueSet.getAttributeGroup().isMultiple())
					messageCode = "success.valueset.delete";
				request.setAttribute("msg_success", resources.getMessage(
						messageCode, null, locale));
			}
	
			/*
			 * clear the model
			 */
			valueSets.remove(valueSet);
			model.remove(COMMAND);
			model.addAttribute(COMMAND, getPrivateData());
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_error", resources.getMessage("error.valueset.delete", null,
					locale));
		}
	}

	/**
	 * Handles the <code>addValue</code> action request by adding a new {@link Value} based on the
	 * specified {@link Attribute} to the list of all {@link Value}s in the {@link PrivateData}
	 * object.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = "action=addValue")
	public void handleAddValueAction(@RequestParam("valueSetIndex") final int valueSetIndex,
			@RequestParam("attributeId") final long attributeId,
			@RequestParam("addIndex") final int addIndex,
			@ModelAttribute(COMMAND) PrivateData privateData, final Errors errors, ModelMap model,
			final ActionRequest request) {
		logger.info("handleAddValueAction() entered");

		try {
			ValueSet valueSet = privateData.getValueSets().get(valueSetIndex);

			/*
			 * create the new value
			 */
			Attribute attribute = privateDataManagement.getAttributeById(SecurityContextHolder
					.getClientName(), attributeId);
			Value newValue = new Value(attribute, "");

			/*
			 * add the new value at the end of the attribute value list
			 */
			List<Value> values = valueSet.getValues();
			if (addIndex >= values.size())
				values.add(newValue);
			else
				values.add(addIndex, newValue);

			/*
			 * correct sort order
			 */
			for (int i = 0; i < values.size(); i++) {
				values.get(i).setSortOrder(i);
			}

			/*
			 * persist changes
			 */
			privateDataManagement.setValues(SecurityContextHolder.getUUID(), valueSet, false);

			/*
			 * clear the model and set success message
			 */
			request.setAttribute("msg_success", resources.getMessage("success.value.add", null,
					request.getLocale()));
			model.remove(COMMAND);
			model.addAttribute(COMMAND, getPrivateData());
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.value.add");
		}
	}

	/**
	 * Handles the <code>deleteValue</code> action request by invoking the
	 * <code>getValuesById</code> and the <code>setValues</code> of injected
	 * {@link PrivateDataManagement} and the <code>deleteValueById</code> of the {@link ValueSet}.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = "action=deleteValue")
	public void handleDeleteValueAction(@ModelAttribute(COMMAND) final PrivateData privateData,
			@RequestParam("valueSetIndex") final int valueSetIndex,
			@RequestParam("valueIndex") final int valueIndex, final ActionRequest request,
			ModelMap model, final Locale locale) {
		logger.info("handleDeleteValueAction() entered");

		String uuid = SecurityContextHolder.getUUID();

		try {
			/*
			 * remove the specified value
			 */
			ValueSet valueSet = privateData.getValueSets().get(valueSetIndex);
			Attribute attribute = valueSet.getValues().get(valueIndex).getAttribute();
			valueSet.getValues().remove(valueIndex);

			/*
			 * if the value was the last of its attribute, create a new one in its place
			 */
			if (valueSet.getValuesByAttribute(attribute.getName()).isEmpty()) {
				valueSet.getValues().add(valueIndex, new Value(attribute, null));
			}

			/*
			 * persist the deletion
			 */
			privateDataManagement.setValues(uuid, valueSet, false);

			/*
			 * clear the form, it will be displayed again without of deleted value
			 */
			request.setAttribute("msg_success", resources.getMessage("success.value.delete", null,
					locale));
			model.remove(COMMAND);
			model.addAttribute(COMMAND, getPrivateData());
		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_error", resources.getMessage("error.value.delete", null,
					locale));
		}
	}

	/**
	 * Handles the <code>uploadFile</code> action request by redirect to the {@link FileUpload}
	 * controller.
	 * 
	 * @see org.evolvis.idm.selfmanagement.controller.FileUpload
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = "action=uploadFile")
	public void handleUploadFileAction(@RequestParam("valueSetIndex") final int valueSetIndex,
			@RequestParam("valueIndex") final int valueIndex, final ActionRequest request,
			final ActionResponse response, ModelMap model, final Locale locale) {
		logger.info("handleUploadFileAction() entered");

		try {
			/*
			 * redirect to UploadFile
			 * 
			 * see
			 * org.evolvis.idm.common.interceptor.ParameterMappingInterceptor.afterActionCompletion
			 * () the intercepter puts the attribute as render parameters
			 */
			request.setAttribute("ctx", "UploadFile");
			response.setRenderParameter("valueSetIndex", Integer.toString(valueSetIndex));
			response.setRenderParameter("valueIndex", Integer.toString(valueIndex));

			model.remove(COMMAND);
			model.addAttribute(COMMAND, getPrivateData());

		} catch (Exception e) {
			logger.warn(e);
			request.setAttribute("msg_error", resources.getMessage("error.value.create", null,
					locale));
		}
	}

	/**
	 * Handles the render request and returns a name of the PrivateDataManagement view. The
	 * {@link ModelMap} contains following objects: <br/>
	 * <br/>
	 * <i>{@link PrivateData}</i> command object stored under the <code>command</code> name. <br/>
	 * <i>navPoints</i> List of all {@link NavPoint}.
	 * 
	 * This will always fetch all necessary data form the database service! All not persisted
	 * changes on the command object will be lost at this point.
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping
	public String handleRenderRequest(RenderRequest request, ModelMap model) {
		logger.info("handleRenderRequest() entered");

		if (request.getParameter("hasErrors") == null) {
			model.remove(COMMAND);
			model.addAttribute(COMMAND, getPrivateData());
		}

		return "PrivateDataManagement";
	}
}
