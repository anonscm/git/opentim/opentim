package org.evolvis.idm.selfmanagement.constant;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.idm.common.model.NavPoint;

/**
 * @author Dmytro Mayster
 */
public class UserNavPoints {

    public static final NavPoint ACCOUNT_SUMMARY = new NavPoint("AccountSummary", "nav.account_summary", "accountsummary-button");

    public static final NavPoint PRIVATEDATA_MANAGEMENT = new NavPoint("PrivateDataManagement", "nav.privatedata-management", "privatedatamanagement-button");
    

    public static List<NavPoint> getAll() {
    	List<NavPoint> tmpList = new ArrayList<NavPoint>();
    	tmpList.add(ACCOUNT_SUMMARY);
        tmpList.add(PRIVATEDATA_MANAGEMENT);
    	
    	return tmpList;
    }
}
