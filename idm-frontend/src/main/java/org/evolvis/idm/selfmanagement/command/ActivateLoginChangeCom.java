package org.evolvis.idm.selfmanagement.command;


public class ActivateLoginChangeCom {

    private String password;
    
    private String activationKey;

	public String getPassword() {
		return password;
	}

	public void setPassword(String newPassword) {
		this.password = newPassword;
	}
	
	public void setActivationKey(String activationKey) {
		this.activationKey = activationKey;
	}

	public String getActivationKey() {
		return activationKey;
	}
}
