package org.evolvis.idm.selfmanagement.controller;

import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.model.NavPoint;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.security.SecurityContextHolder;
import org.evolvis.idm.selfmanagement.command.PrivateData;
import org.evolvis.idm.selfmanagement.constant.UserNavPoints;
import org.evolvis.idm.selfmanagement.propertyEditors.ValueSetPropertyEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.portlet.bind.PortletRequestDataBinder;
import org.springframework.web.portlet.multipart.MultipartActionRequest;

/**
 * controller that will process a file upload
 * 
 * @author Dmytro Mayster
 * 
 */
@Controller
@RequestMapping(value = "view", params = "ctx=UploadFile")
@SessionAttributes({FileUpload.COMMAND})
public class FileUpload {
	public static final String COMMAND = "command";
	
	private int maxUploadSize;
	
	private final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	protected org.evolvis.idm.identity.privatedata.service.PrivateDataManagement privateDataManagement;
	
	@Autowired
	protected MessageSource resources;
	
	@Autowired
	protected ValueSetPropertyEditor valueSetPropertyEditor;

	public int getMaxUploadSize() {
		return maxUploadSize;
	}

	public void setMaxUploadSize(int maxUploadSize) {
		this.maxUploadSize = maxUploadSize;
	}
	
	@InitBinder
	protected void initBinder(PortletRequestDataBinder binder) {
		// to actually be able to convert Multipart instance to byte[]
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(ValueSet.class, valueSetPropertyEditor);
	}
	
	/**
     * Handles an <code>uploadFile</code> action request by use of
     * the {@link MultipartActionRequest} for upload and <code>setValues</code>
     * of injected {@link PrivateDataManagement}.
     * 
     * @param request ActionRequest object
     * @param valueSetIndex the index of valueSet to upload 
     * @param valueIndex the index of value to upload 
     * @param command The command object {@link PrivateData} contains the file to upload
     * @param errors Errors object to collect from errors
     * @param model the holder of model attributes
     * 
     * @see org.evolvis.idm.selfmanagement.controller.FileUpload
     */
	@Secured({"ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER"})
    @RequestMapping(params = "action=uploadFile")
    public void handleUploadFileAction(ActionRequest request, ActionResponse response,
    		@RequestParam("valueSetIndex") final int valueSetIndex,
			@RequestParam("valueIndex") final int valueIndex,
            @ModelAttribute(COMMAND) PrivateData privateData, Errors errors, 
            ModelMap model, Locale locale) {
		logger.info("handleUploadFileAction() entered");
		
    	String uuid = SecurityContextHolder.getUUID();

    	ValueSet valueSet = privateData.getValueSets().get(valueSetIndex);
    	Value value = valueSet.getValues().get(valueIndex);
    	value.setId(null); // necessary because BLOB values will not be updated if they already have an ID!
    	String additionalTypeConstraint = value.getAttribute().getAdditionalTypeConstraint();

        try {
        	MultipartActionRequest multipartRequest = (MultipartActionRequest) request;
        	
        	/*
        	 * check file size if there was a limit set
        	 * return immediately if file is to big
        	 */
        	if (additionalTypeConstraint != null && additionalTypeConstraint.matches("^[0-9]+.*")) {
        		int fileSizeLimit = Integer.parseInt(additionalTypeConstraint.split(" ", 2)[0]);
        		if (multipartRequest.getContentLength() > fileSizeLimit  * 1024) {
        			errors.reject("error.fileSizeLimit");
            		response.setRenderParameter("valueSetIndex", Integer.toString(valueSetIndex));
            		response.setRenderParameter("valueIndex", Integer.toString(valueIndex));
            		return;
        		}
        	}

        	/*
        	 * get the file
        	 */
            MultipartFile multipartFile = multipartRequest.getFile("file");
            String fileName = "fileName";
            if (multipartFile != null) {
            	fileName = multipartFile.getOriginalFilename();
            }
        	
        	/*
        	 * check file name extensions if there is a restriction
        	 */
        	if (additionalTypeConstraint != null && additionalTypeConstraint.matches("^[0-9]* .+$")) {
        		String allowedFileExtensions = additionalTypeConstraint.split(" ", 2)[1];
        		if (!fileName.matches(".*" + (allowedFileExtensions))) {
        			errors.reject("error.notAllowedFileExtension");
        		}
			}
        	
        	/*
        	 * return if errors were detected
        	 */
        	if (errors.hasErrors()) {
        		/*
        		 * read parameters needed for rendering (could be done by ParameterMapper too)
        		 */
        		response.setRenderParameter("valueSetIndex", Long.toString(valueSetIndex));
        		response.setRenderParameter("valueIndex", Long.toString(valueIndex));
        		return;
        	}

        	byte[] byteValue = privateData.getFile();
        	
        	if (byteValue != null) {
        		value.setValue(fileName);
        		value.setByteValue(byteValue);
				valueSet = privateDataManagement.setValues(uuid, valueSet, false);
		        
				StringBuffer msg = new StringBuffer(); 
				msg.append(resources.getMessage("success.file.upload", null, request.getLocale()));
				msg.append(" ");
				msg.append(resources.getMessage("success.value.create", null, request.getLocale()));
				request.setAttribute("msg_success", msg.toString());
			
				// clears the model after submit:
				model.clear();
        	} else {
        		// the user did not upload anything
        		errors.reject("error.file.upload");
        	}
		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.value.create");
		}
		
    	/*
    	 * return if errors were detected
    	 */
    	if (errors.hasErrors()) {
    		/*
    		 * read parameters needed for rendering (could be done by ParameterMapper too)
    		 */
    		response.setRenderParameter("valueSetIndex", Long.toString(valueSetIndex));
    		response.setRenderParameter("valueIndex", Long.toString(valueIndex));
    		return;
    	}
		
		// redirect:
		// actionResponse.setRenderParameter("ctx", "PrivateDataManagement");
		// see: org.evolvis.idm.common.interceptor.ParameterMappingInterceptor.afterActionCompletion()
		// the interceptor puts the attribute as render parameters:
		request.setAttribute("ctx", "PrivateDataManagement");
    }

	/**
     * Handles a render request and returns a name of
     * the FileUpload view. The {@link ModelMap} contains
     * following objects:
     * <br/>
     * <br/><i>{@link PrivateData}</i> command object stored under the <code>command</code> name.
     * <br/><i>valueSetIndex</i> the index of valueSet to upload 
     * <br/><i>valueIndex</i> the index of value to upload 
     * <br/><i>navPoints</i> List of all {@link NavPoint}.
     * 
     * @param renderResponse RenderResponse object
     * @param model the holder of model attributes
     * @param valueSetIndex the index of valueSet to upload 
     * @param valueIndex the index of value to upload 
     * 
     * @return String the name of the FileUpload view
     */
	@RequestMapping
	protected String handleRenderRequest(ModelMap model,RenderResponse renderResponse,
			@RequestParam("valueSetIndex") int valueSetIndex,
    		@RequestParam("valueIndex") int valueIndex) {
		
		PrivateData privateData = (PrivateData) model.get(COMMAND);
		model.addAttribute(COMMAND, formBackingObject(privateData));
		model.addAttribute("valueSetIndex", valueSetIndex);
		model.addAttribute("valueIndex", valueIndex);
		
		Value value = privateData.getValueSets().get(valueSetIndex).getValues().get(valueIndex);
		String additionalTypeConstraint = value.getAttribute().getAdditionalTypeConstraint();
		
		/*
		 * add file size limit and allowed file extensions to the model for help text
		 */
    	if (additionalTypeConstraint != null && !additionalTypeConstraint.isEmpty()) {
    		String fileSizeLimit = "*";
    		if (additionalTypeConstraint.matches("^[0-9]+.*")) {
    			fileSizeLimit = additionalTypeConstraint.split(" ", 2)[0];
    		}
			model.addAttribute("fileSizeLimit", fileSizeLimit);
    		String allowedFileExtensions = "*";
    		if (additionalTypeConstraint != null && additionalTypeConstraint.matches(".*[a-zA-Z0-9]$")) {
    			allowedFileExtensions = additionalTypeConstraint.split(" ", 2)[1];
    			allowedFileExtensions = allowedFileExtensions.replaceAll("\\|", ", ");
    			allowedFileExtensions = allowedFileExtensions.replaceFirst("(.*)(?:, ([a-zA-Z0-9]+))$", "$1 oder $2");
    		}
    		model.addAttribute("allowedFileExtensions", allowedFileExtensions);
    	}
    
		model.addAttribute("navPoints", UserNavPoints.getAll());
		
		return "FileUpload";
	}
	
	private PrivateData formBackingObject(PrivateData privateData) {
		privateData.setFile(new byte[getMaxUploadSize()]);
		
		return privateData;
	}
}
	