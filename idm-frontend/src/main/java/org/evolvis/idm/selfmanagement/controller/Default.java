package org.evolvis.idm.selfmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Dmytro Mayster
 * 
 * This is the default controller which is called if no context is given.
 * Let this controller extend the controller you want to load as default context.
 * 
 */
@Controller
@RequestMapping(value = "view", params = "!ctx")
public class Default extends AccountSummary {

    @RequestMapping
    public String handleRenderRequest(ModelMap model) {
    	model.addAttribute("ctx", "AccountSummary");
    	return super.handleRenderRequest(model);
    }
}
