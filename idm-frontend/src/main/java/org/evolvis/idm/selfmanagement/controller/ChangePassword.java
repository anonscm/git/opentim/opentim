package org.evolvis.idm.selfmanagement.controller;

import javax.portlet.ActionRequest;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.model.NavPoint;
import org.evolvis.idm.common.service.AuthenticationService;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.security.SecurityContextHolder;
import org.evolvis.idm.security.SpringSecurityUser;
import org.evolvis.idm.selfmanagement.command.ChangePasswordCom;
import org.evolvis.idm.selfmanagement.constant.UserNavPoints;
import org.evolvis.opensso.rest.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;

/**
 * Manages a password change
 * 
 * @author Dmytro Mayster
 * 
 */

@Controller
@RequestMapping(value = "view", params = "ctx=ChangePassword")
@SessionAttributes( { ChangePassword.COMMAND })
public class ChangePassword {
	public static final String COMMAND = "command";

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	protected Validator validator;

	@Autowired
	protected MessageSource resources;

	@Autowired
	protected AuthenticationService authenticationService;

	/**
	 * Handles an <code>ChangePassword</code> action request by use a {@link RestClient} for connect
	 * to a <code>OpenSSO</code> server.
	 * 
	 * @param actionRequest
	 *            ActionRequest object
	 * @param command
	 *            The command object {@link ChangePasswordCom} contains the file to upload
	 * @param errors
	 *            Errors object to collect from errors
	 * 
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping(params = "action=changePassword")
	public void handleChangePasswordAction(ActionRequest actionRequest,
			@ModelAttribute(COMMAND) @Valid ChangePasswordCom command, Errors errors) {
		logger.info("handleChangePasswordAction() entered");

		SpringSecurityUser springSecurityUser = (SpringSecurityUser) org.springframework.security.context.SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();

		try {
			/*
			 * check password policy pattern
			 */
			String passwordPattern = SecurityContextHolder.getClientPropertyMap().get(
					ClientPropertyMap.KEY_SECURITY_PASSWORT_PATTERN);
			if (!passwordPattern.isEmpty() && !command.getPassword().matches(passwordPattern)) {
				errors.rejectValue("password", "violation.password.password");
			}

			/*
			 * check old password
			 */
			if (authenticationService.login(springSecurityUser.getUsername(),
					command.getOldPassword()) == null) {
				errors.rejectValue("oldPassword", "error.password.old");
			}
			
			/*
			 * return if there are any errors
			 */
			if (errors.hasErrors()) return;

			/*
			 * try to perform the update
			 */
			boolean success = authenticationService.updatePassword(
					springSecurityUser.getUsername(), command.getPassword());
			if (success) {
				actionRequest.setAttribute("msg_success", resources.getMessage(
						"success.password.change", null, actionRequest.getLocale()));
			} else {
				errors.reject("error.password.change");
			}

		} catch (Exception e) {
			logger.warn(e);
			errors.reject("error.password.change");
		}
	}

	/**
	 * Handles a render request and returns a {@link ModelAndView} object with a ChangePassword
	 * view. The {@link ModelAndView} contains following objects: <br/>
	 * <br/>
	 * <i>{@link ChangePasswordCom}</i> command object stored under the <code>command</code> name. <br/>
	 * <i>navPoints</i> List of all {@link NavPoint}.
	 * 
	 * 
	 * @return modelAndView the name of the ChangePassword view
	 */
	@Secured( { "ROLE_SUPER_ADMIN", "ROLE_CLIENT_ADMIN", "ROLE_SECTION_ADMIN", "ROLE_USER" })
	@RequestMapping
	public ModelAndView handleRenderRequest() {

		ModelAndView modelAndView = new ModelAndView("ChangePassword", COMMAND, new ChangePasswordCom());
		modelAndView.addObject("navPoints", UserNavPoints.getAll());

		return modelAndView;
	}

}
