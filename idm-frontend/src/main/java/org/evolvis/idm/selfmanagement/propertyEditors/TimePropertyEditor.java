package org.evolvis.idm.selfmanagement.propertyEditors;

import java.beans.PropertyEditorSupport;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;

import org.evolvis.idm.selfmanagement.constant.DatePatterns;
import org.springframework.stereotype.Component;

/**
 * This PropertyEditor is used to effect the conversion between a form and a
 * command object. It is used in SpringMVC controllers.
 * 
 * @author Dmytro Mayster
 * 
 */

@Component
public class TimePropertyEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) {
		if (text != null && !text.isEmpty()) {
			SimpleDateFormat dateFormat = new SimpleDateFormat(DatePatterns.TIME);
			// round up the date will be turned off:
			dateFormat.setLenient(false);
			// check if the value is a valid time:
			ParsePosition parsePosition = new ParsePosition(0);
			dateFormat.parse(text, parsePosition);
			// the entire string 'text' must be parsed without errors
			if (text.length() < DatePatterns.TIME.length() || parsePosition.getIndex() != text.length()) {
				throw new IllegalArgumentException("Not a valid time format entered: " + text + ", must be "
						+ DatePatterns.TIME);
			}
		}

		// the value is a valid time:
		this.setValue(text);
	}
	
	@Override
	public String getAsText() {
		if(this.getValue() != null)
			return (String)this.getValue();
		
		return "";
	}

}
