package org.evolvis.idm.selfmanagement.command;

import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.evolvis.idm.common.validation.Email;

public class RequestLoginChangeCom {

	@Email
	@Size(min = 6, max = 60, message = "violation.account.size")
	@Transient
	private String login;
	
	private String activationURL;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setActivationURL(String activationURL) {
		this.activationURL = activationURL;
	}

	public String getActivationURL() {
		return activationURL;
	}
}
