package org.evolvis.idm.em.connector.liferay;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import junit.framework.Assert;

import org.evolvis.idm.em.agent.model.AgentEvent;
import org.evolvis.idm.em.core.model.EventType;
import org.evolvis.idm.relation.permission.model.Group;
import org.junit.Before;
import org.junit.Test;

public class LiferayConnectorTest {
	
	private LiferayConnectorRemote liferayConnector;
	
	protected LiferayConnectorRemote getLiferayConnector() {
		if (liferayConnector == null) {
			try {
				Hashtable<Object, Object> jndiProps = new Hashtable<Object, Object>();
				jndiProps.put("java.naming.factory.initial","org.jnp.interfaces.NamingContextFactory");
				jndiProps.put("java.naming.factory.url.pkgs","org.jboss.naming:org.jnp.interfaces");
				jndiProps.put("java.naming.provider.url","localhost:1099");
				Context context = new InitialContext(jndiProps);
				liferayConnector = (LiferayConnectorRemote) context.lookup("idm-connector-liferay-ear/" + LiferayConnectorImpl.class.getSimpleName() + "/remote");
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return liferayConnector;
	}
	
	@Before
	public void configure() {
		Map<String, String> configuration = new HashMap<String, String>();
		configuration.put("companyId", "10108");
		getLiferayConnector().configure(configuration);
	}
	
	@Test
	public void insertUpdateDeleteGroup() throws Exception {
		Group group = new Group();
		group.setName("liferayConnectorTestName");
		group.setDisplayName("liferayConnectorTestDisplayName");
		AgentEvent<Group> groupInsertEvent = new AgentEvent<Group>(group, EventType.INSERT);
		long groupId = liferayConnector.processGroupEvent(groupInsertEvent);
		List<String> groups = liferayConnector.getGroups();
		Assert.assertTrue(groups.contains("liferayConnectorTestName"));
		
		AgentEvent<Group> groupUpdateEvent = new AgentEvent<Group>(group, EventType.UPDATE);
		groupUpdateEvent.setTargetEntityIdentifier(String.valueOf(groupId));
		group.setName("liferayConnectorTestNameUpdated");
		liferayConnector.processGroupEvent(groupUpdateEvent);
		groups = liferayConnector.getGroups();
		Assert.assertTrue(groups.contains("liferayConnectorTestNameUpdated"));
		Assert.assertFalse(groups.contains("liferayConnectorTestName"));
		
		AgentEvent<Group> groupDeleteEvent = new AgentEvent<Group>(group, EventType.DELETE);
		groupDeleteEvent.setTargetEntityIdentifier(String.valueOf(groupId));
		liferayConnector.processGroupEvent(groupDeleteEvent);
		groups = liferayConnector.getGroups();
		Assert.assertFalse(groups.contains("liferayConnectorTestNameUpdated"));
	}
}
