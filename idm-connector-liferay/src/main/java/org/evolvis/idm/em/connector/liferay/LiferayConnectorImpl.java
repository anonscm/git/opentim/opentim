package org.evolvis.idm.em.connector.liferay;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.evolvis.idm.em.agent.model.AgentEvent;
import org.evolvis.idm.relation.permission.model.Group;

import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.UserGroupLocalServiceUtil;

@Stateful
@TransactionAttribute(TransactionAttributeType.NEVER)
public class LiferayConnectorImpl implements LiferayConnectorRemote {

	public static final String CONFIG_KEY_COMPANY_ID = "companyId";
	
	private long companyId;
	
	@Override
	public List<String> getRoles() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getGroups() {
		try {
			List<UserGroup> userGroups = UserGroupLocalServiceUtil.getUserGroups(getCompanyId());
			List<String> groups = new ArrayList<String>(userGroups.size());
			for (UserGroup userGroup : userGroups) {
				groups.add(userGroup.getName());
			}
			return groups;
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long processGroupEvent(AgentEvent<Group> groupEvent) {
		try {
			switch (groupEvent.getEventType()) {
				case INSERT: 
					return processGroupInsertEvent(groupEvent);
				case UPDATE: 
					processGroupUpdateEvent(groupEvent); 
					return groupEvent.getTargetEntityIdentitierAsLong();
				case DELETE: 
					processGroupDeleteEvent(groupEvent); 
					return groupEvent.getTargetEntityIdentitierAsLong();
			}
		} catch (PortalException pe) {
			// TODO
		} catch (SystemException e) {
			// TODO
		}
		return 0;
	}
	
	protected long processGroupInsertEvent(AgentEvent<Group> groupEvent) throws PortalException, SystemException {
		return UserGroupLocalServiceUtil.addUserGroup(10133, getCompanyId(), groupEvent.getEntity().getName(), groupEvent.getEntity().getDisplayName()).getUserGroupId();
	}
	
	protected void processGroupUpdateEvent(AgentEvent<Group> groupEvent) throws PortalException, SystemException {
		UserGroup userGroup = getGroup(groupEvent.getTargetEntityIdentitierAsLong());
		userGroup.setName(groupEvent.getEntity().getName());
		userGroup.setDescription(groupEvent.getEntity().getDisplayName());
		UserGroupLocalServiceUtil.updateUserGroup(userGroup);
	}
	
	protected void processGroupDeleteEvent(AgentEvent<Group> groupEvent) throws PortalException, SystemException {
		UserGroupLocalServiceUtil.deleteUserGroup(groupEvent.getTargetEntityIdentitierAsLong());
	}
	
	protected UserGroup getGroup(long userGroupId) throws PortalException, SystemException {
		return UserGroupLocalServiceUtil.getUserGroup(userGroupId);
	}

	@Override
	public void configure(Map<String, String> configuration) {
		setCompanyId(Long.valueOf(configuration.get(CONFIG_KEY_COMPANY_ID)));
	}

	protected long getCompanyId() {
		return companyId;
	}

	protected void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
}