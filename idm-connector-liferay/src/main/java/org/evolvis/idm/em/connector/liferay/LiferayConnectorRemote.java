package org.evolvis.idm.em.connector.liferay;

import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import org.evolvis.idm.em.agent.model.AgentEvent;
import org.evolvis.idm.relation.permission.model.Group;

@Remote
public interface LiferayConnectorRemote {

	public void configure(Map<String, String> configuration);

	public long processGroupEvent(AgentEvent<Group> groupEvent);

	public List<String> getGroups();

	public List<String> getRoles();

}
