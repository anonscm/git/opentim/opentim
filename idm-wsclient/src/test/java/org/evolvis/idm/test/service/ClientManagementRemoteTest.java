package org.evolvis.idm.test.service;

import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.test.service.ClientManagementServiceAbstractTest;
import org.evolvis.idm.test.service.ServiceProvider;
import org.junit.Test;

public class ClientManagementRemoteTest extends ClientManagementServiceAbstractTest {
	/**
	 * Test to fix Eclipse JUnit bug with abstract test cases
	 */
	@Test
	public void workaroundTest() {
		// Nothing.
	}

	@Override
	protected ClientManagement getService() {
		return getServiceProvider().getClientManagementService();
	}

	protected ServiceProvider serviceProvider;
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderRemote();
		return serviceProvider;
	}
}
