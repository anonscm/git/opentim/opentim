package org.evolvis.idm.test.service;

import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.test.service.GroupAndRoleManagementAbstractTest;
import org.evolvis.idm.test.service.ServiceProvider;
import org.junit.Test;

public class GroupAndRoleManagementRemoteTest extends GroupAndRoleManagementAbstractTest {

	/**
	 * Test to fix Eclipse JUnit bug with abstract test cases
	 */
	@Test
	public void workaroundTest() {
		// Nothing.
	}

	@Override
	protected GroupAndRoleManagement getService() {
		return getServiceProvider().getGroupAndRoleManagementService();
	}

	protected ServiceProvider serviceProvider;
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderRemote();
		return serviceProvider;
	}
}
