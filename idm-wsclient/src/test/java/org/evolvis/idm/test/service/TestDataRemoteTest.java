package org.evolvis.idm.test.service;

import java.util.List;

import junit.framework.Assert;

import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.junit.Test;

public class TestDataRemoteTest extends AbstractBaseServiceAbstractTest<ClientManagement> {
 
 //@Test
 public void createTestData() throws Exception {
	 Client client = getServiceProvider().getClientManagementService().getClientByName("stage3.9.tarent.buero");
	 
//	 Client client = getCurrentClient(true);
	 //Account account = getCurrentAccount(client, true);
	 
	 Account account = getServiceProvider().getAccountManagementService().getAccountByUsername(client.getClientName(), "test10@tarent.de");
	 
	 //AttributeGroup contactData = getServiceProvider().getPrivateDataConfigurationService().getAttributeGroup(client.getClientName(), "contactData");
	 
//	 ValueSet valueSet = new ValueSet(account, contactData);
//	 valueSet = getServiceProvider().getPrivateDataManagementService().setValues(account.getUuid(), valueSet, false);
	 
	 List<ValueSet> values = getServiceProvider().getPrivateDataManagementService().getValues(account.getUuid());
	 System.out.println(values.size());
	 Assert.assertEquals(values.size(), 5);
	 
//	 for (ValueSet valueSet : values) {
//		 if (valueSet.getAttributeGroup().getName().equals("contactData"))
//			 getServiceProvider().getPrivateDataManagementService().deleteValues(account.getUuid(), valueSet, false);
//	 }
//	 
////	 getServiceProvider().getPrivateDataManagementService().deleteValues(account.getUuid(), valueSet, false);
//	 
//	 values = getServiceProvider().getPrivateDataManagementService().getValues(account.getUuid());
//	 
//	 Assert.assertEquals(values.size(), 4);
  
//  Client client = getCurrentClient(true);
//  
//  AttributeGroup attributeGroup = new AttributeGroup("testsync", "testsync");
//  Attribute attribute = new Attribute("testsyncatt", "testsyncatt");
//  attribute.setSyncOptions("postalCode");
//  attributeGroup.addAttribute(attribute);
//  
//  getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), attributeGroup);
//  
//  ClientPropertyMap clientPropertyMap = new ClientPropertyMap();
//  clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_HOSTANDPORT, "windows-ad.qs.tarent.de:389");
//  clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_BASEDN, "dc=qs,dc=tarent,dc=de");
//  clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_USERDN, "CN=Administrator,CN=Users,dc=qs,dc=tarent,dc=de");
//  clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_PASSWORD, "tarentinerCC");
//  clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHDN, "cn=Users");
//  clientPropertyMap.put(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHFILTER, "(&(objectClass=person)(cn=LOGINNAME))");
//  getServiceProvider().getClientManagementService().setClientPropertyMap(client.getName(), clientPropertyMap);
//	Client client = getServiceProvider().getClientManagementService().getClientByName("stage3.6.tarent.buero");
//	 getServiceProvider().getClientManagementService().getClients(null);
//	
//	for (Account account : getServiceProvider().getAccountManagementService().getAccounts(client.getName(), null).getResultList()) {
//		getServiceProvider().getAccountManagementService().deleteAccount(account.getUuid());
//	}
	
//	getServiceProvider().getClientManagementService().deleteClient(client);
//	client.setId(null);
//	getServiceProvider().getClientManagementService().setClient(0, client);
//	ClientPropertyMap clientPropertyMap = getServiceProvider().getClientManagementService().getClientPropertyMap("stage3.6.tarent.buero");
//	clientPropertyMap.put(ClientPropertyMap.KEY_SYSTEM_DAILYJOBEXECUTIONTIME, "04:41");
//	clientPropertyMap.put(ClientPropertyMap.KEY_SYSTEM_DEACTIVATEDACCOUNTLIFETIMEINDAYS, "0");
//	getServiceProvider().getClientManagementService().setClientPropertyMap("stage3.6.tarent.buero", clientPropertyMap);
//	
//	clientPropertyMap = getServiceProvider().getClientManagementService().getClientPropertyMap("stage3.6.tarent.buero-employee");
//	clientPropertyMap.put(ClientPropertyMap.KEY_SYSTEM_DAILYJOBEXECUTIONTIME, "04:55");
//	getServiceProvider().getClientManagementService().setClientPropertyMap("stage3.6.tarent.buero-employee", clientPropertyMap);
//	 Client client = getServiceProvider().getClientManagementService().getClientByName("stage3.6.tarent.buero");
//	 for (int i = 0; i < 1000; i++) {
//		 User user = new User();
//		 user.setClient(client);
//		 user.setLogin("testjens"+i+"@test.de");
//		 user.setDisplayName("testjens"+i);
//		 user.setFirstname("Test");
//		 user.setLastname("Jens");
//		 user.setPassword("test1234");
//		 user.setPasswordRepeat("test1234");
//		 getServiceProvider().getUserManagementService().createUser(user);
//	 }
	 
//	 List<Group> groups = getServiceProvider().getGroupAndRoleManagementService().getGroups("stage3.6.tarent.buero", null).getResultList();
//	 for (Group group : groups) {
//		 getServiceProvider().getGroupAndRoleManagementService().deleteGroup("stage3.6.tarent.buero", group.getId());
//	 }
//	 
//	 List<Role> roles = getServiceProvider().getGroupAndRoleManagementService().getRolesByClient("stage3.6.tarent.buero", null).getResultList();
//	 for (Role role : roles) {
//		 getServiceProvider().getGroupAndRoleManagementService().deleteRole("stage3.6.tarent.buero", role.getId());
//	 }
	 
//	 List<Application> apps = getServiceProvider().getApplicationManagementService().getApplications("stage3.6.tarent.buero", null).getResultList();
//	 for (Application app : apps) {
//		 if (app.getName().startsWith("default"))
//			 getServiceProvider().getApplicationManagementService().deleteApplication("stage3.6.tarent.buero", app);
//	 }
	 
//	 List<Group> groups = getServiceProvider().getInternalRoleScopeManagementService().getInternalRoleScopesForAccount(SecurityContextHo, InternalRoleType.SECTION_ADMIN)
//	 for (Group group : groups) {
//		 System.out.println(group.getName());
//	 }
//	 
	//  try{
//	Account account = this.getServiceProvider().getAccountManagementService().getAccountByUsername("stage3.15.tarent.buero", "t.schmitz@tarent.de");
//	this.getServiceProvider().getAccountManagementService().deleteAccount(account.getUuid());
//	  if(account == null){
//		  account = new Account("test1", "test1", client);
//		  account = getServiceProvider().getAccountManagementService().setAccount(client.getName(), account);
//	  }
//  } catch(Exception e){
//	  account = new Account("test1", "test1", client);
//	  account = getServiceProvider().getAccountManagementService().setAccount(client.getName(), account);
//  }
// 
//  
//  //getServiceProvider().getAccountManagementService().setAccount(client.getName(), account);
//  getServiceProvider().getDirectoryServiceSynchronizationService().synchroniseAccount(account.getUuid());
//  
//  List<ValueSet> values = getServiceProvider().getPrivateDataManagementService().getValues(account.getUuid());
//  
//  boolean valueSynced = false;
//  for (ValueSet valueSet : values) {
//   if (valueSet.getName().equals("testsync")) {
//    Assert.assertTrue(valueSet.getValue("testsyncatt").getValue().equals("555555"));
//    valueSynced = true;
//   }
//  }
//  Assert.assertTrue(valueSynced);
  
  // Delete all existing clients to prevent duplicate client exceptions
//  List clients = getServiceProvider().getClientManagementService().getClients(null).getResultList();
//  for (Client client : clients)
//   getServiceProvider().getClientManagementService().deleteClient(client);
  
  // Creating a client
//  Client client0 = new Client("stage3.15.tarent.buero","stage3.15.tarent.buero");
//	 Client client1 = new Client("stage3.6.tarent.buero-employee","stage3.6.tarent.buero-employee");
//  Client client2 = new Client("stage3.7.tarent.buero","stage3.7.tarent.buero");
//  Client client3 = new Client("stage3.7.tarent.buero-employee","stage3.7.tarent.buero-employee");
//  Client client4 = new Client("stage3.9.tarent.buero","stage3.9.tarent.buero");
//  Client client5 = new Client("stage3.9.tarent.buero-employee","stage3.9.tarent.buero-employee");
//  Client client6 = new Client("service.brandenburg.de","service.brandenburg.de");
//  client0 = getServiceProvider().getClientManagementService().setClient(0, client0);
//  client1 = getServiceProvider().getClientManagementService().setClient(0, client1);
//	 client = getServiceProvider().getClientManagementService().setClient(0, client2);
//  client = getServiceProvider().getClientManagementService().setClient(0, client3);
//  client = getServiceProvider().getClientManagementService().setClient(0, client4);
//  client = getServiceProvider().getClientManagementService().setClient(0, client5);
//  client = getServiceProvider().getClientManagementService().setClient(0, client6);
//  Client client = getServiceProvider().getClientManagementService().getClientByName("stage3.7.tarent.buero-employees");
  
  // Creating applications
//  Application application1 = new Application("application1", "Test-Anwendung 1", client);
//  Application application2 = new Application("application2", "Test-Anwendung 2", client);
//  application1 = getServiceProvider().getApplicationManagementService().setApplication(client.getName(), application1);
//  application2 = getServiceProvider().getApplicationManagementService().setApplication(client.getName(), application2);
  
//  // Creating users
//  Account account1 = new Account("Angela", "Angela", client);
//  Account account2 = new Account("Bert", "Bert", client);
//  Account account3 = new Account("Claus", "Claus", client);
//  Account account4 = new Account("Dieter", "Dieter", client);
//  account1 = getServiceProvider().getAccountManagementService().setAccount(account1);
//  account2 = getServiceProvider().getAccountManagementService().setAccount(account2);
//  account3 = getServiceProvider().getAccountManagementService().setAccount(account3);
//  account4 = getServiceProvider().getAccountManagementService().setAccount(account4);
  
  // creating user attributes
//  AttributeGroup name = new AttributeGroup("name" , "Name");
//  name.addAttribute(new Attribute("salutation", "Anrede"));
//  name.addAttribute(new Attribute("firstname", "Vorname"));
//  name.addAttribute(new Attribute("lastname", "Nachname"));
//  getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);
  
//  // creating user data
//  ValueSet testAccountName1 = new ValueSet(name.getName());
//  testAccountName1.addValue(new Value("salutation", "Frau"));
//  testAccountName1.addValue(new Value("firstname", "Angela"));
//  testAccountName1.addValue(new Value("lastname", "Merkur"));
//  getServiceProvider().getPrivateDataManagementService().setValues(account1.getUuid(), testAccountName1);
//  ValueSet testAccountName2 = new ValueSet(name.getName());
//  testAccountName2.addValue(new Value("salutation", "Herr"));
//  testAccountName2.addValue(new Value("firstname", "Bert"));
//  testAccountName2.addValue(new Value("lastname", "Merkur"));
//  getServiceProvider().getPrivateDataManagementService().setValues(account2.getUuid(), testAccountName2);
//  ValueSet testAccountName3 = new ValueSet(name.getName());
//  testAccountName3.addValue(new Value("salutation", "Herr"));
//  testAccountName3.addValue(new Value("firstname", "Claus"));
//  testAccountName3.addValue(new Value("lastname", "Hermes"));
//  getServiceProvider().getPrivateDataManagementService().setValues(account3.getUuid(), testAccountName3);
//  ValueSet testAccountName4 = new ValueSet(name.getName());
//  testAccountName4.addValue(new Value("salutation", "Herr"));
//  testAccountName4.addValue(new Value("firstname", "Dieter"));
//  testAccountName4.addValue(new Value("lastname", "Admin"));
//  getServiceProvider().getPrivateDataManagementService().setValues(account4.getUuid(), testAccountName4);
//  
//  // creating roles
//  Role role1 = new Role("VKS_MdJ", application1, "VKS_MdJ");
//  Role role2 = new Role("VKS_LT", application1, "VKS_LT");
//  Role role3 = new Role("VKS_Admin", application1, "VKS_Admin");
//  role1 = getServiceProvider().getGroupAndRoleManagementService().setRole(role1);
//  role2 = getServiceProvider().getGroupAndRoleManagementService().setRole(role2);
//  role3 = getServiceProvider().getGroupAndRoleManagementService().setRole(role3);
//  
//  // creating role assignments  
//  getServiceProvider().getGroupAndRoleManagementService().addUserToRole(account1.getUuid(), role1.getId(), "default");
//  getServiceProvider().getGroupAndRoleManagementService().addUserToRole(account2.getUuid(), role1.getId(), "default");
//  getServiceProvider().getGroupAndRoleManagementService().addUserToRole(account3.getUuid(), role2.getId(), "default");
//  getServiceProvider().getGroupAndRoleManagementService().addUserToRole(account4.getUuid(), role3.getId(), "default");
//  
//  // creating groups
//  Group group1 = new Group("VKS_MdJ", false, client, "VKS_MdJ");
//  Group group2 = new Group("VKS_LT", false, client, "VKS_LT");
//  Group group3 = new Group("VKS_Admin", false, client, "VKS_Admin");
//  group1 = getServiceProvider().getGroupAndRoleManagementService().setGroup(group1);
//  group2 = getServiceProvider().getGroupAndRoleManagementService().setGroup(group2);
//  group3 = getServiceProvider().getGroupAndRoleManagementService().setGroup(group3);
//  
//  // creating group assignments
//  getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account1.getUuid(), group1.getId());
//  getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account2.getUuid(), group1.getId());
//  getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account3.getUuid(), group2.getId());
//  getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account4.getUuid(), group3.getId());
//  
//  // creating org unit example
//  // Landtag -> Gruppe VKS_LT
//  OrgUnit orgUnit = new OrgUnit();
//  orgUnit.setGroup(group2);
//  orgUnit.setName("Landtag");
//  orgUnit = getServiceProvider().getOrganisationalUnitManagementService().setOrgUnit(orgUnit, client.getClientName());
//  
//  // retrieve test data - private data
//  List values1 = getServiceProvider().getPrivateDataManagementService().getValues(account1.getUuid());
//  System.out.println(values1.toString());
//  List values2 = getServiceProvider().getPrivateDataManagementService().getValues(account1.getUuid());
//  System.out.println(values2.toString());
//  List values3 = getServiceProvider().getPrivateDataManagementService().getValues(account1.getUuid());
//  System.out.println(values3.toString());
//  
//  // retrieve test data - roles
//  List roles1 = getServiceProvider().getGroupAndRoleManagementService().getRolesByUserAndApplication(account1.getUuid(), application1.getId(), "default");
//  System.out.println(roles1.toString());
//  List roles2 = getServiceProvider().getGroupAndRoleManagementService().getRolesByUserAndApplication(account2.getUuid(), application1.getId(), "default");
//  System.out.println(roles2.toString());
//  List roles3 = getServiceProvider().getGroupAndRoleManagementService().getRolesByUserAndApplication(account3.getUuid(), application1.getId(), "default");
//  System.out.println(roles3.toString());
//  List roles4 = getServiceProvider().getGroupAndRoleManagementService().getRolesByUserAndApplication(account4.getUuid(), application1.getId(), "default");
//  System.out.println(roles4.toString());
//  
//  // retrieve test data - groups
//  List groups1 = getServiceProvider().getGroupAndRoleManagementService().getGroupsByUser(account1.getUuid());
//  System.out.println(groups1.toString());
//  List groups2 = getServiceProvider().getGroupAndRoleManagementService().getGroupsByUser(account2.getUuid());
//  System.out.println(groups2.toString());
//  List groups3 = getServiceProvider().getGroupAndRoleManagementService().getGroupsByUser(account3.getUuid());
//  System.out.println(groups3.toString());
//  List groups4 = getServiceProvider().getGroupAndRoleManagementService().getGroupsByUser(account4.getUuid());
//  System.out.println(groups4.toString());
 }
 
 protected ServiceProvider serviceProvider;
 protected ServiceProvider getServiceProvider() {
  if (serviceProvider == null)
   serviceProvider = new ServiceProviderRemote();
  return serviceProvider;
 }

 @Override
 protected ClientManagement getService() {
  return getServiceProvider().getClientManagementService();
 }

}
