package org.evolvis.idm.test.service;

import org.evolvis.idm.identity.account.service.UserManagement;
import org.junit.Test;

public class UserManagementTestRemote extends UserManagementAbstractTest {

	/**
	 * Test to fix Eclipse JUnit bug with abstract test cases
	 */
	@Test
	public void workaroundTest() {
		// Nothing.
	}

	@Override
	protected UserManagement getService() {
		return getServiceProvider().getUserManagementService();
	}

	protected ServiceProvider serviceProvider;
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderRemote();
		return serviceProvider;
	}
}
