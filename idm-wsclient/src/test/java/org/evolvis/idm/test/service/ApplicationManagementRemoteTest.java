package org.evolvis.idm.test.service;

import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.junit.Test;

public class ApplicationManagementRemoteTest extends ApplicationManagementAbstractTest {
	
	/**
	 * Test to fix Eclipse JUnit bug with abstract test cases
	 */
	@Test
	public void workaroundTest() {
		// Nothing.
	}

	@Override
	protected ApplicationManagement getService() {
		return getServiceProvider().getApplicationManagementService();
	}

	protected ServiceProvider serviceProvider;
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderRemote();
		return serviceProvider;
	}
}
