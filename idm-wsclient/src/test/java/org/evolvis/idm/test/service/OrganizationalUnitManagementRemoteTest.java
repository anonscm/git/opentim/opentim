/**
 * 
 */
package org.evolvis.idm.test.service;

import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.test.service.OrganizationalUnitManagementAbstractTest;
import org.evolvis.idm.test.service.ServiceProvider;

/**
 * @author Jens Neumaier
 * @author Yorka Neumann, tarent GmbH
 *
 */
public class OrganizationalUnitManagementRemoteTest extends OrganizationalUnitManagementAbstractTest {

	@Override
	protected OrganizationalUnitManagement getService() {
		return getServiceProvider().getOrganisationalUnitManagementService();
	}


	protected ServiceProvider serviceProvider;
	
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderRemote();
		return serviceProvider;
	}
}
