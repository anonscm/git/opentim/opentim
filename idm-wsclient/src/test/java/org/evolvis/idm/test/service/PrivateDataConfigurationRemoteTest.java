package org.evolvis.idm.test.service;

import org.evolvis.idm.identity.privatedata.service.PrivateDataConfiguration;
import org.evolvis.idm.test.service.PrivateDataConfigurationAbstractTest;
import org.evolvis.idm.test.service.ServiceProvider;
import org.junit.Test;

public class PrivateDataConfigurationRemoteTest extends PrivateDataConfigurationAbstractTest {
	/**
	 * Test to fix Eclipse JUnit bug with abstract test cases
	 */
	@Test
	public void workaroundTest() {
		// Nothing.
	}

	@Override
	protected PrivateDataConfiguration getService() {
		return getServiceProvider().getPrivateDataConfigurationService();
	}

	protected ServiceProvider serviceProvider;
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderRemote();
		return serviceProvider;
	}
}
