package org.evolvis.idm.test.service;

import java.util.ResourceBundle;

import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.identity.privatedata.service.PrivateDataConfiguration;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement;
import org.evolvis.idm.synchronization.service.DirectoryServiceSynchronization;
import org.evolvis.idm.ws.InternalWsClientFactory;
import org.evolvis.idm.identity.applicationdata.service.ApplicationMetaDataManagement;

public class ServiceProviderRemote implements ServiceProvider {

	public ApplicationManagement getApplicationManagementService() {
		return InternalWsClientFactory.getInstance().getApplicationManagementService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}

	public ClientManagement getClientManagementService() {
		return InternalWsClientFactory.getInstance().getClientManagementService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}

	public GroupAndRoleManagement getGroupAndRoleManagementService() {
		return InternalWsClientFactory.getInstance().getGroupAndRoleManagementService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}

	public PrivateDataConfiguration getPrivateDataConfigurationService() {
		return InternalWsClientFactory.getInstance().getPrivateDataConfigurationService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}

	public PrivateDataManagement getPrivateDataManagementService() {
		return InternalWsClientFactory.getInstance().getPrivateDataManagementService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}

	public AccountManagement getAccountManagementService() {
		return InternalWsClientFactory.getInstance().getAccountManagementService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}

	public InternalRoleManagement getInternalRoleManagementService() {
		return InternalWsClientFactory.getInstance().getInternalRoleManagementService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}

	public InternalRoleScopeManagement getInternalRoleScopeManagementService() {
		return InternalWsClientFactory.getInstance().getInternalRoleScopeManagementService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}

	public OrganizationalUnitManagement getOrganisationalUnitManagementService() {
		return InternalWsClientFactory.getInstance().getOrganizationalUnitManagementService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}

	public ApplicationMetaDataManagement getApplicationMetaDataManagementService() {
		return InternalWsClientFactory.getInstance().getApplicationMetaDataManagementService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}
	
	public DirectoryServiceSynchronization getDirectoryServiceSynchronizationService() {
		return InternalWsClientFactory.getInstance().getDirectoryServiceSynchronizationService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}

	@Override
	public UserManagement getUserManagementService() {
		return InternalWsClientFactory.getInstance().getUserManagementService(
				ResourceBundle.getBundle(InternalWsClientFactory.class.getName()).getString(InternalWsClientFactory.ENDPOINT_URL_PROPERTY));
	}
}
