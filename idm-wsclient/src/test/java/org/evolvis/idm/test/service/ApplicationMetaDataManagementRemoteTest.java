package org.evolvis.idm.test.service;

import org.evolvis.idm.test.service.ApplicationMetaDataManagementAbstractTest;
import org.evolvis.idm.test.service.ServiceProvider;
import org.evolvis.idm.identity.applicationdata.service.ApplicationMetaDataManagement;
import org.junit.Test;

public class ApplicationMetaDataManagementRemoteTest extends ApplicationMetaDataManagementAbstractTest {

	
	/**
	 * Test to fix Eclipse JUnit bug with abstract test cases
	 */
	@Test
	public void workaroundTest() {
		// Nothing.
	}
	
	@Override
	protected ApplicationMetaDataManagement getService() {
		return getServiceProvider().getApplicationMetaDataManagementService();
	}


	protected ServiceProvider serviceProvider;
	
	
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderRemote();
		return serviceProvider;
	}

}
