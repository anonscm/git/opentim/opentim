package org.evolvis.idm.test.service;

import org.evolvis.idm.relation.permission.service.InternalRoleManagement;
import org.evolvis.idm.test.service.InternalRoleManagementAbstractTest;
import org.evolvis.idm.test.service.ServiceProvider;

public class InternalRoleManagementRemoteTest extends InternalRoleManagementAbstractTest {

	@Override
	protected InternalRoleManagement getService() {
		return getServiceProvider().getInternalRoleManagementService();
	}

	protected ServiceProvider serviceProvider;

	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderRemote();
		return serviceProvider;
	}

}
