package org.evolvis.idm.test.service;

import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.test.service.PrivateDataManagementAbstractTest;
import org.evolvis.idm.test.service.ServiceProvider;
import org.junit.Test;

public class PrivateDataManagementRemoteTest extends PrivateDataManagementAbstractTest {
	/**
	 * Test to fix Eclipse JUnit bug with abstract test cases
	 */
	@Test
	public void workaroundTest() {
		// Nothing.
	}

	@Override
	protected PrivateDataManagement getService() {
		return getServiceProvider().getPrivateDataManagementService();
	}

	protected ServiceProvider serviceProvider;
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderRemote();
		return serviceProvider;
	}
}
