package org.evolvis.idm.test.service;

import java.io.IOException;

import junit.framework.Assert;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.util.RestClientUtil;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.opensso.rest.RestClient;
import org.evolvis.opensso.rest.constant.UserAttribute;
import org.evolvis.opensso.rest.constant.UserStatus;
import org.evolvis.opensso.rest.service.CreateUser;
import org.evolvis.opensso.rest.service.DeleteUser;
import org.evolvis.opensso.rest.service.Search;
import org.junit.Test;

public class ChangeEmailTest {
	
	protected ServiceProvider serviceProvider;
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderRemote();
		return serviceProvider;
	}

	@Test
	public void changeEmail() throws Exception {
		String uuid = "1239f7b3-a04f-4b5a-ad3d-c454f21c090d";
		String password = "Test1234";
		
		ClientPropertyMap clientPropertyMap = getServiceProvider().getClientManagementService().getClientPropertyMap("stage3.6.tarent.buero");
		RestClient restClient = RestClientUtil.createClient(clientPropertyMap);
		
		Account account = getServiceProvider().getAccountManagementService().getAccountByUuid(uuid);
		String oldName = account.getUsername();
		String newName = "changemail2@tarent.de";
		Assert.assertEquals("changemail@tarent.de", oldName);
		
		String clientName = "stage3.6.tarent.buero";
		String realm = "/" + clientName;
		

		
		try {
			/*
			 * BEGIN deleteOpenSsoUser
			 */
			
			Search search = restClient.callService(new Search(realm).setAttr(UserAttribute.USER_UUID, uuid));
			
			// trying to create new user first
			

			if (search.success() && search.getUid() != null) {

				DeleteUser deleteUser = restClient.callService(new DeleteUser(realm, search.getUid()));

				if (!deleteUser.success()) {
					// TODO exception / rollback idm user
					System.out.println("FAILED!!!");
				}
			}

			/*
			 * END deleteOpenSsoUser
			 */
		} catch (Exception e) {
			System.out.println("FAILED!!! " + e.toString());
			// TODO exception
		}
		
		/*
		 * BEGIN createOpenSsoUser
		 */
		
		String userStatus = UserStatus.ACTIVE;

		try {
			CreateUser createUser = restClient.callService(new CreateUser(realm, newName, password, ".", ".", newName, userStatus, uuid));

			if (!createUser.success()) {
				throw new BackendException("createOpenSsoUser() create user failed, error=" + createUser.getError());
			}
		} catch (IOException ioe) {
			throw new BackendException("createOpenSsoUser() failed with IOException, error=" + ioe.getLocalizedMessage());
		}
		/*
		 * END createOpenSsoUser
		 */
		
		// rollback idm user changes
		try {
			account.setClient(getServiceProvider().getClientManagementService().getClientByName("stage3.6.tarent.buero"));
			account.setUsername(newName);
			account = getServiceProvider().getAccountManagementService().setAccount(clientName, account);
			Assert.assertEquals(newName, account.getUsername());
		} catch (BackendException be) {
			// TODO exception
			Assert.fail();
		}
	}
}
