package org.evolvis.idm.test.service;

import java.util.ArrayList;
import java.util.List;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.ws.ExternalWsClientFactory;

public class CITTest {
	
//	@Test
	public void citTest() throws BackendException {
		String endpointURI = "http://cit-oss.tarent.net:8080/idm-backend";
		String clientName = "cit.externe.tarent.net";
		String applicationName = "application1";
		String orgUnitName = "Test-Organisation";
		String roleName = "Test-Rolle";
		
		List<Account> users = ExternalWsClientFactory.getInstance().getMetaDataRetrievalService(endpointURI).getUsersByOrgUnitAndRole(clientName, applicationName, orgUnitName, roleName);
		
		// Alternative 1 - AttributeGroups abfragen
		AttributeGroup nameAttributeGroup = ExternalWsClientFactory.getInstance().getPrivateDataRetrievalService(endpointURI).getAttributeGroupByName(clientName, "name");
		List<AttributeGroup> requestedAttributeGroups = new ArrayList<AttributeGroup>();
		requestedAttributeGroups.add(nameAttributeGroup);
		
		for (Account account : users) {
			System.out.println("Benutzername: " + account.getUsername());
			ValueSet name = ExternalWsClientFactory.getInstance().getPrivateDataRetrievalService(endpointURI).getValuesByAttributes(applicationName, account.getUuid(), requestedAttributeGroups).get(0);
			System.out.println("Vorname: " + name.getValue("firstname").getValue() + " Nachname: " + name.getValue("lastname").getValue());
		}
		
		// Alternative 2 - Alle Werte abholen, gewünschte Gruppe suchen
		for (Account account : users) {
			System.out.println("Benutzername: " + account.getUsername());
			List<ValueSet> values = ExternalWsClientFactory.getInstance().getPrivateDataRetrievalService(endpointURI).getValues(applicationName, account.getUuid());
			for (ValueSet valueSet : values) {
				if (valueSet.getName().equals("name"))
					System.out.println("Vorname: " + valueSet.getValue("firstname").getValue() + " Nachname: " + valueSet.getValue("lastname").getValue());
			}
		}
	}
}
