package org.evolvis.idm.test.service;

import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.test.service.ServiceProvider;
import org.evolvis.idm.test.service.AccountManagementAbstractTest;
import org.junit.Test;

public class AccountManagementTestRemote extends AccountManagementAbstractTest {

	/**
	 * Test to fix Eclipse JUnit bug with abstract test cases
	 */
	@Test
	public void workaroundTest() {
		// Nothing.
	}

	@Override
	protected AccountManagement getService() {
		return getServiceProvider().getAccountManagementService();
	}

	protected ServiceProvider serviceProvider;
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderRemote();
		return serviceProvider;
	}
}
