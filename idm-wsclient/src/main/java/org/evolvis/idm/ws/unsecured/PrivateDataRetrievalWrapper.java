package org.evolvis.idm.ws.unsecured;

import java.util.List;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;

public class PrivateDataRetrievalWrapper extends AbstractServiceWrapper<PrivateDataRetrieval> implements PrivateDataRetrieval {

	public PrivateDataRetrievalWrapper(Service service, String endpointURI) {
		super(PrivateDataRetrieval.class, service, endpointURI);
	}

	@Override
	public AttributeGroup getAttributeGroupByName(String clientName, String attributeGroupName) throws BackendException {
		return servicePort.get().getAttributeGroupByName(clientName, attributeGroupName);
	}

	@Override
	public List<AttributeGroup> getAttributes(String clientName) throws BackendException {
		return servicePort.get().getAttributes(clientName);
	}

	@Override
	public Value getValueWithBinaryAttachmentById(String uuid, Long valueId) throws BackendException, IllegalRequestException {
		return servicePort.get().getValueWithBinaryAttachmentById(uuid, valueId);
	}

	@Override
	public List<ValueSet> getValues(String applicationName, String uuid) throws BackendException {
		return servicePort.get().getValues(applicationName, uuid);
	}

	@Override
	public List<ValueSet> getValuesByAttributes(String applicationName, String uuid, List<AttributeGroup> attributes) throws BackendException {
		return servicePort.get().getValuesByAttributes(applicationName, uuid, attributes);
	}

	@Override
	public ValueSet getValuesById(String applicationName, String uuid, Long id) throws BackendException {
		return servicePort.get().getValuesById(applicationName, uuid, id);
	}

	@Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

}
