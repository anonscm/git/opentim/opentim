package org.evolvis.idm.ws;

import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.identity.account.service.AccountManagementWrapper;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.identity.account.service.UserManagementWrapper;
import org.evolvis.idm.identity.applicationdata.service.ApplicationMetaDataManagement;
import org.evolvis.idm.identity.applicationdata.service.ApplicationMetaDataManagementWrapper;
import org.evolvis.idm.identity.privatedata.service.PrivateDataConfiguration;
import org.evolvis.idm.identity.privatedata.service.PrivateDataConfigurationWrapper;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagementWrapper;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagementWrapper;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.relation.multitenancy.service.ClientManagementWrapper;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagementWrapper;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagementWrapper;
import org.evolvis.idm.relation.permission.service.InternalRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleManagementWrapper;
import org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleScopeManagementWrapper;
import org.evolvis.idm.synchronization.service.DirectoryServiceSynchronization;
import org.evolvis.idm.synchronization.service.DirectoryServiceSynchronizationWrapper;

public class InternalWsClientFactory extends AbstractWsClientFactory {
	private static InternalWsClientFactory instance = new InternalWsClientFactory();
	
	public static InternalWsClientFactory getInstance() {
		return instance;
	}
	
	private InternalWsClientFactory() {
		
	}
	
	public AccountManagement getAccountManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(AccountManagementWrapper.class, AccountManagement.class, endpointURI + "/AccountManagementService");
	}
	
	public ApplicationManagement getApplicationManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(ApplicationManagementWrapper.class, ApplicationManagement.class, endpointURI + "/ApplicationManagementService");
	}

	public ApplicationMetaDataManagement getApplicationMetaDataManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(ApplicationMetaDataManagementWrapper.class, ApplicationMetaDataManagement.class, endpointURI + "/ApplicationMetaDataManagementService");
	}

	public ClientManagement getClientManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(ClientManagementWrapper.class, ClientManagement.class, endpointURI + "/ClientManagementService");
	}
	
	public DirectoryServiceSynchronization getDirectoryServiceSynchronizationService(String endpointURI) {
		return getInstance().getWrapperInstance(DirectoryServiceSynchronizationWrapper.class, DirectoryServiceSynchronization.class, endpointURI + "/DirectoryServiceSynchronizationService");
	}
	
	public GroupAndRoleManagement getGroupAndRoleManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(GroupAndRoleManagementWrapper.class, GroupAndRoleManagement.class, endpointURI + "/GroupAndRoleManagementService");
	}
	
	public InternalRoleManagement getInternalRoleManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(InternalRoleManagementWrapper.class, InternalRoleManagement.class, endpointURI + "/InternalRoleManagementService");
	}
	
	public InternalRoleScopeManagement getInternalRoleScopeManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(InternalRoleScopeManagementWrapper.class, InternalRoleScopeManagement.class, endpointURI + "/InternalRoleScopeManagementService");
	}
	
	public OrganizationalUnitManagement getOrganizationalUnitManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(OrganizationalUnitManagementWrapper.class, OrganizationalUnitManagement.class, endpointURI + "/OrganizationalUnitManagementService");
	}
	
	public PrivateDataConfiguration getPrivateDataConfigurationService(String endpointURI) {
		return getInstance().getWrapperInstance(PrivateDataConfigurationWrapper.class, PrivateDataConfiguration.class, endpointURI + "/PrivateDataConfigurationService");
	}
	
	public PrivateDataManagement getPrivateDataManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(PrivateDataManagementWrapper.class, PrivateDataManagement.class, endpointURI + "/PrivateDataManagementService");
	}
	
	public UserManagement getUserManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(UserManagementWrapper.class, UserManagement.class, endpointURI + "/UserManagementService");
	}
}
