package org.evolvis.idm.ws;

import org.evolvis.idm.ws.unsecured.ApplicationMetaDataManagement;
import org.evolvis.idm.ws.unsecured.ApplicationMetaDataManagementWrapper;
import org.evolvis.idm.ws.unsecured.MetaDataRetrieval;
import org.evolvis.idm.ws.unsecured.MetaDataRetrievalWrapper;
import org.evolvis.idm.ws.unsecured.PrivateDataRetrieval;
import org.evolvis.idm.ws.unsecured.PrivateDataRetrievalWrapper;
import org.evolvis.idm.ws.unsecured.RoleManagement;
import org.evolvis.idm.ws.unsecured.RoleManagementWrapper;

public class ExternalWsClientFactory extends AbstractWsClientFactory {
	private static ExternalWsClientFactory instance = new ExternalWsClientFactory();
	
	private ExternalWsClientFactory() {
		
	}
	
	public ApplicationMetaDataManagement getApplicationMetaDataManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(ApplicationMetaDataManagementWrapper.class, ApplicationMetaDataManagement.class, endpointURI + "/ApplicationMetaDataManagementService");
	}
	
	public MetaDataRetrieval getMetaDataRetrievalService(String endpointURI) {
		return getInstance().getWrapperInstance(MetaDataRetrievalWrapper.class, MetaDataRetrieval.class, endpointURI + "/MetaDataRetrievalService");
	}
	
	public PrivateDataRetrieval getPrivateDataRetrievalService(String endpointURI) {
		return getInstance().getWrapperInstance(PrivateDataRetrievalWrapper.class, PrivateDataRetrieval.class, endpointURI + "/PrivateDataRetrievalService");
	}
	
	public RoleManagement getRoleManagementService(String endpointURI) {
		return getInstance().getWrapperInstance(RoleManagementWrapper.class, RoleManagement.class, endpointURI + "/RoleManagementService");
	}
	
	public static ExternalWsClientFactory getInstance() {
		return instance;
	}

}
