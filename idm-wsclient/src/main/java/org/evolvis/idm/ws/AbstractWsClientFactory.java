package org.evolvis.idm.ws;

import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.evolvis.idm.common.service.AbstractServiceWrapper;

/** 
 * @author Jens Neumaier
 * @author Tino Rink
 */
public class AbstractWsClientFactory {
	public static final String ENDPOINT_URL_PROPERTY = "endpointURL";
	
	private ThreadLocal<Map<String, Service>> serviceLookUp = new ThreadLocal<Map<String, Service>>() {
	    protected java.util.Map<String, Service> initialValue() {
	        return new HashMap<String, Service>();
	    };
	};
	
	public static Map<String, Object> wrapperLookUp = new HashMap<String, Object>();

	public AbstractWsClientFactory() {
//		System.setProperty("org.jboss.wsse.keyStore", "/home/jneuma/temp/keystore/keystore.jks");
//		System.setProperty("org.jboss.wsse.keyStorePassword", "changeit");
//		System.setProperty("org.jboss.wsse.keyStoreType", "x509v3");
//		System.setProperty("org.jboss.wsse.trustStore", "/home/jneuma/temp/keystore/cacerts.jks");
//		System.setProperty("org.jboss.wsse.trustStorePassword", "changeit");
//		System.setProperty("org.jboss.wsse.trustStoreType", "x509v3");
//		System.setProperty("javax.xml.ws.spi.Provider", "org.jboss.ws.core.jaxws.spi.ProviderImpl");
//		wrapperLookUp.set(new HashMap<String, Object>());
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getPort(Class<? extends T> serviceInterface, String endpointURI, String wsseConfig) {
		if (wrapperLookUp.containsKey(endpointURI))
			return (T)wrapperLookUp.get(endpointURI);
		
		T port = createService(serviceInterface);
		
		((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURI);
		
//		URL securityURL = WsClientFactory.class.getResource("jboss-wsse-client.xml");
//		((StubExt) port).setSecurityConfig(securityURL.toExternalForm());
//		((StubExt) port).setSecurityConfig(wsseConfig);
//		((StubExt) port).setConfigName("Standard WSSecurity Client");
//		((BindingProvider) port).getRequestContext().put(StubExt.PROPERTY_AUTH_TYPE, StubExt.PROPERTY_AUTH_TYPE_WSSE);
		
		wrapperLookUp.put(endpointURI, port);
		
		return port;
	}
	
	protected Service getService(Class<?> serviceInterface) {
		if (serviceLookUp.get().containsKey(serviceInterface.getName()))
			return serviceLookUp.get().get(serviceInterface.getName());
		
		URL wsdlURL = this.getClass().getResource(serviceInterface.getSimpleName() + "Service.wsdl"); 
		QName serviceName = new QName("http://idm.evolvis.org", serviceInterface.getSimpleName()+"Service");
		Service service = Service.create(wsdlURL, serviceName);
				
		serviceLookUp.get().put(serviceInterface.getName(), service);
		
		return service;
	}
	
	public static <T> T createService(Class<? extends T> serviceInterface) {
		URL wsdlURL = AbstractWsClientFactory.class.getResource(serviceInterface.getSimpleName() + "Service.wsdl"); 
		QName serviceName = new QName("http://idm.evolvis.org", serviceInterface.getSimpleName()+"Service");
		Service service = Service.create(wsdlURL, serviceName);
		T port = (T) service.getPort(serviceInterface);
		
		return port;
	}
	
	@SuppressWarnings("unchecked")
	public <T, S extends AbstractServiceWrapper<T>> S getWrapperInstance(Class<S> wrapperClass, Class<T> serviceInterface, String endpointURI) {
		if (wrapperLookUp.containsKey(endpointURI))
			return (S) wrapperLookUp.get(endpointURI);
		else {
			Service service = getService(serviceInterface);
			
			Class[] constructorArgs = new Class[2];
			constructorArgs[0] = Service.class;
			constructorArgs[1] = String.class;
			Constructor wrapperConstructor;
			
			AbstractServiceWrapper<T> wrapper = null;
			
			try {
				wrapperConstructor = wrapperClass.getConstructor(constructorArgs);
				wrapper = (AbstractServiceWrapper<T>) wrapperConstructor.newInstance(service, endpointURI);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			wrapperLookUp.put(endpointURI, wrapper);
			
			return (S) wrapper;
		}
	}
}
