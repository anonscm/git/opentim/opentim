package org.evolvis.idm.ws.unsecured;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.identity.applicationdata.model.AppMetaData;

public class ApplicationMetaDataManagementWrapper extends AbstractServiceWrapper<ApplicationMetaDataManagement> implements ApplicationMetaDataManagement {

	public ApplicationMetaDataManagementWrapper(Service service, String endpointURI) {
		super(ApplicationMetaDataManagement.class, service, endpointURI);
	}

	@Override
	public void deleteApplicationMetaData(String applicationName, String uuid) throws BackendException, IllegalRequestException {
		servicePort.get().deleteApplicationMetaData(applicationName, uuid);
	}

	@Override
	public AppMetaData getApplicationMetaData(String applicationName, String uuid) throws BackendException, IllegalRequestException {
		return servicePort.get().getApplicationMetaData(applicationName, uuid);
	}

	@Override
	public String getApplicationMetaDataValue(String applicationName, String uuid, String metaDataKey) throws BackendException, IllegalRequestException {
		return servicePort.get().getApplicationMetaDataValue(applicationName, uuid, metaDataKey);
	}

	@Override
	public void removeApplicationMetaDataValue(String applicationName, String uuid, String metaDataKey) throws BackendException, IllegalRequestException {
		servicePort.get().removeApplicationMetaDataValue(applicationName, uuid, metaDataKey);
	}

	@Override
	public AppMetaData setApplicationMetaData(AppMetaData appMetaData) throws BackendException, IllegalRequestException {
		return servicePort.get().setApplicationMetaData(appMetaData);
	}

	@Override
	public String setApplicationMetaDataValue(String applicationName, String uuid, String metaDataKey, String metaDataValue) throws BackendException, IllegalRequestException {
		return servicePort.get().setApplicationMetaDataValue(applicationName, uuid, metaDataKey, metaDataValue);
	}
    
    @Override
	public void fault(String arg0) {
		servicePort.get().fault(arg0);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

    @Override
	public Long ping(Long arg0) {
		return servicePort.get().ping(arg0);
	}
	
}
