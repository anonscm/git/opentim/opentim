package org.evolvis.idm.ws.unsecured;

import java.util.Calendar;
import java.util.List;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;

public class MetaDataRetrievalWrapper extends AbstractServiceWrapper<MetaDataRetrieval> implements MetaDataRetrieval {

	public MetaDataRetrievalWrapper(Service service, String endpointURI) {
		super(MetaDataRetrieval.class, service, endpointURI);
	}

	@Override
	public Account getAccountByCertificate(String arg0, String arg1) throws BackendException {
		return servicePort.get().getAccountByCertificate(arg0, arg1);
	}

	@Override
	public Account getAccountByUUID(String arg0) throws BackendException {
		return servicePort.get().getAccountByUUID(arg0);
	}

	@Override
	public Account getAccountByUsername(String arg0, String arg1) throws BackendException {
		return servicePort.get().getAccountByUsername(arg0, arg1);
	}

	@Override
	public Application getApplicationByName(String arg0, String arg1) throws BackendException {
		return servicePort.get().getApplicationByName(arg0, arg1);
	}

	@Override
	public String getCertificateByUUID(String arg0) throws BackendException {
		return servicePort.get().getCertificateByUUID(arg0);
	}

	@Override
	public List<Account> getDeactivatedUsersSince(String arg0, Calendar arg1) throws BackendException {
		return servicePort.get().getDeactivatedUsersSince(arg0, arg1);
	}

	@Override
	public Group getGroupByName(String arg0, String arg1) throws BackendException {
		return servicePort.get().getGroupByName(arg0, arg1);
	}

	@Override
	public List<Group> getGroups(String arg0) throws BackendException {
		return servicePort.get().getGroups(arg0);
	}

	@Override
	public List<Group> getGroupsByUser(String arg0) throws BackendException {
		return servicePort.get().getGroupsByUser(arg0);
	}

	@Override
	public OrgUnit getOrgUnitByName(String arg0, String arg1) throws BackendException {
		return servicePort.get().getOrgUnitByName(arg0, arg1);
	}

	@Override
	public List<OrgUnit> getOrgUnitsByMetaDataValue(String arg0, String arg1) throws BackendException {
		return servicePort.get().getOrgUnitsByMetaDataValue(arg0, arg1);
	}

	@Override
	public List<OrgUnit> getOrgUnitsByUser(String arg0) throws BackendException {
		return servicePort.get().getOrgUnitsByUser(arg0);
	}

	@Override
	public List<OrgUnit> getOrganisationalUnits(String arg0) throws BackendException {
		return servicePort.get().getOrganisationalUnits(arg0);
	}

	@Override
	public Role getRoleByName(String arg0, String arg1, String arg2) throws BackendException {
		return servicePort.get().getRoleByName(arg0, arg1, arg2);
	}

	@Override
	public List<Role> getRolesByApplication(String arg0, String arg1) throws BackendException {
		return servicePort.get().getRolesByApplication(arg0, arg1);
	}

	@Override
	public List<Role> getRolesByGroup(String arg0, String arg1) throws BackendException {
		return servicePort.get().getRolesByGroup(arg0, arg1);
	}

	@Override
	public List<Role> getRolesByUser(String arg0, String arg1, String arg2) throws BackendException {
		return servicePort.get().getRolesByUser(arg0, arg1, arg2);
	}

	@Override
	public List<Account> getUsersByGroup(String arg0, String arg1) throws BackendException {
		return servicePort.get().getUsersByGroup(arg0, arg1);
	}

	@Override
	public List<Account> getUsersByOrgUnit(String arg0, String arg1) throws BackendException {
		return servicePort.get().getUsersByOrgUnit(arg0, arg1);
	}

	@Override
	public List<Account> getUsersByOrgUnitAndRole(String arg0, String arg1, String arg2, String arg3) throws BackendException {
		return servicePort.get().getUsersByOrgUnitAndRole(arg0, arg1, arg2, arg3);
	}

	@Override
	public List<Account> getUsersByRole(String arg0, String arg1, String arg2) throws BackendException {
		return servicePort.get().getUsersByRole(arg0, arg1, arg2);
	}

	@Override
	public boolean hasUserRole(String arg0, String arg1, String arg2) throws BackendException {
		return servicePort.get().hasUserRole(arg0, arg1, arg2);
	}

	@Override
	public boolean isUserActive(String arg0) throws BackendException {
		return servicePort.get().isUserActive(arg0);
	}

	@Override
	public boolean isUserInGroup(String arg0, String arg1) throws BackendException {
		return servicePort.get().isUserInGroup(arg0, arg1);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

}
