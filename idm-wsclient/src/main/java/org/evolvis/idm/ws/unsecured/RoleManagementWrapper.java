package org.evolvis.idm.ws.unsecured;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.relation.permission.model.Role;

public class RoleManagementWrapper extends AbstractServiceWrapper<RoleManagement> implements RoleManagement {

	public RoleManagementWrapper(Service service, String endpointURI) {
		super(RoleManagement.class, service, endpointURI);
	}

	@Override
	public void addGroupToRole(String clientName, String applicationName, String groupName, String roleName) throws BackendException {
		servicePort.get().addGroupToRole(clientName, applicationName, groupName, roleName);
	}

	@Override
	public void addUserToRole(String clientName, String applicationName, String uuid, String roleName) throws BackendException {
		servicePort.get().addUserToRole(clientName, applicationName, uuid, roleName);
	}

	@Override
	public void deleteRole(String clientName, String applicationName, String roleName) throws BackendException {
		servicePort.get().deleteRole(clientName, applicationName, roleName);
	}

	@Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

	@Override
	public void removeGroupFromRole(String clientName, String applicationName, String groupName, String roleName) throws BackendException {
		servicePort.get().removeGroupFromRole(clientName, applicationName, groupName, roleName);
	}

	@Override
	public void removeUserFromRole(String applicationName, String uuid, String roleName) throws BackendException {
		servicePort.get().removeUserFromRole(applicationName, uuid, roleName);
	}

	@Override
	public Role setRole(Role role) throws BackendException {
		return servicePort.get().setRole(role);
	}

}
