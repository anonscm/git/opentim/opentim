package org.evolvis.idm.relation.multitenancy.service;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.relation.multitenancy.model.Application;

public class ApplicationManagementWrapper extends AbstractServiceWrapper<ApplicationManagement> implements ApplicationManagement {

	public ApplicationManagementWrapper(Service service, String endpointURI) {
		super(ApplicationManagement.class, service, endpointURI);
	}

	@Override
	public void deleteApplication(String arg0, Application arg1) throws BackendException, AccessDeniedException, IllegalRequestException {
		servicePort.get().deleteApplication(arg0, arg1);
	}

	@Override
	public Application setApplication(String arg0, Application arg1) throws BackendException, IllegalRequestException, AccessDeniedException {
		return servicePort.get().setApplication(arg0, arg1);
	}

	@Override
	public Application getApplicationById(String arg0, Long arg1) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getApplicationById(arg0, arg1);
	}

	@Override
	public Application getApplicationByName(String arg0, String arg1) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getApplicationByName(arg0, arg1);
	}

	@Override
	public Application getApplicationByUuid(String arg0, String arg1) throws IllegalRequestException, BackendException {
		return servicePort.get().getApplicationByUuid(arg0, arg1);
	}

	@Override
	public QueryResult<Application> getApplications(String arg0, QueryDescriptor arg1) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getApplications(arg0, arg1);
	}
    
    @Override
	public void fault(String arg0) {
		servicePort.get().fault(arg0);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

    @Override
	public Long ping(Long arg0) {
		return servicePort.get().ping(arg0);
	}

}
