package org.evolvis.idm.relation.organization.service;

import java.util.List;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.organization.model.OrgUnitMap;
import org.evolvis.idm.relation.organization.model.OrgUnitMapEntryQueryResult;
import org.evolvis.idm.relation.permission.model.Group;

public class OrganizationalUnitManagementWrapper extends AbstractServiceWrapper<OrganizationalUnitManagement> implements OrganizationalUnitManagement {

	public OrganizationalUnitManagementWrapper(Service service, String endpointURI) {
		super(OrganizationalUnitManagement.class, service, endpointURI);
	}

	@Override
	public String addValueToOrgUnit(String arg0, Long arg1, String arg2, String arg3) throws BackendException, IllegalRequestException {
		return servicePort.get().addValueToOrgUnit(arg0, arg1, arg2, arg3);
	}

	@Override
	public void deleteOrgUnit(String arg0, OrgUnit arg1) throws BackendException, IllegalRequestException {
		servicePort.get().deleteOrgUnit(arg0, arg1);
	}

	@Override
	public void deleteOrgUnitById(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		servicePort.get().deleteOrgUnitById(arg0, arg1);
	}

	@Override
	public Group getGroupByOrgUnit(String arg0, String arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getGroupByOrgUnit(arg0, arg1);
	}

	@Override
	public Group getGroupByOrgUnitId(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getGroupByOrgUnitId(arg0, arg1);
	}

	@Override
	public OrgUnit getOrgUnit(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getOrgUnit(arg0, arg1);
	}

	@Override
	public OrgUnit getOrgUnitByName(String arg0, String arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getOrgUnitByName(arg0, arg1);
	}

	@Override
	public OrgUnitMap getOrgUnitMap(String arg0, Long arg1, QueryDescriptor arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().getOrgUnitMap(arg0, arg1, arg2);
	}

	@Override
	public OrgUnitMapEntryQueryResult getOrgUnitMapEntries(String arg0, Long arg1, QueryDescriptor arg2) throws IllegalRequestException, BackendException {
		return servicePort.get().getOrgUnitMapEntries(arg0, arg1, arg2);
	}

	@Override
	public QueryResult<OrgUnit> getOrgUnits(String arg0, QueryDescriptor arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getOrgUnits(arg0, arg1);
	}

	@Override
	public List<OrgUnit> getOrgUnitsByGroup(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getOrgUnitsByGroup(arg0, arg1);
	}

	@Override
	public List<OrgUnit> getOrgUnitsByUser(String arg0) throws BackendException, IllegalRequestException {
		return servicePort.get().getOrgUnitsByUser(arg0);
	}

	@Override
	public List<OrgUnit> getOrgUnitsByValue(String arg0, String arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getOrgUnitsByValue(arg0, arg1);
	}

	@Override
	public List<Account> getUsersByOrgUnit(String arg0, String arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getUsersByOrgUnit(arg0, arg1);
	}

	@Override
	public List<Account> getUsersByOrgUnitAndGroup(String arg0, String arg1, String arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().getUsersByOrgUnitAndGroup(arg0, arg1, arg2);
	}

	@Override
	public List<Account> getUsersByOrgUnitAndGroupById(String arg0, Long arg1, Long arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().getUsersByOrgUnitAndGroupById(arg0, arg1, arg2);
	}

	@Override
	public List<Account> getUsersByOrgUnitAndRole(String arg0, Long arg1, Long arg2, String arg3) throws BackendException, IllegalRequestException {
		return servicePort.get().getUsersByOrgUnitAndRole(arg0, arg1, arg2, arg3);
	}

	@Override
	public List<Account> getUsersByOrgUnitId(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getUsersByOrgUnitId(arg0, arg1);
	}

	@Override
	public String getValueFromOrgUnit(String arg0, Long arg1, String arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().getValueFromOrgUnit(arg0, arg1, arg2);
	}

	@Override
	public String removeValueFromOrgUnit(String arg0, Long arg1, String arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().removeValueFromOrgUnit(arg0, arg1, arg2);
	}

	@Override
	public OrgUnit setOrgUnit(String arg0, OrgUnit arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().setOrgUnit(arg0, arg1);
	}
    
    @Override
	public void fault(String arg0) {
		servicePort.get().fault(arg0);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

    @Override
	public Long ping(Long arg0) {
		return servicePort.get().ping(arg0);
	}

}
