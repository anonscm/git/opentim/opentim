package org.evolvis.idm.relation.multitenancy.service;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.relation.multitenancy.model.ClientQueryResult;

public class ClientManagementWrapper extends AbstractServiceWrapper<ClientManagement> implements ClientManagement {

	public ClientManagementWrapper(Service service, String endpointURI) {
		super(ClientManagement.class, service, endpointURI);
	}

	@Override
	public void deleteClient(Client arg0) throws BackendException, AccessDeniedException, IllegalRequestException {
		servicePort.get().deleteClient(arg0);
	}

	@Override
	public ClientPropertyMap getClientPropertyMap(String arg0) throws BackendException, IllegalRequestException, AccessDeniedException {
		return servicePort.get().getClientPropertyMap(arg0);
	}

	@Override
	public Long getLastUserListChangeTimeMillis(String arg0) throws BackendException, IllegalRequestException {
		return servicePort.get().getLastUserListChangeTimeMillis(arg0);
	}

	@Override
	public Client setClient(int arg0, Client arg1) throws BackendException, IllegalRequestException, AccessDeniedException {
		return servicePort.get().setClient(arg0, arg1);
	}

	@Override
	public ClientPropertyMap setClientPropertyMap(String arg0, ClientPropertyMap arg1) throws BackendException, IllegalRequestException, AccessDeniedException {
		return servicePort.get().setClientPropertyMap(arg0, arg1);
	}

	@Override
	public void updateLastChangeUserList(String arg0) throws BackendException {
		servicePort.get().updateLastChangeUserList(arg0);
	}

	@Override
	public Client getClientByName(String arg0) throws BackendException, IllegalRequestException {
		return servicePort.get().getClientByName(arg0);
	}

	@Override
	public ClientQueryResult getClients(QueryDescriptor arg0) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getClients(arg0);
	}
    
    @Override
	public void fault(String arg0) {
		servicePort.get().fault(arg0);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

    @Override
	public Long ping(Long arg0) {
		return servicePort.get().ping(arg0);
	}

}
