package org.evolvis.idm.relation.permission.service;

import java.util.List;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.model.RoleUser;
import org.evolvis.idm.relation.permission.model.UserRole;

public class GroupAndRoleManagementWrapper extends AbstractServiceWrapper<GroupAndRoleManagement> implements GroupAndRoleManagement {

	public GroupAndRoleManagementWrapper(Service service, String endpointURI) {
		super(GroupAndRoleManagement.class, service, endpointURI);
	}

	@Override
	public void addGroupToRole(String arg0, Long arg1, Long arg2, String arg3) throws BackendException, IllegalRequestException {
		servicePort.get().addGroupToRole(arg0, arg1, arg2, arg3);
	}

	@Override
	public void addUserToGroup(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		servicePort.get().addUserToGroup(arg0, arg1);
	}

	@Override
	public void addUserToRole(String arg0, Long arg1, String arg2) throws BackendException, IllegalRequestException {
		servicePort.get().addUserToRole(arg0, arg1, arg2);
	}

	@Override
	public void deleteGroup(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		servicePort.get().deleteGroup(arg0, arg1);
	}

	@Override
	public void deleteGroupFromRole(String arg0, Long arg1, Long arg2, String arg3) throws BackendException, IllegalRequestException {
		servicePort.get().deleteGroupFromRole(arg0, arg1, arg2, arg3);
	}

	@Override
	public void deleteRole(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		servicePort.get().deleteRole(arg0, arg1);
	}

	@Override
	public void deleteUserFromGroup(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		servicePort.get().deleteUserFromGroup(arg0, arg1);
	}

	@Override
	public void deleteUserFromRole(String arg0, Long arg1, String arg2) throws BackendException, IllegalRequestException {
		servicePort.get().deleteUserFromRole(arg0, arg1, arg2);
	}

	@Override
	public Client getClientByGroup(Long arg0) throws BackendException, IllegalRequestException {
		return servicePort.get().getClientByGroup(arg0);
	}

	@Override
	public Group getGroup(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getGroup(arg0, arg1);
	}

	@Override
	public Group getGroupByName(String arg0, String arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getGroupByName(arg0, arg1);
	}

	@Override
	public QueryResult<Group> getGroups(String arg0, QueryDescriptor arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getGroups(arg0, arg1);
	}

	@Override
	public List<Group> getGroupsByRole(String arg0, String arg1, String arg2, String arg3) throws BackendException, IllegalRequestException {
		return servicePort.get().getGroupsByRole(arg0, arg1, arg2, arg3);
	}

	@Override
	public List<Group> getGroupsByRoleId(String arg0, Long arg1, String arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().getGroupsByRoleId(arg0, arg1, arg2);
	}

	@Override
	public List<Group> getGroupsByUser(String arg0) throws BackendException, IllegalRequestException {
		return servicePort.get().getGroupsByUser(arg0);
	}

	@Override
	public Role getRole(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getRole(arg0, arg1);
	}

	@Override
	public Role getRoleByName(String arg0, String arg1, String arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().getRoleByName(arg0, arg1, arg2);
	}

	@Override
	public List<RoleUser> getRoleUsers(String arg0, Long arg1, String arg2) throws IllegalRequestException, BackendException {
		return servicePort.get().getRoleUsers(arg0, arg1, arg2);
	}

	@Override
	public List<Role> getRolesByApplication(String arg0, String arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getRolesByApplication(arg0, arg1);
	}

	@Override
	public List<Role> getRolesByApplicationId(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getRolesByApplicationId(arg0, arg1);
	}

	@Override
	public QueryResult<Role> getRolesByClient(String arg0, QueryDescriptor arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getRolesByClient(arg0, arg1);
	}

	@Override
	public QueryResult<Role> getRolesByClientId(String arg0, Long arg1, QueryDescriptor arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().getRolesByClientId(arg0, arg1, arg2);
	}

	@Override
	public List<Role> getRolesByGroup(String arg0, String arg1, String arg2) throws IllegalRequestException, BackendException {
		return servicePort.get().getRolesByGroup(arg0, arg1, arg2);
	}

	@Override
	public List<Role> getRolesByGroupId(String arg0, Long arg1, String arg2) throws IllegalRequestException, BackendException {
		return servicePort.get().getRolesByGroupId(arg0, arg1, arg2);
	}

	@Override
	public List<Role> getRolesByUser(String arg0, String arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getRolesByUser(arg0, arg1);
	}

	@Override
	public List<Role> getRolesByUserAndApplication(String arg0, Long arg1, String arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().getRolesByUserAndApplication(arg0, arg1, arg2);
	}

	@Override
	public QueryResult<User> getUserAccountsByGroupId(String arg0, Long arg1, QueryDescriptor arg2) throws IllegalRequestException, BackendException {
		return servicePort.get().getUserAccountsByGroupId(arg0, arg1, arg2);
	}

	@Override
	public List<UserRole> getUserRoles(String arg0, Long arg1, String arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().getUserRoles(arg0, arg1, arg2);
	}

	@Override
	public List<Account> getUsersByGroup(String arg0, String arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getUsersByGroup(arg0, arg1);
	}

	@Override
	public List<Account> getUsersByGroupAndRole(String arg0, Long arg1, Long arg2, String arg3) throws BackendException, IllegalRequestException {
		return servicePort.get().getUsersByGroupAndRole(arg0, arg1, arg2, arg3);
	}

	@Override
	public List<Account> getUsersByGroupId(String arg0, Long arg1) throws IllegalRequestException, BackendException {
		return servicePort.get().getUsersByGroupId(arg0, arg1);
	}

	@Override
	public List<Account> getUsersByRole(String arg0, String arg1, String arg2, String arg3) throws BackendException, IllegalRequestException {
		return servicePort.get().getUsersByRole(arg0, arg1, arg2, arg3);
	}

	@Override
	public List<Account> getUsersByRoleId(String arg0, Long arg1, String arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().getUsersByRoleId(arg0, arg1, arg2);
	}

	@Override
	public boolean isGroupInRole(String arg0, Long arg1, Long arg2, String arg3) throws BackendException, IllegalRequestException {
		return servicePort.get().isGroupInRole(arg0, arg1, arg2, arg3);
	}

	@Override
	public boolean isUserInGroup(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().isUserInGroup(arg0, arg1);
	}

	@Override
	public boolean isUserInRole(String arg0, String arg1, String arg2, String arg3) throws BackendException, IllegalRequestException {
		return servicePort.get().isUserInRole(arg0, arg1, arg2, arg3);
	}

	@Override
	public boolean isUserInRoleById(String arg0, Long arg1, String arg2) throws BackendException, IllegalRequestException {
		return servicePort.get().isUserInRoleById(arg0, arg1, arg2);
	}

	@Override
	public Group setGroup(String arg0, Group arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().setGroup(arg0, arg1);
	}

	@Override
	public Role setRole(String arg0, Role arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().setRole(arg0, arg1);
	}
    
    @Override
	public void fault(String arg0) {
		servicePort.get().fault(arg0);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

    @Override
	public Long ping(Long arg0) {
		return servicePort.get().ping(arg0);
	}

}
