package org.evolvis.idm.relation.permission.service;

import java.util.List;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.InternalRoleAssignment;
import org.evolvis.idm.relation.permission.model.InternalRoleType;

public class InternalRoleManagementWrapper extends AbstractServiceWrapper<InternalRoleManagement> implements InternalRoleManagement {

	public InternalRoleManagementWrapper(Service service, String endpointURI) {
		super(InternalRoleManagement.class, service, endpointURI);
	}

	@Override
	public void addUserToInternalRole(String arg0, InternalRoleType arg1) throws BackendException, IllegalRequestException {
		servicePort.get().addUserToInternalRole(arg0, arg1);
	}

	@Override
	public InternalRoleType getInternalRoleForUser(String arg0) throws IllegalRequestException, BackendException {
		return servicePort.get().getInternalRoleForUser(arg0);
	}

	@Override
	public List<InternalRoleType> getInternalRolesOfUser(String arg0) throws IllegalRequestException, BackendException {
		return servicePort.get().getInternalRolesOfUser(arg0);
	}

	@Override
	public List<Account> getUsersOfInternalRole(String arg0, InternalRoleType arg1) throws IllegalRequestException, BackendException {
		return servicePort.get().getUsersOfInternalRole(arg0, arg1);
	}

	@Override
	public void removeUserFromInternalRole(String arg0, InternalRoleType arg1) throws BackendException, IllegalRequestException {
		servicePort.get().removeUserFromInternalRole(arg0, arg1);
	}

	@Override
	public void deleteInternalRoleScope(String arg0, InternalRoleType arg1, Long arg2) throws IllegalRequestException, BackendException {
		servicePort.get().deleteInternalRoleScope(arg0, arg1, arg2);
	}

	@Override
	public List<Group> getInternalRoleScopesForAccount(String arg0, InternalRoleType arg1, QueryDescriptor arg2) throws IllegalRequestException, BackendException {
		return servicePort.get().getInternalRoleScopesForAccount(arg0, arg1, arg2);
	}

	@Override
	public List<InternalRoleAssignment> getInternalRolesForGroup(String arg0, Long arg1) throws IllegalRequestException, BackendException {
		return servicePort.get().getInternalRolesForGroup(arg0, arg1);
	}

	@Override
	public void setInternalRoleScope(Long arg0, Long arg1) throws IllegalRequestException, BackendException {
		servicePort.get().setInternalRoleScope(arg0, arg1);
	}

	@Override
	public void setInternalRoleScope(String arg0, InternalRoleType arg1, Long arg2) throws IllegalRequestException, BackendException {
		servicePort.get().setInternalRoleScope(arg0, arg1, arg2);
	}
    
    @Override
	public void fault(String arg0) {
		servicePort.get().fault(arg0);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

    @Override
	public Long ping(Long arg0) {
		return servicePort.get().ping(arg0);
	}

}
