package org.evolvis.idm.synchronization.service;

import java.util.List;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.synchronization.service.fault.SynchronizationException;

public class DirectoryServiceSynchronizationWrapper extends AbstractServiceWrapper<DirectoryServiceSynchronization> implements DirectoryServiceSynchronization {

	public DirectoryServiceSynchronizationWrapper(Service service, String endpointURI) {
		super(DirectoryServiceSynchronization.class, service, endpointURI);
	}

	@Override
	public List<Value> synchroniseAccount(String arg0) throws AccessDeniedException, IllegalRequestException, BackendException, SynchronizationException {
		return servicePort.get().synchroniseAccount(arg0);
	}
    
    @Override
	public void fault(String arg0) {
		servicePort.get().fault(arg0);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

    @Override
	public Long ping(Long arg0) {
		return servicePort.get().ping(arg0);
	}

}
