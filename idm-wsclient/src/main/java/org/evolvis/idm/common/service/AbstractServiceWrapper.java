package org.evolvis.idm.common.service;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

/**
 * @author Tino Rink
 * @author Jens Neumaier
 */
public class AbstractServiceWrapper<T> {

    private Class<T> serviceInterface;
    
    private Service service;
    
    private String endpointURI;
    
    protected ThreadLocal<T> servicePort = new ThreadLocal<T>() {
      protected T initialValue() {
          final T servicePort;
          synchronized (service) {
              servicePort = service.getPort(serviceInterface);            
          }
          ((BindingProvider) servicePort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURI);
          return servicePort;
      };  
    };

    protected AbstractServiceWrapper(Class<T> serviceInterface, Service service, String endpointURI) {
        this.serviceInterface = serviceInterface;
        this.service = service;
        this.endpointURI = endpointURI;
    }
}