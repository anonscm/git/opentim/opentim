package org.evolvis.idm.identity.privatedata.service;

import java.util.List;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;

public class PrivateDataConfigurationWrapper extends AbstractServiceWrapper<PrivateDataConfiguration> implements PrivateDataConfiguration {

	public PrivateDataConfigurationWrapper(Service service, String endpointURI) {
		super(PrivateDataConfiguration.class, service, endpointURI);
	}

	@Override
	public void deleteAttribute(String arg0, Attribute arg1) throws IllegalRequestException, BackendException {
		servicePort.get().deleteAttribute(arg0, arg1);
	}

	@Override
	public void deleteAttributeGroup(String arg0, AttributeGroup arg1) throws BackendException, AccessDeniedException, IllegalRequestException {
		servicePort.get().deleteAttributeGroup(arg0, arg1);
	}

	@Override
	public Attribute setAttribute(String arg0, Attribute arg1) throws IllegalRequestException, BackendException {
		return servicePort.get().setAttribute(arg0, arg1);
	}

	@Override
	public AttributeGroup setAttributeGroup(String arg0, AttributeGroup arg1) throws BackendException, IllegalRequestException, AccessDeniedException {
		return servicePort.get().setAttributeGroup(arg0, arg1);
	}

	@Override
	public List<AttributeGroup> setAttributeGroups(String arg0, List<AttributeGroup> arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().setAttributeGroups(arg0, arg1);
	}

	@Override
	public Attribute getAttributeById(String arg0, Long arg1) throws IllegalRequestException, BackendException {
		return servicePort.get().getAttributeById(arg0, arg1);
	}

	@Override
	public AttributeGroup getAttributeGroupById(String arg0, Long arg1) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getAttributeGroupById(arg0, arg1);
	}

	@Override
	public List<AttributeGroup> getAttributeGroups(String arg0) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getAttributeGroups(arg0);
	}
    
    @Override
	public void fault(String arg0) {
		servicePort.get().fault(arg0);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

    @Override
	public Long ping(Long arg0) {
		return servicePort.get().ping(arg0);
	}

	@Override
	public AttributeGroup getAttributeGroup(String clientName, String attributeGroupName) throws IllegalRequestException, BackendException {
		return servicePort.get().getAttributeGroup(clientName, attributeGroupName);
	}

}
