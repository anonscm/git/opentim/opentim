package org.evolvis.idm.identity.account.service;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.identity.account.model.User;

public class UserManagementWrapper extends AbstractServiceWrapper<UserManagement> implements UserManagement {

	public UserManagementWrapper(Service service, String endpointURI) {
		super(UserManagement.class, service, endpointURI);
	}

	@Override
	public void createUser(User user) throws BackendException, IllegalRequestException {
		servicePort.get().createUser(user);
	}

	@Override
	public void deleteUser(String uuid) throws BackendException, IllegalRequestException {
		servicePort.get().deleteUser(uuid);
	}

	@Override
	public User getUserByUsername(String clientName, String username) throws BackendException, IllegalRequestException {
		return servicePort.get().getUserByUsername(clientName, username);
	}

	@Override
	public User getUserByUuid(String uuid) throws BackendException, IllegalRequestException {
		return servicePort.get().getUserByUuid(uuid);
	}

	@Override
	public QueryResult<User> getUsers(String clientName, QueryDescriptor queryDescriptor) throws BackendException, IllegalRequestException {
		return servicePort.get().getUsers(clientName, queryDescriptor);
	}

	@Override
	public boolean isUserExisting(String clientName, String username) throws BackendException, IllegalRequestException {
		return servicePort.get().isUserExisting(clientName, username);
	}

	@Override
	public String registerUser(String clientName, String username, String password, String displayName, String firstname, String lastname, String localeString) throws BackendException, IllegalRequestException {
		return servicePort.get().registerUser(clientName, username, password, displayName, firstname, lastname, localeString);
	}

	@Override
	public void setUserNames(String uuid, String firstname, String lastname) throws BackendException, IllegalRequestException {
		servicePort.get().setUserNames(uuid, firstname, lastname);
	}

	@Override
	public void setUserStatus(String uuid, String status) throws BackendException, IllegalRequestException {
		servicePort.get().setUserStatus(uuid, status);
	}
    
    @Override
	public void fault(String arg0) {
		servicePort.get().fault(arg0);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

    @Override
	public Long ping(Long arg0) {
		return servicePort.get().ping(arg0);
	}

	@Override
	public String changeUsername(String uuid, String newUsername, String currentPassword) throws BackendException, IllegalRequestException {
		return servicePort.get().changeUsername(uuid, newUsername, currentPassword);
	}

	@Override
	public String requestUsernameChange(String uuid, String newUsername) throws BackendException, IllegalRequestException {
		return servicePort.get().requestUsernameChange(uuid, newUsername);
	}

}
