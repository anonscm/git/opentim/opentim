package org.evolvis.idm.identity.account.service;

import java.util.Calendar;
import java.util.List;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.Client;

/**
 * @author Jens Neumaier
 */
public class AccountManagementWrapper extends AbstractServiceWrapper<AccountManagement> implements AccountManagement {
	
	public AccountManagementWrapper(Service service, String endpointURI) {
		super(AccountManagement.class, service, endpointURI);
	}

	@Override
	public void deleteAccount(String arg0) throws BackendException, AccessDeniedException, IllegalRequestException {
		servicePort.get().deleteAccount(arg0);
	}

	@Override
	public QueryResult<Account> getAccounts(String arg0, QueryDescriptor arg1) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getAccounts(arg0, arg1);
	}

	@Override
	public List<Account> getDeactivatedAccountsSince(String arg0, Calendar arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getDeactivatedAccountsSince(arg0, arg1);
	}

	@Override
	public boolean isDeactived(String arg0) throws BackendException, IllegalRequestException {
		return servicePort.get().isDeactived(arg0);
	}

	@Override
	public Account setAccount(String arg0, Account arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().setAccount(arg0, arg1);
	}

	@Override
	public Account getAccountByUsername(String arg0, String arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getAccountByUsername(arg0, arg1);
	}

	@Override
	public Account getAccountByUuid(String arg0) throws BackendException, IllegalRequestException {
		return servicePort.get().getAccountByUuid(arg0);
	}

	@Override
	public Client getClientByUuid(String arg0) throws BackendException, IllegalRequestException {
		return servicePort.get().getClientByUuid(arg0);
	}
    
    @Override
	public void fault(String arg0) {
		servicePort.get().fault(arg0);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

    @Override
	public Long ping(Long arg0) {
		return servicePort.get().ping(arg0);
	}
    
}
