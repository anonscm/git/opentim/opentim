package org.evolvis.idm.identity.privatedata.service;

import java.util.List;

import javax.xml.ws.Service;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractServiceWrapper;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.privatedata.model.Approval;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;

public class PrivateDataManagementWrapper extends AbstractServiceWrapper<PrivateDataManagement> implements PrivateDataManagement {

	public PrivateDataManagementWrapper(Service service, String endpointURI) {
		super(PrivateDataManagement.class, service, endpointURI);
	}

	@Override
	public void deleteValues(String arg0, ValueSet arg1, boolean arg2) throws BackendException, AccessDeniedException, IllegalRequestException {
		servicePort.get().deleteValues(arg0, arg1, arg2);
	}

	@Override
	public Account getAccount(String arg0) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getAccount(arg0);
	}

	@Override
	public ValueSet setValues(String arg0, ValueSet arg1, boolean arg2) throws BackendException, IllegalRequestException, AccessDeniedException {
		return servicePort.get().setValues(arg0, arg1, arg2);
	}

	@Override
	public Value getValueWithBinaryAttachmentById(String arg0, Long arg1) throws BackendException, IllegalRequestException {
		return servicePort.get().getValueWithBinaryAttachmentById(arg0, arg1);
	}

	@Override
	public List<ValueSet> getValues(String arg0) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getValues(arg0);
	}

	@Override
	public List<ValueSet> getValuesByAttributes(String arg0, List<AttributeGroup> arg1) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getValuesByAttributes(arg0, arg1);
	}

	@Override
	public ValueSet getValuesById(String arg0, Long arg1) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getValuesById(arg0, arg1);
	}

	@Override
	public Attribute getAttributeById(String arg0, Long arg1) throws IllegalRequestException, BackendException {
		return servicePort.get().getAttributeById(arg0, arg1);
	}

	@Override
	public AttributeGroup getAttributeGroupById(String arg0, Long arg1) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getAttributeGroupById(arg0, arg1);
	}

	@Override
	public List<AttributeGroup> getAttributeGroups(String arg0) throws BackendException, AccessDeniedException, IllegalRequestException {
		return servicePort.get().getAttributeGroups(arg0);
	}
    
    @Override
	public void fault(String arg0) {
		servicePort.get().fault(arg0);
	}

    @Override
	public String getVersion() {
		return servicePort.get().getVersion();
	}

    @Override
	public Long ping(Long arg0) {
		return servicePort.get().ping(arg0);
	}

	@Override
	public List<Approval> getApprovalsByAccount(String uuid) throws BackendException, IllegalRequestException {
		return servicePort.get().getApprovalsByAccount(uuid);
	}

	@Override
	public Approval setApproval(String clientName, Approval approval) throws IllegalRequestException, BackendException {
		return servicePort.get().setApproval(clientName, approval);
	}

	@Override
	public void deleteApproval(String clientName, Long approvalId) throws IllegalRequestException, BackendException {
		 servicePort.get().deleteApproval(clientName, approvalId);
		
	}

	@Override
	public List<Approval> getApprovalsByApplicationId(String clientName, Long applicationId) throws BackendException, IllegalRequestException {
		
		return servicePort.get().getApprovalsByApplicationId(clientName, applicationId);
	}

	@Override
	public List<Approval> getApprovalsByUuidAndApplicationId(String uuid, Long applicationId) throws BackendException, IllegalRequestException {
		return servicePort.get().getApprovalsByUuidAndApplicationId(uuid, applicationId);
	}

	@Override
	public AttributeGroup getAttributeGroup(String clientName, String attributeGroupName) throws IllegalRequestException, BackendException {
		return servicePort.get().getAttributeGroup(clientName, attributeGroupName);
	}

}
