package org.evolvis.idm.em.connector.liferay;

import org.evolvis.idm.em.agent.model.GroupCapableAgent;
import org.evolvis.idm.em.agent.model.RoleCapableAgent;

public interface LiferayAgent extends RoleCapableAgent, GroupCapableAgent {

}
