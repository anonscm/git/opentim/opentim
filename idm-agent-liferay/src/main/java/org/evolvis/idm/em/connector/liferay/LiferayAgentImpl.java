package org.evolvis.idm.em.connector.liferay;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.evolvis.idm.em.agent.model.AgentEvent;
import org.evolvis.idm.em.core.model.Event;
import org.evolvis.idm.em.core.model.EventResult;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.permission.model.Group;

@Stateful
@Local(LiferayAgent.class)
public class LiferayAgentImpl implements LiferayAgent {
	
	public static final String CONFIG_KEY_LIFERAY_HOST = "companyId";
	public static final String CONFIG_KEY_LIFERAY_JNDI_PORT = "companyId";
	public static final String CONFIG_KEY_COMPANY_ID = "companyId";
	
	protected Map<String, String> configuration;
	protected LiferayConnectorRemote liferayConnector;
	
	protected long companyId;
	
	protected LiferayConnectorRemote getLiferayConnector() {
		if (liferayConnector == null) {
			try {
				Hashtable<Object, Object> jndiProps = new Hashtable<Object, Object>();
				jndiProps.put("java.naming.factory.initial","org.jnp.interfaces.NamingContextFactory");
				jndiProps.put("java.naming.factory.url.pkgs","org.jboss.naming:org.jnp.interfaces");
				jndiProps.put("java.naming.provider.url", configuration.get(CONFIG_KEY_LIFERAY_HOST) + ":" + configuration.get(CONFIG_KEY_LIFERAY_JNDI_PORT));
				Context context = new InitialContext(jndiProps);
				liferayConnector = (LiferayConnectorRemote) context.lookup(LiferayConnectorImpl.class.getSimpleName() + "/remote");
				liferayConnector.configure(configuration);
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return liferayConnector;
	}
	
	@Override
	public Map<String, String> getManualRoleMapping() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getRoles() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getUnmappedRoles() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String> getManualGroupMapping() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getUnmappedGroups() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EventResult processGroupEvent(Event<Group> groupEvent) {
		liferayConnector.processGroupEvent(new AgentEvent<Group>(groupEvent));
		return null;
	}

	@Override
	public void configure(Map<String, String> configuration) {
		setConfiguration(configuration);
		setCompanyId(Long.valueOf(configuration.get(CONFIG_KEY_COMPANY_ID)));
	}
	
	public Map<String, String> getConfiguration() {
		return configuration;
	}
	
	public void setConfiguration(Map<String, String> configuration) {
		this.configuration = configuration;
	}

	protected long getCompanyId() {
		return companyId;
	}

	protected void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	@Override
	public EventResult processAccountEvent(Event<Account> accountEvent) {
		// TODO Auto-generated method stub
		return null;
	}
}