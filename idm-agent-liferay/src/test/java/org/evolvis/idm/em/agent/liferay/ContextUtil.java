package org.evolvis.idm.em.agent.liferay;

import java.util.*;

import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ContextUtil {
	private static Context context = null;
	private static ConnectionFactory connectionFactory = null;
	
	public static Context getNamingContext() throws NamingException {
		if (context == null) {
			Hashtable<Object, Object> jndiProps = new Hashtable<Object, Object>();
			jndiProps.put("java.naming.factory.initial","org.jnp.interfaces.NamingContextFactory");
			jndiProps.put("java.naming.factory.url.pkgs","org.jboss.naming:org.jnp.interfaces");
			jndiProps.put("java.naming.provider.url","localhost:1099");
			context = new InitialContext();
		}
		return context;
	}
	
	
	public static ConnectionFactory getConnectionFactory() throws NamingException {
		if (connectionFactory == null) {
			connectionFactory = (ConnectionFactory) getNamingContext().lookup("ConnectionFactory");
		}
		return connectionFactory;
	}
}
