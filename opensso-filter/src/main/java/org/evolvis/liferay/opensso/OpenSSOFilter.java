package org.evolvis.liferay.opensso;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.evolvis.config.Configuration;
import org.evolvis.config.PropertyConfig;
import org.evolvis.opensso.rest.constant.PropSSOKeys;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Company;
import com.liferay.portal.servlet.filters.BasePortalFilter;
import com.liferay.portal.util.PortalUtil;

/**
 * OpenSSOFilter for LifeRay
 * 
 * @author Tino Rink
 */
public class OpenSSOFilter extends BasePortalFilter {

	public static final String CONFIGURATION_COOKIE_DOMAIN_KEY = "tarent.sso.cookieDomain";
	/*
	 * Do not change the value of this key while it is not correctly referenced
	 * from PortletConfigKeys
	 */
	public static final String CONFIGURATION_SSO_LOGIN_AUTOREDIRECT = "tarent.sso.service.autoRedirect";

	public static final String SESSION_URI_LAST_REQUEST = "tarent.sso.uri.lastRequest";
	public static final String SESSION_URI_LAST_VISITED_BEFORE_LOGIN = "tarent.sso.uri.lastVisitedBeforeLogin";

	public static final String COOKIE_DOMAIN = PropertyConfig.getInstance().get(
			CONFIGURATION_COOKIE_DOMAIN_KEY);
	public static final boolean IS_SSO_LOGIN_AUTOREDIRECT_ENABLED = Boolean.valueOf(PropertyConfig
			.getInstance().get(CONFIGURATION_SSO_LOGIN_AUTOREDIRECT, "false"));

	private static final Log logger = LogFactoryUtil.getLog(OpenSSOFilter.class);

	private Configuration config = PropertyConfig.getInstance();

	@Override
	protected void processFilter(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain) {

		try {
			boolean enabled = Boolean.valueOf(config.get(PropSSOKeys.TARENT_SSO_ENABLED));

			String serviceUrl = config.get(PropSSOKeys.TARENT_SSO_SERVICE_URL);

			/*
			 * check if OpenSSO is enabled
			 */
			if (!enabled || Validator.isNull(serviceUrl)) {
				processFilter(OpenSSOFilter.class, request, response, filterChain);
				return;
			}

			String cookieDomain = getCookieDomain(request);
			String requestURI = GetterUtil.getString(request.getRequestURI());

			/*
			 * if CONFIGURATION_SSO_LOGIN_AUTOREDIRECT configuration key has
			 * been enabled the user will be redirected to the page he visited
			 * before entering the login page when he has successfully logged
			 * in.
			 * 
			 * Saving last visited page on login request (ignoring login page
			 * reloads).
			 */
			if (IS_SSO_LOGIN_AUTOREDIRECT_ENABLED
					&& requestURI.endsWith("/login")
					&& request.getSession().getAttribute(SESSION_URI_LAST_REQUEST) != null
					&& !request.getSession().getAttribute(SESSION_URI_LAST_REQUEST).toString()
							.endsWith("/login")) {
				request.getSession().setAttribute(SESSION_URI_LAST_VISITED_BEFORE_LOGIN,
						request.getSession().getAttribute(SESSION_URI_LAST_REQUEST));
			}
			/*
			 * Saving last request URI on all requests.
			 */
			if (IS_SSO_LOGIN_AUTOREDIRECT_ENABLED) {
				request.getSession().setAttribute(SESSION_URI_LAST_REQUEST, requestURI);
			}

			try {
				String cookieToken = OpenSSOUtil.getCookieToken(request, serviceUrl);
				String sessionToken = OpenSSOUtil.getSessionToken(request);
				String loginToken = OpenSSOUtil.getLoginToken(request);

				/*
				 * check for cookieToken
				 */
				if (OpenSSOUtil.isValidToken(cookieToken)
						&& OpenSSOUtil.isAuthenticated(serviceUrl, cookieToken)) {

					// do logout
					if (requestURI.endsWith("/portal/logout")) {
						OpenSSOUtil.doLogout(serviceUrl, cookieToken);
						OpenSSOUtil.setCookieToken(response, serviceUrl, cookieDomain, null, 0);
					}
					// check cookie token against session token
					else {
						if ((null == sessionToken) || StringPool.NULL.equals(sessionToken)
								|| !cookieToken.equals(sessionToken)) {
							OpenSSOUtil.doLogout(serviceUrl, cookieToken);
							OpenSSOUtil.setCookieToken(response, serviceUrl, cookieDomain, null, -1);
							OpenSSOUtil.setSessionToken(request, null);
							OpenSSOUtil.setLoginToken(request, null);
							request.getSession().invalidate();
							response.sendRedirect("/");
							return;
						}
					}
				}
				// check for loginToken
				else if (OpenSSOUtil.isValidToken(loginToken)) {
					// check token and set new attributes
					if (OpenSSOUtil.isAuthenticated(serviceUrl, loginToken)) {
						OpenSSOUtil.setCookieToken(response, serviceUrl, cookieDomain, loginToken, -1);
						OpenSSOUtil.setSessionToken(request, loginToken);
						OpenSSOUtil.setLoginToken(request, null);

						/*
						 * if CONFIGURATION_SSO_LOGIN_AUTOREDIRECT configuration
						 * key has been enabled the user will be redirected to
						 * the page he visited before entering the login page
						 * when he has successfully logged in.
						 */
						if (IS_SSO_LOGIN_AUTOREDIRECT_ENABLED
								&& request.getSession().getAttribute(SESSION_URI_LAST_VISITED_BEFORE_LOGIN) != null) {
							response.sendRedirect(request.getSession().getAttribute(
									SESSION_URI_LAST_VISITED_BEFORE_LOGIN).toString());
						} else {
							response.sendRedirect(requestURI);
						}
						return;
					}
				}
				// if cookie token is deleted and session token exist -> delete
				// session (security)
				else if (OpenSSOUtil.isValidToken(sessionToken)) {
					// invalidate old session
					request.getSession().invalidate();
					response.sendRedirect(requestURI);
					return;
				}
				// TODO jneuma check for better / more stable solution
				// redirect to original uri to force another call with a valid session
				else if (request.getSession(false) != null && request.getSession(false).isNew() && request.getParameter("createSessionRedirect") == null) {
					String queryString = request.getQueryString();
					if (queryString == null)
						requestURI += "?createSessionRedirect=true";
					else
						requestURI += "?" + queryString + "&createSessionRedirect=true";
					response.sendRedirect(requestURI);
					return;
				}
			} catch (final Exception e) {
				logger.error(e, e);
			}

			// logout
			if (requestURI.endsWith("/portal/logout")) {
				OpenSSOUtil.setCookieToken(response, serviceUrl, cookieDomain, null, 0);
				OpenSSOUtil.setSessionToken(request, null);
				OpenSSOUtil.setLoginToken(request, null);
				request.getSession().invalidate();
				response.sendRedirect("/");
				return;
			}

			processFilter(OpenSSOFilter.class, request, response, filterChain);
		} catch (final Exception e) {
			logger.error(e, e);
		}
	}

	private String getCookieDomain(final HttpServletRequest request) {
		String cookieDomain = null;
		try {
			// trying to read Cookie Domain from portal-tarent.properties
			cookieDomain = COOKIE_DOMAIN;
			// use Liferay Virtual Host if no explicit Cookie Domain has been
			// set
			if (cookieDomain == null) {
				final Company company = PortalUtil.getCompany(request);
				if (null != company) {
					cookieDomain = company.getVirtualHost();
				}
			}
		} catch (final Exception e) {
			logger.warn("getCookieDomain() failed with Exception", e);
		}
		if (null == cookieDomain) {
			final String serverName = request.getServerName();
			final int firstDotIdx = serverName.lastIndexOf(".");
			final int secondDotIdx = serverName.lastIndexOf(".", firstDotIdx - 1);
			cookieDomain = secondDotIdx > 1 ? serverName.substring(secondDotIdx) : serverName;
		}
		return cookieDomain;
	}
}