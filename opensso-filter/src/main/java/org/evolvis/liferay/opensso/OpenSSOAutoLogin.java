package org.evolvis.liferay.opensso;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.config.Configuration;
import org.evolvis.config.PropertyConfig;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.ws.InternalWsClientFactory;
import org.evolvis.opensso.rest.RestClient;
import org.evolvis.opensso.rest.constant.PropSSOKeys;
import org.evolvis.opensso.rest.constant.UserAttribute;
import org.evolvis.opensso.rest.service.GetAttributes;

import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.NoSuchUserException;
import com.liferay.portal.PortalException;
import com.liferay.portal.SystemException;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.AutoLogin;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.PwdGenerator;

/**
 * OpenSSOAutoLogin for LifeRay
 * 
 * @author Tino Rink
 * @author Jens Neumaier
 */
public class OpenSSOAutoLogin implements AutoLogin {

	// TODO jneuma externalize constant
	public static final String CONFIG_KEY_IDM_BACKEND_ENDPOINT = "tarent.idm.backend.endpoint";
	public static final String IDM_BACKEND_ENDPOINT_URI = PropertyConfig.getInstance().get(
			CONFIG_KEY_IDM_BACKEND_ENDPOINT);

	private static Log logger = LogFactory.getLog(OpenSSOAutoLogin.class);

	private final Configuration config = PropertyConfig.getInstance();

	public String[] login(final HttpServletRequest request, final HttpServletResponse response) {

		final long companyId = PortalUtil.getCompanyId(request);
		
		try {
			if (!Boolean.valueOf(config.get(PropSSOKeys.TARENT_SSO_ENABLED))) {
				return null;
			}

			final String serviceUrl = config.get(PropSSOKeys.TARENT_SSO_SERVICE_URL);
			final String ssoToken = OpenSSOUtil.getCookieToken(request, serviceUrl);

			if ((null == serviceUrl) || StringPool.BLANK.equals(serviceUrl)) {
				logger.error("Login error: OpenSSO service URL is null or blank: " + serviceUrl);
				return null;
			}

			if (!OpenSSOUtil.isValidToken(ssoToken)) {
				return null;
			}
			
			// TODO jneuma check if locale is realy neccessary in Liferay
			final ThemeDisplay themeDisplay = (ThemeDisplay) request
			.getAttribute(WebKeys.THEME_DISPLAY);

			// ThemeDisplay should never be null, but some users
			// complain of
			// this error. Cause is unknown.
			final Locale locale;
			if (themeDisplay != null) {
				locale = themeDisplay.getLocale();
			} else {
				locale = LocaleUtil.getDefault();
			}

			final RestClient restClient = RestClient.getInstance();
			restClient.initialize(serviceUrl);
			final GetAttributes getAttributes = restClient.callService(new GetAttributes(ssoToken));

			if (getAttributes.success()) {

				final List<String> uuidValues = getAttributes.attribute(UserAttribute.USER_UUID);
				final String uuid = (uuidValues != null && !uuidValues.isEmpty()) ? uuidValues
						.get(0) : null;

				org.evolvis.idm.identity.account.model.User idmUser = null;
				User liferayUser = null;
				try {
					idmUser = InternalWsClientFactory.getInstance().getUserManagementService(
							IDM_BACKEND_ENDPOINT_URI).getUserByUuid(uuid);
					liferayUser = UserLocalServiceUtil.getUserByUuidAndCompanyId(uuid, companyId);
				} catch (BackendException be) {
					logger.error("Login was not successful because IdM user could not be retrieved for uuid="+uuid+", error: "
									+ be.getMessage());
					return null;
				} catch (NoSuchUserException nsue) {
					liferayUser = addUser(companyId, idmUser, locale);
				}

				synchronizeLiferayUser(idmUser, liferayUser, locale);

				final String[] credentials = new String[3];

				credentials[0] = String.valueOf(liferayUser.getUserId());
				credentials[1] = liferayUser.getPassword();
				credentials[2] = Boolean.TRUE.toString();

				return credentials;
			}

			if (logger.isDebugEnabled())
				logger.debug("Login was not successful because OpenSSO attributes could not be retrieved, error: "
								+ getAttributes.getError());
			
			return null;
		}
		catch (Exception e) {
			logger.error("Login failed with exception", e);
			return null;
		}
	}

	protected void synchronizeLiferayUser(org.evolvis.idm.identity.account.model.User idmUser,
			User liferayUser, Locale locale) throws PortalException, SystemException {
		if (		!liferayUser.getFirstName().equals(idmUser.getFirstname())
				|| 	!liferayUser.getLastName().equals(idmUser.getLastname())) {
			Contact liferayUserContactData = liferayUser.getContact();
			liferayUserContactData.setFirstName(idmUser.getFirstname());
			liferayUserContactData.setLastName(idmUser.getLastname());
			ContactLocalServiceUtil.updateContact(liferayUserContactData);
		}
		if (!liferayUser.getEmailAddress().equals(idmUser.getUsername())) {
			try {
				UserLocalServiceUtil.updateEmailAddress(liferayUser.getUserId(), liferayUser.getPassword(), idmUser.getUsername(), idmUser.getUsername());
			} catch (DuplicateUserEmailAddressException e) {
				// an old liferay user may exist which is not present in the IdM anymore
				// this user will be deleted and re-added
				addUser(liferayUser.getCompanyId(), idmUser, locale);
			}
		}
	}

	/**
	 * Add a new liferay user for the given parameters. Use
	 * com.liferay.portal.action.TCKAction._getUser(..) as pattern.
	 * @param locale 
	 * @throws SystemException 
	 * @throws PortalException 
	 */
	protected User addUser(final long companyId, org.evolvis.idm.identity.account.model.User idmUser, Locale locale) throws PortalException, SystemException {

		final long creatorUserId = 0;
		final boolean autoPassword = false;
		final String password1 = PwdGenerator.getPassword();
		final boolean autoScreenName = true;
		final String screenName = StringPool.BLANK;
		final String openId = StringPool.BLANK;
		final String middleName = StringPool.BLANK;
		final int prefixId = 0;
		final int suffixId = 0;
		final boolean male = true;
		final int birthdayMonth = Calendar.JANUARY;
		final int birthdayDay = 1;
		final int birthdayYear = 1970;
		final String jobTitle = StringPool.BLANK;
		final long[] groupIds = new long[0];
		final long[] organizationIds = new long[0];
		final long[] roleIds = new long[0];
		final long[] userGroupIds = new long[0];
		final boolean sendEmail = false;
		final ServiceContext serviceContext = new ServiceContext();
		
		User liferayUser = null;
		boolean allowDuplicateUserEmailOverwrite = true;
		try {
			logger.info("Trying to add new IdM user into Liferay database: uuid="+idmUser.getUuid()+", username="+idmUser.getUsername());
			liferayUser = UserLocalServiceUtil.addUser(idmUser.getUuid(), creatorUserId, companyId, autoPassword,
				password1, password1, autoScreenName, screenName, idmUser.getUsername(), openId, locale,
				idmUser.getFirstname(), middleName, idmUser.getLastname(), prefixId, suffixId, male, birthdayMonth,
				birthdayDay, birthdayYear, jobTitle, groupIds, organizationIds, roleIds,
				userGroupIds, sendEmail, serviceContext);
			logger.info("Successfully synchonised a new IdM user into Liferay database: uuid="+liferayUser.getUuid()+", username="+liferayUser.getLogin());
		} catch (DuplicateUserEmailAddressException e) {
			if (allowDuplicateUserEmailOverwrite) {
				// recreate user if present from previous installation
				logger.info("A user with username="+idmUser.getUsername()+" is allready present in the Liferay database. Trying to delete unsynchronised user and recreate user IdM user: uuid="+idmUser.getUuid()+", username="+idmUser.getUsername());
				liferayUser = UserLocalServiceUtil.getUserByEmailAddress(companyId, idmUser.getUsername());
				UserLocalServiceUtil.deleteUser(liferayUser);
				logger.info("Successfully deleted unsynchronised liferay user with uuid="+liferayUser.getUuid()+", username="+liferayUser.getLogin());
				// user are not added in a recursive loop to avoid an endless loop
				liferayUser = UserLocalServiceUtil.addUser(idmUser.getUuid(), creatorUserId, companyId, autoPassword,
						password1, password1, autoScreenName, screenName, idmUser.getUsername(), openId, locale,
						idmUser.getFirstname(), middleName, idmUser.getLastname(), prefixId, suffixId, male, birthdayMonth,
						birthdayDay, birthdayYear, jobTitle, groupIds, organizationIds, roleIds,
						userGroupIds, sendEmail, serviceContext);
				logger.info("Successfully synchonised a new IdM user into Liferay database: uuid="+idmUser.getUuid()+", username="+idmUser.getUsername());
			} else {
				throw e;
			}
		}
		return liferayUser;
	}
}