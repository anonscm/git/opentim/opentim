package org.evolvis.liferay.opensso;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class CookieUtil {

    /**
     * @return Cookie or null if cookie not exist
     */
    public static Cookie get(final HttpServletRequest request, final String name) {
        final Cookie[] cookies = request.getCookies();
        if ((null == cookies) || (0 == cookies.length)) {
            return null;
        }
        for (final Cookie cookie : cookies) {
            final String cookieName = cookie.getName();
            if ((null != cookieName) && cookieName.equalsIgnoreCase(name)) {
                return cookie;
            }
        }
        return null;
    }

    /**
     * @return CookieValue or null if cookie not exist
     */
    public static String getValue(final HttpServletRequest request, final String cookieName) {
        final Cookie cookie = get(request, cookieName);
        if (null != cookie) {
            return cookie.getValue();
        }
        return null;
    }
}