package org.evolvis.liferay.opensso;

import java.util.Map;

import com.liferay.portal.security.auth.AuthException;
import com.liferay.portal.security.auth.Authenticator;

/**
 * A always failing Authenticator. If used, it disables all types of Logins via Authentication-Pipeline. 
 *  
 * @author Tino Rink
 */
public class FailureAuthz implements Authenticator {

    public int authenticateByEmailAddress(final long companyId, final String emailAddress, final String password,
            final Map<String, String[]> headerMap, final Map<String, String[]> parameterMap) throws AuthException {
        return FAILURE;
    }

    public int authenticateByScreenName(final long companyId, final String screenName, final String password,
            final Map<String, String[]> headerMap, final Map<String, String[]> parameterMap) throws AuthException {
        return FAILURE;
    }

    public int authenticateByUserId(final long companyId, final long userId, final String password,
            final Map<String, String[]> headerMap, final Map<String, String[]> parameterMap) throws AuthException {
        return FAILURE;
    }
}