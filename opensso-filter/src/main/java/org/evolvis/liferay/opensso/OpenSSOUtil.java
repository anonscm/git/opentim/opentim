package org.evolvis.liferay.opensso;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.evolvis.opensso.rest.RestClient;
import org.evolvis.opensso.rest.service.GetCookieName;
import org.evolvis.opensso.rest.service.IsTokenValid;
import org.evolvis.opensso.rest.service.Logout;

import util.Base64;

import com.liferay.portal.kernel.util.StringPool;

/**
 * UtilClass for CookieToken, SessionToken & communication with OpenSSO
 * 
 * @author Tino Rink
 */
public class OpenSSOUtil {

    private static final String LIFERAY_SHARED_OPENSSO_TOKEN = "LIFERAY_SHARED_OPENSSO_TOKEN";

    private static final String SESSION_SSO_TOKEN = "SESSION_SSO_TOKEN";

    private static final OpenSSOUtil instance = new OpenSSOUtil();

    private final Map<String, String> tokenCookieNameMap = new ConcurrentHashMap<String, String>();

    public static String getLoginToken(final HttpServletRequest request) {
        final Object tokenObj = request.getSession().getAttribute(LIFERAY_SHARED_OPENSSO_TOKEN);
        if (null != tokenObj) {
            return String.valueOf(tokenObj);
        }
        return null;
    }

    public static void setLoginToken(final HttpServletRequest request, final String token) {
        request.getSession().setAttribute(LIFERAY_SHARED_OPENSSO_TOKEN, token);
    }

    public static String getSessionToken(final HttpServletRequest request) {
        final Object tokenObj = request.getSession().getAttribute(SESSION_SSO_TOKEN);
        if (null != tokenObj) {
            return String.valueOf(tokenObj);
        }
        return null;
    }

    public static void setSessionToken(final HttpServletRequest request, final String token) {
        request.getSession().setAttribute(SESSION_SSO_TOKEN, token);
    }

    /**
     * Do Logout
     */
    public static void doLogout(final String serviceUrl, final String token) throws IOException {
        instance._doLogout(serviceUrl, token);
    }

    /**
     * Return the Token of the OpenSSO cookie or null
     */
    public static String getCookieToken(final HttpServletRequest request, final String serviceUrl) throws IOException {
        return instance._getCookieToken(request, serviceUrl);
    }

    /**
     * Set the Token for the OpenSSO cookie
     */
    public static void setCookieToken(final HttpServletResponse response, final String serviceUrl, final String domain,
            final String token, final int maxAge) throws IOException {
        instance._setCookieToken(response, serviceUrl, domain, token, maxAge);
    }

    /**
     * Check if the Tooken-Cookie is set.
     * 
     * @return True / False if Cookie is set.
     */
    public static boolean hasTokenCookie(final HttpServletRequest request, final String serviceUrl) {
        return instance._hasTokenCookie(request, serviceUrl);
    }

    /**
     * Check if the given token could be valid
     */
    public static boolean isValidToken(final String token) {
        return (null != token) && !StringPool.BLANK.equals(token) && !StringPool.NULL.equalsIgnoreCase(token);
    }

    /**
     * Check if the given token is Authenticated
     */
    public static boolean isAuthenticated(final String serviceUrl, final String token) throws IOException {
        return instance._isAuthenticated(serviceUrl, token);
    }

    private OpenSSOUtil() {
    }

    private void _doLogout(final String serviceUrl, final String token) throws IOException {
    	// check serviceUrl & token
        if ((null == serviceUrl) || !isValidToken(token)) {
            return;
        }
        // is token valid
        final RestClient restClient = RestClient.getInstance();
        restClient.initialize(serviceUrl);
        restClient.callService(new Logout(token));
    }

    private String _getTokenCookieName(final String serviceUrl) throws IOException {
        // try cache
        String tokenCookieName = tokenCookieNameMap.get(serviceUrl);
        if (null != tokenCookieName) {
            return tokenCookieName;
        }

        // use rest api
        final RestClient restClient = RestClient.getInstance();
        restClient.initialize(serviceUrl);
        tokenCookieName = restClient.callService(new GetCookieName()).getTokenCookieName();

        tokenCookieNameMap.put(serviceUrl, tokenCookieName);
        return tokenCookieName;
    }

    private boolean _hasTokenCookie(final HttpServletRequest request, final String serviceUrl) {

        // get name for token cookie
        final String tokenCookieName;
        try {
            tokenCookieName = _getTokenCookieName(serviceUrl);
        }
        catch (final IOException ioe) {
            return false;
        }

        // check cookie
        final Cookie cookie = CookieUtil.get(request, tokenCookieName);
        if ((null != cookie) && (null != cookie.getValue()) && !StringPool.BLANK.equals(cookie.getValue())) {
            return true;
        }
        else {
            return false;
        }
    }

    private String _getCookieToken(final HttpServletRequest request, final String serviceUrl) throws IOException {
        final String cookieName = _getTokenCookieName(serviceUrl);
        final String value = CookieUtil.getValue(request, cookieName);
        return value != null ? new String(Base64.decodeFast(value)) : null;
    }

    private void _setCookieToken(final HttpServletResponse response, final String serviceUrl, final String domain,
            final String token, final int maxAge) throws IOException {
        final String cookieName = _getTokenCookieName(serviceUrl);
        final Cookie cookie = new Cookie(cookieName, null != token ? Base64.encodeToString(token.getBytes(), false) : null);
        cookie.setDomain(domain);
        cookie.setPath("/");
        cookie.setMaxAge(-1);
        response.addCookie(cookie);
    }

    private boolean _isAuthenticated(final String serviceUrl, final String token) throws IOException {
        // check serviceUrl & token
        if ((null == serviceUrl) || !isValidToken(token)) {
            return false;
        }
        // is token valid
        final RestClient restClient = RestClient.getInstance();
        restClient.initialize(serviceUrl);
        return restClient.callService(new IsTokenValid(token)).valid();
    }
}