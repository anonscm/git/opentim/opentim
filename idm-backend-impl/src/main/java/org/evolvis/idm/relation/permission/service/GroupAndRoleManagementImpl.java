/**
 * 
 */
package org.evolvis.idm.relation.permission.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryConfiguration;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.GroupAssignment;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.model.RoleAssignment;
import org.evolvis.idm.relation.permission.model.RoleUser;
import org.evolvis.idm.relation.permission.model.UserRole;
import org.jboss.ws.annotation.EndpointConfig;
import org.jboss.wsf.spi.annotation.WebContext;

/**
 * @author Yorka Neumann
 */
@Stateless
@Local(GroupAndRoleManagement.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "GroupAndRoleManagementService", name = "GroupAndRoleManagementServicePT", portName = "GroupAndRoleManagementServicePort")
@WebContext(contextRoot = "idm-backend", urlPattern = "/GroupAndRoleManagementService")
// @EndpointConfig(configName = "Standard WSSecurity Endpoint")
public class GroupAndRoleManagementImpl extends AbstractBaseServiceImpl implements GroupAndRoleManagement {

	private AccountManagement accountManagement;

	@EJB(name = "ejb/AccountManagementImpl/local")
	public void setAccountManagement(AccountManagement accountManagement) {
		this.accountManagement = accountManagement;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	public void deleteGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException {
		if (groupId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Group group = getEntityManager().find(Group.class, groupId);
		if (group != null) {
			getEntityManager().remove(group);
		} else
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	public void deleteRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "roleId") Long roleId) throws BackendException, IllegalRequestException {
		if (roleId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Role role = getEntityManager().find(Role.class, roleId);
		if (role != null)
			getEntityManager().remove(role);
		else
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "group")
	public Group getGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException {
		if (groupId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		String qlString = "SELECT group1 FROM Group group1 WHERE group1.id = :groupId";
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("groupId", groupId);
		
		List<Group> resultList = this.executeQuery(Group.class, qlString, parameterMap);
		
		if(resultList.size() == 0)
			return null;
		Group group = resultList.get(0);
		group = new Group(group);
		return group;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "groups")
	public QueryResult<Group> getGroups(@WebParam(name = "clientName") String clientName, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, IllegalRequestException {
		if (clientName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		String qlString = "SELECT group1 FROM Group group1 WHERE group1.client.name = :clientName";
		String defaultSortOrder = "group1.name ASC";

		List<Class<?>> allowedOrderBeans = new LinkedList<Class<?>>();
		allowedOrderBeans.add(Group.class);

		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("clientName", clientName);

		QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap, queryDescriptor, defaultSortOrder, allowedOrderBeans);
		QueryResult<Group> executionResult = new QueryResult<Group>();
		this.executeSimpleQuery(qlString, queryConfiguration, executionResult);
		//log.info(("queryResult-Listsize:" + executionResult.getResultList().size()));
		return executionResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "groups")
	public List<Group> getGroupsByRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "roleName") String roleName, @WebParam(name = "scope") String scope)
			throws BackendException, IllegalRequestException {
		if (clientName == null | applicationName == null | roleName == null | scope == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		String whereClause = " WHERE rAssignment.role.name = :roleName AND rAssignment.role.application.name = :applicationName AND rAssignment.role.application.client.name = :clientName AND rAssignment.scope = :roleScope AND rAssignment.group IS NOT null ORDER BY rAssignment.group.name";
		String qlString = "SELECT rAssignment.group FROM RoleAssignment rAssignment" + whereClause;
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("clientName", clientName);
		query.setParameter("roleName", roleName);
		query.setParameter("roleScope", scope);
		query.setParameter("applicationName", applicationName);

		List<Group> resultList = this.getResultList(query);
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "groups")
	public List<Group> getGroupsByRoleId(@WebParam(name = "clientName") String clientName, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException {
		if (roleId == null | scope == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		String qlString = "SELECT rAssignment.group FROM RoleAssignment rAssignment WHERE rAssignment.scope = :roleScope AND rAssignment.role.id = :roleId AND rAssignment.group IS NOT null ORDER BY name";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("roleId", roleId);
		query.setParameter("roleScope", scope);
		List<Group> resultList = this.getResultList(query);
		return resultList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "userGroups")
	@SuppressWarnings("unchecked")
	public List<Group> getGroupsByUser(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException {
		if (uuid == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		this.beginTransaction();
		String queryString1 = "SELECT gAssignment.group FROM GroupAssignment gAssignment JOIN gAssignment.account gAccount WHERE gAccount.uuid =:uuid1 ORDER BY gAssignment.group.name";
		Query query = this.getEntityManager().createQuery(queryString1);
		query.setParameter("uuid1", uuid);
		List<Group> resultList = this.getResultList(query);
		this.commitTransaction();
		return resultList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	// @WebResult(name = "role")
	public Role getRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "roleId") Long roleId) throws BackendException, IllegalRequestException {
		if (roleId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Role role = this.getEntityManager().find(Role.class, roleId);
		return role;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "roles")
	public QueryResult<Role> getRolesByClient(@WebParam(name = "clientName") String clientName, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, IllegalRequestException {
		if (clientName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		this.beginTransaction();

		List<Class<?>> allowedOrderBeans = new LinkedList<Class<?>>();
		allowedOrderBeans.add(Role.class);

		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("clientName", clientName);

		String qlString = "SELECT new Role(role.id, role.name, role.application, role.displayName) FROM Role role WHERE role.application.client.name = :clientName";

		String defaultSortOrder = " role.displayName ASC ";

		QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap, queryDescriptor, defaultSortOrder, allowedOrderBeans);

		QueryResult<Role> queryResult = new QueryResult<Role>();
		queryResult = this.executeSimpleQuery(qlString, queryConfiguration, queryResult);
		this.commitTransaction();

		return queryResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "roles")
	public QueryResult<Role> getRolesByClientId(@WebParam(name = "clientName") String clientName, @WebParam(name = "clientId") Long clientId, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException,
			IllegalRequestException {
		if (clientId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		this.beginTransaction();

		List<Class<?>> allowedOrderBeans = new LinkedList<Class<?>>();
		allowedOrderBeans.add(Role.class);

		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("clientId", clientId);

		String qlString = "SELECT new Role(role.id, role.name, role.application, role.displayName) FROM Role role WHERE role.application.client.id = :clientId";
		String defaultSortOrder = " role.name ASC ";

		QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap, queryDescriptor, defaultSortOrder, allowedOrderBeans);

		QueryResult<Role> queryResult = new QueryResult<Role>();
		queryResult = this.executeSimpleQuery(qlString, queryConfiguration, queryResult);
		this.commitTransaction();

		return queryResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(name = "roles")
	public List<Role> getRolesByUserAndApplication(@WebParam(name = "uuid") String uuid, @WebParam(name = "applicationId") Long applicationId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException {
		if (uuid == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		
		String qlString0 = "SELECT gAssignment.group FROM GroupAssignment gAssignment WHERE gAssignment.account.uuid = :uuid1";
		String qlString1 = "SELECT rAssignment.role FROM RoleAssignment rAssignment WHERE rAssignment.account.uuid = :uuid1";
		
		if(applicationId != null){
			qlString1 += " AND rAssignment.role.application.id = :applicationId";
		}
		if (scope != null)
			qlString1 += " AND rAssignment.scope = :rScope";
		
		String qlString2 = "SELECT rAssignment.role FROM RoleAssignment rAssignment WHERE ";
		if(applicationId != null){
			qlString2 += " rAssignment.role.application.id = :applicationId AND ";
		}
		if (scope != null)
			qlString2 += " rAssignment.scope = :rScope AND ";
		qlString2 += " rAssignment.group IN (" + qlString0 + ")";
		
		String qlString = "SELECT role FROM Role role WHERE role IN (" + qlString1 + ") OR role IN (" + qlString2 + ") ORDER BY role.name";

		Query query = this.getEntityManager().createQuery(qlString);
		
		if(applicationId != null){
			query.setParameter("applicationId", applicationId);
		}
		
		query.setParameter("uuid1", uuid);
		if (scope != null)
			query.setParameter("rScope", scope);
		List<Role> roleList = this.getResultList(query);
		return roleList;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "roles")
	public List<Role> getRolesByUser(@WebParam(name = "uuid") String uuid, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException{
		if(uuid == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		else return this.getRolesByUserAndApplication(uuid, null, scope);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(name = "userRoles")
	public List<UserRole> getUserRoles(@WebParam(name = "uuid") String uuid, @WebParam(name = "applicationId") Long applicationId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException {
		if (uuid == null | applicationId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		String qlString0 = "SELECT gAssignment.group FROM GroupAssignment gAssignment WHERE gAssignment.account.uuid = :uuid1";
		String qlString1 = "SELECT rAssignment FROM RoleAssignment rAssignment WHERE rAssignment.role.application.id = :applicationId AND rAssignment.account.uuid = :uuid1";
		if (scope != null)
			qlString1 += " AND rAssignment.scope = :rScope";
		String qlString2 = "SELECT rAssignment FROM RoleAssignment rAssignment WHERE rAssignment.role.application.id = :applicationId AND rAssignment.group IN (" + qlString0 + ")";
		if (scope != null)
			qlString2 += " AND rAssignment.scope = :rScope";
		String qlString = "SELECT roleAssignment FROM RoleAssignment roleAssignment WHERE roleAssignment IN (" + qlString1 + ") OR roleAssignment IN (" + qlString2 + ") ORDER BY roleAssignment.role.name";

		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("applicationId", applicationId);
		query.setParameter("uuid1", uuid);
		if (scope != null)
			query.setParameter("rScope", scope);
		List<UserRole> roleList = this.getResultList(query);

		return roleList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(name = "usersOfGroup")
	public List<Account> getUsersByGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupName") String groupName) throws BackendException, IllegalRequestException {
		if (groupName == null | clientName == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		this.beginTransaction();
		String queryString = "SELECT gAssignment.account FROM GroupAssignment gAssignment JOIN gAssignment.group group1 JOIN gAssignment.account account1 WHERE account1.softDelete =:softDelete1 AND group1.client.name = :clientName AND group1.name = :groupName ORDER BY gAssignment.account.username";
		Query query = this.getEntityManager().createQuery(queryString);
		query.setParameter("groupName", groupName);
		query.setParameter("softDelete1", false);
		query.setParameter("clientName", clientName);
		List<Account> accountList = this.getResultList(query);
		this.commitTransaction();
		return accountList;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "usersOfGroup")
	public List<Account> getUsersByGroupId(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId) throws IllegalRequestException, BackendException {
		if (groupId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		String queryString = "SELECT gAssignment.account FROM GroupAssignment gAssignment WHERE gAssignment.group.id = :groupId ORDER BY gAssignment.account.username";
		Query query = this.getEntityManager().createQuery(queryString);
		query.setParameter("groupId", groupId);
		List<Account> resultList = this.getResultList(query);
		return resultList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "usersInRole")
	public List<Account> getUsersByRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "roleName") String roleName, @WebParam(name = "scope") String scope)
			throws BackendException, IllegalRequestException {
		if (clientName == null | applicationName == null | roleName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		Role role = this.getRoleByName(clientName, applicationName, roleName);
		return this.getUsersByRoleId(clientName, role.getId(), scope);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "userInGroup")
	@SuppressWarnings("unchecked")
	public boolean isUserInGroup(@WebParam(name = "uuid") String uuid, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException {
		if (uuid == null | groupId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		Query query = this.getEntityManager().createQuery("SELECT gAssignment FROM GroupAssignment gAssignment JOIN gAssignment.account acc JOIN gAssignment.group g  WHERE acc.uuid = :uuid AND g.id = :groupId");
		query.setParameter("uuid", uuid);
		query.setParameter("groupId", groupId);
		List<GroupAssignment> assignments = this.getResultList(query);
		if (assignments.size() == 1)
			return true;
		if (assignments.size() == 0)
			return false;
		throw new BackendException("Consistence Error: Multiple Assignments of an account to a group");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "userInRole")
	public boolean isUserInRoleById(@WebParam(name = "uuid") String uuid, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException {
		if (uuid == null | roleId == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		// TODO improvement: querying the client should not be necessary
		Client client = this.accountManagement.getClientByUuid(uuid);
		if (client != null) {
			List<Account> resultList = this.getUsersByRoleId(client.getClientName(), roleId, scope);
			for (Account account : resultList) {
				if (account.getUuid().equals(uuid))
					return true;
			}
			return false;
		} else
			throw new BackendException("Client does not exist for account with given uuid");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "userInRole")
	public boolean isUserInRole(@WebParam(name = "uuid") String uuid, @WebParam(name = "roleName") String roleName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "scope") String scope) throws BackendException,
			IllegalRequestException {
		if (uuid == null | roleName == null | applicationName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		Client client = this.accountManagement.getClientByUuid(uuid);
		Role role = this.getRoleByName(client.getName(), applicationName, roleName);
		return this.isUserInRoleById(uuid, role.getId(), scope);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "group")
	public Group setGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "group") Group group) throws BackendException, IllegalRequestException {
		if (group == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		ValidationUtil.validate(group);
		this.beginTransaction();
		if (group.getId() == null) {
			this.persist(group);
		} else {
			group = this.mergeInNewTransAction(group);
		}
		this.commitTransaction();
		return group;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "role")
	public Role setRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "role") Role role) throws BackendException, IllegalRequestException {
		if (role == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		ValidationUtil.validate(role);
		this.beginTransaction();
		if (role.getId() == null) {
			this.persist(role);
		} else {
			role = this.mergeInNewTransAction(role);
		}
		this.commitTransaction();
		return role;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	public void addUserToGroup(@WebParam(name = "uuid") String uuid, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException {
		if (uuid == null | groupId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		Group group = this.getGroup(null, groupId);
		Account account = this.accountManagement.getAccountByUuid(uuid);
		if (account == null | group == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		GroupAssignment groupAssignment = new GroupAssignment(account, group);
		ValidationUtil.validate(groupAssignment);

		this.beginTransaction();
		this.persist(groupAssignment);
		this.commitTransaction();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	public void addUserToRole(@WebParam(name = "uuid") String uuid, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException {
		if (uuid == null | roleId == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		if (this.isUserInRoleById(uuid, roleId, scope))
			throw new BackendException("User has already been assigned to the given role");
		Role role = this.getEntityManager().find(Role.class, roleId);
		Account account = this.accountManagement.getAccountByUuid(uuid);

		if (role != null && account != null) {
			RoleAssignment roleAssignment = new RoleAssignment();
			roleAssignment.setAccount(account);
			roleAssignment.setScope(scope);
			roleAssignment.setRole(role);
			this.beginTransaction();
			this.persist(roleAssignment);
			this.commitTransaction();
		} else {
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@SuppressWarnings("unchecked")
	public void deleteUserFromGroup(@WebParam(name = "uuid") String uuid, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException {
		// initial work to identify the assignment entity
		if (uuid == null | groupId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		this.beginTransaction();
		Query query = this.getEntityManager().createQuery("SELECT gAssignment FROM GroupAssignment gAssignment WHERE gAssignment.account.uuid =:uuid AND gAssignment.group.id =:groupId");
		query.setParameter("uuid", uuid);
		query.setParameter("groupId", groupId);
		List<GroupAssignment> groupAssignments = this.getResultList(query);
		if (groupAssignments.size() != 1) {
			if (groupAssignments.size() == 0) {
				// TODO Exception concept
				throw new BackendException("There is no assignment for the given group and user account");
			}
			// the following should never happen
			if (groupAssignments.size() > 1) {
				throw new BackendException("There are duplicate assignments for the given group and user account");
			}
		}
		// now delete the assignment

		this.getEntityManager().remove(groupAssignments.get(0));
		this.commitTransaction();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@SuppressWarnings("unchecked")
	public void deleteUserFromRole(@WebParam(name = "uuid") String uuid, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException {
		// TODO scope is probably optional
		if (uuid == null | roleId == null | scope == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		// initial work to identify the assignment entity
		// this.beginTransaction();
		Query query = this.getEntityManager().createQuery("SELECT rAssignment FROM RoleAssignment rAssignment JOIN rAssignment.account account1 JOIN rAssignment.role role1 WHERE account1.uuid =:uuid AND role1.id =:roleId AND rAssignment.scope =:scope1");
		query.setParameter("uuid", uuid);
		query.setParameter("roleId", roleId);
		query.setParameter("scope1", scope);
		List<RoleAssignment> roleAssignments = this.getResultList(query);
		if (roleAssignments.size() != 1) {
			if (roleAssignments.size() == 0) {
				// TODO Exception concept
				throw new BackendException("There is no assignment for the given group and user account");
			}
			// the following should never happen
			if (roleAssignments.size() > 1) {
				throw new BackendException("There are duplicate assignments for the given group and user account");
			}
		}
		// now delete the assignment
		this.getEntityManager().remove(roleAssignments.get(0));
		// this.commitTransaction();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	public void addGroupToRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException {
		// TODO scope is probably optional
		if (groupId == null | roleId == null | scope == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Group group = this.getGroup(clientName, groupId);
		Role role = this.getRole(clientName, roleId);
		if (role == null | group == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		RoleAssignment roleAssignment = new RoleAssignment(role, null, group, scope);
		ValidationUtil.validate(roleAssignment);
		this.beginTransaction();
		if (!this.isGroupInRole(clientName, groupId, roleId, scope)) {
			this.persist(roleAssignment);
			this.commitTransaction();
		} else
			throw IllegalRequestException.INVALID_DUPLICATE_EXCEPTION;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@SuppressWarnings("unchecked")
	public void deleteGroupFromRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException,
			IllegalRequestException {
		// TODO scope is probably optional
		if (groupId == null | roleId == null | scope == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		this.beginTransaction();
		Query query = this.getEntityManager().createQuery("SELECT rAssignment FROM RoleAssignment rAssignment JOIN rAssignment.group group1 JOIN rAssignment.role role1 WHERE group1.id = :groupId AND role1.id = :roleId AND rAssignment.scope = :scope1");
		query.setParameter("groupId", groupId);
		query.setParameter("roleId", roleId);
		query.setParameter("scope1", scope);
		List<RoleAssignment> roleAssignments = this.getResultList(query);
		if (roleAssignments.size() != 1) {
			if (roleAssignments.size() == 0)
				throw new BackendException("Error deleting Assignment of a given group to a role: Assignment does not exist");
			// this should never happen
			if (roleAssignments.size() > 1)
				throw new BackendException("Error deleting Assignment of a given group to a role: Multiple assignments exists");
		}
		this.getEntityManager().remove(roleAssignments.get(0));
		this.commitTransaction();
	}

	@Override
	@WebMethod
	@WebResult(name = "group")
	public Group getGroupByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupName") String groupName) throws BackendException, IllegalRequestException {
		if (groupName == null | clientName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		
		String qlString = "SELECT group1 FROM Group group1 JOIN group1.client client1 WHERE group1.name = :groupName AND client1.name = :clientName";
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("clientName", clientName);
		parameterMap.put("groupName", groupName);
		
		List<Group> resultList = this.executeQuery(Group.class, qlString, parameterMap);
		
		if(resultList.size() == 0)
			return null;
		Group group = resultList.get(0);
		group = new Group(group);
		return group;
		
	}

	@Override
	@WebMethod
	@WebResult(name = "role")
	public Role getRoleByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "roleName") String roleName) throws BackendException, IllegalRequestException {
		if (roleName == null | clientName == null | applicationName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		Query query = this.getEntityManager().createQuery("SELECT role FROM Role role JOIN role.application appl JOIN appl.client client1 WHERE role.name = :roleName AND client1.name = :clientName AND appl.name = :applicationName");
		query.setParameter("clientName", clientName);
		query.setParameter("roleName", roleName);
		query.setParameter("applicationName", applicationName);
		return (Role) this.getSingleResult(query);
	}

	@WebMethod
	@SuppressWarnings("unchecked")
	@Override
	@WebResult(name = "applicationRoles")
	public List<Role> getRolesByApplication(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName) throws BackendException, IllegalRequestException {
		if (clientName == null | applicationName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		this.beginTransaction();
		String queryString1 = "SELECT role FROM Role role JOIN role.application appl JOIN appl.client client1 WHERE appl.name = :applicationName  AND client1.name = :clientName";
		Query query = this.getEntityManager().createQuery(queryString1);
		query.setParameter("clientName", clientName);
		query.setParameter("applicationName", applicationName);
		List<Role> resultList = this.getResultList(query);
		this.commitTransaction();
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "usersInRole")
	public List<Account> getUsersByRoleId(@WebParam(name = "clientName") String clientName, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException {
		if (roleId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		String roleGroups = "SELECT rAssignment.group FROM RoleAssignment rAssignment WHERE rAssignment.role.id = :roleId";
		if (scope != null)
			roleGroups += " AND rAssignment.scope = :scope1";
		String groupAccounts = "SELECT DISTINCT gAssignment.account FROM GroupAssignment gAssignment WHERE gAssignment.group IN (" + roleGroups + ") AND gAssignment.account.softDelete = :softDelete1";
		String roleAccounts = "SELECT rAssignment.account FROM RoleAssignment rAssignment WHERE rAssignment.role.id = :roleId AND rAssignment.account.softDelete = :softDelete1";
		if (scope != null)
			roleAccounts += " AND rAssignment.scope = :scope1";
		String qlString = "SELECT DISTINCT account FROM Account account WHERE account IN (" + groupAccounts + ") OR account IN (" + roleAccounts + ")";

		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("roleId", roleId);
		query.setParameter("softDelete1", false);
		if (scope != null)
			query.setParameter("scope1", scope);
		List<Account> resultList = this.getResultList(query);
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "roleUsers")
	public List<RoleUser> getRoleUsers(@WebParam(name = "clientName") String clientName, @WebParam(name = "roleId") Long roleId, @WebParam(name = "roleScope") String roleScope) throws IllegalRequestException, BackendException {

		if (roleId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		String qlString = "SELECT roleUser FROM RoleUser roleUser WHERE roleUser.role_Id = :roleId";
		if (roleScope != null) {
			qlString += " AND roleUser.scope = :roleScope ";
		}
		qlString += " ORDER BY roleUser.displayName ";
		Query query = this.getEntityManager().createQuery(qlString);
		if (roleScope != null) {
			query.setParameter("roleScope", roleScope);
		}
		query.setParameter("roleId", roleId);

		List<RoleUser> resultList = this.getResultList(query);

		return resultList;
	}

	@WebMethod
	@WebResult(name = "rolesByGroup")
	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getRolesByGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupName") String groupName, @WebParam(name = "scope") String scope) throws IllegalRequestException, BackendException {
		if (clientName == null | groupName == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		String qlString = "SELECT rAssignment.role FROM RoleAssignment rAssignment WHERE rAssignment.group.name = :groupName AND rAssignment.group.client.name = :clientName";
		if (scope != null)
			qlString += " AND rAssignment.scope = :scope1";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("groupName", groupName);
		query.setParameter("clientName", clientName);
		if (scope != null)
			query.setParameter("scope1", scope);
		List<Role> resultList = this.getResultList(query);
		return resultList;
	}

	@WebMethod
	@WebResult(name = "clientOfGroup")
	@Override
	public Client getClientByGroup(@WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException {
		if (groupId == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		String qlString = "SELECT group1.client FROM Group group1 WHERE group1.id = :groupId";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("groupId", groupId);
		Client client = (Client) this.getSingleResult(query);
		return client;
	}

	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "applicationRoles")
	public List<Role> getRolesByApplicationId(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationId") Long applicationId) throws BackendException, IllegalRequestException {
		if (applicationId == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		String qlString = "SELECT role FROM Role role WHERE role.application.id = :applicationId";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("applicationId", applicationId);
		List<Role> resultList = this.getResultList(query);
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "rolesByGroup")
	public List<Role> getRolesByGroupId(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId, @WebParam(name = "scope") String scope) throws IllegalRequestException, BackendException {
		if (groupId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		String qlString = "SELECT DISTINCT rAssignment.role FROM RoleAssignment rAssignment WHERE rAssignment.group.id = :groupId";
		if (scope != null)
			qlString += " AND rAssignment.scope = :scope1";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("groupId", groupId);
		if (scope != null)
			query.setParameter("scope1", scope);
		List<Role> resultList = this.getResultList(query);
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	public boolean isGroupInRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException {
		if (groupId == null | roleId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		String qlString = "SELECT roleAssignment.group FROM RoleAssignment roleAssignment WHERE roleAssignment.role.id = :roleId AND roleAssignment.group.id = :groupId AND roleAssignment.scope = :roleScope";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("groupId", groupId);
		query.setParameter("roleId", roleId);
		query.setParameter("roleScope", scope);
		List<Group> resultList = this.getResultList(query);
		if (resultList.size() == 0)
			return false;
		else
			return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "usersByGroupAndRole")
	public List<Account> getUsersByGroupAndRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException,
			IllegalRequestException {
		if (groupId == null | roleId == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		boolean groupInRole = this.isGroupInRole(clientName, groupId, roleId, scope);
		if (groupInRole) {
			return this.getUsersByGroupId(clientName, groupId);
		}
		String subQuery0 = "SELECT groupAssignment.account FROM GroupAssignment groupAssignment WHERE groupAssignment.group.id = :groupId";
		String subQuery1 = "SELECT roleAssignment.account FROM RoleAssignment roleAssignment WHERE roleAssignment.account IN (" + subQuery0 + ") AND roleAssignment.role.id = :roleId AND roleAssignment.scope = :roleScope";
		String subQuery3 = "SELECT roleAssignment.group FROM RoleAssignment roleAssignment WHERE roleAssignment.scope = :roleScope AND roleAssignment.role.id = :roleId";
		String subQuery2 = "SELECT groupAssignment.account FROM GroupAssignment groupAssignment WHERE groupAssignment.group IN (" + subQuery3 + ") AND groupAssignment.account IN (" + subQuery0 + ")";
		String qlString = "SELECT DISTINCT account FROM Account account WHERE account IN (" + subQuery2 + ") OR account IN (" + subQuery1 + ")";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("roleScope", scope);
		query.setParameter("groupId", groupId);
		query.setParameter("roleId", roleId);
		List<Account> resultList = this.getResultList(query);
		return resultList;
	}

	@Override
	@WebMethod
	@WebResult(name = "userAccounts")
	public QueryResult<User> getUserAccountsByGroupId(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId,@WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws IllegalRequestException, BackendException {
		if(groupId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		this.beginTransaction();
		String defaultSortOrder = " user.username ASC ";		
		String qlString = "SELECT user FROM User user WHERE user.id IN (SELECT gAssignment.account.id FROM GroupAssignment gAssignment WHERE gAssignment.group.id = :groupId)";
		
		List<Class<?>> allowedOrderBeans = new LinkedList<Class<?>>();
		allowedOrderBeans.add(User.class);

		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("groupId", groupId);

		QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap, queryDescriptor, defaultSortOrder, allowedOrderBeans);
		QueryResult<User> executionResult = new QueryResult<User>();
		executionResult = this.executeSimpleQuery(qlString, queryConfiguration, executionResult);
		
		this.commitTransaction();
		return executionResult;
	}

/*	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "userAccounts")
	public List<User> getUserAccountsByGroupId(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId) throws IllegalRequestException, BackendException {
		if(groupId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		this.beginTransaction();
		String queryString = "SELECT new org.evolvis.idm.identity.account.model.User(gAssignment.account,'flag') FROM GroupAssignment gAssignment  WHERE gAssignment.group.id = :groupId ORDER BY gAssignment.account.username";
		Query query = this.getEntityManager().createQuery(queryString);
		query.setParameter("groupId", groupId);
		List<User> resultList = this.getResultList(query);
		this.commitTransaction();
		return resultList;
	}*/
}
