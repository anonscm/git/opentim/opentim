/**
 * 
 */
package org.evolvis.idm.relation.permission.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryConfiguration;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.InternalRoleAssignment;
import org.evolvis.idm.relation.permission.model.InternalRoleScope;
import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.jboss.wsf.spi.annotation.WebContext;

/**
 * @author Yorka Neumann
 */
@Stateless
@Local(InternalRoleManagement.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "InternalRoleManagementService", name = "InternalRoleManagementServicePT", portName = "InternalRoleManagementServicePort")
@WebContext(contextRoot = "idm-backend", urlPattern = "/InternalRoleManagementService")
// @EndpointConfig(configName = "Standard WSSecurity Endpoint")
public class InternalRoleManagementImpl extends AbstractBaseServiceImpl implements InternalRoleManagement {

	@Override
	@WebMethod
	@SuppressWarnings("unchecked")
	public void addUserToInternalRole(@WebParam(name = "uuid") String uuid, @WebParam(name = "internalRole") InternalRoleType internalRole) throws IllegalRequestException, BackendException {
		if (uuid == null | internalRole == null)
			throw new IllegalRequestException("Invalid parameter setting");
		this.beginTransaction();
		// first look if there exists an account with the given id.
		// TODO : replace concrete query with
		// AccountManagement.getAccount()
		Query query = this.getEntityManager().createQuery("SELECT account FROM Account account WHERE account.uuid = :uuid");
		query.setParameter("uuid", uuid);
		Account account = (Account) this.getSingleResult(query);

		// account exists, so look if there is already such an assignment
		// for this account.
		query = this.getEntityManager().createQuery("SELECT irAssignment FROM InternalRoleAssignment irAssignment WHERE irAssignment.type = :internalRole AND irAssignment.account = :account1");
		query.setParameter("account1", account);
		query.setParameter("internalRole", internalRole);

		List<InternalRoleAssignment> resultList = this.getResultList(query);
		// the account assignment to a specific internal role should only
		// exist once.
		if (resultList.size() == 0) {
			InternalRoleAssignment irAssignment = new InternalRoleAssignment();
			irAssignment.setAccount(account);
			irAssignment.setType(internalRole);
			this.persist(irAssignment);
		}
		this.commitTransaction();
	}

	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	public void deleteInternalRoleScope(@WebParam(name = "uuid") String uuid, @WebParam(name = "internalRole") InternalRoleType internalRole, @WebParam(name = "groupId") Long groupId) throws IllegalRequestException, BackendException {
		if (uuid == null | internalRole == null | groupId == null) {
			throw new IllegalRequestException("Illegal parameter setting");
		}
		this.beginTransaction();
		String qlString = "SELECT irScope FROM InternalRoleScope irScope WHERE irScope.group.id = :groupId AND irScope.internalRoleAssignment.type = :internalRole AND irScope.internalRoleAssignment.account.uuid = :uuid1";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("uuid1", uuid);
		query.setParameter("internalRole", internalRole);
		query.setParameter("groupId", groupId);
		List<InternalRoleScope> roleScopes = this.getResultList(query);
		// locate persistent InternalRoleScope entity with that id.
		if (roleScopes.size() == 1) {
			this.getEntityManager().remove(roleScopes.get(0));
			this.commitTransaction();
		}

		else {
			this.commitTransaction();
			if (roleScopes.size() == 0)
				throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
			else
				throw new BackendException("More than one role scope found. This is a constraint violation");
		}
	}

	/**
	 * @see org.evolvis.idm.relation.permission.service.InternalRoleManagement#getInternalRoleForUser(String)
	 */
	@WebMethod
	@SuppressWarnings("unchecked")
	@WebResult(name = "internalRole")
	public InternalRoleType getInternalRoleForUser(@WebParam(name = "uuid") String uuid) throws IllegalRequestException, BackendException {
		if (uuid == null) {
			throw new IllegalRequestException("Invalid parameter setting");
		}
		this.beginTransaction();
		// obtain the list of persistent internal role assignments for the
		// given user
		Query query = this.getEntityManager().createQuery("SELECT irAssignment.type FROM InternalRoleAssignment irAssignment JOIN irAssignment.account account WHERE account.uuid = :uuid1");
		query.setParameter("uuid1", uuid);
		List<InternalRoleType> resultList = this.getResultList(query);
		this.commitTransaction();
		if (resultList.size() >= 1) {
			// identify the role with the highest level
			InternalRoleType highestRole = InternalRoleType.USER;
			for (InternalRoleType type : resultList) {
				if (highestRole.compareTo(type) > 0) {
					highestRole = type;
				}
			}
			return highestRole;
		} else
			return InternalRoleType.USER;
	}

	/**
	 * @see 
	 *      org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement
	 *      # getInternalRoleScopesForAccount(List<InternalRoleScope>)
	 */
	@Override
	@WebMethod
	@WebResult(name = "groups")
	public List<Group> getInternalRoleScopesForAccount(@WebParam(name = "uuid") String uuid, @WebParam(name = "internalRole") InternalRoleType internalRole, @WebParam(name = "queryDescriptor")QueryDescriptor queryDescriptor) throws IllegalRequestException, BackendException {
		if (uuid == null | internalRole == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		this.beginTransaction();
		
		String subQuery = "SELECT irAssignment FROM InternalRoleAssignment irAssignment JOIN irAssignment.account account WHERE account.uuid = :uuid AND irAssignment.type = :internalRole";
		String qlString = "SELECT internalRoleScope.group FROM InternalRoleScope internalRoleScope WHERE internalRoleScope.internalRoleAssignment IN (" + subQuery + ") ";
		//Query query = this.getEntityManager().createQuery("SELECT irScope.group FROM InternalRoleScope irScope WHERE irScope.internalRoleAssignment IN (" + subQuery + ") ORDER BY irScope.group.displayName ASC");
		String defaultSortOrder = " internalRoleScope.group.displayName ASC";
		
		List<Class<?>> allowedOrderBeans = new LinkedList<Class<?>>();
		allowedOrderBeans.add(InternalRoleScope.class);
			
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("uuid", uuid);
		parameterMap.put("internalRole", internalRole);
		
		QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap, queryDescriptor, defaultSortOrder, allowedOrderBeans);
		
		QueryResult<Group> executionResult = new QueryResult<Group>();
		
		executionResult = this.executeSimpleQuery(qlString, queryConfiguration, executionResult);
		
		
		this.commitTransaction();
		return executionResult.getResultList();
	}

	@Override
	@WebMethod
	@SuppressWarnings("unchecked")
	@WebResult(name = "internalRoleAssignments")
	public List<InternalRoleAssignment> getInternalRolesForGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId) throws IllegalRequestException, BackendException {
		if (groupId == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		this.beginTransaction();
		Query query = this.getEntityManager().createQuery("SELECT irScope.internalRoleAssignment FROM InternalRoleScope irScope JOIN irScope.group group1 WHERE group1.id = :groupId");
		query.setParameter("groupId", groupId);
		List<InternalRoleAssignment> resultList = this.getResultList(query);
		this.commitTransaction();
		return resultList;
	}

	@Override
	@WebMethod
	@SuppressWarnings("unchecked")
	@WebResult(name = "internalRoles")
	public List<InternalRoleType> getInternalRolesOfUser(@WebParam(name = "uuid") String uuid) throws IllegalRequestException, BackendException {
		if (uuid == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		this.beginTransaction();
		Query query = this.getEntityManager().createQuery("SELECT irAssignment.type FROM InternalRoleAssignment irAssignment JOIN irAssignment.account account WHERE account.uuid = :uuid");
		query.setParameter("uuid", uuid);
		List<InternalRoleType> resultList = this.getResultList(query);
		if (resultList.size() == 0)
			resultList.add(InternalRoleType.USER);
		return resultList;
	}

	@Override
	@WebMethod
	@SuppressWarnings("unchecked")
	@WebResult(name = "accounts")
	public List<Account> getUsersOfInternalRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "internalRole") InternalRoleType internalRole) throws IllegalRequestException, BackendException {
		if (clientName == null | internalRole == null) {
			// A super administrator does not need an associated client.
			if (internalRole != InternalRoleType.SUPER_ADMIN)
				throw new IllegalRequestException("Invalid parameter setting");
		}
		this.beginTransaction();
		String queryString;
		Query query;
		if (internalRole == InternalRoleType.SUPER_ADMIN) {
			queryString = "SELECT DISTINCT account FROM InternalRoleAssignment irAssignment JOIN irAssignment.account account WHERE irAssignment.type = :internalRole";
			query = this.getEntityManager().createQuery(queryString);
		} else {
			queryString = "SELECT DISTINCT account FROM InternalRoleAssignment irAssignment JOIN irAssignment.account account JOIN account.client client WHERE irAssignment.type = :internalRole AND client.name = :clientName";
			query = this.getEntityManager().createQuery(queryString);
			query.setParameter("clientName", clientName);
		}
		query.setParameter("internalRole", internalRole);
		List<Account> resultList = this.getResultList(query);
		this.commitTransaction();
		return resultList;
	}

	@Override
	@WebMethod
	@SuppressWarnings("unchecked")
	public void removeUserFromInternalRole(@WebParam(name = "uuid") String uuid, @WebParam(name = "internalRole") InternalRoleType internalRole) throws IllegalRequestException, BackendException {
		if (uuid == null | internalRole == null) {
			throw new IllegalRequestException("Invalid parameter setting");
		}
		this.beginTransaction();
		// obtain the persistent assignment
		Query query = this.getEntityManager().createQuery("SELECT irAssignment FROM InternalRoleAssignment irAssignment JOIN irAssignment.account account WHERE account.uuid = :uuid AND irAssignment.type = :internalRole");
		query.setParameter("uuid", uuid);
		query.setParameter("internalRole", internalRole);
		// get a result list in order to be able to
		List<InternalRoleAssignment> resultList = this.getResultList(query);
		if (resultList.size() == 1) {
			// all right: remove assignment
			InternalRoleAssignment irAssignment = (InternalRoleAssignment) this.getSingleResult(query);
			this.getEntityManager().remove(irAssignment);
			this.commitTransaction();
		} else {
			this.commitTransaction();
			if (resultList.size() > 1) {
				// more than one assignment for account and special internal
				// role is recognized as an error
				throw new BackendException("Internal error: There is more than one assignment for the internal role " + internalRole + " and account " + uuid);
				// else: size 0 means that account is a has the internal
				// role
				// "USER" which does not need an explicit assignment: no
				// error
			} else
				throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		}
	}

	@Override
	@WebMethod(exclude = true)
	public void setInternalRoleScope(@WebParam(name = "internalRoleAssignmentId") Long internalRoleAssignmentId, @WebParam(name = "groupId") Long groupId) throws IllegalRequestException, BackendException {
		if (internalRoleAssignmentId == null | groupId == null) {
			throw new IllegalRequestException("Invalid parameter setting");
		}
		this.beginTransaction();

		InternalRoleAssignment irAssignment = this.getEntityManager().find(InternalRoleAssignment.class, internalRoleAssignmentId);

		// check if the assigned internal role is section administrator
		if (irAssignment.getType() == InternalRoleType.SECTION_ADMIN) {
			Group group = this.getEntityManager().find(Group.class, groupId);

			if (irAssignment == null | group == null)
				throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
			InternalRoleScope irScope = new InternalRoleScope();
			irScope.setGroup(group);
			irScope.setInternalRoleAssignment(irAssignment);
			this.persist(irScope);
			this.commitTransaction();
		} else {
			this.commitTransaction();
			throw new IllegalRequestException("This operation is not allowed to be executed for the internal role: " + irAssignment.getType().name());
		}
	}

	@Override
	@WebMethod
	public void setInternalRoleScope(@WebParam(name = "uuid") String uuid, @WebParam(name = "internalRole") InternalRoleType internalRole, @WebParam(name = "groupId") Long groupId) throws IllegalRequestException, BackendException {
		if (uuid == null | internalRole == null | groupId == null) {
			throw new IllegalRequestException("Invalid parameter setting");
		}
		this.beginTransaction();
		Query query = this.getEntityManager().createQuery("SELECT irAssignment.id FROM InternalRoleAssignment irAssignment JOIN irAssignment.account account WHERE account.uuid = :uuid AND irAssignment.type = :internalRole");
		query.setParameter("uuid", uuid);
		query.setParameter("internalRole", internalRole);
		Long irAssignmentId = (Long) this.getSingleResult(query);
		this.commitTransaction();
		this.setInternalRoleScope(irAssignmentId, groupId);
	}

	protected boolean isClientAdministrationAllowed() {
		return true;
		// return
		// isSuperAdministrationAllowed(sessionContext.getCallerPrincipal());
	}

}
