package org.evolvis.idm.relation.multitenancy.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.synchronization.service.DirectoryServiceSynchronization;
import org.jboss.ejb3.annotation.LocalBinding;
import org.jboss.ejb3.annotation.Service;

@Service
@LocalBinding(jndiBinding = "idm-backend-ear/ClientSchedulerImpl/local")
public class ClientSchedulerImpl extends AbstractBaseServiceImpl implements ClientScheduler {

	public static final DateFormat dateFormatter = new SimpleDateFormat("hh:mm");
	public static final long millisPerDay = 24 * 60 * 60 * 1000;

	private boolean hasBeenInitialized = false;

	@Resource
	private TimerService timerservice;

	private ClientManagement clientManagement;

	@EJB(name = "ejb/ClientManagementImpl")
	public void setClientManagement(ClientManagement clientManagement) {
		this.clientManagement = clientManagement;
	}

	private DirectoryServiceSynchronization directoryServiceSynchronization;

	@EJB(name = "ejb/DirectoryServiceSynchronizationImpl")
	public void setDirectoryServiceSynchronization(DirectoryServiceSynchronization directoryServiceSynchronization) {
		this.directoryServiceSynchronization = directoryServiceSynchronization;
	}

	private AccountManagement accountManagement;

	@EJB(name = "ejb/AccountManagementImpl")
	public void setAccountManagement(AccountManagement accountManagement) {
		this.accountManagement = accountManagement;
	}

	private UserManagement userManagement;

	@EJB(name = "ejb/UserManagementImpl")
	public void setUserManagement(UserManagement userManagement) {
		this.userManagement = userManagement;
	}

	@PostConstruct
	public void initialize() {
		if (!hasBeenInitialized) {
			log.info("Initializing ClientScheduler ...");
			
			hasBeenInitialized = true;

			initializeTimerservice();

			log.info("Successfully initialized ClientScheduler.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.evolvis.idm.relation.multitenancy.service.ClientScheduler#
	 * initialiseTimerservice()
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void initializeTimerservice() {
		log.info("Trying to initialize daily task execution timers ...");

		// cancel existing timers
		for (Timer timer : (Collection<Timer>) this.timerservice.getTimers()) {
			log.info("Existing timer will be canceled ... Timer info: " + timer.getInfo());
			timer.cancel();
			log.info("Existing timer has been canceled.");
		}
		// recreate timers for each client
		try {
			for (Client client : clientManagement.getClients(null).getResultList()) {
				ClientPropertyMap clientPropertyMap = client.getClientPropertyMap() == null ? ClientPropertyMap.createDefaultClientProperties() : client.getClientPropertyMap();
				ClientPropertyMap.fillMissingDefaults(client.getClientPropertyMap());
				String stringDailyJobExecutionTime = clientPropertyMap.get(ClientPropertyMap.KEY_SYSTEM_DAILYJOBEXECUTIONTIME);
				try {
					Date dailyJobExecutionTimeDate = dateFormatter.parse(stringDailyJobExecutionTime);
					Calendar hourDailyJobExecutionTime = Calendar.getInstance();
					hourDailyJobExecutionTime.setTime(dailyJobExecutionTimeDate);
					Calendar dailyJobExecutionTime = Calendar.getInstance();
					dailyJobExecutionTime.set(Calendar.HOUR_OF_DAY, hourDailyJobExecutionTime.get(Calendar.HOUR_OF_DAY));
					dailyJobExecutionTime.set(Calendar.MINUTE, hourDailyJobExecutionTime.get(Calendar.MINUTE));

					long millisCurrentTime = Calendar.getInstance().getTimeInMillis();
					long millisDailyJobExecutionTime = dailyJobExecutionTime.getTimeInMillis();

					long intervalMillisToNextExecution = millisDailyJobExecutionTime - millisCurrentTime;

					if (intervalMillisToNextExecution < 0) {
						intervalMillisToNextExecution = millisPerDay + intervalMillisToNextExecution;
					}

					Calendar calendar1970ToNextExecution = Calendar.getInstance();
					calendar1970ToNextExecution.setTimeInMillis(intervalMillisToNextExecution);

					log.info("Creating timer for client \"" + client.getName() + "\". Next execution will start in " 
							+ (calendar1970ToNextExecution.get(Calendar.HOUR_OF_DAY) - 1) + ":" // TODO remove -1 hack
							+ calendar1970ToNextExecution.get(Calendar.MINUTE) + ":"
							+ calendar1970ToNextExecution.get(Calendar.SECOND) + " (hh:mm:ss).");
					this.timerservice.createTimer(intervalMillisToNextExecution, millisPerDay, client.getName());
				} catch (ParseException e) {
					// skip client if date cannot be parsed
					continue;
				}
			}
			log.info("Successfully created daily task execution timers.");
		} catch (BackendException e) {
			log.error("An unknown error occured while initializing daily task execution timers.", e);
			e.printStackTrace();
		}
	}
	
	@Timeout
	public void executeJobs(Timer timer) {
		try {
			String clientName = (String) timer.getInfo();
			log.info("Executing daily jobs for client \"" + clientName + "\" ...");

			ClientPropertyMap clientPropertyMap = clientManagement.getClientPropertyMap(clientName);

			boolean ldapSyncEnabled = clientPropertyMap.getBoolean(ClientPropertyMap.KEY_LDAP_SYNC_ENABLED);
			long deactivatedAccountLifetimeInDays = Long.valueOf(clientPropertyMap.get(ClientPropertyMap.KEY_SYSTEM_DEACTIVATEDACCOUNTLIFETIMEINDAYS));

			long millisCurrentTime = System.currentTimeMillis();

			log.info("Task 1: Delete accounts which have been deactivated for at least " + deactivatedAccountLifetimeInDays + " days.");
			if (ldapSyncEnabled) {
				log.info("Task 2: Synchronize private data with LDAP server.");
			}

			for (Account account : accountManagement.getAccounts(clientName, null).getResultList()) {
				// delete accounts if account deletion interval has been reached
				if (account.isSoftDelete()) {
					long millisDeactivationTime = account.getDeactivationDate().getTimeInMillis();
					long daysAccountIsDeactivated = (millisCurrentTime - millisDeactivationTime) / millisPerDay;

					// delete user if account deactivation lifetime has been
					// reached or exceeded
					if (daysAccountIsDeactivated >= deactivatedAccountLifetimeInDays) {
						try {
							log.info("Deleting account with uuid=" + account.getUuid() + " ...");
							userManagement.deleteUser(account.getUuid());
						} catch (BackendException be) {
							// ignore exception on single user deletion
							log.warn("An error occurred while trying to delete user with uuid=" + account.getUuid(), be);
						}
					}
				}

				// synchronize personal data with LDAP if synchronization is
				// enabled
				// and user has not been deleted before
				else if (ldapSyncEnabled) {
					log.info("Deleting account with uuid=" + account.getUuid() + " ...");
					try {
						directoryServiceSynchronization.synchroniseAccount(account.getUuid());
					} catch (BackendException be) {
						// TODO log synchronization details properly in separate
						// log-files
						// ignore exception on single user sync
						log.warn("An error occurred while synchronizing account with uuid=" + account.getUuid(), be);
					}
				}
			}
			log.info("Successfully executed daily jobs for client \"" + clientName + "\".");
		} catch (Exception e) {
			log.error("An unknown error occured while executing job timer. Timer info: " + timer.getInfo(), e);
			e.printStackTrace();
		}
	}
}
