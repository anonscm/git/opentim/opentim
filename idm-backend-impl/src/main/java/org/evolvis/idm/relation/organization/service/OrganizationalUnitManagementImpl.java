package org.evolvis.idm.relation.organization.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryConfiguration;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.organization.model.OrgUnitMap;
import org.evolvis.idm.relation.organization.model.OrgUnitMapEntry;
import org.evolvis.idm.relation.organization.model.OrgUnitMapEntryQueryResult;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.jboss.wsf.spi.annotation.WebContext;

/**
 * @author Jens Neumaier
 * @author Yorka Neumann
 */
@Stateless
@Local(OrganizationalUnitManagement.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "OrganizationalUnitManagementService", name = "OrganizationalUnitManagementServicePT", portName = "OrganizationalUnitManagementServicePort")
@WebContext(contextRoot = "idm-backend", urlPattern = "/OrganizationalUnitManagementService")
public class OrganizationalUnitManagementImpl extends AbstractBaseServiceImpl implements OrganizationalUnitManagement {

	private GroupAndRoleManagement groupAndRoleManagement;
	private AccountManagement accountManagement;
	private ClientManagement clientManagement;

	@EJB(name = "ejb/ClientManagementImpl")
	public void setClientManagement(ClientManagement clientManagement) {
		this.clientManagement = clientManagement;
	}

	@EJB(name = "ejb/GroupAndRoleManagementImpl")
	public void setGroupAndRoleManagement(GroupAndRoleManagement groupAndRoleManagement) {
		this.groupAndRoleManagement = groupAndRoleManagement;
	}

	@EJB(name = "ejb/AccountManagementImpl")
	public void setAccountManagement(AccountManagement accountManagement) {
		this.accountManagement = accountManagement;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	public void deleteOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnit") OrgUnit orgUnit) throws BackendException, IllegalRequestException {
		if (orgUnit == null)
			throw new IllegalRequestException("Invalid parameter setting");
		else
			this.deleteOrgUnitById(orgUnit.getClientName(), orgUnit.getId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	public void deleteOrgUnitById(@WebParam(name = "clientName") String clientName, @WebParam(name = "organisationalUnitId") Long organisationalUnitId) throws BackendException, IllegalRequestException {
		if (organisationalUnitId == null)
			throw new IllegalRequestException("Invalid parameter setting");

		OrgUnit orgUnit = this.getOrgUnit(clientName, organisationalUnitId);
		if (orgUnit != null) {
			this.beginTransaction();
			this.remove(orgUnit);
			orgUnit.getGroup();
			this.commitTransaction();
		} else
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "orgUnit")
	public OrgUnit getOrgUnitByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitName") String organisationalUnitName) throws BackendException, IllegalRequestException {
		if (clientName == null | organisationalUnitName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		String qlString = "SELECT orgunit FROM OrgUnit orgUnit WHERE orgUnit.name = :orgUnitName AND orgUnit.group.client.name =:clientName";
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("orgUnitName", organisationalUnitName);
		parameterMap.put("clientName", clientName);
		List<OrgUnit> resultList = this.executeQuery(OrgUnit.class, qlString, parameterMap);
		if(resultList.size() == 0)
			return null;
		return resultList.get(0);
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "orgUnit")
	public OrgUnit getOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long organisationalUnitId) throws BackendException, IllegalRequestException {
		if (organisationalUnitId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		OrgUnit orgUnit = this.find(OrgUnit.class, organisationalUnitId);

		return orgUnit;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(name = "orgUnitsByUser")
	public List<OrgUnit> getOrgUnitsByUser(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException {
		if (uuid == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Account account = this.accountManagement.getAccountByUuid(uuid);
		if (account == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		this.beginTransaction();
		String queryString = "SELECT orgUnit FROM OrgUnit orgUnit WHERE orgUnit.group IN (SELECT gAssignment.group FROM GroupAssignment gAssignment WHERE gAssignment.account.uuid =:uuid1) ORDER BY orgUnit.name";
		Query query = this.getEntityManager().createQuery(queryString);
		query.setParameter("uuid1", uuid);
		List<OrgUnit> resultList = this.getResultList(query);
		this.commitTransaction();
		return resultList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "orgUnits")
	public QueryResult<OrgUnit> getOrgUnits(@WebParam(name = "clientName") String clientName, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, IllegalRequestException {
		if (clientName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("clientName", clientName);

		List<Class<?>> allowedOrderBeans = new LinkedList<Class<?>>();
		allowedOrderBeans.add(OrgUnit.class);

		String qlString = "SELECT orgUnit FROM OrgUnit orgUnit WHERE orgUnit.group.client.name = :clientName";
		QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap, queryDescriptor, "orgUnit.name ASC", allowedOrderBeans);

		QueryResult<OrgUnit> queryResult = new QueryResult<OrgUnit>();
		queryResult = this.executeSimpleQuery(qlString, queryConfiguration, queryResult);
		return queryResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "accountsOfOrgUnit")
	public List<Account> getUsersByOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitName") String orgUnitName) throws BackendException, IllegalRequestException {
		if (orgUnitName == null | clientName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Group group = this.getGroupByOrgUnit(clientName, orgUnitName);
		if (group == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		List<Account> resultList = this.getUsersByOrgUnitAndGroup(clientName, group.getName(), orgUnitName);
		return resultList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "accountsOfOrgUnitAndGroup")
	public List<Account> getUsersByOrgUnitAndGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupName") String groupName, @WebParam(name = "orgUnitName") String orgUnitName) throws BackendException, IllegalRequestException {
		if (orgUnitName == null | groupName == null | clientName == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		return this.groupAndRoleManagement.getUsersByGroup(clientName, groupName);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "orgUnit")
	public OrgUnit setOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnit") OrgUnit orgUnit) throws BackendException, IllegalRequestException {
		if (orgUnit == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		ValidationUtil.validate(orgUnit);
		if (orgUnit.getOrgUnitMap() != null)
			ValidationUtil.validate(orgUnit.getOrgUnitMap());
		Client client = null;
		if (orgUnit.getClientName() == null)
			client = this.clientManagement.getClientByName(clientName);
		else
			client = orgUnit.getGroup().getClient();
		this.beginTransaction();
		if (orgUnit.getId() == null) {
			// control if there is no orgUnit with the same name for that client
			OrgUnit persistentOrgUnit = this.getOrgUnitByName(client.getName(), orgUnit.getName());
			if(persistentOrgUnit != null)
				throw IllegalRequestException.INVALID_DUPLICATE_EXCEPTION;
			this.persist(orgUnit);
		} else {
			boolean isAlreadyPersisted = false;
			OrgUnit persistentOrgUnit = this.getOrgUnitByName(client.getName(), orgUnit.getName());
			if(persistentOrgUnit != null && !persistentOrgUnit.getId().equals(orgUnit.getId()))
				isAlreadyPersisted = true;
			if (isAlreadyPersisted)
				throw IllegalRequestException.INVALID_DUPLICATE_EXCEPTION;
				
			orgUnit = this.syncDeletedMapEntries(orgUnit);
			orgUnit = this.merge(orgUnit);
			//orgUnit = this.getOrgUnit(orgUnit.getClientName(), orgUnit.getId());
		}
		this.commitTransaction();

		return orgUnit;
	}

	/*	*//**
	 * {@inheritDoc}
	 */
	/*
	 * @Override
	 * 
	 * @WebMethod(exclude = true)
	 * 
	 * @WebResult(name = "accountsOfOrgUnit") public List<Account>
	 * getUsersByOrgUnit(@WebParam(name = "organisationalUnitId") Long
	 * organisationalUnitId) throws BackendException, IllegalRequestException {
	 * if (organisationalUnitId == null) throw
	 * IllegalRequestException.MISSING_PARAMETERS_EXCEPTION; try {
	 * 
	 * } catch (Exception e) { log.error(e.toString(), e); throw new
	 * BackendException(e.getMessage()); } }
	 */

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "accountsOfOrgUnitAndGroup")
	public List<Account> getUsersByOrgUnitAndGroupById(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long organisationalUnitId, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException {
		if (organisationalUnitId == null | groupId == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		return this.groupAndRoleManagement.getUsersByGroupId(clientName, groupId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "removedOrgUnitValue")
	public String addValueToOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "organisationalUnitId") Long organisationalUnitId, @WebParam(name = "key") String key, @WebParam(name = "value") String value)
			throws IllegalRequestException, BackendException {
		if (organisationalUnitId == null | key == null | value == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		OrgUnit orgUnit = this.getOrgUnit(clientName, organisationalUnitId);
		if (orgUnit == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		String oldValue = orgUnit.getOrgUnitMap().put(key, value);
		return oldValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "metaDataMap")
	public OrgUnitMap getOrgUnitMap(@WebParam(name = "clientName") String clientName, @WebParam(name = "organisationalUnitId") Long organisationalUnitId, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, IllegalRequestException {
		if (organisationalUnitId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		OrgUnit orgUnit = this.getOrgUnit(clientName, organisationalUnitId);
		return orgUnit.getOrgUnitMap();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(name = "orgUnitsByGroup")
	public List<OrgUnit> getOrgUnitsByGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException {
		if (groupId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Query query = this.getEntityManager().createQuery("SELECT orgUnit FROM OrgUnit orgUnit WHERE orgUnit.group.id = :groupId ORDER BY orgUnit.name ASC");
		query.setParameter("groupId", groupId);
		this.beginTransaction();
		List<OrgUnit> resultList = this.getResultList(query);
		this.commitTransaction();
		return resultList;
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "orgUnitsByValue")
	public List<OrgUnit> getOrgUnitsByValue(@WebParam(name = "clientName") String clientName, @WebParam(name = "value") String value) throws BackendException, IllegalRequestException {
		if (clientName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		if (value == null | value.compareTo("") == 0)
			return new java.util.Vector<OrgUnit>();

		String queryString = "SELECT DISTINCT orgUnitMapValue.map.orgUnit FROM OrgUnitMapEntry orgUnitMapValue WHERE orgUnitMapValue.value =:value1 AND orgUnitMapValue.map.orgUnit.group.client.name =:clientName ORDER BY orgUnitMapValue.map.orgUnit.name";
		Query query = this.getEntityManager().createQuery(queryString);
		query.setParameter("value1", value);
		query.setParameter("clientName", clientName);
		List<OrgUnit> resultList = this.getResultList(query);
		return resultList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "metaDataValue")
	public String getValueFromOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "organisationalUnitId") Long organisationalUnitId, @WebParam(name = "key") String key) throws BackendException, IllegalRequestException {
		if (key == null | organisationalUnitId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		OrgUnit orgUnit = this.getOrgUnit(clientName, organisationalUnitId);
		if (orgUnit != null) {
			String result = orgUnit.getOrgUnitMap().get(key);
			return result;
		}
		throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
	}

	private OrgUnit syncDeletedMapEntries(OrgUnit orgUnit) throws BackendException {
		OrgUnit persistentUnit = this.getOrgUnit(orgUnit.getClientName(), orgUnit.getId());
		List<OrgUnitMapEntry> entriesToDelete = this.getMissingElements(persistentUnit.getOrgUnitMap().getMapEntries(), orgUnit.getOrgUnitMap().getMapEntries());
		for (OrgUnitMapEntry entryToDelete : entriesToDelete) {
			this.remove(entryToDelete);
		}
		return orgUnit;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "removedOrgUnitValue")
	public String removeValueFromOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "organisationalUnitId") Long organisationalUnitId, @WebParam(name = "key") String key) throws BackendException, IllegalRequestException {
		if (key == null | organisationalUnitId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		OrgUnit orgUnit = this.getOrgUnit(clientName, organisationalUnitId);

		if (orgUnit != null) {
			// in order to detach the orgUnit - future TODO : improve this
			getEntityManager().clear();
			if(orgUnit.getOrgUnitMap().containsKey(key)){
				String result = orgUnit.getOrgUnitMap().remove(key);
				this.setOrgUnit(clientName, orgUnit);
				return result;
			}
			else return null;
		} else
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
	}

	@Override
	@WebMethod
	@WebResult(name = "groupOfOrgUnit")
	public Group getGroupByOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitName") String orgUnitName) throws BackendException, IllegalRequestException {
		if (clientName == null | orgUnitName == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		this.beginTransaction();
		String qlString = "SELECT orgUnit.group FROM OrgUnit orgUnit WHERE orgUnit.name = :orgUnitName AND orgUnit.group.client.name = :clientName";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("orgUnitName", orgUnitName);
		query.setParameter("clientName", clientName);
		Group group = (Group) this.getSingleResult(query);
		this.commitTransaction();
		return group;
	}

	@Override
	@WebMethod
	@WebResult(name = "accountsOfOrgUnit")
	public List<Account> getUsersByOrgUnitId(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long orgUnitId) throws BackendException, IllegalRequestException {
		if (orgUnitId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Group group = this.getGroupByOrgUnitId(clientName, orgUnitId);
		return groupAndRoleManagement.getUsersByGroupId(clientName, group.getId());
	}

	@Override
	@WebMethod
	@WebResult(name = "groupOfOrgUnit")
	public Group getGroupByOrgUnitId(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long orgUnitId) throws BackendException {
		if (orgUnitId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		String qlString = "SELECT orgUnit.group FROM OrgUnit orgUnit WHERE orgUnit.id = :orgUnitId";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("orgUnitId", orgUnitId);
		Group group = (Group) this.getSingleResult(query);
		return group;
	}

	@Override
	@WebMethod
	@WebResult(name = "orgUnitAndRoleUsers")
	public List<Account> getUsersByOrgUnitAndRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long orgUnitId, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException,
			IllegalRequestException {
		if (orgUnitId == null | roleId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Group group = this.getGroupByOrgUnitId(clientName, orgUnitId);
		if (group == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		return this.groupAndRoleManagement.getUsersByGroupAndRole(clientName, group.getId(), roleId, scope);
	}

	@Override
	@WebMethod
	@WebResult(name = "mapEntries")
	public OrgUnitMapEntryQueryResult getOrgUnitMapEntries(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long orgUnitId, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor)
			throws IllegalRequestException, BackendException {
		if (orgUnitId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		this.beginTransaction();
		String qlString = "SELECT orgUnitMapEntry FROM OrgUnitMapEntry orgUnitMapEntry WHERE orgUnitMapEntry.map.orgUnit.id = :orgUnitId ";

		List<Class<?>> allowedOrderBeans = new LinkedList<Class<?>>();
		allowedOrderBeans.add(OrgUnitMapEntry.class);

		String defaultSortOrder = " orgUnitMapEntry.key ASC ";

		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("orgUnitId", orgUnitId);

		QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap, queryDescriptor, defaultSortOrder, allowedOrderBeans);

		OrgUnitMapEntryQueryResult queryResult = new OrgUnitMapEntryQueryResult();
		queryResult = this.executeSimpleQuery(qlString, queryConfiguration, queryResult);
		/*This method is used to get the entries of the map of an org unit in a certain order and may be only a subset of it.
		 *....*/
		//if(queryResult.size()==0){
			qlString = "SELECT orgUnit FROM OrgUnit orgUnit WHERE orgUnit.id = :orgUnitId";
			List<OrgUnit> mapList = this.executeQuery(OrgUnit.class, qlString, parameterMap);
			if(mapList != null && mapList.size() == 1){
				this.getEntityManager().clear();
				OrgUnit orgUnit = mapList.get(0);
				orgUnit.getOrgUnitMap().setMapEntries(null);
				queryResult.setOrgUnit(orgUnit);
			}
			else{
				this.commitTransaction();
				throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
			}
		//}
		this.commitTransaction();

		return queryResult;
	}

}