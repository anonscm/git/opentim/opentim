package org.evolvis.idm.relation.multitenancy.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryConfiguration;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.jboss.wsf.spi.annotation.WebContext;

/**
 * @author Jens Neumaier
 * @author Yorka Neumann
 */
@Stateless
@Local(ApplicationManagement.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "ApplicationManagementService", name = "ApplicationManagementServicePT", portName = "ApplicationManagementServicePort")
@WebContext(contextRoot = "idm-backend", urlPattern = "/ApplicationManagementService")
// @EndpointConfig(configName = "Standard WSSecurity Endpoint")
public class ApplicationManagementImpl extends AbstractBaseServiceImpl implements ApplicationManagement {
	
	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	@WebResult(name = "application")
	public Application setApplication(@WebParam(name = "clientName") String clientName, @WebParam(name = "application") Application application) throws BackendException, IllegalRequestException, AccessDeniedException {
		if (!isApplicationAdministrationAllowed())
			throw new AccessDeniedException("Coundn't set application data. Permission denied.");
		
		ValidationUtil.validate(application);
		
		this.beginTransaction();
		// TODO may be a client should by injected 
		if(application.getId() == null){
			this.persist(application);
		}
		else{
			application = this.mergeInNewTransAction(application);
		}
		this.commitTransaction();

		return (Application) application;
	}

	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	public void deleteApplication(@WebParam(name = "clientName") String clientName, @WebParam(name = "application") Application application) throws BackendException, AccessDeniedException, IllegalRequestException {
		if (!isApplicationAdministrationAllowed())
			throw new AccessDeniedException("Coundn't delete application data. Permission denied.");

		if (isContainerManagedTransactionDisabled())
			getEntityManager().getTransaction().begin();
		application = getEntityManager().find(Application.class, application.getId());
		if (application != null)
			getEntityManager().remove(application);
		if (isContainerManagedTransactionDisabled())
			getEntityManager().getTransaction().commit();
	}

	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	@WebResult(name = "applications")
	public QueryResult<Application> getApplications(@WebParam(name = "clientName") String clientName, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, AccessDeniedException, IllegalRequestException {
		if (!isApplicationAdministrationAllowed())
			throw new AccessDeniedException("Coundn't get application data. Permission denied.");

		String qlString = "SELECT application FROM Application application WHERE application.client.name = :clientName";
		String defaultSortOrder = " application.name ASC ";
		List<Class<?>> allowedOrderBeans = new LinkedList<Class<?>>();
		allowedOrderBeans.add(Application.class);

		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("clientName", clientName);

		QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap, queryDescriptor, defaultSortOrder, allowedOrderBeans);

		QueryResult<Application> queryResult = new QueryResult<Application>();
		queryResult = this.executeSimpleQuery(qlString, queryConfiguration, queryResult);
		return queryResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	@WebResult(name = "application")
	public Application getApplicationById(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationId") Long applicationId) throws BackendException, AccessDeniedException, IllegalRequestException {
		if (!isApplicationAdministrationAllowed())
			throw new AccessDeniedException("Coundn't get application data. Permission denied.");

		Application application = getEntityManager().find(Application.class, applicationId);
		if (application == null)
			return null;

		return application;
	}

	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	@WebResult(name = "application")
	public Application getApplicationByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName) throws BackendException, AccessDeniedException, IllegalRequestException {
		if (clientName == null | applicationName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Query query = getEntityManager().createQuery("SELECT app FROM Application app WHERE app.name = :applicationName AND app.client.name = :clientName");
		query.setParameter("applicationName", applicationName);
		query.setParameter("clientName", clientName);

		return (Application) this.getSingleResult(query);
	}

	protected boolean isApplicationAdministrationAllowed() {
		// TODO replace this with code that respect WS-Security
		return true;
	}


	@Override
	@WebMethod
	@WebResult(name = "application")
	public Application getApplicationByUuid(@WebParam(name = "uuid") String uuid, @WebParam(name = "applicationName") String applicationName) throws IllegalRequestException, BackendException {
		if (uuid == null | applicationName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		this.beginTransaction();
		String clientByUuid = "SELECT account.client FROM Account account WHERE account.uuid = :uuid1";
		String qlString = "SELECT application FROM Application application WHERE application.name = :applicationName AND application.client IN (" + clientByUuid + ")";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("applicationName", applicationName);
		query.setParameter("uuid1", uuid);

		Application application = (Application) this.getSingleResult(query);
		this.commitTransaction();
		return application;
	}
}
