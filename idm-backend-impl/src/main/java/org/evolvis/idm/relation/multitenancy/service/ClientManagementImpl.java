package org.evolvis.idm.relation.multitenancy.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.CurrentDate;
import org.evolvis.idm.common.model.QueryConfiguration;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.AttributeType;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMapEntry;
import org.evolvis.idm.relation.multitenancy.model.ClientQueryResult;
import org.jboss.wsf.spi.annotation.WebContext;

/**
 * @author Jens Neumaier
 * @author Yorka Neumann
 */
@Stateless
@Local(ClientManagement.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "ClientManagementService", name = "ClientManagementServicePT", portName = "ClientManagementServicePort")
@WebContext(contextRoot = "idm-backend", urlPattern = "/ClientManagementService")
// @EndpointConfig(configName = "Standard WSSecurity Endpoint")
public class ClientManagementImpl extends AbstractBaseServiceImpl implements ClientManagement {

	private ClientScheduler clientScheduler;

	// @EJB(name = "ejb/ClientSchedulerImpl")
	public void setClientScheduler(ClientScheduler clientScheduler) {
		this.clientScheduler = clientScheduler;
	}

	@PostConstruct
	public void initialize() {
		try {
			InitialContext localContext = new InitialContext();
			setClientScheduler((ClientScheduler) localContext.lookup("idm-backend-ear/ClientSchedulerImpl/local"));
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	@WebResult(name = "client")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Client setClient(@WebParam(name = "entityManagerId") int entityManagerId, @WebParam(name = "client") Client client) throws BackendException, IllegalRequestException {
		// Client client = new Client(client);

		if (!isClientAdministrationAllowed())
			throw new SecurityException("Coundn't set client data. Permission denied.");

		ValidationUtil.validate(client);

		this.beginTransaction();

		if (entityManagerId < 0 || entityManagerId >= getEntityManagers().length)
			throw BackendException.ENTITYMANAGER_UNAVAILABE_EXCEPTION;

		if (client.getId() == null) {

			for (Client existingClient : getClients(null).getResultList()) {
				if (existingClient.getName().equals(client.getName()))
					throw IllegalRequestException.INVALID_DUPLICATE_CLIENT_EXCEPTION;
			}
			getEntityManagers()[entityManagerId].persist(client);

			// TODO we could load this also from a "default" client from the
			// database ???

			AttributeGroup name = new AttributeGroup("personalData", "Name", 1, false);
			name.setClient(client);
			/* create and add select list-attribute for salutation */
			Attribute salutationAttribute = new Attribute("salutation", "Anrede", 1, false, AttributeType.SELECTLIST, false);
			salutationAttribute.addSubAttribute(new Attribute("mr","Herr",1,false,AttributeType.STRING,false));
			salutationAttribute.addSubAttribute(new Attribute("mrs","Frau",2,false,AttributeType.STRING,false));
			salutationAttribute.addSubAttribute(new Attribute("miss","Fräulein",3,false,AttributeType.STRING,false));
			name.addAttribute(salutationAttribute);
			
			/* create and add select list-attribute for firstname */
			Attribute attributeFirstname = new Attribute("firstname", "Vorname", 2, false, AttributeType.VALIDATABLE, true);
			attributeFirstname.setAdditionalTypeConstraint("[\\p{L}-'`´ ]{2,40}");
			name.addAttribute(attributeFirstname);
			
			/* create and add select list-attribute for lastname */
			Attribute attributeLastname = new Attribute("lastname", "Nachname", 3, false, AttributeType.VALIDATABLE, true);
			attributeLastname.setAdditionalTypeConstraint("[\\p{L}-'`´ ]{2,40}");
			name.addAttribute(attributeLastname);
			/* create and add select list-attribute for birthdate */
			name.addAttribute(new Attribute("birthdate", "Geburtsdatum", 4, false, AttributeType.DATE, false));
			
			/* create and add select list-attribute for gender */
			Attribute genderAttribute = new Attribute("gender", "Geschlecht", 5, false, AttributeType.SELECTLIST, false);
			genderAttribute.addSubAttribute(new Attribute("male", "männlich", 1, false, AttributeType.STRING, false));
			genderAttribute.addSubAttribute(new Attribute("female", "weiblich",2, false, AttributeType.STRING, false));
			name.addAttribute(genderAttribute);
			
			/* create and add select list-attribute for nationality */
			Attribute  nationalityAttribute = new Attribute("nationality", "Nationalität", 6, false, AttributeType.SELECTLIST, false);
			nationalityAttribute.addSubAttribute(new Attribute("german", "deutsch", 1, false, AttributeType.STRING, false));
			name.addAttribute(nationalityAttribute);
			

			AttributeGroup primaryAddress = new AttributeGroup("primaryAddress", "Meldeadresse", 2, false);
			primaryAddress.setClient(client);
			primaryAddress.addAttribute(new Attribute("streetName", "Straße", 1, false, AttributeType.STRING, false));
			primaryAddress.addAttribute(new Attribute("streetNumber", "Hausnummer", 2, false, AttributeType.STRING, false));
			Attribute primaryCountryAttribute = new Attribute("country", "Land", 3, false, AttributeType.SELECTLIST, false);
			primaryCountryAttribute.addSubAttribute(new Attribute("DE","Deutschland",1,false, AttributeType.STRING, false));
			primaryCountryAttribute.addSubAttribute(new Attribute("FR","Frankreich",2,false, AttributeType.STRING, false));
			primaryCountryAttribute.addSubAttribute(new Attribute("PL","Polen",3,false, AttributeType.STRING, false));
			primaryCountryAttribute.addSubAttribute(new Attribute("GB","Vereinigtes Königreich",4,false, AttributeType.STRING, false));
			primaryAddress.addAttribute(primaryCountryAttribute);
			primaryAddress.addAttribute(new Attribute("zipCode", "PLZ", 4, false, AttributeType.STRING, false));
			primaryAddress.addAttribute(new Attribute("city", "Ort", 5, false, AttributeType.STRING, false));
			primaryAddress.addAttribute(new Attribute("state", "Bundesland", 6, false, AttributeType.STRING, false));

			AttributeGroup contact = new AttributeGroup("contactData", "Kontaktdaten", 3, false);
			contact.setClient(client);
			contact.addAttribute(new Attribute("phone", "Telefon", 1, true, AttributeType.STRING, false));
			contact.addAttribute(new Attribute("fax", "Telefax", 2, true, AttributeType.STRING, false));
			contact.addAttribute(new Attribute("mobile", "Mobil", 3, true, AttributeType.STRING, false));
			
			AttributeGroup furtherAddresses = new AttributeGroup("furtherAddresses", "Weitere Adressen", 2, false);
			furtherAddresses.setClient(client);
			furtherAddresses.addAttribute(new Attribute("streetName", "Straße", 1, false, AttributeType.STRING, false));
			furtherAddresses.addAttribute(new Attribute("streetNumber", "Hausnummer", 2, false, AttributeType.STRING, false));
			Attribute furtherCountryAttribute = new Attribute("country", "Land", 3, false, AttributeType.SELECTLIST, false);
			furtherCountryAttribute.addSubAttribute(new Attribute("DE","Deutschland",1,false, AttributeType.STRING, false));
			furtherCountryAttribute.addSubAttribute(new Attribute("FR","Frankreich",2,false, AttributeType.STRING, false));
			furtherCountryAttribute.addSubAttribute(new Attribute("PL","Polen",3,false, AttributeType.STRING, false));
			furtherCountryAttribute.addSubAttribute(new Attribute("GB","Vereinigtes Königreich",4,false, AttributeType.STRING, false));
			furtherAddresses.addAttribute(furtherCountryAttribute);
			furtherAddresses.addAttribute(new Attribute("zipCode", "PLZ", 4, false, AttributeType.STRING, false));
			furtherAddresses.addAttribute(new Attribute("city", "Ort", 5, false, AttributeType.STRING, false));
			furtherAddresses.addAttribute(new Attribute("state", "Bundesland", 6, false, AttributeType.STRING, false));

			getEntityManagers()[entityManagerId].persist(name);
			getEntityManagers()[entityManagerId].persist(primaryAddress);
			getEntityManagers()[entityManagerId].persist(contact);
			getEntityManagers()[entityManagerId].persist(furtherAddresses);
		} else {
			client = getEntityManagers()[entityManagerId].merge(client);
		}
		this.commitTransaction();
		return client;
	}

	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	public void deleteClient(@WebParam(name = "client") Client client) throws BackendException, IllegalRequestException {
		if (!isClientAdministrationAllowed())
			throw new SecurityException("Coundn't delete client data. Permission denied.");

		this.beginTransaction();
		Client foundClient = null;
		int i = 0;
		for (; i < this.getEntityManagers().length && foundClient == null; i++) {
			foundClient = getEntityManagers()[i].find(Client.class, client.getId());
			// erase foundClient if only id is correct
			if (foundClient != null && !foundClient.getName().equals(client.getName()))
				foundClient = null;
		}
		if (foundClient != null)
			getEntityManager().remove(foundClient);
		this.commitTransaction();
	}

	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	@WebResult(name = "clients")
	public ClientQueryResult getClients(@WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, IllegalRequestException {
		if (!isClientAdministrationAllowed())
			throw new SecurityException("Coundn't get client data. Permission denied.");
		String qlString = "SELECT client FROM Client client ";
		Map<String, Object> parameterMap = null;
		String defaultSortOrder = " client.displayName ASC ";
		
		int offSet = 0;
		int limit = Integer.MAX_VALUE;
		
		if (queryDescriptor != null){
			if(queryDescriptor.hasSearchProperties()) {
				parameterMap = new HashMap<String, Object>();
			}
			limit = queryDescriptor.getLimit();
			offSet = queryDescriptor.getOffSet();
			queryDescriptor.setLimit(Integer.MAX_VALUE);
			queryDescriptor.setOffSet(0);
		}
		List<Class<?>> allowedOrderBeans = new LinkedList<Class<?>>();
		allowedOrderBeans.add(Client.class);
		
		QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap, queryDescriptor, defaultSortOrder, allowedOrderBeans);
			
		ClientQueryResult queryResult = new ClientQueryResult();
		Set<Client> clients = new HashSet<Client>();
		/* In order to be able to use this method somewhere else - save the current entity manager */
		EntityManager currentEntityManger = this.getEntityManager();
		/* query all clients from all persistence units */
		for (int i = 0; i < this.getEntityManagers().length; i++) {
//			Query query = getEntityManagers()[i].createQuery(qlString);
//			clients.addAll(this.getResultList(query));
			this.setEntityManager(this.getEntityManagers()[i]);
			clients.addAll(executeSimpleQuery(qlString, queryConfiguration, queryResult).getResultList());
		}
		this.setEntityManager(currentEntityManger);
		if(queryDescriptor != null){
			queryDescriptor.setLimit(limit);
			queryDescriptor.setOffSet(offSet);
		}
		queryResult.clear();
		queryResult.setTotalSize(0);
		queryResult.addAll(clients);
		queryResult.setQueryDescriptor(queryDescriptor);
		/* process order and paging requests */
		queryResult.adaptResultListOrderAndPaging();
		return queryResult;
	}

	@WebMethod
	@WebResult(name = "client")
	public Client getClientByName(@WebParam(name = "clientName") String clientName) throws BackendException, IllegalRequestException {
		if (!isClientAdministrationAllowed())
			throw new SecurityException("Coundn't get client data. Permission denied.");
		if (clientName == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		// String qlString =
		// "SELECT client FROM Client client WHERE client.name = :clientName";
		// Query query = this.getEntityManager().createQuery(qlString);
		// query.setParameter("clientName", clientName);
		// List<Client> resultList = this.getResultList(query);
		// if (resultList.size() == 1)
		// return resultList.get(0);
		// if (resultList.size() == 0)
		// return null;
		// TODO check code, quick release workaround jneuma
		for (Client client : getClients(null).getResultList()) {
			if (clientName.equals(client.getName()))
				return client;
		}
		throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
	}

	protected boolean isClientAdministrationAllowed() {
		return true;
		// return
		// isSuperAdministrationAllowed(sessionContext.getCallerPrincipal());
	}

	@Override
	@WebMethod
	@WebResult(name = "clientPropertyMap")
	public ClientPropertyMap setClientPropertyMap(@WebParam(name = "clientName") String clientName, @WebParam(name = "clientPropertyMap") ClientPropertyMap clientPropertyMap) throws BackendException, IllegalRequestException, AccessDeniedException {
		Client client = this.getClientByName(clientName);
		clientPropertyMap.setClient(client);
		if (clientPropertyMap.getId() == null)
			this.persist(clientPropertyMap);
		else {
			this.syncDeletedMapEntries(clientPropertyMap);
			clientPropertyMap = this.merge(clientPropertyMap);
		}
		log.info("Reinitializing ClientScheduler tasks because a ClientPropertyMap has been changed ...");
		if (!clientName.startsWith("default-client"))
			this.clientScheduler.initializeTimerservice();
		return clientPropertyMap;
	}

	@Override
	@WebMethod
	@WebResult(name = "clientPropertyMap")
	public ClientPropertyMap getClientPropertyMap(@WebParam(name = "clientName") String clientName) throws BackendException, IllegalRequestException, AccessDeniedException {
		// make sure the ClientScheduler will be initialized
		if (!clientName.startsWith("default-client"))
			this.clientScheduler.initialize();

		Client client = this.getClientByName(clientName);
		// set default client property map if none present
		if (client.getClientPropertyMap() == null) {
			return setClientPropertyMap(clientName, ClientPropertyMap.createDefaultClientProperties());
		} else if (ClientPropertyMap.fillMissingDefaults(client.getClientPropertyMap())) {
			return setClientPropertyMap(clientName, client.getClientPropertyMap());
		}
		return client.getClientPropertyMap();
	}

	@WebMethod(exclude = true)
	public void updateLastChangeUserList(@WebParam(name = "clientName") String clientName) throws BackendException {
		if (clientName.startsWith("default-client"))
			return;
		String qlString = "SELECT cd FROM CurrentDate cd";
		Query query = getEntityManager().createQuery(qlString);
		CurrentDate currentDate = (CurrentDate) getSingleResult(query);
		ClientPropertyMap clientPropertyMap = this.getClientPropertyMap(clientName);
		clientPropertyMap.put(ClientPropertyMap.KEY_INTERNAL_LASTUPDATEUSERS, String.valueOf(currentDate.getTimestamp().getTimeInMillis()));
		
		Client client = this.getClientByName(clientName);
		clientPropertyMap.setClient(client);
		if (clientPropertyMap.getId() == null)
			this.persist(clientPropertyMap);
		else {
			this.syncDeletedMapEntries(clientPropertyMap);
			clientPropertyMap = this.merge(clientPropertyMap);
		}
	}

	@WebMethod
	@WebResult(name = "lastChangeMillis")
	public Long getLastUserListChangeTimeMillis(@WebParam(name = "clientName") String clientName) throws BackendException, IllegalRequestException {
		ClientPropertyMap clientPropertyMap = this.getClientPropertyMap(clientName);
		String lastUpdate = clientPropertyMap.get(ClientPropertyMap.KEY_INTERNAL_LASTUPDATEUSERS);
		if (lastUpdate != null) {
			return Long.valueOf(lastUpdate);
		} else {
			return 0l;
		}
	}

	private ClientPropertyMap syncDeletedMapEntries(ClientPropertyMap clientPropertyMap) throws BackendException {
		ClientPropertyMap persistentMap = this.find(ClientPropertyMap.class, clientPropertyMap.getId());
		List<ClientPropertyMapEntry> entriesToDelete = this.getMissingElements(persistentMap.getMapEntries(), clientPropertyMap.getMapEntries());
		for (ClientPropertyMapEntry entryToDelete : entriesToDelete) {
			this.remove(entryToDelete);
		}
		return clientPropertyMap;
	}
}
