package org.evolvis.idm.common.service;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.AbstractBean;

public interface QueryExecutionHelper {

	public abstract <T extends AbstractBean> void persist(EntityManager em, T entityBean) throws BackendException, IllegalRequestException;

	public abstract <T extends AbstractBean> T merge(EntityManager em, T entityBean) throws BackendException, IllegalRequestException;
	
	public <T extends AbstractBean> T mergeInNewTransAction(EntityManager em, T entityBean) throws BackendException, IllegalRequestException;
	
	public abstract <T extends AbstractBean> void remove(EntityManager em, T entityBean) throws BackendException, IllegalRequestException;

	public abstract List<? extends AbstractBean> executeQuery(EntityManager entityManager, String qlString, Map<String, Object> parameterMap);

	public abstract <T extends AbstractBean> T find(EntityManager entityManager, Class<T> entityType, long primaryKey) throws BackendException, IllegalRequestException;
	
	public Object getSingleResult(Query query) throws IllegalRequestException, BackendException;
	
	@SuppressWarnings("unchecked")
	public List getResultList(Query query);
	
	public void testNoTransactionPresent();
	public void testTransactionPresent();
	public void initTransaction();
	
}