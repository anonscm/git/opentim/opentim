package org.evolvis.idm.common.service;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.interceptor.PersistenceExceptionTranslatorInterceptor;
import org.evolvis.idm.common.model.AbstractBean;

@Stateless
@Local(QueryExecutionHelper.class)
@Interceptors(PersistenceExceptionTranslatorInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class QueryExecutionHelperImpl implements QueryExecutionHelper {
	
	/* (non-Javadoc)
	 * @see org.evolvis.idm.common.service.QueryExecutionHelper#persist(javax.persistence.EntityManager, T)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public <T extends AbstractBean> void persist(EntityManager em, T entityBean) throws BackendException, IllegalRequestException {
		em.persist(entityBean);
	}
	
	/* (non-Javadoc)
	 * @see org.evolvis.idm.common.service.QueryExecutionHelper#merge(T)
	 */
	public <T extends AbstractBean> T merge(EntityManager em, T entityBean) throws BackendException, IllegalRequestException {
		return em.merge(entityBean);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public <T extends AbstractBean> T mergeInNewTransAction(EntityManager em, T entityBean) throws BackendException, IllegalRequestException {
		return this.merge(em, entityBean);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends AbstractBean> void remove(EntityManager em, T entityBean) throws BackendException, IllegalRequestException {
		em.clear();
//		em.flush();
		entityBean = (T) em.find(entityBean.getClass(), entityBean.getId());
		em.remove(entityBean);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<? extends AbstractBean> executeQuery(EntityManager entityManager, String qlString, Map<String, Object> parameterMap) {
		Query query = entityManager.createQuery(qlString);
		for(Map.Entry<String, Object> entry : parameterMap.entrySet()){
			query.setParameter(entry.getKey(), entry.getValue());
		}
		List<? extends AbstractBean> returnList = query.getResultList();
		return returnList;
	}

	@Override
	public <T extends AbstractBean> T find(EntityManager entityManager, Class<T> entityType, long primaryKey) throws BackendException, IllegalRequestException {
		return entityManager.find(entityType, primaryKey);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List getResultList(Query query) {
		return query.getResultList();
	}

	@Override
	public Object getSingleResult(Query query) throws IllegalRequestException, BackendException {
		return query.getSingleResult();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void initTransaction() {
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public void testNoTransactionPresent() {
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void testTransactionPresent() {
	}
}
