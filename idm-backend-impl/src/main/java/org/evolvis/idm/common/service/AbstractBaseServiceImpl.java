package org.evolvis.idm.common.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalProperty;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.interceptor.MultipleEntityManagersInterceptor;
import org.evolvis.idm.common.interceptor.PersistenceExceptionTranslatorInterceptor;
import org.evolvis.idm.common.model.AbstractBean;
import org.evolvis.idm.common.model.OrderProperty;
import org.evolvis.idm.common.model.QueryConfiguration;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.model.SearchProperty;
import org.evolvis.idm.common.model.ValueType;
import org.evolvis.idm.identity.account.model.AccountPropertyFields;
import org.evolvis.idm.relation.multitenancy.model.Client;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "AbstractManagementService", name = "AbstractManagementServicePT", portName = "AbstractManagementServicePort")
@Interceptors(value = { MultipleEntityManagersInterceptor.class, PersistenceExceptionTranslatorInterceptor.class })
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public abstract class AbstractBaseServiceImpl implements AbstractBaseService, AbstractJPAService {
	protected final Log log = LogFactory.getLog(getClass());

	protected EntityManager currentEntityManager = null;

	@PersistenceContext(unitName = "idm-backend-live-0")
	protected EntityManager entityManager1;

	@PersistenceContext(unitName = "idm-backend-live-1")
	protected EntityManager entityManager2;

	protected EntityManager[] entityManagers;

	@Resource
	protected SessionContext sessionContext;

	protected boolean isContainerManagedTransactionDisabled;

	private QueryExecutionHelper queryExecutionHelper;

	@EJB(name = "ejb/QueryExecutionHelper/local")
	public void setQueryExecutionHelper(QueryExecutionHelper queryExecutionHelper) {
		this.queryExecutionHelper = queryExecutionHelper;
	}

	public QueryExecutionHelper getQueryExecutionHelper() {
		return queryExecutionHelper;
	}

	protected EntityManager[] getEntityManagers() {
		if (entityManagers == null) {
			List<EntityManager> tempListEntityManagers = new ArrayList<EntityManager>();
			try {
				if (entityManager1 != null)
					tempListEntityManagers.add(entityManager1);
				if (entityManager2 != null)
					tempListEntityManagers.add(entityManager2);
				entityManagers = tempListEntityManagers.toArray(new EntityManager[0]);
				log.debug("getEntityManagers: unit 0 and 1 added");
			} catch (Exception e) {
				log.debug("getEntityManagers: exeption interrupt : " + e.getMessage());
				entityManagers = (EntityManager[]) tempListEntityManagers.toArray(new EntityManager[0]);
			}
		}

		return entityManagers;
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	protected Client getClientByQuery(String qlString, EntityManager entityManager, String valueName, String value) {
		Query query = entityManager.createQuery(qlString);
		query.setParameter(valueName, value);
		List<Client> clients = this.getResultList(query);
		if (clients.size() == 0)
			return null;
		else
			return clients.get(0);
	}

	@WebMethod(exclude = true)
	public EntityManager getEntityManager() {
		if (this.currentEntityManager != null)
			return currentEntityManager;
		else
			return entityManager1;
		// return getEntityManagerInterceptorMagic(0);
	}

	@WebMethod(exclude = true)
	public void setEntityManager(EntityManager[] entityManagers) {
		this.entityManager1 = entityManagers[0];
		this.entityManager2 = entityManagers[1];

	}

	@WebMethod(exclude = true)
	public void setEntityManager(EntityManager entityManager) {
		this.currentEntityManager = entityManager;
	}

	@WebMethod(exclude = true)
	public SessionContext getSessionContext() {
		return sessionContext;
	}

	@WebMethod(exclude = true)
	public void setSessionContext(SessionContext sessionContext) {
		this.sessionContext = sessionContext;
	}

	@WebMethod(exclude = true)
	public boolean isContainerManagedTransactionDisabled() {
		return isContainerManagedTransactionDisabled;
	}

	@WebMethod(exclude = true)
	public void setContainerManagedTransactionDisabled(boolean isContainerManagedTransactionDisabled) {
		this.isContainerManagedTransactionDisabled = isContainerManagedTransactionDisabled;
	}

	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	@WebResult(name = "servertime")
	public Long ping(@WebParam(name = "clienttime") Long clienttime) {
		if (clienttime == null)
			return null;
		Long servertime = System.currentTimeMillis();
		if (log.isDebugEnabled())
			log.debug("Request handled after: " + (servertime - clienttime));
		return servertime;
	}

	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	public void fault(@WebParam(name = "className") String className) {
		if (className == null)
			return;
		try {
			if (log.isDebugEnabled())
				log.debug(Class.forName(className).newInstance());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	@WebResult(name = "version")
	public String getVersion() {
		return "1.1.0"; // TODO add version information for each webservice
	}

	@WebMethod(exclude = true)
	public boolean isSuperAdministrationAllowed(Principal callerPrincipal) {
		// TODO catch NullPointerExceptions
		KeyStore keystore = null;
		try {
			keystore = KeyStore.getInstance(System.getProperty("org.jboss.ws.wsse.trustStoreType"));
			keystore.load(new FileInputStream(System.getProperty("org.jboss.ws.wsse.trustStore")), System.getProperty("org.jboss.ws.wsse.trustStorePassword").toCharArray());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String encodedCallerCertificate = "";
		try {
			encodedCallerCertificate = new String(Base64.encodeBase64(keystore.getCertificate("mykey").getPublicKey().getEncoded()));
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return encodedCallerCertificate.equals(callerPrincipal.toString());
	}

	// @WebMethod(exclude = true)
	// @SuppressWarnings("unchecked")
	// public Client getCurrentClient(EntityManager entityManager, Principal callerPrincipal) {
	// // TODO replace this with code that respect WS-Security
	// Query query = getEntityManager().createQuery("SELECT c FROM Client c ORDER BY c.id ASC");
	//
	// List<Client> result = this.getResultList(query);
	// if (!result.isEmpty())
	// return result.get(0);
	//
	// // if (PersistenceUtil.isLive())
	// // throw BackendException.NO_CLIENT;
	//
	// Client client = new Client("default-client", "default-client");
	// if (isContainerManagedTransactionDisabled())
	// getEntityManager().getTransaction().begin();
	// getEntityManager().persist(client);
	// if (isContainerManagedTransactionDisabled())
	// getEntityManager().getTransaction().commit();
	// return client;
	// // TODO Bisherige Implementierung ausgekommentiert zu Testzwecken!!!
	// /*
	// * Query query =getEntityManager().createQuery(
	// * "SELECT client FROM Client client WHERE client.certificationData = :certificationData"
	// * ); query.setParameter("certificationData",
	// * callerPrincipal.toString()); Client client = (Client)
	// * this.getSingleResult(query);
	// *
	// * if (client != null) return client;
	// */
	//
	// /*
	// * query =getEntityManager().createQuery(
	// * "SELECT application FROM Application application WHERE application.certificationData = :certificationData"
	// * ); query.setParameter("certificationData",
	// * callerPrincipal.toString()); Application application = (Application)
	// * this.getSingleResult(query); return application.getClient();
	// */
	// // return null;
	// }

	@WebMethod(exclude = true)
	public void beginTransaction() {
		if (isContainerManagedTransactionDisabled())
			this.getEntityManager().getTransaction().begin();
	}

	@WebMethod(exclude = true)
	public void commitTransaction() {
		if (isContainerManagedTransactionDisabled())
			this.getEntityManager().getTransaction().commit();
	}

	@WebMethod(exclude = true)
	public void rollbackTransaction() {
		if (isContainerManagedTransactionDisabled())
			this.getEntityManager().getTransaction().rollback();
	}

	/**
	 * Returns a query may have some dynamic properties like ordering, paging, or further search criteria information.
	 * 
	 * @param qlString the string that represents the query.
	 * @param queryConfiguration instance that contains information to configure the query.
	 * @return a query instance.
	 * @throws BackendException
	 */
	protected Query createQuery(String qlString, QueryConfiguration queryConfiguration) throws BackendException {
		if (qlString == null)
			throw new BackendException("Unable to create query: empty query string");
		if (queryConfiguration != null) {
			if (queryConfiguration.getQueryDescriptor() != null) {
				/*
				 * if there are any search or filter properties add these to the original query
				 */
				if (queryConfiguration.getQueryDescriptor().hasSearchProperties()) {
					// TODO validation of the search properties!!!!!!
					/*
					 * for(SearchProperty searchProperty : queryConfiguration.getQueryDescriptor ().getSearchProperties()){ if(!queryConfiguration.getAllowedSearchBeans ().contains(searchProperty)) throw new
					 * IllegalRequestException("Invalid search entity: " + searchProperty.getEntityType().getCanonicalName()); }
					 */
					String[] splittedQlString = qlString.split("WHERE (?!EXISTS)");
					if (splittedQlString.length > 2)
						throw new BackendException("Unable to create query: splitting failed");
					qlString = splittedQlString[0] + " WHERE (";
					/* add search properties */
					int j = 0;

					// TODO remove evil AccountPropertyFields.FIELD_PATH_SOFTDELETE workaround
					// TODO yneuma nicht aufregen
					// TODO jneuma bestrafen!
					SearchProperty evilWorkaround = null;

					boolean foundLongValue = false;

					for (SearchProperty searchProperty : queryConfiguration.getQueryDescriptor().getSearchProperties()) {
						if (searchProperty.getFieldPath().equals(AccountPropertyFields.FIELD_PATH_SOFTDELETE)) {
							evilWorkaround = searchProperty;
						} else {
							String inputParam = "inputParam" + j;

							if (j > 0) {
								if (searchProperty.getValueType().equals(ValueType.LONG) && !foundLongValue) {
									qlString += " ) AND ( ";
								} else {
									qlString += " OR ";
								}
							}

							j++;

							if (searchProperty.getValueType().equals(ValueType.LONG)) {
								foundLongValue = true;
								qlString += searchProperty.getFullPropertyFieldPath(null) + " = :" + inputParam;
								queryConfiguration.getParameterMap().put(inputParam, Long.valueOf(searchProperty.getSearchValue()));
							} else {
								qlString += "LOWER(" + searchProperty.getFullPropertyFieldPath(null) + ") LIKE :" + inputParam + " ESCAPE '#'";
								String rawParameter = searchProperty.getSearchValue().toLowerCase();
							
								String parameter = this.convertStringToSQL(rawParameter);
								
								parameter = "%" + parameter + "%";
								log.debug("createQuery: added search param: "+parameter);
								queryConfiguration.getParameterMap().put(inputParam, parameter);
							}
						}
					}
					if (evilWorkaround != null) {
						if(j > 0){
							qlString +=") AND (";
						}
						qlString += " "+evilWorkaround.getFullPropertyFieldPath(null) + " = :" + "evilProperty ";
						queryConfiguration.getParameterMap().put("evilProperty", Boolean.valueOf(evilWorkaround.getSearchValue()));
					}

					/*
					 * if there were already any conditions in the original query add these to the new
					 */
					qlString += ") ";
					if (splittedQlString.length == 2) {
						qlString += " AND " + splittedQlString[1];
					}
				}
				// if available validate and add ordering information to the
				// query string
				if (queryConfiguration.getQueryDescriptor().isQueryToOrder() && queryConfiguration.getAllowedOrderBeans() != null) {

					for (OrderProperty orderProperty : queryConfiguration.getQueryDescriptor().getOrderProperties()) {
						if (!queryConfiguration.getAllowedOrderBeans().contains(orderProperty.getEntityType()))
							throw new IllegalRequestException("Invalid order entity: " + orderProperty.getEntityType().getCanonicalName());
						if (!orderProperty.isValid())
							throw new IllegalRequestException("Invalid order property: " + orderProperty.getFieldPath());
					}
					String orderString = queryConfiguration.getQueryDescriptor().createOrderString();

					qlString += orderString;
				}
			}
		}

		if (queryConfiguration == null || !QueryDescriptor.isQueryToOrder(queryConfiguration.getQueryDescriptor())) {
			// TODO case: what if there are no allowed beans that are to be used
			// for ordering?

			/*
			 * if no special order requests are given use default order if existent.
			 */
			if (queryConfiguration.getDefaultSortOrder() != null && !(queryConfiguration.getDefaultSortOrder().equals(""))) {

				qlString += " ORDER BY " + queryConfiguration.getDefaultSortOrder();
			}
		}
		/* Now create the query */
		log.debug("createQuery: beginn to create query for query string: " + qlString + "...");

		Query query = this.getEntityManager().createQuery(qlString);
		log.debug("createQuery: creation successfull");
		if (queryConfiguration != null) {
			if (QueryDescriptor.isPaging(queryConfiguration.getQueryDescriptor())) {
				// set paging information to the query
				if (queryConfiguration.getQueryDescriptor().getLimit() < Integer.MAX_VALUE) {
					query.setMaxResults(queryConfiguration.getQueryDescriptor().getLimit());
				}
				query.setFirstResult(queryConfiguration.getQueryDescriptor().getOffSet());
			}

			/*
			 * if parameter are given by the configuration instance add these to the query
			 */
			if (queryConfiguration.getParameterMap() != null) {
				for (Map.Entry<String, Object> mapEntry : queryConfiguration.getParameterMap().entrySet()) {
					query.setParameter(mapEntry.getKey(), mapEntry.getValue());
				}
			}
		}

		return query;
	}

	protected String convertStringToSQL(String string){
/*		string = string.replaceAll("\\", "\\\\");
		string = string.replaceAll("%", "\\%");
		string = string.replaceAll("_","\\_");
		return string;*/
		Set<Character> escapeSymbols = new HashSet<Character>();
		escapeSymbols.add('_');
		//escapeSymbols.add('\\');
		escapeSymbols.add('%');
		escapeSymbols.add('#');
		StringBuilder newString = new StringBuilder(string.length() + 10);
		//TODO improve this (StringBuffer
		for (char c : string.toCharArray()) {
			if (escapeSymbols.contains(c)) {
				newString.append("#");
			}
			newString.append(c);
		}
		log.debug("Converted '"+string+"' to '"+newString+"'");
		return newString.toString();

	}
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	protected <T extends QueryResult> T executeSimpleQueryIsolated(String qlString, QueryConfiguration queryConfiguration, T queryResult) throws BackendException, IllegalRequestException {
		return this.executeSimpleQuery(qlString, queryConfiguration, queryResult);
	}
	
	
	@SuppressWarnings("unchecked")
	protected <T extends QueryResult> T executeSimpleQuery(String qlString, QueryConfiguration queryConfiguration, T queryResult) throws BackendException, IllegalRequestException {
		if (queryConfiguration != null) {
			// assign queryDescriptor to the query result
			queryResult.setQueryDescriptor(queryConfiguration.getQueryDescriptor());
			if (QueryDescriptor.isPaging(queryConfiguration.getQueryDescriptor())) {
				/*
				 * paging is set so calculate the total size of the query without paging and put the result to the query result
				 */
				queryResult.setTotalSize(this.getTotalSizeOfSimpleQuery(qlString, queryConfiguration));

				/* if the offset exceeds the total size - correct the offset to obtain the last page */
				if ((queryResult.getTotalSize() - 1) < queryConfiguration.getQueryDescriptor().getOffSet()) {
					queryConfiguration.setQueryDescriptor(queryResult.getDescriptorForPage(queryResult.getPageAmount()));
				}
			}
		}
		log.debug("executeSimpleQuery: begin to create query ...");
		Query query = this.createQuery(qlString, queryConfiguration);
		queryResult.setResultList(this.getResultList(query));
		return queryResult;
	}

	protected IllegalRequestException getDuplicatePropertyException(Class<? extends AbstractBean> beanClass, String duplicateProperty) {
		IllegalProperty illegalProperty = new IllegalProperty();
		illegalProperty.setBeanName(beanClass.getName());
		illegalProperty.setBeanId(null);
		illegalProperty.setPropertyName(duplicateProperty);
		illegalProperty.setConstraintType("Duplicate");
		// TODO fix xs:string -> prefix xs is not bound to a namespace
		// illegalProperty.setInvalidValue(client.getName());
		return new IllegalRequestException(Collections.singletonList(illegalProperty));
	}

	protected <T extends AbstractBean> List<T> getMissingElements(List<T> persistentElements, List<T> incomingElements) throws BackendException {
		List<T> resultList = new LinkedList<T>();
		if (incomingElements == null || incomingElements.size() == 0)
			return persistentElements;
		T incomingEntity = null;
		int incomingElementIndex = -1;
		for (T persistentEntity : persistentElements) {
			incomingElementIndex = incomingElements.indexOf(persistentEntity);
			if (incomingElementIndex > -1) {
				incomingEntity = incomingElements.get(incomingElementIndex);
				if (incomingEntity.getId() == null) {
					incomingEntity.setId(persistentEntity.getId());
				}
			} else {
				resultList.add(persistentEntity);
			}
		}
		return resultList;
	}

	protected void validateNotNullParameter(Map<String, Object> parameter) throws IllegalRequestException {
		List<IllegalProperty> illegalProperties = null;
		for (Entry<String, Object> entry : parameter.entrySet()) {
			if (entry.getValue() == null) {
				if (illegalProperties == null) {
					illegalProperties = new LinkedList<IllegalProperty>();
				}
				IllegalProperty illegalProperty = new IllegalProperty();
				illegalProperty.setPropertyName(entry.getKey());
				illegalProperty.setConstraintValue("NOT NULL");
				illegalProperty.setInvalidValue(null);
				illegalProperties.add(illegalProperty);
			}
		}
		if (illegalProperties != null) {
			IllegalRequestException exception = IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
			exception.setIllegalProperties(illegalProperties);
			throw exception;
		}
	}

	protected Integer getTotalSizeOfSimpleQuery(String qlString, QueryConfiguration queryConfiguration) throws BackendException {
		String[] splittedQlString = qlString.split("\\ ");
		if (splittedQlString.length > 3) {
			String newQlString = null;
			int i = 2;
			/* examine the query */
			if (splittedQlString[0].equals("SELECT") && splittedQlString[2].equals("FROM")) {
				/* the query is simple build like : "SELECT x FROM X x" */
				newQlString = "SELECT count(" + splittedQlString[1];
			}

			else {
				/*
				 * this case handles if the query uses the "new" statement. Then it should be something like: "SELECT new X(u,s,v) FROM X x
				 */
				if (splittedQlString[0].equals("SELECT") && splittedQlString[1].startsWith("new")) {
					newQlString = "SELECT count( ";
					i = 2;
					while (i < splittedQlString.length && !splittedQlString[i].equals("FROM")) {
						i++;
					}
					if (i == splittedQlString.length || i + 2 >= splittedQlString.length) {
						log.error("Query is not processable for method getTotalSizeOfSimpleQuery: " + qlString);
						throw new BackendException("Not a valid query");
					}
					newQlString += splittedQlString[i + 2];
				}

				if (splittedQlString[0].equals("SELECT") && splittedQlString[1].toUpperCase().equals("DISTINCT")) {
					newQlString = "SELECT  count( DISTINCT " + splittedQlString[2];
					// TODO verfeinern genauer FROM nicht an fester position annehmen.
					i = 3;
				}

			}
			/* Now the select clause should be ready to be closed */
			newQlString += ") FROM ";
			i++;
			/* add the rest of the query */
			for (int j = i; j < splittedQlString.length; j++) {
				newQlString += " " + splittedQlString[j];
			}
			// TODO UNSCHOEN!
			QueryDescriptor saveQueryDescriptor = queryConfiguration.getQueryDescriptor();
			String defaultSortOrder = queryConfiguration.getDefaultSortOrder();
			queryConfiguration.setDefaultSortOrder(null);
			QueryDescriptor tempQueryDescriptor = new QueryDescriptor();
			tempQueryDescriptor.setSearchProperties(saveQueryDescriptor.getSearchProperties());
			queryConfiguration.setQueryDescriptor(tempQueryDescriptor);
			// TODO search parameter
			/*
			 * save the paging information of the query descriptor and reset them temporarily
			 */
			log.debug("getTotalSizeOf-ResultingQuery: " + newQlString);
			/* create count query */
			Query query = this.createQuery(newQlString, queryConfiguration);
			/* extract total query size */
			Number totalSize = (Number) this.getSingleResult(query);

			/* restore paging information in the queryDescriptor */
			// TODO UNSCHOEN ?!
			queryConfiguration.setQueryDescriptor(saveQueryDescriptor);
			queryConfiguration.setDefaultSortOrder(defaultSortOrder);
			return totalSize.intValue();

		}

		// remove this:
		return new Integer(0);
	}

	// TODO javadoc
	public <T extends AbstractBean> void persist(T entityBean) throws IllegalRequestException, BackendException {
		getQueryExecutionHelper().persist(getEntityManager(), entityBean);
	}

	// TODO javadoc

	public <T extends AbstractBean> T merge(T entityBean) throws IllegalRequestException, BackendException {
		return getQueryExecutionHelper().merge(getEntityManager(), entityBean);
	}

	public <T extends AbstractBean> T mergeInNewTransAction(T entityBean) throws IllegalRequestException, BackendException {
		return getQueryExecutionHelper().mergeInNewTransAction(this.getEntityManager(), entityBean);
	}

	// TODO javadoc
	public <T extends AbstractBean> void remove(T entityBean) throws IllegalRequestException, BackendException {
		getQueryExecutionHelper().remove(getEntityManager(), entityBean);
	}

	@SuppressWarnings("unchecked")
	public <T extends AbstractBean> List<T> executeQuery(Class<T> entityType, String qlString, Map<String, Object> parameterMap) {
		return (List<T>) getQueryExecutionHelper().executeQuery(getEntityManager(), qlString, parameterMap);
	}

	public <T extends AbstractBean> T find(Class<T> entityType, long primaryKey) throws IllegalRequestException, BackendException {
		return getQueryExecutionHelper().find(getEntityManager(), entityType, primaryKey);
	}

	public Object getSingleResult(Query query) throws IllegalRequestException, BackendException {
		return getQueryExecutionHelper().getSingleResult(query);
	}

	@SuppressWarnings("unchecked")
	public List getResultList(Query query) {
		return getQueryExecutionHelper().getResultList(query);
	}
}
