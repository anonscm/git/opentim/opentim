/**
 * 
 */
package org.evolvis.idm.common.util;

import java.security.Principal;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;
import org.jboss.security.CertificatePrincipal;
import org.jboss.security.SimplePrincipal;

/**
 * @author jneuma
 */
public class SubjectCertificateMapper implements CertificatePrincipal {

	/* (non-Javadoc)
	 * @see org.jboss.security.CertificatePrincipal#toPrinicipal(java.security.cert.X509Certificate[])
	 */
	public Principal toPrinicipal(X509Certificate[] certs) {
		final String encodedPublicKey = new String(Base64.encodeBase64(certs[0].getPublicKey().getEncoded()));
		return new SimplePrincipal(encodedPublicKey);
	}
}
