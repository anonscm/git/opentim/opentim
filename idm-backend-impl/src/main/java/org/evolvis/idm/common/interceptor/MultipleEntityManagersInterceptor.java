package org.evolvis.idm.common.interceptor;

import java.lang.annotation.Annotation;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.model.AbstractBean;
import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;

public class MultipleEntityManagersInterceptor extends AbstractBaseServiceImpl {

	public static final String CONTEXT_KEY_ENTITYMANAGER_NUMBER = "entityManagerNumber";

	@AroundInvoke
	public Object doMagic(InvocationContext context) throws Exception {
		String methodName = context.getMethod().getName();
		log.trace("Invoking method: " + methodName);

		// if (methodName.equals("getEntityManagerInterceptorMagic")) {
		// Integer entityManagerNumber = (Integer)
		// context.getContextData().get(CONTEXT_KEY_ENTITYMANAGER_NUMBER);
		// if (entityManagerNumber != null) {
		// context.setParameters(new Object[] { entityManagerNumber });
		// }
		// }
		// if there is only one EntityManager configured always select this
		// EntityManager and do nothing else
		// only check for WebMethods
		if (methodName.equals("getClients") || methodName.equals("ping")
				|| methodName.equals("fault") || methodName.equals("getVersion")) {
			// nothing
		} else if (getEntityManagers().length > 1
				&& context.getMethod().getAnnotation(WebMethod.class) != null) {
			Annotation[][] annotations = context.getMethod().getParameterAnnotations();
			String fieldName = null, fieldValue = null;
			for (int i = 0; i < annotations.length; i++) {
				Annotation[] annotationsForParam = annotations[i];
				for (int j = 0; j < annotationsForParam.length; j++) {
					if (annotationsForParam[j] instanceof WebParam) {
						log.trace("Name for Param #" + i + ": "
								+ ((WebParam) annotationsForParam[j]).name());
						String webParamName = ((WebParam) annotationsForParam[j]).name();
						if (webParamName.equals("clientName")) {
							fieldName = "clientName";
							fieldValue = (String) context.getParameters()[i];
						} else if (webParamName.equals("uuid")) {
							fieldName = "uuid";
							fieldValue = (String) context.getParameters()[i];
						} else if (methodName.startsWith("set") || methodName.startsWith("delete")
								|| methodName.startsWith("create")) {
							/*
							 * the called method is one to store or to update an
							 * entity so there has to be a client instance
							 * inside the parameter entity
							 */
							log.trace("INTERCEPTOR: setter method call\n");
							try {
								AbstractBean parameterBean = (AbstractBean) context.getParameters()[i];
								if (((AbstractEntity)parameterBean).getClientName() != null) {
									fieldName = "clientName";
									fieldValue = ((AbstractEntity)parameterBean).getClientName();
								}
							} catch (ClassCastException e) {
								// nothing
							} catch (NullPointerException e) {
								// optional parameter may be null
							}
						} /* end of else if (..uuid) */
					}
					if (fieldName != null && fieldValue != null)
						break;
				}
				if (fieldName != null && fieldValue != null)
					break;
			} /* end of for(i..) */
			int correctEntityManagerNumber = 0;

			if (fieldValue == null) {
				throw BackendException.CLIENT_NOT_AVAILABLE_EXCEPTION;
			}

			// find the correct EntityManager
			else {

				// case uuid is given:
				if (fieldName.equals("uuid")) {
					for (int i = 0; i < getEntityManagers().length; i++) {
						String qlString = "SELECT account.client FROM Account account WHERE account.uuid = :uuid1";
						// Query query =
						// this.getEntityManagers()[i].createQuery(
						// "SELECT account.client FROM Account account WHERE account.uuid = :uuid1"
						// );
						// query.setParameter("uuid1", fieldValue);
						log.trace("ENTITYMANAGER#" + i + " : " + getEntityManagers()[i]);
						if (this.getClientByQuery(qlString, this.getEntityManagers()[i], "uuid1",
								fieldValue) != null) {
							correctEntityManagerNumber = i;
							break;
						}
					}
				}
				// case clientName is given:
				else if (fieldName.equals("clientName")) {
					for (int i = 0; i < getEntityManagers().length; i++) {
						String qlString = "SELECT client FROM Client client WHERE client.name = :clientName";
						// Query query =
						// this.getEntityManagers()[i].createQuery(qlString);
						// query.setParameter("clientName", fieldValue);
						log.trace("ENTITYMANAGER#" + i);
						if (this.getClientByQuery(qlString, this.getEntityManagers()[i],
								"clientName", fieldValue) != null) {
							correctEntityManagerNumber = i;
							break;
						}
					}
				}
			}
			
			context.getContextData().put(CONTEXT_KEY_ENTITYMANAGER_NUMBER,
					correctEntityManagerNumber);
			((AbstractBaseServiceImpl) context.getTarget())
					.setEntityManager(getEntityManagers()[correctEntityManagerNumber]);
		}

		return context.proceed();

	}
}
