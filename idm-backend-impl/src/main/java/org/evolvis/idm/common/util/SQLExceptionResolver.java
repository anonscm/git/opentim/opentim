package org.evolvis.idm.common.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.StringUtils;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.AbstractBean;
import org.hibernate.exception.ConstraintViolationException;

public class SQLExceptionResolver {
	
	@SuppressWarnings("unchecked")
	public static BackendException translatePersistenceException(PersistenceException e) {
		if (e.getCause() instanceof ConstraintViolationException) {
			// The only expected constraint violations after validating the entity beans
			// is caused by unique constraint violations. Therefore a duplicate error will be thrown.
			
			// find type of affected entity bean from error message
			Class<? extends AbstractBean> beanType = null;			
			int startIndexIdMEntity = e.getMessage().indexOf("org.evolvis.idm");
			if (startIndexIdMEntity != -1) {
				int endIndexIdMEntity = e.getMessage().indexOf("]", startIndexIdMEntity);
				if (endIndexIdMEntity != -1) {
					String entityBeanClass = e.getMessage().substring(startIndexIdMEntity, endIndexIdMEntity);
					try {
						beanType = (Class<? extends AbstractBean>) Class.forName(entityBeanClass);
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					}
				}
			}
			
			if (beanType != null) {
				// retrieving unique constraints from bean class
				Table tableAnnotation = beanType.getAnnotation(Table.class);
				List<String> uniqueColumns = new ArrayList<String>();
				if (tableAnnotation != null) {
					// search for constraints in table annotation if present
					UniqueConstraint[] uniqueConstraints;
					uniqueConstraints = tableAnnotation.uniqueConstraints();

					for (int i = 0; i < uniqueConstraints.length; i++) {
						uniqueColumns.addAll(Arrays.asList(uniqueConstraints[i].columnNames()));
					}
				}
				else {
					// search for constraints in unique constraint annotation
					UniqueConstraint uniqueConstraint = beanType.getAnnotation(UniqueConstraint.class);
					if (uniqueConstraint != null)
						uniqueColumns.addAll(Arrays.asList(uniqueConstraint.columnNames()));
				}

				String uniqueConstraintsMessage = StringUtils.join(uniqueColumns, ", ");

				// generating detailed exception
				return new IllegalRequestException(IllegalRequestException.INVALID_DUPLICATE_MESSAGE_DETAIL_PREFIX + uniqueConstraintsMessage,
						IllegalRequestException.CODE_INVALID_DUPLICATE);
			}
			else {
				// generating general exception
				String message = "";
				if(e.getCause() instanceof ConstraintViolationException)
					if(e.getCause().getMessage() != null)
						message = e.getCause().getMessage();
				return new IllegalRequestException(IllegalRequestException.INVALID_DUPLICATE_MESSAGE_GENERAL + " " + message,
						IllegalRequestException.CODE_INVALID_DUPLICATE);
			}
		}
		return new BackendException(e.getMessage(), BackendException.CODE_UNKNOWN_ERROR);	
	}

}
