package org.evolvis.idm.common.interceptor;

import javax.ejb.EJBTransactionRolledbackException;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.SQLExceptionResolver;

public class PersistenceExceptionTranslatorInterceptor extends AbstractBaseServiceImpl {

	@AroundInvoke
	public Object translateException(InvocationContext context) throws Exception {
		try {
			log.trace("PERSISTENCEINTERCEPTOR entered..");
			return context.proceed();
		} 
		catch(IllegalRequestException e){
			//TODO nicht schoen
			if(e.getErrorCode() != null && e.getErrorCode().equals("request.invalidParameters")){
				if(e.getIllegalProperties() != null && e.getIllegalProperties().size() == 1 && e.getIllegalProperties().get(0).getConstraintType().equals("AttributeListUniqueNames"))
					throw IllegalRequestException.INVALID_DUPLICATE_EXCEPTION;
			}
			throw e;
		} 
		catch (NoResultException e) {
			log.trace("no result exception");
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		} catch (PersistenceException e) {
			log.trace("PERSISENTCEINTERCEPTOR: persistence exception");
			throw SQLExceptionResolver.translatePersistenceException(e);
		} catch (EJBTransactionRolledbackException e) {
			log.trace("PERSISENTCEINTERCEPTOR: EJBTransactionRollBackException: " + e.getMessage());
			PersistenceException underlyingPersistenceException = (PersistenceException) e
					.getCause().getCause();
			if (underlyingPersistenceException == null)
				throw e;
			throw SQLExceptionResolver.translatePersistenceException((PersistenceException) e.getCause().getCause());
		} catch (Exception e) {
			log.info("An unknown exception has been thrown: " + e.getClass().getCanonicalName(), e);
			if (log.isDebugEnabled())
				e.printStackTrace();

			if (!(e instanceof BackendException)) {
				BackendException returnException = null;
				if (e.getMessage() != null)
					returnException = new BackendException(e.getMessage(),	BackendException.CODE_UNKNOWN_ERROR);
				else {
					returnException = new BackendException("", BackendException.CODE_UNKNOWN_ERROR);
				}
				throw returnException;
			}

			throw e;
		}
	}
}
