package org.evolvis.idm.common.util;
///**
// * 
// */
//package org.evolvis.idm.common.util;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.io.FileWriter;
//import java.security.Key;
//import java.security.KeyPair;
//import java.security.KeyStore;
//import java.security.KeyStoreException;
//import java.security.NoSuchAlgorithmException;
//import java.security.PrivateKey;
//import java.security.PublicKey;
//import java.security.UnrecoverableKeyException;
//import java.security.cert.Certificate;
//import java.security.cert.X509Certificate;
//
//import org.apache.commons.codec.binary.Base64;
//
//import sun.security.x509.CertAndKeyGen;
//import sun.security.x509.X500Name;
//
///**
// * @author jneuma
// * 
// */
//public class KeytoolHelper {
//	private File keystoreFile;
//	private String keyStoreType;
//	private char[] password;
//	private String alias;
//	private File exportedFile;
//	private File truststoreFile;
//
//	public static KeyPair getPrivateKey(KeyStore keystore, String alias, char[] password) {
//		try {
//			Key key = keystore.getKey(alias, password);
//			if (key instanceof PrivateKey) {
//				Certificate cert = keystore.getCertificate(alias);
//				PublicKey publicKey = cert.getPublicKey();
//				return new KeyPair(publicKey, (PrivateKey) key);
//			}
//		} catch (UnrecoverableKeyException e) {
//		} catch (NoSuchAlgorithmException e) {
//		} catch (KeyStoreException e) {
//		}
//		return null;
//	}
//
//	public void export() throws Exception {
//		KeyStore keystore = KeyStore.getInstance(keyStoreType);
//		keystore.load(new FileInputStream(keystoreFile), password);
//		KeyPair keyPair = getPrivateKey(keystore, alias, password);
//		final String encodedPrivateKey = new String(Base64.encodeBase64(keyPair.getPrivate().getEncoded()));
//		FileWriter fw = new FileWriter(exportedFile);
//		fw.write("—–BEGIN PRIVATE KEY—–\n");
//		fw.write(encodedPrivateKey);
//		fw.write("\n");
//		fw.write("—–END PRIVATE KEY—–");
//		fw.close();
//	}
//	
////	public void importBase64() throws Exception {
////		KeyStore keystore = KeyStore.getInstance(keyStoreType);
////		BASE64Encoder encoder = new BASE64Encoder();
////		keystore.load(new FileInputStream(keystoreFile), password);
////		KeyPair keyPair = getPrivateKey(keystore, alias, password);
////		PrivateKey privateKey = keyPair.getPrivate();
////		
////		BASE64Decoder encoder = new BASE64Decoder();
////		keystore.load(new FileInputStream(keystoreFile), password);
////		keystore.
////		KeyPair keyPair = getPrivateKey(keystore, alias, password);
////		PrivateKey privateKey = keyPair.getPrivate();
////		String encoded = encoder.encode(privateKey.getEncoded());
////		FileWriter fw = new FileWriter(exportedFile);
////		fw.write("—–BEGIN PRIVATE KEY—–\n");
////		fw.write(encoded);
////		fw.write("\n");
////		fw.write("—–END PRIVATE KEY—–");
////		fw.close();
////	}
//	
//	
//	
//
//	public static void main(String args[]) throws Exception {
//		KeytoolHelper export = new KeytoolHelper();
//		export.keystoreFile = new File(args[0]);
//		export.keyStoreType = args[1];
//		export.password = args[2].toCharArray();
//		export.alias = args[3];
//		export.exportedFile = new File(args[4]);
//		export.truststoreFile = new File(args[5]);
////		export.export();
//		export.generate("test123");
//		
//	}
//	
//	public void generate(String alias) throws Exception {
//		KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//		if (keystoreFile.exists())
//			keyStore.load(new FileInputStream(keystoreFile), password);
//		else 
//			keyStore.load(null, password);
//		
//		doGenKeyPair(keyStore, password, alias, "CN=IdM-Certificate,OU=Unknown,O=Unknown,L=Unknown,ST=Unknown,C=Unknown", "RSA", 2048, null);
//		keyStore.store(new FileOutputStream(keystoreFile), password);
//		
//		Certificate cert = keyStore.getCertificate(alias);
//		
//		KeyStore trustStore = KeyStore.getInstance(keyStoreType);
//		if (truststoreFile.exists())
//			trustStore.load(new FileInputStream(truststoreFile), password);
//		else
//			trustStore.load(null, password);
//		trustStore.setCertificateEntry(alias, cert);
//		trustStore.store(new FileOutputStream(truststoreFile), password);
//	}
//	
//	/**
//     * Creates a new key pair and self-signed certificate.
//     */
//    private void doGenKeyPair(KeyStore keyStore, char[] keyPass, String alias, String dname, String keyAlgName,
//                              int keysize, String sigAlgName) throws Exception
//    {
//        if (sigAlgName == null) {
//            if ("DSA".equalsIgnoreCase(keyAlgName)) {
//                sigAlgName = "SHA1WithDSA";
//            } else if ("RSA".equalsIgnoreCase(keyAlgName)) {
//                sigAlgName = "SHA1WithRSA";
//            } else if ("EC".equalsIgnoreCase(keyAlgName)) {
//                sigAlgName = "SHA1withECDSA";
//            } else {
//                throw new Exception("Cannot derive signature algorithm");
//            }
//        }
//        CertAndKeyGen keypair = new CertAndKeyGen(keyAlgName, sigAlgName, null);
//
//        X500Name x500Name = new X500Name(dname);
//        keypair.generate(keysize);
//        PrivateKey privKey = keypair.getPrivateKey();
//
//        X509Certificate[] chain = new X509Certificate[1];
//        chain[0] = keypair.getSelfCertificate(x500Name, 50L*365L*24L*60L*60L); // valid 50 years in seconds
//
//        keyStore.setKeyEntry(alias, privKey, keyPass, chain);
//    }
//}
