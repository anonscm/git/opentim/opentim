package org.evolvis.idm.synchronization.service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang.StringUtils;
import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.ReferencedEntityContainer;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.AttributeType;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.synchronization.service.fault.SynchronizationException;
import org.jboss.wsf.spi.annotation.WebContext;

@Stateless
@Local(DirectoryServiceSynchronization.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "DirectoryServiceSynchronizationService", name = "DirectoryServiceSynchronizationServicePT", portName = "DirectoryServiceSynchronizationServicePort")
@WebContext(contextRoot = "idm-backend", urlPattern = "/DirectoryServiceSynchronizationService")
public class DirectoryServiceSynchronizationImpl extends AbstractBaseServiceImpl implements DirectoryServiceSynchronization {

	private ClientManagement clientManagement;

	@EJB(name = "ejb/ClientManagementImpl/local")
	public void setClientManagement(ClientManagement clientManagement) {
		this.clientManagement = clientManagement;
	}

	private AccountManagement accountManagement;

	@EJB(name = "ejb/AccountManagementImpl/local")
	public void setAccountManagement(AccountManagement accountManagement) {
		this.accountManagement = accountManagement;
	}

	private PrivateDataManagement privateDataManagement;

	@EJB(name = "ejb/PrivateDataManagementImpl/local")
	public void setPrivateDataManagement(PrivateDataManagement privateDataManagement) {
		this.privateDataManagement = privateDataManagement;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new DirectoryServiceSynchronizationImpl();
	}

	public DirectoryServiceSynchronizationImpl() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.evolvis.idm.synchronization.service.DirectoryServiceSynchronization
	 * #synchroniseAccount(java.lang.String)
	 */
	@WebMethod
	public List<Value> synchroniseAccount(@WebParam(name = "uuid") String uuid) throws AccessDeniedException, IllegalRequestException, BackendException, SynchronizationException {
		Account account = accountManagement.getAccountByUuid(uuid);
		ClientPropertyMap clientProperties = clientManagement.getClientPropertyMap(account.getClientName());
		if (clientProperties.getBoolean(ClientPropertyMap.KEY_LDAP_SYNC_ENABLED)) {
			log.info("Trying to synchronise private data for uuid=" + uuid + " with LDAP...");
			List<ValueSet> currentValues = privateDataManagement.getValues(uuid);
			List<Value> rejectedValues = new ArrayList<Value>();

			synchroniseValues(clientProperties, account, currentValues, rejectedValues);

			for (ValueSet valueSet : currentValues) {
				privateDataManagement.setValues(uuid, valueSet, true);
				log.debug("Updating private data for uuid=" + uuid + ", setting valueSet=" + valueSet);
			}
			log.info("User with uuid=" + uuid + " has been successfully synchronized");

			return rejectedValues;
		} else {
			log.info("LDAP synchronisation is disabled for clientName=" + account.getClientName() + ". Ignoring synchronisation request for uuid=" + uuid);
			return new ArrayList<Value>();
		}
	}

	protected void synchroniseValues(ClientPropertyMap clientProperties, Account account, List<ValueSet> values, List<Value> rejectedValues) throws AccessDeniedException, IllegalRequestException, BackendException, SynchronizationException {
		String hostAndPort = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_HOSTANDPORT);
		String baseDN = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_BASEDN);
		String userDN = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_USERDN);
		String password = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_PASSWORD);
		String searchDN = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHDN);
		String searchFilter = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHFILTER);

		/*
		 * Validating ldapSync-Properties
		 */
		if (StringUtils.isEmpty(hostAndPort))
			throw new SynchronizationException(SynchronizationException.MESSAGE_LDAP_SYNC_PROPERTY_MISSING_GENERAL + ", property=" + ClientPropertyMap.KEY_LDAP_SYNC_HOSTANDPORT);
		else if (!hostAndPort.matches(".*:.*"))
			throw SynchronizationException.LDAP_SYNC_HOSTPORT_INVALID_EXCEPTION;
		else if (StringUtils.isEmpty(baseDN))
			throw new SynchronizationException(SynchronizationException.MESSAGE_LDAP_SYNC_PROPERTY_MISSING_GENERAL + ", property=" + ClientPropertyMap.KEY_LDAP_SYNC_BASEDN);
		else if (StringUtils.isEmpty(userDN))
			throw new SynchronizationException(SynchronizationException.MESSAGE_LDAP_SYNC_PROPERTY_MISSING_GENERAL + ", property=" + ClientPropertyMap.KEY_LDAP_SYNC_USERDN);
		else if (searchDN == null)
			searchDN = "";
		else if (StringUtils.isEmpty(password))
			throw new SynchronizationException(SynchronizationException.MESSAGE_LDAP_SYNC_PROPERTY_MISSING_GENERAL + ", property=" + ClientPropertyMap.KEY_LDAP_SYNC_PASSWORD);
		else if (StringUtils.isEmpty(searchFilter))
			throw new SynchronizationException(SynchronizationException.MESSAGE_LDAP_SYNC_PROPERTY_MISSING_GENERAL + ", property=" + ClientPropertyMap.KEY_LDAP_SYNC_SEARCHFILTER);
		else if (!searchFilter.matches(".*" + ClientPropertyMap.PLACEHOLDER_LOGINNAME_LDAP_SYNC_SEARCHFILTER + ".*"))
			throw SynchronizationException.LDAP_SYNC_MISSING_LOGIN_PLACEHOLDER_EXCEPTION;

		searchFilter = searchFilter.replaceAll(ClientPropertyMap.PLACEHOLDER_LOGINNAME_LDAP_SYNC_SEARCHFILTER, account.getUsername());

		Hashtable<String, String> hstEnvironment = new Hashtable<String, String>();

		hstEnvironment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

		// Host and Port:
		// windows-ad.qs.tarent.de:389
		// e.g. dc=qs,dc=tarent,dc=de
		hstEnvironment.put(Context.PROVIDER_URL, "ldap://" + hostAndPort + "/" + baseDN);

		hstEnvironment.put(Context.SECURITY_AUTHENTICATION, "simple");
		// Username:
		// e.g. CN=Administrator,CN=Users,dc=qs,dc=tarent,dc=de
		hstEnvironment.put(Context.SECURITY_PRINCIPAL, userDN);
		// Password:
		hstEnvironment.put(Context.SECURITY_CREDENTIALS, password);

		try {
			DirContext dirContext = new InitialDirContext(hstEnvironment);

			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			// SearchDN: e.g. cn=Users
			// SearchFilter: e.g. (&(objectClass=person)(postalCode=555555))
			NamingEnumeration<SearchResult> searchResults = dirContext.search(searchDN, searchFilter, searchControls);

			SearchResult searchResult;
			if (searchResults.hasMore())
				searchResult = searchResults.next();
			else
				return;

			Attributes foundAttributes = searchResult.getAttributes();

			// todo cache client attributes
			List<AttributeGroup> idmAttributes = privateDataManagement.getAttributeGroups(account.getClientName());

			for (AttributeGroup attributeGroup : idmAttributes) {
				synchronizeAttributeGroup(attributeGroup, values, foundAttributes, account, rejectedValues);
			}
		} catch (NamingException ne) {
			// TODO throw BackendException
			log.warn("Leseoperation fehlgeschlagen!");
			ne.printStackTrace();
		}
		return;
	}

	public void synchronizeAttributeGroup(AttributeGroup attributeGroup, List<ValueSet> values, Attributes foundAttributes, Account account, List<Value> rejectedValues) throws NamingException {
		if (!attributeGroup.isMultiple()) {
			ValueSet valueSet = getValueSetByName(values, attributeGroup.getName());
			// instantiate ValueSet if no current ValueSet present
			if (valueSet == null) {
				valueSet = new ValueSet();
				valueSet.setAccount(account);
				valueSet.setAttributeGroup(attributeGroup);
				values.add(valueSet);
			}
			for (Attribute attribute : attributeGroup.getAttributes()) {
				synchronizeAttribute(attribute, valueSet, foundAttributes, account, rejectedValues);
			}
		}
	}

	public void synchronizeAttribute(Attribute attribute, ValueSet valueSet, Attributes foundAttributes, Account account, List<Value> rejectedValues) throws NamingException {
		Value value = valueSet.getValue(attribute);
		if (value != null) {
			// remove lock in case no synchronization happens
			value.setLocked(false);
		}
		if (attribute.getSyncOptions() != null) {
			javax.naming.directory.Attribute foundAttribute = foundAttributes.get(attribute.getSyncOptions());
			if (foundAttribute != null) {
				String syncedAttributeValue = (String) foundAttribute.get();

				// only sync AttributeType.STRING and AttributeType.VALIDATABLE
				// and only allow Strings with length > 0
				if ((attribute.getType().equals(AttributeType.STRING) || attribute.getType().equals(AttributeType.VALIDATABLE)) && syncedAttributeValue != null && syncedAttributeValue.length() > 0) {

					boolean createdNewValue = false;

					if (value == null) {
						value = new Value();
						value.setAttribute(attribute);
					}

					value.setLocked(true);

					// set new value before validation
					// store old value for rollback
					String originalValue = value.getValue();
					value.setValue(syncedAttributeValue);

					// try to validate content of syncedAttributeValue
					try {
						ValidationUtil.validate(value);

						// log changes if necessary
						if (originalValue == null || !originalValue.equals(syncedAttributeValue)) {
							log.info("Updating value for uuid=" + account.getUuid() + ", attributeGroupName=" + attribute.getAttributeGroup().getName() + " and attributeName=" + attribute.getName() + " with value=\"" + syncedAttributeValue + "\"");
						}

					} catch (IllegalRequestException e) {
						// content is invalid, log rejection
						log.info("Rejected invalid value update for uuid=" + account.getUuid() + ", attributeGroupName=" + attribute.getAttributeGroup().getName() + " and attributeName=" + attribute.getName() + " with value=\"" + syncedAttributeValue
								+ "\"");
						// add value copy to reject list
						Value valueCopy = new Value(value);
						rejectedValues.add(valueCopy);
						ReferencedEntityContainer referencedEntityContainer = new ReferencedEntityContainer();
						referencedEntityContainer.addAttributeGroup(attribute.getAttributeGroup());
						valueCopy.setReferencedEntityContainer(referencedEntityContainer);
						// rollback to original value if a value was present
						value.setValue(originalValue);
						// end value synchronization
						return;
					}

					// add value to ValueSet if newly created
					if (createdNewValue)
						valueSet.addValue(value);
				}
			}
		}
	}

	protected ValueSet getValueSetByName(List<ValueSet> values, String name) {
		for (ValueSet valueSet : values)
			if (valueSet.getName().equals(name))
				return valueSet;
		return null;
	}

}
