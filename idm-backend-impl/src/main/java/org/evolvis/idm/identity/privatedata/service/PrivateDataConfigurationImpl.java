package org.evolvis.idm.identity.privatedata.service;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.jboss.wsf.spi.annotation.WebContext;

/**
 * @author Jens Neumaier, tarent GmbH
 * @author Yorka Neumann, tarent GmbH
 */
@Stateless
@Local(PrivateDataConfiguration.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "PrivateDataConfigurationService", name = "PrivateDataConfigurationServicePT", portName = "PrivateDataConfigurationServicePort")
@WebContext(contextRoot = "idm-backend", urlPattern = "/PrivateDataConfigurationService")
// @EndpointConfig(configName = "Standard WSSecurity Endpoint")
public class PrivateDataConfigurationImpl extends AbstractBaseServiceImpl implements PrivateDataConfiguration {

	private ClientManagement clientManagement;
	private PrivateDataManagement privateDataManagement;

	@Override
	@WebMethod
	public void deleteAttribute(@WebParam(name = "clientName") String clientName, @WebParam(name = "attribute") Attribute attribute) throws IllegalRequestException, BackendException {
		if (attribute == null || attribute.getId() == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		this.beginTransaction();
		attribute = this.find(Attribute.class, attribute.getId());
		if (attribute == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		this.executeAttributeDelete(clientName, attribute);
	}

	/**
	 * ${@inheritDoc}
	 * 
	 * @throws IllegalRequestException
	 */
	@WebMethod
	public void deleteAttributeGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeGroup") AttributeGroup attributeGroup) throws BackendException, AccessDeniedException, IllegalRequestException {
		if (!isAttributeAdministrationAllowed())
			throw new AccessDeniedException("Coundn't delete attribute data. Permission denied.");
		if (attributeGroup.isWriteProtected())
			throw IllegalRequestException.LOCKED_ATTRIBUTE_EXCEPTION;

		this.beginTransaction();
		attributeGroup = getEntityManager().find(AttributeGroup.class, attributeGroup.getId());
		if (attributeGroup != null) {
			if (attributeGroup.isWriteProtected())
				throw new BackendException("Unable to delete attribute group: it contains write protected attributes. ");
			getEntityManager().remove(attributeGroup);
			this.commitTransaction();
		} else {
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		}

	}

	/**
	 * This method removes an attribute from persistent context.
	 * 
	 * @param clientName the unique name of the client.
	 * @param attribute the attribute to remove.
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	private void executeAttributeDelete(String clientName, Attribute attribute) throws IllegalRequestException, BackendException {
		if (attribute.isWriteProtected())
			throw IllegalRequestException.LOCKED_ATTRIBUTE_EXCEPTION;
		/*
		 * An attribute can be referenced by several values. These values will be removed by the following before removing the attribute, because the cascading delete (see annotation in Class Attribute) does not work in this case.
		 */
		this.getEntityManager().clear();
		String qlString = "DELETE FROM Value value WHERE value.attribute.id = :attributeId";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("attributeId", attribute.getId());
		query.executeUpdate();
		qlString = "DELETE FROM Attribute attribute WHERE attribute.id = :attributeId";
		query = this.getEntityManager().createQuery(qlString);
		query.setParameter("attributeId", attribute.getId());
		query.executeUpdate();
	}

	@Override
	@WebMethod
	@WebResult(name = "attribute")
	public Attribute getAttributeById(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeId") Long attributeId) throws IllegalRequestException, BackendException {
		return this.privateDataManagement.getAttributeById(clientName, attributeId);
	}

	@Override
	@WebMethod
	@WebResult(name = "attributeGroup")
	public AttributeGroup getAttributeGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeGroupName") String attributeGroupName) throws IllegalRequestException, BackendException {
		return this.privateDataManagement.getAttributeGroup(clientName, attributeGroupName);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "attributeGroup")
	public AttributeGroup getAttributeGroupById(@WebParam(name = "clientName") String clientName, @WebParam(name = "id") Long id) throws BackendException, AccessDeniedException, IllegalRequestException {
		return this.privateDataManagement.getAttributeGroupById(clientName, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "attributeGroups")
	public List<AttributeGroup> getAttributeGroups(@WebParam(name = "clientName") String clientName) throws BackendException, IllegalRequestException {
		return this.privateDataManagement.getAttributeGroups(clientName);
	}

	protected boolean isAttributeAdministrationAllowed() {
		// TODO replace this with code that respect WS-Security
		return true;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private AttributeGroup mergeAttributeGroup(AttributeGroup attributeGroup) throws BackendException, IllegalRequestException {
		/*
		 * check if the name has changed to one that is also the name of another attribute group assigned to the same client which would be a constraint violation
		 */
		AttributeGroup persistentGroup = this.getAttributeGroup(attributeGroup.getClientName(), attributeGroup.getName());
		if (persistentGroup != null && !persistentGroup.getId().equals(attributeGroup.getId()))
			throw IllegalRequestException.INVALID_DUPLICATE_EXCEPTION;
		/*
		 * may be there are attributes removed from the attribute group - persist that change.
		 */
		attributeGroup = this.syncDeletedMapEntries(attributeGroup, persistentGroup);
		if (attributeGroup.getAttributes() == null)
			attributeGroup.setAttributes(new LinkedList<Attribute>());
		/*
		 * update all other changes. 
		 */
		attributeGroup = this.merge(attributeGroup);
		return attributeGroup;
	}

	@Override
	@WebMethod
	@WebResult(name = "attribute")
	public Attribute setAttribute(@WebParam(name = "clientName") String clientName, @WebParam(name = "attribute") Attribute attribute) throws IllegalRequestException, BackendException {
		if (attribute == null || clientName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		Attribute persistentAttribute = this.getAttributeById(clientName, attribute.getId());
		if (persistentAttribute == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		attribute.setAttributeGroup(persistentAttribute.getAttributeGroup());
		attribute.setValues(persistentAttribute.getValues());
		ValidationUtil.validate(attribute);
		this.beginTransaction();
		this.mergeInNewTransAction(attribute);
		this.commitTransaction();
		return attribute;
	}

	/**
	 * ${@inheritDoc}
	 */
	@WebMethod
	@WebResult(name = "attributeGroup")
	@Override
	public AttributeGroup setAttributeGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeGroup") AttributeGroup attributeGroup) throws BackendException, IllegalRequestException, AccessDeniedException {
		if (!isAttributeAdministrationAllowed())
			throw new AccessDeniedException("Coundn't set attribute data. Permission denied.");

		ValidationUtil.validate(attributeGroup);
		this.beginTransaction();
		if (!(attributeGroup.getClient() != null && attributeGroup.getClient().getName().equals(clientName))) {
			attributeGroup.setClient(this.clientManagement.getClientByName(clientName));
		}
		if (attributeGroup.getId() == null)
			/* attribute group is not already persistent - do insert */
			this.persist(attributeGroup);
		else {
			return this.mergeAttributeGroup(attributeGroup);
		}
		this.commitTransaction();

		return attributeGroup;

	}

	@Override
	@WebMethod
	@WebResult(name = "attributeGroups")
	public List<AttributeGroup> setAttributeGroups(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeGroups") List<AttributeGroup> attributeGroups) throws BackendException, IllegalRequestException {
		if (clientName == null | attributeGroups == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		for (AttributeGroup attributeGroup : attributeGroups) {
			this.setAttributeGroup(clientName, attributeGroup);
		}
		return this.getAttributeGroups(clientName);
	}

	@EJB(name = "ejb/ClientManagementImpl")
	public void setClientManagement(ClientManagement clientManagement) {
		this.clientManagement = clientManagement;
	}

	@EJB(name = "ejb/PrivateDataManagementImpl")
	public void setPrivateDataManagement(PrivateDataManagement privateDataManagement) {
		this.privateDataManagement = privateDataManagement;
	}

	/**
	 * Removes attributes from persistent context that have been removed from the given attribute group.
	 * 
	 * @param attributeGroup the attribute group which may have less attributes than the persistent version.
	 * @return the given attributeGroup.
	 * @throws BackendException
	 */
	private AttributeGroup syncDeletedMapEntries(AttributeGroup attributeGroup, AttributeGroup persistentGroup) throws BackendException {
		if (persistentGroup == null)
			persistentGroup = this.getAttributeGroupById(attributeGroup.getClientName(), attributeGroup.getId());
		List<Attribute> entriesToDelete = this.getMissingElements(persistentGroup.getAttributes(), attributeGroup.getAttributes());
		for (Attribute entryToDelete : entriesToDelete) {
			this.executeAttributeDelete(null, entryToDelete);
		}
		return attributeGroup;
	}

}
