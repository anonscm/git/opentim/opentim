package org.evolvis.idm.identity.privatedata.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.model.QueryConfiguration;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.identity.privatedata.model.Approval;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.AttributeType;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.jboss.wsf.spi.annotation.WebContext;

/**
 * @author Jens Neumaier
 * @author Yorka Neumann
 */
@Stateless
@Local(PrivateDataManagement.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "PrivateDataManagementService", name = "PrivateDataManagementServicePT", portName = "PrivateDataManagementServicePort")
@WebContext(contextRoot = "idm-backend", urlPattern = "/PrivateDataManagementService")
// @EndpointConfig(configName = "Standard WSSecurity Endpoint")
public class PrivateDataManagementImpl extends AbstractBaseServiceImpl implements PrivateDataManagement {

	private AccountManagement accountManagement;

	private ClientManagement clientManagement;

	@Override
	@WebMethod
	public void deleteApproval(@WebParam(name = "clientName") String clientName, @WebParam(name = "approvalId") Long approvalId) throws IllegalRequestException, BackendException {
		if (approvalId == null || clientName == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		this.beginTransaction();
		Approval approval = this.getEntityManager().find(Approval.class, approvalId);
		this.remove(approval);
		this.commitTransaction();
	}

	@Override
	@WebMethod
	public void deleteValues(@WebParam(name = "uuid") String uuid, @WebParam(name = "values") ValueSet valueSet, @WebParam(name = "ignoreLock") boolean ignoreLock) throws BackendException, AccessDeniedException, IllegalRequestException {
		if (uuid == null | valueSet == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		// Account account = this.accountManagement.getAccountByUuid(uuid);

		// TODO check account
		// System.err.println(account);

		this.beginTransaction();
		valueSet = this.find(ValueSet.class, valueSet.getId());
		if (valueSet != null) {
			if (!valueSet.isLockedRecursive()) {
				this.remove(valueSet);
			} else {
				throw IllegalRequestException.LOCKED_VALUESET_EXCEPTION;
			}
		}
		this.commitTransaction();
	}

	@WebMethod
	@WebResult(name = "account")
	public Account getAccount(@WebParam(name = "uuid") String uuid) throws BackendException, AccessDeniedException, IllegalRequestException {
		try {
			return this.accountManagement.getAccountByUuid(uuid);
		} finally {
			// getEntityManager().close();
		}
	}

	private Approval getApproval(String uuid, Value value, ValueSet valueSet, Application application) throws BackendException, IllegalRequestException {
		if(uuid == null && application == null && (value == null ^ valueSet == null))
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		
		this.beginTransaction();
		HashMap<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("uuid", uuid);
		parameterMap.put("application", application);
		String qlString = "SELECT approval FROM Approval approval WHERE approval.account.uuid = :uuid AND approval.application = :application";
		if(value != null){
			parameterMap.put("value", value);
			qlString += " AND approval.approvedAttributeValue = :value AND approval.approvedValueSet IS NULL";
		} else{
			parameterMap.put("valueSet", value);
			qlString += " AND approval.approvedAttributeValue IS NULL AND approval.approvedValueSet = :valueSet";
		}
		List<Approval> approvals = this.executeQuery(Approval.class, qlString, parameterMap);
		this.commitTransaction();
		if(approvals.size() == 0)
			return null;
		return approvals.get(0);
	}

	private QueryResult<Approval> getApprovals(QueryConfiguration queryConfiguration) throws BackendException, IllegalRequestException {
		if (queryConfiguration == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;

		String qlString = "SELECT new Approval(approval) FROM Approval approval";
		if (queryConfiguration.getQlString() != null)
			qlString += " WHERE " + queryConfiguration.getQlString();
		QueryResult<Approval> queryResult = new QueryResult<Approval>();
		queryResult = this.executeSimpleQuery(qlString, queryConfiguration, queryResult);
		return queryResult;

	}

	@Override
	@WebMethod
	@WebResult(name = "approvals")
	public List<Approval> getApprovalsByAccount(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException {
		if (uuid == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("uuid", uuid);
		String qlStringExtension = "approval.account.uuid = :uuid";
		QueryConfiguration queryConfiguration = new QueryConfiguration(qlStringExtension, parameterMap, null, null, null);
		return this.getApprovals(queryConfiguration).getResultList();
	}

	@WebMethod
	@WebResult(name = "approvals")
	public List<Approval> getApprovalsByApplicationId(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationId") Long applicationId) throws BackendException, IllegalRequestException {
		if (applicationId == null || clientName == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("applicationId", applicationId);
		String qlStringExtension = "approval.application.id = :applicationId";
		QueryConfiguration queryConfiguration = new QueryConfiguration(qlStringExtension, parameterMap, null, null, null);
		return this.getApprovals(queryConfiguration).getResultList();
	}

	@WebMethod
	@WebResult(name = "approvals")
	public List<Approval> getApprovalsByUuidAndApplicationId(@WebParam(name = "uuid") String uuid, @WebParam(name = "applicationId") Long applicationId) throws BackendException, IllegalRequestException {
		if (applicationId == null || uuid == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("uuid", uuid);
		parameterMap.put("applicationId", applicationId);
		String qlStringExtension = "approval.account.uuid = :uuid AND approval.application.id = :applicationId";
		QueryConfiguration queryConfiguration = new QueryConfiguration(qlStringExtension, parameterMap, null, null, null);
		return this.getApprovals(queryConfiguration).getResultList();
	}

	@Override
	@WebMethod
	@WebResult(name = "attribute")
	public Attribute getAttributeById(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeId") Long attributeId) throws IllegalRequestException, BackendException {
		if (attributeId == null || clientName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		Attribute attribute = this.find(Attribute.class, attributeId);
		return attribute;
	}

	@SuppressWarnings("unchecked")
	protected Attribute getAttributeByName(AttributeGroup attributeGroup, String name) {
		Query query = getEntityManager().createQuery("SELECT a FROM Attribute a WHERE a.attributeGroup = :attributeGroup AND a.name = :name");

		query.setParameter("attributeGroup", attributeGroup);
		query.setParameter("name", name);

		List<Attribute> result = this.getResultList(query);
		if (!result.isEmpty())
			return result.get(0);
		else
			throw new IllegalArgumentException("Illegal name for attribute: " + name);
	}

	@Override
	@WebMethod
	@WebResult(name = "attributeGroup")
	public AttributeGroup getAttributeGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeGroupName") String attributeGroupName) throws IllegalRequestException, BackendException {
		if (attributeGroupName == null || clientName == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("clientName", clientName);
		parameterMap.put("attributeGroupName", attributeGroupName);
		String qlString = "SELECT attributeGroup FROM AttributeGroup attributeGroup WHERE attributeGroup.name = :attributeGroupName AND attributeGroup.client.name = :clientName";
		List<AttributeGroup> resultList = this.executeQuery(AttributeGroup.class, qlString, parameterMap);
		if (resultList.size() == 0)
			return null;
		return resultList.get(0);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "attributeGroup")
	public AttributeGroup getAttributeGroupById(@WebParam(name = "clientName") String clientName, @WebParam(name = "id") Long id) throws BackendException, AccessDeniedException, IllegalRequestException {
		if (id == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		AttributeGroup attributeGroup = getEntityManager().find(AttributeGroup.class, id);
		if (attributeGroup == null)
			return null;

		return attributeGroup;
	}

	@SuppressWarnings("unchecked")
	protected AttributeGroup getAttributeGroupByName(String clientName, String name) throws BackendException {
		Query query = getEntityManager().createQuery("SELECT ag FROM AttributeGroup ag WHERE ag.client.name = :clientName AND ag.name = :name");

		query.setParameter("clientName", clientName);
		query.setParameter("name", name);

		List<AttributeGroup> result = this.getResultList(query);
		if (!result.isEmpty())
			return result.get(0);
		else
			throw new IllegalArgumentException("Illegal name for attribute group: " + name);
	}

	// TODO jneuma remove redundant code!!!
	/**
	 * ${@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(name = "attributeGroups")
	public List<AttributeGroup> getAttributeGroups(@WebParam(name = "clientName") String clientName) throws BackendException, IllegalRequestException {
		if (clientName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		Query query = getEntityManager().createQuery("SELECT ag FROM AttributeGroup ag WHERE ag.client.name = :clientName ORDER BY ag.displayOrder");

		query.setParameter("clientName", clientName);

		List<AttributeGroup> result = this.getResultList(query);
		return result;
	}

	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(name = "values")
	public List<ValueSet> getValues(@WebParam(name = "uuid") String uuid) throws BackendException, AccessDeniedException, IllegalRequestException {
		if (uuid == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		Account account = this.accountManagement.getAccountByUuid(uuid);

		Query query = getEntityManager().createQuery("SELECT new ValueSet(vs) FROM ValueSet vs WHERE vs.account = :account");

		query.setParameter("account", account);
		List<ValueSet> result = this.getResultList(query);
		return result;
	}

	@SuppressWarnings("unchecked")
	@WebMethod
	@WebResult(name = "values")
	public List<ValueSet> getValuesByAttributes(@WebParam(name = "uuid") String uuid, @WebParam(name = "attributes") List<AttributeGroup> attributes) throws BackendException, AccessDeniedException, IllegalRequestException {
		if (uuid == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		if (attributes == null || attributes.isEmpty())
			return getValues(uuid);

		for (AttributeGroup attributeGroup : attributes)
			if (attributeGroup == null || attributeGroup.getId() == null)
				throw new IllegalRequestException("Attribute group or attribute group id is null.");

		Account account = this.accountManagement.getAccountByUuid(uuid);

		StringBuffer buffer = new StringBuffer();
		buffer.append("SELECT new ValueSet(vs) FROM ValueSet vs WHERE vs.account = :account");

		Query query = getEntityManager().createQuery(buffer.toString());

		query.setParameter("account", account);

		List<ValueSet> result = this.getResultList(query);

		List<Long> ids = new LinkedList<Long>();
		for (AttributeGroup attributeGroup : attributes) {
			ids.add(attributeGroup.getId());
		}

		// TODO improvement
		// TODO remove this hack and use the QL instead!
		for (Iterator<ValueSet> it = result.iterator(); it.hasNext();)
			if (!ids.contains(it.next().getAttributeGroup().getId()))
				it.remove();



		return (List<ValueSet>) (Object) result;
	}

	/**
	 * {@inheritDoc}
	 */
	@WebMethod
	@WebResult(name = "attributes")
	public ValueSet getValuesById(@WebParam(name = "uuid") String uuid, @WebParam(name = "valueSetId") Long valueSetId) throws BackendException, AccessDeniedException, IllegalRequestException {
		if (uuid == null | valueSetId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		ValueSet valueSet = getEntityManager().find(ValueSet.class, valueSetId);
		if (valueSet == null)
			return null;
		ValueSet returnValueSet = new ValueSet(valueSet);
		
		if (!valueSet.getAccount().getUuid().equals(uuid))
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;


		return returnValueSet;
	}

	@Override
	@WebMethod
	@WebResult(name = "value")
	public Value getValueWithBinaryAttachmentById(@WebParam(name = "uuid") String uuid, @WebParam(name = "valueId") Long valueId) throws BackendException, IllegalRequestException {
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("uuid", uuid);
		parameterMap.put("valueId", valueId);
		this.validateNotNullParameter(parameterMap);

		String qlString = "SELECT new Value(value) FROM Value value WHERE value.id = :valueId";
		parameterMap.remove("uuid");
		// parameterMap.put("trueValue", new Boolean(true));
		List<Value> resultList = this.executeQuery(Value.class, qlString, parameterMap);
		if (resultList.size() < 1)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		return resultList.get(0);
	}

	@EJB(name = "ejb/AccountManagementImpl")
	public void setAccountManagement(AccountManagement accountManagement) {
		this.accountManagement = accountManagement;
	}

	@Override
	@WebMethod
	@WebResult(name = "approval")
	public Approval setApproval(@WebParam(name = "clientName") String clientName, @WebParam(name = "approval") Approval approval) throws IllegalRequestException, BackendException {
		if (approval == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		/* common approval validation */
		ValidationUtil.validate(approval);
		/* check if not both is set value and value set */
		if (!(approval.getApprovedAttributeValue() != null ^ approval.getApprovedValueSet() != null))
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		/* the value (set) to approve has to be already persist */
		AbstractEntity entityToApprove = approval.getApprovedAttributeValue();
		if (entityToApprove == null)
			entityToApprove = approval.getApprovedValueSet();
		if (entityToApprove.getId() == null)
			throw IllegalRequestException.APPROVAL_FOR_UNPERSISTENT_VALUE_EXCEPTION;
		this.beginTransaction();
		if (approval.getId() == null) {

			/* security check: account of an approval must be the same as the one of the value (set) */
			if (approval.getApprovedValueSet() != null)
				this.getValuesById(approval.getAccount().getUuid(), approval.getApprovedValueSet().getId());
			else {
				Value persistentValue = this.find(Value.class, approval.getApprovedAttributeValue().getId());
				if (persistentValue == null || !persistentValue.getValueSet().getAccount().equals(approval.getAccount()))
					throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
			}
			if(this.getApproval(approval.getAccount().getUuid(), approval.getApprovedAttributeValue(), approval.getApprovedValueSet(), approval.getApplication()) != null)
				throw IllegalRequestException.INVALID_DUPLICATE_EXCEPTION;
			this.persist(approval);
		} else {
			Approval persistentApproval = this.find(Approval.class, approval.getId());
			if (persistentApproval != null) {
				/* Nothing is allowed to change except the application of an approval - check this */
				boolean legalMergeCondition = persistentApproval.getAccount().equals(approval.getAccount())
						&& (persistentApproval.getApprovedAttributeValue() != null && persistentApproval.getApprovedAttributeValue().equals(approval.getApprovedAttributeValue()) || persistentApproval.getApprovedValueSet() != null
								&& persistentApproval.getApprovedValueSet().equals(approval.getApprovedValueSet()));
				if (!legalMergeCondition)
					throw IllegalRequestException.FORBIDDEN_APPROVAL_MODIFICATION_EXCEPTION;
			}
			approval = this.merge(approval);
		}
		this.commitTransaction();
		return approval;
	}

	@EJB(name = "ejb/ClientManagementImpl")
	public void setClientManagement(ClientManagement clientManagement) {
		this.clientManagement = clientManagement;
	}
	
	@WebMethod
	@WebResult(name = "values")
	public ValueSet setValues(@WebParam(name = "uuid") String uuid, @WebParam(name = "values") ValueSet valueSet, @WebParam(name = "ignoreLock") boolean ignoreLock) throws BackendException, IllegalRequestException, AccessDeniedException {
		if (uuid == null | valueSet == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		ValidationUtil.validate(valueSet);
		if (valueSet.getId() != null) {
			/* Get the persistent version to control constraints */
			ValueSet persistentValueSet = this.find(ValueSet.class, valueSet.getId());
			
			/* TODO: control if persistentValueSet null!!!! - Control if locking violations are given */
			if ((persistentValueSet.isLocked()) && !ignoreLock)
				throw IllegalRequestException.LOCKED_VALUESET_EXCEPTION;
			Value valueToCompare = null;
			if (persistentValueSet.isLockedRecursive() && !ignoreLock) {
				valueToCompare = null;
				for (Value persistentValue : persistentValueSet.getValues()) {
					if (persistentValue.isLocked()) {
						/* control incoming locked value */
						valueToCompare = valueSet.getValueById(persistentValue.getId());

						/* if the value has been removed or modified */
						boolean lockConstraintViolated = (valueToCompare == null || !persistentValue.getValue().equals(valueToCompare.getValue()));

						/* if the lock has been removed */
						lockConstraintViolated = lockConstraintViolated || (!valueToCompare.isLocked());

						/* if the associated attribute has been changed */
						lockConstraintViolated = lockConstraintViolated || (valueToCompare.getAttribute() != null) && !(valueToCompare.getAttribute().getId().equals(persistentValue.getAttribute().getId()));

						/*
						 * The following is a special case of locking and may requires a special exception Setting a byte array is only allowed to values of AttributeType.BLOB
						 */
						lockConstraintViolated = lockConstraintViolated || (!persistentValue.getAttribute().getType().equals(AttributeType.BLOB) && valueToCompare.getByteValue() != null);

						if (lockConstraintViolated)
							throw IllegalRequestException.LOCKED_VALUESET_EXCEPTION;
						// throwException = true;

						// if (valueToCompare == null || !persistentValue.getValue().equals(valueToCompare.getValue()))
						// throw IllegalRequestException.LOCKED_VALUESET_EXCEPTION;

						// if (!valueToCompare.isLocked())
						// throw IllegalRequestException.LOCKED_VALUESET_EXCEPTION;

						// if ((valueToCompare.getAttribute() != null) && !(valueToCompare.getAttribute().getId().equals(persistentValue.getAttribute().getId())))
						// throw IllegalRequestException.LOCKED_VALUESET_EXCEPTION;
						/*
						 * The following is a special case of locking and may requires a special exception Setting a byte array is only allowed to values of AttributeType.BLOB
						 */
						// if (!persistentValue.getAttribute().getType().equals(AttributeType.BLOB) && valueToCompare.getByteValue() != null)
						// throwException = true;
					}

				}
			}
		}
		if (valueSet.getAccount() == null) {
			Account account = getAccount(uuid);
			valueSet.setAccount(account);
		}
		if (valueSet.getAttributeGroup() == null) {
			Client client = this.accountManagement.getClientByUuid(uuid);
			valueSet.setAttributeGroup(getAttributeGroupByName(client.getName(), valueSet.getName()));
		}

		/* synchronize value set approvals */

		// List<Approval> incomingApprovals = new LinkedList<Approval>();
		// List<Approval> existingApprovals = new LinkedList<Approval>();
		// Query queryApprovals =
		// getEntityManager().createQuery("SELECT approval FROM Approval approval WHERE approval.approvedValueSet = :valueSet AND approval.account = :account");
		// queryApprovals.setParameter("valueSet", valueSet);
		// queryApprovals.setParameter("account", account);
		// if (valueSet.getId() != null)
		// existingApprovals = (List<Approval>)
		// this.getResultList(queryApprovals);
		//			
		// if (valueSet.getApprovals() != null) {
		// for (Approval approval : valueSet.getApprovals()) {
		// approval.setAccount(account);
		// approval.setApprovedValueSet(valueSet);
		// for (Approval existingApproval : existingApprovals) {
		// if (existingApproval.getApprovedAttributeValue() == null)
		// approval.setId(existingApproval.getId());
		// }
		// incomingApprovals.add(approval);
		// }
		// }
		//			
		if (valueSet.getValues() != null) {
			for (Value value : valueSet.getValues()) {
				value.setAttribute(getAttributeByName(valueSet.getAttributeGroup(), value.getName()));
				value.setValueSet(valueSet);
				// if (value.getApprovals() != null) {
				// for (Approval approval : value.getApprovals()) {
				// approval.setAccount(account);
				// approval.setApprovedValueSet(valueSet);
				// approval.setApprovedAttributeValue(value);
				// for (Approval existingApproval : existingApprovals) {
				// if (existingApproval.getApprovedAttributeValue() != null
				// &&
				// existingApproval.getApprovedAttributeValue().equals(value))
				// approval.setId(existingApproval.getId());
				// }
				// incomingApprovals.add(approval);
				// }
				// }
			}
		}

		this.beginTransaction();

		if (valueSet.getId() != null) {
			/* Remove persistent values that are not given any more */
			this.syncDeletedValues(uuid, valueSet);

			/* Exclude values of attribute type BLOB. */
			List<Value> valuesToExclude = new LinkedList<Value>();
			for (Value value : valueSet.getValues()) {
				if (value.getAttribute().getType().equals(AttributeType.BLOB) && value.getId() != null) {
					Value persistentValue = this.getEntityManager().find(Value.class, value.getId());
					persistentValue = new Value(persistentValue);
					valuesToExclude.add(persistentValue);
				}
			}
			if (valuesToExclude.size() > 0) {
				for (Value value : valuesToExclude) {
					valueSet.deleteValueById(value.getId());
					valueSet.addValue(value);
				}
			}
			valueSet = this.merge(valueSet);

		} else {
			this.persist(valueSet);
		}

		// remove values if necessary
		// Query query =
		// getEntityManager().createQuery("SELECT v FROM Value v WHERE v.valueSet = :valueSet");
		// query.setParameter("valueSet", valueSet);
		// for (Value value : (List<Value>) this.getResultList(query)) {
		// if (!valueSet.getValues().contains(value)) {
		// log.debug("Removing value with id: "+value.getId());
		// getEntityManager().remove(value);
		// }
		// }

		// remove approvals if necessary
		// existingApprovals = (List<Approval>)
		// queryApprovals.setParameter("valueSet",
		// valueSet).getResultList();
		// for (Approval approval: existingApprovals) {
		// if (!incomingApprovals.contains(approval) && !isNew) {
		// log.debug("Removing approval with id: "+approval.getId());
		// getEntityManager().remove(approval);
		// }
		// }

		this.commitTransaction();

		// re-set JPA transient fields
		valueSet.setName(valueSet.getAttributeGroup().getName());
		for (Value value : valueSet.getValues())
			value.setName(value.getAttribute().getName());

		// Set all XmlTransient fields to null for the local service tests.
		// valueSet.setAttributeGroup(null);
		// valueSet.setAccount(null);

		if (valueSet.getName().equals("personalData")) {
			String firstname = valueSet.getValue("firstname").getValue();
			String lastname = valueSet.getValue("lastname").getValue();
			ClientPropertyMap clientPropertyMap = clientManagement.getClientPropertyMap(valueSet.getAccount().getClientName());
			String displayName;
			boolean displayNameGenerationEnabled = clientPropertyMap.getBoolean(ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED);
			String displayNameGenerationPattern = clientPropertyMap.get(ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_PATTERN);
			if (displayNameGenerationEnabled) {
				displayName = displayNameGenerationPattern;
				displayName = displayName.replace("%firstname%", firstname);
				displayName = displayName.replace("%lastname%", lastname);

				Account account = accountManagement.getAccountByUuid(uuid);
				account.setDisplayName(displayName);
				accountManagement.setAccount(account.getClientName(), account);
			}
		}

		clientManagement.updateLastChangeUserList(valueSet.getClientName());

		return valueSet;
	}

	private ValueSet syncDeletedValues(String uuid, ValueSet valueSet) throws BackendException {
		ValueSet persistentValueSet = this.getValuesById(uuid, valueSet.getId());
		List<Value> entriesToDelete = this.getMissingElements(persistentValueSet.getValues(), valueSet.getValues());
		for (Value entryToDelete : entriesToDelete) {
			this.getEntityManager().remove(entryToDelete);
		}
		return valueSet;
	}

	
	
}
