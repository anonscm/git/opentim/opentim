package org.evolvis.idm.identity.account.service;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.QueryConfiguration;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.jboss.wsf.spi.annotation.WebContext;

/**
 * @author Yorka Neumann
 */
@Stateless
@Local(AccountManagement.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "AccountManagementService", name = "AccountManagementServicePT", portName = "AccountManagementServicePort")
@WebContext(contextRoot = "idm-backend", urlPattern = "/AccountManagementService")
// @EndpointConfig(configName = "Standard WSSecurity Endpoint")
public class AccountManagementImpl extends AbstractBaseServiceImpl implements AccountManagement {

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	public void deleteAccount(@WebParam(name = "uuid") String uuid) throws BackendException, AccessDeniedException, IllegalRequestException {
		// if operation is allowed is proofed in deleteAccount(Account account)
		if (uuid == null)
			throw new BackendException("No valid parameter to delete an account");

		Account account = this.getAccountByUuid(uuid);

		if (account == null)
			throw new BackendException("Account does not exist");
		this.beginTransaction();
		this.getEntityManager().remove(account);
		this.commitTransaction();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "account")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Account setAccount(@WebParam(name = "clientName") String clientName, @WebParam(name = "account") Account account) throws BackendException, IllegalRequestException {
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("account", account);
		this.validateNotNullParameter(parameterMap);
		ValidationUtil.validate(account);
		if (account.isSoftDelete() && account.getDeactivationDate() == null) {
			account.setDeactivationDate(Calendar.getInstance());
		} else if (!account.isSoftDelete() && account.getDeactivationDate() != null) {
			account.setDeactivationDate(null);
		}
		this.beginTransaction();
		if (account.getId() == null) {
			account.setCreationDate(Calendar.getInstance());
			this.persist(account);
		} else {
			account = this.mergeInNewTransAction(account);
		}
		this.commitTransaction();
		return account;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "account")
	public Account getAccountByUuid(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException {
		if (uuid == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		// this.beginTransaction();
		Query query = this.getEntityManager().createQuery("SELECT account FROM Account account  WHERE account.uuid =:uuid");
		query.setParameter("uuid", uuid);
		return (Account) this.getSingleResult(query);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@WebMethod
	@WebResult(name = "account")
	public Account getAccountByUsername(@WebParam(name = "clientName") String clientName, @WebParam(name = "username") String username) throws BackendException, IllegalRequestException {
		if (clientName == null | username == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		Query query = this.getEntityManager().createQuery("SELECT account FROM Account account  WHERE account.username =:username AND account.client.name =:clientName");
		query.setParameter("clientName", clientName);
		query.setParameter("username", username);
		return (Account) this.getSingleResult(query);
	}

	protected boolean isAccountAdministrationAllowed() {
		// TODO WSSecurity
		return true;
	}

	@Override
	@WebMethod
	@WebResult(name = "accounts")
	public QueryResult<Account> getAccounts(@WebParam(name = "clientName") String clientName, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, AccessDeniedException, IllegalRequestException {
		if (clientName == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		/* create a map for standard used parameters */
		Map<String, Object> parameterMap = new HashMap<String, Object>();
		parameterMap.put("clientName", clientName);

		/* create a list of allowed entity beans to use for ordering the query */
		List<Class<?>> allowedOrderBeans = null;
		allowedOrderBeans = new LinkedList<Class<?>>();
		allowedOrderBeans.add(Account.class);
		String whereClause = " WHERE account.client.name = :clientName";
		String qlString = "SELECT account FROM Account account";
		String defaultSortOrder = " account.displayName ASC ";

		if (queryDescriptor != null && queryDescriptor.hasSearchProperties()) {
			/*
			 * TODO the following is a workaround to the request that the search for a certain pattern of users should be extended to their master data.
			 */
			if (queryDescriptor.getSearchProperties().size() <= 2 && queryDescriptor.getSearchProperties().get(0).getFieldPath().equals("all")) {
				/* choose an alternativ query string */
				qlString = "SELECT DISTINCT account FROM Value value JOIN value.valueSet.account account ";
				whereClause += " AND (LOWER(value.value) LIKE :searchParam ESCAPE '#' OR LOWER(account.displayName) LIKE :searchParam ESCAPE '#' OR LOWER(account.username) LIKE :searchParam ESCAPE '#') ";
				/*
				 * now delete the search property in order to prevent generic query generation for it.
				 */
				BeanSearchProperty searchProperty = queryDescriptor.getSearchProperties().remove(0);
				String parameter = this.convertStringToSQL(searchProperty.getSearchValue().toLowerCase());
				parameterMap.put("searchParam", "%" + parameter + "%");
			}
		}
		qlString += whereClause;
		QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap, queryDescriptor, defaultSortOrder, allowedOrderBeans);

		QueryResult<Account> queryResult = new QueryResult<Account>();

		queryResult = this.executeSimpleQuery(qlString, queryConfiguration, queryResult);

		return queryResult;
	}

	@Override
	@WebMethod
	@WebResult(name = "client")
	public Client getClientByUuid(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException {
		if (uuid == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		Query query = this.getEntityManager().createQuery("SELECT account.client FROM Account account WHERE account.uuid = :uuid1");
		query.setParameter("uuid1", uuid);
		return (Client) this.getSingleResult(query);
	}

	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "deactivatedAccounts")
	public List<Account> getDeactivatedAccountsSince(@WebParam(name = "clientName") String clientName, @WebParam(name = "sinceData") Calendar sinceDate) throws BackendException, IllegalRequestException {
		if (clientName == null | sinceDate == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;

		String qlString = "SELECT account FROM Account account WHERE account.client.name = :clientName AND account.deactivationDate >= :sinceDate AND account.deactivationDate IS NOT NULL";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("clientName", clientName);
		query.setParameter("sinceDate", sinceDate);
		List<Account> resultList = this.getResultList(query);
		return resultList;
	}

	@Override
	@WebMethod
	public boolean isDeactived(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException {
		if (uuid == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		Account account = this.getAccountByUuid(uuid);
		if (account == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		if (account.isSoftDelete())
			return true;
		return false;
	}
}
