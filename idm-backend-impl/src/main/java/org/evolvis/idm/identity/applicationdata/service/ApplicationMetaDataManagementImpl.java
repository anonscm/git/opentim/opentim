package org.evolvis.idm.identity.applicationdata.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.identity.applicationdata.model.AppMetaData;
import org.evolvis.idm.identity.applicationdata.model.AppMetaDataValue;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.jboss.wsf.spi.annotation.WebContext;

/**
 * @author Jens Neumaier
 * @author Yorka Neumann
 */

@Stateless 
@Local(ApplicationMetaDataManagement.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "ApplicationMetaDataManagementService", name = "ApplicationMetaDataManagementServicePT", portName = "ApplicationMetaDataManagementServicePort")
@WebContext(contextRoot = "idm-backend", urlPattern = "/ApplicationMetaDataManagementService")
public class ApplicationMetaDataManagementImpl extends AbstractBaseServiceImpl implements ApplicationMetaDataManagement {

	private AccountManagement accountManagement;

	private ApplicationManagement applicationManagement;

	@EJB(name = "ejb/AccountManagementImpl")
	public void setAccountManagement(AccountManagement accountManagement) {
		this.accountManagement = accountManagement;
	}


	@EJB(name = "ejb/ApplicationManagementImpl")
	public void setApplicationManagement(ApplicationManagement applicationManagement) {
		this.applicationManagement = applicationManagement;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	@WebMethod
	@WebResult(name = "appMetaData")
	public AppMetaData getApplicationMetaData(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException {
		if (applicationName == null | uuid == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
			String qlString = "SELECT apm FROM AppMetaData apm WHERE apm.account.uuid = :uuid1 AND apm.application.name = :applicationName";
			Query query = this.getEntityManager().createQuery(qlString);
			query.setParameter("uuid1", uuid);
			query.setParameter("applicationName", applicationName);
			List<AppMetaData> resultList = this.getResultList(query);
			if (resultList.size() == 1)
				return resultList.get(0);
			if (resultList.size() == 0)
				return null;
			// TODO
			throw new BackendException("More than one data set found!");
	}


	@Override
	@WebMethod
	@WebResult(name = "appMetaDataValue")
	public String getApplicationMetaDataValue(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "metaDataKey") String metaDataKey) throws BackendException, IllegalRequestException {
		if (applicationName == null | uuid == null | metaDataKey == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
			// query persistent meta data
			AppMetaData applicationMetaData = this.getApplicationMetaData(applicationName, uuid);
			if (applicationMetaData != null) {
				return applicationMetaData.get(metaDataKey);
			}
			// no application meta data
			return null;
	}


	@Override
	@WebMethod
	@WebResult(name = "appMetaData")
	public AppMetaData setApplicationMetaData(@WebParam(name = "appMetaData") AppMetaData appMetaData) throws BackendException, IllegalRequestException {
		if (appMetaData == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		ValidationUtil.validate(appMetaData);
			this.beginTransaction();
			if (appMetaData.getId() == null) {
				// application meta data not already persistent - do insert
				this.persist(appMetaData);
			} else {
				// application meta data already persistent - do update
				appMetaData = this.syncDeletedMapEntries(appMetaData);
				appMetaData = this.merge(appMetaData);
			}
			this.commitTransaction();
			return appMetaData;
	}


	@Override
	@WebMethod
	@WebResult(name = "appMetaDataValue")
	public String setApplicationMetaDataValue(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "metaDataKey") String metaDataKey, @WebParam(name = "metaDataValue") String metaDataValue) throws BackendException, IllegalRequestException {
		if (applicationName == null | uuid == null | metaDataKey == null | metaDataValue == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
			String returnValue = null;
			// query for persistent application meta data
			AppMetaData appMetaData = this.getApplicationMetaData(applicationName, uuid);
			
			if (appMetaData == null) {
				// there is no persistent application meta data - so create it
				appMetaData = new AppMetaData();
				appMetaData.setAccount(this.accountManagement.getAccountByUuid(uuid));

				appMetaData.setApplication(this.applicationManagement.getApplicationByUuid(uuid, applicationName));
			}
			// insert the new key/value pair it
			returnValue = appMetaData.put(metaDataKey,metaDataValue);
			// and persist the changes	
			this.setApplicationMetaData(appMetaData);
			return returnValue; 
	}


	@Override
	@WebMethod
	public void removeApplicationMetaDataValue(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "metaDataKey") String metaDataKey) throws BackendException, IllegalRequestException {
		if (applicationName == null | uuid == null | metaDataKey == null) {
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		}
			AppMetaData appMetaData = this.getApplicationMetaData(applicationName, uuid);
			this.getEntityManager().clear();
			String metaDataValue = appMetaData.remove(metaDataKey);
			if (metaDataValue == null)
				throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
			this.setApplicationMetaData(appMetaData);
	}

	
	private AppMetaData getApplicationMetaDataById(@WebParam(name = "appMetaDataId") Long appMetaDataId) throws BackendException, IllegalRequestException{
		if(appMetaDataId == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		try{
			AppMetaData appMetaData = this.getEntityManager().find(AppMetaData.class, appMetaDataId);
			return appMetaData;
			
		} catch(Exception e){
			log.error(e.toString(), e);
			throw new BackendException(e.getMessage());
		}
	}
	
	private AppMetaData syncDeletedMapEntries(AppMetaData appMetaData) throws BackendException{
		AppMetaData persistentMap = this.getApplicationMetaDataById(appMetaData.getId());
		List<AppMetaDataValue> entriesToDelete = this.getMissingElements(persistentMap.getMapEntries(), appMetaData.getMapEntries());
		try{
			for(AppMetaDataValue entryToDelete : entriesToDelete){
				this.getEntityManager().remove(entryToDelete);				
			}
			return appMetaData;
		} catch(Exception e){
			log.error(e.toString(), e);
			throw new BackendException(e.getMessage());
		}
	}


	@Override
	@WebMethod
	public void deleteApplicationMetaData(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException {
		if(applicationName == null | uuid == null)
			throw IllegalRequestException.MISSING_PARAMETERS_EXCEPTION;
		try{
			this.beginTransaction();
			AppMetaData appMetaData = this.getApplicationMetaData(applicationName, uuid);
			if(appMetaData == null)
				throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
			this.getEntityManager().remove(appMetaData);
			this.commitTransaction();
		} catch(Exception e){
			log.error(e.toString(), e);
			throw new BackendException(e.getMessage());
		}
	}
	

}
