package org.evolvis.idm.identity.account.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.Query;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.QueryConfiguration;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.I18nUtil;
import org.evolvis.idm.common.util.RestClientUtil;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.model.UserPropertyFields;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.synchronization.service.DirectoryServiceSynchronization;
import org.evolvis.opensso.rest.RestClient;
import org.evolvis.opensso.rest.constant.UserAttribute;
import org.evolvis.opensso.rest.constant.UserStatus;
import org.evolvis.opensso.rest.service.AuthenticateUser;
import org.evolvis.opensso.rest.service.CreateUser;
import org.evolvis.opensso.rest.service.DeleteUser;
import org.evolvis.opensso.rest.service.Search;
import org.evolvis.opensso.rest.service.UpdateUser;
import org.jboss.wsf.spi.annotation.WebContext;
import org.safehaus.uuid.UUIDGenerator;

/**
 * 
 * @author Jens Neumaier
 * 
 */
@Stateless
@Local(UserManagement.class)
@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "UserManagementService", name = "UserManagementServicePT", portName = "UserManagementServicePort")
@WebContext(contextRoot = "idm-backend", urlPattern = "/UserManagementService")
public class UserManagementImpl extends AbstractBaseServiceImpl implements UserManagement {
	
	public static Map<String, Long> userListLastChange = new HashMap<String, Long>();

	public static String getOpenSSORealm(String clientName) {
		if (clientName != null)
			return "/" + clientName;
		return null;
	}

	private ClientManagement clientManagement;

	private org.evolvis.idm.identity.account.service.AccountManagement accountManagement;

	private PrivateDataManagement privateDataManagement;
	
	private DirectoryServiceSynchronization directoryServiceSynchronization;

	@EJB(name = "ejb/AccountManagementImpl")
	public void setAccountManagement(org.evolvis.idm.identity.account.service.AccountManagement accountManagement) {
		this.accountManagement = accountManagement;
	}

	@EJB(name = "ejb/ClientManagementImpl")
	public void setClientManagement(ClientManagement clientManagement) {
		this.clientManagement = clientManagement;
	}

	@EJB(name = "ejb/PrivateDataManagementImpl")
	public void setPrivateDataManagement(PrivateDataManagement privateDataManagement) {
		this.privateDataManagement = privateDataManagement;
	}
	
	@EJB(name = "ejb/DirectoryServiceSynchronizationImpl")
	public void setDirectoryServiceSynchronization(DirectoryServiceSynchronization directoryServiceSynchronization) {
		this.directoryServiceSynchronization = directoryServiceSynchronization;
	}

	/**
	 * Create User in Idm-Backend
	 * 
	 * @param clientName
	 * @param userStatusActive 
	 */
	private Account createIdMUser(String clientName, String uuid, String login, String firstname, String lastname, String displayName, boolean userStatusActive) throws BackendException {
		Client client = clientManagement.getClientByName(clientName);
		
		// creating account
		Account account = null;
		try {
			account = accountManagement.setAccount(client.getName(), new Account(uuid, login, displayName, client, !userStatusActive));
		} catch(BackendException e){
			log.error("createIdmUser() failed with setAccount() uuid=" + uuid + ", login=" + login + ", client=" + client.getName(), e);
			throw e;
		}
		
		catch (Exception e) {
			log.error("createIdmUser() failed with setAccount() uuid=" + uuid + ", login=" + login + ", client=" + client.getName(), e);
			throw new BackendException(e.getLocalizedMessage());
		}

		// saving name
		ValueSet name = new ValueSet("personalData");
		name.addValue(new Value("firstname", firstname));
		name.addValue(new Value("lastname", lastname));
		try {
			privateDataManagement.setValues(uuid, name, false);
		} catch(BackendException e){
			log.error("createIdmUser() failed with setAccount() uuid=" + uuid + ", login=" + login + ", client=" + client.getName(), e);
			throw e;
		} 
		catch (Exception e) {
			log.error("createIdmUser() failed with setValues() uuid=" + uuid + ", name=" + name, e);
			throw new BackendException(e.getLocalizedMessage());
		}
		
		directoryServiceSynchronization.synchroniseAccount(uuid);
		
		updateDisplayName(clientName, uuid, firstname, lastname);
		
		return account;
	}

	/**
	 * Add a User in Liferay. Use com.liferay.portal.action.TCKAction._getUser(..) as pattern.
	 */
	// private User createLiferayUser(long companyId, String firstName, String
	// lastName, String emailAddress,
	// String screenName, Locale locale) throws PortalException, SystemException
	// {
	//
	// return null;
	// long creatorUserId = 0;
	// boolean autoPassword = false;
	// String password = PwdGenerator.getPassword();
	// boolean autoScreenName = false;
	// String openId = StringPool.BLANK;
	// String middleName = StringPool.BLANK;
	// int prefixId = 0;
	// int suffixId = 0;
	// boolean male = true;
	// int birthdayMonth = Calendar.JANUARY;
	// int birthdayDay = 1;
	// int birthdayYear = 1970;
	// String jobTitle = StringPool.BLANK;
	// long[] groupIds = new long[0];
	// long[] organizationIds = new long[0];
	// long[] roleIds = new long[0];
	// long[] userGroupIds = new long[0];
	// boolean sendEmail = false;
	// ServiceContext serviceContext = new ServiceContext();
	//        
	// log.info("Registering liferay user: ScreenName: "+screenName+" Email: "
	// +emailAddress);
	//
	// return UserLocalServiceUtil.addUser(creatorUserId, companyId,
	// autoPassword, password, password, autoScreenName, screenName,
	// emailAddress, openId, locale, firstName, middleName, lastName, prefixId,
	// suffixId, male, birthdayMonth, birthdayDay,
	// birthdayYear, jobTitle, groupIds, organizationIds, roleIds, userGroupIds,
	// sendEmail, serviceContext);
	// }
	/**
	 * Create User in OpenSSO, returns RegisterUUID if DOUBLE_OPT_IN is active
	 * 
	 * @param clientSSORealm
	 */
	private void createOpenSSOUser(String clientName, String login, String password, String email, String uuid, boolean isInactive, String openSSORealm) throws BackendException {

		String passwortPattern = clientManagement.getClientPropertyMap(clientName).get(ClientPropertyMap.KEY_SECURITY_PASSWORT_PATTERN);
        if (!passwortPattern.isEmpty() && !password.matches(passwortPattern)) {
        	// TODO jneuma create exception
        	throw new IllegalRequestException("Password must match the following pattern: "+passwortPattern);
        }
		
		RestClient restClient = getRestClient(clientName);
		
		String userStatus = isInactive == true ? UserStatus.INACTIVE : UserStatus.ACTIVE;

		try {
			CreateUser createUser = restClient.callService(new CreateUser(openSSORealm, login, password, ".", ".", email, userStatus, uuid));

			if (!createUser.success()) {
				throw new BackendException("createOpenSsoUser() create user failed, error=" + createUser.getError());
			}
		} catch (IOException ioe) {
			throw new BackendException("createOpenSsoUser() failed with IOException, error=" + ioe.getLocalizedMessage());
		}
	}

	protected String createUser(String clientName, String username, String password, String displayName, String firstname, String lastname, Locale locale, boolean softDelete) throws BackendException {

		// generate uuid
		String uuid = UUIDGenerator.getInstance().generateRandomBasedUUID().toString();

		String realm = getOpenSSORealm(clientName);

		log.info("Trying to create user on clientName=" + clientName + " with uuid=" + uuid + ", username=" + username);

		// register user
		Account account = null;
		try {
			// add user to idm-backend
			account = createIdMUser(clientName, uuid, username, firstname, lastname, displayName, !softDelete);

			// add user to opensso
			createOpenSSOUser(clientName, username, password, username, uuid, softDelete, realm);
		} catch (Exception e) {
			log.error("createAccount() failed, username=" + username + ", password=*****, displayName=" + displayName + ", firstname=" + firstname + ", lastname=" + lastname + ", locale=" + locale, e);
			if (account != null) {
				try {
					deleteOpenSSOUser(clientName, uuid);
				} catch (Exception e2) {
					log.error("Error while deleting OpenSSO account with uuid="+uuid+" on client="+clientName, e2);
				}
				try {
					deleteIdMUser(uuid);
				} catch (Exception e2) {
					log.error("Error while deleting IdM user with uuid="+uuid+" on client="+clientName, e2);
				}				
			}

			if(e instanceof IllegalRequestException){
				log.error("Rethrow IllegalRequestException");
				throw (IllegalRequestException) e;
			}
			if (e instanceof BackendException){
				log.error("Rethrow BackendException");
				throw (BackendException) e;
			}
			else
				throw BackendException.UNKNOWN_ERROR_EXCEPTION;
		}

		log.info("Successfully created user on clientName=" + clientName + " with uuid=" + uuid + ", username=" + username);
		return uuid;
	}

	@WebMethod
	public void createUser(@WebParam(name = "user") User user) throws BackendException, IllegalRequestException {
		createUser(user.getClientName(), user.getUsername(), user.getPassword(), user.getDisplayName(), user.getFirstname(), user.getLastname(), I18nUtil.getLocaleFromString(user.getLocaleString()), user.isSoftDelete());
		clientManagement.updateLastChangeUserList(user.getClientName());
	}
	
	private void deleteIdMUser(String uuid) throws BackendException {
		try {
			accountManagement.deleteAccount(uuid);
		} catch (Exception e) {
			log.error("deleteIdmUser() failed with uuid=" + uuid, e);
			throw new BackendException(e.getMessage());
		}
	}

	private void deleteOpenSSOUser(String clientName, String uuid) throws IllegalRequestException, BackendException {

		Account account = accountManagement.getAccountByUuid(uuid);
		String realm = getOpenSSORealm(account.getClientName());
		RestClient restClient = getRestClient(clientName);

		try {
			Search search = restClient.callService(new Search(realm).setAttr(UserAttribute.USER_UUID, uuid));

			if (search.success() && search.getUid() != null) {

				DeleteUser deleteUser = restClient.callService(new DeleteUser(realm, search.getUid()));

				if (!deleteUser.success()) {
					log.error("deleteOpenSsoUser() failed, realm=" + realm + ", uuid=" + uuid + ", uid=" + search.getUid());
				}
			}
		} catch (Exception e) {
			log.error("deleteOpenSsoUser() failed, realm=" + realm + ", uuid=" + uuid, e);
		}
	}

	@Override
	@WebMethod
	public void deleteUser(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException {
		if (uuid != null) {
			Account account = accountManagement.getAccountByUuid(uuid);
			deleteOpenSSOUser(account.getClientName(), uuid);
			deleteIdMUser(uuid);
		}
	}

	// helper methods

//	/**
//	 * Fill the given User with account data (firstname, lastname) from IdM. The uuid must be set!
//	 * 
//	 * @deprecated
//	 * @param user
//	 */
//	private void fillUserAccountWithIdMData(User user) {
//		try {
//			List<ValueSet> values = privateDataManagement.getValues(user.getUuid());
//			for (ValueSet valueSet : values) {
//				if (valueSet.getName().equals("personalData")) {
//					user.setFirstname(valueSet.getValue("firstname").getValue());
//					user.setLastname(valueSet.getValue("lastname").getValue());
//				}
//			}
//		} catch (Exception e) {
//			log.warn("fillUserAccountWithIdmDatas() failed for userAccount=" + user, e);
//		}
//	}

//	/**
//	 * Fill the given User with account data (login, status, email) from OpenSSO. The uuid must be set!
//	 * 
//	 * @deprecated
//	 * @param restClient 
//	 * @param user
//	 */
//	private void fillUserAccountWithOpenSsoDatas(RestClient restClient, User user) throws BackendException {
//
//		if (restClient == null)
//			restClient = getRestClient(user.getClientName());
//
//		// TODO: Get Realm from ClientId
//		String realm = user.getOpenSSORealm();
//
//		// search user
//		try {
//			Search search = restClient.callService(new Search(realm).setAttr(UserAttribute.USER_UUID, user.getUuid()));
//
//			if (search.success() && search.getUids().size() == 1) {
//
//				Read read = restClient.callService(new Read(realm, search.getUid()));
//
//				user.setStatus(read.getAttributeValue(UserAttribute.USERSTATUS));
//			}
//		} catch (Exception e) {
//			log.warn("fillUserAccountWithOpenSsoDatas() failed for userAccount=" + user, e);
//		}
//	}

	// private void deleteLifeayUser(String uuid) {
	//
	// User user;
	//
	// try {
	// user = UserLocalServiceUtil.getUserByUuid(uuid);
	// }
	// catch (Exception e) {
	// log.error("deleteLifeayUser() get user failed, uuid=" + uuid,
	// e);
	// return;
	// }
	//
	// try {
	// UserLocalServiceUtil.deleteUser(user);
	// }
	// catch (Exception e) {
	// log.error("deleteLifeayUser() delete user failed, user=" + user, e);
	// }
	//
	// Group group;
	// try {
	// group = GroupLocalServiceUtil.getFriendlyURLGroup(user.getCompanyId(),
	// StringPool.SLASH + user.getScreenName());
	// if (null != group) {
	// GroupLocalServiceUtil.deleteGroup(group);
	// }
	// }
	// catch (Exception e) {
	// log.error("deleteLifeayUser() delete user's groups failed, user=" +
	// user, e);
	// }
	// }

	private RestClient getRestClient(String clientName) throws IllegalRequestException, AccessDeniedException, BackendException {
		ClientPropertyMap clientPropertyMap = clientManagement.getClientPropertyMap(clientName);
		return RestClientUtil.createClient(clientPropertyMap);
	}

	protected User getUserByAccount(Account account) throws BackendException {
		return this.getUserByUuid(account.getUuid());
	}

	@Override
	@WebMethod
	@WebResult(name = "user")
	public User getUserByUsername(@WebParam(name = "clientName") String clientName, @WebParam(name = "username") String username) throws BackendException {
		// TODO get from view
		return getUserByAccount(accountManagement.getAccountByUsername(clientName, username));
	}

	@Override
	@WebMethod
	@WebResult(name = "user")
	public User getUserByUuid(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException {
		if (uuid == null)
			throw IllegalRequestException.INVALID_PARAMETER_COMBINATION_EXCEPTION;
		this.beginTransaction();
		String qlString = "SELECT user FROM User user WHERE user.uuid = :uuid";
		Query query = this.getEntityManager().createQuery(qlString);
		query.setParameter("uuid", uuid);
		User user = (User) query.getSingleResult();
		this.commitTransaction();
		log.debug("User: "+user.toString());
		return user;
	}

	@Override
	@WebMethod
	@WebResult(name = "users")
	public QueryResult<User> getUsers(@WebParam(name = "clientName") String clientName, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, IllegalRequestException {
		// TODO: check permissions -> only superadmin, clientadmin, fachadmin

		// TODO: get all for callers permission
		

		QueryResult<User> userQueryResult = new QueryResult<User>();
		try {
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			parameterMap.put("clientName", clientName);
			String qlString = "SELECT user FROM User user WHERE user.client.name = :clientName";
			List<Class<?>> allowedOrderBeans = new LinkedList<Class<?>>();
			allowedOrderBeans.add(User.class);
			
			if (queryDescriptor != null && queryDescriptor.hasSearchProperties()) {
				/*
				 * TODO the following is a workaround to the request that the search for a certain pattern of users should be extended to their master data.
				 */
				if (queryDescriptor.getSearchProperties().size() <= 2 && queryDescriptor.getSearchProperties().get(0).getFieldPath().equals("all")) {
					/* choose an alternativ query string */
					qlString = "SELECT user FROM User user WHERE EXISTS (SELECT DISTINCT account FROM Value value JOIN value.valueSet.account account ";
					String whereClause = "WHERE account.client.name = :clientName AND account.id = user.id";
					whereClause += " AND (LOWER(value.value) LIKE :searchParam ESCAPE '#' OR LOWER(account.displayName) LIKE :searchParam ESCAPE '#' OR LOWER(account.username) LIKE :searchParam ESCAPE '#')) ";
					qlString += whereClause;
					/* now delete the search property in order to prevent generic query generation for it. */
					BeanSearchProperty searchProperty = queryDescriptor.getSearchProperties().remove(0);
					String parameter = this.convertStringToSQL(searchProperty.getSearchValue().toLowerCase());
					parameterMap.put("searchParam", "%" + parameter + "%");
					
					/* now check if there is also a user status search property */
					if(queryDescriptor.getSearchProperties().size() == 1 && queryDescriptor.getSearchProperties().get(0).getFieldPath().equals(UserPropertyFields.FIELD_PATH_SOFTDELETE)){
						qlString += " AND user.softDelete = :softDelete ";
						searchProperty = queryDescriptor.getSearchProperties().remove(0);
						Boolean softDelete = Boolean.valueOf(searchProperty.getSearchValue());
						parameterMap.put("softDelete", softDelete);
					}
				}
			}
			
			QueryConfiguration queryConfiguration = new QueryConfiguration(qlString, parameterMap, queryDescriptor, " user.username ASC " , allowedOrderBeans);
			userQueryResult = executeSimpleQuery(qlString, queryConfiguration, userQueryResult);
		} catch (Exception e) {
			log.warn(e);
			throw BackendException.UNKNOWN_ERROR_EXCEPTION;
		}

		log.debug("getUsers() found " + userQueryResult.size()+ " users.");
	
		return userQueryResult;
	}

	@WebMethod
	@WebResult(name = "isUserExisting")
	public boolean isUserExisting(@WebParam(name = "clientName") String clientName, @WebParam(name = "username") String username) throws BackendException, IllegalRequestException {
		boolean isExist = false;
		try {
			if (accountManagement.getAccountByUsername(clientName, username) != null)
				isExist = true;
		} catch (IllegalRequestException e) {
			return false;
		}

		return isExist;
	}

	/**
	 * Register User in OpenSSO, returns RegisterUUID if DOUBLE_OPT_IN is active
	 * 
	 * @param userStatus
	 * @param openSSORealm
	 */
	private String registerOpenSSOUser(String clientName, String openSSORealm, String login, String password, String email, String uuid) throws BackendException {

		String passwortPattern = clientManagement.getClientPropertyMap(clientName).get(ClientPropertyMap.KEY_SECURITY_PASSWORT_PATTERN);
        if (!password.matches(passwortPattern)) {
        	// TODO jneuma create exception
        	throw new IllegalRequestException("Password must match the following pattern: "+passwortPattern);
        }
		
		// TODO jneuma !!!
		// // check for double_opt_in
		// String userStatus;
		// if (PortletConfig.REGISTER_DOUBLE_OPT_IN) {
		String userStatus = UserStatus.INACTIVE;
		// }
		// else {
		// userStatus = UserStatus.ACTIVE;
		// }

		RestClient restClient = getRestClient(clientName);

		try {
			CreateUser createUser = restClient.callService(new CreateUser(openSSORealm, login, password, ".", ".", email, userStatus, uuid));

			if (createUser.success()) {

				// check for double_opt_in
				if (userStatus.equals(UserStatus.INACTIVE)) {

					String registerKey = java.util.UUID.randomUUID().toString();

					UpdateUser updateUser = restClient.callService(new UpdateUser(openSSORealm, login).setAttr(UserAttribute.REGISTER_UUID, registerKey));

					if (!updateUser.success()) {
						throw new BackendException("registerOpenSsoUser() update user failed, error=" + updateUser.getError());
					} else {
						return registerKey;
					}
				} else {
					return null;
				}
			} else {
				throw new BackendException("registerOpenSsoUser() create user failed, error=" + createUser.getError());
			}
		} catch (IOException ioe) {
			throw new BackendException("registerOpenSsoUser() failed with IOException, error=" + ioe.getLocalizedMessage());
		}
	}

	@Override
	@WebMethod
	@WebResult(name = "activationKey")
	public String registerUser(@WebParam(name = "clientName") String clientName, @WebParam(name = "username") String username, @WebParam(name = "password") String password, @WebParam(name = "displayName") String displayName,
			@WebParam(name = "firstname") String firstname, @WebParam(name = "lastname") String lastname, @WebParam(name = "localeString") String localeString) throws BackendException, IllegalRequestException {

		log.info("Trying to register user on clientName=" + clientName + " with username=" + username);

		// generate uuid
		String uuid = UUIDGenerator.getInstance().generateRandomBasedUUID().toString();

		String realm = getOpenSSORealm(clientName);
		Locale locale = I18nUtil.getLocaleFromString(localeString);

		if (log.isDebugEnabled()) {
			log.debug("registerAccount() entered, username=" + username + ", password=*****, displayName=" + displayName + ", firstname=" + firstname + ", lastname=" + lastname + ", locale=" + locale);
		}

		Account account = null;
		try {
			// add user to idm-backend
			account = createIdMUser(clientName, uuid, username, firstname, lastname, displayName, false);

			// add user to opensso
			String registerKey = registerOpenSSOUser(clientName, realm, username, password, username, uuid);
			if (registerKey != null)
				log.info("Successfully registered user on clientName=" + clientName + " with uuid=" + uuid + ", username=" + username);

			clientManagement.updateLastChangeUserList(clientName);
			
			return registerKey;
		} catch (Exception e) {
			log.error("doRegister() failed, login=" + username + ", password=*****, email=" + username + ", firstname=" + firstname + ", lastname=" + lastname + ", locale=" + locale, e);

			if (account != null) {
				try {
					deleteOpenSSOUser(clientName, uuid);
					// try to delete IdM user even if exception has been thrown
				} catch (Exception e2) {
					deleteIdMUser(uuid);
				}
			}

			if (e instanceof BackendException)
				throw (BackendException) e;
			else
				throw BackendException.UNKNOWN_ERROR_EXCEPTION;
		}
	}

	@Override
	@WebMethod
	public void setUserNames(@WebParam(name = "uuid") String uuid, @WebParam(name = "firstname") String firstname, @WebParam(name = "lastname") String lastname) throws BackendException, IllegalRequestException {

		User user = getUserByUuid(uuid);

		// saving names in idm
		try {
			boolean isFound = false;
			ValueSet name = null;
			List<ValueSet> values = privateDataManagement.getValues(uuid);
			for (ValueSet valueSet : values) {
				if (valueSet.getName().equals("personalData")) {
					if (valueSet.getValue("firstname") != null) {
						valueSet.getValue("firstname").setValue(firstname);
					} else {
						valueSet.addValue(new Value("firstname", firstname));
					}
					if (valueSet.getValue("lastname") != null) {
						valueSet.getValue("lastname").setValue(lastname);
					} else {
						valueSet.addValue(new Value("lastname", lastname));
					}
					name = valueSet;
					isFound = true;
					break;
				}
			}

			if (!isFound) {
				name = new ValueSet("personalData");
				name.addValue(new Value("firstname", firstname));
				name.addValue(new Value("lastname", lastname));
			}

			privateDataManagement.setValues(uuid, name, false);
			
			updateDisplayName(user.getClientName(), uuid, firstname, lastname);
			
		} catch (Exception e) {
			log.error("setUserNames() failed with setValues() in Idm for uuid=" + uuid, e);
			throw new BackendException(e.getLocalizedMessage());
		}
		
		clientManagement.updateLastChangeUserList(user.getClientName());
	}
	
	protected void updateDisplayName(String clientName, String uuid, String firstname, String lastname) throws BackendException, IllegalRequestException {
		ClientPropertyMap clientPropertyMap = clientManagement.getClientPropertyMap(clientName);
		String displayName;
		boolean displayNameGenerationEnabled = clientPropertyMap.getBoolean(ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED);
		String displayNameGenerationPattern = clientPropertyMap.get(ClientPropertyMap.KEY_REGISTRATION_DISPLAYNAMEGENERATION_PATTERN);
		if (displayNameGenerationEnabled) {
			displayName = displayNameGenerationPattern;
			displayName = displayName.replace("%firstname%", firstname);
			displayName = displayName.replace("%lastname%", lastname);
			
			Account account = accountManagement.getAccountByUuid(uuid);
			account.setDisplayName(displayName);
			accountManagement.setAccount(account.getClientName(), account);
		}
	}
	
	@Override
	@WebMethod
	public void setUserStatus(@WebParam(name = "uuid") String uuid, @WebParam(name = "status") String status) throws BackendException, IllegalRequestException {
		Account account = accountManagement.getAccountByUuid(uuid);
		User user = getUserByUuid(uuid);
		RestClient restClient = getRestClient(user.getClientName());

		String realm = user.getOpenSSORealm();

		UpdateUser updateUser = null;
		// search user
		try {
			Search search = restClient.callService(new Search(realm).setAttr(UserAttribute.USER_UUID, uuid));

			if (search.success() && search.getUids().size() == 1) {

				updateUser = new UpdateUser(realm, search.getUid());
				updateUser = restClient.callService(updateUser.setAttr(UserAttribute.USERSTATUS, status).setAttr(UserAttribute.REGISTER_UUID, ""));

				if (!updateUser.success()) {
					log.error("setAccountStatus() failed when update user, realm=" + realm + ", uuid=" + uuid + ", uid=" + search.getUid() + ", error=" + updateUser.getError());
					throw new BackendException(updateUser.getError());
				}
			}
		} catch (Exception e) {
			log.error("setAccountStatus() failed for realm=" + realm + ", uuid=" + uuid, e);
			throw new BackendException(e.getLocalizedMessage());
		}

		// update account in idm
		try {
			if (UserStatus.ACTIVE.equals(status)) {
				account.setSoftDelete(false);
			} else {
				account.setSoftDelete(true);
			}
			accountManagement.setAccount(account.getClientName(), account);
		} catch (Exception e) {
			// compensative handling:
			try {
				if (UserStatus.ACTIVE.equals(status)) {
					updateUser = restClient.callService(updateUser.setAttr(UserAttribute.USERSTATUS, UserStatus.INACTIVE));
				} else {
					updateUser = restClient.callService(updateUser.setAttr(UserAttribute.USERSTATUS, UserStatus.ACTIVE).setAttr(UserAttribute.REGISTER_UUID, ""));
				}
			} catch (IOException ioe) {
				log.error("setAccountStatus() failed with compensative handling, uuid=" + uuid, ioe);
				throw new BackendException(ioe.getLocalizedMessage());
			}
			log.error("setAccountStatus() failed with setAccount() uuid=" + uuid, e);
			throw new BackendException(e.getLocalizedMessage());
		}
	}

	@WebMethod
	@WebResult(name = "ssoToken")
	public String changeUsername(@WebParam(name = "uuid") String uuid, @WebParam(name = "newUsername") String newUsername, @WebParam(name = "currentPassword") String currentPassword) throws BackendException, IllegalRequestException {
		Account account = accountManagement.getAccountByUuid(uuid);
		User user = getUserByUuid(uuid);
		String realm = user.getOpenSSORealm();
		
		// check given user password
		AuthenticateUser authenticateUser;
		try {
			authenticateUser = getRestClient(user.getClientName()).callService(new AuthenticateUser(realm, user.getUsername(), currentPassword));
		} catch (IOException e) {
			throw BackendException.UNKNOWN_ERROR_EXCEPTION;
		}
        if (!authenticateUser.success() || authenticateUser.getToken() == null) {
        	throw IllegalRequestException.INVALID_CREDENTIALS_EXCEPTION;
        }
		
		deleteOpenSSOUser(user.getClientName(), uuid);
		createOpenSSOUser(user.getClientName(), newUsername, currentPassword, newUsername, uuid, false, realm);
		
		account.setUsername(newUsername);
		accountManagement.setAccount(account.getClientName(), account);
		
		return authenticateUser.getToken();
	}
	
	@WebMethod
	@WebResult(name = "activationKey")
	public String requestUsernameChange(@WebParam(name = "uuid") String uuid, @WebParam(name = "newUsername") String newUsername) throws BackendException, IllegalRequestException {
		User user = getUserByUuid(uuid);
		RestClient restClient = getRestClient(user.getClientName());
		String realm = user.getOpenSSORealm();
		
		/*
		 * generate activation uuid
		 */
		String activationUuid = UUIDGenerator.getInstance().generateRandomBasedUUID().toString();
		/*
		 * append newUsername to the activation uuid to ensure that this activation uuid can only
		 * be used for a username change to the verified newUsername
		 */
		activationUuid += DELIMITER_USERNAME_CHANGE_SPLIT_ACTIVATIONID_EMAIL + newUsername;

		UpdateUser updateUser = null;
		/*
		 * search user
		 */
		try {
			Search search = restClient.callService(new Search(realm).setAttr(UserAttribute.USER_UUID, uuid));

			if (search.success() && search.getUids().size() == 1) {

				updateUser = new UpdateUser(realm, search.getUid());
				updateUser = restClient.callService(updateUser.setAttr(UserAttribute.REGISTER_UUID, activationUuid));

				if (!updateUser.success()) {
					log.error("requestUsernameChange() failed when update user, realm=" + realm + ", uuid=" + uuid + ", uid=" + search.getUid() + ", error=" + updateUser.getError());
					throw new BackendException(updateUser.getError());
				}
			}
		} catch (Exception e) {
			log.error("requestUsernameChange() failed for realm=" + realm + ", uuid=" + uuid, e);
			throw new BackendException(e.getLocalizedMessage());
		}
		
		return activationUuid;
	}
}
