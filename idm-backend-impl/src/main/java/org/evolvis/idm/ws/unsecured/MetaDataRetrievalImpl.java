package org.evolvis.idm.ws.unsecured;

import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.jboss.wsf.spi.annotation.WebContext;

@Stateless 
@Local(MetaDataRetrieval.class)
@WebService(serviceName = "MetaDataRetrievalService", name = "MetaDataRetrievalServicePT", portName = "MetaDataRetrievalServicePort", targetNamespace = "http://idm.evolvis.org")
@WebContext(contextRoot = "idm-backend", urlPattern = "/MetaDataRetrievalService")
public class MetaDataRetrievalImpl implements MetaDataRetrieval {
	private ApplicationManagement applicationManagement;
	private AccountManagement accountManagement;
	private GroupAndRoleManagement groupAndRoleManagement;
	private OrganizationalUnitManagement organisationalUnitManagement;


	@EJB(name = "ejb/AccountManagementImpl")
	public void setAccountManagement(AccountManagement accountManagement) {
		this.accountManagement = accountManagement;
	}


	@EJB(name = "ejb/GroupAndRoleManagementImpl")
	public void setGroupAndRoleManagement(GroupAndRoleManagement groupAndRoleManagement) {
		this.groupAndRoleManagement = groupAndRoleManagement;
	}


	@EJB(name = "ejb/ApplicationManagementImpl")
	public void setApplicationManagement(ApplicationManagement applicationManagement) {
		this.applicationManagement = applicationManagement;
	}


	@EJB(name = "ejb/OrganizationalManagementImpl")
	public void setOrganisationalUnitManagement(OrganizationalUnitManagement organisationalUnitManagement) {
		this.organisationalUnitManagement = organisationalUnitManagement;
	}

	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getAccountByUUID(java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "account")
	public Account getAccountByUUID(@WebParam(name = "uuid") String uuid) throws BackendException {
		return accountManagement.getAccountByUuid(uuid);
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getAccountByUsername(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "account")
	public Account getAccountByUsername(@WebParam(name = "clientName") String clientName, @WebParam(name = "username") String username) throws BackendException {
		return accountManagement.getAccountByUsername(clientName, username);
	}
	
	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getAccountByCertificate(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "account")
	public Account getAccountByCertificate(@WebParam(name = "clientName") String clientName, @WebParam(name = "certificate") String certficate) throws BackendException {
		return null;
		//return accountManagement.getAccountByUsername(clientName, username);
	}
	
	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getCertificateByUUID(java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "certificate")
	public String getCertificateByUUID(@WebParam(name = "uuid") String uuid) throws BackendException {
		return null;
		//return accountManagement.getAccountByUsername(clientName, username);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getGroups(java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "groups")
	public List<Group> getGroups(@WebParam(name = "clientName") String clientName) throws BackendException {
		return groupAndRoleManagement.getGroups(clientName, null).getResultList();
	}

	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getGroupByName(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "group")
	public Group getGroupByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupName") String groupName) throws BackendException {
		return groupAndRoleManagement.getGroupByName(clientName, groupName);
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getGroupsByUser(java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "userGroups")
	public List<Group> getGroupsByUser(@WebParam(name = "uuid") String uuid) throws BackendException {
		return groupAndRoleManagement.getGroupsByUser(uuid);
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#isUserInGroup(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "userInGroup")
	public boolean isUserInGroup(@WebParam(name = "groupName") String groupName, @WebParam(name = "uuid") String uuid) throws BackendException {
		Client client = this.accountManagement.getClientByUuid(uuid);
		Group group = getGroupByName(client.getName(), groupName);
		return groupAndRoleManagement.isUserInGroup(uuid, group.getId());
	}

	//TODO rename or remove one of this two methods: "isUserInGroup"

//	@WebMethod
//	@WebResult(name = "userInGroup")
//	public boolean isUserInGroupById(@WebParam(name = "uuid") String uuid, @WebParam(name = "groupId") Long groupId) throws BackendException{
//		return this.groupAndRoleManagement.isUserInGroup(uuid, groupId);
//	}
	
//	/**
//	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#getGroup(Long)
//	 */
//	@WebMethod
//	@WebResult(name = "group")
//	public Group getGroupById(@WebParam(name = "groupId") Long groupId) throws BackendException {
//		return groupAndRoleManagement.getGroup(groupId);
//	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getUsersByGroup(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "users")
	public List<Account> getUsersByGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupName") String groupName) throws BackendException {
		return groupAndRoleManagement.getUsersByGroup(clientName, groupName);
	}

//	/**
//	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#getUsersByGroupId(Long)
//	 */
//	@WebMethod
//	@WebResult(name = "users")
//	public List<Account> getUsersByGroupId(@WebParam(name = "groupId") Long groupId) throws BackendException {
//		return groupAndRoleManagement.getUsersByGroupId(groupId);
//	}

	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getRoleByName(java.lang.String, java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "roles")
	public Role getRoleByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "roleName") String roleName) throws BackendException {
		return groupAndRoleManagement.getRoleByName(clientName, applicationName, roleName);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getRolesByApplication(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "roles")
	public List<Role> getRolesByApplication(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName) throws BackendException {
		return groupAndRoleManagement.getRolesByApplication(clientName, applicationName);
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getRolesByGroup(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "roles")
	public List<Role> getRolesByGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupName") String groupName) throws BackendException {
		return groupAndRoleManagement.getRolesByGroup(clientName, groupName, "default");
	}


//	/**
//	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#getRolesByGroupId(Long, String)
//	 */
//	@WebMethod
//	@WebResult(name = "roles")
//	public List<Role> getRolesByGroupId(@WebParam(name = "groupId") Long groupId) throws BackendException {
//		return this.groupAndRoleManagement.getRolesByGroupId(groupId, "default");
//	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getRolesByUser(java.lang.String, java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "roles")
	public List<Role> getRolesByUser(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid)
			throws BackendException {
		Long applicationId = applicationManagement.getApplicationByName(clientName, applicationName).getId();
		return groupAndRoleManagement.getRolesByUserAndApplication(uuid, applicationId, "default");
	}
	
	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#hasUserRole(java.lang.String, java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "hasUserRole")
	public boolean hasUserRole(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "roleName") String roleName) throws BackendException {
		Client client = this.accountManagement.getClientByUuid(uuid);
		Long roleId = getRoleByName(client.getName(), applicationName, roleName).getId();
		return groupAndRoleManagement.isUserInRoleById(uuid, roleId, "default");
	}

		
	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getUsersByRole(java.lang.String, java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "users")
	public List<Account> getUsersByRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "roleName") String roleName)
			throws BackendException {
		return groupAndRoleManagement.getUsersByRole(clientName, applicationName, roleName, "default");
	}

	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getOrgUnitByName(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "orgUnit")
	public OrgUnit getOrgUnitByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitName") String orgUnitName) throws BackendException {
		return this.organisationalUnitManagement.getOrgUnitByName(clientName, orgUnitName);
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getOrgUnitsByUser(java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "orgUnits")
	public List<OrgUnit> getOrgUnitsByUser(@WebParam(name = "uuid") String uuid) throws BackendException {
		return this.organisationalUnitManagement.getOrgUnitsByUser(uuid);
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getOrgUnitsByMetaDataValue(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "orgUnits")
	public List<OrgUnit> getOrgUnitsByMetaDataValue(@WebParam(name = "clientName") String clientName, @WebParam(name = "value") String value) throws BackendException {
		return this.organisationalUnitManagement.getOrgUnitsByValue(clientName, value);
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getOrganisationalUnits(java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "orgUnits")
	public List<OrgUnit> getOrganisationalUnits(@WebParam(name = "clientName") String clientName) throws BackendException {
		return this.organisationalUnitManagement.getOrgUnits(clientName, null).getResultList();
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getUsersByOrgUnit(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "users")
	public List<Account> getUsersByOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitName") String orgUnitName) throws BackendException {
		return this.organisationalUnitManagement.getUsersByOrgUnit(clientName, orgUnitName);
	}
	


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getUsersByOrgUnitAndRole(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "users")
	public List<Account> getUsersByOrgUnitAndRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "orgUnitName") String orgUnitName, @WebParam(name = "roleName") String roleName) throws BackendException{
		OrgUnit orgUnit = this.organisationalUnitManagement.getOrgUnitByName(clientName, orgUnitName);
		Role role = this.groupAndRoleManagement.getRoleByName(clientName, applicationName, roleName);
		return this.organisationalUnitManagement.getUsersByOrgUnitAndRole(clientName,orgUnit.getId(), role.getId(), "default");
	}

	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getDeactivatedUsersSince(java.lang.String, java.util.Calendar)
	 */
	@WebMethod
	@WebResult(name = "users")
	public List<Account> getDeactivatedUsersSince(@WebParam(name = "clientName") String clientName, @WebParam(name = "sinceDate") Calendar sinceDate) throws BackendException {
		return this.accountManagement.getDeactivatedAccountsSince(clientName, sinceDate);
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#isUserActive(java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "isUserActive")
	public boolean isUserActive(@WebParam(name = "uuid") String uuid) throws BackendException {
		return this.accountManagement.isDeactived(uuid);
	}

	
	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getApplicationByName(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "application")
	public Application getApplicationByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName) throws BackendException{
		return this.applicationManagement.getApplicationByName(clientName, applicationName);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.MetaDataRetrieval#getVersion()
	 */
	@WebMethod
	@WebResult(name = "version")
	public String getVersion() {
		return "1.1.0";
	}

}
