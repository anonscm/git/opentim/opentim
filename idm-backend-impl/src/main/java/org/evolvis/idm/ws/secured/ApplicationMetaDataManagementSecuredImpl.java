package org.evolvis.idm.ws.secured;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.identity.applicationdata.model.AppMetaData;
import org.evolvis.idm.identity.applicationdata.service.ApplicationMetaDataManagement;
import org.jboss.ws.annotation.EndpointConfig;
import org.jboss.wsf.spi.annotation.WebContext;

@Stateless
@WebService(serviceName = "ApplicationMetaDataManagementSecuredService", name = "ApplicationMetaDataManagementSecuredServicePT", portName = "ApplicationMetaDataManagementSecuredServicePort", targetNamespace = "http://idm.evolvis.org")
@WebContext(contextRoot = "idm-backend", urlPattern = "/ApplicationMetaDataManagementSecuredService")
@EndpointConfig(configName = "Standard WSSecurity Endpoint")
public class ApplicationMetaDataManagementSecuredImpl {

	ApplicationMetaDataManagement applicationMetaDataManagement;


	@EJB(name = "ejb/GroupAndRoleManagementImpl")
	public void setApplicationMetaDataManagement(ApplicationMetaDataManagement applicationMetaDataManagement) {
		this.applicationMetaDataManagement = applicationMetaDataManagement;
	}


	@WebMethod
	@WebResult(name = "appMetaData")
	public AppMetaData getApplicationMetaData(String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid) throws BackendException {
		return this.applicationMetaDataManagement.getApplicationMetaData(applicationName, uuid);
	}


	@WebMethod
	@WebResult(name = "appMetaDataValue")
	public String getApplicationMetaDataValue(String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "metaDataKey") String metaDataKey) throws BackendException {
		return this.applicationMetaDataManagement.getApplicationMetaDataValue(applicationName, uuid, metaDataKey);
	}


	@WebMethod
	@WebResult(name = "appMetaData")
	public AppMetaData setApplicationMetaData(@WebParam(name = "appMetaData") AppMetaData appMetaData) throws BackendException {
		return this.applicationMetaDataManagement.setApplicationMetaData(appMetaData);
	}


	@WebMethod
	@WebResult(name = "appMetaDataValue")
	public String setApplicationMetaDataValue(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "metaDataKey") String metaDataKey,
			@WebParam(name = "metaDataValue") String metaDataValue) throws BackendException {
		return this.setApplicationMetaDataValue(clientName, applicationName, uuid, metaDataKey, metaDataValue);
	}


	@WebMethod
	public void removeApplicationMetaDataValue(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "metaDataKey") String metaDataKey)
			throws BackendException {
		this.applicationMetaDataManagement.removeApplicationMetaDataValue(applicationName, uuid, metaDataKey);
	}


	@WebMethod
	public void removeApplicationMetaData(@WebParam(name = "metaDataId") Long metaDataId) throws BackendException {

	}


	@WebMethod
	@WebResult(name = "version")
	public String getVersion() {
		return null;
	}
}
