package org.evolvis.idm.ws.unsecured;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.model.RoleModelConfigurator;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.jboss.wsf.spi.annotation.WebContext;

@Stateless 
@Local(RoleManagement.class)
@WebService(serviceName = "RoleManagementService", name = "RoleManagementServicePT", portName = "RoleManagementServicePort", targetNamespace = "http://idm.evolvis.org")
@WebContext(contextRoot = "idm-backend", urlPattern = "/RoleManagementService")
public class RoleManagementImpl implements RoleManagement {
	
	private ApplicationManagement applicationManagement;

	@EJB(name = "ejb/ApplicationManagementImpl")
	public void setApplicationManagement(ApplicationManagement applicationManagement) {
		this.applicationManagement = applicationManagement;
	}

	private GroupAndRoleManagement groupAndRoleManagement;

	@EJB(name = "ejb/GroupAndRoleManagementImpl")
	public void setGroupAndRoleManagement(GroupAndRoleManagement groupAndRoleManagement) {
		this.groupAndRoleManagement = groupAndRoleManagement;
	}
	
	public RoleManagementImpl() {
		RoleModelConfigurator.setXmlTransientApplication(true);
	}

	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.RoleManagement#addGroupToRole(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@WebMethod
	public void addGroupToRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "groupName") String groupName, @WebParam(name = "roleName") String roleName) throws BackendException {
		Role role = this.groupAndRoleManagement.getRoleByName(clientName, applicationName, roleName);
		Group group = this.groupAndRoleManagement.getGroupByName(clientName, groupName);
		this.groupAndRoleManagement.addGroupToRole(clientName, group.getId(), role.getId(), "default");
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.RoleManagement#addUserToRole(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@WebMethod
	public void addUserToRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "roleName") String roleName) throws BackendException {
		Role role = groupAndRoleManagement.getRoleByName(clientName, applicationName, roleName);
		groupAndRoleManagement.addUserToRole(uuid, role.getId(), "default");
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.RoleManagement#removeGroupFromRole(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@WebMethod
	public void removeGroupFromRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "groupName") String groupName, @WebParam(name = "roleName") String roleName)
			throws BackendException {
		Role role = groupAndRoleManagement.getRoleByName(clientName, applicationName, roleName);
		Group group = this.groupAndRoleManagement.getGroupByName(clientName, groupName);
		this.groupAndRoleManagement.deleteGroupFromRole(clientName, group.getId(), role.getId(), "default");
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.RoleManagement#deleteRole(java.lang.String, java.lang.String, java.lang.String)
	 */
	@WebMethod
	public void deleteRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "roleName") String roleName) throws BackendException {
		Role role = this.groupAndRoleManagement.getRoleByName(clientName, applicationName, roleName);
		this.groupAndRoleManagement.deleteRole(clientName, role.getId());
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.RoleManagement#removeUserFromRole(java.lang.String, java.lang.String, java.lang.String)
	 */
	@WebMethod
	public void removeUserFromRole(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "roleName") String roleName) throws BackendException {
		String clientName = this.applicationManagement.getApplicationByUuid(uuid, applicationName).getClient().getName();
		if(clientName == null)
			throw IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION;
		Role role = this.groupAndRoleManagement.getRoleByName(clientName, applicationName, roleName);
		this.groupAndRoleManagement.deleteUserFromRole(uuid, role.getId(), "default");
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.RoleManagement#setRole(org.evolvis.idm.relation.permission.model.Role)
	 */
	@WebMethod
	@WebResult(name = "role")
	public Role setRole(@WebParam(name = "role") Role role) throws BackendException {
		return this.groupAndRoleManagement.setRole(role.getClientName(), role);
	}


	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.RoleManagement#getVersion()
	 */
	@WebMethod
	@WebResult(name = "version")
	public String getVersion() {
		return "1.1.0";
	}
}
