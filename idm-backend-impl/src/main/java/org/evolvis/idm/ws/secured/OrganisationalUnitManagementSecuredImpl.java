package org.evolvis.idm.ws.secured;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.jboss.ws.annotation.EndpointConfig;
import org.jboss.wsf.spi.annotation.WebContext;

@Stateless
@WebService(serviceName = "OrganisationalUnitManagementSecuredService", name = "OrganisationalUnitManagementSecuredServicePT", portName = "OrganisationalUnitManagementSecuredServicePort", targetNamespace = "http://idm.evolvis.org")
@WebContext(contextRoot = "idm-backend", urlPattern = "/OrganisationalUnitManagementSecuredService")
@EndpointConfig(configName = "Standard WSSecurity Endpoint")
public class OrganisationalUnitManagementSecuredImpl {
	@WebMethod
	public void deleteOrgUnit(@WebParam(name = "orgUnitId") Long orgUnitId)  throws BackendException{
	}

	@WebMethod
	@WebResult(name = "organisationalUnit")
	public OrgUnit getOrgUnit(@WebParam(name = "orgUnitId") Long orgUnitId)  throws BackendException{
		return null;
	}

	@WebMethod
	@WebResult(name = "organisationalUnits")
	public List<OrgUnit> getOrgUnitsByUser(@WebParam(name = "uuid") String uuid)  throws BackendException {
		return null;
	}

	@WebMethod
	@WebResult(name = "organisationalUnits")
	public List<OrgUnit> getOrganisationalUnits(@WebParam(name = "clientName") String clientName)  throws BackendException{
		// TODO Auto-generated method stub
		return null;
	}

	@WebMethod
	@WebResult(name = "users")
	public List<Account> getUsersByOrgUnit(@WebParam(name = "orgUnitId") Long orgUnitId)  throws BackendException{
		return null;
	}

	@WebMethod
	@WebResult(name = "organisationalUnit")
	public OrgUnit setOrgUnit(@WebParam(name = "orgUnit") OrgUnit orgUnit) {
		return null;
	}
	
	@WebMethod
	@WebResult(name = "version")
	public String getVersion() {
		return null;
	}
}
