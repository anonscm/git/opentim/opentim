package org.evolvis.idm.ws.secured;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.jboss.ws.annotation.EndpointConfig;
import org.jboss.wsf.spi.annotation.WebContext;

@Stateless
@WebService(serviceName = "GroupManagementService", name = "GroupManagementServicePT", portName = "GroupManagementServicePort", targetNamespace = "http://idm.evolvis.org")
@WebContext(contextRoot = "idm-backend", urlPattern = "/GroupManagementService")
@EndpointConfig(configName = "Standard WSSecurity Endpoint")
public class GroupManagementImpl {
	
	private GroupAndRoleManagement groupAndRoleManagement;
	
	@EJB(name = "ejb/GroupAndRoleManagementImpl")
	public void setGroupAndRoleManagement(GroupAndRoleManagement groupAndRoleManagement) {
		this.groupAndRoleManagement = groupAndRoleManagement;
	}
	
	@WebMethod
	public void addUserToGroup(@WebParam(name = "uuid") String uuid, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException {
		this.groupAndRoleManagement.addUserToGroup(uuid, groupId);
	}

	@WebMethod
	public void deleteGroup(@WebParam(name = "groupId") Long groupId) throws BackendException {
		//TODO Überarbeitung
		this.groupAndRoleManagement.deleteGroup(null, groupId);
	}

	@WebMethod
	public void removeUserFromGroup(@WebParam(name = "uuid") String uuid, @WebParam(name = "groupId") Long groupId) throws BackendException {
		this.groupAndRoleManagement.deleteUserFromGroup(uuid, groupId);
	}

//	@WebMethod
//	@WebResult(name = "group")
//	public Group getGroup(@WebParam(name = "groupId") Long groupId) throws BackendException {
//		return null;
//	}

	@WebMethod
	@WebResult(name = "groups")
	public List<Group> getGroups(@WebParam(name = "clientName") String clientName) throws BackendException {
		return this.groupAndRoleManagement.getGroups(clientName, null).getResultList();
	}

	@WebMethod
	@WebResult(name = "groups")
	public List<Group> getGroupsByUser(@WebParam(name = "clientName") String clientName, @WebParam(name = "uuid") String uuid) throws BackendException {
		return this.groupAndRoleManagement.getGroupsByUser(uuid);
	}

	@WebMethod
	@WebResult(name = "users")
	public List<Account> getUsersByGroup(@WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException {
		//TODO Überarbeitung
		return this.groupAndRoleManagement.getUsersByGroupId(null, groupId);
	}

	@WebMethod
	@WebResult(name = "isUserInGroup")
	public boolean isUserInGroup(@WebParam(name = "uuid") String uuid, @WebParam(name = "groupId") Long groupId) throws BackendException {
		return this.isUserInGroup(uuid, groupId);
	}

	@WebMethod
	@WebResult(name = "group")
	public Group setGroup(@WebParam(name = "group") Group group) throws BackendException, IllegalRequestException {
		return this.groupAndRoleManagement.setGroup(group.getClientName(), group);
	}

	@WebMethod
	@WebResult(name = "version")
	public String getVersion() {
		return null;
	}
}
