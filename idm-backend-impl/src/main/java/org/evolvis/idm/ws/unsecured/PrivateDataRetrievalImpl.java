package org.evolvis.idm.ws.unsecured;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.jboss.wsf.spi.annotation.WebContext;

@Stateless 
@Local(PrivateDataRetrieval.class)
@WebService(serviceName = "PrivateDataRetrievalService", name = "PrivateDataRetrievalServicePT", portName = "PrivateDataRetrievalServicePort", targetNamespace = "http://idm.evolvis.org")
@WebContext(contextRoot = "idm-backend", urlPattern = "/PrivateDataRetrievalService")
//@EndpointConfig(configName = "Standard WSSecurity Endpoint")
public class PrivateDataRetrievalImpl implements PrivateDataRetrieval {
	private PrivateDataManagement privateDataManagement;

	@EJB(name = "ejb/PrivateDataManagementImpl")
	public void setPrivateDataRetrieval(PrivateDataManagement privateDataManagement) {
		this.privateDataManagement = privateDataManagement;
	}
	
	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.PrivateDataRetrieval#getAttributes(java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "attributes")
	public List<AttributeGroup> getAttributes(@WebParam(name = "clientName") String clientName) throws BackendException {
		return privateDataManagement.getAttributeGroups(clientName);
	}
	
	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.PrivateDataRetrieval#getValues(java.lang.String, java.lang.String)
	 */
	@WebMethod
	@WebResult(name = "values")
	public List<ValueSet> getValues(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid) throws BackendException {
		return privateDataManagement.getValues(uuid);
	}
	
	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.PrivateDataRetrieval#getValuesById(java.lang.String, java.lang.String, java.lang.Long)
	 */
	@WebMethod
	@WebResult(name = "valueSet")
	public ValueSet getValuesById(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "id") Long id) throws BackendException {
		return privateDataManagement.getValuesById(uuid, id);
	}
	
	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.PrivateDataRetrieval#getValuesByAttributes(java.lang.String, java.lang.String, java.util.List)
	 */
	@WebMethod
	@WebResult(name = "values")
	public List<ValueSet> getValuesByAttributes(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "attributes") List<AttributeGroup> attributes) throws BackendException {
		return privateDataManagement.getValuesByAttributes(uuid, attributes);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.PrivateDataRetrieval#getValueWithBinaryAttachmentById(java.lang.String, java.lang.Long)
	 */
	@WebMethod
	@WebResult(name = "value")
	public Value getValueWithBinaryAttachmentById(@WebParam(name = "uuid") String uuid, @WebParam(name = "valueId") Long valueId) throws BackendException, IllegalRequestException{
		return privateDataManagement.getValueWithBinaryAttachmentById(uuid, valueId);
	}
	
	
	/* (non-Javadoc)
	 * @see org.evolvis.idm.ws.unsecured.PrivateDataRetrieval#getVersion()
	 */
	@WebMethod
	@WebResult(name = "version")
	public String getVersion() {
		return "1.1.0";
	}

	@WebMethod
	@WebResult(name = "attributes")
	public AttributeGroup getAttributeGroupByName(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "attributeGroupName") String attributeGroupName)
			throws BackendException {
		List<AttributeGroup> attributes = this.getAttributes(clientName);
		if(attributes != null){
			for (AttributeGroup attributeGroup : attributes) {
				if (attributeGroup.getName().equals(attributeGroupName)) {
					return attributeGroup;
				}				
			}
		}
		return null;
	}
}
