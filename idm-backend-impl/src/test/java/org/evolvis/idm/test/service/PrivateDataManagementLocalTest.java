package org.evolvis.idm.test.service;

import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.junit.Test;

public class PrivateDataManagementLocalTest extends PrivateDataManagementAbstractTest {
	@Override
	protected PrivateDataManagement getService() {
		return new ServiceProviderLocal().getPrivateDataManagementService();
//		return getServiceProvider().getPrivateDataManagementService();
	}

	protected ServiceProvider serviceProvider;
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderLocal();
		return serviceProvider;
	}
	
	/**
	 * Test to fix Eclipse JUnit bug with abstract test cases
	 */
	@Test
	public void workaroundTest() {
		// Nothing.
	}
}
