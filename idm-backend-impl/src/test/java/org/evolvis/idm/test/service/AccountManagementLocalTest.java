package org.evolvis.idm.test.service;

import org.evolvis.idm.identity.account.service.AccountManagement;


public class AccountManagementLocalTest extends AccountManagementAbstractTest {
	
	protected ServiceProvider serviceProvider;
	
	protected AccountManagement getService() {
		return getServiceProvider().getAccountManagementService();
	}

	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderLocal();
		return serviceProvider;
	}

	
}
