package org.evolvis.idm.test.service;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;

import org.evolvis.idm.common.service.AbstractBaseServiceImpl;
import org.evolvis.idm.common.util.PersistenceUtil;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.identity.applicationdata.service.ApplicationMetaDataManagement;
import org.evolvis.idm.identity.privatedata.service.PrivateDataConfiguration;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement;
import org.evolvis.idm.synchronization.service.DirectoryServiceSynchronization;


public class ServiceProviderLocal implements ServiceProvider {
	protected ApplicationManagement applicationManagementService;
	protected ClientManagement clientManagement;
	protected GroupAndRoleManagement groupAndRoleMangagement;
	protected InternalRoleManagement internalRoleManagement;
	protected InternalRoleScopeManagement internalRoleScopeManagement;
	protected PrivateDataConfiguration privateDataConfiguration;
	protected PrivateDataManagement privateDataManagement;
	protected AccountManagement accountManagement;
	protected OrganizationalUnitManagement organisationalUnitManagement;
	protected ApplicationMetaDataManagement applicationMetaDataManagement;
	protected DirectoryServiceSynchronization directoryServiceSynchronization;
	protected UserManagement userManagement;
	protected Context jndiContext;
	
	public ServiceProviderLocal() {
		Properties p = new Properties();
        p.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.LocalInitialContextFactory");
        
        p.setProperty("openejb.altdd.prefix", "test");
        
        p.setProperty("openejb.deployments.classpath.include", ".*idm.*");
        //p.setProperty("openejb.deployments.classpath.exclude", ".*persistence.*");
        
        //p.setProperty("openejb.validation.output.level", "VERBOSE");
		
        p.setProperty("idm-backend-live-0.hibernate.hbm2ddl.auto", "create-drop");
        p.setProperty("idm-backend-live-1.hibernate.hbm2ddl.auto", "create-drop");
        p.setProperty("idm-backend-live-0.hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
        p.setProperty("idm-backend-live-1.hibernate.dialect", "org.hibernate.dialect.HSQLDialect");

    	InitialContext localContext = null;
		try {
			localContext = new InitialContext(p);
		} catch (NamingException e) {
			e.printStackTrace();
		}
    	setJndiContext(localContext);
	}
	
	/**
	 * @see org.evolvis.idm.test.service.ServiceProvider#getAccountManagementService()
	 */
	public AccountManagement getAccountManagementService() {
		if(accountManagement == null) {
			accountManagement = (AccountManagement) jndiLookup("AccountManagementImplLocal");
		}
		return this.accountManagement;
	}
	
	
	/**
	 * @see org.evolvis.idm.test.service.ServiceProvider#getApplicationManagementService()
	 */
	public ApplicationManagement getApplicationManagementService() {
		if (applicationManagementService == null) {
			applicationManagementService = (ApplicationManagement) jndiLookup("ApplicationManagementImplLocal");
		}
		return applicationManagementService;
	}
	
	@Override
	public ApplicationMetaDataManagement getApplicationMetaDataManagementService(){
		if (applicationMetaDataManagement == null) {
			applicationMetaDataManagement = (ApplicationMetaDataManagement) jndiLookup("ApplicationMetaDataManagementImplLocal");
		}
		return applicationMetaDataManagement;
	}

	/**
	 * @see org.evolvis.idm.test.service.ServiceProvider#getClientManagementService()
	 */
	public ClientManagement getClientManagementService() {
		if (clientManagement == null) {
			clientManagement = (ClientManagement) jndiLookup("ClientManagementImplLocal");
		}
		return clientManagement;
	}
	
	
	
	
	@Override
	public DirectoryServiceSynchronization getDirectoryServiceSynchronizationService() {
		if (directoryServiceSynchronization == null) {
			directoryServiceSynchronization = (DirectoryServiceSynchronization) jndiLookup("DirectoryServiceSynchronizationImplLocal");
		}
		return directoryServiceSynchronization;
	}
	
	
	/**
	 * @see org.evolvis.idm.test.service.ServiceProvider#getGroupAndRoleManagementService()
	 */
	public GroupAndRoleManagement getGroupAndRoleManagementService(){
		if(groupAndRoleMangagement == null) {
			groupAndRoleMangagement = (GroupAndRoleManagement) jndiLookup("GroupAndRoleManagementImplLocal");
		}
		return groupAndRoleMangagement;
	}


	/**
	 * @see org.evolvis.idm.test.service.ServiceProvider#getInternalRoleManagementService()
	 */
	public InternalRoleManagement getInternalRoleManagementService() {
		if(internalRoleManagement == null){
			internalRoleManagement = (InternalRoleManagement) jndiLookup("InternalRoleManagementImplLocal");
		}
		return internalRoleManagement;
	}
	
	/**
	 * @see org.evolvis.idm.test.service.ServiceProvider#getInternalRoleScopeManagementService()
	 */
	public InternalRoleScopeManagement getInternalRoleScopeManagementService(){
		if(internalRoleScopeManagement == null) {
			internalRoleScopeManagement = (InternalRoleScopeManagement) jndiLookup("InternalRoleScopeManagementImplLocal");
		}
		return internalRoleScopeManagement;
	}
	
	
	public Context getJndiContext() {
		return jndiContext;
	}

	
	/**
	 * @see org.evolvis.idm.test.service.ServiceProvider#getOrganisationalUnitManagementService()
	 */
	public OrganizationalUnitManagement getOrganisationalUnitManagementService(){
		if(organisationalUnitManagement == null) {
			organisationalUnitManagement = (OrganizationalUnitManagement) jndiLookup("OrganizationalUnitManagementImplLocal");
		}
		return organisationalUnitManagement;
	}


	/**
	 * @see org.evolvis.idm.test.service.ServiceProvider#getPrivateDataConfigurationService()
	 */
	public PrivateDataConfiguration getPrivateDataConfigurationService() {
		if (privateDataConfiguration == null) {
			privateDataConfiguration = (PrivateDataConfiguration) jndiLookup("PrivateDataConfigurationImplLocal");
		}
		return privateDataConfiguration;
	}
	
	/**
	 * @see org.evolvis.idm.test.service.ServiceProvider#getPrivateDataManagementService()
	 */
	public PrivateDataManagement getPrivateDataManagementService() {
		if (privateDataManagement == null) {
			privateDataManagement = (PrivateDataManagement) jndiLookup("PrivateDataManagementImplLocal");
		}
		return privateDataManagement;
	}
	
	//@Override
	public UserManagement getUserManagementService() {
		if (userManagement == null) {
			userManagement = (UserManagement) jndiLookup("UserManagementImplLocal");
		}
		return userManagement;
	}

	protected void injectEntityManager(AbstractBaseServiceImpl service) {
		EntityManager entityManager = PersistenceUtil.createEntityManager();
		EntityManager[] entityManagers = {entityManager, entityManager};
		service.setEntityManager(entityManagers);
		service.setContainerManagedTransactionDisabled(true);
	}

	protected Object jndiLookup(String uri) {
		try {
			return getJndiContext().lookup(uri);
		} catch (NamingException e) {			
			e.printStackTrace();
		}
		return null;
	}

	public void setJndiContext(Context jndiContext) {
		this.jndiContext = jndiContext;
	}
}
