package org.evolvis.idm.test.service;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import junit.framework.Assert;

import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;

public class LdapSearchTest {

//	@Test
	public void testLdapSearchSubtree() {
		ClientPropertyMap clientProperties = new ClientPropertyMap();
		
//		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_HOSTANDPORT, "windows-ad.qs.tarent.de:389");
//		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_BASEDN, "dc=qs,dc=tarent,dc=de");
//		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_USERDN, "CN=Administrator,CN=Users,dc=qs,dc=tarent,dc=de");
//		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_PASSWORD, "tarentinerCC");
//		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHDN, "");
//		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHFILTER, "(&(objectClass=person)(cn=jens.neumaier@tarent.de))");
		
		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_HOSTANDPORT, "localhost:389");
		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_BASEDN, "dc=verwalt-berlin,dc=de ");
		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_USERDN, "cn=eudlrldap,cn=users,dc=metavz,dc=verwalt-berlin,dc=de");
		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_PASSWORD, "$DuR#12#");
		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHDN, "dc=metavz");
		clientProperties.put(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHFILTER, "(&(objectClass=person)(mail=Claudia.Keller@senwtf.berlin.de))");
		
		String hostAndPort = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_HOSTANDPORT);
		String baseDN = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_BASEDN);
		String userDN = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_USERDN);
		String password = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_PASSWORD);
		String searchDN = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHDN);
		String searchFilter = clientProperties.get(ClientPropertyMap.KEY_LDAP_SYNC_SEARCHFILTER);

		Hashtable<String, String> hstEnvironment = new Hashtable<String, String>();

		hstEnvironment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

		// windows-ad.qs.tarent.de:389
		// dc=qs,dc=tarent,dc=de
		hstEnvironment.put(Context.PROVIDER_URL, "ldap://" + hostAndPort + "/" + baseDN);

		hstEnvironment.put(Context.SECURITY_AUTHENTICATION, "simple");
		// Benutzername
		// CN=Administrator,CN=Users,dc=qs,dc=tarent,dc=de
		hstEnvironment.put(Context.SECURITY_PRINCIPAL, userDN);
		// Passwort
		// tarentinerCC
		hstEnvironment.put(Context.SECURITY_CREDENTIALS, password);

		try {
			DirContext oContext = new InitialDirContext(hstEnvironment);
			
			SearchControls searchControls = new SearchControls();
			searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			
			// SearchDN: e.g. cn=Users
			// SearchFilter: e.g. (&(objectClass=person)(postalCode=555555))
			NamingEnumeration<SearchResult> searchResults = oContext.search(searchDN, searchFilter,
					searchControls);

			Assert.assertTrue(searchResults.hasMore());
			
			SearchResult searchResult = searchResults.next();
			
			System.out.println(searchResult.getName());
			System.out.println(searchResult);
			
			System.out.println(searchResult.getAttributes().get("givenname").get());
			
		} catch (Exception e) {
			Assert.fail();
		}
	}
}
