package org.evolvis.idm.test.service;

import org.evolvis.idm.identity.privatedata.service.PrivateDataConfiguration;
import org.junit.Test;

public class PrivateDataConfigurationLocalTest extends PrivateDataConfigurationAbstractTest {
	@Override
	protected PrivateDataConfiguration getService() {
		return getServiceProvider().getPrivateDataConfigurationService();
	}

	protected ServiceProvider serviceProvider;
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderLocal();
		return serviceProvider;
	}
	
	/**
	 * Test to fix Eclipse JUnit bug with abstract test cases
	 */
	@Test
	public void workaroundTest() {
		// Nothing.
	}
}
