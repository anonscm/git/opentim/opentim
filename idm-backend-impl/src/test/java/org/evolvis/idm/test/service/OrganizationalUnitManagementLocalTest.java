/**
 * 
 */
package org.evolvis.idm.test.service;

import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;

/**
 * @author Yorka Neumann, tarent GmbH
 * 
 */
public class OrganizationalUnitManagementLocalTest extends OrganizationalUnitManagementAbstractTest {

	protected ServiceProvider serviceProvider;


	/**
	 * @see org.evolvis.idm.test.service.AbstractBaseServiceAbstractTest#getService()
	 */
	protected OrganizationalUnitManagement getService() {
		return getServiceProvider().getOrganisationalUnitManagementService();
	}


	/**
	 * @see org.evolvis.idm.test.service.AbstractBaseServiceAbstractTest#getServiceProvider()
	 */
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderLocal();
		return serviceProvider;
	}

	

	
	
}
