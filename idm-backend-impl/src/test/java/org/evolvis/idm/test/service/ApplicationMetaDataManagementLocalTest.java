package org.evolvis.idm.test.service;

import org.evolvis.idm.identity.applicationdata.service.ApplicationMetaDataManagement;

public class ApplicationMetaDataManagementLocalTest extends ApplicationMetaDataManagementAbstractTest {

	@Override
	protected ApplicationMetaDataManagement getService() {
		return getServiceProvider().getApplicationMetaDataManagementService();
	}

	
	protected ServiceProvider serviceProvider;

	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderLocal();
		return serviceProvider;
	}

}
