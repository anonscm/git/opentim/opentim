package org.evolvis.idm.test.service;


import org.evolvis.idm.relation.permission.service.InternalRoleManagement;

public class InternalRoleManagementLocalTest extends InternalRoleManagementAbstractTest {

	
	protected InternalRoleManagement getService() {
		return getServiceProvider().getInternalRoleManagementService();
	}


	protected ServiceProvider serviceProvider;
		
	
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderLocal();
		return serviceProvider;
	}
	
/*	@Test
	public void removeUserFromInternalRole() throws Exception{
		//TODO: solving the problem that this test fails local but not remote!
		//do nothing 
	}*/

}
