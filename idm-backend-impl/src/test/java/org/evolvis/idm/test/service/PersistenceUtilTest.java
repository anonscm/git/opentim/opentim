//package org.evolvis.idm.test.service;
//
//import javax.persistence.EntityManager;
//
//import org.evolvis.idm.common.util.PersistenceUtil;
//import org.evolvis.idm.relation.multitenancy.model.Application;
//import org.evolvis.idm.relation.multitenancy.model.Client;
//import org.junit.Assert;
//
//public class PersistenceUtilTest {
//	private EntityManager entityManager;
//	
////	@Before
//	public void before() {
//		entityManager = PersistenceUtil.createEntityManager();
//	}
//	
////	@After
//	public void after() {
//		if (entityManager != null) {
//			entityManager.close();
//			entityManager = null;
//		}
//	}
//	
////	@Test
//	public void testDatabase() {
//		Long currentTime = System.currentTimeMillis();
//		Client client1 = new Client(getClass().getSimpleName() + "_Client_1"+currentTime, getClass().getName() + "_Client_1"+currentTime);
//		Application application1 = new Application(getClass().getSimpleName() + "_1"+currentTime, getClass().getName() + "_1"+currentTime);
//		Application application2 = new Application(getClass().getSimpleName() + "_2"+currentTime, getClass().getName() + "_2"+currentTime);
//		application1.setClient(client1);
//		application2.setClient(client1);
//		
//		Client client2 = new Client(getClass().getSimpleName() + "_Client_2"+currentTime, getClass().getName() + "_Client_2"+currentTime);
//		Application application3 = new Application(getClass().getSimpleName() + "_3"+currentTime, getClass().getName() + "_3"+currentTime);
//		Application application4 = new Application(getClass().getSimpleName() + "_4"+currentTime, getClass().getName() + "_4"+currentTime);
//		application3.setClient(client2);
//		application4.setClient(client2);
//		
//		entityManager.getTransaction().begin();
//		
//		entityManager.persist(client1);
//		entityManager.persist(client2);
//		
//		entityManager.persist(application1);
//		entityManager.persist(application2);
//		entityManager.persist(application3);
//		entityManager.persist(application4);
//		
//		entityManager.getTransaction().commit();
//		
//		Assert.assertNotNull(client1.getId());
//		Assert.assertNotNull(client2.getId());
//		
//		Assert.assertNotNull(application1.getId());
//		Assert.assertNotNull(application2.getId());
//		Assert.assertNotNull(application3.getId());
//		Assert.assertNotNull(application4.getId());
//		
//		client1 = entityManager.getReference(Client.class, client1.getId());
//		client2 = entityManager.getReference(Client.class, client2.getId());
//		
//		application1 = entityManager.getReference(Application.class, application1.getId());
//		application2 = entityManager.getReference(Application.class, application2.getId());
//		application3 = entityManager.getReference(Application.class, application3.getId());
//		application4 = entityManager.getReference(Application.class, application4.getId());
//
//		Assert.assertNotNull(client1);
//		Assert.assertNotNull(client2);
//		
//		Assert.assertNotNull(application1);
//		Assert.assertNotNull(application2);
//		Assert.assertNotNull(application3);
//		Assert.assertNotNull(application4);
//	}
//}
