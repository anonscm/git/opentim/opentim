package org.evolvis.idm.test.service;

import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;

public class GroupAndRoleManagementLocalTest extends GroupAndRoleManagementAbstractTest {

	@Override
	protected GroupAndRoleManagement getService() {
		return getServiceProvider().getGroupAndRoleManagementService();
	}


	protected ServiceProvider serviceProvider;
		
	@Override
	protected ServiceProvider getServiceProvider() {
		if (serviceProvider == null)
			serviceProvider = new ServiceProviderLocal();
		return serviceProvider;
	}
	
	/**
	 * Override local test of entity UserRole
	 */
	public void getUserAccountByGroupIdTest() throws Exception {
		
	}
}
