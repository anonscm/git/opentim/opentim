/**
 * 
 */
package org.evolvis.idm.test.service;

import java.text.DecimalFormat;

import junit.framework.Assert;

import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.model.UserPropertyFields;
import org.evolvis.idm.identity.account.model.UserSearchableFields;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.junit.Test;

/**
 * @author Yorka Neumann
 * 
 */
public abstract class UserManagementAbstractTest extends AbstractBaseServiceAbstractTest<UserManagement> {

	String errorPrefix = "UserManagementAbstractTest";

	@Test
	public void getUserByUuid() throws Exception {
		Client client = this.getCurrentClient(true);
		Account account = this.getCurrentAccount(client, true);

		ValueSet testAccountName = new ValueSet("personalData");
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));
		this.getServiceProvider().getPrivateDataManagementService().setValues(account.getUuid(), testAccountName, false);
		long begin = System.currentTimeMillis();
		User user = getService().getUserByUuid(account.getUuid());
		long end = System.currentTimeMillis();
		double duration = (end - begin) / 1000;
		DecimalFormat money = new DecimalFormat("0.00");
		System.err.println("Duration: " + money.format(duration));
		Assert.assertNotNull(user);
		Assert.assertEquals(account.getId(), user.getId());
		Assert.assertEquals("Peter", user.getFirstname());
		Assert.assertEquals("Silie", user.getLastname());
	}

	
	@Test
	public void getUsers() throws Exception {
		Client client = getCurrentClient(true);
		// Client client = this.getServiceProvider().getClientManagementService().getClientByName("stage3.6.tarent.buero");
		Account account1 = getCurrentAccount(client, true);
		Account account2 = getCurrentAccount(client, true);
		/* create third account without first and last name to be able to control that such will not appear in the list of users */
		getCurrentAccount(client, true);

		/* associate first and last name to the first two accounts */
		ValueSet testAccountName = new ValueSet("personalData");
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));
		this.getServiceProvider().getPrivateDataManagementService().setValues(account1.getUuid(), testAccountName, false);
		this.getServiceProvider().getPrivateDataManagementService().setValues(account2.getUuid(), testAccountName, false);
		QueryDescriptor queryDescriptor = new QueryDescriptor();

		BeanOrderProperty orderProperty = null;

		// if(sortOrder != null && sortOrder.equals("status-ASC")) {
		// orderProperty = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_SOFTDELETE, false);
		// } else if (sortOrder != null && sortOrder.equals("status-DESC")) {
		// orderProperty = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_SOFTDELETE, true);
		// } else if (sortOrder != null && sortOrder.equals("login-DESC")) {
		// orderProperty = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_USERNAME, false);
		// } else {
		orderProperty = new BeanOrderProperty(User.class, UserPropertyFields.FIELD_PATH_USERNAME, true);
		// }

		// queryDescriptor.setLimit(10);

		// if (offset != null) {
		// page = page != null ? page : new Integer(0);
		// queryDescriptor.setOffSet(offset[page]);
		// } else {
		queryDescriptor.setOffSet(0);
		// }

		queryDescriptor.addOrderProperty(orderProperty);

//		BeanSearchProperty searchPropertyAll = null;
//		BeanSearchProperty searchPropertyStatus = null;

		// if(search != null) {
		// searchPropertyAll = new BeanSearchProperty(Account.class, AccountPropertyFields.ALL, search);
		// queryDescriptor.addSearchProperty(searchPropertyAll);
		// }

		// if (statusFilter != null && !statusFilter.equals("all")) {
		// searchPropertyStatus = new BeanSearchProperty(Account.class, AccountPropertyFields.FIELD_PATH_SOFTDELETE, "active".equals(statusFilter) ? "false" : "true");
		// queryDescriptor.addSearchProperty(searchPropertyStatus);
		// }

//		long begin = System.currentTimeMillis();
		QueryResult<User> users = getService().getUsers(client.getName(), queryDescriptor);
//		long end = System.currentTimeMillis();
//		double duration = (end - begin) / 1000;
//		DecimalFormat money = new DecimalFormat("0.00");
//		System.err.println("Duration: " + money.format(duration));
		Assert.assertEquals(2, users.size());
		findIdInList(users.getResultList(), account1.getId());
		findIdInList(users.getResultList(), account2.getId());
	}
	
	
	
	@Test
	public void getUsersWithQueryDescriptor() throws Exception {
		Client client = getCurrentClient(true);
		// Client client = this.getServiceProvider().getClientManagementService().getClientByName("stage3.6.tarent.buero");
		Account account1 = getCurrentAccount(client, true);
		Account account2 = getCurrentAccount(client, true);
		/* create third account without first and last name to be able to control that such will not appear in the list of users */
		getCurrentAccount(client, true);

		/* associate first and last name to the first two accounts */
		ValueSet testAccountName = new ValueSet("personalData");
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));
		this.getServiceProvider().getPrivateDataManagementService().setValues(account1.getUuid(), testAccountName, false);
		this.getServiceProvider().getPrivateDataManagementService().setValues(account2.getUuid(), testAccountName, false);
		QueryDescriptor queryDescriptor = new QueryDescriptor();

		BeanOrderProperty orderProperty = null;

		// if(sortOrder != null && sortOrder.equals("status-ASC")) {
		// orderProperty = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_SOFTDELETE, false);
		// } else if (sortOrder != null && sortOrder.equals("status-DESC")) {
		// orderProperty = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_SOFTDELETE, true);
		// } else if (sortOrder != null && sortOrder.equals("login-DESC")) {
		// orderProperty = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_USERNAME, false);
		// } else {
		orderProperty = new BeanOrderProperty(User.class, UserPropertyFields.FIELD_PATH_SOFTDELETE, true);
		// }

		// queryDescriptor.setLimit(10);

		// if (offset != null) {
		// page = page != null ? page : new Integer(0);
		// queryDescriptor.setOffSet(offset[page]);
		// } else {
		queryDescriptor.setOffSet(0);
		// }

		queryDescriptor.addOrderProperty(orderProperty);

		String search = "Pe";
//		BeanSearchProperty searchPropertyStatus = new BeanSearchProperty(User.class, UserPropertyFields.ALL, search);
//		queryDescriptor.addSearchProperty(searchPropertyStatus);

		
		BeanSearchProperty searchPropertySoftDelete  = new BeanSearchProperty(User.class, UserSearchableFields.FIELD_PATH_SOFTDELETE, "false");


		

		BeanSearchProperty searchPropertyUsername = new BeanSearchProperty(User.class,
		UserSearchableFields.FIELD_PATH_USERNAME, search);
		queryDescriptor.addSearchProperty(searchPropertyUsername);
		BeanSearchProperty searchPropertyDisplayName = new BeanSearchProperty(User.class,
		UserSearchableFields.FIELD_PATH_DISPLAYNAME, search);
		queryDescriptor.addSearchProperty(searchPropertyDisplayName);
		
		
		
		BeanSearchProperty searchPropertyFirstname = new BeanSearchProperty(User.class,
		UserSearchableFields.FIELD_PATH_FIRSTNAME, search);
		queryDescriptor.addSearchProperty(searchPropertyFirstname);
		BeanSearchProperty searchPropertyLastname = new BeanSearchProperty(User.class,
		UserSearchableFields.FIELD_PATH_LASTNAME, search);
		queryDescriptor.addSearchProperty(searchPropertyLastname);
		queryDescriptor.addSearchProperty(searchPropertySoftDelete);

		QueryResult<User> users = getService().getUsers(client.getName(), queryDescriptor);

		Assert.assertEquals(2, users.size());
		findIdInList(users.getResultList(), account1.getId());
		findIdInList(users.getResultList(), account2.getId());
	}

	@Test
	public void isUserExisting() throws Exception {
		Client client = getCurrentClient(true);
		Account account = getCurrentAccount(client, true);
		Assert.assertTrue(getService().isUserExisting(client.getClientName(), account.getUsername()));
	}
}
