/**
 * 
 */
package org.evolvis.idm.test.service;

import java.util.List;

import junit.framework.Assert;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalProperty;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.organization.model.OrgUnitMap;
import org.evolvis.idm.relation.organization.model.OrgUnitMapEntry;
import org.evolvis.idm.relation.organization.model.OrgUnitMapEntryPropertyFields;
import org.evolvis.idm.relation.organization.model.OrgUnitMapEntryQueryResult;
import org.evolvis.idm.relation.organization.model.OrgUnitPropertyFields;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.GroupPropertyFields;
import org.evolvis.idm.relation.permission.model.Role;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Yorka Neumann, tarent GmbH
 * 
 */
public abstract class OrganizationalUnitManagementAbstractTest extends AbstractBaseServiceAbstractTest<OrganizationalUnitManagement> {

	public Client client = null;

	@Before
	public void init() throws Exception {
		client = this.getCurrentClient(true);
	}

	/**
	 * Tries to persist an organisational unit for a group
	 */
	@Test
	public void setOrgUnit() throws Exception {
		OrgUnit orgUnit;
		Group group = this.getCurrentGroup(client, true);
		group.setClient(client);
		try {
			// first persist an org. unit without a meta data map.

			orgUnit = new OrgUnit();
			orgUnit.setGroup(group);
			orgUnit.setName("default-orgUnit" + System.currentTimeMillis());
			orgUnit.setDisplayName(orgUnit.getName());
			orgUnit = this.getService().setOrgUnit(client.getClientName(), orgUnit);
			Assert.assertNotNull(orgUnit);
			Assert.assertNotNull(orgUnit.getId());
		} catch (Exception e) {
			throw e;
		}
		try {
			// now append a meta data map and persist the change
			OrgUnitMap orgUnitMap = orgUnit.getOrgUnitMap();
			if (orgUnitMap != null) {
				orgUnitMap.put("Test2Key", "Test2Value");
				orgUnit = this.getService().setOrgUnit(client.getClientName(), orgUnit);
				Assert.assertNotNull(orgUnit);
				Assert.assertNotNull(orgUnit.getId());
				Assert.assertNotNull(orgUnit.getOrgUnitMap().getId());
			} else
				Assert.fail("Associated Map is null!");
		} catch (Exception e) {
			throw e;
		}
		try {
			// Now try to persist an org. unit directly with a meta data map.
			orgUnit = new OrgUnit();
			orgUnit.setGroup(group);

			orgUnit.setName("default-orgUnit-" + System.currentTimeMillis());
			orgUnit.setDisplayName(orgUnit.getName());
			// now append a meta data map and persist the change

			OrgUnitMap orgUnitMap = orgUnit.getOrgUnitMap();
			if (orgUnitMap != null) {
				orgUnitMap.put("Test2Key", "Test2Value");
				orgUnit = this.getService().setOrgUnit(client.getClientName(), orgUnit);
			} else
				Assert.fail("Associated Map is null!");
			Assert.assertNotNull(orgUnit);
			Assert.assertNotNull(orgUnit.getId());
			Assert.assertNotNull(orgUnit.getOrgUnitMap().getId());

		} catch (Exception e) {
			throw e;
		}
		try {
			/* persist and remove an org unit map entry */ 
			OrgUnitMap orgUnitMap = orgUnit.getOrgUnitMap();
			orgUnitMap.put("Test2Key", "Test2-2Value");
			orgUnitMap.put("Test3Key", "Test3Value");
			orgUnit = this.getService().setOrgUnit(client.getClientName(), orgUnit);
			Assert.assertEquals(2, orgUnit.getOrgUnitMap().size());
			Assert.assertTrue(orgUnit.getOrgUnitMap().get("Test2Key").equals("Test2-2Value"));
			String controllValue = orgUnit.getOrgUnitMap().remove("Test3Key");
			Assert.assertNotNull(controllValue);
			Assert.assertEquals(1, orgUnit.getOrgUnitMap().size());
			orgUnit.setGroup(group);
			orgUnit = this.getService().setOrgUnit(client.getClientName(), orgUnit);
			Assert.assertEquals(1, orgUnit.getOrgUnitMap().size());

			/*now change the value of one map entry*/
			String controlValue = orgUnit.getOrgUnitMap().get("Test2Key");
			Assert.assertNotNull(controlValue);
			String controlValue2 = orgUnit.getOrgUnitMap().put("Test2Key", "NEWVALUE");
			Assert.assertEquals(controlValue, controlValue2);
			orgUnit = this.getService().setOrgUnit(client.getName(), orgUnit);
			Assert.assertEquals("NEWVALUE", orgUnit.getOrgUnitMap().get("Test2Key"));
			orgUnit = this.getService().getOrgUnit(client.getName(), orgUnit.getId());
			Assert.assertEquals("NEWVALUE", orgUnit.getOrgUnitMap().get("Test2Key"));
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void setOrgUnitWithEmptyValueEntry() throws Exception{
		OrgUnit orgUnit;
		Group group = this.getCurrentGroup(client, true);
		group.setClient(client);
		
		orgUnit = new OrgUnit();
		orgUnit.setGroup(group);
		orgUnit.setName("default-orgUnit-EmptyValue-Entry" + System.currentTimeMillis());
		orgUnit.setDisplayName(orgUnit.getName());
		orgUnit = this.getService().setOrgUnit(client.getClientName(), orgUnit);
		Assert.assertNotNull(orgUnit);
		Assert.assertNotNull(orgUnit.getId());
		
		orgUnit.getOrgUnitMap().put("Key1", "");
		orgUnit.getOrgUnitMap().put("Key2", "Value2");
		orgUnit.getOrgUnitMap().put("Key3", "Value3");
		orgUnit.getOrgUnitMap().put("Key4", "Value4");
		try{
			orgUnit = this.getService().setOrgUnit(client.getClientName(), orgUnit);
			Assert.assertNotNull(orgUnit);
			Assert.assertNotNull(orgUnit.getOrgUnitMap());
			Assert.assertEquals(4,orgUnit.getOrgUnitMap().size());
			Assert.assertEquals("",orgUnit.getOrgUnitMap().get("Key1"));
			Assert.assertEquals("Value2", orgUnit.getOrgUnitMap().get("Key2"));
			Assert.assertEquals("Value3", orgUnit.getOrgUnitMap().get("Key3"));
			Assert.assertEquals("Value4", orgUnit.getOrgUnitMap().get("Key4"));
			
			String controlString = this.getService().removeValueFromOrgUnit(client.getName(), orgUnit.getId(),"Key1");
			//TODO check if it is ok that e.g. oracle delivers null and postgresql an empty string
			Assert.assertTrue(controlString == null || controlString.equals(""));
			orgUnit = this.getService().getOrgUnit(client.getName(), orgUnit.getId());
			Assert.assertEquals(3,orgUnit.getOrgUnitMap().size());
			Assert.assertEquals("Value2", orgUnit.getOrgUnitMap().get("Key2"));
			Assert.assertEquals("Value3", orgUnit.getOrgUnitMap().get("Key3"));
			Assert.assertEquals("Value4", orgUnit.getOrgUnitMap().get("Key4"));
			
		} catch(Exception e){
			throw e;
		}
	}
	
	
	@Test
	public void setAndChangeOrgUnit() throws Exception {
		Client client = this.getCurrentClient(true);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		OrgUnit orgUnit1 = this.getCurrentOrgUnit(group1, true);
		OrgUnit orgUnit2 = this.getCurrentOrgUnit(group2, true);
		try{
			/*First try to change some parameter of the org unit */
			String newSuffix = "-new";
			orgUnit1.setDisplayName(orgUnit1.getDisplayName()+newSuffix);
			orgUnit1.setName(orgUnit1.getName()+newSuffix);
			this.getService().setOrgUnit(client.getClientName(), orgUnit1);
			
		} catch(Exception e){
			throw e;
		}
		try{
			orgUnit1.setName(orgUnit2.getName());
			this.getService().setOrgUnit(client.getName(), orgUnit1);
			Assert.fail("No exception has been thrown");
		} catch(IllegalRequestException e){
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
	}

	@Test
	public void setDuplicateOrgUnit() throws Exception {
		Client client = this.getCurrentClient(true);
		OrgUnit orgUnit;
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		group1.setClient(client);
		group2.setClient(client);
		try {
			// first persist an org. unit without a meta data map.
	
			orgUnit = new OrgUnit();
			orgUnit.setGroup(group1);
			orgUnit.setName("default-orgUnit" + System.currentTimeMillis());
			orgUnit.setDisplayName(orgUnit.getName());
			orgUnit.setDisplayName(orgUnit.getName());
			orgUnit = this.getService().setOrgUnit(client.getClientName(), orgUnit);
			Assert.assertNotNull(orgUnit);
			Assert.assertNotNull(orgUnit.getId());
	
			OrgUnit orgUnitDuplicate = new OrgUnit();
			orgUnitDuplicate.setName(orgUnit.getName());
			orgUnitDuplicate.setDisplayName(orgUnitDuplicate.getName());
			orgUnitDuplicate.setGroup(group2);
	
			orgUnitDuplicate = this.getService().setOrgUnit(client.getClientName(), orgUnitDuplicate);
	
			Assert.fail("Expected Exception has not been thrown!");
	
		} catch (BackendException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void setIllegalOrgUnit() throws Exception {
		Group group = this.getCurrentGroup(client, true);
		group.setClient(client);
		try {
			// Group group = new Group();
			// group.setName("bad-group");

			OrgUnit orgUnit = new OrgUnit();
			// orgUnit.setGroup(group);
			// orgUnit.setName("bad-orgUnit");
			this.getService().setOrgUnit(client.getClientName(), orgUnit);
		} catch (IllegalRequestException e) {
			Assert.assertNotNull(e.getIllegalProperties());
			Assert.assertEquals(2, e.getIllegalProperties().size());
			for (IllegalProperty illegalProperty : e.getIllegalProperties()) {
				Assert.assertEquals(OrgUnit.class.getName(), illegalProperty.getBeanName());
				if (!(illegalProperty.getPropertyName().equals("name")) && !(illegalProperty.getPropertyName().equals("displayName")))
					Assert.fail();
			}
		}
		/* Now test to insert two org. units with the same name */
		try {
			OrgUnit orgUnit1 = new OrgUnit();
			OrgUnit orgUnit2 = new OrgUnit();
			orgUnit1.setGroup(group);
			orgUnit2.setGroup(group);
			orgUnit1.setName("duplicateOrgUnit-Test");
			orgUnit1.setDisplayName(orgUnit1.getName());
			orgUnit2.setName("duplicateOrgUnit-Test");
			orgUnit2.setDisplayName(orgUnit2.getName());
			this.getService().setOrgUnit(client.getClientName(), orgUnit1);
			this.getService().setOrgUnit(client.getClientName(), orgUnit2);
			Assert.fail("No expected Exception has been thrown");

		} catch(IllegalRequestException e){
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
		 catch (BackendException e) {
			// for the remote testing hibernate context.
			if (e.getMessage().contains("org.hibernate.exception.ConstraintViolationException: could not insert: [org.evolvis.idm.test.relation.organization.model.OrgUnit]"))
				;
			return;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * 
	 */
	@Test
	public void deleteOrgUnit() throws Exception {
		Group group = this.getCurrentGroup(client, true);
		group.setClient(client);
		OrgUnit orgUnit = this.getCurrentOrgUnit(group, true);
		try {
			this.getService().deleteOrgUnitById(client.getClientName(), orgUnit.getId());
			orgUnit = this.getCurrentOrgUnit(group, true);
			orgUnit.setGroup(group);
			this.getService().deleteOrgUnit(client.getClientName(), orgUnit);
			orgUnit = this.getService().getOrgUnit(client.getClientName(), orgUnit.getId());
			Assert.assertNull(orgUnit);

		} catch (Exception e) {
			throw e;
		}
		try {
			orgUnit = new OrgUnit();
			orgUnit.setId(new Long(-1));
			this.getService().deleteOrgUnit(client.getClientName(), orgUnit);
			Assert.fail("Expected Backend-Exception has not been thrown.");
		} catch (BackendException e) {
			if (!e.getMessage().equals(IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION.getMessage()))
				Assert.fail("Thrown Backend-Exception is not the expected one :" + e.getMessage());
		} catch (Exception e) {
			Assert.fail("Thrown Exception is not a BackEndException");
		}
		try {
			orgUnit = this.getCurrentOrgUnit(group, true);
			orgUnit.getOrgUnitMap().put("TestKEY", "TestVALUE");
			orgUnit.getOrgUnitMap().put("TestKEY2", "TESTVALUE2");
			orgUnit = this.getService().setOrgUnit(client.getClientName(), orgUnit);
			Assert.assertNotNull(orgUnit.getOrgUnitMap().getId());
			for (OrgUnitMapEntry mapEntry : orgUnit.getOrgUnitMap().getMapEntries()) {
				Assert.assertNotNull(mapEntry.getId());
			}

			this.getService().deleteOrgUnit(client.getClientName(), orgUnit);
		} catch (Exception e) {
			throw e;
		}

	}

	/**
	 * 
	 */
	@Test
	public void getOrgUnit() throws Exception {
		Client client = this.getCurrentClient(true);
		try {
			// query the "empty" list of org. units

			List<OrgUnit> resultList = this.getService().getOrgUnits(client.getName(), null).getResultList();

			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			// now a filled list
			Group group = this.getCurrentGroup(client, true);
			group.setClient(client);
			OrgUnit orgUnit1 = this.getCurrentOrgUnit(group, true);
			OrgUnit orgUnit2 = this.getCurrentOrgUnit(group, true);

			resultList = this.getService().getOrgUnits(client.getName(), null).getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			boolean condition1 = resultList.get(0).getName().equals(orgUnit1.getName()) && resultList.get(1).getName().equals(orgUnit2.getName());
			boolean condition2 = resultList.get(0).getName().equals(orgUnit2.getName()) && resultList.get(1).getName().equals(orgUnit1.getName());
			Assert.assertTrue(condition1 | condition2);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * 
	 */
	@Test
	public void getUsersByOrgUnitAndGroup() throws Exception {

		Group group = this.getCurrentGroup(client, true);
		group.setClient(client);
		OrgUnit orgUnit = this.getCurrentOrgUnit(group, true);
		Account account1 = this.getCurrentAccount(client, true);
		Account account2 = this.getCurrentAccount(client, true);

		try {
			this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account1.getUuid(), group.getId());
			this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account2.getUuid(), group.getId());

			List<Account> resultList = this.getService().getUsersByOrgUnitAndGroupById(client.getClientName(), orgUnit.getId(), group.getId());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			boolean condition1 = resultList.get(0).getUuid().equals(account1.getUuid()) & resultList.get(1).getUuid().equals(account2.getUuid());
			boolean condition2 = resultList.get(1).getUuid().equals(account1.getUuid()) & resultList.get(0).getUuid().equals(account2.getUuid());
			Assert.assertTrue(condition1 | condition2);

			resultList = this.getService().getUsersByOrgUnitAndGroup(client.getName(), group.getName(), orgUnit.getName());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			Assert.assertTrue(condition1 | condition2);
		} catch (Exception e) {
			throw e;
		}

	}

	@Test
	public void getOrgUnitsByUser() throws Exception {

		Client client = this.getCurrentClient(false);
		Account account1 = this.getCurrentAccount(client, true);
		Account account2 = this.getCurrentAccount(client, true);
		Account account3 = this.getCurrentAccount(client, true);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);

		OrgUnit orgUnit1 = new OrgUnit();
		orgUnit1.setGroup(group1);
		orgUnit1.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1 = this.getService().setOrgUnit(client.getClientName(), orgUnit1);

		OrgUnit orgUnit2 = new OrgUnit();
		orgUnit2.setGroup(group2);
		orgUnit2.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit2.setDisplayName(orgUnit2.getName());
		orgUnit2.setDisplayName(orgUnit2.getName());
		orgUnit2 = this.getService().setOrgUnit(client.getClientName(), orgUnit2);

		List<OrgUnit> resultList;

		try {
			resultList = this.getService().getOrgUnitsByUser(account1.getUuid());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());
		} catch (Exception e) {
			throw e;
		}

		this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account1.getUuid(), group1.getId());
		this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account2.getUuid(), group1.getId());
		this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account1.getUuid(), group2.getId());
		this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account3.getUuid(), group2.getId());

		try {
			resultList = this.getService().getOrgUnitsByUser(account1.getUuid());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, orgUnit1.getId());
			this.findIdInList(resultList, orgUnit2.getId());

			resultList = this.getService().getOrgUnitsByUser(account2.getUuid());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(1, resultList.size());
			this.findIdInList(resultList, orgUnit1.getId());

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getOrgUnitByName() throws Exception {
		Client client = this.getCurrentClient(false);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		group1.setClient(client);
		group2.setClient(client);

		OrgUnit orgUnit1 = new OrgUnit();
		orgUnit1.setGroup(group1);
		orgUnit1.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1 = this.getService().setOrgUnit(client.getClientName(), orgUnit1);

		OrgUnit orgUnit2 = new OrgUnit();
		orgUnit2.setGroup(group2);
		orgUnit2.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit2.setDisplayName(orgUnit2.getName());
		orgUnit2.setDisplayName(orgUnit2.getName());
		orgUnit2 = this.getService().setOrgUnit(client.getClientName(), orgUnit2);
		// first query for an existing org. unit
		try {
			OrgUnit resultOrgUnit = this.getService().getOrgUnitByName(client.getName(), orgUnit1.getName());
			Assert.assertNotNull(resultOrgUnit);
			Assert.assertTrue(orgUnit1.getName().equals(resultOrgUnit.getName()));
		} catch (Exception e) {
			throw e;
		}
		// now query for a not existing org. unit.
		try {
			OrgUnit orgUnit = this.getService().getOrgUnitByName(client.getName(), "NotExisting-OrgUnit");
			Assert.assertNull(orgUnit);
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getUsersByOrgUnit() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account1 = this.getCurrentAccount(client, true);
		Account account2 = this.getCurrentAccount(client, true);
		Account account3 = this.getCurrentAccount(client, true);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		group1.setClient(client);
		group2.setClient(client);

		OrgUnit orgUnit1 = new OrgUnit();
		orgUnit1.setGroup(group1);
		orgUnit1.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1 = this.getService().setOrgUnit(client.getClientName(), orgUnit1);

		OrgUnit orgUnit2 = new OrgUnit();
		orgUnit2.setGroup(group2);
		orgUnit2.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit2.setDisplayName(orgUnit2.getName());
		orgUnit2.setDisplayName(orgUnit2.getName());
		orgUnit2 = this.getService().setOrgUnit(client.getClientName(), orgUnit2);
		try {
			List<Account> resultList = this.getService().getUsersByOrgUnit(client.getName(), orgUnit1.getName());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			resultList = this.getService().getUsersByOrgUnitAndGroup(client.getName(), group1.getName(), orgUnit1.getName());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			resultList = this.getService().getUsersByOrgUnitId(client.getClientName(), orgUnit1.getId());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account1.getUuid(), group1.getId());
			this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account2.getUuid(), group1.getId());
			this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account1.getUuid(), group2.getId());
			this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account3.getUuid(), group2.getId());

			resultList = this.getService().getUsersByOrgUnit(client.getName(), orgUnit1.getName());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, account1.getId());
			this.findIdInList(resultList, account2.getId());

			resultList = this.getService().getUsersByOrgUnitId(client.getClientName(), orgUnit1.getId());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, account1.getId());
			this.findIdInList(resultList, account2.getId());

			resultList = this.getService().getUsersByOrgUnitAndGroup(client.getName(), group1.getName(), orgUnit1.getName());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, account1.getId());
			this.findIdInList(resultList, account2.getId());

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getOrgUnitsByGroup() throws Exception {
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		group1.setClient(client);
		group2.setClient(client);
		OrgUnit orgUnit1 = new OrgUnit();
		orgUnit1.setGroup(group1);
		orgUnit1.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1 = this.getService().setOrgUnit(client.getClientName(), orgUnit1);

		OrgUnit orgUnit2 = new OrgUnit();
		orgUnit2.setGroup(group1);
		orgUnit2.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit2.setDisplayName(orgUnit2.getName());
		orgUnit2.setDisplayName(orgUnit2.getName());
		orgUnit2 = this.getService().setOrgUnit(client.getClientName(), orgUnit2);

		List<OrgUnit> resultList;

		try {
			resultList = this.getService().getOrgUnitsByGroup(client.getClientName(), group1.getId());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());

			resultList = this.getService().getOrgUnitsByGroup(client.getClientName(), group2.getId());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			OrgUnit orgUnit3 = new OrgUnit();
			orgUnit3.setGroup(group2);
			orgUnit3.setName("OrgUnit" + System.currentTimeMillis());
			orgUnit3.setDisplayName(orgUnit3.getName());
			orgUnit3.setDisplayName(orgUnit3.getName());
			orgUnit3 = this.getService().setOrgUnit(client.getClientName(), orgUnit3);

			resultList = this.getService().getOrgUnitsByGroup(client.getClientName(), group2.getId());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(1, resultList.size());

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getOrgUnitsByValue() throws Exception {
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		group1.setClient(client);
		group2.setClient(client);
		Long currentTime = System.currentTimeMillis();
		String value1 = "TestValue" + currentTime;
		currentTime++;
		String value2 = "TestValue" + currentTime;
		String value3 = "NotPersistentValue";

		OrgUnit orgUnit1 = new OrgUnit();
		orgUnit1.setGroup(group1);
		orgUnit1.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.getOrgUnitMap().put("TestKey1-1", value1);
		orgUnit1.getOrgUnitMap().put("TestKey1-2", value2);
		orgUnit1 = this.getService().setOrgUnit(client.getClientName(), orgUnit1);

		OrgUnit orgUnit2 = new OrgUnit();
		orgUnit2.setGroup(group1);
		orgUnit2.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit2.setDisplayName(orgUnit2.getName());
		orgUnit2.setDisplayName(orgUnit2.getName());
		orgUnit2.getOrgUnitMap().put("TestKey1-2", value1);
		orgUnit2 = this.getService().setOrgUnit(client.getClientName(), orgUnit2);

		OrgUnit orgUnit3 = new OrgUnit();
		orgUnit3.setGroup(group2);
		orgUnit3.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit3.setDisplayName(orgUnit3.getName());
		orgUnit3.setDisplayName(orgUnit3.getName());
		orgUnit3.getOrgUnitMap().put("TestKey1-1", value2);
		orgUnit3 = this.getService().setOrgUnit(client.getClientName(), orgUnit3);

		List<OrgUnit> resultList;

		try {
			resultList = this.getService().getOrgUnitsByValue(client.getName(), value1);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, orgUnit1.getId());
			this.findIdInList(resultList, orgUnit2.getId());

			resultList = this.getService().getOrgUnitsByValue(client.getName(), value3);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getOrgUnitValueMap() throws Exception {
		Group group1 = this.getCurrentGroup(client, true);
		group1.setClient(client);
		Long currentTime = System.currentTimeMillis();
		String value1 = "TestValue" + currentTime;
		currentTime++;
		String value2 = "TestValue" + currentTime;
		String value3 = "NotPersistentValue";
		String testKey1 = "TestKey-1-1";
		String testKey2 = "TestKey-1-2";

		OrgUnit orgUnit1 = new OrgUnit();
		orgUnit1.setGroup(group1);
		orgUnit1.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.getOrgUnitMap().put(testKey1, value1);
		orgUnit1.getOrgUnitMap().put(testKey2, value2);
		orgUnit1 = this.getService().setOrgUnit(client.getClientName(), orgUnit1);

		OrgUnit orgUnit2 = new OrgUnit();
		orgUnit2.setGroup(group1);

		orgUnit2.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit2.setDisplayName(orgUnit2.getName());

		OrgUnitMap orgUnitMap;
		try {
			// for a not persistent orgunit
			try {
				orgUnitMap = this.getService().getOrgUnitMap(client.getClientName(), orgUnit2.getId(), null);
				Assert.assertNotNull(orgUnitMap);
				Assert.assertEquals(0, orgUnitMap.size());
			} catch (BackendException e) {
				if (!e.getMessage().contains(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage()))
					Assert.fail("The thrown exception is not the expected one");
			}
			// for a persistent org unit but without values;
			orgUnit2 = this.getService().setOrgUnit(client.getClientName(), orgUnit2);
			orgUnitMap = this.getService().getOrgUnitMap(client.getClientName(), orgUnit2.getId(), null);
			Assert.assertNotNull(orgUnitMap);
			Assert.assertEquals(0, orgUnitMap.size());

			// for a persistent org unit but with values;
			orgUnitMap = this.getService().getOrgUnitMap(client.getClientName(), orgUnit1.getId(), null);
			Assert.assertNotNull(orgUnitMap);
			Assert.assertEquals(2, orgUnitMap.size());
			Assert.assertTrue(orgUnitMap.containsKey(testKey1));
			Assert.assertTrue(orgUnitMap.containsKey(testKey2));
			Assert.assertTrue(orgUnitMap.containsValue(value1));
			Assert.assertTrue(orgUnitMap.containsValue(value2));
			Assert.assertFalse(orgUnitMap.containsValue(value3));

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getOrgUnitValue() throws Exception {
		Group group1 = this.getCurrentGroup(client, true);
		group1.setClient(client);

		Long currentTime = System.currentTimeMillis();
		String value1 = "TestValue" + currentTime;
		currentTime++;
		String value2 = "TestValue" + currentTime;
		String testKey1 = "TestKey-1-1";
		String testKey2 = "TestKey-1-2";

		OrgUnit orgUnit1 = new OrgUnit();
		orgUnit1.setGroup(group1);
		orgUnit1.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.getOrgUnitMap().put(testKey1, value1);
		orgUnit1.getOrgUnitMap().put(testKey2, value2);
		orgUnit1 = this.getService().setOrgUnit(client.getClientName(), orgUnit1);

		try {
			String controlValue = this.getService().getValueFromOrgUnit(client.getClientName(), orgUnit1.getId(), testKey1);
			Assert.assertNotNull(controlValue);
			Assert.assertTrue(value1.equals(controlValue));

			controlValue = this.getService().getValueFromOrgUnit(client.getClientName(), orgUnit1.getId(), "NotPersistentKey");
			Assert.assertNull(controlValue);
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void addValueToOrgUnit() throws Exception {
		Group group1 = this.getCurrentGroup(client, true);
		group1.setClient(client);
		Long currentTime = System.currentTimeMillis();
		String value1 = "TestValue" + currentTime;
		currentTime++;
		String value2 = "TestValue" + currentTime;

		String testKey1 = "TestKey-1-1";
		String testKey2 = "TestKey-1-2";

		OrgUnit orgUnit1 = new OrgUnit();
		orgUnit1.setGroup(group1);
		orgUnit1.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.getOrgUnitMap().put(testKey1, value1);
		orgUnit1.getOrgUnitMap().put(testKey2, value2);
		orgUnit1 = this.getService().setOrgUnit(client.getClientName(), orgUnit1);

		try {
			String controlValue = this.getService().addValueToOrgUnit(client.getClientName(), orgUnit1.getId(), "TestKey-1-3", "TestValue-1-3");
			Assert.assertNull(controlValue);
			controlValue = this.getService().getValueFromOrgUnit(client.getClientName(), orgUnit1.getId(), "TestKey-1-3");
			Assert.assertNotNull(controlValue);
			Assert.assertTrue(controlValue.equals("TestValue-1-3"));

			controlValue = this.getService().addValueToOrgUnit(client.getClientName(), orgUnit1.getId(), testKey1, "TestValue-X");
			Assert.assertNotNull(controlValue);
			Assert.assertTrue(controlValue.equals(value1));
			controlValue = this.getService().getValueFromOrgUnit(client.getClientName(), orgUnit1.getId(), testKey1);
			Assert.assertNotNull(controlValue);
			Assert.assertTrue(controlValue.equals("TestValue-X"));

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void removeValueFromOrgUnit() throws Exception {
		Group group1 = this.getCurrentGroup(client, true);
		group1.setClient(client);
		Long currentTime = System.currentTimeMillis();
		String value1 = "TestValue" + currentTime;
		currentTime++;
		String value2 = "TestValue" + currentTime;
		String testKey1 = "TestKey-1-1";
		String testKey2 = "TestKey-1-2";

		OrgUnit orgUnit1 = new OrgUnit();
		orgUnit1.setGroup(group1);
		orgUnit1.setName("OrgUnit" + System.currentTimeMillis());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.setDisplayName(orgUnit1.getName());
		orgUnit1.getOrgUnitMap().put(testKey1, value1);
		orgUnit1.getOrgUnitMap().put(testKey2, value2);
		orgUnit1 = this.getService().setOrgUnit(client.getClientName(), orgUnit1);

		try {
			String result = this.getService().removeValueFromOrgUnit(client.getClientName(), orgUnit1.getId(), "NotPersistentKey");
			Assert.assertNull(result);
			result = this.getService().removeValueFromOrgUnit(client.getClientName(), orgUnit1.getId(), testKey1);
			Assert.assertNotNull(result);
			Assert.assertTrue(result.equals(value1));
			String controlValue = this.getService().getValueFromOrgUnit(client.getClientName(), orgUnit1.getId(), testKey1);
			Assert.assertNull(controlValue);
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getGroupByOrgUnitId() throws Exception {
		Group group = this.getCurrentGroup(client, true);
		group.setClient(client);
		OrgUnit orgUnit = this.getCurrentOrgUnit(group, true);
		try {
			Group controlGroup = this.getService().getGroupByOrgUnitId(client.getClientName(), orgUnit.getId());
			Assert.assertNotNull(controlGroup);
			Assert.assertNotNull(controlGroup.getId());
			Assert.assertTrue(controlGroup.getId().equals(group.getId()));

			controlGroup = this.getService().getGroupByOrgUnit(client.getName(), orgUnit.getName());
			Assert.assertNotNull(controlGroup);
			Assert.assertNotNull(controlGroup.getId());
			Assert.assertTrue(controlGroup.getId().equals(group.getId()));
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getUsersByOrgUnitAndRole() throws Exception {
		Client client = this.getCurrentClient(true);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		group1.setClient(client);
		group2.setClient(client);
		Application application = this.getCurrentApplication(client, true);
		Role role = this.getCurrentRole(application, true);
		OrgUnit orgUnit = this.getCurrentOrgUnit(group1, true);
		Account account1 = this.getCurrentAccount(client, true);
		Account account2 = this.getCurrentAccount(client, true);
		Account account3 = this.getCurrentAccount(client, true);
		Account account4 = this.getCurrentAccount(client, true);
		String scope = "TestScope";
		try {

			this.getServiceProvider().getGroupAndRoleManagementService().addUserToRole(account1.getUuid(), role.getId(), scope);
			this.getServiceProvider().getGroupAndRoleManagementService().addGroupToRole(client.getClientName(), group2.getId(), role.getId(), scope);
			this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account4.getUuid(), group2.getId());

			this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account1.getUuid(), group1.getId());
			this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account2.getUuid(), group1.getId());
			this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account3.getUuid(), group1.getId());
			this.getServiceProvider().getGroupAndRoleManagementService().addUserToGroup(account4.getUuid(), group1.getId());
			// first test if account1 will be return if the group is not
			// assigned to the role
			List<Account> resultList = this.getService().getUsersByOrgUnitAndRole(client.getClientName(), orgUnit.getId(), role.getId(), scope);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, account1.getId());
			this.findIdInList(resultList, account4.getId());
			// now test if all accounts will be return if the group is assigned
			// to the role
			this.getServiceProvider().getGroupAndRoleManagementService().addGroupToRole(client.getClientName(), group1.getId(), role.getId(), scope);
			resultList = this.getService().getUsersByOrgUnitAndRole(client.getClientName(), orgUnit.getId(), role.getId(), scope);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(4, resultList.size());
			this.findIdInList(resultList, account1.getId());
			this.findIdInList(resultList, account2.getId());
			this.findIdInList(resultList, account3.getId());
			this.findIdInList(resultList, account4.getId());
		} catch (Exception e) {
			throw e;
		}

		try {
			this.getService().getUsersByOrgUnitAndRole(client.getClientName(), null, role.getId(), scope);
		} catch (IllegalRequestException e) {
			Assert.assertTrue(e.getMessage().contains(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage()));
		}
		try {
			this.getService().getUsersByOrgUnitAndRole(client.getClientName(), orgUnit.getId(), null, scope);
		} catch (IllegalRequestException e) {
			Assert.assertTrue(e.getMessage().contains(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage()));
		}
	}

	@Test
	public void getSortedOrgUnitMapEntries() throws Exception {
		/*the scene: a client, a group and an org unit*/
		Group group = this.getCurrentGroup(client, true);
		group.setClient(client);
		OrgUnit orgUnit = this.getCurrentOrgUnit(group, true);
		/*test if querying the map entries of an empty org. unit map delivers the org unit and org unit map*/
		//TODO überarbeiten:
		OrgUnitMapEntryQueryResult queryResult = this.getService().getOrgUnitMapEntries(client.getClientName(), orgUnit.getId(), null);
		//Assert.assertEquals(orgUnit, queryResult.getOrgUnit());
		//Assert.assertEquals(orgUnit.getOrgUnitMap(), queryResult.getOrgUnitMap());
		
		/*now add key/value pairs and persist the change*/
		orgUnit.getOrgUnitMap().put("Key1", "Value1");
		orgUnit.getOrgUnitMap().put("Key2", "Value2");
		orgUnit = this.getService().setOrgUnit(client.getClientName(), orgUnit);
		QueryDescriptor queryDescriptor = new QueryDescriptor();
		BeanOrderProperty orderProperty = new BeanOrderProperty(OrgUnitMapEntry.class, OrgUnitMapEntryPropertyFields.FIELD_PATH_ORGUNITMAPENTRY_KEY, true);
		queryDescriptor.addOrderProperty(orderProperty);
		queryResult = this.getService().getOrgUnitMapEntries(client.getClientName(), orgUnit.getId(), queryDescriptor);
		List<OrgUnitMapEntry> mapEntries = queryResult.getResultList();
		Assert.assertNotNull(mapEntries);
		Assert.assertEquals(2, mapEntries.size());
		Assert.assertEquals("Key1", mapEntries.get(0).getKey());
		Assert.assertEquals("Key2", mapEntries.get(1).getKey());
		// TODO s.o: Assert.assertEquals(orgUnit, queryResult.getOrgUnit());
		//Assert.assertEquals(orgUnit.getOrgUnitMap(), queryResult.getOrgUnitMap());

		orderProperty.setOrderAscending(false);
		queryResult = this.getService().getOrgUnitMapEntries(client.getClientName(), orgUnit.getId(), queryDescriptor);
		mapEntries = queryResult.getResultList();
		Assert.assertNotNull(mapEntries);
		Assert.assertEquals(2, mapEntries.size());
		Assert.assertEquals("Key2", mapEntries.get(0).getKey());
		Assert.assertEquals("Key1", mapEntries.get(1).getKey());
		//TODO s.o.Assert.assertEquals(orgUnit, queryResult.getOrgUnit());
		//Assert.assertEquals(orgUnit.getOrgUnitMap(), queryResult.getOrgUnitMap());
	}


	@Test
	public void TestgetMapEntriesPaging() throws Exception {
		Group group = this.getCurrentGroup(client, true);
		group.setClient(client);
		OrgUnit orgUnit = this.getCurrentOrgUnit(group, true);
		for (int i = 0; i < 50; i++) {
			orgUnit.getOrgUnitMap().put("Key" + i, "Value" + i);
		}
		orgUnit = this.getService().setOrgUnit(client.getClientName(), orgUnit);
		QueryDescriptor queryDescriptor = new QueryDescriptor();
		queryDescriptor.setLimit(10);
		queryDescriptor.setOffSet(10);
		OrgUnitMapEntryQueryResult mapEntries = this.getService().getOrgUnitMapEntries(client.getClientName(), orgUnit.getId(), queryDescriptor);
		Assert.assertNotNull(mapEntries);
		Assert.assertEquals(10, mapEntries.size());
		Assert.assertEquals(new Integer(50), mapEntries.getTotalSize());
		Assert.assertEquals(5, mapEntries.getPageAmount());
		Assert.assertEquals(2, mapEntries.getPageNumber());
	// TODO s.o.	Assert.assertEquals(orgUnit,mapEntries.getOrgUnit());
		//TODO s.o. Assert.assertEquals(orgUnit.getOrgUnitMap(), mapEntries.getOrgUnitMap());

	}

	@Test
	public void TestGetOrgUnitsWithQueryDescriptor() throws Exception {
		Client client = this.getCurrentClient(true);
		Group group = this.getCurrentGroup(client, true);
		group.setClient(client);
		@SuppressWarnings("unused")
		OrgUnit orgUnit = null;
		for (int i = 0; i < 5; i++) {
			orgUnit = this.getCurrentOrgUnit(group, true);
		}
		try {
			/* do the query without any additional information */
			QueryDescriptor queryDescriptor = null;
			QueryResult<OrgUnit> queryResult = this.getService().getOrgUnits(client.getName(), queryDescriptor);
			Assert.assertNotNull(queryResult);
			Assert.assertEquals(5, queryResult.size());

			/* now add a order instruction */
			queryDescriptor = new QueryDescriptor();

			BeanOrderProperty orderProperty1 = new BeanOrderProperty(OrgUnit.class, OrgUnitPropertyFields.FIELD_PATH_DISPLAYNAME, false);
			BeanOrderProperty orderProperty2 = new BeanOrderProperty(OrgUnit.class, OrgUnitPropertyFields.FIELD_PATH_GROUP_PREFIX + GroupPropertyFields.FIELD_PATH_DISPLAYNAME, true);

			queryDescriptor.addOrderProperty(orderProperty1);
			queryDescriptor.addOrderProperty(orderProperty2);
			queryResult = this.getService().getOrgUnits(client.getName(), queryDescriptor);
			Assert.assertNotNull(queryResult);
			Assert.assertEquals(5, queryResult.size());
			Assert.assertTrue(queryResult.get(0).getId() > queryResult.get(4).getId());

			/* now add paging information */
			queryDescriptor.setLimit(2);
			queryDescriptor.setOffSet(2);

			queryDescriptor.addOrderProperty(orderProperty1);
			queryDescriptor.addOrderProperty(orderProperty2);
			queryResult = this.getService().getOrgUnits(client.getName(), queryDescriptor);
			Assert.assertNotNull(queryResult);
			Assert.assertEquals(2, queryResult.size());
			Assert.assertTrue(queryResult.get(0).getId() > queryResult.get(1).getId());
			Assert.assertEquals(3, queryResult.getPageAmount());
			Assert.assertEquals(new Integer(5), queryResult.getTotalSize());
			Assert.assertEquals(2, queryResult.getPageNumber());

		} catch (Exception e) {
			throw e;
		}
	}
}
