package org.evolvis.idm.test.model;

import java.util.LinkedList;

import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.junit.Test;

public class AttributeGroupValidationTest {
	@Test(expected = IllegalRequestException.class)
	public void testNullName() throws IllegalRequestException {
		ValidationUtil.validate(new AttributeGroup(null, null));
	}

	@Test(expected = IllegalRequestException.class)
	public void testEmptyName() throws IllegalRequestException {
		ValidationUtil.validate(new AttributeGroup("", null));
	}

	@Test
	public void testNameWithEmptyAttributeList() throws IllegalRequestException {
		AttributeGroup attributeGroup = new AttributeGroup("OK", "OK");
		attributeGroup.setAttributes(new LinkedList<Attribute>());
		ValidationUtil.validate(attributeGroup);
	}

	@Test
	public void testNameWithFilledAttributeList() throws IllegalRequestException {
		AttributeGroup attributeGroup = new AttributeGroup("OK", "OK");
		attributeGroup.addAttribute(new Attribute("OK", "OK"));
		ValidationUtil.validate(attributeGroup);
	}

	@Test(expected = IllegalRequestException.class)
	public void testNameWithIllegalAttributeList() throws IllegalRequestException {
		AttributeGroup attributeGroup = new AttributeGroup("OK", "OK");
		attributeGroup.addAttribute(new Attribute("name", "name"));
		attributeGroup.addAttribute(new Attribute("name", "name"));
		ValidationUtil.validate(attributeGroup);
	}

	@Test(expected = IllegalRequestException.class)
	public void testTooLongName() throws IllegalRequestException {
		ValidationUtil.validate(new AttributeGroup(getChars(101), null));
	}
	
	protected String getChars(int length) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < length; i++)
			buffer.append(".");
		return buffer.toString();
	}
}
