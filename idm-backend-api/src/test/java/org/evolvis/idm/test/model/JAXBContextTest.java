package org.evolvis.idm.test.model;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.JAXBContext;

import org.evolvis.idm.common.model.AbstractMap;
import org.evolvis.idm.common.model.AbstractMapEntry;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.applicationdata.model.AppMetaData;
import org.evolvis.idm.identity.applicationdata.model.AppMetaDataValue;
import org.evolvis.idm.identity.privatedata.model.Approval;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.AttributeType;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.organization.model.OrgUnitMap;
import org.evolvis.idm.relation.organization.model.OrgUnitMapEntry;
import org.evolvis.idm.relation.permission.model.Group;
import org.junit.Assert;
import org.junit.Test;

public class JAXBContextTest {
	@Test
	public void testCreateJAXBContext() throws Exception {
		Assert.assertNotNull(JAXBContext.newInstance(Application.class, Approval.class, Attribute.class, AttributeGroup.class, AttributeType.class, Client.class, Account.class, Value.class, ValueSet.class, QueryResult.class));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRecursivXML() throws Exception {
		JAXBContext context = JAXBContext.newInstance(Application.class, Approval.class, Attribute.class, AttributeGroup.class, AttributeType.class, Client.class, Account.class, Value.class, ValueSet.class, OrgUnitMap.class, OrgUnitMapEntry.class,
				AppMetaData.class, AppMetaDataValue.class, AbstractMap.class, AbstractMapEntry.class, QueryResult.class);

		// test AttributeGroup
		StringWriter writer0 = new StringWriter();
		Value value = new Value("name", "value");
		context.createMarshaller().marshal(value, writer0);
		System.err.println(writer0);
		Value value2 = (Value) context.createUnmarshaller().unmarshal(new StringReader(writer0.toString()));
		System.err.println(value2);

		AttributeGroup attributeGroup = new AttributeGroup("ag", "ag");
		attributeGroup.addAttribute(new Attribute("a1", "a1"));
		attributeGroup.addAttribute(new Attribute("a2", "a2"));
		attributeGroup.addAttribute(new Attribute("a3", "a3"));

		StringWriter writer = new StringWriter();

		context.createMarshaller().marshal(attributeGroup, writer);

		System.err.println(writer);

		AttributeGroup attributeGroup2 = (AttributeGroup) context.createUnmarshaller().unmarshal(new StringReader(writer.toString()));

		System.err.println(attributeGroup);
		System.err.println(attributeGroup2);

		// test Account

		StringWriter writer2 = new StringWriter();
		Account account = new Account();
		context.createMarshaller().marshal(account, writer2);
		System.err.println(writer2);
		Account account2 = (Account) context.createUnmarshaller().unmarshal(new StringReader(writer2.toString()));
		System.err.println(account2);

		// test OrgUnit

		OrgUnit orgUnit = new OrgUnit();
		orgUnit.setName("TEST-OrgUnit");
		orgUnit.setId(new Long(1));
		orgUnit.getOrgUnitMap().put("KEY", "VALUE");
		orgUnit.getOrgUnitMap().setId(new Long(2));
		StringWriter orgUnitWriter = new StringWriter();

		context.createMarshaller().marshal(orgUnit, orgUnitWriter);
		System.err.println(orgUnitWriter);
		OrgUnit orgUnit2 = (OrgUnit) context.createUnmarshaller().unmarshal(new StringReader(orgUnitWriter.toString()));

		System.err.println(orgUnit);
		System.err.println(orgUnit2);

		// test AppMetaData (Entries)

		StringWriter writer3 = new StringWriter();
		AppMetaData appMetaData = new AppMetaData();
		appMetaData.put("key", "value");
		context.createMarshaller().marshal(appMetaData, writer3);
		System.err.println(writer3);
		AppMetaData appMetaData2 = (AppMetaData) context.createUnmarshaller().unmarshal(new StringReader(writer3.toString()));
		System.err.println(appMetaData2);
		
		
		StringWriter writer4 = new StringWriter();
		QueryResult<Group> queryResult = new QueryResult<Group>();
		queryResult.setTotalSize(100);
		QueryDescriptor queryDescriptor = new QueryDescriptor();
		queryResult.setQueryDescriptor(queryDescriptor);
		Group group = new Group();
		group.setId(new Long(1));
		group.setName("name");
		group.setDisplayName("displayName");
		group.setWriteProtected(true);
		List<Group> groupies = new LinkedList<Group>();
		groupies.add(group);
		queryResult.setResultList(groupies);
		context.createMarshaller().marshal(queryResult, writer4);
		System.err.println(writer4);
		QueryResult<Group> queryResult2 = (QueryResult<Group>) context.createUnmarshaller().unmarshal(new StringReader(writer4.toString()));
		Assert.assertNotNull(queryResult2);
		Assert.assertEquals(1, queryResult2.getResultList().size());
		Assert.assertNotNull(queryResult2.getTotalSize());
		//List<Group> testList = queryResult2;
		//testList.size();
		Group group1 = queryResult2.getResultList().get(0);
		Assert.assertEquals("name", group1.getName());
		System.err.println(group1.getName());

	}
}
