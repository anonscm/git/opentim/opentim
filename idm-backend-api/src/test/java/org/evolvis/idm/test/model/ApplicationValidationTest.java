package org.evolvis.idm.test.model;


import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.junit.Test;

public class ApplicationValidationTest {
	@Test(expected = IllegalRequestException.class)
	public void testNullName() throws IllegalRequestException {
		ValidationUtil.validate(new Application(null, null));
	}

	@Test(expected = IllegalRequestException.class)
	public void testEmptyName() throws IllegalRequestException {
		ValidationUtil.validate(new Application("", ""));
	}

	@Test
	public void testName() throws IllegalRequestException, Exception {
		Application application = null;
		try{
			application = new Application("OK", "OK");
			ValidationUtil.validate(application);
		}catch(IllegalRequestException e){
			//TODO
		}
		catch(Exception e){
			throw e;
		}
		try{
			application.setClient(new Client("TestCliend","TestClient"));
			ValidationUtil.validate(application);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Test(expected = IllegalRequestException.class)
	public void testTooLongName() throws IllegalRequestException {
		ValidationUtil.validate(new Application(getChars(101), null));
	}
	
	protected String getChars(int length) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < length; i++)
			buffer.append(".");
		return buffer.toString();
	}
}
