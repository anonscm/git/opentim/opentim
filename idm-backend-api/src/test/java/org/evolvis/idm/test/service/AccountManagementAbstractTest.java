/**
 * 
 */
package org.evolvis.idm.test.service;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalProperty;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.model.AccountPropertyFields;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.permission.model.Role;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Yorka Neumann
 * 
 */
public abstract class AccountManagementAbstractTest extends AbstractBaseServiceAbstractTest<AccountManagement> {

	String errorPrefix = "AccountManagementAbstractTest";

	/**
	 * tests to persist a simple account that is a) not already persistent b) already persistent (modified)
	 * 
	 * @throws Exception
	 */
	@Test
	public void setAccount() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account = new Account();
		account.setUuid("default-" + System.currentTimeMillis());
		account.setUsername("default-user" + System.currentTimeMillis());
		account.setClient(client);
		account = this.getService().setAccount(client.getName(), account);
		/* Now try to persist a account that is not activated before it is persistent */
		Account deactivatedAccount = new Account();
		deactivatedAccount.setUuid("default-"+System.currentTimeMillis());
		deactivatedAccount.setUsername("default-user"+System.currentTimeMillis());
		deactivatedAccount.setClient(client);
		deactivatedAccount.setSoftDelete(true);
		deactivatedAccount = this.getService().setAccount(client.getName(), deactivatedAccount);
		Assert.assertNotNull(deactivatedAccount);
		Assert.assertNotNull(deactivatedAccount.getId());
		Assert.assertNotNull(deactivatedAccount.getDeactivationDate());
	}

	@Test
	public void setAccountDuplicates() throws Exception {
		Client client = this.getCurrentClient(true);
		Account account1 = new Account();
		Account account2 = new Account();
		Account account3 = new Account();

		account1.setClient(client);
		account2.setClient(client);
		account3.setClient(client);

		account1.setUuid("Test-uuid1");
		account2.setUuid("Test-uuid1");
		account3.setUuid("Test-uuid3");

		account1.setUsername("Test-Username1");
		account2.setUsername("Test-Username1");
		account3.setUsername("Test-Username3");

		account1.setDisplayName("Test-Username1");
		account2.setDisplayName("Test-Username1");
		account3.setDisplayName("Test-Username3");

		/* try to persist two accounts with the same uuid,username,displayname */
		try {
			account1 = this.getService().setAccount(client.getName(), account1);
			account2 = this.getService().setAccount(client.getName(), account2);
			Assert.fail("No exception has been thrown");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
		/* try to rename uuid, username, diplayname of a persistent account to values of another persistent account */
		account3 = this.getService().setAccount(client.getName(), account3);
		try {
			account3.setUuid(account1.getUuid());
			account3.setUsername(account1.getUsername());
			account3.setDisplayName(account1.getDisplayName());
			account3 = this.getService().setAccount(client.getName(), account3);
			Assert.fail("No exception has been thrown");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
	}

	@Test
	public void setAccountWithComplexDisplayName() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account = new Account();
		account.setUuid("default-" + System.currentTimeMillis());
		account.setUsername("default-user" + System.currentTimeMillis());
		account.setDisplayName("abcäüößé10-_.,'`´()[]{}$&§\\/ ");
		account.setClient(client);

		account = this.getService().setAccount(client.getName(), account);
	}

	@Test
	public void setAccountWithIllegalData() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account = new Account();
		account.setClient(client);
		try {
			account = this.getService().setAccount(client.getName(), account);
			Assert.fail();
		} catch (BackendException e) {
			// TODO fix and retrieve IllegalRequestException
			// System.err.println(this.errorPrefix + "setGroup: IllegalRequestException while persisting an account: ");
			// List<IllegalProperty> illegalProperties = e.getIllegalProperties();
			// System.err.println("Illegal Properties: " + illegalProperties.size());
			// for (IllegalProperty illegalProperty : illegalProperties) {
			// System.err.println(illegalProperty.toString());
			// }
		} catch (Exception e) {
			System.err.println(this.errorPrefix + "setAccount: Error while persisting an account: " + e.getMessage() + " " + e.toString());
			throw e;
		}
	}

	/**
	 * tries to persist an account that has not the needed data (uuid) to persist and expects a special IllegalRequestException to be thrown.
	 * 
	 * @throws Exception : Expected is an IllegalRequestException that indicates that the unique account id is missing
	 */
	@Test
	public void setAccountWithoutData() throws Exception {
		try {
			this.getService().setAccount(null, null);
			Assert.fail();
		} catch (BackendException e) {
			Assert.assertEquals(BackendException.CODE_CLIENT_NOT_AVAILABLE, e.getErrorCode());
		} catch (Exception e) {
			throw e;
		}

		try {
			getService().setAccount(this.getCurrentClient(true).getName(), new Account());
		} catch (IllegalRequestException e) {
			Assert.assertNotNull(e.getIllegalProperties());
			Assert.assertEquals(1, e.getIllegalProperties().size());
			for (IllegalProperty illegalProperty : e.getIllegalProperties()) {
				Assert.assertEquals(Account.class.getName(), illegalProperty.getBeanName());
				if (!(illegalProperty.getPropertyName().equals("uuid")))
					Assert.fail();
				Assert.assertNull(Role.class.getName(), illegalProperty.getInvalidValue());
			}
			throw e;
		} catch (BackendException e) {
			// TODO fix test
			// Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
		}
	}

	@Test
	public void getAccountById() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account = this.getCurrentAccount(client, true);
		Account resultAccount = null;
		try {
			resultAccount = this.getService().getAccountByUuid(account.getUuid());
		} catch (Exception e) {
			throw e;
		}
		Assert.assertNotNull(resultAccount);
		Assert.assertNotNull(resultAccount.getUuid());
		Assert.assertNotNull(resultAccount.getId());
		Assert.assertTrue(resultAccount.getUuid().equals(account.getUuid()));
	}

	@Test
	// TODO
	public void getAccountNonExistingAccount() throws Exception {
		Client client = this.getCurrentClient(false);
		try {
			this.getService().getAccountByUsername(client.getName(), "--------");
			Assert.fail();
		} catch (IllegalRequestException e) {
			if (!e.getErrorCode().equals(IllegalRequestException.CODE_ENTITY_NOT_PERSITENT))
				throw e;
		}
	}

	/**
	 * tests if trying to request a non-existing account results in a null return and not in an exception
	 * 
	 * @throws Exception
	 */
	@Test
	public void getNotExistentAccount() throws Exception {
		// try to get persistent account by id
		try {
			this.getService().getAccountByUuid("NOTEXISTENTACCOUNT");
			Assert.fail("No exception has been thrown!");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_ENTITY_NOT_PERSITENT, e.getErrorCode());
		}
	}

	/**
	 * tests if the try to delete an not existing (persistent account) causes the expected exception.
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteNotExistingAccount() throws Exception {
		try {
			this.getService().deleteAccount("NotExististingAccountToDelete");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_ENTITY_NOT_PERSITENT, e.getErrorCode());
		}
	}

	/**
	 * tests if deleting an existing account is successful.
	 * 
	 */
	@Test
	public void deleteAccount() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account = this.getCurrentAccount(client, true);
		this.getService().deleteAccount(account.getUuid());
		try {
			account = this.getService().getAccountByUuid(account.getUuid());
			Assert.fail("No exception has been thrown!");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_ENTITY_NOT_PERSITENT, e.getErrorCode());
		}
		// TODO !!!!!!!!!!!
		account = this.getCurrentAccount(client, true);

	}

	@Test
	public void getUserByName() throws Exception {
		Client client = this.getCurrentClient(true);
		Account account = this.getCurrentAccount(client, true);
		try {
			Account resultAccount = this.getService().getAccountByUsername(client.getName(), account.getUsername());
			Assert.assertNotNull(resultAccount);
			Assert.assertTrue(resultAccount.getUuid().equals(account.getUuid()));
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void isUserDeactivated() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account = this.getCurrentAccount(client, true);
		try {
			// user is not deactivated - test this
			Assert.assertFalse(this.getService().isDeactived(account.getUuid()));
			account.setSoftDelete(true);
			account = this.getService().setAccount(client.getName(), account);
			// now his is deactivated
			Assert.assertTrue(account.isSoftDelete());
			Assert.assertNotNull(account.getDeactivationDate());
			Assert.assertTrue(this.getService().isDeactived(account.getUuid()));
			// now reactivate it
			account.setSoftDelete(false);
			account.setClient(client);
			account = this.getService().setAccount(client.getName(), account);
			Assert.assertFalse(this.getService().isDeactived(account.getUuid()));
			Assert.assertNull(account.getDeactivationDate());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getDeactivatedAccountsSince() throws Exception {
		Client client = this.getCurrentClient(true);
		Account account1 = this.getCurrentAccount(client, true);
		Account account2 = this.getCurrentAccount(client, true);
		Account account3 = this.getCurrentAccount(client, true);
		try {
			account1.setSoftDelete(true);
			account2.setSoftDelete(true);
			account3.setSoftDelete(true);
			account1 = this.getService().setAccount(client.getName(), account1);
			account2 = this.getService().setAccount(client.getName(), account2);
			account3 = this.getService().setAccount(client.getName(), account3);

			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH - 5);
			account1.setDeactivationDate(calendar);
			account1.setClient(client);
			this.getService().setAccount(client.getName(), account1);
			List<Account> resultList = this.getService().getDeactivatedAccountsSince(client.getName(), calendar);
			Assert.assertEquals(3, resultList.size());
			calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH - 1);
			resultList = this.getService().getDeactivatedAccountsSince(client.getName(), calendar);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getAccounts() throws Exception {
		Client client1 = this.getCurrentClient(true);
		Client client2 = this.getCurrentClient(true);
		Account account1 = this.getCurrentAccount(client1, true);
		Account account2 = this.getCurrentAccount(client1, true);
		try {
			List<Account> resultList = this.getService().getAccounts(client2.getName(), null).getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			@SuppressWarnings("unused")
			Account account3 = this.getCurrentAccount(client2, true);
			resultList = this.getService().getAccounts(client1.getName(), null).getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			findIdInList(resultList, account1.getId());
			findIdInList(resultList, account2.getId());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getAccountsWithSearchProperties() throws Exception {
		Client client1 = this.getCurrentClient(true);
		// Client client2 = this.getCurrentClient(true);

		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));

		name = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client1.getName(), name);

		ValueSet testAccountName1 = new ValueSet(name.getName());
		testAccountName1.addValue(new Value("salutation", "Herr"));
		testAccountName1.addValue(new Value("firstname", "Luke"));
		testAccountName1.addValue(new Value("lastname", "Skywalker"));

		ValueSet testAccountName2 = new ValueSet(name.getName());
		testAccountName2.addValue(new Value("salutation", "Herr"));
		testAccountName2.addValue(new Value("firstname", "Anakin"));
		testAccountName2.addValue(new Value("lastname", "SKYWALKER"));

		ValueSet testAccountName3 = new ValueSet(name.getName());
		testAccountName3.addValue(new Value("salutation", "Herr"));
		testAccountName3.addValue(new Value("firstname", "Test-1"));
		testAccountName3.addValue(new Value("lastname", "Test-4"));

		ValueSet testAccountName4 = new ValueSet(name.getName());
		testAccountName4.addValue(new Value("salutation", "Herr"));
		testAccountName4.addValue(new Value("firstname", "Test-2"));
		testAccountName4.addValue(new Value("lastname", "Test-3"));

		ValueSet testAccountName5 = new ValueSet(name.getName());
		testAccountName5.addValue(new Value("salutation", "Herr"));
		testAccountName5.addValue(new Value("firstname", "Test-4"));
		
		testAccountName5.addValue(new Value("lastname", "Testß\\_'-5"));
		/* 1) 
		 * only used in an initial test - # is not an allowed char
		 * testAccountName5.addValue(new Value("lastname", "T#_estß#ß\\_%'-5"));
		 */

		Account account1 = this.getCurrentAccount(client1, true);
		Account account2 = this.getCurrentAccount(client1, true);
		Account account3 = this.getCurrentAccount(client1, true);
		Account account4 = this.getCurrentAccount(client1, true);
		Account account5 = this.getCurrentAccount(client1, true);

		account5.setUuid("Skywalker");
		account5 = this.getService().setAccount(client1.getName(), account5);

		getServiceProvider().getPrivateDataManagementService().setValues(account1.getUuid(), testAccountName1, false);
		getServiceProvider().getPrivateDataManagementService().setValues(account2.getUuid(), testAccountName2, false);
		getServiceProvider().getPrivateDataManagementService().setValues(account3.getUuid(), testAccountName3, false);
		getServiceProvider().getPrivateDataManagementService().setValues(account4.getUuid(), testAccountName4, false);
		getServiceProvider().getPrivateDataManagementService().setValues(account5.getUuid(), testAccountName5, false);

		try {
			/* create a query descriptor with an order property and a search property (over all attributes) */
			QueryDescriptor queryDescriptor = new QueryDescriptor();
			BeanOrderProperty beanOrderProperty = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_USERNAME, true);
			BeanSearchProperty beanSearchProperty = new BeanSearchProperty(Account.class, AccountPropertyFields.ALL, "ywalk");
			queryDescriptor.addSearchProperty(beanSearchProperty);
			queryDescriptor.addOrderProperty(beanOrderProperty);
			/* set paging information */
			queryDescriptor.setLimit(10);
			QueryResult<Account> queryResult = this.getService().getAccounts(client1.getName(), queryDescriptor);
			Assert.assertEquals(new Integer(2), queryResult.getTotalSize());
			List<Account> resultList = queryResult.getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			findIdInList(resultList, account1.getId());
			findIdInList(resultList, account2.getId());

			if (queryDescriptor.getSearchProperties().size() == 0) {
				/* local test removes this special bean search property */
				queryDescriptor.addSearchProperty(beanSearchProperty);
				System.out.println("Add search property again - this should only happen in a local test");
			}
			BeanSearchProperty beanSearchSoftDelete = new BeanSearchProperty(Account.class, AccountPropertyFields.FIELD_PATH_SOFTDELETE, "false");
			queryDescriptor.addSearchProperty(beanSearchSoftDelete);
			queryResult = this.getService().getAccounts(client1.getName(), queryDescriptor);
			resultList = queryResult.getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			findIdInList(resultList, account1.getId());
			findIdInList(resultList, account2.getId());

			/* set limit to one */
			queryDescriptor.setLimit(1);
			if (queryDescriptor.getSearchProperties().size() == 1) {
				/* local test removes this special bean search property */
				queryDescriptor.getSearchProperties().add(0, beanSearchProperty);
				System.out.println("Add search property again - this should only happen in a local test");
			}
			queryResult = this.getService().getAccounts(client1.getName(), queryDescriptor);
			Assert.assertEquals(new Integer(2), queryResult.getTotalSize());
			resultList = queryResult.getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(1, resultList.size());
			findIdInList(resultList, account1.getId());

			account1.setSoftDelete(true);
			getService().setAccount(client1.getName(), account1);
			beanSearchSoftDelete.setSearchValue("true");
			resultList = this.getService().getAccounts(client1.getName(), queryDescriptor).getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(1, resultList.size());
			findIdInList(resultList, account1.getId());


		/* because querying for accounts is a special case - do the test for special characters here too */
	
			beanSearchProperty.setSearchValue("\\");
			beanSearchProperty.setFieldPath(AccountPropertyFields.ALL);
			queryDescriptor.getSearchProperties().remove(beanSearchSoftDelete);
			queryResult = this.getService().getAccounts(client1.getName(), queryDescriptor);
			Assert.assertNotNull(queryResult);
			Assert.assertEquals(queryResult.size(), 1);
			Assert.assertEquals(account5.getUuid(), queryResult.get(0).getUuid());
			Assert.assertEquals(new Integer(1), queryResult.getTotalSize());
			
			beanSearchProperty.setSearchValue("_");
			beanSearchProperty.setFieldPath(AccountPropertyFields.ALL);
			queryResult = this.getService().getAccounts(client1.getName(), queryDescriptor);
			Assert.assertNotNull(queryResult);
			Assert.assertEquals(queryResult.size(), 1);
			Assert.assertEquals(account5.getUuid(), queryResult.get(0).getUuid());
			Assert.assertEquals(new Integer(1), queryResult.getTotalSize());
			
			beanSearchProperty.setSearchValue("ß");
			beanSearchProperty.setFieldPath(AccountPropertyFields.ALL);
			queryResult = this.getService().getAccounts(client1.getName(), queryDescriptor);
			Assert.assertNotNull(queryResult);
			Assert.assertEquals(queryResult.size(), 1);
			Assert.assertEquals(account5.getUuid(), queryResult.get(0).getUuid());
			Assert.assertEquals(new Integer(1), queryResult.getTotalSize());
			
			/* 1) beanSearchProperty.setSearchValue("ß#");
			beanSearchProperty.setFieldPath(AccountPropertyFields.ALL);
			queryResult = this.getService().getAccounts(client1.getName(), queryDescriptor);
			Assert.assertNotNull(queryResult);
			Assert.assertEquals(queryResult.size(), 1);
			Assert.assertEquals(account5.getUuid(), queryResult.get(0).getUuid());
			Assert.assertEquals(new Integer(1), queryResult.getTotalSize());*/
			} catch (Exception e) {
				throw e;
			}
	}

	
	@Test
	public void getAccountWithNullName() throws Exception {
		Client client1 = this.getCurrentClient(true);
		// Client client2 = this.getCurrentClient(true);

		AttributeGroup name = getServiceProvider().getPrivateDataConfigurationService().getAttributeGroup(client1.getName(), "personalData");
		Assert.assertNotNull(name);

		ValueSet testAccountName1 = new ValueSet(name.getName());
		testAccountName1.addValue(new Value("salutation", "Herr"));
		testAccountName1.addValue(new Value("firstname", ""));
		testAccountName1.addValue(new Value("lastname", null));

		ValueSet testAccountName2 = new ValueSet(name.getName());
		testAccountName2.addValue(new Value("salutation", "Herr"));
		testAccountName2.addValue(new Value("firstname", "Luke"));
		testAccountName2.addValue(new Value("lastname", "Skywalker"));

		
		Account account1 = this.getCurrentAccount(client1, true);
		Account account2 = this.getCurrentAccount(client1, true);

		getServiceProvider().getPrivateDataManagementService().setValues(account1.getUuid(), testAccountName1, false);
		getServiceProvider().getPrivateDataManagementService().setValues(account2.getUuid(), testAccountName2, false);

		QueryResult<User> users = this.getServiceProvider().getUserManagementService().getUsers(client1.getName(), null);
			
		Assert.assertNotNull(users);
		
		
	}
	
	
	@Test
	@SuppressWarnings(value = "unused")
	public void getAccountsSortedByDeactivation() throws Exception {
		Client client = this.getCurrentClient(true);
		Calendar calendar = new GregorianCalendar();
		int maxUserIndex = 30;
		Account account;
		for (int i = maxUserIndex; i > -1; i--) {
			String suffix = "0";
			if (i > 9)
				suffix = "";
			account = new Account();
			account.setClient(client);
			account.setUsername("TestSorted" + suffix + i);
			account.setUuid("UUID-" + System.currentTimeMillis());
			account.setCreationDate(calendar);
			if ((System.currentTimeMillis() % 2) == 1) {
				account.setSoftDelete(true);
				account.setDeactivationDate(calendar);
				System.out.print("deactivated, ");
			} else
				System.out.print("activated, ");
			// account.setDeactivationDate(calendar);
			// account.setSoftDelete(true);
			account = this.getService().setAccount(client.getName(), account);
		}
		System.out.println("");
		account = new Account();
		try {
			BeanOrderProperty orderProperty1 = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_SOFTDELETE, true);
			BeanOrderProperty orderProperty2 = new BeanOrderProperty(Account.class, "displayName", true);
			BeanOrderProperty orderProperty3 = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_DEACTIVATIONDATE, true);
			BeanOrderProperty orderProperty4 = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_CREATIONDATE, false);
			QueryDescriptor queryDescriptor = new QueryDescriptor();

			queryDescriptor.addOrderProperty(orderProperty1);
			// queryDescriptor.addOrderProperty(orderProperty2);
			// queryDescriptor.addOrderProperty(orderProperty3);
			// queryDescriptor.addOrderProperty(orderProperty4);
			QueryResult<Account> queryResult = this.getService().getAccounts(client.getName(), queryDescriptor);
			List<Account> resultList = queryResult.getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(31, resultList.size());
			Assert.assertEquals(new Integer(31), queryResult.getTotalSize());
			// Assert.assertEquals();
			for (int i = 0; i < 30; i++) {
				System.out.print(resultList.get(i).isSoftDelete() + ",");
			}
			System.out.println("");
			Iterator<Account> iterator = resultList.iterator();
			if (iterator.hasNext()) {
				do {
					account = iterator.next();
					System.out.print(account.isSoftDelete());
					if (account.getDeactivationDate() != null)
						System.out.println(", " + account.getDeactivationDate().toString());
					else
						System.out.println(", null.");
				} while (!account.isSoftDelete() && iterator.hasNext());
				while (iterator.hasNext()) {
					account = iterator.next();
					System.out.print(account.isSoftDelete() + ", ");
					Assert.assertTrue(account.isSoftDelete());
					if (account.getDeactivationDate() != null)
						System.out.println(", " + account.getDeactivationDate().toString());
					else
						System.out.println(", null.");
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Tests if the persisting an account that has a diplay name of max. size is possible the try to persist one which display name exceeds the max. leads to an error. the actual max. size is 81.
	 * 
	 * @throws Exception
	 */
	@Test
	public void validateDisplayNameSizeConstraint() throws Exception {
		Client client = this.getCurrentClient(true);
		Account account = new Account();
		account.setClient(client);
		/* try to persist an account with a display name of max. length */
		try {
			account.setUuid("uuid" + System.currentTimeMillis());
			account.setDisplayName("------");
			account = this.getService().setAccount(client.getName(), new Account(account.getUuid(), "irgendwas", "------", client, false));
			AttributeGroup attributeGroup = this.getServiceProvider().getPrivateDataConfigurationService().getAttributeGroup(client.getName(), "personalData");
			Assert.assertNotNull(attributeGroup);
			ValueSet valueSet = new ValueSet();
			valueSet.setAccount(account);
			valueSet.setAttributeGroup(attributeGroup);
			Value firstName = new Value();
			Value lastName = new Value();
			firstName.setAttribute(attributeGroup.getAttribute("firstname"));
			lastName.setAttribute(attributeGroup.getAttribute("lastname"));
			firstName.setValueSet(valueSet);
			lastName.setValueSet(valueSet);
			valueSet.addValue(firstName);
			valueSet.addValue(lastName);
			firstName.setValue("Längstmöglicher  Vorname Längstmöglicher");
			lastName.setValue("Längstmöglicher  Nachname Längstmögliche");
			Assert.assertEquals(40, lastName.getValue().length());
			Assert.assertEquals(40, firstName.getValue().length());
			valueSet = this.getServiceProvider().getPrivateDataManagementService().setValues(account.getUuid(), valueSet, false);
			account.setDisplayName(firstName.getValue() + " " + lastName.getValue());
			account = this.getService().setAccount(client.getName(), account);
		} catch (Exception e) {
			throw e;
		}

		/* now add one more character and expect a validation error */
		try {
			account.setDisplayName(account.getDisplayName() + "x");
			account = this.getService().setAccount(client.getName(), account);
			Assert.fail(NO_EXCEPTION);
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_VALIDATION_ERROR, e.getErrorCode());
			Assert.assertNotNull(e.getIllegalProperties());
			Assert.assertEquals(1, e.getIllegalProperties().size());
			Assert.assertEquals(e.getIllegalProperties().get(0).getPropertyName(), "displayName");
		}
	}

	/*
	 * @Test public void getAccountsSortedByDeactivationStage39() throws Exception {
	 * 
	 * System.out.println(""); Account account = new Account(); try { BeanOrderProperty orderProperty1 = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_SOFTDELETE, false); BeanOrderProperty orderProperty2 = new
	 * BeanOrderProperty(Account.class, "displayName", true); BeanOrderProperty orderProperty3 = new BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_DEACTIVATIONDATE, true); BeanOrderProperty orderProperty4 = new
	 * BeanOrderProperty(Account.class, AccountPropertyFields.FIELD_PATH_CREATIONDATE, false); QueryDescriptor queryDescriptor = new QueryDescriptor(); queryDescriptor.setLimit(10); queryDescriptor.setOffSet(0);
	 * queryDescriptor.addOrderProperty(orderProperty1); // queryDescriptor.addOrderProperty(orderProperty2); // queryDescriptor.addOrderProperty(orderProperty3); // queryDescriptor.addOrderProperty(orderProperty4); QueryResult<Account> queryResult =
	 * this.getService().getAccounts("stage3.9.tarent.buero", queryDescriptor); List<Account> resultList = queryResult.getResultList(); Assert.assertNotNull(resultList); // Assert.assertEquals(); for (int i = 0; i < resultList.size(); i++) {
	 * System.out.println("#"+i+": "+resultList.get(i).getSoftDelete() + ","); } System.out.println(""); Iterator<Account> iterator = resultList.iterator(); if (iterator.hasNext()) { do { account = iterator.next();
	 * System.out.print(account.getSoftDelete()); if (account.getDeactivationDate() != null) System.out.println(", " + account.getDeactivationDate().toString()); else System.out.println(", null."); } while (!account.getSoftDelete() && iterator.hasNext());
	 * while (iterator.hasNext()) { account = iterator.next(); System.out.print(account.getSoftDelete() + ", "); Assert.assertTrue(account.getSoftDelete()); if (account.getDeactivationDate() != null) System.out.println(", " +
	 * account.getDeactivationDate().toString()); else System.out.println(", null."); } } } catch (Exception e) { throw e; }
	 * 
	 * }
	 */
	@Test
	public void getClientByNotPersistentUuid() throws Exception {
		try {
			this.getService().getClientByUuid("NOT_PERSISTENT_ENTITY");
			Assert.fail();
		} catch (IllegalRequestException e) {
			// TODO
		}
	}

}
