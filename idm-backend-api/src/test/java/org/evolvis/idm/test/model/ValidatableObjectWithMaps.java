/**
 * 
 */
package org.evolvis.idm.test.model;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.evolvis.idm.common.validation.MapPattern;

/**
 * @author Michael Kutz, tarent GmbH
 *
 */
public class ValidatableObjectWithMaps {
	
	@MapPattern(keyPattern = "keyValid[0-9]+", valuePattern = "valueValid[0-9]+")
	private Map<String, String> mapWithBothPatterns = new HashMap<String, String>();
	
	@MapPattern(valuePattern = "valueValid[0-9]+")
	private Map<String, String> mapWithValuePattern = new TreeMap<String, String>();

	@MapPattern(keyPattern = "keyValid[0-9]+")
	private Map<String, String> mapWithKeyPattern = new HashMap<String, String>();

	/**
	 * @return the mapWithBothPatterns
	 */
	public Map<String, String> getMapWithBothPatterns() {
		return mapWithBothPatterns;
	}

	/**
	 * @param mapWithBothPatterns the mapWithBothPatterns to set
	 */
	public void setMapWithBothPatterns(Map<String, String> mapWithBothPatterns) {
		this.mapWithBothPatterns = mapWithBothPatterns;
	}

	/**
	 * @return the mapWithValuePattern
	 */
	public Map<String, String> getMapWithValuePattern() {
		return mapWithValuePattern;
	}

	/**
	 * @param mapWithValuePattern the mapWithValuePattern to set
	 */
	public void setMapWithValuePattern(Map<String, String> mapWithValuePattern) {
		this.mapWithValuePattern = mapWithValuePattern;
	}

	/**
	 * @return the mapWithKeyPattern
	 */
	public Map<String, String> getMapWithKeyPattern() {
		return mapWithKeyPattern;
	}

	/**
	 * @param mapWithKeyPattern the mapWithKeyPattern to set
	 */
	public void setMapWithKeyPattern(Map<String, String> mapWithKeyPattern) {
		this.mapWithKeyPattern = mapWithKeyPattern;
	}

}
