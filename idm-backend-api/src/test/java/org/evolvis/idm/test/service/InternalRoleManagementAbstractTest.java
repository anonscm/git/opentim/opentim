package org.evolvis.idm.test.service;

import java.util.List;

import javax.xml.ws.soap.SOAPFaultException;

import junit.framework.Assert;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.GroupPropertyFields;
import org.evolvis.idm.relation.permission.model.InternalRoleAssignment;
import org.evolvis.idm.relation.permission.model.InternalRoleScope;
import org.evolvis.idm.relation.permission.model.InternalRoleScopePropertyFields;
import org.evolvis.idm.relation.permission.model.InternalRoleType;
import org.evolvis.idm.relation.permission.service.InternalRoleManagement;
import org.junit.Before;
import org.junit.Test;

public abstract class InternalRoleManagementAbstractTest extends AbstractBaseServiceAbstractTest<InternalRoleManagement> {

	
	protected Client client;


	@Before
	public void init() {
		try {
			this.client = this.getCurrentClient(false);
		} catch (Exception e) {

		}
	}
	
	/**
	 * Tries to obtain internal role assignment of a user that has no explicit one. The result of the method calls should be that the user has the internal role USER.
	 * 
	 * @throws Exception
	 */
	@Test
	public void getInternalRoleForUserFromEmpty() throws Exception {
		Client client = this.getCurrentClient(true);
		Account account = this.getCurrentAccount(client, true);

		/* get the highest role of the user which should be USER */
		InternalRoleType internalRole = this.getService().getInternalRoleForUser(account.getUuid());
		Assert.assertNotNull(internalRole);
		Assert.assertTrue(internalRole.equals(InternalRoleType.USER));

		/* get all internal roles - there should only be one : USER */
		List<InternalRoleType> resultList = this.getService().getInternalRolesOfUser(account.getUuid());
		Assert.assertNotNull(resultList);
		Assert.assertEquals(1, resultList.size());
		Assert.assertEquals(InternalRoleType.USER, resultList.get(0));
	}


	/**
	 * Tries to persist a new assignment of a user account to an internal role type. This should be done without any exceptions
	 */
	@Test
	public void addUserToInternalRole() throws Exception {
		
		try {

			Client client = this.getCurrentClient(false);
			/* assign an account to an internal role for example SECTION_ADMIN */
			Account account = this.getCurrentAccount(client, true);
			InternalRoleType internalRole = InternalRoleType.SECTION_ADMIN;
			this.getService().addUserToInternalRole(account.getUuid(), internalRole);

			/* do the control query */
			InternalRoleType internalRoleType = this.getService().getInternalRoleForUser(account.getUuid());
			Assert.assertNotNull(internalRoleType);
			Assert.assertTrue(internalRoleType.equals(InternalRoleType.SECTION_ADMIN));
			

			/* do the same again to control that this is no duplicate assignment possible but also not result in an error exception */
			this.getService().addUserToInternalRole(account.getUuid(), internalRole);
			Assert.assertNotNull(internalRoleType);
			Assert.assertTrue(internalRoleType.equals(InternalRoleType.SECTION_ADMIN));
			
			/* now assign the the same account to another internal role */
			internalRole = InternalRoleType.CLIENT_ADMIN;
			this.getService().addUserToInternalRole(account.getUuid(), internalRole);
			List<InternalRoleType> resultList = this.getService().getInternalRolesOfUser(account.getUuid());
			Assert.assertEquals(2, resultList.size());
			boolean condition1 = resultList.get(0).equals(InternalRoleType.CLIENT_ADMIN) && resultList.get(1).equals(InternalRoleType.SECTION_ADMIN);
			boolean condition2 = resultList.get(0).equals(InternalRoleType.SECTION_ADMIN) && resultList.get(1).equals(InternalRoleType.CLIENT_ADMIN);
			Assert.assertTrue(condition1 | condition2);

		} catch (Exception e) {
			throw e;
		}

	}


	/**
	 * Tests the two methods of the internal role management service to get information about the internal roles of a single given user.
	 * 
	 * @throws Exception
	 */
	@Test
	public void getInternalRoleForUser() throws Exception {
		Account account = this.getCurrentAccount(this.getCurrentClient(false), true);
		try {
			this.getService().addUserToInternalRole(account.getUuid(), InternalRoleType.CLIENT_ADMIN);
			this.getService().addUserToInternalRole(account.getUuid(), InternalRoleType.SECTION_ADMIN);
			InternalRoleType internalRole = this.getService().getInternalRoleForUser(account.getUuid());
			Assert.assertNotNull(internalRole);
			Assert.assertEquals(InternalRoleType.CLIENT_ADMIN, internalRole);

			List<InternalRoleType> resultList = this.getService().getInternalRolesOfUser(account.getUuid());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());

			boolean condition1 = resultList.get(0) == InternalRoleType.CLIENT_ADMIN && resultList.get(1) == InternalRoleType.SECTION_ADMIN;
			boolean condition2 = resultList.get(0) == InternalRoleType.SECTION_ADMIN && resultList.get(1) == InternalRoleType.CLIENT_ADMIN;
			Assert.assertTrue(condition1 | condition2);
		} catch (Exception e) {
			throw e;
		}
	}


	/**
	 * Tests if removing an assignment of a user account to an internal role type is successful.
	 * 
	 * @throws Exception
	 */
	@Test
	public void removeUserFromInternalRole() throws Exception {
		Account account = this.getCurrentAccount(client, true);
		try {
			this.getService().addUserToInternalRole(account.getUuid(), InternalRoleType.SECTION_ADMIN);
			/* Simple remove assignment */
			this.getService().removeUserFromInternalRole(account.getUuid(), InternalRoleType.SECTION_ADMIN);
			/* Look if has been really removed */
			List<Account> accounts = this.getService().getUsersOfInternalRole(client.getClientName(), InternalRoleType.SECTION_ADMIN);
			for(Account tempAccount : accounts)
				Assert.assertFalse(tempAccount.getId().equals(account.getId()));
		} catch (Exception e) {
			throw e;
		}
		/* Second part: Try to remove the account once again */
		try {
			this.getService().removeUserFromInternalRole(account.getUuid(), InternalRoleType.SECTION_ADMIN);
		} catch (Exception e) {
			/* The expected exception is a BackendException that indicates that the wanted entity is not persistent. */
			Assert.assertEquals(IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION.getMessage(), e.getMessage());
		}
		/* Now try to remove an internal role association that is used for an internal role scope assignment */
		try{
			Account account1 = this.getCurrentAccount(client,true);
			this.getService().addUserToInternalRole(account1.getUuid(), InternalRoleType.SECTION_ADMIN);
			Group group = this.getCurrentGroup(client, true);
			this.getService().setInternalRoleScope(account1.getUuid(), InternalRoleType.SECTION_ADMIN, group.getId());
			this.getService().removeUserFromInternalRole(account1.getUuid(), InternalRoleType.SECTION_ADMIN);
			//this.getServiceProvider().getGroupAndRoleManagementService().deleteGroup(group.getId());
		} catch(Exception e){
			throw e;
		}
		try{
			Account account1 = this.getCurrentAccount(client,true);
			this.getService().addUserToInternalRole(account1.getUuid(), InternalRoleType.SECTION_ADMIN);
			Group group = this.getCurrentGroup(client, true);
			this.getService().setInternalRoleScope(account1.getUuid(), InternalRoleType.SECTION_ADMIN, group.getId());
			this.getServiceProvider().getGroupAndRoleManagementService().deleteGroup(client.getClientName(), group.getId());
		} catch(Exception e){
			throw e;
		}
	}


	/**
	 * Tests if requesting a list of user that have a special internal role for a given client can be successfully executed.
	 * 
	 * @throws Exception
	 */
	@Test
	public void getUsersOfInternalRole() throws Exception {
		try {
			/* prepare the database state */
			Client client = this.getCurrentClient(true);
			Account account1 = this.getCurrentAccount(client, true);
			Account account2 = this.getCurrentAccount(client, true);
			this.getService().addUserToInternalRole(account1.getUuid(), InternalRoleType.CLIENT_ADMIN);
			this.getService().addUserToInternalRole(account2.getUuid(), InternalRoleType.CLIENT_ADMIN);
			this.getService().addUserToInternalRole(account2.getUuid(), InternalRoleType.SECTION_ADMIN);

			/* do the test */
			List<Account> accounts = this.getService().getUsersOfInternalRole(client.getClientName(), InternalRoleType.CLIENT_ADMIN);
			Assert.assertNotNull(accounts);
			Assert.assertEquals(2, accounts.size());
			Assert.assertTrue(accounts.contains(account1) && accounts.contains(account2));
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Tests if extending the internal role scope of two users with a specific internal role with a given group can successfully done.
	 * 
	 * @throws Exception
	 */
	@Test
	public void setInternalRoleScope() throws Exception {
		Account account = this.getCurrentAccount(this.client, true);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		try {
			this.getServiceProvider().getInternalRoleManagementService().addUserToInternalRole(account.getUuid(), InternalRoleType.SECTION_ADMIN);
			this.getService().setInternalRoleScope(account.getUuid(), InternalRoleType.SECTION_ADMIN, group1.getId());
			this.getService().setInternalRoleScope(account.getUuid(), InternalRoleType.SECTION_ADMIN, group2.getId());
			List<Group> resultList = this.getService().getInternalRoleScopesForAccount(account.getUuid(), InternalRoleType.SECTION_ADMIN, null);

			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			for (Object tempObject : resultList.toArray()) {
				Group tempGroup = (Group) tempObject;
				Assert.assertTrue(group1.getId().equals(tempGroup.getId()) | group2.getId().equals(tempGroup.getId()));
			}
		} catch (Exception e) {
			throw e;
		}
		try {
			// try to persist a duplicate role scope.
			this.getService().setInternalRoleScope(account.getUuid(), InternalRoleType.SECTION_ADMIN, group1.getId());
			Assert.fail("Persisting a duplicate internal role scope assignment shoult result in an exception");
		} catch(IllegalRequestException e){
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
		catch (BackendException e) {
			if (!e.getMessage().contains("ConstraintViolationException: could not insert"))
				Assert.fail("Persisting a duplicate internal role scope results in another exception than expected: " + e.getMessage());
		} 
		catch(SOAPFaultException e){
			throw e;
		}
		catch (Exception e) {
			Assert.fail("Persisting a duplicate internal role scope results in another exception than expected: " + e.getMessage());
		}
	}


	/**
	 * Tests if querying the groups that belongs to the role scope by a given user ( with a given internal role ) can be successfully done.
	 * 
	 */
	@Test
	public void getInternalRoleScopesForAccount() throws Exception {
		//this.clearInternalRoleAssignments();
		Account account = this.getCurrentAccount(this.client, true);
		Group group = this.getCurrentGroup(client, true);
		Group group1 = this.getCurrentGroup(client, true);
		// first try to query although there is no internal role and therefore no internal role scope assigned to the user.
		try {
			this.getServiceProvider().getInternalRoleManagementService().addUserToInternalRole(account.getUuid(), InternalRoleType.SECTION_ADMIN);
			List<Group> internalRoleScopes = this.getService().getInternalRoleScopesForAccount(account.getUuid(), InternalRoleType.SECTION_ADMIN, null);
			Assert.assertEquals(0, internalRoleScopes.size());
		} catch (Exception e) {
			throw e;
		}
		// now assign an internal role and an internal role scope to the user but query for another internal role.
		try {
			
			this.getService().setInternalRoleScope(account.getUuid(), InternalRoleType.SECTION_ADMIN, group.getId());
			this.getService().setInternalRoleScope(account.getUuid(), InternalRoleType.SECTION_ADMIN, group1.getId());
			List<Group> internalRoleScopes = this.getService().getInternalRoleScopesForAccount(account.getUuid(), InternalRoleType.CLIENT_ADMIN, null);
			Assert.assertEquals(0, internalRoleScopes.size());
		} catch (Exception e) {
			throw e;
		}
		// now query for the right role.
		try {
			List<Group> internalRoleScopes = this.getService().getInternalRoleScopesForAccount(account.getUuid(), InternalRoleType.SECTION_ADMIN, null);
			Assert.assertNotNull(internalRoleScopes);
			Assert.assertEquals(2, internalRoleScopes.size());

		} catch (Exception e) {
			throw e;
		}
		//just do the it again with a simple query descriptor
		try {
			QueryDescriptor queryDescriptor = new QueryDescriptor();
			BeanOrderProperty orderProperty1 = new BeanOrderProperty(InternalRoleScope.class, InternalRoleScopePropertyFields.FIELD_PATH_GROUP_PREFIX+GroupPropertyFields.FIELD_PATH_DISPLAYNAME, false);
			queryDescriptor.addOrderProperty(orderProperty1);
			queryDescriptor.setLimit(10);
			List<Group> internalRoleScopes = this.getService().getInternalRoleScopesForAccount(account.getUuid(), InternalRoleType.SECTION_ADMIN, queryDescriptor);
			Assert.assertNotNull(internalRoleScopes);
			Assert.assertEquals(2, internalRoleScopes.size());

		} catch (Exception e) {
			throw e;
		}
		
		
	}


	
	

	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void getInternalRolesForGroup() throws Exception {
		
		Account account1 = this.getCurrentAccount(this.client, true);
		Account account2 = this.getCurrentAccount(this.client, true);
		Group group = this.getCurrentGroup(client, true);
		
		this.getServiceProvider().getInternalRoleManagementService().addUserToInternalRole(account1.getUuid(), InternalRoleType.SECTION_ADMIN);
		this.getServiceProvider().getInternalRoleManagementService().addUserToInternalRole(account2.getUuid(), InternalRoleType.SECTION_ADMIN);
		
		try {
			// first do the query with no role scope assignments in the persistence context
			List<InternalRoleAssignment> internalRoles = this.getService().getInternalRolesForGroup(client.getClientName(), group.getId());
			Assert.assertNotNull(internalRoles);
			Assert.assertEquals(0, internalRoles.size());
		} catch (Exception e) {
			throw e;
		}
		try {
			// now test what happens if this group belongs to the role scope of two accounts

			// prepare the database state
			this.getService().setInternalRoleScope(account1.getUuid(), InternalRoleType.SECTION_ADMIN, group.getId());
			this.getService().setInternalRoleScope(account2.getUuid(), InternalRoleType.SECTION_ADMIN, group.getId());

			// do the query
			List<InternalRoleAssignment> internalRoles = this.getService().getInternalRolesForGroup(client.getClientName(), group.getId());

			// examine the result
			Assert.assertNotNull(internalRoles);
			Assert.assertEquals(2, internalRoles.size());
			Assert.assertTrue(internalRoles.get(0).getType() == InternalRoleType.SECTION_ADMIN);
			Assert.assertTrue(internalRoles.get(1).getType() == InternalRoleType.SECTION_ADMIN);
			Account testAccount0 = internalRoles.get(0).getAccount();
			Account testAccount1 = internalRoles.get(1).getAccount();
			boolean condition1 = testAccount0.getUuid().equals(account1.getUuid()) && testAccount1.getUuid().equals(account2.getUuid());
			boolean condition2 = testAccount0.getUuid().equals(account2.getUuid()) && testAccount1.getUuid().equals(account1.getUuid());
			Assert.assertTrue(condition1 | condition2);
		} catch (Exception e) {
			throw e;
		}

	}


	@Test
	public void getInternalRolesForGroupIllegalArguments() throws Exception {
		// call the method with the null argument
		try {
			this.getService().getInternalRolesForGroup(client.getClientName(), null);
			Assert.fail("IllegalRequestException expected for getting internal roles of group.");
		} catch (IllegalRequestException e) {
			if (!e.getMessage().contains(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage()))
				Assert.fail("Thrown IllegalRequestException is not the excepted for getting internal roles of a group. Exception message: " + e.getMessage());
		} catch (Exception e) {
			Assert.fail("Thrown Exception is not the excepted for getting internal roles of a group. Exception message: " + e.getMessage());
		}
		// call the method with an id argument that belongs to no persistent content
		try {
			List<InternalRoleAssignment> resultList = this.getService().getInternalRolesForGroup(client.getClientName(), new Long(1000));
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());	
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	@Test 
	public void deleteInternalRoleScope() throws Exception{
		Account account1 = this.getCurrentAccount(this.client, true);
		Account account2 = this.getCurrentAccount(client, true);
		Group group = this.getCurrentGroup(client, true);
		Group group1 = this.getCurrentGroup(client, true);
		this.getService().addUserToInternalRole(account1.getUuid(), InternalRoleType.SECTION_ADMIN);
		this.getService().addUserToInternalRole(account2.getUuid(), InternalRoleType.SECTION_ADMIN);
		this.getService().setInternalRoleScope(account1.getUuid(), InternalRoleType.SECTION_ADMIN, group.getId());
		this.getService().setInternalRoleScope(account1.getUuid(), InternalRoleType.SECTION_ADMIN, group1.getId());
		// prepare the database state
		this.getService().setInternalRoleScope(account2.getUuid(), InternalRoleType.SECTION_ADMIN, group.getId());
		// do the query
		List<InternalRoleAssignment> internalRoles = this.getService().getInternalRolesForGroup(client.getClientName(), group.getId());
		// examine the result
		Assert.assertNotNull(internalRoles);
		Assert.assertEquals(2, internalRoles.size());
		
			
		try{
			this.getService().deleteInternalRoleScope(account1.getUuid(), InternalRoleType.SECTION_ADMIN, group.getId());
			List<Group> groups = this.getService().getInternalRoleScopesForAccount(account1.getUuid(), InternalRoleType.SECTION_ADMIN, null);
			Assert.assertNotNull(groups);
			Assert.assertEquals(1,groups.size());
			Assert.assertTrue(groups.get(0).getId().equals(group1.getId()));
		} catch(Exception e){
			throw e;
		}
	}
}
