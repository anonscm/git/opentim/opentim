package org.evolvis.idm.test.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.privatedata.model.Approval;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.AttributeType;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public abstract class PrivateDataManagementAbstractTest extends AbstractBaseServiceAbstractTest<PrivateDataManagement> {
	protected String uuid = getName();

	public PrivateDataManagementAbstractTest() {
		baseProperties = new ArrayList<String>();
		baseProperties.add("id");
		baseProperties.add("name");
	}

	Client client;
	Account account;

	@Before
	public void init() throws Exception {
		client = this.getCurrentClient(true);
		account = this.getCurrentAccount(client, true);
	}

	@Test
	public void setValues() throws Exception {
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));
		Assert.assertEquals(3, name.getAttributes().size());

		AttributeGroup address = new AttributeGroup("address" + getName(), "Adresse", null, true);
		address.addAttribute(new Attribute("street", "Strasse"));
		address.addAttribute(new Attribute("zipcode", "PLZ"));
		address.addAttribute(new Attribute("city", "Ort"));
		address.addAttribute(new Attribute("state", "Bundesland"));
		address.addAttribute(new Attribute("country", "Land"));
		Assert.assertEquals(5, address.getAttributes().size());

		AttributeGroup contact = new AttributeGroup("contact" + getName(), "Kontaktdaten", null, false);
		contact.addAttribute(new Attribute("fon", "Telefon", null, true));
		contact.addAttribute(new Attribute("mobile", "Mobil", null, true));
		Assert.assertEquals(2, contact.getAttributes().size());

		AttributeGroup result1 = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		Assert.assertEquals(3, result1.getAttributes().size());
		AttributeGroup result2;
		try {
			result2 = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), address);
			Assert.assertNotNull(result2);
			Assert.assertNotNull(result2.getId());
			Assert.assertEquals(5, result2.getAttributes().size());
		} catch (Exception e) {
			throw e;
		}
		AttributeGroup result3 = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), contact);
		Assert.assertNotNull(result3);
		Assert.assertNotNull(result3.getId());
		Assert.assertEquals(2, result3.getAttributes().size());

		result1 = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), result1);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		Assert.assertEquals(3, result1.getAttributes().size());

		result2 = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), result2);
		Assert.assertNotNull(result2);
		Assert.assertNotNull(result2.getId());
		Assert.assertEquals(5, result2.getAttributes().size());

		result3 = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), result3);
		Assert.assertNotNull(result3);
		Assert.assertNotNull(result3.getId());
		Assert.assertEquals(2, result3.getAttributes().size());

		ValueSet testAccountName = new ValueSet(name.getName());
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));

		ValueSet testAccountAddress = new ValueSet(address.getName());
		testAccountAddress.addValue(new Value("street", "Traumstraße 123"));
		testAccountAddress.addValue(new Value("zipcode", "12345"));
		testAccountAddress.addValue(new Value("city", "Musterstadt"));
		testAccountAddress.addValue(new Value("state", "Bundesland"));
		testAccountAddress.addValue(new Value("country", "Land"));

		ValueSet testAccountContact = new ValueSet(contact.getName());
		testAccountContact.addValue(new Value("fon", "0228 12345678"));
		testAccountContact.addValue(new Value("mobile", "0177 12345678"));

		ValueSet result4 = getService().setValues(account.getUuid(), testAccountName, false);
		Assert.assertNotNull(result4);
		Assert.assertNotNull(result4.getId());

		ValueSet result5 = getService().setValues(account.getUuid(), testAccountAddress, false);
		Assert.assertNotNull(result5);
		Assert.assertNotNull(result5.getId());

		ValueSet result6 = getService().setValues(account.getUuid(), testAccountContact, false);
		Assert.assertNotNull(result6);
		Assert.assertNotNull(result6.getId());

		// now try cases with illegal parameter arguments
		try {
			@SuppressWarnings("unused")
			ValueSet result7 = this.getService().setValues(null, testAccountContact, false);
			Assert.fail();
		} catch (IllegalRequestException e) {
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		} catch (BackendException e) {
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
		}
		try {
			@SuppressWarnings("unused")
			ValueSet result7 = this.getService().setValues(account.getUuid(), null, false);
			Assert.fail();
		} catch (IllegalRequestException e) {
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
	}

	@Test
	public void getValues() throws Exception {
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));

		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		ValueSet testAccountName = new ValueSet(name.getName());
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));

		ValueSet result1 = getService().setValues(account.getUuid(), testAccountName, false);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		ValueSet result2 = findIdInList(getService().getValues(account.getUuid()), result1.getId());

		compareBeansStringProperties(result1, result2, baseProperties, null);
		List<String> assertPropertiesEqual = new ArrayList<String>(baseProperties);
		assertPropertiesEqual.add("value");
		Assert.assertNotNull(result2.getAttributeGroup());
		// compareBeanListStringProperties(result1.getValues(),
		// result2.getValues(), assertPropertiesEqual, null);
		this.getServiceProvider().getAccountManagementService().getAccountByUuid(account.getUuid());
	}

	@Test
	public void getValuesFilteredByAttribute() throws Exception {
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));

		name = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		System.err.println("attribute group: " + name.getId());
		Assert.assertNotNull(name.getId());
		for (Attribute attribute : name.getAttributes()) {
			Assert.assertNotNull(attribute.getId());
		}

		// ignore the attributes in the rest of this test.
		name.setAttributes(null);

		ValueSet testAccountName = new ValueSet(name.getName());
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));

		ValueSet result1 = getService().setValues(account.getUuid(), testAccountName, false);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		List<ValueSet> result2 = getService().getValuesByAttributes(account.getUuid(), Collections.singletonList(name));

		Assert.assertEquals(1, result2.size());
	}

	@Test
	public void setAndGetMultiplableAttributeValues() throws Exception{
		// prepare database state
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		Attribute firstName = new Attribute("firstname", "Vorname");
		firstName.setMultiple(true);
		name.addAttribute(firstName);
		name.addAttribute(new Attribute("lastname", "Nachname"));

		name = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);
		
		ValueSet testAccountName = new ValueSet(name.getName());
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Frische"));
		testAccountName.addValue(new Value("firstname", "grüne"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));
		
		try{
		
		ValueSet result1 = getService().setValues(account.getUuid(), testAccountName, false);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		Assert.assertEquals(5, result1.getValues().size());
		
		result1 = this.getService().getValuesById(account.getUuid(), result1.getId());
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		Assert.assertEquals(5, result1.getValues().size());
		} catch(Exception e){
			throw e;
		}
	}
	
	
	@Test
	public void deleteSingleValuesFromValueSet() throws Exception {
		// prepare database state
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));

		name = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		ValueSet testAccountName = new ValueSet(name.getName());
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));

		ValueSet result1 = getService().setValues(account.getUuid(), testAccountName, false);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		Assert.assertEquals(3, result1.getValues().size());
		
		// now delete a value from the value set and persist the changes
		List<Value> values = result1.getValues();
		Long idFromEntry1 = result1.getValues().get(1).getId();
		Long idFromEntry2 = result1.getValues().get(2).getId();
		values.remove(0);
		result1.setValues(values);
		result1 = getService().setValues(account.getUuid(), result1, false);

		Assert.assertNotNull(result1);
		Assert.assertEquals(2, result1.getValues().size());
		findIdInList(result1.getValues(), idFromEntry1);
		findIdInList(result1.getValues(), idFromEntry2);
	}

	@Test
	public void setAndGetValues() throws Exception {
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));

		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		ValueSet testAccountName = new ValueSet(name.getName());
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));

		ValueSet result1 = getService().setValues(account.getUuid(), testAccountName, false);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		List<ValueSet> resultList = getService().getValues(account.getUuid());

		ValueSet result2 = findIdInList(resultList, result1.getId());
		compareBeansStringProperties(result1, result2, baseProperties, null);
	}

	@Test
	public void setAndChangeValueResult() throws Exception {
		ValueSet result1 = null;
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));

		name = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);
		try {

			ValueSet testAccountName = new ValueSet(name.getName());
			testAccountName.addValue(new Value("salutation", "Herr"));
			testAccountName.addValue(new Value("firstname", "Peter"));
			testAccountName.addValue(new Value("lastname", "Silie"));
			testAccountName.setAccount(account);

			result1 = getService().setValues(account.getUuid(), testAccountName, false);
			Assert.assertNotNull(result1);
			Assert.assertNotNull(result1.getId());
		} catch (Exception e) {
			throw e;
		}
		try {
			Value valueResult1 = findNameInList(result1.getValues(), "salutation");
			valueResult1.setValue("Frau");
			result1.setAccount(account);
			result1.setAttributeGroup(name);
			ValueSet result2 = getService().setValues(account.getUuid(), result1, false);
			result2 = getService().getValues(account.getUuid()).get(0);
			Value valueResult2 = findNameInList(result2.getValues(), "salutation");
			Assert.assertFalse(valueResult2.getValue().equals("Herr"));

			@SuppressWarnings("unused")
			ValueSet result3 = findIdInList(getService().getValues(account.getUuid()), result1.getId());

			// Value valueResult3 = findNameInList(result3.getValues(),
			// "salutation");
			// Assert.assertFalse(oldValue.equals(valueResult3.getValue()));
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void setAndChangeValueByList() throws Exception {
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));

		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		ValueSet testAccountName = new ValueSet(name.getName());
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));

		ValueSet result1 = getService().setValues(account.getUuid(), testAccountName, false);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		ValueSet result2 = findIdInList(getService().getValues(account.getUuid()), result1.getId());

		Value valueResult2 = findNameInList(result1.getValues(), "salutation");
		valueResult2.setValue("Frau");

		ValueSet result3 = getService().setValues(account.getUuid(), result2, false);

		compareBeansStringProperties(result1, result3, new String[] { "id" }, new String[] {});

		Value valueResult3 = findNameInList(result3.getValues(), "salutation");
		compareBeansStringProperties(valueResult2, valueResult3, new String[] { "id" }, new String[] { "value" });
	}

	@Test
	public void setAndDeleteValues() throws Exception {
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));

		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		ValueSet testAccountName = new ValueSet(name.getName());
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));

		ValueSet result1 = getService().setValues(account.getUuid(), testAccountName, false);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		Assert.assertEquals(3, result1.getValues().size());
		
		getService().deleteValues(account.getUuid(), result1, false);

		getService().deleteValues(account.getUuid(), result1, false);

		List<ValueSet> resultList = getService().getValues(account.getUuid());
		try {
			findIdInList(resultList, result1.getId());
			Assert.fail("Id " + result1.getId() + " already deleted, but exists in bean list: " + resultList);
		} catch (AssertionError e) {
			// correct
		}
	}

	@Test
	public void setAndDeleteAttributeWithValues() throws Exception {
		// TODO check this scenario again
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		Attribute salution = new Attribute("salutation-test", "Anrede");
		name.addAttribute(salution);
		name.addAttribute(new Attribute("firstname-test", "Vorname"));
		name.addAttribute(new Attribute("lastname-test", "Nachname"));

		name = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);
		for (Attribute attribute : name.getAttributes()) {
			if (attribute.getName().equals(salution.getName()))
				salution = attribute;
		}
		Assert.assertNotNull(salution.getId());
		ValueSet testAccountName = new ValueSet(name.getName());
		testAccountName.addValue(new Value("salutation-test", "Herr"));
		testAccountName.addValue(new Value("firstname-test", "Peter"));
		testAccountName.addValue(new Value("lastname-test", "Silie"));

		ValueSet result1 = getService().setValues(account.getUuid(), testAccountName, false);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		// name =
		// this.getServiceProvider().getPrivateDataConfigurationService().
		// setAttributeGroup(client.getName(), name);
		this.getServiceProvider().getPrivateDataConfigurationService().deleteAttribute(client.getName(), salution);
		AttributeGroup attributeGroup = this.getService().getAttributeGroupById(client.getName(), name.getId());

		Assert.assertEquals(2, attributeGroup.getAttributes().size());
		Assert.assertFalse(attributeGroup.getAttributes().contains(salution));
	}

	/*TODO ? value sets does not carry approvals throw xml context */
	//@Test
	public void setAndApproveValues() throws Exception {
		try {
			/* Create client, attribute group with some attributes */
			Client client = this.getCurrentClient(true);

			AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
			name.addAttribute(new Attribute("salutation", "Anrede"));
			name.addAttribute(new Attribute("firstname", "Vorname"));
			name.addAttribute(new Attribute("lastname", "Nachname"));
			Assert.assertEquals(3, name.getAttributes().size());
			getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

			/* create application and account */
			ApplicationManagement appService = getServiceProvider().getApplicationManagementService();
			Application application = new Application("testApp" + getName(), "Test-App", client);
			application = appService.setApplication(client.getName(), application);

			Account account = this.getCurrentAccount(client, true);

			/* create a value set and add an approval for one value */
			ValueSet testAccountName = new ValueSet(name.getName());
			Approval valueSetApproval = new Approval(application, account);
			valueSetApproval.setApprovedValueSet(testAccountName);
			testAccountName.setApprovals(Collections.singleton(valueSetApproval));
			Value salutation = new Value("salutation", "Herr");

			// TODO approval of single values !!!
			// Approval valueApproval = new Approval(application, account);
			// valueApproval.setApprovedAttributeValue(salutation);
			// salutation.setApprovals(Collections.singleton(valueSetApproval));
			testAccountName.addValue(salutation);
			testAccountName.addValue(new Value("firstname", "Peter"));
			testAccountName.addValue(new Value("lastname", "Silie"));

			/* Now try to persist value set with approval for one value */
			ValueSet result2 = getService().setValues(account.getUuid(), testAccountName, false);
			Assert.assertNotNull(result2);
			Assert.assertNotNull(result2.getId());
			// TODO update approvals on set
			Assert.assertEquals(1, result2.getApprovals().size());
			// Assert.assertEquals(1,
			// result2.getValue("salutation").getApprovals().size());

			getService().getValuesById(account.getUuid(), result2.getId());
			// Assert.assertEquals(2, result3.getApprovals().size());

			// Assert.assertEquals(1,
			// result3.getValue("salutation").getApprovals().size());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void setAndDeleteLockedValues() throws Exception {
		/*
		 * create and persist client, account and attribute group with some attributes
		 */
		Client client = this.getCurrentClient(true);
		Account account = this.getCurrentAccount(client, true);
		AttributeGroup attributeGroup = new AttributeGroup("setAndDeleteLockedValues" + getName(), "Name");
		attributeGroup.addAttribute(new Attribute("salutation", "Anrede"));
		attributeGroup.addAttribute(new Attribute("firstname", "Vorname"));
		attributeGroup.addAttribute(new Attribute("lastname", "Nachname"));
		attributeGroup = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), attributeGroup);

		/* create and persist a value set */
		ValueSet valueSet = new ValueSet(attributeGroup.getName());
		valueSet.addValue(new Value("firstname", "Clark"));
		Value value = new Value("salutation", "Mr.");
		value.setLocked(true);
		valueSet.addValue(value);
		valueSet.addValue(new Value("lastname", "Kent"));
		try {
			valueSet = getService().setValues(account.getUuid(), valueSet, false);
			Assert.assertFalse(valueSet.isLocked());
			Assert.assertTrue(valueSet.isLockedRecursive());
			for (Value testValue : valueSet.getValues()) {
				if (!testValue.getName().equals("salutation"))
					Assert.assertFalse(testValue.isLocked());
				else
					Assert.assertTrue(testValue.isLocked());
			}
			this.getService().deleteValues(account.getUuid(), valueSet, false);
			Assert.fail("Excepted exception has not been thrown!");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_LOCKED_VALUESET, e.getErrorCode());
		}
	}

	@Test
	public void setAndModifyLockedValues() throws Exception {
		/*
		 * create and persist client, account and attribute group with some attributes
		 */

		AttributeGroup attributeGroup = new AttributeGroup("setAndDeleteLockedValues" + getName(), "Name");
		attributeGroup.addAttribute(new Attribute("salutation", "Anrede"));
		attributeGroup.addAttribute(new Attribute("firstname", "Vorname"));
		attributeGroup.addAttribute(new Attribute("lastname", "Nachname"));
		attributeGroup = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), attributeGroup);

		/* create and persist a value set */
		ValueSet valueSet = new ValueSet(attributeGroup.getName());
		valueSet.addValue(new Value("firstname", "Clark"));
		Value value = new Value("salutation", "Mr.");
		value.setLocked(true);
		valueSet.addValue(value);
		valueSet.addValue(new Value("lastname", "Kent"));
		try {
			/* Persist value set */
			valueSet = getService().setValues(account.getUuid(), valueSet, false);
			/* check the result */
			Assert.assertFalse(valueSet.isLocked());
			Assert.assertTrue(valueSet.isLockedRecursive());
			for (Value testValue : valueSet.getValues()) {
				if (!testValue.getName().equals("salutation"))
					Assert.assertFalse(testValue.isLocked());
				else
					Assert.assertTrue(testValue.isLocked());
			}

			/* Try to update the value set without modifications */
			valueSet = getService().setValues(account.getUuid(), valueSet, false);
			Assert.assertEquals(3, valueSet.getValues().size());

			/* check locking informations */
			Assert.assertFalse(valueSet.isLocked());
			Assert.assertTrue(valueSet.isLockedRecursive());
			for (Value testValue : valueSet.getValues()) {
				if (!testValue.getName().equals("salutation"))
					Assert.assertFalse(testValue.isLocked());
				else
					Assert.assertTrue(testValue.isLocked());
			}
			/* Try to update the value set with a modified unlocked value */
			Value valueToModify = valueSet.getValue("firstname");
			valueToModify.setValue("Kenny");
			valueSet = getService().setValues(account.getUuid(), valueSet, false);
			valueSet = this.getService().getValues(account.getUuid()).get(0);
			Assert.assertEquals(3, valueSet.getValues().size());
			Value controlValue = valueSet.getValue("firstname");
			Assert.assertEquals("Kenny", controlValue.getValue());
			/* check locking informations */
			Assert.assertFalse(valueSet.isLocked());
			Assert.assertTrue(valueSet.isLockedRecursive());
			for (Value testValue : valueSet.getValues()) {
				if (!testValue.getName().equals("salutation"))
					Assert.assertFalse(testValue.isLocked());
				else
					Assert.assertTrue(testValue.isLocked());
			}
			/* Try to delete an unlocked value */
			valueSet.getValues().remove(controlValue);
			valueSet = getService().setValues(account.getUuid(), valueSet, false);
			valueSet = this.getService().getValues(account.getUuid()).get(0);
			Assert.assertEquals(2, valueSet.getValues().size());
			Assert.assertNull(valueSet.getValue("firstname"));

			/* check locking informations */
			Assert.assertFalse(valueSet.isLocked());
			Assert.assertTrue(valueSet.isLockedRecursive());
			for (Value testValue : valueSet.getValues()) {
				if (!testValue.getName().equals("salutation"))
					Assert.assertFalse(testValue.isLocked());
				else
					Assert.assertTrue(testValue.isLocked());
			}

			/* try to delete the complete value set */
			try {
				this.getService().deleteValues(account.getUuid(), valueSet, false);
				Assert.fail("Excepted exception has not been thrown!");
			} catch (IllegalRequestException e) {
				Assert.assertEquals(IllegalRequestException.CODE_LOCKED_VALUESET, e.getErrorCode());
			}

			/* try to modify a locked value from value set */
			try {
				valueToModify = valueSet.getValue("salutation");
				valueToModify.setValue("Master");
				valueSet = getService().setValues(account.getUuid(), valueSet, false);
				Assert.fail("Excepted exception has not been thrown!");
			} catch (IllegalRequestException e) {
				Assert.assertEquals(IllegalRequestException.CODE_LOCKED_VALUESET, e.getErrorCode());
			}

			/* try to remove a locked value from value set */
			try {
				valueToModify = valueSet.getValue("salutation");
				valueToModify.setValue("Mr.");
				valueSet.getValues().remove(valueSet.getValue("salutation"));
				valueSet = getService().setValues(account.getUuid(), valueSet, false);
				Assert.fail("Excepted exception has not been thrown!");
			} catch (IllegalRequestException e) {
				Assert.assertEquals(IllegalRequestException.CODE_LOCKED_VALUESET, e.getErrorCode());
			}

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void setValueSetAndDeleteSingleValue() throws Exception {
		AttributeGroup attributeGroup = new AttributeGroup("sVSAndDsV" + getName(), "Name");
		attributeGroup.addAttribute(new Attribute("salutation", "Anrede"));
		attributeGroup.addAttribute(new Attribute("firstname", "Vorname"));
		attributeGroup.addAttribute(new Attribute("lastname", "Nachname"));
		// ttributeGroup.setDisplayName("Test");
		attributeGroup = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), attributeGroup);

		/* create and persist a value set */
		ValueSet valueSet = new ValueSet(attributeGroup.getName());
		valueSet.addValue(new Value("firstname", "Bruce"));
		valueSet.addValue(new Value("lastname", "Wayne"));
		Value value = new Value("salutation", "Mr.");
		valueSet.addValue(value);

		try {
			valueSet = getService().setValues(account.getUuid(), valueSet, false);
			value = valueSet.getValue("lastname");
			Assert.assertNotNull(value.getId());
			/*
			 * now remove the single value from value set and try to persist the change
			 */
			valueSet.deleteValueById(value.getId());
			valueSet = getService().setValues(account.getUuid(), valueSet, false);
			valueSet = this.getService().getValuesById(account.getUuid(), valueSet.getId());
			Assert.assertEquals(2, valueSet.getValues().size());
			Assert.assertNotNull(valueSet.getValue("firstname"));
			Assert.assertNotNull(valueSet.getValue("salutation"));

		} catch (Exception e) {
			throw e;
		}
	}

	// TODO temporary removed test until approval structure implementation
	// completed @Test
	public void removeApprovedValue() throws Exception {
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		Client client = this.getCurrentClient(true);
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));
		Assert.assertEquals(3, name.getAttributes().size());

		AttributeGroup result1 = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		Assert.assertEquals(3, result1.getAttributes().size());

		result1 = getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), result1);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		Assert.assertEquals(3, result1.getAttributes().size());

		ApplicationManagement appService = getServiceProvider().getApplicationManagementService();
		Application application = new Application("testApp" + getName(), "Test-App", client);
		application = appService.setApplication(client.getName(), application);

		Account account = this.getCurrentAccount(client, true);

		ValueSet testAccountName = new ValueSet(name.getName());
		Value salutation = new Value("salutation", "Herr");
		salutation.setApprovals(Collections.singleton(new Approval(application, account)));
		testAccountName.addValue(salutation);
		Value firstName = new Value("firstname", "Peter");
		firstName.setApprovals(Collections.singleton(new Approval(application, account)));
		testAccountName.addValue(firstName);
		testAccountName.addValue(new Value("lastname", "Parker"));

		ValueSet result2 = getService().setValues(account.getUuid(), testAccountName, false);
		Assert.assertNotNull(result2);
		Assert.assertNotNull(result2.getId());
		firstName = result2.getValue("firstname");
		salutation = result2.getValue("salutation");
		Assert.assertEquals(1, firstName.getApprovals().size());
		Assert.assertEquals(1, salutation.getApprovals().size());

		Value removeValue = result2.getValue("salutation");
		result2.deleteValueById(removeValue.getId());

		ValueSet result3 = getService().setValues(account.getUuid(), result2, false);
		ValueSet result4 = this.getService().getValuesById(account.getUuid(), result2.getId());
		Assert.assertEquals(2, result4.getValues().size());
		firstName = result3.getValue("firstname");
		Assert.assertEquals(1, firstName.getApprovals().size());

	}

	@Test
	public void setAndGetFileValue() throws Exception {
		byte[] file = new byte[1000];
		for (int i = 0; i < 999; i++) {
			file[i] = (byte) (i % 128);
		}
		AttributeGroup name = new AttributeGroup("name" + getName(), "ExtendedName");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));
		Attribute foto = new Attribute("photo", "Foto");
		Attribute foto2 = new Attribute("photo2", "Foto2");
		foto.setType(AttributeType.BLOB);
		foto2.setType(AttributeType.BLOB);
		name.addAttribute(foto);
		name.addAttribute(foto2);

		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		ValueSet testAccountName = new ValueSet(name.getName());
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));
		Value fotofile = new Value("photo", "myPicture.jpg");
		fotofile.setByteValue(file);
		testAccountName.addValue(fotofile);
		ValueSet result1 = getService().setValues(account.getUuid(), testAccountName, false);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		List<ValueSet> resultList = getService().getValues(account.getUuid());
		Assert.assertEquals(1, resultList.size());
		fotofile = resultList.get(0).getValue("photo");
		Assert.assertNotNull(fotofile);
		Assert.assertNotNull(fotofile.getId());
		Value controlValue = this.getService().getValueWithBinaryAttachmentById(account.getUuid(), fotofile.getId());
		Assert.assertNotNull(controlValue.getByteValue());
		ValueSet result2 = findIdInList(resultList, result1.getId());
		compareBeansStringProperties(result1, result2, baseProperties, null);

		/* now do a merge of this value set */
		Value fotofile2 = new Value("photo2", "myPicture2.jpg");
		fotofile2.setByteValue(file);
		resultList.get(0).addValue(fotofile2);
		result1 = this.getService().setValues(account.getUuid(), resultList.get(0), false);
		// Assert.assertEquals(4, result1.getValues().size());
		fotofile = result1.getValue("photo");
		fotofile2 = result1.getValue("photo2");
		Assert.assertNotNull(fotofile);
		Assert.assertNotNull(fotofile.getId());
		Assert.assertNotNull(fotofile2);
		Assert.assertNotNull(fotofile2.getId());
		Assert.assertEquals("myPicture.jpg", fotofile.getValue());
		Assert.assertEquals("myPicture2.jpg", fotofile2.getValue());
		Assert.assertNull(fotofile.getByteValue());
		Assert.assertNull(fotofile2.getByteValue());
		result1 = this.getService().getValues(account.getUuid()).get(0);
		Assert.assertEquals(5, result1.getValues().size());
		controlValue = this.getService().getValueWithBinaryAttachmentById(account.getUuid(), fotofile.getId());
		Assert.assertNotNull(controlValue.getByteValue());
		// Assert.assertEquals(file, controlValue.getByteValue());
		controlValue = this.getService().getValueWithBinaryAttachmentById(account.getUuid(), fotofile2.getId());
		Assert.assertNotNull(controlValue.getByteValue());
		// Assert.assertEquals(file, controlValue.getByteValue());
	}

	@Test
	public void setApproval() throws Exception {
		/*
		 * Create the scene client and attribute group
		 */
		Client client = this.getCurrentClient(true);
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));
		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		/* application and account */
		ApplicationManagement appService = getServiceProvider().getApplicationManagementService();
		Application application = new Application("testApp" + getName(), "Test-App", client);
		application = appService.setApplication(client.getName(), application);

		Account account = this.getCurrentAccount(client, true);

		/* a value set with some approved values */
		ValueSet testAccountName = new ValueSet(name.getName());
		Value salutation = new Value("salutation", "Miss");
		testAccountName.addValue(salutation);
		Value firstName = new Value("firstname", "Vicky");
		testAccountName.addValue(firstName);
		testAccountName.addValue(new Value("lastname", "Vale"));

		testAccountName = getService().setValues(account.getUuid(), testAccountName, false);

		try {
			/* create and persist approval for the value set */
			Approval valueSetApproval = new Approval(application, account, testAccountName);
			valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
			Assert.assertNotNull(valueSetApproval);
			Assert.assertNotNull(valueSetApproval.getId());
			Assert.assertNotNull(valueSetApproval.getApprovedValueSet());

			/* create and persist approval for a single value */
			salutation = testAccountName.getValue("salutation");
			Approval salutationApproval = new Approval(application, account, salutation);
			salutationApproval = this.getService().setApproval(client.getName(), salutationApproval);
			Assert.assertNotNull(salutationApproval);
			Assert.assertNotNull(salutationApproval.getId());
			Assert.assertNotNull(salutationApproval.getApprovedAttributeValue());

			/*
			 * Try to do updates
			 */
			try {
				/* setting a new account to an persistent approval is illegal */
				Account newAccount = this.getCurrentAccount(client, true);
				valueSetApproval.setAccount(newAccount);
				valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
				Assert.fail("Expected exception has not been thrown");
			} catch (IllegalRequestException e) {
				Assert.assertEquals(IllegalRequestException.CODE_FORBIDDEN_APPROVAL_MODIFICATION, e.getErrorCode());
				valueSetApproval.setAccount(account);
			}
			try {
				/* setting a new value to an persistent approval is illegal */
				valueSetApproval.setApprovedAttributeValue(salutation);
				valueSetApproval.setApprovedValueSet(null);
				valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
				Assert.fail("Expected exception has not been thrown");
			} catch (IllegalRequestException e) {
				Assert.assertEquals(IllegalRequestException.CODE_FORBIDDEN_APPROVAL_MODIFICATION, e.getErrorCode());
				valueSetApproval.setApprovedValueSet(testAccountName);
				valueSetApproval.setApprovedAttributeValue(null);
			}
			try {
				/* setting a new value set to an persistent approval is illegal */
				ValueSet testAccountAlias = new ValueSet(name.getName());
				Value salutationAlias = new Value("salutation", "Miss");
				testAccountAlias.addValue(salutationAlias);
				Value firstNameAlias = new Value("firstname", "Vicky");
				testAccountAlias.addValue(firstNameAlias);
				testAccountAlias.addValue(new Value("lastname", "Vale"));

				testAccountAlias = getService().setValues(account.getUuid(), testAccountAlias, false);

				valueSetApproval.setApprovedValueSet(testAccountAlias);

				valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
				Assert.fail("Expected exception has not been thrown");
			} catch (IllegalRequestException e) {
				Assert.assertEquals(IllegalRequestException.CODE_FORBIDDEN_APPROVAL_MODIFICATION, e.getErrorCode());
				valueSetApproval.setApprovedValueSet(testAccountName);
			}

			/* setting a new application to an persistent approval is allowed and should work */
			Application newApplication = this.getCurrentApplication(client, true);
			valueSetApproval.setApplication(newApplication);

			valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void setIllegalApproval() throws Exception {
		/*
		 * Create the scene client and attribute group
		 */
		Client client = this.getCurrentClient(true);
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));
		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		/* application and account */
		ApplicationManagement appService = getServiceProvider().getApplicationManagementService();
		Application application = new Application("testApp" + getName(), "Test-App", client);
		application = appService.setApplication(client.getName(), application);

		Account account = this.getCurrentAccount(client, true);

		/* a value set with some approved values */
		ValueSet testAccountName = new ValueSet(name.getName());
		Value salutation = new Value("salutation", "Miss");
		testAccountName.addValue(salutation);
		Value firstName = new Value("firstname", "Vicky");
		testAccountName.addValue(firstName);
		testAccountName.addValue(new Value("lastname", "Vale"));

		try {
			/* create and try to persist illegal approvals for the value set */
			Approval valueSetApproval = null;
			try {
				/* The value set is not persistent */
				valueSetApproval = new Approval(application, account, testAccountName);
				valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
				Assert.fail("Expected exception has not been thrown");
			} catch (IllegalRequestException e) {
				Assert.assertEquals(IllegalRequestException.CODE_INVALID_VALUE_TO_APPROVE, e.getErrorCode());
			}
			testAccountName = getService().setValues(account.getUuid(), testAccountName, false);
			try {
				/* application is absent */
				valueSetApproval = new Approval(null, account, testAccountName);
				valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
				Assert.fail("Expected exception has not been thrown");
			} catch (BackendException e) {
				Assert.assertEquals(IllegalRequestException.CODE_VALIDATION_ERROR, e.getErrorCode());
			}
			/* account is absent */
			try {
				valueSetApproval = new Approval(application, null, testAccountName);
				valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
				Assert.fail("Expected exception has not been thrown");
			} catch (IllegalRequestException e) {
				Assert.assertEquals(IllegalRequestException.CODE_VALIDATION_ERROR, e.getErrorCode());
			}

			/* value set is not configured */
			try {
				valueSetApproval = new Approval(application, account, new ValueSet());
				valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
				Assert.fail("Expected exception has not been thrown");
			} catch (IllegalRequestException e) {
				Assert.assertEquals(IllegalRequestException.CODE_INVALID_VALUE_TO_APPROVE, e.getErrorCode());
			}
			/* client is absent - both as parameter and hidden in approval */
			try {
				application.setClient(null);
				account.setClient(null);
				valueSetApproval = new Approval(application, account, testAccountName);
				valueSetApproval = this.getService().setApproval(null, valueSetApproval);
				Assert.fail("Expected exception has not been thrown");
			} catch (BackendException e) {
				Assert.assertEquals(BackendException.CODE_CLIENT_NOT_AVAILABLE, e.getErrorCode());
			}
			/* storing an approval without client in approval structure should be possible */
			try {
				application.setClient(null);
				account.setClient(null);
				valueSetApproval = new Approval(application, account, testAccountName);
				if (valueSetApproval.getClientName() != null)
					Assert.fail("Test execution not possible because the client is set inside the approval structure");
				valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
			} catch (IllegalRequestException e) {
				throw e;
			}

			/* storing an approval without client as parameter but hidden in structure should be possible */
			try {
				application.setClient(client);
				account.setClient(client);
				/* a value set with some approved values */
				ValueSet testAccountName2 = new ValueSet(name.getName());
				Value salutation2 = new Value("salutation", "Miss");
				testAccountName2.addValue(salutation2);
				Value firstName2 = new Value("firstname", "Vicky");
				testAccountName2.addValue(firstName2);
				testAccountName2.addValue(new Value("lastname", "Vale"));
				testAccountName2 = this.getService().setValues(account.getUuid(), testAccountName2, false);

				valueSetApproval = new Approval(application, account, testAccountName2);
				if (valueSetApproval.getClientName() != null)
					valueSetApproval = this.getService().setApproval(null, valueSetApproval);
				else
					Assert.fail("Test execution not possible because client is not available from created approval");
			} catch (IllegalRequestException e) {
				throw e;
			}

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void setApprovalForAnotherAccount() throws Exception {
		/*
		 * Create the scene client and attribute group
		 */
		Client client = this.getCurrentClient(true);
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));
		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		/* application and account */
		ApplicationManagement appService = getServiceProvider().getApplicationManagementService();
		Application application = new Application("testApp" + getName(), "Test-App", client);
		application = appService.setApplication(client.getName(), application);

		Account account = this.getCurrentAccount(client, true);

		/* a value set with some approved values */
		ValueSet testAccountName = new ValueSet(name.getName());
		Value salutation = new Value("salutation", "Miss");
		testAccountName.addValue(salutation);
		Value firstName = new Value("firstname", "Vicky");
		testAccountName.addValue(firstName);
		testAccountName.addValue(new Value("lastname", "Vale"));

		testAccountName = getService().setValues(account.getUuid(), testAccountName, false);

		/* create another account that should be assigned to the approval */
		Account secondAccount = this.getCurrentAccount(client, true);

		try {
			/* create and persist approval for the value set */
			Approval valueSetApproval = new Approval(application, secondAccount, testAccountName);
			valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
			Assert.fail("Expected exception has not been thrown");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_PARAMETER_COMBINATION, e.getErrorCode());
		}
		try {
			/* create and persist approval for a single value */
			salutation = testAccountName.getValue("salutation");
			Approval salutationApproval = new Approval(application, secondAccount, salutation);
			salutationApproval = this.getService().setApproval(client.getName(), salutationApproval);
			Assert.fail("Expected exception has not been thrown");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_PARAMETER_COMBINATION, e.getErrorCode());
		}
	}

	@Test
	public void getApprovalsByAccount() throws Exception {
		/*
		 * Create the scene client and attribute group
		 */
		Client client = this.getCurrentClient(true);
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));
		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		/* application and account */
		ApplicationManagement appService = getServiceProvider().getApplicationManagementService();
		Application application = new Application("testApp" + getName(), "Test-App", client);
		application = appService.setApplication(client.getName(), application);

		Account account = this.getCurrentAccount(client, true);

		/* a value set with some approved values */
		ValueSet testAccountName = new ValueSet(name.getName());
		Value salutation = new Value("salutation", "Dr.");
		testAccountName.addValue(salutation);
		Value firstName = new Value("firstname", "David");
		testAccountName.addValue(firstName);
		testAccountName.addValue(new Value("lastname", "Banner"));

		/* a value set that is approved */
		ValueSet testAccountAlias = new ValueSet(name.getName());
		Value salutationAlias = new Value("salutation", "Mr.");
		testAccountAlias.addValue(salutationAlias);
		Value firstNameAlias = new Value("firstname", "Steve");
		testAccountAlias.addValue(firstNameAlias);
		testAccountAlias.addValue(new Value("lastname", "Austin"));
		// TODO testAccountAlias.setApprovals(Collections.singleton(new Approval(application, account, testAccountAlias)));

		/* another value set where only some values are approved */
		testAccountAlias = getService().setValues(account.getUuid(), testAccountAlias, false);
		testAccountName = getService().setValues(account.getUuid(), testAccountName, false);
		Approval valueSetApproval = new Approval(application, account, testAccountAlias);
		valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);

		/* persistent version is needed to persist a new approval */
		firstName = testAccountName.getValue("firstname");
		salutation = testAccountName.getValue("salutation");
		Approval salutationApproval = new Approval(application, account, salutation);
		Approval firstNameApproval = new Approval(application, account, firstName);
		try {
			salutationApproval = this.getService().setApproval(client.getName(), salutationApproval);
			firstNameApproval = this.getService().setApproval(client.getName(), firstNameApproval);
		} catch (Exception e) {
			throw e;
		}

		/*
		 * Create another account with a value set and some approvals to proof that only the expected approvals will be returned
		 */
		Account secondAccount = this.getCurrentAccount(client, true);
		Application secondApplication = this.getCurrentApplication(client, true);

		ValueSet secondAccountName = new ValueSet(name.getName());
		Value secondSalutation = new Value("salutation", "Mr.");
		secondAccountName.addValue(secondSalutation);
		Value secondFirstName = new Value("firstname", "Steve");
		secondAccountName.addValue(secondFirstName);
		secondAccountName.addValue(new Value("lastname", "Austin"));

		secondAccountName = getService().setValues(secondAccount.getUuid(), secondAccountName, false);

		/* persistent version is needed to persist a new approval */
		secondFirstName = secondAccountName.getValue("firstname");
		secondSalutation = secondAccountName.getValue("salutation");
		/* one value to the same application */
		Approval secondSalutationApproval = new Approval(secondApplication, secondAccount, secondSalutation);
		/* the other to a second application */
		Approval secondFirstNameApproval = new Approval(application, secondAccount, secondFirstName);

		secondSalutationApproval = this.getService().setApproval(client.getName(), secondSalutationApproval);
		secondFirstNameApproval = this.getService().setApproval(client.getName(), secondFirstNameApproval);

		/*
		 * try to obtain the approvals and examine the result
		 */
		List<Approval> approvals = this.getService().getApprovalsByAccount(account.getUuid());
		Assert.assertNotNull(approvals);
		Assert.assertEquals(3, approvals.size());
		boolean valueSetApprovalExists = false;
		boolean salutationApprovalExists = false;
		boolean firstNameApprovalExists = false;
		for (Approval approval : approvals) {
			ValueSet approvedValueSet = approval.getApprovedValueSet();
			Value approvedValue = approval.getApprovedAttributeValue();

			if (approvedValueSet != null && approvedValueSet.getId().equals(testAccountAlias.getId()))
				valueSetApprovalExists = true;
			if (approvedValue != null && approvedValue.getId().equals(testAccountName.getValue("salutation").getId()))
				salutationApprovalExists = true;
			if (approvedValue != null && approvedValue.getId().equals(testAccountName.getValue("firstname").getId()))
				firstNameApprovalExists = true;
		}
		Assert.assertTrue(valueSetApprovalExists && salutationApprovalExists && firstNameApprovalExists);
	}

	@Test
	public void getApprovalsByUuidAndApplicationId() throws Exception {
		/*
		 * Create the scene client and attribute group
		 */
		Client client = this.getCurrentClient(true);
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));
		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		/* application and account */
		ApplicationManagement appService = getServiceProvider().getApplicationManagementService();
		Application application = new Application("testApp" + getName(), "Test-App", client);
		application = appService.setApplication(client.getName(), application);

		Account account = this.getCurrentAccount(client, true);

		/* a value set with some approved values */
		ValueSet testAccountName = new ValueSet(name.getName());
		Value salutation = new Value("salutation", "Meister");
		testAccountName.addValue(salutation);
		Value firstName = new Value("firstname", "Obiwan");
		testAccountName.addValue(firstName);
		testAccountName.addValue(new Value("lastname", "Kenobi"));

		testAccountName = getService().setValues(account.getUuid(), testAccountName, false);

		Approval valueSetApproval = new Approval(application, account, testAccountName);
		valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
		firstName = testAccountName.getValue("firstname");
		salutation = testAccountName.getValue("salutation");

		Approval salutationApproval = new Approval(application, account, salutation);
		Approval firstNameApproval = new Approval(application, account, firstName);

		salutationApproval = this.getService().setApproval(client.getName(), salutationApproval);
		firstNameApproval = this.getService().setApproval(client.getName(), firstNameApproval);

		/*
		 * Add some more data to proof that not more is returned than expected
		 */

		/* Approve the value set for the second application */
		Application secondApplication = this.getCurrentApplication(client, true);
		Approval valueSetApprovalForSecondApplication = new Approval(secondApplication, account, testAccountName);
		valueSetApprovalForSecondApplication = this.getService().setApproval(client.getName(), valueSetApprovalForSecondApplication);

		/* Add one more value set and approval for a different account and a different application */
		Account secondAccount = this.getCurrentAccount(client, true);

		ValueSet secondTestAccountName = new ValueSet(name.getName());
		Value secondSalutation = new Value("salutation", "Mr.");
		secondTestAccountName.addValue(secondSalutation);
		Value secondFirstName = new Value("firstname", "Jabba");
		secondTestAccountName.addValue(secondFirstName);
		secondTestAccountName.addValue(new Value("lastname", "the Hutt"));

		secondTestAccountName = getService().setValues(secondAccount.getUuid(), secondTestAccountName, false);

		/* approve the second value set to the same (requested) application */
		Approval secondValueSetApproval = new Approval(application, secondAccount, secondTestAccountName);
		secondValueSetApproval = this.getService().setApproval(client.getName(), secondValueSetApproval);

		/* approve the second value set to a second application */
		Approval thirdValueSetApproval = new Approval(secondApplication, secondAccount, secondTestAccountName);
		thirdValueSetApproval = this.getService().setApproval(client.getName(), thirdValueSetApproval);

		/*
		 * execute the request and examine the result.
		 */
		List<Approval> approvals = this.getService().getApprovalsByUuidAndApplicationId(account.getUuid(), application.getId());
		Assert.assertNotNull(approvals);
		Assert.assertEquals(3, approvals.size());
		findIdInList(approvals, valueSetApproval.getId());
		findIdInList(approvals, salutationApproval.getId());
		findIdInList(approvals, firstNameApproval.getId());
		for (Approval approval : approvals) {
			Assert.assertTrue(approval.getApprovedAttributeValue() != null ^ approval.getApprovedValueSet() != null);
			Assert.assertNotNull(approval.getApplication());
			Assert.assertNotNull(approval.getAccount());
		}

	}

	@Test
	public void getApprovalsByApplicationId() throws Exception {
		/*
		 * Create the scene: client and attribute group
		 */
		Client client = this.getCurrentClient(true);
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));
		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		/* application and account */
		ApplicationManagement appService = getServiceProvider().getApplicationManagementService();
		Application application = new Application("testApp" + getName(), "Test-App", client);
		application = appService.setApplication(client.getName(), application);

		Account account = this.getCurrentAccount(client, true);

		/* a value set with some approved values */
		ValueSet testAccountName = new ValueSet(name.getName());
		Value salutation = new Value("salutation", "Meister");
		testAccountName.addValue(salutation);
		Value firstName = new Value("firstname", "Obiwan");
		testAccountName.addValue(firstName);
		testAccountName.addValue(new Value("lastname", "Kenobi"));

		testAccountName = getService().setValues(account.getUuid(), testAccountName, false);

		Approval valueSetApproval = new Approval(application, account, testAccountName);
		valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
		firstName = testAccountName.getValue("firstname");
		salutation = testAccountName.getValue("salutation");

		Approval salutationApproval = new Approval(application, account, salutation);
		Approval firstNameApproval = new Approval(application, account, firstName);

		salutationApproval = this.getService().setApproval(client.getName(), salutationApproval);
		firstNameApproval = this.getService().setApproval(client.getName(), firstNameApproval);

		/*
		 * Add some more data to prove that not more is returned than expected
		 */

		/* Approve the value set for the second application */
		Application secondApplication = this.getCurrentApplication(client, true);
		Approval valueSetApprovalForSecondApplication = new Approval(secondApplication, account, testAccountName);
		valueSetApprovalForSecondApplication = this.getService().setApproval(client.getName(), valueSetApprovalForSecondApplication);

		/* Add one more value set and approval for a different account and a different application */
		Account secondAccount = this.getCurrentAccount(client, true);

		ValueSet secondTestAccountName = new ValueSet(name.getName());
		Value secondSalutation = new Value("salutation", "Mr.");
		secondTestAccountName.addValue(secondSalutation);
		Value secondFirstName = new Value("firstname", "Jabba");
		secondTestAccountName.addValue(secondFirstName);
		secondTestAccountName.addValue(new Value("lastname", "the Hutt"));

		secondTestAccountName = getService().setValues(secondAccount.getUuid(), secondTestAccountName, false);

		/* approve the second value set to the same (requested) application */
		Approval secondValueSetApproval = new Approval(application, secondAccount, secondTestAccountName);
		secondValueSetApproval = this.getService().setApproval(client.getName(), secondValueSetApproval);

		/* approve the second value set to a second application */
		Approval thirdValueSetApproval = new Approval(secondApplication, secondAccount, secondTestAccountName);
		thirdValueSetApproval = this.getService().setApproval(client.getName(), thirdValueSetApproval);

		/*
		 * execute the request and examine the result.
		 */
		List<Approval> approvals = this.getService().getApprovalsByApplicationId(client.getName(), application.getId());
		Assert.assertNotNull(approvals);
		Assert.assertEquals(4, approvals.size());
		findIdInList(approvals, valueSetApproval.getId());
		findIdInList(approvals, salutationApproval.getId());
		findIdInList(approvals, firstNameApproval.getId());
		findIdInList(approvals, secondValueSetApproval.getId());
		for (Approval approval : approvals) {
			Assert.assertTrue(approval.getApprovedAttributeValue() != null ^ approval.getApprovedValueSet() != null);
			Assert.assertNotNull(approval.getApplication());
			Assert.assertNotNull(approval.getAccount());
		}

	}

	@Test
	public void deleteApproval() throws Exception {
		/*
		 * Create the scene: client and attribute group
		 */
		Client client = this.getCurrentClient(true);
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));
		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		/* application and account */
		ApplicationManagement appService = getServiceProvider().getApplicationManagementService();
		Application application = new Application("testApp" + getName(), "Test-App", client);
		application = appService.setApplication(client.getName(), application);

		Account account = this.getCurrentAccount(client, true);

		/* a value set with some approved values */
		ValueSet testAccountName = new ValueSet(name.getName());
		Value salutation = new Value("salutation", "Meister");
		testAccountName.addValue(salutation);
		Value firstName = new Value("firstname", "Obiwan");
		testAccountName.addValue(firstName);
		testAccountName.addValue(new Value("lastname", "Kenobi"));

		testAccountName = getService().setValues(account.getUuid(), testAccountName, false);

		Approval valueSetApproval = new Approval(application, account, testAccountName);
		valueSetApproval = this.getService().setApproval(client.getName(), valueSetApproval);
		firstName = testAccountName.getValue("firstname");
		salutation = testAccountName.getValue("salutation");

		Approval salutationApproval = new Approval(application, account, salutation);
		Approval firstNameApproval = new Approval(application, account, firstName);

		salutationApproval = this.getService().setApproval(client.getName(), salutationApproval);
		firstNameApproval = this.getService().setApproval(client.getName(), firstNameApproval);

		/* control request - before delete */
		List<Approval> approvals = this.getService().getApprovalsByAccount(account.getUuid());
		Assert.assertNotNull(approvals);
		Assert.assertEquals(3, approvals.size());
		approvals = this.getService().getApprovalsByApplicationId(client.getName(), application.getId());
		Assert.assertNotNull(approvals);
		Assert.assertEquals(3, approvals.size());

		this.getService().deleteApproval(client.getName(), salutationApproval.getId());

		/* control request - before delete */
		approvals = this.getService().getApprovalsByAccount(account.getUuid());
		Assert.assertNotNull(approvals);
		Assert.assertEquals(2, approvals.size());
		this.findIdInList(approvals, firstNameApproval.getId());
		this.findIdInList(approvals, valueSetApproval.getId());
	}

	@Test
	public void setDuplicateApproval() throws Exception {
		/*
		 * Create the scene: client and attribute group
		 */
		Client client = this.getCurrentClient(true);
		AttributeGroup name = new AttributeGroup("name" + getName(), "Name");
		name.addAttribute(new Attribute("salutation", "Anrede"));
		name.addAttribute(new Attribute("firstname", "Vorname"));
		name.addAttribute(new Attribute("lastname", "Nachname"));
		getServiceProvider().getPrivateDataConfigurationService().setAttributeGroup(client.getName(), name);

		/* application and account */
		ApplicationManagement appService = getServiceProvider().getApplicationManagementService();
		Application application = new Application("testApp" + getName(), "Test-App", client);
		application = appService.setApplication(client.getName(), application);

		Account account = this.getCurrentAccount(client, true);

		/* a value set with some approved values */
		ValueSet testAccountName = new ValueSet(name.getName());
		Value salutation = new Value("salutation", "Meister");
		testAccountName.addValue(salutation);
		Value firstName = new Value("firstname", "Obiwan");
		testAccountName.addValue(firstName);
		testAccountName.addValue(new Value("lastname", "Kenobi"));

		testAccountName = getService().setValues(account.getUuid(), testAccountName, false);

		Approval valueSetApproval = new Approval(application, account, testAccountName);
	
		try {
			this.getService().setApproval(client.getName(), valueSetApproval);
			valueSetApproval.setId(null);
			this.getService().setApproval(client.getName(), valueSetApproval);
			Assert.fail("Expected exception has not been thrown!");
		} catch (BackendException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
	}
}
