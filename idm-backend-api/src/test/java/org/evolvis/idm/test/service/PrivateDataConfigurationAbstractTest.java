package org.evolvis.idm.test.service;

import java.util.LinkedList;
import java.util.List;

import org.evolvis.idm.common.fault.IllegalProperty;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.AttributeType;
import org.evolvis.idm.identity.privatedata.service.PrivateDataConfiguration;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.junit.Assert;
import org.junit.Test;

public abstract class PrivateDataConfigurationAbstractTest extends AbstractBaseServiceAbstractTest<PrivateDataConfiguration> {
	@Test
	public void setAttributes() throws Exception {
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("personalData" + getName(), "Name");
		personalData.addAttribute(new Attribute("salutation", "Anrede"));
		personalData.addAttribute(new Attribute("firstname", "Vorname"));
		personalData.addAttribute(new Attribute("lastname", "Nachname"));

		AttributeGroup address = new AttributeGroup("address" + getName(), "Name", null, true);
		address.addAttribute(new Attribute("street", "Strasse"));
		address.addAttribute(new Attribute("zipcode", "PLZ"));
		address.addAttribute(new Attribute("city", "Ort"));
		address.addAttribute(new Attribute("state", "Bundesland"));
		address.addAttribute(new Attribute("country", "Land"));

		AttributeGroup contact = new AttributeGroup("contact" + getName(), "Kontaktdaten", null, false);
		contact.addAttribute(new Attribute("fon", "Telefon", null, true));
		contact.addAttribute(new Attribute("mobile", "Mobil", null, true));

		AttributeGroup result1 = getService().setAttributeGroup(client.getName(), personalData);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		AttributeGroup result2 = getService().setAttributeGroup(client.getName(), address);
		Assert.assertNotNull(result2);
		Assert.assertNotNull(result2.getId());

		AttributeGroup result3 = getService().setAttributeGroup(client.getName(), contact);
		Assert.assertNotNull(result3);
		Assert.assertNotNull(result3.getId());
	}

	@Test(expected = IllegalRequestException.class)
	public void setAttributesWithIllegalAttribute() throws Exception {
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("personalData" + getName(), "Name");
		// Display name to short
		personalData.addAttribute(new Attribute("salutation", ""));
		try {
			getService().setAttributeGroup(client.getName(), personalData);
		} catch (IllegalRequestException e) {
			Assert.assertNotNull(e.getIllegalProperties());
			List<IllegalProperty> illegalProperties = e.getIllegalProperties();
			Assert.assertEquals(1, illegalProperties.size());
			Assert.assertEquals("Size", illegalProperties.get(0).getConstraintType());
			throw e;
		}
	}

	@Test(expected = IllegalRequestException.class)
	public void setAttributesWithoutData() throws Exception {
		Client client = this.getCurrentClient(true);
		getService().setAttributeGroup(client.getName(), new AttributeGroup());
	}

	// TODO ausbauen
	@Test
	public void setAttributesDuplicates() throws Exception {
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("personalData" + getName(), "Name");
		Attribute salutation = new Attribute("salutation", "Anrede");
		Attribute firstName = new Attribute("firstname", "Vorname");
		Attribute lastName = new Attribute("lastname", "Nachname");
		personalData.addAttribute(salutation);
		personalData.addAttribute(firstName);
		personalData.addAttribute(lastName);
		getService().setAttributeGroup(client.getName(), personalData);

		/* scene: try to persist a new attribute group with same properties like another persistent one. */
		try {
			personalData.setId(null);
			getService().setAttributeGroup(client.getName(), personalData);
			Assert.fail(NO_EXCEPTION);
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}

		/* scene: try to update an already persistent attribute group with the same properties like another persistent one. */
		try {
			AttributeGroup personalData2 = new AttributeGroup("personalData" + getName(), "Name");
			Attribute salutation2 = new Attribute("salutation", "Anrede");
			Attribute firstName2 = new Attribute("firstname", "Vorname");
			Attribute lastName2 = new Attribute("lastname", "Nachname");
			personalData2.addAttribute(salutation2);
			personalData2.addAttribute(firstName2);
			personalData2.addAttribute(lastName2);
			personalData2 = getService().setAttributeGroup(client.getName(), personalData2);

			personalData2.setName(personalData.getName());

			personalData2 = getService().setAttributeGroup(client.getName(), personalData2);
			Assert.fail(NO_EXCEPTION);
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
	}

	@Test
	public void getAttributes() throws Exception {
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("personalData" + getName(), "Name");
		personalData.addAttribute(new Attribute("salutation", "Anrede"));
		personalData.addAttribute(new Attribute("firstname", "Vorname"));
		personalData.addAttribute(new Attribute("lastname", "Nachname"));

		AttributeGroup result1 = getService().setAttributeGroup(client.getName(), personalData);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		List<AttributeGroup> resultList = getService().getAttributeGroups(client.getName());
		Assert.assertNotNull(resultList);
		Assert.assertTrue(resultList.size() > 0);

		AttributeGroup result2 = findIdInList(getService().getAttributeGroups(client.getName()), result1.getId());

		compareBeansStringProperties(result1, result2, baseProperties, null);
	}

	@Test
	public void setAndGetAttributes() throws Exception {
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("personalData" + getName(), "Name");
		personalData.addAttribute(new Attribute("salutation", "Anrede"));
		personalData.addAttribute(new Attribute("firstname", "Vorname"));
		personalData.addAttribute(new Attribute("lastname", "Nachname"));

		AttributeGroup result1 = getService().setAttributeGroup(client.getName(), personalData);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		AttributeGroup result2 = findIdInList(getService().getAttributeGroups(client.getName()), result1.getId());
		Assert.assertNotNull(result2);

		compareBeansStringProperties(result1, result2, baseProperties, null);

		Assert.assertEquals(result2.getAttribute("salutation").getDisplayName(), result1.getAttribute("salutation").getDisplayName());
	}

	@Test
	public void setAndChangeAttributesResult() throws Exception {
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("personalData" + getName(), "Name");
		// personalData.setClient(client);
		Attribute salutation = new Attribute("salutation", "Anrede");
		personalData.addAttribute(salutation);
		personalData.addAttribute(new Attribute("firstname", "Vorname"));
		personalData.addAttribute(new Attribute("lastname", "Nachname"));

		AttributeGroup result1 = getService().setAttributeGroup(client.getName(), personalData);

		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		Assert.assertEquals(3, result1.getAttributes().size());
		salutation = result1.getAttribute("salutation");
		Assert.assertNotNull(salutation.getId());
		AttributeGroup result4 = findIdInList(getService().getAttributeGroups(client.getName()), result1.getId());
		Assert.assertEquals(3, result4.getAttributes().size());

		result1.getAttributes().remove(result1.getAttribute("salutation"));
		/* generate new random name for the attribute group */
		String oldName = personalData.getName();
		result1.setName("personalData" + getName());
		Assert.assertEquals(2, result1.getAttributes().size());

		AttributeGroup result2 = this.getService().setAttributeGroup(client.getName(), result1);

		result2 = getService().getAttributeGroupById(client.getName(), result1.getId());
		// assert name changed
		Assert.assertFalse(result2.getName().equals(oldName));
		// assert one attribute removed
		Assert.assertEquals(2, result2.getAttributes().size());

		AttributeGroup result3 = findIdInList(getService().getAttributeGroups(client.getName()), result1.getId());
		// assert one attribute removed
		Assert.assertEquals(2, result3.getAttributes().size());
		// assert name changed
		Assert.assertFalse(result3.getName().equals(oldName));

	}

	@Test
	public void setAndChangeAttributesByList() throws Exception {
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("personalData" + getName(), "Name");
		personalData.addAttribute(new Attribute("salutation", "Anrede"));
		personalData.addAttribute(new Attribute("firstname", "Vorname"));
		personalData.addAttribute(new Attribute("lastname", "Nachname"));

		AttributeGroup result1 = getService().setAttributeGroup(client.getName(), personalData);
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		// remove attribute salutation
		AttributeGroup result2 = findIdInList(getService().getAttributeGroups(client.getName()), result1.getId());
		result2.getAttributes().remove(result2.getAttribute("salutation"));
		// generate new random name for the attribute group
		result2.setName("personalData" + getName());
		AttributeGroup result3 = getService().setAttributeGroup(client.getName(), result2);

		// assert name changed
		Assert.assertFalse(result3.getName().equals(personalData.getName()));
		// assert one attribute removed
		Assert.assertEquals(personalData.getAttributes().size(), result3.getAttributes().size() + 1);

		AttributeGroup result4 = findIdInList(getService().getAttributeGroups(client.getName()), result1.getId());
		// assert name changed
		Assert.assertFalse(result4.getName().equals(personalData.getName()));
		// assert one attribute removed
		Assert.assertEquals(personalData.getAttributes().size(), result4.getAttributes().size() + 1);
	}

	@Test
	public void setAndDeleteAttributes() throws Exception {
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("personalData" + getName(), "Name");
		personalData.addAttribute(new Attribute("salutation", "Anrede"));
		personalData.addAttribute(new Attribute("firstname", "Vorname"));
		personalData.addAttribute(new Attribute("lastname", "Nachname"));

		personalData = getService().setAttributeGroup(client.getName(), personalData);
		Assert.assertNotNull(personalData);
		Assert.assertNotNull(personalData.getId());

		// delete attribute group
		getService().deleteAttributeGroup(client.getClientName(), findIdInList(getService().getAttributeGroups(client.getName()), personalData.getId()));

		// assert attribute group is not found anymore
		boolean found = false;
		List<AttributeGroup> result3 = getService().getAttributeGroups(client.getName());
		for (AttributeGroup attributeGroup : result3) {
			if (attributeGroup.getId().equals(personalData.getId())) {
				found = true;
				break;
			}
		}
		Assert.assertFalse(found);
	}

	@Test
	public void setAttributeGroups() throws Exception {
		Client client = this.getCurrentClient(true);
		List<AttributeGroup> attributeGroups = new LinkedList<AttributeGroup>();
		for (int i = 0; i < 10; i++) {
			AttributeGroup personalData = new AttributeGroup("personalData" + getName() + i, "Name" + i);
			personalData.addAttribute(new Attribute("salutation" + i, "Anrede" + i));
			personalData.addAttribute(new Attribute("firstname" + i, "Vorname" + i));
			personalData.addAttribute(new Attribute("lastname" + i, "Nachname" + i));
			attributeGroups.add(personalData);
		}
		try {
			attributeGroups = this.getService().setAttributeGroups(client.getName(), attributeGroups);
			Assert.assertNotNull(attributeGroups);
			/* ten attributes stored for this test and three were stored by persisting client */
			Assert.assertEquals(14, attributeGroups.size());
			for (int i = 0; i < attributeGroups.size(); i++) {
				Assert.assertNotNull(attributeGroups.get(i).getId());
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void setGetAndChangeAttribute() throws Exception {
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("personalData" + getName(), "Name");
		personalData.addAttribute(new Attribute("salutation", "Anrede"));
		personalData.addAttribute(new Attribute("firstname", "Vorname"));
		personalData.addAttribute(new Attribute("lastname", "Nachname"));

		personalData = getService().setAttributeGroup(client.getName(), personalData);
		Long attributeId = personalData.getAttribute("lastname").getId();
		/*
		 * scene: a persistent attribute will be removed and a new attribute with the same name will be inserted. This will be done in one update call
		 */
		try {
			personalData = this.getService().getAttributeGroup(client.getName(), personalData.getName());
			Assert.assertNotNull(personalData);
			Attribute attribute = personalData.getAttribute("firstname");
			int oldSize = personalData.getAttributes().size();
			personalData.getAttributes().remove(attribute);
			Assert.assertEquals(oldSize - 1, personalData.getAttributes().size());
			Attribute newAttribute = new Attribute();
			newAttribute.setName(attribute.getName());
			newAttribute.setDisplayName(attribute.getDisplayName() + "x");
			newAttribute.setType(AttributeType.STRING);
			personalData.addAttribute(newAttribute);

			/* persist the change */
			personalData = this.getService().setAttributeGroup(client.getName(), personalData);

			Assert.assertNotNull(personalData);
			Assert.assertEquals(oldSize, personalData.getAttributes().size());
			Attribute controllAttribute = personalData.getAttribute("firstname");
			Assert.assertFalse(attribute.getId().equals(controllAttribute.getId()));
			Assert.assertEquals(newAttribute.getDisplayName(), controllAttribute.getDisplayName());
		} catch (Exception e) {
			throw e;
		}

		try {
			Attribute attribute = this.getService().getAttributeById(client.getName(), attributeId);
			Assert.assertNotNull(attribute);
			attribute.setDisplayName("Anlaber");
			attribute = this.getService().setAttribute(client.getName(), attribute);
			attribute = this.getService().getAttributeById(client.getName(), attributeId);
			Assert.assertNotNull(attribute);
			Assert.assertEquals("Anlaber", attribute.getDisplayName());
		} catch (Exception e) {
			throw e;
		}

	}

	// TODO warum kein test mehr?
	public void setAndDeleteLockedAttributeGroups() throws Exception {
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("personalData" + getName(), "Name");
		personalData.addAttribute(new Attribute("salutation", "Anrede"));
		personalData.addAttribute(new Attribute("firstname", "Vorname"));
		Attribute attribute = new Attribute("lastname", "Nachname");
		attribute.setWriteProtected(true);
		personalData.addAttribute(attribute);

		personalData = getService().setAttributeGroup(client.getName(), personalData);
		/* try to delete attribute group with a locked attribute */
		try {
			this.getService().deleteAttributeGroup(client.getName(), personalData);
			Assert.fail("No exception has been thrown!");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_LOCKED_ATTRIBUTE, e.getErrorCode());
		}
		/* try to delete a locked attribute from attribute group */
		try {
			attribute = personalData.getAttribute("lastname");
			Assert.assertNotNull(attribute);
			personalData.getAttributes().remove(attribute);
			personalData = this.getService().setAttributeGroup(client.getName(), personalData);
			Assert.fail("No exception has been thrown!");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_LOCKED_ATTRIBUTE, e.getErrorCode());
		}
		/* delete an (unlocked) attribute from an attribute group that has a locked attribute */
		attribute = personalData.getAttribute("firstname");
		Assert.assertNotNull(attribute);
		personalData.getAttributes().remove(attribute);
		personalData = this.getService().setAttributeGroup(client.getName(), personalData);
		personalData = this.getService().getAttributeGroupById(client.getName(), personalData.getId());
		Assert.assertNotNull(personalData);
		Assert.assertEquals(2, personalData.getAttributes().size());
		Assert.assertNotNull(personalData.getAttribute("lastname"));
		Assert.assertNotNull(personalData.getAttribute("salutation"));
	}

	@Test
	public void setAttributeGroupAndDeleteLastAttribute() throws Exception {
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("testData" + getName(), "testData");
		personalData.addAttribute(new Attribute("salutation", "Anrede"));
		// personalData.addAttribute(new Attribute("firstname", "Vorname"));

		personalData = getService().setAttributeGroup(client.getName(), personalData);
		Assert.assertNotNull(personalData);
		Assert.assertNotNull(personalData.getId());
		Assert.assertTrue(personalData.getAttributes().size() == 1);

		// delete attribute group
		personalData.getAttributes().remove(0);
		Assert.assertTrue(personalData.getAttributes().size() == 0);
		// getService().deleteAttributeGroup(client.getClientName(), findIdInList(getService().getAttributeGroups(client.getName()), personalData.getId()));
		personalData = getService().setAttributeGroup(client.getName(), personalData);

		personalData = getService().getAttributeGroupById(client.getName(), personalData.getId());

		Assert.assertEquals(0, personalData.getAttributes().size());

	}

	@Test
	public void setAttributeDuplicates() throws Exception {
		/* setup a client and attribute group with to attributes */
		Client client = this.getCurrentClient(true);
		AttributeGroup personalData = new AttributeGroup("testData" + getName(), "testData");
		personalData.addAttribute(new Attribute("salutation", "Anrede"));
		personalData.addAttribute(new Attribute("firstname", "Vorname"));

		/* scene: try to persist an (not already persistent) attribute group that has two attributes with the same name */
		Attribute attribute = new Attribute("salutation", "AnredeX");

		try {
			personalData.addAttribute(attribute);
			personalData = this.getService().setAttributeGroup(client.getName(), personalData);
			Assert.fail(NO_EXCEPTION);
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
			personalData.getAttributes().remove(attribute);
		}

		personalData = getService().setAttributeGroup(client.getName(), personalData);

		/* scene: try to give an attribute the same name of another persistent attribute in the same attribute group */
		attribute = personalData.getAttribute("firstname");
		Assert.assertNotNull(attribute);
		Assert.assertNotNull(attribute.getId());
		attribute.setName("salutation");

		/* 1.: try to persist the renaming via updating the attribute group */
		try {
			personalData = this.getService().setAttributeGroup(client.getName(), personalData);
			Assert.fail(NO_EXCEPTION);
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
		/* 2.: try to persist the renaming via updating the attribute group */
		try {
			attribute = this.getService().setAttribute(client.getName(), attribute);
			Assert.fail(NO_EXCEPTION);
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
	}

//	@Test
//	public void setAttributeSelectList() throws Exception {
//
//		Client client = this.getCurrentClient(true);
//		AttributeGroup personalData = new AttributeGroup("testData" + getName(), "testData");
//		personalData.addAttribute(new Attribute("lastname", "Nachname"));
//		personalData.addAttribute(new Attribute("firstname", "Vorname"));
//
//		/* Create attribute of type SELECTLIST */
//		Attribute salutation = new Attribute("salutation", "Anrede");
//		salutation.setType(AttributeType.SELECTLIST);
//
//		Attribute subAttribute1 = new Attribute("mr", "Herr");
//		Attribute subAttribute2 = new Attribute("mrs", "Frau");
//		Attribute subAttribute3 = new Attribute("miss", "Fräulein");
//
//		subAttribute1.setType(AttributeType.STRING);
//		subAttribute2.setType(AttributeType.STRING);
//		subAttribute3.setType(AttributeType.STRING);
//
//		salutation.addSubAttribute(subAttribute1);
//		salutation.addSubAttribute(subAttribute2);
//		salutation.addSubAttribute(subAttribute3);
//
//		personalData.addAttribute(salutation);
//		/* try to persist and control the result */
//		try {
//			personalData = this.getService().setAttributeGroup(client.getName(), personalData);
//			Assert.assertNotNull(personalData);
//			Assert.assertEquals(3, personalData.getAttributes().size());
//
//			Attribute controlSalutation = personalData.getAttribute("salutation");
//			Assert.assertNotNull(controlSalutation);
//			Assert.assertNotNull(controlSalutation.getSubAttributes());
//			Assert.assertEquals(3, controlSalutation.getSubAttributes().size());
//			Assert.assertNotNull(controlSalutation.getSubAttributes().get(0).getId());
//		} catch (Exception e) {
//			throw e;
//		}
//		
//		/* try to persist and control the result */
//		try {
//			personalData = this.getService().getAttributeGroup(client.getName(), personalData.getName());
//			
//			Assert.assertNotNull(personalData);
//			Assert.assertEquals(3, personalData.getAttributes().size());
//
//			Attribute controlSalutation = personalData.getAttribute("salutation");
//			Assert.assertNotNull(controlSalutation);
//			Assert.assertNotNull(controlSalutation.getSubAttributes());
//			Assert.assertEquals(3, controlSalutation.getSubAttributes().size());
//			Assert.assertNotNull(controlSalutation.getSubAttributes().get(0).getId());
//		} catch (Exception e) {
//			throw e;
//		}
//
//	}
//	@Test
//	public void setAttributeAndDeleteSelectList() throws Exception {
//
//		Client client = this.getCurrentClient(true);
//		AttributeGroup personalData = new AttributeGroup("testData" + getName(), "testData");
//		personalData.addAttribute(new Attribute("lastname", "Nachname"));
//		personalData.addAttribute(new Attribute("firstname", "Vorname"));
//
//		/* Create attribute of type SELECTLIST */
//		Attribute salutation = new Attribute("salutation", "Anrede");
//		salutation.setType(AttributeType.SELECTLIST);
//
//		Attribute subAttribute1 = new Attribute("mr", "Herr");
//		Attribute subAttribute2 = new Attribute("mrs", "Frau");
//		Attribute subAttribute3 = new Attribute("miss", "Fräulein");
//
//		subAttribute1.setType(AttributeType.STRING);
//		subAttribute2.setType(AttributeType.STRING);
//		subAttribute3.setType(AttributeType.STRING);
//
//		salutation.addSubAttribute(subAttribute1);
//		salutation.addSubAttribute(subAttribute2);
//		salutation.addSubAttribute(subAttribute3);
//
//		personalData.addAttribute(salutation);
//
//		/* try to persist and control the result */
//		try {
//			personalData = this.getService().setAttributeGroup(client.getName(), personalData);
//			Assert.assertNotNull(personalData);
//			Assert.assertEquals(3, personalData.getAttributes().size());
//
//			personalData = this.getService().getAttributeGroup(client.getName(), personalData.getName());
//
//			this.getService().deleteAttributeGroup(client.getName(), personalData);
//			
//			AttributeGroup controlGroup = this.getService().getAttributeGroup(client.getName(), personalData.getName());
//			Assert.assertNull(controlGroup);
//		
//		} catch (Exception e) {
//			throw e;
//		}
//	}
}
