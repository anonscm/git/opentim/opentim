package org.evolvis.idm.test.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;

import junit.framework.Assert;

import org.evolvis.idm.common.fault.IllegalRequestException;
import org.junit.Test;

public class MapValidationTest {

	@Test
	public void testName() throws IllegalRequestException, Exception {
		ValidatableObjectWithMaps object = new ValidatableObjectWithMaps();
		object.getMapWithBothPatterns().put("keyInvalid", "valueInvalid"); // invalid
		object.getMapWithBothPatterns().put("keyValid1", "valueValid1"); // valid
		object.getMapWithBothPatterns().put("keyValid22", "valueValid339"); // valid
		object.getMapWithKeyPattern().put("keyValid1", "valueValid1"); // valid
		object.getMapWithKeyPattern().put("keyValid22", "valueValid339"); // valid
		object.getMapWithKeyPattern().put("keyInvalid", "valueInvalid"); // invalid
		object.getMapWithValuePattern().put("keyValid1", "valueValid1"); // valid
		object.getMapWithValuePattern().put("keyInvalid", "valueInvalid"); // invalid
		object.getMapWithValuePattern().put("keyValid22", "valueValid339"); // valid
		
		Set<ConstraintViolation<ValidatableObjectWithMaps>> constraintViolations =  Validation.buildDefaultValidatorFactory().getValidator().validate(object);
		
		List<String> expectedPaths = new LinkedList<String>();
		expectedPaths.add("mapWithBothPatterns[keyInvalid]");
		expectedPaths.add("mapWithKeyPattern[keyInvalid]");
		expectedPaths.add("mapWithValuePattern[keyInvalid]");
		
		for (ConstraintViolation<ValidatableObjectWithMaps> violation : constraintViolations) {
			Assert.assertFalse("The following property was unexpetedly marked invalid: " + violation.getPropertyPath().toString(), expectedPaths.isEmpty());
			expectedPaths.remove(violation.getPropertyPath().toString());
		}
		Assert.assertTrue("Following properties were not marked invalid: " + expectedPaths.toString(), expectedPaths.isEmpty());
	}
}
