package org.evolvis.idm.test.service;

import java.util.LinkedList;
import java.util.List;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalProperty;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.model.ValueType;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyFields;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.relation.permission.model.Group;
import org.junit.Assert;
import org.junit.Test;

public abstract class ClientManagementServiceAbstractTest extends AbstractBaseServiceAbstractTest<ClientManagement> {

	@Test
	public void setClient() throws Exception {
		try {
			Client client = getService().setClient(0, new Client(getName(), getDisplayName()));
			Assert.assertNotNull(client);
			Assert.assertNotNull(client.getId());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test(expected = BackendException.class)
	public void setClientWithoutData() throws Exception {
		try {
			getService().setClient(0, new Client());
		} catch (IllegalRequestException e) {
			Assert.assertNotNull(e.getIllegalProperties());
			Assert.assertEquals(2, e.getIllegalProperties().size());
			for (IllegalProperty illegalProperty : e.getIllegalProperties()) {
				Assert.assertEquals(Client.class.getName(), illegalProperty.getBeanName());
				if (!(illegalProperty.getPropertyName().equals("name") || illegalProperty.getPropertyName().equals("displayName")))
					Assert.fail();
				Assert.assertNull(Client.class.getName(), illegalProperty.getInvalidValue());
				// Assert.assertEquals("NotNull", illegalProperty
				// .getConstraintType());
				// Assert.assertNull(Client.class.getName(), illegalProperty
				// .getConstraintValue());
			}
			throw e;
		} catch (BackendException e) {
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
			throw e;
		}
	}

	@Test(expected = IllegalRequestException.class)
	public void setClientWithEmptyDisplayName() throws Exception {
		try {
			getService().setClient(0, new Client("test", ""));
		} catch (IllegalRequestException e) {
			Assert.assertNotNull(e.getIllegalProperties());
			Assert.assertEquals(1, e.getIllegalProperties().size());
			for (IllegalProperty illegalProperty : e.getIllegalProperties()) {
				Assert.assertEquals(Client.class.getName(), illegalProperty.getBeanName());
				if (!(illegalProperty.getPropertyName().equals("displayName")))
					Assert.fail();
				// Assert.assertEquals("Min", illegalProperty
				// .getConstraintType());
				// Assert.assertNotNull(Client.class.getName(), illegalProperty
				// .getConstraintValue());
			}
			throw e;
		} catch (BackendException e) {
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
		}
	}

	@Test(expected = BackendException.class)
	public void setClientDuplicates() throws Exception {
		String name = getName();
		String displayName = getDisplayName();

		/* scene : try to persist to clients with the same name and display name */
		try {
			getService().setClient(0, new Client(name, displayName));
			getService().setClient(0, new Client(name, displayName));
			Assert.fail(this.NO_EXCEPTION);
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
			throw new BackendException();
		}

		/* scene : try to update a persistent client with the same name and display name as another persistent client */
		try {
			Client client = this.getCurrentClient(true);
			client.setName(name);
			client.setDisplayName(displayName);
			client = this.getService().setClient(0, client);
			Assert.fail(this.NO_EXCEPTION);
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
			throw new BackendException();
		}

	}

	@Test
	public void getClients() throws Exception {
		Client client = getService().setClient(0, new Client(getName(), getDisplayName()));
		Assert.assertNotNull(client);
		Assert.assertNotNull(client.getId());

		QueryResult<Client> queryResult = getService().getClients(null);
		Client result = findIdInList(queryResult.getResultList(), client.getId());

		compareBeansStringProperties(client, result, baseProperties, null);
	}

	@Test
	public void getClientsWithQueryDescriptor() throws Exception {
		//TODO improve test - check if default client(s) are installed and check it better...
		List<Client> clients = new LinkedList<Client>();
		try {
			Client client;
			for (int i = 0; i < 10; i++) {
				client = new Client();
				client.setName("Client" + i);
				client.setDisplayName("Client" + i);
				client = this.getService().setClient(i % 2, client);
				clients.add(client);
			}
			QueryDescriptor queryDescriptor = new QueryDescriptor();

			/* begin with a simple paging - set a limit */
			queryDescriptor.setLimit(2);
			QueryResult<Client> queryResult = this.getService().getClients(queryDescriptor);

			Assert.assertNotNull(queryResult);
			Assert.assertEquals(2, queryResult.size());
			//may be there is also the default client, therefore check two possibilities:
			Assert.assertTrue(queryResult.getTotalSize().equals(new Integer(10)) || queryResult.getTotalSize().equals(new Integer(11)));
			Assert.assertEquals(clients.get(0).getId(), queryResult.get(0).getId());
			Assert.assertEquals(clients.get(1).getId(), queryResult.get(1).getId());

			/* now extend - get page 2 */
			queryDescriptor.setOffSet(2);
			queryResult = this.getService().getClients(queryDescriptor);

			Assert.assertNotNull(queryResult);
			Assert.assertEquals(2, queryResult.size());
			/* normally there will be 10 clients but there may be one more - the default client*/
			Assert.assertTrue(queryResult.getTotalSize().equals(new Integer(10)) || queryResult.getTotalSize().equals(new Integer(11)));
			Assert.assertEquals(clients.get(2).getId(), queryResult.get(0).getId());
			Assert.assertEquals(clients.get(3).getId(), queryResult.get(1).getId());

			/* add an order property - to get the reverse default order */
			BeanOrderProperty orderProperty = new BeanOrderProperty(Client.class, ClientPropertyFields.FIELD_PATH_DISPLAYNAME, false);
			queryDescriptor.addOrderProperty(orderProperty);
			queryResult = this.getService().getClients(queryDescriptor);

			Assert.assertNotNull(queryResult);
			Assert.assertEquals(2, queryResult.size());
			Assert.assertTrue(queryResult.getTotalSize().equals(new Integer(10)) || queryResult.getTotalSize().equals(new Integer(11)));
			client = queryResult.get(0);
			int compareIndex = 8;
			if(queryResult.getTotalSize().equals(new Integer(10)))
				compareIndex = 7;
			Assert.assertEquals(clients.get(compareIndex).getId(), client.getId());
			client = queryResult.get(1);
			Assert.assertEquals(clients.get(--compareIndex).getId(), queryResult.get(1).getId());
			
			/* add an additional property - prepare the test by adding a client that has the same display name as another one */
			client = new Client();
			client.setDisplayName("Client9");
			client.setName("Client99");
			client = this.getService().setClient(0, client);
			clients.add(client);

			BeanOrderProperty orderProperty2 = new BeanOrderProperty(Client.class, ClientPropertyFields.FIELD_PATH_NAME, false);
			queryDescriptor.addOrderProperty(orderProperty2);
			queryDescriptor.setOffSet(0);
			queryResult = this.getService().getClients(queryDescriptor);

			Assert.assertNotNull(queryResult);
			Assert.assertEquals(2, queryResult.size());
			Assert.assertTrue(queryResult.getTotalSize().equals(new Integer(11)) || queryResult.getTotalSize().equals(new Integer(12)));
			if(queryResult.getTotalSize().equals(12)){
				client = queryResult.get(1);
				Assert.assertEquals(clients.get(10).getId(), queryResult.get(1).getId());
			}
			else{
				client = queryResult.get(0);
				Assert.assertEquals(clients.get(10).getId(), client.getId());
				client = queryResult.get(1);
				Assert.assertEquals(clients.get(9).getId(), queryResult.get(1).getId());
			}
			/* try query with a search property */
			queryDescriptor.setOffSet(0);
			queryDescriptor.setLimit(Integer.MAX_VALUE);
			queryResult = this.getService().getClients(queryDescriptor);
			Assert.assertNotNull(queryResult);
			Assert.assertTrue(queryResult.getTotalSize().equals(new Integer(11)) || queryResult.getTotalSize().equals(new Integer(12)));
			if(queryResult.getTotalSize().equals(12)){
				Assert.assertEquals(clients.get(10).getId(), queryResult.get(1).getId());
				Assert.assertEquals(clients.get(9).getId(), queryResult.get(2).getId());
				Assert.assertEquals(clients.get(1).getId(), queryResult.get(10).getId());
				Assert.assertEquals(clients.get(0).getId(), queryResult.get(11).getId());
			}
			else{
				Assert.assertEquals(clients.get(10).getId(), queryResult.get(0).getId());
				Assert.assertEquals(clients.get(9).getId(), queryResult.get(1).getId());
				Assert.assertEquals(clients.get(1).getId(), queryResult.get(9).getId());
				Assert.assertEquals(clients.get(0).getId(), queryResult.get(10).getId());
			}
			BeanSearchProperty searchProperty = new BeanSearchProperty(Client.class, ClientPropertyFields.FIELD_PATH_DISPLAYNAME, "Client9", ValueType.STRING);
			queryDescriptor.addSearchProperty(searchProperty);
			queryResult = this.getService().getClients(queryDescriptor);

			Assert.assertNotNull(queryResult);
			Assert.assertEquals(2, queryResult.size());
			Assert.assertEquals(new Integer(2), queryResult.getTotalSize());
			Assert.assertEquals(clients.get(10).getId(), queryResult.get(0).getId());
			Assert.assertEquals(clients.get(9).getId(), queryResult.get(1).getId());

			queryDescriptor.getSearchProperties().clear();

			/* now test to get the last page that does not contain the less clients than limit */
			queryDescriptor.setLimit(5);
			queryDescriptor.setOffSet(8);
			queryResult = this.getService().getClients(queryDescriptor);
			Assert.assertNotNull(queryResult);
			if(queryResult.getTotalSize().equals(new Integer(12))){
				Assert.assertEquals(4, queryResult.size());
				Assert.assertEquals(clients.get(2).getId(), queryResult.get(1).getId());
				Assert.assertEquals(clients.get(1).getId(), queryResult.get(2).getId());
				Assert.assertEquals(clients.get(0).getId(), queryResult.get(3).getId());
				
			}
			else{
				Assert.assertEquals(3, queryResult.size());
				Assert.assertEquals(clients.get(2).getId(), queryResult.get(0).getId());
				Assert.assertEquals(clients.get(1).getId(), queryResult.get(1).getId());
				Assert.assertEquals(clients.get(0).getId(), queryResult.get(2).getId());
			}
		} catch (Exception e) {
			throw e;
		}

		/* clean up */
		finally {
			for (Client clientToDelete : clients)
				this.getService().deleteClient(clientToDelete);
		}
	}

	@Test
	public void setAndGetClient() throws Exception {
		Client client = getService().setClient(0, new Client(getName(), getDisplayName()));
		Assert.assertNotNull(client);
		Assert.assertNotNull(client.getId());

		Client result = getService().getClientByName(client.getName());

		compareBeansStringProperties(client, result, baseProperties, null);
	}

	@Test
	public void setAndChangeClientResult() throws Exception {
		Client result1 = getService().setClient(0, new Client(getName(), getDisplayName()));
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		String oldName = result1.getName();
		result1.setName(getName());

		Client result2 = getService().setClient(0, result1);
		Assert.assertEquals(result1.getId(), result2.getId());
		Assert.assertFalse(oldName.equals(result2.getName()));

		Client result3 = findIdInList(getService().getClients(null).getResultList(), result1.getId());
		Assert.assertEquals(result1.getId(), result3.getId());
		Assert.assertFalse(oldName.equals(result3.getName()));
	}

	@Test
	public void setAndChangeClientById() throws Exception {
		Client client = getService().setClient(0, new Client(getName(), getDisplayName()));
		Assert.assertNotNull(client);
		Assert.assertNotNull(client.getId());

		Client result = getService().getClientByName(client.getName());
		String oldName = client.getName();
		result.setName(getName());

		Client result2 = getService().setClient(0, result);
		Assert.assertNotNull(result2);
		Assert.assertNotNull(result2.getId());
		Assert.assertEquals(client.getId(), result2.getId());
		Assert.assertFalse(oldName.equals(result2.getName()));

		Client result3 = getService().getClientByName(result2.getName());
		Assert.assertNotNull(result3);
		Assert.assertNotNull(result3.getId());
		Assert.assertEquals(client.getId(), result3.getId());
		Assert.assertFalse(oldName.equals(result3.getName()));
		Assert.assertEquals(client.getDisplayName(), result3.getDisplayName());
	}

	@Test
	public void setAndChangeClientByList() throws Exception {
		try {
			Client client = getService().setClient(0, new Client(getName(), getDisplayName()));
			Assert.assertNotNull(client);
			Assert.assertNotNull(client.getId());

			Client result = findIdInList(getService().getClients(null).getResultList(), client.getId());
			String oldName = client.getName();
			result.setName(getName());

			Client result2 = getService().setClient(0, result);
			Assert.assertNotNull(result2);
			Assert.assertNotNull(result2.getId());
			Assert.assertEquals(client.getId(), result2.getId());
			Assert.assertFalse(oldName.equals(result2.getName()));
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void setAndDeleteClient() throws Exception {
		try {
			Client client = getService().setClient(0, new Client(getName(), getDisplayName()));
			Assert.assertNotNull(client);
			Assert.assertNotNull(client.getId());

			getService().deleteClient(client);

			Client result = getService().getClientByName(client.getClientName());

			Assert.assertNull(result);
		} catch (BackendException e) {
			Assert.assertEquals(IllegalRequestException.CODE_ENTITY_NOT_PERSITENT, e.getErrorCode());
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@Test
	// TODO
	public void setAndDeleteClientWithAdditionalData() throws Exception {
		Client client = this.getCurrentClient(true);
		@SuppressWarnings("unused")
		Group group = this.getCurrentGroup(client, true);
		try {
			this.getService().deleteClient(client);
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void setClientWithProperties() throws Exception {
		Client client = this.getCurrentClient(true);

		try {
			ClientPropertyMap clientMap = this.getService().getClientPropertyMap(client.getName());
			int defaultMapSize = clientMap.getMapEntries().size();
			clientMap.put("testKey1", "testValue1");
			clientMap.put("testKey2", "testValue2");

			this.getService().setClientPropertyMap(client.getName(), clientMap);

			ClientPropertyMap result = this.getService().getClientPropertyMap(client.getName());

			Assert.assertNotNull(result.getId());
			Assert.assertEquals(2, result.size() - defaultMapSize);
			Assert.assertEquals("testValue1", result.get("testKey1"));
			Assert.assertEquals("testValue2", result.get("testKey2"));

			/* remove a key/value pair, add a new one and modify another */
			String removedValue = result.remove("testKey1");
			Assert.assertNotNull(removedValue);
			Assert.assertEquals("testValue1", removedValue);
			result.put("testKey3", "testValue3");
			result.put("testKey2", "testValue4");
			this.getService().setClientPropertyMap(client.getName(), result);
			result = this.getService().getClientPropertyMap(client.getName());

			Assert.assertNotNull(result.getId());
			Assert.assertTrue(result.size() >= 2);
			Assert.assertEquals("testValue3", result.get("testKey3"));
			Assert.assertEquals("testValue4", result.get("testKey2"));
		} catch (Exception e) {
			throw e;
		}
	}
}
