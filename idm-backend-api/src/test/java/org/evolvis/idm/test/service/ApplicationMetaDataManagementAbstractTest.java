package org.evolvis.idm.test.service;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.applicationdata.model.AppMetaData;
import org.evolvis.idm.identity.applicationdata.model.AppMetaDataValue;
import org.evolvis.idm.identity.applicationdata.service.ApplicationMetaDataManagement;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.junit.Before;
import org.junit.Test;

public abstract class ApplicationMetaDataManagementAbstractTest extends AbstractBaseServiceAbstractTest<ApplicationMetaDataManagement> {

	Client client = null;


	@Before
	public void init() throws Exception {
		client = this.getCurrentClient(true);
	}


	@Test
	public void setApplicationMetaData() throws Exception {
		try {
			//now try to persist an empty application meta data structure
			Account account = this.getCurrentAccount(client, true);
			Application application = this.getCurrentApplication(client, true);
			AppMetaData appMetaData = new AppMetaData();
			appMetaData.setAccount(account);
			appMetaData.setApplication(application);
			this.getService().setApplicationMetaData(appMetaData);
		} catch (Exception e) {
			throw e;
		}
		try {
			// now try to persist an application meta data structure that contains values
			Account account = this.getCurrentAccount(client, true);
			Application application = this.getCurrentApplication(client, true);
			AppMetaData appMetaData = new AppMetaData();
			appMetaData.setAccount(account);
			appMetaData.setApplication(application);

			Long currentValue = System.currentTimeMillis();
			String testKey1 = "AppMetaDataKey" + currentValue;
			String testValue1 = "AppMetaDataValue" + currentValue;

			appMetaData.put(testKey1, testValue1);

			appMetaData = this.getService().setApplicationMetaData(appMetaData);

			// test the correctness
			Assert.assertNotNull(appMetaData);
			Assert.assertNotNull(appMetaData.getId());
			Assert.assertEquals(1, appMetaData.size());
			Set<Map.Entry<String, String>> controlSet = (Set<Map.Entry<String, String>>) appMetaData.entrySet();
			Iterator<Map.Entry<String, String>> iterator = controlSet.iterator();
			while (iterator.hasNext()) {
				AppMetaDataValue appMetaDataValue = (AppMetaDataValue) iterator.next();
				Assert.assertNotNull(appMetaDataValue);
				Assert.assertNotNull(appMetaDataValue.getId());
			}/* TODO */
		} catch (Exception e) {
			throw e;
		}
	}


	@Test
	public void setIllegalApplicationMetaData() throws Exception {
		try {
			// first try to persist an empty application meta data structure
			Account account = new Account();
			Application application = new Application();
			AppMetaData appMetaData = new AppMetaData();
			appMetaData.setAccount(account);
			appMetaData.setApplication(application);
			this.getService().setApplicationMetaData(appMetaData);
			Assert.fail("An expected exception is not thrown");
		} catch (Exception e) {
			if (!(e instanceof BackendException) || !((BackendException)e).getErrorCode().equals( BackendException.CODE_CLIENT_NOT_AVAILABLE))
				Assert.fail("Thrown exception is not the expected one" + e.getMessage());
		}
//		try{
//			this.getService().setApplicationMetaData(null);
//			Assert.fail();
//		} catch(IllegalRequestException e){
//			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
//		}
	}


	@Test
	public void getApplicationMetaData() throws Exception {
		Application application = this.getCurrentApplication(client, true);
		Account account = this.getCurrentAccount(client, true);
		try {
			AppMetaData appMetaData = this.getService().getApplicationMetaData(application.getName(), account.getUuid());
			Assert.assertNull(appMetaData);
		} catch (Exception e) {
			throw e;
		}
		try {
			// now try to persist an application meta data structure that contains values

			AppMetaData appMetaData = new AppMetaData();
			appMetaData.setAccount(account);
			appMetaData.setApplication(application);

			Long currentValue = System.currentTimeMillis();
			String testKey1 = "AppMetaDataKey" + currentValue;
			String testValue1 = "AppMetaDataValue" + currentValue;

			appMetaData.put(testKey1, testValue1);

			appMetaData = this.getService().setApplicationMetaData(appMetaData);

			// do the query
			appMetaData = this.getService().getApplicationMetaData(application.getName(), account.getUuid());
			// test the correctness
			Assert.assertNotNull(appMetaData);
			Assert.assertNotNull(appMetaData.getId());
			Assert.assertEquals(1, appMetaData.getMapEntries().size());
			AppMetaDataValue appMetaDataValue = appMetaData.getMapEntries().get(0);
			Assert.assertNotNull(appMetaDataValue.getId());
			Assert.assertEquals(testValue1, appMetaDataValue.getValue());
		} catch (Exception e) {
			throw e;
		}
		// now try cases with illegal parameter arguments
		try{
			@SuppressWarnings("unused")
			AppMetaData appMetaData = this.getService().getApplicationMetaData(null, account.getUuid());
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
		try{
			@SuppressWarnings("unused")
			AppMetaData appMetaData = this.getService().getApplicationMetaData(application.getName(), null);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
		catch(BackendException e){
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
		}
	}


	@Test
	public void getApplicationMetaDataValue() throws Exception {
		Application application = this.getCurrentApplication(client, true);
		Account account = this.getCurrentAccount(client, true);

		AppMetaData appMetaData = new AppMetaData();
		appMetaData.setAccount(account);
		appMetaData.setApplication(application);

		Long currentValue = System.currentTimeMillis();
		String testKey1 = "AppMetaDataKey" + currentValue;
		String testValue1 = "AppMetaDataValue" + currentValue;

		appMetaData.put(testKey1, testValue1);

		currentValue = System.currentTimeMillis();
		String testKey2 = "AppMetaDataKey" + currentValue;
		String testValue2 = "AppMetaDataValue" + currentValue;

		appMetaData.put(testKey2, testValue2);

		appMetaData = this.getService().setApplicationMetaData(appMetaData);

		try {
			String controlValue = null;
			controlValue = this.getService().getApplicationMetaDataValue(application.getName(), account.getUuid(), testKey1);
			Assert.assertNotNull(controlValue);
			Assert.assertTrue(controlValue.equals(testValue1));

			controlValue = this.getService().getApplicationMetaDataValue(application.getName(), account.getUuid(), "NotPersistentValue");
			Assert.assertNull(controlValue);
		} catch (Exception e) {
			throw e;
		}
		// now try cases with illegal parameter arguments
		try{
			@SuppressWarnings("unused")
			String controlValue = this.getService().getApplicationMetaDataValue(application.getName(), null, testKey1);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
		catch(BackendException e){
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
		}
		try{
			@SuppressWarnings("unused")
			String controlValue = this.getService().getApplicationMetaDataValue(null, account.getUuid(), testKey1);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
		try{
			@SuppressWarnings("unused")
			String controlValue = this.getService().getApplicationMetaDataValue(application.getName(), account.getUuid(), null);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
	}


	@Test
	public void setApplicationMetaDataValue() throws Exception {
		Application application = this.getCurrentApplication(client, true);
		Account account = this.getCurrentAccount(client, true);

		Long currentValue = System.currentTimeMillis();
		String testKey1 = "AppMetaDataKey" + currentValue;
		String testValue1 = "AppMetaDataValue" + currentValue;

		currentValue += 1;
		String testKey2 = "AppMetaDataKey" + currentValue;
		String testValue2 = "AppMetaDataValue" + currentValue;

		try {
			String controlValue = this.getService().setApplicationMetaDataValue(application.getName(), account.getUuid(), testKey1, testValue1);
			Assert.assertNull(controlValue);

			controlValue = this.getService().getApplicationMetaDataValue(application.getName(), account.getUuid(), testKey1);
			Assert.assertNotNull(controlValue);
			Assert.assertTrue(controlValue.equals(testValue1));

			// test overwriting an value of a certain key
			controlValue = this.getService().setApplicationMetaDataValue(application.getName(), account.getUuid(), testKey1, testValue2);
			Assert.assertNotNull(controlValue);
			Assert.assertTrue(controlValue.equals(testValue1));
			controlValue = this.getService().getApplicationMetaDataValue(application.getName(), account.getUuid(), testKey1);
			Assert.assertNotNull(controlValue);
			Assert.assertTrue(controlValue.equals(testValue2));
			// insert a second to control functionality with more stored values

			controlValue = this.getService().setApplicationMetaDataValue(application.getName(), account.getUuid(), testKey2, testValue2);
			Assert.assertNull(controlValue);

			controlValue = this.getService().getApplicationMetaDataValue(application.getName(), account.getUuid(), testKey2);
			Assert.assertTrue(controlValue.equals(testValue2));
		} catch (Exception e) {
			throw e;
		}
		// now try cases with illegal parameter arguments
		try{
			@SuppressWarnings("unused")
			String controlValue = this.getService().setApplicationMetaDataValue(null, account.getUuid(), testKey1, testValue2);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
		try{
			@SuppressWarnings("unused")
			String controlValue = this.getService().setApplicationMetaDataValue(application.getName(), null, testKey1, testValue2);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
		catch(BackendException e){
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
		}
		try{
			@SuppressWarnings("unused")
			String controlValue = this.getService().setApplicationMetaDataValue(application.getName(), account.getUuid(), null, testValue2);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}

		try{
			@SuppressWarnings("unused")
			String controlValue = this.getService().setApplicationMetaDataValue(application.getName() , account.getUuid(), testKey1, null);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}

	}


	@Test
	public void removeApplicationMetaDataValue() throws Exception {
		Application application = this.getCurrentApplication(client, true);
		Account account = this.getCurrentAccount(client, true);

		Long currentValue = System.currentTimeMillis();
		String testKey1 = "AppMetaDataKey" + currentValue;
		String testValue1 = "AppMetaDataValue" + currentValue;

		currentValue += 1;
		String testKey2 = "AppMetaDataKey" + currentValue;
		String testValue2 = "AppMetaDataValue" + currentValue;
		try {
			this.getService().setApplicationMetaDataValue(application.getName(), account.getUuid(), testKey1, testValue1);
			this.getService().setApplicationMetaDataValue(application.getName(), account.getUuid(), testKey2, testValue2);
			String controlMetaDataValue = this.getService().getApplicationMetaDataValue(application.getName(), account.getUuid(), testKey1);
			Assert.assertNotNull(controlMetaDataValue);
			Assert.assertTrue(controlMetaDataValue.equals(testValue1));

			this.getService().removeApplicationMetaDataValue(application.getName(), account.getUuid(), testKey1);

			controlMetaDataValue = this.getService().getApplicationMetaDataValue(application.getName(), account.getUuid(), testKey1);
			Assert.assertNull(controlMetaDataValue);

			AppMetaData appMetaData = this.getService().getApplicationMetaData(application.getName(), account.getUuid());
			Assert.assertNotNull(appMetaData);
			Assert.assertEquals(1, appMetaData.size());
			Assert.assertTrue(appMetaData.containsKey(testKey2));
			// TODO Erweiterung ?
		} catch (Exception e) {
			throw e;
		}
		// now try cases with illegal parameter arguments
		try{
			this.getService().removeApplicationMetaDataValue(null, account.getUuid(), testKey1);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
		try{
			this.getService().removeApplicationMetaDataValue(application.getName(), null, testKey1);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
		catch(BackendException e){
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
		}
		try{
			this.getService().removeApplicationMetaDataValue(application.getName(), account.getUuid(), null);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
	}


	@Test
	public void removeApplicationMetaData() throws Exception {
		Application application = this.getCurrentApplication(client, true);
		Account account = this.getCurrentAccount(client, true);

		Long currentValue = System.currentTimeMillis();
		String testKey1 = "AppMetaDataKey" + currentValue;
		String testValue1 = "AppMetaDataValue" + currentValue;

		currentValue += 1;
		String testKey2 = "AppMetaDataKey" + currentValue;
		String testValue2 = "AppMetaDataValue" + currentValue;
		try {
			this.getService().setApplicationMetaDataValue(application.getName(), account.getUuid(), testKey1, testValue1);
			this.getService().setApplicationMetaDataValue(application.getName(), account.getUuid(), testKey2, testValue2);

			this.getService().deleteApplicationMetaData(application.getName(), account.getUuid());

			AppMetaData appMetaData = this.getService().getApplicationMetaData(application.getName(), account.getUuid());
			Assert.assertNull(appMetaData);
		} catch (Exception e) {
			throw e;
		}
		// now try cases with illegal parameter arguments
		try{
			this.getService().deleteApplicationMetaData(null, account.getUuid());
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
		// now try cases with illegal parameter arguments
		try{
			this.getService().deleteApplicationMetaData(application.getName(), null);
			Assert.fail();
		} catch(IllegalRequestException e){
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
		catch(BackendException e){
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
		}
	}
}
