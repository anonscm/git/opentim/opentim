package org.evolvis.idm.test.model;

import java.util.Locale;

import org.evolvis.idm.common.fault.IllegalProperty;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.util.ValidationUtil;
import org.evolvis.idm.identity.account.model.User;
import org.junit.Test;

public class UserValidationTest {
	
	@Test
	public void testValidUser() throws Exception {
		User user = new User();
//		user.setUuid("");
		user.setUsername("test@tarent.de");
		user.setDisplayName("Tester");
		user.setFirstname("Test");
		user.setLastname("o Mania");
		user.setPassword("test1234");
		user.setPasswordRepeat("test1234");
		user.setLocaleString(Locale.getDefault().toString());
		
		try {
			ValidationUtil.validate(user);
		} catch (IllegalRequestException e) {
			for (IllegalProperty ip : e.getIllegalProperties())
				System.out.println(ip);
			throw e;
		}
	}

}
