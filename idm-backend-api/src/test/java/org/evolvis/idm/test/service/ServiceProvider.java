package org.evolvis.idm.test.service;

import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.identity.account.service.UserManagement;
import org.evolvis.idm.identity.applicationdata.service.ApplicationMetaDataManagement;
import org.evolvis.idm.identity.privatedata.service.PrivateDataConfiguration;
import org.evolvis.idm.identity.privatedata.service.PrivateDataManagement;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.relation.organization.service.OrganizationalUnitManagement;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleManagement;
import org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement;
import org.evolvis.idm.synchronization.service.DirectoryServiceSynchronization;

public interface ServiceProvider {
	public ApplicationManagement getApplicationManagementService();


	public PrivateDataConfiguration getPrivateDataConfigurationService();


	public ClientManagement getClientManagementService();


	public PrivateDataManagement getPrivateDataManagementService();


	/**
	 * Returns the internal instance of the {@link org.evolvis.idm.relation.permission.service.GroupAndRoleManagement} service.
	 * 
	 * @return an implementing instance of GroupAndRoleManagement.
	 */
	public GroupAndRoleManagement getGroupAndRoleManagementService();


	/**
	 * Returns the internal instance of the {@link org.evolvis.idm.identity.account.service.AccountManagement} service.
	 * 
	 * @return an implementing instance of AccountManagement.
	 */
	public AccountManagement getAccountManagementService();


	/**
	 * Returns the internal instance of the {@link org.evolvis.idm.relation.permission.service.InternalRoleManagement} service.
	 * 
	 * @return an implementing instance of InternalRoleManagement.
	 */
	public InternalRoleManagement getInternalRoleManagementService();


	/**
	 * Returns the internal instance of the {@link org.evolvis.idm.relation.permission.service.InternalRoleScopeManagement} service.
	 * 
	 * @return an implementing instance of InternalRoleScopeManagement
	 */
	public InternalRoleScopeManagement getInternalRoleScopeManagementService();


	/**
	 * Returns the internal instance of the {@link org.evolvis.idm.OrganizationalUnitManagement.organization.service.OrganisationalUnitManagement} service.
	 * 
	 * @return an implementing instance of ApplicationMetaDataManagement
	 */
	public OrganizationalUnitManagement getOrganisationalUnitManagementService();


	/**
	 * Returns the internal instance of the {@link org.evolvis.idm.ws.unsecured.identity.applicationdata.service.ApplicationMetaDataManagement} service.
	 * 
	 * @return an implementing instance of OrganizationalUnitManagement
	 */
	public ApplicationMetaDataManagement getApplicationMetaDataManagementService();
	
	public DirectoryServiceSynchronization getDirectoryServiceSynchronizationService();
	
	public UserManagement getUserManagementService();
}