package org.evolvis.idm.test.service;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalProperty;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.BeanSearchProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.ApplicationPropertyFields;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.GroupPropertyFields;
import org.evolvis.idm.relation.permission.model.GroupSearchableFields;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.model.RolePropertyFields;
import org.evolvis.idm.relation.permission.model.RoleUser;
import org.evolvis.idm.relation.permission.model.UserRole;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public abstract class GroupAndRoleManagementAbstractTest extends AbstractBaseServiceAbstractTest<GroupAndRoleManagement> {

	@BeforeClass
	public static void init() {
		errorPrefix = "GroupAndRoleManagementTest: ";
	}

	@After
	public void clearAll() {

	}

	/* methods to build a special persistent state */

	/**
	 * Tries to persist a new group.
	 * 
	 * @throws Exception
	 */
	@Test
	public void setGroup() throws Exception {
		Client client = this.getCurrentClient(false);
		try {
			Group group = getService().setGroup(client.getName(), new Group("Group" + System.currentTimeMillis(), false, client));
			Assert.assertNotNull(group);
			Assert.assertNotNull(group.getId());
		} catch (IllegalRequestException e) {
			System.err.println(errorPrefix + "setGroup: IllegalRequestException while persisting a group: ");
			List<IllegalProperty> illegalProperties = e.getIllegalProperties();
			System.err.println("Illegal Properties: " + illegalProperties.size());
			for (IllegalProperty illegalProperty : illegalProperties) {
				System.err.println(illegalProperty.toString());
			}
		} catch (Exception e) {
			System.err.println(errorPrefix + "setGroup: Error while persisting a group: " + e.getMessage() + " " + e.toString());
			throw e;
		}
		try {
			Group group = this.getCurrentGroup(client, true);
			group.setId(null);
			group = this.getService().setGroup(client.getName(), group);
			Assert.fail("No exception has been thrown");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
	}

	@Test
	public void setGroupDuplicate() throws Exception {
		Client client = this.getCurrentClient(true);
		Group group1 = new Group();
		Group group2 = new Group();
		Group group3 = new Group();
		group1.setClient(client);
		group2.setClient(client);
		group3.setClient(client);
		group1.setDisplayName("Group1");
		group2.setDisplayName("Group2");
		group3.setDisplayName("Group3");
		group1.setName("Group1");
		group2.setName("Group2");
		group3.setName("Group1");

		group1 = this.getService().setGroup(client.getName(), group1);
		group2 = this.getService().setGroup(client.getName(), group2);
		
		/* scene: try to persist a new group with the name of an already persistent one */
		try {
			group3 = this.getService().setGroup(client.getName(), group3);
			Assert.fail("No exception has been thrown");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
		
		/* scene: two persistent groups - rename the second with name of the first */
		try {
			group2.setName("Group1");
			group2 = this.getService().setGroup(client.getName(), group2);
			Assert.fail("No exception has been thrown");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
	}

	@Test
	@SuppressWarnings("unused")
	public void createMultipleGroups() throws Exception {
		Client client = this.getCurrentClient(true);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		Group group2a = this.getCurrentGroup(client, true);
		Group group3 = this.getCurrentGroup(client, false);
		QueryResult<Group> queryResult = this.getService().getGroups(client.getName(), null);
		Assert.assertNotNull(queryResult);
		Assert.assertEquals(3, queryResult.size());
	}

	/**
	 * Tries to persist a new role.
	 * 
	 * @throws Exception
	 */
	@Test
	public void setRole() throws Exception {
		try {
			Role role = new Role();
			role.setName("test-Role");
			Client client = this.getCurrentClient(true);
			Application application = this.getCurrentApplication(client, false);
			role.setDisplayName("Test-Description");
			role.setApplication(application);
			role = getService().setRole(client.getName(), role);
			Assert.assertNotNull(role);
			Assert.assertNotNull(role.getId());

			try {
				role.setId(null);
				role = getService().setRole(client.getName(), role);
				Assert.fail("No exception has been thrown");
			} catch (IllegalRequestException e) {
				Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
			}
		} catch (Exception e) {
			throw e;
		}
	}

	
	@Test
	public void setRoleDuplicate() throws Exception {
		Client client = this.getCurrentClient(true);
		Application application = this.getCurrentApplication(client, true);
		Role role1 = new Role();
		Role role2 = new Role();
		Role role3 = new Role();
		
		role1.setApplication(application);
		role2.setApplication(application);
		role3.setApplication(application);
		
		role1.setName("Role1");
		role2.setName("Role2");
		role3.setName("Role1");
		
		role1.setDisplayName("Role1");
		role2.setDisplayName("Role2");
		role3.setDisplayName("Role3");
		role1 = this.getService().setRole(client.getName(), role1);
		role2 = this.getService().setRole(client.getName(), role2);
		
		/* scene: try to persist a new group with the name of an already persistent one */
		try {
			role3 = this.getService().setRole(client.getName(), role3);
			Assert.fail("No exception has been thrown");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
		
		/* scene: two persistent groups - rename the second with name of the first */
		try {
			role2.setName("Role1");
			role2 = this.getService().setRole(client.getName(), role2);
			Assert.fail("No exception has been thrown");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}



	}
	
	
	/**
	 * Tries to persist a group that has no the needed data.
	 * 
	 * @throws Exception
	 */
	@Test(expected = BackendException.class)
	public void setGroupWithoutData() throws Exception {
		try {
			Group group = new Group();
			group = getService().setGroup(null, group);
			// } catch (IllegalRequestException e) {
			// List<IllegalProperty> illegalProperties =
			// e.getIllegalProperties();
			// for (IllegalProperty illegalProperty : illegalProperties) {
			// System.err.println(illegalProperty.toString());
			// }
			// Assert.assertNotNull(e.getIllegalProperties());
			// Assert.assertEquals(3, e.getIllegalProperties().size());
			// for (IllegalProperty illegalProperty : e.getIllegalProperties())
			// {
			// Assert.assertEquals(Group.class.getName(),
			// illegalProperty.getBeanName());
			// if (!(illegalProperty.getPropertyName().equals("name") |
			// illegalProperty.getPropertyName().equals("client") |
			// illegalProperty.getPropertyName().equals("displayName")))
			// Assert.fail();
			// Assert.assertNull(Group.class.getName(),
			// illegalProperty.getInvalidValue());
			//
			// }
			// throw e;
		} catch (BackendException e) {
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
			throw e;
		}
		try {
			@SuppressWarnings("unused")
			Group group = this.getService().setGroup(null, null);
			Assert.fail();
		} catch (IllegalRequestException e) {
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		} catch (BackendException e) {
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
		}
	}

	/**
	 * Tries to persist a role that has not the needed information for it, so it should result in an exception.
	 * 
	 * @throws Exception
	 */
	@Test(expected = BackendException.class)
	public void setRoleWithoutData() throws Exception {
		try {
			getService().setRole(null, new Role());
		} catch (BackendException e) {
			Assert.assertEquals(BackendException.CODE_CLIENT_NOT_AVAILABLE, e.getErrorCode());
		}
		try {
			@SuppressWarnings("unused")
			Role role = this.getService().setRole(null, null);
			Assert.fail();
		} catch (IllegalRequestException e) {
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		} catch (BackendException e) {
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
			throw e;
		}
	}

	/**
	 * Tests the deletion of an existing group
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteGroup() throws Exception {
		Client client = this.getCurrentClient(true);
		Group group = this.getCurrentGroup(client, true);
		try {
			this.getService().deleteGroup(client.getClientName(), group.getId());
		} catch (Exception e) {
			Assert.fail("Deletion of an existing Group failed:");
			e.printStackTrace();
		}
		try {
			this.getService().deleteGroup(client.getClientName(), null);
			Assert.fail();
		} catch (IllegalRequestException e) {
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}

		try {
			group = this.getCurrentGroup(client, true);
			Account account = this.getCurrentAccount(client, true);
			this.getService().addUserToGroup(account.getUuid(), group.getId());
			Assert.assertTrue(this.getService().isUserInGroup(account.getUuid(), group.getId()));
			this.getService().deleteGroup(client.getClientName(), group.getId());
			Assert.assertFalse(this.getService().isUserInGroup(account.getUuid(), group.getId()));
		} catch (Exception e) {
			throw e;
		}

	}

	@Test(expected = BackendException.class)
	public void deleteNonExistingGroup() throws Exception {
		Client client = this.getCurrentClient(true);
		Group group = new Group("NotExistingGroup", false, null);
		group.setId(new Long(100023));
		try {
			this.getService().deleteGroup(client.getClientName(), group.getId());
		} catch (BackendException e) {
			if (e.getMessage().contains(IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION.getMessage()))
				throw e;
		}
	}

	/**
	 * tests the deletion of an existing group
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteRole() throws Exception {
		Client client = this.getCurrentClient(true);
		Application application = this.getCurrentApplication(client, true);
		Role role = this.getCurrentRole(application, true);
		try {
			this.getService().deleteRole(client.getClientName(), role.getId());
		} catch (Exception e) {
			Assert.fail("Deletion of an existing role failed:");
			e.printStackTrace();
		}
		try {
			this.getService().deleteRole(client.getClientName(), null);
			Assert.fail();
		} catch (IllegalRequestException e) {
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		}
	}

	@Test(expected = BackendException.class)
	public void deleteNonExistingRole() throws Exception {
		Client client = this.getCurrentClient(true);
		Role role = new Role("NotExistingRole");
		try {
			this.getService().deleteRole(client.getClientName(), role.getId());
		} catch (BackendException e) {
			if (e.getMessage().contains(IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION.getMessage()))
				throw e;
			// id is missing -> original exception is an IllegalRequestException
			if (e.getMessage().contains("id to load is required for loading"))
				;
			throw e;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * tests the method that is used to find out if a given user is member of a given group. In this test both the client and the user are persistent.
	 * 
	 */
	@Test
	public void userInGroup() throws Exception {
		// initial work
		Client client;
		Account account;
		Group group;
		client = this.getCurrentClient(false);
		account = this.getCurrentAccount(client, true);
		group = this.getCurrentGroup(client, true);
		try {

			// test if the method returns false, if the association between
			// group
			// and user does not exist
			Assert.assertFalse(this.getService().isUserInGroup(account.getUuid(), group.getId()));
			// Assert.assertFalse(this.getService().isUserInGroup(account.getUuid(),
			// group.getName()));
			// create assignment between account and group
		} catch (Exception e) {
			throw e;
		}
		try {
			// tests if the method returns true, if the association between
			// group and user does exists
			this.getService().addUserToGroup(account.getUuid(), group.getId());
			Assert.assertTrue(this.getService().isUserInGroup(account.getUuid(), group.getId()));
			// Assert.assertTrue(this.getService().isUserInGroup(account.getUuid(),
			// group.getName()));
		} catch (Exception e) {
			throw e;
		}
		try {
			@SuppressWarnings("unused")
			boolean result = this.getService().isUserInGroup(null, group.getId());
			Assert.fail();
		} catch (IllegalRequestException e) {
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		} catch (BackendException e) {
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
		}
	}

	/**
	 * Tests the method that is used to find out if a given user is member of a given group. In this test both the client and the user are NOT persistent.
	 * 
	 */
	@Test
	public void userInGroupNotPersistant() throws Exception {
		// initial work
		Account account = new Account();
		Group group = new Group();
		group.setName("notPersistantGroup1");
		group.setId(new Long(4711));
		// account.setId(new Long(1234));
		account.setUuid("123-456");

		boolean result = this.getService().isUserInGroup(account.getUuid(), group.getId());
		Assert.assertFalse(result);

	}

	/**
	 * Tests the method that is used to find out if a given user is member of a given group. In this test both the client and the user are persistent.
	 */
	@Test
	public void userInRole() throws Exception {
		// initial work
		Client client;
		Application application;
		Account account;
		Role role;
		String scope = "TestScope";
		try {
			client = this.getCurrentClient(false);
			application = this.getCurrentApplication(client, false);
			account = this.getCurrentAccount(client, true);
			role = this.getCurrentRole(application, true);
		} catch (Exception e) {
			throw e;
		}
		// test if the method returns false, if the association between group
		// and user does not exist
		// Assert.assertFalse(this.getService().isUserInRole(account.getUuid(),
		// role, scope));
		Assert.assertFalse(this.getService().isUserInRoleById(account.getUuid(), role.getId(), scope));
		// Assert.assertFalse(this.getService().isUserInRole(account.getUuid(),
		// role.getName(), application.getName(), scope));
		// create assignment between account and group
		try {

			this.getService().addUserToRole(account.getUuid(), role.getId(), scope);
			// tests if the method returns true, if the association between
			// group
			// and user does exist
			// Assert.assertTrue(this.getService().isUserInRole(account.getUuid(),
			// role, scope));
			Assert.assertTrue(this.getService().isUserInRoleById(account.getUuid(), role.getId(), scope));
			// Assert.assertTrue(this.getService().isUserInRole(account.getUuid(),
			// role.getName(), application.getName()));
		} catch (Exception e) {
			throw e;
		}
		try {
			@SuppressWarnings("unused")
			boolean result = this.getService().isUserInRoleById(null, role.getId(), scope);
			Assert.fail();
		} catch (IllegalRequestException e) {
			Assert.assertTrue(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage().contains(e.getMessage()));
		} catch (BackendException e) {
			Assert.assertTrue(e.getMessage().contains("Unable to determine client"));
		}
	}

	/**
	 * Tests the method that is used to find out if a given user is member of a given group. In this test both the client and the user are NOT persistent.
	 * 
	 */
	@Test
	public void userInRoleNotPersistent() throws Exception {
		// TODO find a way to test view in local test
		if (this.getServiceProvider().getClass().getSimpleName().equals("ServiceProviderLocal"))
			return;
		Application application = this.getCurrentApplication(this.getCurrentClient(false), true);

		Role role = new Role();
		role.setId(new Long(4711));
		String scope = "TestScope";
		Account account = new Account();
		role.setName("notPersistantRole1");
		account.setUuid("123-456");

		try {
			boolean result = this.getService().isUserInRoleById(account.getUuid(), role.getId(), scope);
			Assert.assertFalse(result);
		} catch (BackendException e) {
			Assert.assertEquals(IllegalRequestException.CODE_ENTITY_NOT_PERSITENT, e.getErrorCode());
		}
		try {
			boolean result = this.getService().isUserInRole(account.getUuid(), role.getName(), application.getName(), null);
			Assert.assertFalse(result);
		} catch (BackendException e) {
			Assert.assertEquals(e.getErrorCode(), IllegalRequestException.CODE_ENTITY_NOT_PERSITENT);
		}
	}

	@Test
	public void addGroupToRole() throws Exception {
		Client client = this.getCurrentClient(false);
		Application application = this.getCurrentApplication(client, false);
		Role role = this.getCurrentRole(application, true);
		Group group = this.getCurrentGroup(this.getCurrentClient(false), true);
		String scope = "groupToRole";
		try {
			this.getService().addGroupToRole(client.getClientName(), group.getId(), role.getId(), scope);
			// TODO
			// List<Group> resultList = this.getService().
			// Assert.assertEquals(1, resultList.size());
			// Assert.assertEquals(resultList.get(0).getId(), group.getId());

		} catch (Exception e) {
			throw e;
		}
		try {
			this.getService().addGroupToRole(client.getClientName(), group.getId(), role.getId(), scope);
			Assert.fail("No exception thrown while trying to assign a group to a role two times");

		} catch (Exception e) {
			// TODO examine exception
			System.out.println("");
		}
	}

	@Test
	public void getUsersInRole() throws Exception {
		// setup the database state
		Client client = this.getCurrentClient(false);
		Application application = this.getCurrentApplication(client, false);
		Role role = this.getCurrentRole(application, true);
		String scope = "test-Scope";
		try {
			// expect an empty list
			List<Account> resultList = this.getService().getUsersByRole(client.getName(), application.getName(), role.getName(), scope);
			// resultList = this.getService().getUsersByRole(role.getId(),
			// "test-Scope");
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			// expect a list with two entries
			Account account1 = this.getCurrentAccount(client, true);
			Account account2 = this.getCurrentAccount(client, true);
			this.getService().addUserToRole(account1.getUuid(), role.getId(), scope);
			this.getService().addUserToRole(account2.getUuid(), role.getId(), scope);

			resultList = this.getService().getUsersByRole(client.getName(), application.getName(), role.getName(), scope);
			Assert.assertEquals(2, resultList.size());
			boolean condition1 = resultList.get(0).getUuid().equals(account1.getUuid()) && resultList.get(1).getUuid().equals(account2.getUuid());
			boolean condition2 = resultList.get(0).getUuid().equals(account2.getUuid()) && resultList.get(1).getUuid().equals(account1.getUuid());
			Assert.assertTrue(condition1 | condition2);

			// now set one of these accounts as deactivated
			account2.setSoftDelete(true);
			this.getServiceProvider().getAccountManagementService().setAccount(client.getName(), account2);
			// ..and expect only the other one...
			resultList = this.getService().getUsersByRole(client.getName(), application.getName(), role.getName(), scope);
			Assert.assertEquals(1, resultList.size());
			condition1 = resultList.get(0).getUuid().equals(account1.getUuid());
			Assert.assertTrue(condition1);
		} catch (Exception e) {
			throw e;
		}

	}

	@Test
	public void getUsersByGroup() throws Exception {

		Client client = this.getCurrentClient(false);

		try {
			Group group1 = this.getCurrentGroup(client, true);
			Group group2 = this.getCurrentGroup(client, true);
			List<Account> resultList = this.getService().getUsersByGroup(client.getName(), group1.getName());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			Account account1 = this.getCurrentAccount(client, true);
			Account account2 = this.getCurrentAccount(client, true);
			Account account3 = this.getCurrentAccount(client, true);

			this.getService().addUserToGroup(account1.getUuid(), group1.getId());
			this.getService().addUserToGroup(account2.getUuid(), group1.getId());
			this.getService().addUserToGroup(account3.getUuid(), group2.getId());

			resultList = this.getService().getUsersByGroup(client.getName(), group1.getName());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			boolean condition1 = resultList.get(0).getId().equals(account1.getId()) & resultList.get(1).getId().equals(account2.getId());
			boolean condition2 = resultList.get(1).getId().equals(account1.getId()) & resultList.get(0).getId().equals(account2.getId());
			Assert.assertTrue(condition1 | condition2);
		} catch (Exception e) {
			throw e;
		}

	}

	@Test
	public void getGroupsByUser() throws Exception {
		Client client = this.getCurrentClient(true);
		Account account = this.getCurrentAccount(client, true);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		@SuppressWarnings("unused")
		Group group3 = this.getCurrentGroup(client, true);
		try {
			List<Group> resultList = this.getService().getGroupsByUser(account.getUuid());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			this.getService().addUserToGroup(account.getUuid(), group1.getId());
			this.getService().addUserToGroup(account.getUuid(), group2.getId());

			resultList = this.getService().getGroupsByUser(account.getUuid());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, group1.getId());
			this.findIdInList(resultList, group2.getId());

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getRolesByUserAndApplication() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account = this.getCurrentAccount(client, true);
		Application application = this.getCurrentApplication(client, true);
		Role role1 = this.getCurrentRole(application, true);
		Role role2 = this.getCurrentRole(application, true);
		Role role3 = this.getCurrentRole(application, true);
		@SuppressWarnings("unused")
		Role role4 = this.getCurrentRole(application, true);
		Group group = this.getCurrentGroup(client, true);
		try {
			// Query and expect an empty list
			List<Role> resultList = this.getService().getRolesByUserAndApplication(account.getUuid(), application.getId(), "Test-Scope");
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			// Now build a scene that result in a non empty list
			this.getService().addGroupToRole(client.getClientName(), group.getId(), role1.getId(), "Test-Scope");

			this.getService().addUserToGroup(account.getUuid(), group.getId());

			this.getService().addUserToRole(account.getUuid(), role2.getId(), "Test-Scope");
			this.getService().addUserToRole(account.getUuid(), role3.getId(), "Test-Scope2");

			resultList = this.getService().getRolesByUserAndApplication(account.getUuid(), application.getId(), "Test-Scope");
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());

			resultList = this.getService().getRolesByUserAndApplication(account.getUuid(), application.getId(), null);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());
			this.findIdInList(resultList, role3.getId());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getRolesByUser() throws Exception {
		Client client = this.getCurrentClient(true);
		Account account = this.getCurrentAccount(client, true);
		Application application = this.getCurrentApplication(client, true);
		Application application2 = this.getCurrentApplication(client, true);
		Application application3 = this.getCurrentApplication(client, true);
		Role role1 = this.getCurrentRole(application, true);
		Role role2 = this.getCurrentRole(application2, true);
		Role role3 = this.getCurrentRole(application3, true);
		@SuppressWarnings("unused")
		Role role4 = this.getCurrentRole(application, true);
		Group group = this.getCurrentGroup(client, true);
		try {
			// Query and expect an empty list
			List<Role> resultList = this.getService().getRolesByUserAndApplication(account.getUuid(), application.getId(), "Test-Scope");
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			// Now build a scene that result in a non empty list
			this.getService().addGroupToRole(client.getClientName(), group.getId(), role1.getId(), "Test-Scope");

			this.getService().addUserToGroup(account.getUuid(), group.getId());

			this.getService().addUserToRole(account.getUuid(), role2.getId(), "Test-Scope");
			this.getService().addUserToRole(account.getUuid(), role3.getId(), "Test-Scope2");

			resultList = this.getService().getRolesByUserAndApplication(account.getUuid(), null, "Test-Scope");
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());

			resultList = this.getService().getRolesByUserAndApplication(account.getUuid(), null, null);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());
			this.findIdInList(resultList, role3.getId());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getUserRoles() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account = this.getCurrentAccount(client, true);
		Application application = this.getCurrentApplication(client, true);
		Role role1 = this.getCurrentRole(application, true);
		Role role2 = this.getCurrentRole(application, true);
		Role role3 = this.getCurrentRole(application, true);
		@SuppressWarnings("unused")
		Role role4 = this.getCurrentRole(application, true);
		Group group = this.getCurrentGroup(client, true);
		try {
			// Query and expect an empty list
			List<UserRole> resultList = this.getService().getUserRoles(account.getUuid(), application.getId(), "Test-Scope");
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			// Now build a scene that result in a non empty list
			this.getService().addGroupToRole(client.getClientName(), group.getId(), role1.getId(), "Test-Scope");

			this.getService().addUserToGroup(account.getUuid(), group.getId());

			this.getService().addUserToRole(account.getUuid(), role2.getId(), "Test-Scope");
			this.getService().addUserToRole(account.getUuid(), role3.getId(), "Test-Scope2");

			resultList = this.getService().getUserRoles(account.getUuid(), application.getId(), "Test-Scope");
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			UserRole userRole1 = this.findMatchingPropertyInList(resultList, "role.id", role1.getId());
			UserRole userRole2 = this.findMatchingPropertyInList(resultList, "role.id", role2.getId());
			Assert.assertFalse(userRole1.isDirectAssignment());
			Assert.assertTrue(userRole2.isDirectAssignment());

			resultList = this.getService().getUserRoles(account.getUuid(), application.getId(), null);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());
			userRole1 = this.findMatchingPropertyInList(resultList, "role.id", role1.getId());
			userRole2 = this.findMatchingPropertyInList(resultList, "role.id", role2.getId());
			UserRole userRole3 = this.findMatchingPropertyInList(resultList, "role.id", role3.getId());
			Assert.assertFalse(userRole1.isDirectAssignment());
			Assert.assertTrue(userRole2.isDirectAssignment());
			Assert.assertTrue(userRole3.isDirectAssignment());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getRolesByClient() throws Exception {
		/* Create a new client, two applications and three roles */
		Client client = this.getCurrentClient(true);
		Application application1 = this.getCurrentApplication(client, true);
		Application application2 = this.getCurrentApplication(client, true);
		Role role1 = this.getCurrentRole(application1, true);
		Role role2 = this.getCurrentRole(application1, true);
		Role role3 = this.getCurrentRole(application2, true);

		try {
			/* First query by name without additional requests */
			List<Role> resultList = this.getService().getRolesByClient(client.getName(), null).getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());
			this.findIdInList(resultList, role3.getId());
			/* Now the same by id */
			resultList = this.getService().getRolesByClientId(client.getClientName(), client.getId(), null).getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());
			this.findIdInList(resultList, role3.getId());
			Assert.assertNotNull(resultList.get(0).getApplication());

			/*
			 * Now configure a query descriptor that first sorts by role name and then by it's application display name but without any paging informations
			 */
			QueryDescriptor queryDescriptor = new QueryDescriptor();
			BeanOrderProperty orderProperty1 = new BeanOrderProperty(Role.class, RolePropertyFields.FIELD_PATH_NAME, false);
			BeanOrderProperty orderProperty2 = new BeanOrderProperty(Role.class, RolePropertyFields.FIELD_PATH_APPLICATION_PREFIX + ApplicationPropertyFields.FIELD_PATH_DISPLAYNAME, true);
			queryDescriptor.addOrderProperty(orderProperty1);
			queryDescriptor.addOrderProperty(orderProperty2);
			System.out.println("Sorted by role name:...");
			QueryResult<Role> queryResult = this.getService().getRolesByClient(client.getName(), queryDescriptor);
			System.out.println("...done");

			resultList = queryResult.getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());
			this.findIdInList(resultList, role3.getId());
			Assert.assertEquals(role2.getId(), resultList.get(0).getId());
			Assert.assertEquals(role3.getId(), resultList.get(2).getId());

			/* The same query by id */
			resultList = this.getService().getRolesByClientId(client.getClientName(), client.getId(), queryDescriptor).getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());
			this.findIdInList(resultList, role3.getId());
			Assert.assertEquals(role2.getId(), resultList.get(0).getId());
			Assert.assertEquals(role3.getId(), resultList.get(2).getId());

			/* Now add some simple paging information */
			/* first query by name */
			queryDescriptor.setLimit(2);
			queryResult = this.getService().getRolesByClient(client.getName(), queryDescriptor);
			Assert.assertNotNull(queryResult);
			Assert.assertNotNull(queryResult.getResultList());
			Assert.assertEquals(2, queryResult.size());
			Assert.assertEquals(1, queryResult.getPageNumber());
			Assert.assertEquals(2, queryResult.getPageAmount());
			Assert.assertEquals(3, queryResult.getTotalSize().intValue());
			Assert.assertEquals(role2.getId(), resultList.get(0).getId());
			Assert.assertEquals(role1.getId(), resultList.get(1).getId());

			/* Now by id */
			queryResult = this.getService().getRolesByClient(client.getName(), queryDescriptor);
			Assert.assertNotNull(queryResult);
			Assert.assertNotNull(queryResult.getResultList());
			Assert.assertEquals(2, queryResult.size());
			Assert.assertEquals(1, queryResult.getPageNumber());
			Assert.assertEquals(2, queryResult.getPageAmount());
			Assert.assertEquals(3, queryResult.getTotalSize().intValue());
			Assert.assertEquals(role2.getId(), resultList.get(0).getId());
			Assert.assertEquals(role1.getId(), resultList.get(1).getId());

			/* Now add a search property */
			BeanSearchProperty beanSearchProperty = new BeanSearchProperty(Role.class, RolePropertyFields.FIELD_PATH_DISPLAYNAME, "irgendwas");
			queryDescriptor.addSearchProperty(beanSearchProperty);
			queryDescriptor.setLimit(Integer.MAX_VALUE);
			queryDescriptor.setOffSet(0);
			queryResult = this.getService().getRolesByClient(client.getName(), queryDescriptor);
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getRolesByApplication() throws Exception {
		Client client = this.getCurrentClient(false);
		Application application = this.getCurrentApplication(client, true);
		Role role1 = this.getCurrentRole(application, true);
		Role role2 = this.getCurrentRole(application, true);

		try {
			List<Role> resultList = this.getService().getRolesByApplication(client.getName(), application.getName());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());

			resultList = this.getService().getRolesByApplicationId(client.getClientName(), application.getId());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getGroups() throws Exception {
		try {
			Client client = this.getCurrentClient(true);
			QueryResult<Group> queryResult = this.getService().getGroups(client.getName(), null);
			List<Group> groups = queryResult.getResultList();
			Assert.assertNotNull(groups);
			Assert.assertEquals(0, groups.size());
			// Now add some groups to the client and repeat the test:
			Group group1 = this.getCurrentGroup(client, true);
			Group group2 = this.getCurrentGroup(client, true);
			Group group3 = this.getCurrentGroup(client, true);
			groups = this.getService().getGroups(client.getName(), null).getResultList();
			Assert.assertNotNull(groups);
			Assert.assertEquals(3, groups.size());
			this.findIdInList(groups, group1.getId());
			this.findIdInList(groups, group2.getId());
			this.findIdInList(groups, group3.getId());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getGroupsWithQueryDescriptor() throws Exception {

		Client client = this.getCurrentClient(true);
		Group group = null;
		int limit = Integer.MAX_VALUE;

		/* To test paging create a bigger set of groups */
		for (int i = 0; i < 30; i++) {
			String suffix = "0";
			if (i > 9)
				suffix = "";
			group = new Group();
			group.setClient(client);
			group.setName("TestGroup-QueryDescriptor" + suffix + i);
			group.setDisplayName("Bla_BlaßBla\\Bla\\_BlaBla\'Bla");
			this.getService().setGroup(client.getName(), group);
		}
		try {

			/*
			 * First create a simple query descriptor that instructs that the result list is order by group name
			 */
			QueryDescriptor queryDescriptor = new QueryDescriptor();
			BeanOrderProperty orderProperty = new BeanOrderProperty(Group.class, GroupPropertyFields.FIELD_PATH_NAME, true);
			queryDescriptor.addOrderProperty(orderProperty);
			QueryResult<Group> groups = this.getService().getGroups(client.getName(), queryDescriptor);
			Assert.assertNotNull(groups);
			Assert.assertEquals((limit < 30) ? limit : 30, groups.size());

			/*
			 * Now set a limit and though activate paging ( should deliver page one )
			 */
			queryDescriptor.setLimit(limit = 10);

			groups = this.getService().getGroups(client.getName(), queryDescriptor);
			Assert.assertNotNull(groups);
			Assert.assertEquals((limit < 30) ? limit : 30, groups.size());
			Assert.assertEquals(3, groups.getPageAmount());
			Assert.assertEquals(1, groups.getPageNumber());
			Assert.assertEquals("TestGroup-QueryDescriptor00", groups.getResultList().get(0).getName());
			Assert.assertEquals("TestGroup-QueryDescriptor09", groups.getResultList().get(9).getName());

			/*
			 * Now test if obtaining a query descriptor for the next page delivers the correct result
			 */
			queryDescriptor = groups.getDescriptorForPage(2);
			Assert.assertEquals(10, queryDescriptor.getOffSet());
			groups = this.getService().getGroups(client.getName(), queryDescriptor);
			Assert.assertNotNull(groups);
			Assert.assertEquals((limit < 30) ? limit : 30, groups.size());
			Assert.assertEquals(3, groups.getPageAmount());
			Assert.assertEquals(2, groups.getPageNumber());
			Assert.assertEquals("TestGroup-QueryDescriptor10", groups.getResultList().get(0).getName());
			Assert.assertEquals("TestGroup-QueryDescriptor19", groups.getResultList().get(9).getName());

			/* Now add a filtering search property */
			limit = 5;
			queryDescriptor = groups.getDescriptorForPage(1);
			queryDescriptor.setLimit(limit);
			BeanSearchProperty beanSearchProperty = new BeanSearchProperty(Group.class, GroupSearchableFields.FIELD_PATH_NAME, "TestGroup-QueryDescriptor0");
			queryDescriptor.addSearchProperty(beanSearchProperty);
			groups = this.getService().getGroups(client.getName(), queryDescriptor);
			Assert.assertNotNull(groups);
			Assert.assertEquals(groups.size(), limit < 10 ? limit : 10);
			Assert.assertEquals("TestGroup-QueryDescriptor00", groups.getResultList().get(0).getName());
			Assert.assertEquals("TestGroup-QueryDescriptor04", groups.getResultList().get(4).getName());
			Assert.assertEquals(new Integer(10), groups.getTotalSize());
			
			beanSearchProperty.setSearchValue("_");
			beanSearchProperty.setFieldPath(GroupSearchableFields.FIELD_PATH_DISPLAYNAME);
			groups = this.getService().getGroups(client.getName(), queryDescriptor);
			Assert.assertNotNull(groups);
			Assert.assertEquals(groups.size(), limit < 10 ? limit : 10);
			Assert.assertEquals("TestGroup-QueryDescriptor00", groups.getResultList().get(0).getName());
			Assert.assertEquals("TestGroup-QueryDescriptor04", groups.getResultList().get(4).getName());
			Assert.assertEquals(new Integer(30), groups.getTotalSize());
			
			beanSearchProperty.setSearchValue("\\");
			groups = this.getService().getGroups(client.getName(), queryDescriptor);
			Assert.assertNotNull(groups);
			Assert.assertEquals(groups.size(), limit < 10 ? limit : 10);
			Assert.assertEquals("TestGroup-QueryDescriptor00", groups.getResultList().get(0).getName());
			Assert.assertEquals("TestGroup-QueryDescriptor04", groups.getResultList().get(4).getName());
			Assert.assertEquals(new Integer(30), groups.getTotalSize());
			
			beanSearchProperty.setSearchValue("ß");
			groups = this.getService().getGroups(client.getName(), queryDescriptor);
			Assert.assertNotNull(groups);
			Assert.assertEquals(groups.size(), limit < 10 ? limit : 10);
			Assert.assertEquals("TestGroup-QueryDescriptor00", groups.getResultList().get(0).getName());
			Assert.assertEquals("TestGroup-QueryDescriptor04", groups.getResultList().get(4).getName());
			Assert.assertEquals(new Integer(30), groups.getTotalSize());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getRoleByName() throws Exception {
		Client client = this.getCurrentClient(false);
		Application application = this.getCurrentApplication(client, false);
		Role role = this.getCurrentRole(application, true);
		try {

			Role resultRole = this.getService().getRoleByName(client.getName(), application.getName(), role.getName());

			Assert.assertNotNull(resultRole);
			Assert.assertTrue(resultRole.getName().equals(role.getName()));
		} catch (Exception e) {
			Assert.fail("no execption should be thrown" + e.getMessage());
		}
		try {
			this.getService().getRoleByName(client.getName(), application.getName(), "NotExistentRole");
			Assert.fail();
		} catch (Exception e) {
		}
	}

	@Test
	public void getGroup() throws Exception {
		Client client = this.getCurrentClient(false);
		Group group = this.getCurrentGroup(client, true);
		try {
			Assert.assertNotNull(group.getId());
			Group resultGroup = this.getService().getGroup(client.getName(), group.getId());
			Assert.assertNotNull(resultGroup);
			Assert.assertTrue(resultGroup.getName().equals(group.getName()));
			Assert.assertNotNull(resultGroup.getClient());
		} catch (Exception e) {
			Assert.fail("no execption should be thrown" + e.getMessage());
		}
		try {
			Group resultGroup = this.getService().getGroupByName(client.getName(), "NotPersistentGroup");
			Assert.assertNull(resultGroup);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	
	@Test
	public void getGroupByName() throws Exception {
		Client client = this.getCurrentClient(false);
		Group group = this.getCurrentGroup(client, true);
		try {
			Group resultGroup = this.getService().getGroupByName(client.getName(), group.getName());
			Assert.assertNotNull(resultGroup);
			Assert.assertTrue(resultGroup.getName().equals(group.getName()));
			Assert.assertNotNull(resultGroup.getClient());
		} catch (Exception e) {
			Assert.fail("no execption should be thrown" + e.getMessage());
		}
		try {
			Assert.assertNull(this.getService().getGroupByName(client.getName(), "NotPersistentGroup"));
		} catch (Exception e) {
			// do nothing
		}
	}

	@Test
	public void addUserToGroup() throws Exception {
		Client client = this.getCurrentClient(true);
		Group group = this.getCurrentGroup(client, true);
		Account account = this.getCurrentAccount(client, true);
		try {
			this.getService().addUserToGroup(account.getUuid(), group.getId());
			if (!this.getService().isUserInGroup(account.getUuid(), group.getId()))
				Assert.fail("Assigning user to group failed");
		} catch (Exception e) {
			throw e;
		}
		try {
			this.getService().addUserToGroup(account.getUuid(), group.getId());
			Assert.fail("No exception thrown while trying to assign an account to a group two times");
		} catch (Exception e) {
			// TODO examine exception
			System.out.println("");
		}

	}

	@Test
	public void deleteUserFromGroup() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account = this.getCurrentAccount(client, true);
		Group group = this.getCurrentGroup(client, true);

		try {
			// if a given user is not member of group then trying to remove a
			// assignment should result with a certain exception
			this.getService().deleteUserFromGroup(account.getUuid(), group.getId());
			Assert.fail("No exception including the expected has been thrown");
		} catch (BackendException e) {
			String compareString = "There is no assignment for the given group and user account";
			String message = e.getMessage();
			if (!message.contains(compareString))
				Assert.fail("The thrown BackendException is not the one expected: " + e.getMessage());
		} catch (Exception e) {
			throw e;
		}
		try {
			// a user is assigned to a certain group. Try to remove this
			// assignment.
			this.getService().addUserToGroup(account.getUuid(), group.getId());
			Assert.assertTrue(this.getService().isUserInGroup(account.getUuid(), group.getId()));
			this.getService().deleteUserFromGroup(account.getUuid(), group.getId());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void addUserToRole() throws Exception {
		Client client = this.getCurrentClient(true);
		Account account = this.getCurrentAccount(client, true);
		Application application = this.getCurrentApplication(client, true);
		Role role = this.getCurrentRole(application, true);
		try {
			this.getService().addUserToRole(account.getUuid(), role.getId(), "test-Scope");
		} catch (Exception e) {
			throw e;
		}
		try {
			this.getService().addUserToRole(account.getUuid(), role.getId(), "test-Scope");
			Assert.fail("No exception thrown while trying to assign an account to a role two times");
		} catch (Exception e) {
			// TODO examine exception
			System.out.println(e.getMessage());
		}
	}

	@Test
	public void deleteUserFromRole() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account = this.getCurrentAccount(client, true);
		Application application = this.getCurrentApplication(client, false);
		Role role = this.getCurrentRole(application, true);

		try {
			// if a given user is not member of group then trying to remove a
			// assignment should result with a certain exception
			this.getService().deleteUserFromRole(account.getUuid(), role.getId(), "test-Scope");
			Assert.fail("No exception including the expected has been thrown");
		} catch (BackendException e) {
			String compareString = "There is no assignment for the given group and user account";
			String message = e.getMessage();
			if (!message.contains(compareString))
				Assert.fail("The thrown BackendException is not the one expected: " + e.getMessage());
		} catch (Exception e) {
			throw e;
		}
		try {
			// a user is assigned to a certain group. Try to remove this
			// assignment.
			this.getService().addUserToRole(account.getUuid(), role.getId(), "test-Scope");
			if (this.getService().isUserInRoleById(account.getUuid(), role.getId(), "test-Scope"))
				this.getService().deleteUserFromRole(account.getUuid(), role.getId(), "test-Scope");
			else
				Assert.fail("Persisting assignment of a user to a role failed");
		} catch (Exception e) {
			throw e;
		}

	}

	@Test
	public void isUserInRole() throws Exception {
		Client client = this.getCurrentClient(false);
		Account account = this.getCurrentAccount(client, true);
		Application application = this.getCurrentApplication(client, false);
		Role role = this.getCurrentRole(application, true);

		try {
			this.getService().addUserToRole(account.getUuid(), role.getId(), "test-Scope");
			List<Account> accounts = this.getService().getUsersByRole(client.getName(), application.getName(), role.getName(), "test-Scope");
			Assert.assertNotNull(accounts);
			findIdInList(accounts, account.getId());
			Assert.assertTrue(this.getService().isUserInRoleById(account.getUuid(), role.getId(), "test-Scope"));
			Assert.assertTrue(this.getService().isUserInRole(account.getUuid(), role.getName(), application.getName(), "test-Scope"));
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getRoles() throws Exception {
		/* Create client, two applications and three roles */
		Client client = this.getCurrentClient(false);
		Application application1 = this.getCurrentApplication(client, true);
		Application application2 = this.getCurrentApplication(client, true);
		Role role1 = this.getCurrentRole(application1, true);
		Role role2 = this.getCurrentRole(application1, true);
		@SuppressWarnings("unused")
		Role role3 = this.getCurrentRole(application2, true);
		try {
			/*
			 * Search by id: for application one the result should contain two roles
			 */
			List<Role> resultList = this.getService().getRolesByApplicationId(client.getClientName(), application1.getId());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());

			/* Search by name: the result should be the same */
			resultList = this.getService().getRolesByApplication(client.getName(), application1.getName());
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());
		} catch (Exception e) {
			throw e;
		}

		try {
			this.getService().getRolesByApplicationId(client.getClientName(), null);

			Assert.fail();
		} catch (IllegalRequestException e) {
			if (!e.getMessage().contains(IllegalRequestException.MISSING_PARAMETERS_EXCEPTION.getMessage()))
				Assert.fail("Thrown IllegalRequestException is not the expected one: " + e.getMessage());
		}
	}

	@Test
	public void deleteGroupFromRole() throws Exception {
		Client client = this.getCurrentClient(false);
		Group group = this.getCurrentGroup(client, true);
		Application application = this.getCurrentApplication(client, false);
		Role role = this.getCurrentRole(application, true);
		String scope = "GroupToRoleScope";
		try {
			// first try to delete a group that has no assignments and is not
			// assigned to role
			this.getService().deleteGroupFromRole(client.getClientName(), group.getId(), role.getId(), scope);
			// now add several assignments and try to delete that group
		} catch (BackendException e) {
			if (!e.getMessage().contains("Error deleting Assignment of a given group to a role: Assignment does not exist"))
				Assert.fail("BackendException is not the expected one: " + e.getMessage());
		}
		try {
			this.getService().addGroupToRole(client.getClientName(), group.getId(), role.getId(), scope);
			this.getService().deleteGroupFromRole(client.getClientName(), group.getId(), role.getId(), scope);
			// now add several assignments and try to delete that group
		} catch (BackendException e) {
			throw (e);
		}
		try {
			this.getService().addGroupToRole(client.getClientName(), group.getId(), role.getId(), scope);

			this.getService().deleteGroupFromRole(client.getClientName(), group.getId(), role.getId(), scope);
			// now add several assignments and try to delete that group
		} catch (BackendException e) {
			throw (e);
		}
		try {
			// control if deletion has really been done
			this.getService().deleteGroupFromRole(client.getClientName(), group.getId(), role.getId(), scope);
			// now add several assignments and try to delete that group
		} catch (BackendException e) {
			if (!e.getMessage().contains("Error deleting Assignment of a given group to a role: Assignment does not exist"))
				Assert.fail("BackendException is not the expected one: " + e.getMessage());
		}
	}

	@Test
	public void getRolesByGroup() throws Exception {
		// set up the test database scene
		String scope1 = "RolesByGroup-Scope-1";
		String scope2 = "RolesByGroup-Scope-2";
		Client client = this.getCurrentClient(false);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		Application application = this.getCurrentApplication(client, true);
		Role role1 = this.getCurrentRole(application, true);
		Role role2 = this.getCurrentRole(application, true);
		Role role3 = this.getCurrentRole(application, true);
		Account account1 = this.getCurrentAccount(client, true);
		Account account2 = this.getCurrentAccount(client, true);

		this.getService().addGroupToRole(client.getClientName(), group1.getId(), role1.getId(), scope1);
		this.getService().addGroupToRole(client.getClientName(), group2.getId(), role1.getId(), scope1);
		this.getService().addGroupToRole(client.getClientName(), group2.getId(), role2.getId(), scope1);
		this.getService().addGroupToRole(client.getClientName(), group2.getId(), role3.getId(), scope2);

		this.getService().addUserToRole(account1.getUuid(), role1.getId(), scope1);
		this.getService().addUserToRole(account2.getUuid(), role3.getId(), scope2);

		try {
			// first by name
			List<Role> resultList = this.getService().getRolesByGroup(client.getName(), group2.getName(), scope1);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());
			// now by id
			resultList = this.getService().getRolesByGroupId(client.getClientName(), group2.getId(), scope1);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());

			// Now do the query without a given scope.
			// first by name
			resultList = this.getService().getRolesByGroup(client.getName(), group2.getName(), null);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());
			this.findIdInList(resultList, role3.getId());
			// now by id
			resultList = this.getService().getRolesByGroupId(client.getClientName(), group2.getId(), null);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());
			this.findIdInList(resultList, role1.getId());
			this.findIdInList(resultList, role2.getId());
			this.findIdInList(resultList, role3.getId());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getUsersByRoleId() throws Exception {
		// set up the test database scene
		String scope1 = "UsersByRole-Scope-1";
		String scope2 = "UsersByRole-Scope-2";
		Client client = this.getCurrentClient(false);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		Application application = this.getCurrentApplication(client, true);
		Role role1 = this.getCurrentRole(application, true);
		Role role2 = this.getCurrentRole(application, true);
		Role role3 = this.getCurrentRole(application, true);
		@SuppressWarnings("unused")
		Role role4 = this.getCurrentRole(application, true);
		Account account1 = this.getCurrentAccount(client, true);
		Account account2 = this.getCurrentAccount(client, true);
		Account account3 = this.getCurrentAccount(client, true);

		this.getService().addGroupToRole(client.getClientName(), group1.getId(), role1.getId(), scope1);
		this.getService().addGroupToRole(client.getClientName(), group2.getId(), role1.getId(), scope1);
		this.getService().addGroupToRole(client.getClientName(), group2.getId(), role2.getId(), scope1);
		this.getService().addGroupToRole(client.getClientName(), group2.getId(), role3.getId(), scope2);

		this.getService().addUserToRole(account1.getUuid(), role1.getId(), scope1);
		this.getService().addUserToRole(account2.getUuid(), role1.getId(), scope2);
		this.getService().addUserToGroup(account3.getUuid(), group1.getId());

		try {
			List<Account> resultList = this.getService().getUsersByRoleId(client.getClientName(), role1.getId(), scope1);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, account1.getId());
			this.findIdInList(resultList, account3.getId());

			resultList = this.getService().getUsersByRoleId(client.getClientName(), role1.getId(), scope2);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(1, resultList.size());
			this.findIdInList(resultList, account2.getId());

			resultList = this.getService().getUsersByRoleId(client.getClientName(), role1.getId(), null);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());

			this.findIdInList(resultList, account1.getId());
			this.findIdInList(resultList, account2.getId());
			this.findIdInList(resultList, account3.getId());

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void isGroupInRole() throws Exception {
		Client client = this.getCurrentClient(true);
		Group group = this.getCurrentGroup(client, true);
		Application application = this.getCurrentApplication(client, true);
		Role role = this.getCurrentRole(application, true);
		String scope = "TestScope";
		try {
			Assert.assertFalse(this.getService().isGroupInRole(client.getClientName(), group.getId(), role.getId(), scope));
			this.getService().addGroupToRole(client.getClientName(), group.getId(), role.getId(), scope);
			Assert.assertTrue(this.getService().isGroupInRole(client.getClientName(), group.getId(), role.getId(), scope));
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getUsersByGroupAndRole() throws Exception {
		Client client = this.getCurrentClient(true);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		Application application = this.getCurrentApplication(client, true);
		Role role = this.getCurrentRole(application, true);
		Account account1 = this.getCurrentAccount(client, true);
		Account account2 = this.getCurrentAccount(client, true);
		Account account3 = this.getCurrentAccount(client, true);
		Account account4 = this.getCurrentAccount(client, true);
		String scope = "TestScope";
		try {
			this.getService().addUserToRole(account1.getUuid(), role.getId(), scope);
			this.getService().addUserToGroup(account4.getUuid(), group2.getId());
			this.getService().addGroupToRole(client.getClientName(), group2.getId(), role.getId(), scope);

			this.getService().addUserToGroup(account1.getUuid(), group1.getId());
			this.getService().addUserToGroup(account2.getUuid(), group1.getId());
			this.getService().addUserToGroup(account3.getUuid(), group1.getId());
			this.getService().addUserToGroup(account4.getUuid(), group1.getId());
			// first test if account1 will be return if the group is not
			// assigned to the role
			List<Account> resultList = this.getService().getUsersByGroupAndRole(client.getClientName(), group1.getId(), role.getId(), scope);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(2, resultList.size());
			this.findIdInList(resultList, account1.getId());
			this.findIdInList(resultList, account4.getId());

			// now test if all accounts will be return if the group is assigned
			// to the role
			this.getService().addGroupToRole(client.getClientName(), group1.getId(), role.getId(), scope);
			resultList = this.getService().getUsersByGroupAndRole(client.getClientName(), group1.getId(), role.getId(), scope);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(4, resultList.size());
			this.findIdInList(resultList, account1.getId());
			this.findIdInList(resultList, account2.getId());
			this.findIdInList(resultList, account3.getId());
			this.findIdInList(resultList, account4.getId());

		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getGroupsByRole() throws Exception {
		Client client = this.getCurrentClient(false);
		Application application = this.getCurrentApplication(client, true);
		Role role1 = this.getCurrentRole(application, true);
		Role role2 = this.getCurrentRole(application, true);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		Group group3 = this.getCurrentGroup(client, true);
		String scope = "Test-Scope-GroupsByRole";
		List<Group> resultList = null;
		try {
			this.getService().addGroupToRole(client.getClientName(), group3.getId(), role2.getId(), scope);
			resultList = this.getService().getGroupsByRoleId(client.getClientName(), role1.getId(), scope);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());
			resultList = this.getService().getGroupsByRole(client.getName(), application.getName(), role1.getName(), scope);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(0, resultList.size());

			this.getService().addGroupToRole(client.getClientName(), group1.getId(), role1.getId(), scope);
			this.getService().addGroupToRole(client.getClientName(), group2.getId(), role1.getId(), scope);
			this.getService().addGroupToRole(client.getClientName(), group3.getId(), role1.getId(), scope);

			resultList = this.getService().getGroupsByRoleId(client.getClientName(), role1.getId(), scope);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());
			this.findIdInList(resultList, group1.getId());
			this.findIdInList(resultList, group2.getId());
			this.findIdInList(resultList, group3.getId());
			resultList = this.getService().getGroupsByRole(client.getName(), application.getName(), role1.getName(), scope);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(3, resultList.size());
			this.findIdInList(resultList, group1.getId());
			this.findIdInList(resultList, group2.getId());
			this.findIdInList(resultList, group3.getId());

			resultList = this.getService().getGroupsByRoleId(client.getClientName(), role2.getId(), scope);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(1, resultList.size());
			this.findIdInList(resultList, group3.getId());
			resultList = this.getService().getGroupsByRole(client.getName(), application.getName(), role2.getName(), scope);
			Assert.assertNotNull(resultList);
			Assert.assertEquals(1, resultList.size());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void testUsersByRoleTest() throws Exception {
		// TODO find a way to test view in local test
		if (this.getServiceProvider().getClass().getSimpleName().equals("ServiceProviderLocal"))
			return;
		Client client = this.getCurrentClient(true);
		Application application = this.getCurrentApplication(client, true);
		Role role1 = this.getCurrentRole(application, true);
		Role role2 = this.getCurrentRole(application, true);
		Group group1 = this.getCurrentGroup(client, true);
		Group group2 = this.getCurrentGroup(client, true);
		String scope = "RoleUsers-Scope";

		Account account1 = this.getCurrentAccount(client, true);
		Account account2 = this.getCurrentAccount(client, true);
		Account account3 = this.getCurrentAccount(client, true);
		Account account4 = this.getCurrentAccount(client, true);
		Account account5 = this.getCurrentAccount(client, true);

		/*
		 * Since IdM-Backend version 1.1.7 it is mandatory that every user has several values, i.e. first name and last name. Accounts with that have not a first name or a last name will not appear in the user lists
		 */
		ValueSet testAccountName = new ValueSet("personalData");
		testAccountName.addValue(new Value("salutation", "Herr"));
		testAccountName.addValue(new Value("firstname", "Peter"));
		testAccountName.addValue(new Value("lastname", "Silie"));

		this.getServiceProvider().getPrivateDataManagementService().setValues(account1.getUuid(), testAccountName, false);
		this.getServiceProvider().getPrivateDataManagementService().setValues(account2.getUuid(), testAccountName, false);
		this.getServiceProvider().getPrivateDataManagementService().setValues(account3.getUuid(), testAccountName, false);
		this.getServiceProvider().getPrivateDataManagementService().setValues(account4.getUuid(), testAccountName, false);
		this.getServiceProvider().getPrivateDataManagementService().setValues(account5.getUuid(), testAccountName, false);

		this.getService().addUserToGroup(account1.getUuid(), group1.getId());
		this.getService().addUserToGroup(account2.getUuid(), group1.getId());
		this.getService().addUserToGroup(account3.getUuid(), group1.getId());
		this.getService().addUserToGroup(account3.getUuid(), group2.getId());
		this.getService().addGroupToRole(client.getClientName(), group2.getId(), role1.getId(), scope);
		this.getService().addGroupToRole(client.getClientName(), group1.getId(), role1.getId(), scope);
		this.getService().addUserToRole(account4.getUuid(), role1.getId(), scope);
		this.getService().addUserToRole(account5.getUuid(), role2.getId(), scope);
		// TODO
		List<RoleUser> resultList = this.getService().getRoleUsers(client.getClientName(), role1.getId(), scope);
		Assert.assertNotNull(resultList);
		Assert.assertEquals(4, resultList.size());
		RoleUser roleUser = findIdInList(resultList, account1.getId());
		Assert.assertFalse(roleUser.isDirectAssignment());
		roleUser = findIdInList(resultList, account2.getId());
		Assert.assertFalse(roleUser.isDirectAssignment());
		roleUser = findIdInList(resultList, account3.getId());
		Assert.assertFalse(roleUser.isDirectAssignment());
		roleUser = findIdInList(resultList, account4.getId());
		Assert.assertTrue(roleUser.isDirectAssignment());

	}

	// @Test
	public void getUserAccountByGroupIdTest() throws Exception {
		Client client = this.getCurrentClient(true);
		Application application = this.getCurrentApplication(client, true);
		List<Group> groups = new LinkedList<Group>();
		System.err.println("Creating groups");
		for (int i = 0; i < 100; i++) {
			Group group = this.getCurrentGroup(client, true);
			groups.add(group);
			System.err.print(".");
		}
		System.out.println("\nCreating roles");
		List<Role> roles = new LinkedList<Role>();
		for (int i = 0; i < 100; i++) {
			Role role = this.getCurrentRole(application, true);
			roles.add(role);
			System.err.print(".");
		}

		System.err.println("\nCreating users: ");
		List<Account> accounts = new LinkedList<Account>();
		for (int i = 0; i < 100; i++) {
			ValueSet testAccountName = new ValueSet("personalData");
			testAccountName.addValue(new Value("salutation", "Herr"));
			testAccountName.addValue(new Value("firstname", "Peter"));
			testAccountName.addValue(new Value("lastname", "Silie"));
			Account account = this.getCurrentAccount(client, true);
			this.getService().addUserToGroup(account.getUuid(), groups.get(0).getId());
			this.getService().addUserToRole(account.getUuid(), roles.get(i).getId(), "test-Scope_1");
			getServiceProvider().getPrivateDataManagementService().setValues(account.getUuid(), testAccountName, false);
			accounts.add(account);
			System.err.print(".");
		}
		System.err.println("\nAdding groups to roles: ");
		for (int i = 0; i < 100; i++) {
			this.getService().addGroupToRole(client.getName(), groups.get(i).getId(), roles.get(i).getId(), "testGroupScope");
			System.err.print(".");
			// System.err.print("\n");
		}
		try {
			Group group = groups.get(0);
			System.err.println("Query begin:");
			long begin = System.currentTimeMillis();
			List<User> users = this.getService().getUserAccountsByGroupId(client.getName(), group.getId(), null).getResultList();
			long end = System.currentTimeMillis();
			double duration = (end - begin) / 1000;
			DecimalFormat money = new DecimalFormat("0.0000");
			System.err.println("Duration:" + money.format(duration) + "s");
			Assert.assertNotNull(users);
			Assert.assertEquals(100, users.size());
			for (User user : users) {
				Assert.assertEquals("Peter", user.getFirstname());
				Assert.assertEquals("Silie", user.getLastname());
			}
		} catch (Exception e) {
			throw e;
		}
	}

	// @Test
	public void getUsersByGroupLoadTest() throws Exception {
		/* Create 100 users */
		Client client = this.getCurrentClient(true);
		System.out.println("Creating accounts ");
		List<Account> accounts = new LinkedList<Account>();
		Account account = null;
		for (int i = 0; i < 100; i++) {
			System.out.print("#");
			account = this.getCurrentAccount(client, true);
			accounts.add(account);
		}
		/* Create 100 groups */
		System.out.println("\nCreating groups ");
		List<Group> groups = new LinkedList<Group>();
		Group group = null;
		for (int i = 0; i < 100; i++) {
			System.out.println("#");
			group = this.getCurrentGroup(client, true);
			groups.add(group);
			for (int j = 0; j < accounts.size(); j++) {
				System.out.print(".");
				this.getService().addUserToGroup(accounts.get(j).getUuid(), group.getId());
			}
		}
		System.out.println("\nCreating roles");
		/* Create 100 roles */
		List<Group> roles = new LinkedList<Group>();
		Application application = this.getCurrentApplication(client, true);
		Role role = null;
		for (int i = 0; i < 100; i++) {
			System.out.print(".");
			role = this.getCurrentRole(application, true);
			roles.add(group);
			for (int j = 0; j < accounts.size(); j++) {

				// this.getService().addUserToRole(accounts.get(j).getUuid(),role.getId(),"test-Scope-");

				this.getService().addGroupToRole(client.getName(), groups.get(j).getId(), role.getId(), "test-Scope-");
			}
		}

		/* Now query the users of a group */

		long queryBegin = System.currentTimeMillis();
		System.out.println("\nBegin: " + queryBegin);
		List<Account> groupUsers = this.getService().getUsersByGroup(client.getName(), groups.get(50).getName());
		long queryEnd = System.currentTimeMillis();
		double difference = queryEnd - queryBegin / 1000;
		DecimalFormat money = new DecimalFormat("0.00000");
		System.err.println("Time amount: " + money.format(difference) + "s");
		Assert.assertEquals(100, groupUsers.size());
	}

}
