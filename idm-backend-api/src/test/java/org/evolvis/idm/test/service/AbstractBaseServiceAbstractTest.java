package org.evolvis.idm.test.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.service.AccountManagement;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.evolvis.idm.relation.multitenancy.service.ClientManagement;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.service.GroupAndRoleManagement;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public abstract class AbstractBaseServiceAbstractTest<S extends AbstractBaseService> {
	
	/* Often used standard messages */
	protected final String NO_EXCEPTION = "No exception has been thrown, but was expected";
	
	private static final long startTime = System.currentTimeMillis();
	private static int testCounter = 0;
	protected static String errorPrefix = "";

	protected List<String> baseProperties;

	public AbstractBaseServiceAbstractTest() {
		baseProperties = new ArrayList<String>();
		baseProperties.add("id");
		baseProperties.add("name");
		baseProperties.add("displayName");
	}

	/**
	 * @return Return the main service (see class declaration).
	 */
	protected abstract S getService();

	protected abstract ServiceProvider getServiceProvider();

	@Test
	public void testPing() {
		Assert.assertNull(getService().ping(null));
		Assert.assertNotNull(getService().ping(System.currentTimeMillis()));
	}

	@Test
	public void testFault() {
		getService().fault(null);
	}

	@Test
	public void testVersion() {
		Assert.assertNotNull(getService().getVersion());
	}

	protected String getName() {
		// TODO add method name!
		return "unittest" + "-" + startTime + "-" + testCounter++;
	}

	protected String getDisplayName() {
		// TODO add method name!
		return getClass().getSimpleName() + "-" + startTime + "-" + testCounter++;
	}

	/**
	 * Compares the passed String-value properties of two beans
	 * 
	 * @param bean1
	 * @param bean2
	 * @param assertPropertyStringsEqual
	 * @param assertPropertyStringsNotEqual
	 */
	protected void compareBeansStringProperties(Object bean1, Object bean2, List<String> assertPropertyStringsEqual, List<String> assertPropertyStringsNotEqual) throws Exception {
		Assert.assertNotNull("bean 1 is null", bean1);
		Assert.assertNotNull("bean 2 is null", bean2);

		if (assertPropertyStringsEqual != null) {
			for (String property : assertPropertyStringsEqual) {
				Assert.assertEquals(BeanUtils.getProperty(bean1, property), BeanUtils.getProperty(bean2, property));
			}
		}

		if (assertPropertyStringsNotEqual != null) {
			for (String property : assertPropertyStringsNotEqual) {
				Assert.assertFalse(BeanUtils.getProperty(bean1, property).equals(BeanUtils.getProperty(bean2, property)));
			}
		}
	}

	/**
	 * Compares the passed String-value properties of two beans
	 * 
	 * @param bean1
	 * @param bean2
	 * @param assertPropertyStringsEqual
	 * @param assertPropertyStringsNotEqual
	 */
	protected void compareBeansStringProperties(Object bean1, Object bean2, String[] assertPropertyStringsEqual, String[] assertPropertyStringsNotEqual) throws Exception {
		compareBeansStringProperties(bean1, bean2, Arrays.asList(assertPropertyStringsEqual), Arrays.asList(assertPropertyStringsNotEqual));
	}

	/**
	 * Compares the passed String-value properties of two bean lists
	 * 
	 * The bean lists have to be the same size.
	 * 
	 * @param bean1
	 * @param bean2
	 * @param assertPropertyStringsEqual
	 * @param assertPropertyStringsNotEqual
	 */
	protected void compareBeanListStringProperties(List<? extends Object> beanList1, List<? extends Object> beanList2, List<String> assertPropertyStringsEqual, List<String> assertPropertyStringsNotEqual) throws Exception {
		Assert.assertNotNull("bean list 1 is null", beanList1);
		Assert.assertNotNull("bean list 2 is null", beanList2);

		// JUnit failure if bean lists have different sizes
		if (beanList1.size() != beanList2.size()) {
			Assert.fail("Bean lists has not the same size: " + beanList1.size() + " vs " + beanList2.size() + " - Content of bean list 1: " + beanList1 + " - Content of bean list 2: " + beanList2);
		}

		for (int i = 0; i < beanList1.size(); i++)
			compareBeansStringProperties(beanList1.get(i), beanList2.get(i), assertPropertyStringsEqual, assertPropertyStringsNotEqual);
	}

	/**
	 * Compares the passed String-value properties of two bean lists
	 * 
	 * The bean lists have to be the same size.
	 * 
	 * @param bean1
	 * @param bean2
	 * @param assertPropertyStringsEqual
	 * @param assertPropertyStringsNotEqual
	 */
	protected void compareBeanListStringProperties(List<Object> beanList1, List<Object> beanList2, String[] assertPropertyStringsEqual, String[] assertPropertyStringsNotEqual) throws Exception {
		compareBeanListStringProperties(beanList1, beanList2, Arrays.asList(assertPropertyStringsEqual), Arrays.asList(assertPropertyStringsNotEqual));
	}

	protected <T> T findIdInList(List<T> beanList, Long id) throws Exception {
		return findMatchingPropertyInList(beanList, "id", id);
	}

	protected <T> T findNameInList(List<T> beanList, String name) throws Exception {
		return findMatchingPropertyInList(beanList, "name", name);
	}

	protected <T> T findMatchingPropertyInList(List<T> beanList, String propertyPath, Object value) throws Exception {
		PropertyUtilsBean propertyUtil = new PropertyUtilsBean();

		// resolve child properties if necessary
		String[] path = propertyPath.split("\\.");

		if (path.length == 1) {
			for (T bean : beanList) {
				if ((propertyUtil.getProperty(bean, propertyPath)).equals(value))
					return bean;
			}
		} else if (path.length == 2) {
			for (T bean : beanList) {
				Object childBean = propertyUtil.getProperty(bean, path[0]);
				if (childBean != null) {
					Object childPropertyValue = propertyUtil.getProperty(childBean, path[1]);
					if (childPropertyValue != null && childPropertyValue.equals(value)) {
						return bean;
					}
				}
			}
		} else {
			Assert.fail("Deep child paths are not implemented yet.");
		}

		Assert.fail("No Matching property \"" + propertyPath + "\" with value \"" + value + "\" found in bean list: " + beanList);
		return null;
	}

	/* methods to persist default entities and to obtain it. */

	protected Client getCurrentClient(boolean newClient) throws Exception {
		List<Client> result;
		try {
			ClientManagement clientManagement = this.getServiceProvider().getClientManagementService();
			result = clientManagement.getClients(null).getResultList();

			if (result.size() > 0 && !newClient)
				for (Client existingClient : result) {
					if (existingClient.getName().startsWith("default-client"))
						return existingClient;
				}				
			// No client exists, so create a new one.
			Client client = new Client("default-client" + System.currentTimeMillis(), "default-client" + System.currentTimeMillis());
			client = clientManagement.setClient(0, client);
			return client;
		} catch (Exception e) {
			System.err.println("getCurrentClient: Error while persisting a client entity" + e.toString());
			throw e;
		}
	}

	/**
	 * Finds a persistent Account entity for a given client or creates one and
	 * returns it to the caller.
	 * 
	 */
	protected Account getCurrentAccount(Client client, boolean newAccount) throws Exception {
		try {
			// if no parameter client exists - get the default one
			if (client == null) {
				client = this.getCurrentClient(false);
			}
			AccountManagement accountManagement = this.getServiceProvider().getAccountManagementService();
			if(!newAccount){
				QueryResult<Account> queryResult = accountManagement.getAccounts(client.getName(), null);
				List<Account> result = queryResult.getResultList();
				if (!result.isEmpty())
					return result.get(0);
			}
			Account account = new Account("default-account" + System.currentTimeMillis(), "default-account" + System.currentTimeMillis(), client);
			account = accountManagement.setAccount(client.getName(), account);
			return account;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Finds a persistent Application entity for a given client or creates one
	 * and returns it to the caller.
	 * 
	 * @param client
	 *            the client to which the group is associated
	 * @return a default application of the client
	 * @throws BackendException
	 */
	protected Application getCurrentApplication(Client client, boolean newApplication) throws Exception {
		// if no parameter client exists - get the default one
		if (client == null) {
			client = this.getCurrentClient(false);
		}
		ApplicationManagement appManagement = this.getServiceProvider().getApplicationManagementService();
		List<Application> applications = appManagement.getApplications(client.getName(), null).getResultList();
		if (!applications.isEmpty() && !newApplication)
			return applications.get(0);
		int suffix = applications.size();
		Application application = new Application("default-application" + suffix, "default-application" + suffix, client);
		application = appManagement.setApplication(client.getName(), application);
		return application;
	}

	/**
	 * finds a persistent Group entity for a given client or creates one and
	 * returns it to the caller
	 * 
	 * @param client
	 *            the client to which the group is associated
	 * @return a group of the client
	 * @throws BackendException
	 */
	protected Group getCurrentGroup(Client client, boolean newGroup) throws Exception {
		if (client == null) {
			client = this.getCurrentClient(false);
		}
		try {
			QueryDescriptor queryDescriptor = new QueryDescriptor();
			GroupAndRoleManagement grManagement = this.getServiceProvider().getGroupAndRoleManagementService();
			if(!newGroup){
				QueryResult<Group> queryResult = grManagement.getGroups(client.getName(), queryDescriptor);
				List<Group> result = queryResult.getResultList();
				if (result != null && !result.isEmpty())
					return result.get(0);
			}
			Group group = new Group();
			group.setClient(client);
			group.setName("default-group" + System.currentTimeMillis());
			group.setWriteProtected(false);
			group.setDisplayName("Test-Group-Description");
			group = grManagement.setGroup(client.getName(), group);
			return group;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 *Finds a persistent Group entity for a given client or creates one and
	 * returns it to the caller
	 * 
	 * @param client
	 *            the client to which the group is associated
	 * @return a group of the client
	 * @throws BackendException
	 */
	protected Role getCurrentRole(Application application, boolean newRole) throws Exception {
		Client client = this.getCurrentClient(false);
		if (application == null) {
			application = this.getCurrentApplication(client, true);
		}
		GroupAndRoleManagement grManagement = this.getServiceProvider().getGroupAndRoleManagementService();
		List<Role> result = grManagement.getRolesByApplicationId(client.getClientName(), application.getId());
		if (!result.isEmpty() && !newRole)
			return result.get(0);
		int suffix = result.size();
		Role role = new Role("default-role" + suffix, application);
		role.setDisplayName("Test-Description");
		role = grManagement.setRole(client.getName(), role);
		return role;
	}

	/*	*//**
	 * Finds a persistent internal role assignment for the given account
	 * and internal role type or persists a new one.
	 * 
	 * @param account
	 * @param type
	 * @return
	 * @throws Exception
	 */
	/*
	 * @SuppressWarnings("unchecked") protected InternalRoleAssignment
	 * getCurrentInternalRoleAssignment(Account account, InternalRoleType type)
	 * throws Exception { this.beginTransaction(); query if there exists an
	 * appropriate assignment String queryString =
	 * "SELECT irAssignment FROM InternalRoleAssignment irAssignment WHERE irAssignment.account = :account1 AND irAssignment.type = :type1"
	 * ; Query query = this.getEntityManager().createQuery(queryString);
	 * query.setParameter("account1", account); query.setParameter("type1",
	 * type); List<InternalRoleAssignment> resultList = query.getResultList();
	 * this.commitTransaction(); if (resultList.size() == 1) // one exists:
	 * return that return resultList.get(0); if (resultList.size() > 1) // more
	 * than one should never happen throw new
	 * Exception("Illegal database test state: duplicate internal role assignment"
	 * ); // none exists: create a new one this.beginTransaction();
	 * InternalRoleAssignment internalRoleAssignment = new
	 * InternalRoleAssignment(account, type);
	 * this.getEntityManager().persist(internalRoleAssignment);
	 * this.commitTransaction(); return internalRoleAssignment; }
	 */
	/**
	 * Returns a persistent organisational unit instance for a given group. If
	 * there not already at least one instance persistent or the creation of a
	 * new instance is wanted, a new instance will be created, stored and
	 * returned to the caller.
	 * 
	 * @param group
	 *            the group for which an organisational unit is wanted
	 * @param newOrgUnit
	 *            flag which indicates that a new org. unit instance is to be
	 *            created in any case.
	 * @return a persistent instance of OrgUnit.
	 * @throws Exception
	 */
	protected OrgUnit getCurrentOrgUnit(Group group, boolean newOrgUnit) throws Exception {
		// query for all org. units for the given group
		List<OrgUnit> orgUnits = this.getServiceProvider().getOrganisationalUnitManagementService().getOrgUnits(this.getCurrentClient(false).getName(), null).getResultList();
		if (orgUnits.size() > 0 && !newOrgUnit)
			return orgUnits.get(0);
		OrgUnit orgUnit = new OrgUnit();
		orgUnit.setGroup(group);
		orgUnit.setName("default-orgUnit" + System.currentTimeMillis());
		orgUnit.setDisplayName(orgUnit.getName());
		orgUnit = this.getServiceProvider().getOrganisationalUnitManagementService().setOrgUnit(group.getClientName(), orgUnit);
		return orgUnit;
	}

		
	
	/* methods for clearing database tables */

	@After
	public void clearAll() throws Exception {
		QueryResult<Client> queryResult = this.getServiceProvider().getClientManagementService().getClients(null);
		if(queryResult != null){
			List<Client> clients = queryResult.getResultList();
		
			for (Client client : clients) {
				if (client.getName().startsWith("default-client") || client.getName().startsWith("unittest"))
					this.getServiceProvider().getClientManagementService().deleteClient(client);
			}
		}
	}
}
