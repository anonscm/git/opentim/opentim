package org.evolvis.idm.test.service;

import junit.framework.Assert;

import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.relation.permission.model.Group;
import org.junit.Test;

public class QueryResultPagingTest {

	@Test
	public void nothing() {
		// nothing
	}

	@Test
	public void testGetPageNumber() {
		QueryResult<?> queryResult = new QueryResult<Group>();
		QueryDescriptor queryDescriptor = new QueryDescriptor();
		queryResult.setQueryDescriptor(queryDescriptor);
		Assert.assertEquals(0, queryResult.getPageNumber());
		queryResult.setTotalSize(111);
		queryDescriptor.setLimit(10);
		queryDescriptor.setOffSet(50);
		Assert.assertEquals(12, queryResult.getPageAmount());
		Assert.assertEquals(6, queryResult.getPageNumber());
	}

	@Test
	public void testGetPageAmount() {
		QueryResult<?> queryResult = new QueryResult<Group>();
		QueryDescriptor queryDescriptor = new QueryDescriptor();
		queryResult.setQueryDescriptor(queryDescriptor);
		queryResult.setTotalSize(0);

		Assert.assertEquals(0, queryResult.getPageAmount());

		queryResult.setTotalSize(100);
		Assert.assertEquals(0, queryResult.getPageAmount());
		queryDescriptor.setLimit(10);
		Assert.assertEquals(10, queryResult.getPageAmount());
		queryResult.setTotalSize(101);
		Assert.assertEquals(11, queryResult.getPageAmount());
	}

	@Test
	public void testGetQueryDescriptorForPage() {
		QueryResult<?> queryResult = new QueryResult<Group>();
		QueryDescriptor queryDescriptor = new QueryDescriptor();
		queryResult.setQueryDescriptor(queryDescriptor);
		queryResult.setTotalSize(101);

		QueryDescriptor queryDescriptorControl = queryResult.getDescriptorForPage(10);
		Assert.assertEquals(queryDescriptor.getOffSet(), queryDescriptorControl.getOffSet());

		queryDescriptor.setLimit(10);

		queryDescriptorControl = queryResult.getDescriptorForPage(5);
		Assert.assertEquals(40, queryDescriptorControl.getOffSet());

	}
	
	

	
}
