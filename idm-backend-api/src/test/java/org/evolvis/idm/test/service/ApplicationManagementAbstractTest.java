package org.evolvis.idm.test.service;

import java.util.List;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.multitenancy.model.ApplicationPropertyFields;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyFields;
import org.evolvis.idm.relation.multitenancy.service.ApplicationManagement;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public abstract class ApplicationManagementAbstractTest extends AbstractBaseServiceAbstractTest<ApplicationManagement> {

	Client client;

	@Before
	public void init() throws Exception {
		this.client = this.getCurrentClient(false);
	}

	@Test
	public void setApplication() throws Exception {
		Application application = getService().setApplication(client.getName(), new Application(getName(), getDisplayName(), client));
		Assert.assertNotNull(application);
		Assert.assertNotNull(application.getId());
	}

	@Test(expected = BackendException.class)
	public void setApplicationWithNullName() throws Exception {
		getService().setApplication(client.getName(), new Application(null, null, null));
	}

	@Test(expected = BackendException.class)
	public void setApplicationWithEmptyName() throws Exception {
		getService().setApplication(client.getName(), new Application("", "", null));
	}

	@Test(expected = BackendException.class)
	public void setApplicationWithNullDisplayName() throws Exception {
		getService().setApplication(client.getName(), new Application(getName(), null));
	}

	@Test(expected = BackendException.class)
	public void setApplicationWithEmptyDisplayName() throws Exception {
		getService().setApplication(client.getName(), new Application(getName(), "", null));
	}

	@Test
	public void setApplicationDuplicates() throws Exception {
		String name = getName();
		String displayName = getDisplayName();
		
		/* scene: try to persist two application with same name and displayName */
		try {
			getService().setApplication(client.getName(), new Application(name, displayName, client));
			getService().setApplication(client.getName(), new Application(name, displayName, client));
			Assert.fail("Expected exception has not been thrown.");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}
		
		/* scene: try to update a persistent application that has renamed to another persistent application*/
		try {
			Application application2 = new Application("tempAppName", "tempAppName", client);
			application2 = getService().setApplication(client.getName(), application2);
			application2.setName(name);
			application2.setDisplayName(displayName);
			application2 = this.getService().setApplication(client.getName(), application2);
			Assert.fail("Expected exception has not been thrown.");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_INVALID_DUPLICATE, e.getErrorCode());
		}

	}

	@Test
	public void setAndGetApplication() throws Exception {
		Application application = getService().setApplication(client.getName(), new Application("App" + System.currentTimeMillis(), "App" + System.currentTimeMillis(), client));
		Assert.assertNotNull(application);
		Assert.assertNotNull(application.getId());

		Application result = getService().getApplicationById(client.getClientName(), application.getId());

		Assert.assertEquals(application.getId(), result.getId());
		Assert.assertEquals(application.getName(), result.getName());
		Assert.assertEquals(application.getDisplayName(), result.getDisplayName());
		Assert.assertEquals(application.getCertificationData(), result.getCertificationData());
	}

	@Test
	public void setAndChangeApplicationResult() throws Exception {
		String appName = "App" + System.currentTimeMillis();
		Application result1 = getService().setApplication(client.getName(), new Application(appName, appName, client));
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		String oldName = result1.getName();
		result1.setName(getName());

		Application result2 = getService().setApplication(client.getName(), result1);
		Assert.assertFalse(oldName.equals(result2.getName()));
		Assert.assertEquals(result1.getDisplayName(), result2.getDisplayName());

		Application result3 = findIdInList(getService().getApplications(client.getName(), null).getResultList(), result1.getId());
		Assert.assertFalse(oldName.equals(result3.getName()));
		Assert.assertEquals(result1.getDisplayName(), result3.getDisplayName());
	}

	@Test
	public void setAndChangeApplicationById() throws Exception {
		String appName = "App" + System.currentTimeMillis();
		Application result1 = getService().setApplication(client.getName(), new Application(appName, appName, client));
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		String oldName = result1.getName();

		Application result2 = getService().getApplicationById(client.getClientName(), result1.getId());

		String result1Name = result2.getName();
		result2.setName(getName());

		Application result3 = getService().setApplication(client.getName(), result2);
		Assert.assertFalse(result1Name.equals(result3.getName()));
		Assert.assertEquals(result1.getDisplayName(), result3.getDisplayName());

		Application result4 = findIdInList(getService().getApplications(client.getName(), null).getResultList(), result1.getId());
		Assert.assertFalse(oldName.equals(result4.getName()));
		Assert.assertEquals(result1.getDisplayName(), result4.getDisplayName());
	}

	@Test
	public void setAndChangeApplicationList() throws Exception {
		String appName = "App" + System.currentTimeMillis();
		Application result1 = getService().setApplication(client.getName(), new Application(appName, appName, client));
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());
		String oldName = result1.getName();

		Application result2 = findIdInList(getService().getApplications(client.getName(), null).getResultList(), result1.getId());
		result2.setName(getName());

		Application result3 = getService().setApplication(client.getName(), result2);
		Assert.assertFalse(oldName.equals(result3.getName()));
		Assert.assertEquals(result1.getDisplayName(), result3.getDisplayName());

		Application result4 = findIdInList(getService().getApplications(client.getName(), null).getResultList(), result1.getId());
		Assert.assertFalse(oldName.equals(result4.getName()));
		Assert.assertEquals(result1.getDisplayName(), result4.getDisplayName());
	}

	@Test
	public void setAndDeleteApplication() throws Exception {
		Application result1 = getService().setApplication(client.getName(), new Application(getName(), getDisplayName(), client));
		Assert.assertNotNull(result1);
		Assert.assertNotNull(result1.getId());

		getService().deleteApplication(client.getClientName(), result1);

		Application result2 = getService().getApplicationById(client.getClientName(), result1.getId());
		Assert.assertNull(result2);
	}

	@Test
	public void getApplications() throws Exception {
		Client client = this.getCurrentClient(true);
		Application app1 = new Application("Test-App1", "Test-App1", client);
		Application app2 = new Application("Test-App2", "Test-App2", client);
		Application app3 = new Application("Test-App3", "Test-App3", client);
		try {
			app1 = this.getService().setApplication(client.getName(), app1);
			app2 = this.getService().setApplication(client.getName(), app2);
			app3 = this.getService().setApplication(client.getName(), app3);

			List<Application> applications = this.getService().getApplications(client.getName(), null).getResultList();
			Assert.assertEquals(3, applications.size());
		} catch (Exception e) {
			throw e;
		}
	}

	@Test
	public void getApplicationByUuid() throws Exception {
		Client client = this.getCurrentClient(true);
		Application app1 = this.getCurrentApplication(client, true);
		Account account = this.getCurrentAccount(client, true);

		try {
			Application application = this.getService().getApplicationByUuid(account.getUuid(), app1.getName());
			Assert.assertNotNull(application);
			Assert.assertTrue(application.getId().equals(app1.getId()));
		} catch (Exception e) {
			throw e;
		}
		try {
			this.getService().getApplicationByUuid(account.getUuid(), "Test-App-NotExisten");
			Assert.fail("No exception has been thrown");
		} catch (IllegalRequestException e) {
			Assert.assertEquals(IllegalRequestException.CODE_ENTITY_NOT_PERSITENT, e.getErrorCode());
		}
	}

	@Test
	public void getApplicationByName() throws Exception {
		Client client = this.getCurrentClient(true);
		Application app1 = this.getCurrentApplication(client, true);

		try {
			Application application = this.getService().getApplicationByName(client.getName(), app1.getName());
			Assert.assertNotNull(application);
			Assert.assertTrue(application.getId().equals(app1.getId()));
		} catch (Exception e) {
			throw e;
		}
		// try {
		// this.getService().getApplicationByName(client.getName(), "Test-App-NotExistent");
		// Assert.fail("No exception has been thrown");
		// } catch (BackendException e) {
		// if (!e.getMessage().contains(IllegalRequestException.NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION.getMessage()))
		// Assert.fail("Thrown exception is not the expected one");
		// }
	}

	@Test
	public void getApplicationsOrdered() throws Exception {
		Client client = this.getCurrentClient(true);
		for (int i = 0; i < 30; i++) {
			Application application = new Application();
			application.setClient(client);
			application.setName("OrderedApplication" + i);
			application.setDisplayName("OrderedApplDisplayName" + i);
			this.getService().setApplication(client.getName(), application);
		}
		QueryDescriptor queryDescriptor = new QueryDescriptor();
		BeanOrderProperty orderProperty1 = new BeanOrderProperty(Application.class, ApplicationPropertyFields.FIELD_PATH_NAME, false);
		queryDescriptor.addOrderProperty(orderProperty1);
		List<Application> resultList = this.getService().getApplications(client.getName(), queryDescriptor).getResultList();
		Assert.assertNotNull(resultList);
		Assert.assertEquals(30, resultList.size());
		Assert.assertTrue(resultList.get(0).getName().equals("OrderedApplication" + 9));
		Assert.assertTrue(resultList.get(29).getName().equals("OrderedApplication" + 0));
	}

	@Test
	public void getApplicationsSortedByName() throws Exception {
		Client client = this.getCurrentClient(true);
		int maxApplicationIndex = 30;
		Application application;
		for (int i = maxApplicationIndex; i > -1; i--) {
			String suffix = "0";
			if (i > 9)
				suffix = "";
			application = new Application();
			application.setClient(client);
			application.setName("TestSorted" + suffix + i);
			application.setDisplayName("Appl-" + System.currentTimeMillis());
			this.getService().setApplication(client.getName(), application);

		}

		try {
			BeanOrderProperty orderProperty1 = new BeanOrderProperty(Application.class, ApplicationPropertyFields.FIELD_PATH_NAME, true);
			BeanOrderProperty orderProperty2 = new BeanOrderProperty(Application.class, ApplicationPropertyFields.FIELD_PATH_CLIENT_PREFIX + ClientPropertyFields.FIELD_PATH_DISPLAYNAME, true);
			BeanOrderProperty orderProperty3 = new BeanOrderProperty(Application.class, ApplicationPropertyFields.FIELD_PATH_DISPLAYNAME, true);

			QueryDescriptor queryDescriptor = new QueryDescriptor();
			queryDescriptor.addOrderProperty(orderProperty1);
			queryDescriptor.addOrderProperty(orderProperty2);
			queryDescriptor.addOrderProperty(orderProperty3);

			List<Application> resultList = this.getService().getApplications(client.getName(), queryDescriptor).getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(maxApplicationIndex + 1, resultList.size());
			Assert.assertTrue(resultList.get(0).getName().equals("TestSorted00"));
			Assert.assertTrue(resultList.get(maxApplicationIndex).getName().equals("TestSorted30"));

			queryDescriptor = new QueryDescriptor();
			orderProperty1.setOrderAscending(false);
			queryDescriptor.addOrderProperty(orderProperty1);
			QueryResult<Application> queryResult = this.getService().getApplications(client.getName(), queryDescriptor);
			resultList = queryResult.getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(maxApplicationIndex + 1, resultList.size());
			Assert.assertTrue(resultList.get(0).getName().equals("TestSorted30"));
			Assert.assertTrue(resultList.get(maxApplicationIndex).getName().equals("TestSorted00"));

			queryDescriptor.setLimit(5);
			queryDescriptor.setOffSet(10);
			queryResult = this.getService().getApplications(client.getName(), queryDescriptor);
			resultList = queryResult.getResultList();
			Assert.assertNotNull(resultList);
			Assert.assertEquals(5, resultList.size());
			Assert.assertTrue(resultList.get(0).getName().equals("TestSorted20"));
			Assert.assertTrue(resultList.get(4).getName().equals("TestSorted16"));
			Assert.assertEquals(3, queryResult.getPageNumber());
			Assert.assertEquals(7, queryResult.getPageAmount());

		} catch (Exception e) {
			throw e;
		}
	}

}
