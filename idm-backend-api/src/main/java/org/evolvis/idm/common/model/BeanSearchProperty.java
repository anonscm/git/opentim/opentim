package org.evolvis.idm.common.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "beanSearchProperty", namespace = "http://idm.evolvis.org")
@XmlType (name = "BeanSearchProperty", namespace = "http://idm.evolvis.org")
public class BeanSearchProperty implements SearchProperty {

	private Class<? extends AbstractBean> entityType;
	private String fieldPath;
	private String searchValue;
	private ValueType valueType = ValueType.STRING;

	public BeanSearchProperty() {
	}
	
	public BeanSearchProperty(Class<? extends AbstractBean> entityType, String fieldPath, String searchValue) {
		this.entityType = entityType;
		this.fieldPath = fieldPath;
		this.searchValue = searchValue;
	}
	
	public BeanSearchProperty(Class<? extends AbstractBean> entityType, String fieldPath, String searchValue, ValueType valueType) {
		this(entityType, fieldPath, searchValue);
		setValueType(valueType);
	}
		


	public ValueType getValueType() {
		return valueType;
	}

	public void setValueType(ValueType valueType) {
		this.valueType = valueType;
	}

	public void setFieldPath(String fieldPath) {
		this.fieldPath = fieldPath;
	}


	public String getFieldPath() {
		return fieldPath;
	}

	@Override
	public Class<? extends AbstractBean> getEntityType() {
		return entityType;
	}


	public void setEntityType(Class<? extends AbstractBean> entityType) {
		this.entityType = entityType;
	}


	/**
	 * Returns the complete property path which should have the searched pattern. Normally it is created
	 * by the name of the property owning class and the property name, like "myClass.propertyA" if the name of 
	 * the entity class is "MyClass" and the property path is "propertyA". If the parameter is set "myClass" will
	 * be replaced by that given string.
	 * @param propertyPrefix alternative property path prefix.
	 * @return the complete property path which is used in the query to identify the specific attribute.
	 */
	public String getFullPropertyFieldPath(String propertyPrefix){
		if(propertyPrefix == null){
			String[] fieldPathPieces = this.getEntityType().getCanonicalName().split("\\.");
			String tempString = fieldPathPieces[fieldPathPieces.length-1].substring(0, 1).toLowerCase();
			propertyPrefix = tempString + fieldPathPieces[fieldPathPieces.length-1].substring(1);
			//the property name group might lead to an query syntax exception so rename it to "group1"
			if(propertyPrefix.equals("group"))
				propertyPrefix += 1;
		}
		String returnString = propertyPrefix+"."+this.getFieldPath();
		return returnString;
	}


	@Override
	public boolean isValid() {
		try {
			String[] fieldPath = getFieldPath().split("\\.", 2);
			Class<? extends AbstractBean> currentEntityType = this.getEntityType();
			
			String currentFieldPath = fieldPath[0];
			// retrieve Reflection-Objects for getter and field from Entity-Class
			Method beanGetterMethod = currentEntityType.getMethod("get" + getFieldPath().substring(0, 1).toUpperCase() + currentFieldPath.substring(1), (Class<?>[]) null);
			Field beanField = null;
			try{ 
				 beanField = currentEntityType.getDeclaredField(currentFieldPath);
			} catch(Exception e){
				//don't give up - try whether it can be accessed via getter method
			}
			// check if Reflection-Objects have been found
			if (beanGetterMethod == null && beanField == null)
				// field is not sortable if not present
				return false;
			
			// check if field is marked with @Transient-Annotation
			// check method
			Annotation transientAnnotation = beanGetterMethod.getAnnotation(Transient.class);
			// check field
			if (transientAnnotation == null)
				if(beanField != null)
					transientAnnotation = beanField.getAnnotation(Transient.class);
			// return false for JPA-transient fields
			if (transientAnnotation != null)
				return false;
			
			currentEntityType = extractFieldAsAbstractBean(currentEntityType, currentFieldPath);
			if (fieldPath.length > 1) {
				return new BeanSearchProperty(currentEntityType, fieldPath[1], this.getSearchValue()).isValid();
			} else {
				return true;				
			}
		} catch (Exception e) {
			return false;
		}
	}


	@SuppressWarnings("unchecked")
	private Class<? extends AbstractBean> extractFieldAsAbstractBean(Class<? extends AbstractBean> currentEntityType, String currentFieldPath) throws NoSuchFieldException, NoSuchMethodException {
		Class<? extends AbstractBean> returnType = null;
		try{
			 returnType = (Class<? extends AbstractBean>) currentEntityType.getDeclaredField(currentFieldPath).getType();
		} catch(NoSuchFieldException e){
			 returnType = (Class<? extends AbstractBean>) currentEntityType.getMethod("get" + getFieldPath().substring(0, 1).toUpperCase() + currentFieldPath.substring(1), (Class<?>[]) null).getReturnType();
		}
		return returnType;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getSearchValue() {
		return searchValue;
	}

	// private boolean isSortingAllowedForFieldType(Class<?> fieldType) {
	// boolean isAllowed =
	// Class<?>[] allowedFieldTypes = new Class<?>[] { String.class, Long.class, Calendar.class };
	// = Arrays.asList(allowedFieldTypes).contains(fieldType);
	// }

	// public void setSortOrderAscending(boolean sortOrderAscending) {
	// this.sortOrderAscending = sortOrderAscending;
	// }
	//
	//
	// public void setSubOrderProperty(OrderProperty<? extends AbstractBean> subOrderProperty) {
	// this.subOrderProperty = subOrderProperty;
	// }
	//
	//
	// public OrderProperty<? extends AbstractBean> getSubOrderProperty() {
	// return this.subOrderProperty;
	// }
	//
	//
	// public final String toString() {
	// String returnString = this.getFieldName();
	// if (this.isSortOrderAscending())
	// returnString += " ASC ";
	// else
	// returnString += " DESC ";
	// return returnString;
	// }
	//
	//
	// public abstract AbstractBeanOrderProperty<T>[] getOrderProperties();
	//	
	//
	// public boolean validateProperty(AbstractBeanOrderProperty<T> orderProperty) {
	// for (int i = 0; i < getOrderProperties().length; i++) {
	// if (orderProperty.getFieldName().equals(getOrderProperties()[i].getFieldName())) {
	// return true;
	// }
	// }
	// return false;
	// }
	//
	//
	// public boolean validateProperties(List<AbstractBeanOrderProperty<T>> orderProperties) {
	// for (AbstractBeanOrderProperty<T> orderProperty : orderProperties) {
	// if (!validateProperty(orderProperty))
	// return false;
	// }
	// return true;
	// }
}
