package org.evolvis.idm.common.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PersistenceUtil {
	private static Log log = LogFactory.getLog(PersistenceUtil.class);
	
	private static EntityManagerFactory entityManagerFactory;
	
	public static EntityManagerFactory getEntityManagerFactory() {
		if (entityManagerFactory != null)
			return entityManagerFactory;
		
		synchronized (Thread.currentThread()) {
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("idm-backend-test");
			
			try {
				// small test
				emf.createEntityManager().close();
				return entityManagerFactory = emf;
			} catch (Exception e) {
				log.error(e.toString(), e);
				throw new PersistenceException("Couldn't create entity manager factory: " + e.getMessage(), e);
			}
		}
	}
	
	public static EntityManager createEntityManager() {
		return getEntityManagerFactory().createEntityManager();
	}
	
	public static void main(String[] args) {
		EntityManager em = PersistenceUtil.createEntityManager();
		em.close();
	}

	// TODO jneuma check if method must be reimplemented
	public static boolean isLive() {
		return true;
	}
}
