package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.evolvis.idm.common.model.AbstractMap;
import org.evolvis.idm.common.model.AbstractMapEntry;

/**
 * An annotation to validate the content of a Map<String, String> via JSR303 validation according to
 * patterns for the keys and for the values.
 * 
 * @author Michael Kutz, tarent GmbH
 */
@Retention(RUNTIME)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Constraint(validatedBy = { AbstractMapPattern.Validator.class })
public @interface AbstractMapPattern {

	String message() default "violation.abstractMapPattern";

	String keyPattern() default ".*";

	String valuePattern() default ".*";

	Class<?>[] payload() default {};

	Class<?>[] groups() default {};

	static class Validator implements ConstraintValidator<AbstractMapPattern, AbstractMap<?>> {

		AbstractMapPattern constrainAnnotation;

		public void initialize(AbstractMapPattern constraintAnnotation) {
			this.constrainAnnotation = constraintAnnotation;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see javax.validation.ConstraintValidator#isValid(java.lang.Object,
		 * javax.validation.ConstraintValidatorContext)
		 */
		@Override
		public boolean isValid(AbstractMap<?> value, ConstraintValidatorContext context) {

			context.disableDefaultConstraintViolation();

			/*
			 * check for all entries if key matches keyPattern and value matches valuePattern, if
			 * not generate a constraint violation for that field
			 */
			boolean isValid = true;
			for (AbstractMapEntry<?> entry : value.getMapEntries()) {
				if (entry.getKey() == null || !entry.getKey().matches(constrainAnnotation.keyPattern())) {
					context.buildConstraintViolationWithTemplate(constrainAnnotation.message())
							.addNode("mapEntries").addNode(null).inIterable().atKey(
									value.getMapEntries().indexOf(entry)).addNode("key")
							.addConstraintViolation();

					isValid = false;
				}
				if (entry.getValue() != null && !entry.getValue().matches(constrainAnnotation.valuePattern())) {
					context.buildConstraintViolationWithTemplate(constrainAnnotation.message())
							.addNode("mapEntries").addNode(null).inIterable().atKey(
									value.getMapEntries().indexOf(entry)).addNode("value")
							.addConstraintViolation();

					isValid = false;
				}
			}
			return isValid;
		}
	}
}
