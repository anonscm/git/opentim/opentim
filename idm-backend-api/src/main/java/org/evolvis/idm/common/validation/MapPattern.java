package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Map.Entry;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.hibernate.validator.engine.ConstraintValidatorContextImpl;
import org.hibernate.validator.engine.PathImpl;

/**
 * An annotation to validate the content of a Map<String, String> via JSR303 validation according to
 * patterns for the keys and for the values.
 * 
 * @author Michael Kutz, tarent GmbH
 */
@Retention(RUNTIME)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Constraint(validatedBy = { MapPattern.Validator.class })
public @interface MapPattern {

	String message() default "violation.mapPattern";

	String keyPattern() default ".*";

	String valuePattern() default ".*";

	Class<?>[] payload() default {};

	Class<?>[] groups() default {};

	static class Validator implements ConstraintValidator<MapPattern, Map<String, String>> {

		MapPattern constrainAnnotation;

		public void initialize(MapPattern constraintAnnotation) {
			this.constrainAnnotation = constraintAnnotation;
		}

		public boolean isValid(Map<String, String> map, ConstraintValidatorContext context) {

			/*
			 * retrieving property path from Hibernate implementation class
			 */
			String propertyName = ((ConstraintValidatorContextImpl) context)
					.getMessageAndPathList().get(0).getPath().toString();

			context.disableDefaultConstraintViolation();

			/*
			 * evil workaround to change preset property path, otherwise it is not possible to set
			 * the property path to a particular entry in the map
			 */
			try {
				ConstraintValidatorContextImpl contextImpl = (ConstraintValidatorContextImpl) context;
				Field propertyPathField = contextImpl.getClass().getDeclaredField("propertyPath");
				propertyPathField.setAccessible(true);
				propertyPathField.set(contextImpl, PathImpl.createPathFromString(""));
			} catch (Exception e) {
				e.printStackTrace();
			}

			/*
			 * check for all entries if key matches keyPattern and value matches valuePattern, if
			 * not generate a constraint violation for that field
			 */
			boolean isValid = true;
			for (Entry<String, String> entry : map.entrySet()) {
				if (!entry.getKey().matches(constrainAnnotation.keyPattern())
						|| !entry.getValue().matches(constrainAnnotation.valuePattern())) {

					context.buildConstraintViolationWithTemplate(constrainAnnotation.message())
							.addNode(propertyName).addNode(null).inIterable().atKey(entry.getKey())
							.addConstraintViolation();

					isValid = false;
				}
			}
			return isValid;
		}
	}
}
