package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.net.MalformedURLException;
import java.net.URL;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Retention(RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Constraint(validatedBy = {Url.Validator.class})
public @interface Url {
    
	String message() default "{violation.url}";
    
    Class<?>[] payload() default {};
    
    Class<?>[] groups() default {};
    
    static class Validator implements ConstraintValidator<Url, String> {
        
        public void initialize(Url constraintAnnotation) {}
        
        public boolean isValid(String value, ConstraintValidatorContext context) {
        	try {
				new URL(value);
			} catch (MalformedURLException e) {
				/*
				 * URL is malformed and therefore not valid
				 */
				return false;
			}
			/*
			 * no exception therefore the URL is wellformed
			 */
            return true;
        }
    }
}
