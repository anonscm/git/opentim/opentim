package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Pattern;

@Retention(RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Pattern(message = "violation.email", regexp = "[a-zA-Z0-9_-]{1}[a-zA-Z0-9._-]{0,57}@[a-zA-Z0-9.-]{1,255}\\.[a-zA-Z]{2,6}")
@Constraint(validatedBy = {Email.Validator.class})
public @interface Email {
    
    String message() default "";
    
    Class<?>[] payload() default {};
    
    Class<?>[] groups() default {};
    
    static class Validator implements ConstraintValidator<Email, String> {
        
        public void initialize(Email constraintAnnotation) {}
        
        public boolean isValid(String value, ConstraintValidatorContext context) {
            return true;
        }
    }
}
