package org.evolvis.idm.common.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlRootElement(name = "beanOrderProperty", namespace = "http://idm.evolvis.org")
@XmlType (name = "BeanOrderProperty", namespace = "http://idm.evolvis.org")
public class BeanOrderProperty implements OrderProperty {

	private Class<? extends AbstractBean> entityType;
	private String fieldPath;
	private boolean sortOrderAscending = true;

	public BeanOrderProperty() {
	}

	public BeanOrderProperty(Class<? extends AbstractBean> entityType, String fieldPath, boolean sortOrderAscending) {
		this.entityType = entityType;
		this.fieldPath = fieldPath;
		this.sortOrderAscending = sortOrderAscending;
	}

	public void setFieldPath(String fieldPath) {
		this.fieldPath = fieldPath;
	}

	public String getFieldPath() {
		// if (subOrderProperty == null)
		return fieldPath;
		// else
		// return fieldName + "." + subOrderProperty.getFieldName();
	}

	public boolean isSortOrderAscending() {
		// if (subOrderProperty == null)
		return sortOrderAscending;
		// else
		// return subOrderProperty.isSortOrderAscending();
	}

	// public boolean isSubOrderProperty() {
	// return this.subOrderProperty != null;
	// }

	public boolean getOrderAscending() {
		return this.sortOrderAscending;
	}

	public void setOrderAscending(boolean ascending) {
		this.sortOrderAscending = ascending;
	}

	@Override
	public Class<? extends AbstractBean> getEntityType() {
		return entityType;
	}

	public void setEntityType(Class<? extends AbstractBean> entityType) {
		this.entityType = entityType;
	}

	// @Override
	// public boolean isValid() {
	// try {
	// String[] fieldPath = getFieldPath().split(".");
	// Class<? extends AbstractBean> currentEntityType = null;
	// for (int currentPathDepth = 0; currentPathDepth < fieldPath.length; currentPathDepth++) {
	// String currentFieldPath = fieldPath[currentPathDepth];
	// if (currentPathDepth == 0) {
	// currentEntityType = getEntityType();
	// } else {
	// // retrieve child entity from previous Entity-Class
	// currentEntityType = extractFieldAsAbstractBean(currentEntityType, currentFieldPath);
	// }
	//
	// // retrieve Reflection-Objects for getter and field from Entity-Class
	// Method beanGetterMethod = currentEntityType.getMethod("get" + getFieldPath().substring(0, 1).toUpperCase() + getFieldPath().substring(1), (Class<?>[]) null);
	// Field beanField = currentEntityType.getField(getFieldPath());
	//
	// // check if Reflection-Objects have been found
	// if (beanGetterMethod == null || beanField == null)
	// // field is not sortable if not present
	// return false;
	//
	// // check if field is marked with @Transient-Annotation
	// // check method
	// Annotation transientAnnotation = beanGetterMethod.getAnnotation(Transient.class);
	// // check field
	// if (transientAnnotation == null)
	// transientAnnotation = beanField.getAnnotation(Transient.class);
	// // return false for JPA-transient fields
	// if (transientAnnotation != null)
	// return false;
	// }
	// } catch (Exception e) {
	// return false;
	// }
	// return true;
	// }

	@SuppressWarnings("unchecked")
	@Override
	public boolean isValid() {
		try {
			String[] fieldPath = getFieldPath().split("\\.", 2);
			Class<? extends AbstractBean> currentEntityType = this.getEntityType();

			String currentFieldPath = fieldPath[0];

			// retrieve Reflection-Objects for getter and field from Entity-Class
			Method beanGetterMethod = null;
			try {
				beanGetterMethod = currentEntityType.getMethod("get" + getFieldPath().substring(0, 1).toUpperCase() + currentFieldPath.substring(1), (Class<?>[]) null);
			} catch (NoSuchMethodException e) {
				// ok, try to access field directly
			}
			Field beanField = null;
			try {
				beanField = currentEntityType.getDeclaredField(currentFieldPath);
			} catch (Exception e) {
				// don't give up - try whether it can be accessed via getter method
			}
			// check if Reflection-Objects have been found
			if (beanGetterMethod == null && beanField == null){
				Class<?> superClass = currentEntityType.getSuperclass();
				if(superClass != null){		
					for(Annotation annotation : superClass.getAnnotations()){
						if((annotation.annotationType() == Entity.class || annotation.annotationType().equals(MappedSuperclass.class))){
							try{
								BeanOrderProperty newOrderProperty = new BeanOrderProperty((Class<? extends AbstractBean>)superClass, currentFieldPath, this.sortOrderAscending);
								return newOrderProperty.isValid();
							} catch(ClassCastException e){
								// todo throw warning
							}
						}	
					}
				}
				// field is not sortable if not present
				return false;
			}
			// check if field is marked with @Transient-Annotation
			// check method

			Annotation transientAnnotation = null;
			if (beanGetterMethod != null)
				transientAnnotation = beanGetterMethod.getAnnotation(Transient.class);
			// check field
			if (transientAnnotation == null)
				if (beanField != null)
					transientAnnotation = beanField.getAnnotation(Transient.class);
			// return false for JPA-transient fields
			if (transientAnnotation != null)
				return false;
			

			currentEntityType = extractFieldAsAbstractBean(currentEntityType, currentFieldPath);
			if (fieldPath.length > 1) {
				return new BeanOrderProperty(currentEntityType, fieldPath[1], isSortOrderAscending()).isValid();
			} else {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	private Class<? extends AbstractBean> extractFieldAsAbstractBean(Class<? extends AbstractBean> currentEntityType, String currentFieldPath) throws NoSuchFieldException, NoSuchMethodException {
		Class<? extends AbstractBean> returnType = null;
		try {
			returnType = (Class<? extends AbstractBean>) currentEntityType.getDeclaredField(currentFieldPath).getType();
		} catch (NoSuchFieldException e) {
			returnType = (Class<? extends AbstractBean>) currentEntityType.getMethod("get" + getFieldPath().substring(0, 1).toUpperCase() + currentFieldPath.substring(1), (Class<?>[]) null).getReturnType();
		}
		return returnType;
	}

	// private boolean isSortingAllowedForFieldType(Class<?> fieldType) {
	// boolean isAllowed =
	// Class<?>[] allowedFieldTypes = new Class<?>[] { String.class, Long.class, Calendar.class };
	// = Arrays.asList(allowedFieldTypes).contains(fieldType);
	// }

	// public void setSortOrderAscending(boolean sortOrderAscending) {
	// this.sortOrderAscending = sortOrderAscending;
	// }
	//
	//
	// public void setSubOrderProperty(OrderProperty<? extends AbstractBean> subOrderProperty) {
	// this.subOrderProperty = subOrderProperty;
	// }
	//
	//
	// public OrderProperty<? extends AbstractBean> getSubOrderProperty() {
	// return this.subOrderProperty;
	// }
	//
	//
	// public final String toString() {
	// String returnString = this.getFieldName();
	// if (this.isSortOrderAscending())
	// returnString += " ASC ";
	// else
	// returnString += " DESC ";
	// return returnString;
	// }
	//
	//
	// public abstract AbstractBeanOrderProperty<T>[] getOrderProperties();
	//	
	//
	// public boolean validateProperty(AbstractBeanOrderProperty<T> orderProperty) {
	// for (int i = 0; i < getOrderProperties().length; i++) {
	// if (orderProperty.getFieldName().equals(getOrderProperties()[i].getFieldName())) {
	// return true;
	// }
	// }
	// return false;
	// }
	//
	//
	// public boolean validateProperties(List<AbstractBeanOrderProperty<T>> orderProperties) {
	// for (AbstractBeanOrderProperty<T> orderProperty : orderProperties) {
	// if (!validateProperty(orderProperty))
	// return false;
	// }
	// return true;
	// }
}
