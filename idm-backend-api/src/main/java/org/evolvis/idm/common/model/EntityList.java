package org.evolvis.idm.common.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityList", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "entityList", namespace = "http://idm.evolvis.org")
public class EntityList<T extends AbstractEntity> {

	private List<T> entityList;

	public EntityList() {
		// nothing
	}

	public EntityList(List<T> entityList) {
		this.entityList = entityList;
	}

	public void setEntityList(List<T> entityList) {
		this.entityList = entityList;
	}

	public List<T> getEntityList() {
		return entityList;
	}
}
