package org.evolvis.idm.common.model;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@MappedSuperclass
@UniqueConstraint(columnNames = {"key","map_id"})
@XmlType(name = "AbstractMapEntry", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "abstractMapEntry", namespace = "http://idm.evolvis.org")
public abstract class AbstractMapEntry<T extends AbstractMap<?>> extends AbstractEntity implements Serializable, Map.Entry<String, String>{

	private static final long serialVersionUID = 3197572346600161751L;

	public AbstractMapEntry(){
		//nothing
	}
	
	public AbstractMapEntry(String key, String value){
		this.setKey(key);
		this.setValue(value);
	}
	
	public AbstractMapEntry(String key, String value, T map){
		this(key,value);
		this.setMap(map);
	}
	
	
	private String key;

	private String value;

	

	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public String setValue(String value) {
		String returnValue = this.getValue();
		this.value = value;
		return returnValue;
	}

	public String getValue() {
		return value;
	}
	
	@ManyToOne(optional = false)
	@JoinColumn(name="map_id")
	private T map;
	
	public void setMap(T map){
		this.map = map;
	}
	
	@XmlIDREF
	public T getMap(){
		return this.map;
	}
	
	@Override
	public String toString() {
		return super.toString() + " id: "+getId() + " key: " + getKey() + " value: " + getValue();
	}

	@Override
	public String getClientName() {
		if(this.getMap() != null){
			return this.getMap().getClientName();
		}
		return null;
	}
	
	
}
