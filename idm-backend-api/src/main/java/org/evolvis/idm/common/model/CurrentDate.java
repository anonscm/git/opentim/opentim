package org.evolvis.idm.common.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.evolvis.idm.common.util.ConfigurationUtil;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "CURRENTTIMESTAMP")
public class CurrentDate {

	@Id
	@Temporal(TemporalType.TIMESTAMP)
	Calendar timestamp;

	public Calendar getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Calendar timestamp) {
		this.timestamp = timestamp;
	}
}
