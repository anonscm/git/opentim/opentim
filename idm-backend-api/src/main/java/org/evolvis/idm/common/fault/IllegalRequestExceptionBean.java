package org.evolvis.idm.common.fault;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "IllegalRequestException", namespace = "http://idm.evolvis.org")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IllegalRequestException", namespace = "http://idm.evolvis.org", propOrder = {
    "errorCode",
    "illegalProperties",
    "message"
})
public class IllegalRequestExceptionBean {
	private String message;
	private String errorCode;
	
	public IllegalRequestExceptionBean() {
		// TODO Auto-generated constructor stub
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getErrorCode(){
		return this.errorCode;
	}
	
	public void setErrorCode(String errorCode){
		this.errorCode = errorCode;
	}
	
	private List<IllegalProperty> illegalProperties;

	public List<IllegalProperty> getIllegalProperties() {
		return illegalProperties;
	}

	public void setIllegalProperties(List<IllegalProperty> illegalProperties) {
		this.illegalProperties = illegalProperties;
	}
}
