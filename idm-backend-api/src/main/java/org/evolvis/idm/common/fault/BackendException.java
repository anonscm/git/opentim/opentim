package org.evolvis.idm.common.fault;

import javax.xml.ws.WebFault;

@WebFault(faultBean = "org.evolvis.idm.common.fault.BackendExceptionBean")
public class BackendException extends Exception {

	public static final String CODE_UNKNOWN_ERROR = "general.unknown";
	public static final String CODE_CLIENT_NOT_AVAILABLE = "general.clientNotAvailable";
	public static final String CODE_ENTITYMANAGER_UNAVAILABE = "persistence.entityManagerUnavailable";

	public static final BackendException UNKNOWN_ERROR_EXCEPTION = new BackendException(
			"An unknown error occurred while performing the requested operation.",
			CODE_UNKNOWN_ERROR);
	public static final BackendException CLIENT_NOT_AVAILABLE_EXCEPTION = new BackendException(
			"Unable to determine client", CODE_CLIENT_NOT_AVAILABLE);
	public static final BackendException ENTITYMANAGER_UNAVAILABE_EXCEPTION = new BackendException(
			"The requested EntityManager is not available", CODE_ENTITYMANAGER_UNAVAILABE);

	private static final long serialVersionUID = -5992446230687246123L;

	private String errorCode;

	public BackendException() {
	}

	public BackendException(String message) {
		super(message);
	}

	public BackendException(String message, String errorCode) {
		super(message);
		this.setErrorCode(errorCode);
	}

	public BackendException(String message, BackendExceptionBean faultBean) {
		super(message);
		BackendExceptionBean faultBean2 = (BackendExceptionBean) faultBean;
		this.setErrorCode(faultBean2.getErrorCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BackendException) {
			return ((BackendException) obj).getMessage().equals(this.getMessage())
					&& ((BackendException) obj).getErrorCode().equals(this.getErrorCode());
		}
		return false;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	// public BackendExceptionBean getFaultInfo() {
	// BackendExceptionBean faultInfo = new BackendExceptionBean();
	// faultInfo.setMessage(getMessage());
	// faultInfo.setErrorCode(getErrorCode());
	// return faultInfo;
	// }
}
