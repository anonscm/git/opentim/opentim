package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.validation.Constraint;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder;

import org.evolvis.idm.identity.privatedata.model.AttributeType;
import org.evolvis.idm.identity.privatedata.model.Value;

/**
 * An annotation to validate the content of a List<ValueSet> via JSR303 validation according to type
 * and additionalTypeContraint properties of the contained Value's Attributes.
 * 
 * @author Michael Kutz, tarent GmbH
 */
@Retention(RUNTIME)
@Target( { ElementType.TYPE, ElementType.METHOD, ElementType.FIELD })
@Constraint(validatedBy = { AttributeConstraints.Validator.class })
public @interface AttributeConstraints {

	String message() default "violation.attributeConstraints";

	Class<?>[] payload() default {};

	Class<?>[] groups() default {};

	static class Validator extends ExtendedConstraintValidator<AttributeConstraints, Value> {

		@Override
		public boolean isValid(Value value, ConstraintValidatorContext context) {
			/*
			 * Workaround for failing unit tests for PrivateDataManagement TODO jneuma check
			 * workaround / fix unit tests
			 */
			if (value.getAttribute() == null)
				return true;

			AttributeType type = value.getAttribute().getType();

			boolean isValid = true;

			String stringValue = value.getValue() == null ? "" : value.getValue();

			/*
			 * validatable string values must match the attribute's additional type constraint
			 * regular expression
			 */
			if (type.equals(AttributeType.VALIDATABLE)) {
				setMessage(message + ".validatable");
				String regex = value.getAttribute().getAdditionalTypeConstraint();
				isValid = stringValue.matches(regex != null ? regex : "");
			}
			/*
			 * number values need to be parsable integers
			 */
			else if (type.equals(AttributeType.NUMBER)) {
				try {
					setMessage(message + ".number");
					Integer.parseInt(stringValue);
				} catch (NumberFormatException e) {
					isValid = false;
				}
			}
			/*
			 * date values need to be parsable dates
			 */
			else if (type.equals(AttributeType.DATE)) {
				DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
				try {
					setMessage(message + ".date");
					dateFormat.parse(stringValue);
				} catch (ParseException e) {
					isValid = false;
				}
			}
			/*
			 * time values need to be parsable times
			 */
			else if (type.equals(AttributeType.DATETIME)) {
				setMessage(message + ".dateTime");
				DateFormat dateTimeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
				try {
					dateTimeFormat.parse(stringValue);
				} catch (ParseException e) {
					isValid = false;
				}
			}

			/*
			 * set custom property path
			 */
			if (!isValid) {
				addAdditionalAttribute("name", value.getAttribute().getDisplayName());

				context.disableDefaultConstraintViolation();
				ConstraintViolationBuilder violationBuilder = context
						.buildConstraintViolationWithTemplate(getMessage());
				violationBuilder.addNode("value").addConstraintViolation();
			}

			/*
			 * otherwise return true
			 */
			return isValid;
		}
	}
}
