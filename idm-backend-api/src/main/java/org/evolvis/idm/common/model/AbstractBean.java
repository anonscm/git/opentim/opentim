package org.evolvis.idm.common.model;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;

import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;


/**
 * <p>This class extract primary the JAXB {@link XmlID} attribute. This Id will
 * be used as <code>xs:id</code> data for handle cycles in the webservice
 * request or response object graph. For example the classes {@link Attribute}
 * and {@link AttributeGroup} have an reference on each other.</p>
 * 
 * @author Christoph Jerolimov, tarent GmbH
 * @author Jens Neumaier, tarent GmbH
 */

public abstract class AbstractBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6041838497552580386L;
	
//	private List<AbstractBean> XmlAttachedBeans;
	
	
	@Transient
	private String xmlId;
	
	/**
	 * Returns the id of this bean.
	 * @return the id of this bean.
	 */
	public abstract Long getId();
	
	/**
	 * Sets the id of this bean.
	 * @param id the id.
	 */
	public abstract void setId(Long id);

	@XmlID
	@XmlAttribute
	public String getXmlID() {
		if (xmlId == null)
			xmlId = "0x" + Integer.toHexString(System.identityHashCode(this));
		return xmlId;
	}

	/**
	 * Set the {@link #xmlId} of this bean. This data couldn't be changed
	 * after the first assignment.
	 * 
	 * @param xmlId
	 * @throw IllegalStateException if the id is already set.
	 */
	public void setXmlID(String xmlId) {
		if ((xmlId != null && !xmlId.equals(xmlId)) || (xmlId == null && this.xmlId != null))
			throw new IllegalStateException("Object contains already a unique XmlID.");
		this.xmlId = xmlId;
	}
	
	/**
	 * Check if persistent object share the same id
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof AbstractBean && obj.getClass() == this.getClass()) {
			AbstractBean bean = (AbstractBean) obj;
			if (getId() != null && bean.getId() != null && getId().equals(bean.getId()))
				return true;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

//	/**
//	 * @param xmlAttachedBeans the xmlAttachedBeans to set
//	 */
//	public void setXmlAttachedBeans(List<AbstractBean> xmlAttachedBeans) {
//		XmlAttachedBeans = xmlAttachedBeans;
//	}
//
//	/**
//	 * @return the xmlAttachedBeans
//	 */
//	public List<AbstractBean> getXmlAttachedBeans() {
//		if(XmlAttachedBeans == null)
//			XmlAttachedBeans = new LinkedList<AbstractBean>();
//		return XmlAttachedBeans;
//	}
}
