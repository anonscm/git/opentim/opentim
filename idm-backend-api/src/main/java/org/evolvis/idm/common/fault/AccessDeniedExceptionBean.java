package org.evolvis.idm.common.fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "AccessDeniedException", namespace = "http://idm.evolvis.org")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccessDeniedException", namespace = "http://idm.evolvis.org", propOrder = {
    "errorCode",
    "message"
})
public class AccessDeniedExceptionBean {
	private String message;
	private String errorCode;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getErrorCode(){
		return this.errorCode;
	}
	
	public void setErrorCode(String errorCode){
		this.errorCode = errorCode;
	}
}
