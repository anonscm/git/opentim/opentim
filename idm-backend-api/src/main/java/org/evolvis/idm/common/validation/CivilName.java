/**
 * 
 */
package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Pattern;

/**
 * @author Michael Kutz, tarent GmbH
 */
@Retention(RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Pattern(message = "violation.civilName", regexp = "[\\p{L}-'`´ ]*")
@Constraint(validatedBy = {CivilName.Validator.class})
public @interface CivilName {
    
    String message() default "";
    
    Class<?>[] payload() default {};
    
    Class<?>[] groups() default {};
    
    static class Validator implements ConstraintValidator<CivilName, String> {
        
        public void initialize(CivilName constraintAnnotation) {}
        
        public boolean isValid(String value, ConstraintValidatorContext context) {
            return true;
        }
    }

}
