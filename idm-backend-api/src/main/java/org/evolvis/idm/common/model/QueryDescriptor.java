package org.evolvis.idm.common.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name = "queryDescriptor", namespace = "http://idm.evolvis.org")
@XmlType (name = "QueryDescriptor", namespace = "http://idm.evolvis.org")
@XmlAccessorType(value= XmlAccessType.FIELD)
public class QueryDescriptor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3983585159012052487L;
	private int limit = Integer.MAX_VALUE;
	private int offSet = 0;

	@SuppressWarnings("unused")
	@XmlID
	private String xmlId = "queryDescriptor";
	
	// TODO back to orderproperty (xmlannotation)
	@XmlElementWrapper(name = "orderProperties")
	private List<BeanOrderProperty> orderProperties = null;
	
	private List<BeanSearchProperty> searchProperties;


	public int getLimit() {
		return this.limit;
	}


	public void setLimit(int limit) {
		this.limit = limit;
	}


	public void setOffSet(int offSet) {
		this.offSet = offSet;
	}


	public int getOffSet() {
		return this.offSet;
	}


	public List<BeanOrderProperty> getOrderProperties() {
		if(this.orderProperties == null){
			orderProperties = new LinkedList<BeanOrderProperty>();
		}
		return this.orderProperties;
	}


	public void setOrderProperties(List<BeanOrderProperty> orderProperties) {
		this.orderProperties = orderProperties;
	}


	public void addOrderProperty(BeanOrderProperty orderProperty) {
		if (this.orderProperties == null) {
			orderProperties = new LinkedList<BeanOrderProperty>();
		}
		orderProperties.add(orderProperty);
	}


	public static final boolean isPaging(QueryDescriptor queryDescriptor){
		if(queryDescriptor != null && (queryDescriptor.getLimit() < Integer.MAX_VALUE || queryDescriptor.getOffSet() > 0))
			return true;
		return false;
	}
	
	
	public static final boolean isQueryToOrder(QueryDescriptor queryDescriptor){
		if(queryDescriptor != null && (queryDescriptor.isQueryToOrder()))
			return true;
		return false;
	}
	
	
	public boolean isQueryToOrder() {
		return this.orderProperties != null && this.orderProperties.size() > 0;
	}

	
	public void setSearchProperties(List<BeanSearchProperty> searchProperties){
		this.searchProperties = searchProperties;
	}
	
	public List<BeanSearchProperty> getSearchProperties(){
		if(this.searchProperties == null)
			this.searchProperties = new LinkedList<BeanSearchProperty>();
		return this.searchProperties;
	}
	
	public boolean addSearchProperty(BeanSearchProperty beanSearchProperty){
		return this.getSearchProperties().add(beanSearchProperty);
	}
	
	public String createOrderString() {
		if (!this.isQueryToOrder())
			return "";
		OrderProperty orderProperty = orderProperties.get(0);
		String[] fieldPathPieces = orderProperty.getEntityType().getCanonicalName().split("\\.");
		String propertyPrefix = fieldPathPieces[fieldPathPieces.length-1].toLowerCase();
		
		//the property name group might lead to an query syntax exception so rename it to "group1"
		if(propertyPrefix.equals("group"))
			propertyPrefix += 1;
		
		String orderString = " ORDER BY " +propertyPrefix+"."+orderProperties.get(0).getFieldPath();
		if(orderProperties.get(0).isSortOrderAscending())
			orderString += " ASC ";
		else orderString += " DESC ";
		for (int i = 1; i < orderProperties.size(); i++) {
			orderProperty = orderProperties.get(i);
			fieldPathPieces = orderProperty.getEntityType().getCanonicalName().split("\\.");
			propertyPrefix = fieldPathPieces[fieldPathPieces.length-1].toLowerCase();
			if(propertyPrefix.equals("group"))
				propertyPrefix += 1;
			orderString += ", "+propertyPrefix+"."+orderProperty.getFieldPath();
			if(orderProperty.isSortOrderAscending())
				orderString += " ASC ";
			else orderString += " DESC ";
		}
		return orderString;
	}



	
	public boolean hasSearchProperties() {
		return this.getSearchProperties().size() > 0;
	}

}
