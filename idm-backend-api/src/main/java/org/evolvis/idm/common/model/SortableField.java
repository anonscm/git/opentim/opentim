package org.evolvis.idm.common.model;

public class SortableField<T extends AbstractBean> {

	private String fieldName;
	
	public SortableField(String fieldName) {
		this.fieldName = fieldName;
	}
	
	public String getFieldName() {
		return fieldName;
	}
}
