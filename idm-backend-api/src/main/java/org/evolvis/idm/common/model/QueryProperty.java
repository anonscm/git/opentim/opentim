package org.evolvis.idm.common.model;

public interface QueryProperty {

	public  Class<? extends AbstractBean> getEntityType();
	public  String getFieldPath();
	public  boolean isValid();
}
