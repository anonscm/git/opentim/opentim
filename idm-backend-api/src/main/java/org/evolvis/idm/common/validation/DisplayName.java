/**
 * 
 */
package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Pattern;

/**
 * @author Michael Kutz, tarent GmbH
 */
@Retention(RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Pattern(message = "violation.displayName", regexp = DisplayName.PATTERN)
@Constraint(validatedBy = {DisplayName.Validator.class})
public @interface DisplayName {
	
	public static String CHARACTER_CLASS_BASE = "\\p{L}0-9\\-+*~#_.:,'`´\\(\\)\\[\\]{}@$€&§!?/\\\\ ";
	
	public static String PATTERN = "[" + DisplayName.CHARACTER_CLASS_BASE + "]*";
	public static String PATTERN_INVERSE_ONECHAR = "[^" + DisplayName.CHARACTER_CLASS_BASE + "]";
    
    String message() default "";
    
    Class<?>[] payload() default {};
    
    Class<?>[] groups() default {};
    
    static class Validator implements ConstraintValidator<DisplayName, String> {
        
        public void initialize(DisplayName constraintAnnotation) {}
        
        public boolean isValid(String value, ConstraintValidatorContext context) {
            return true;
        }
    }

}
