package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Retention(RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Constraint(validatedBy = {NotNull.Validator.class})
public @interface NotNull {
    
    String message() default "violation.notNull";
    
    Class<?>[] payload() default {};
    
    Class<?>[] groups() default {};
    
    static class Validator implements ConstraintValidator<NotNull, Object> {
        
        public void initialize(NotNull constraintAnnotation) {}
        
        public boolean isValid(Object value, ConstraintValidatorContext context) {
            return value != null;
        }
    }
}
