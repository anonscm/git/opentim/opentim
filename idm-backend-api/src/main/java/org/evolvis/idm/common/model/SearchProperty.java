package org.evolvis.idm.common.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "searchProperty", namespace = "http://idm.evolvis.org")
@XmlType (name = "SearchProperty", namespace = "http://idm.evolvis.org")
public interface SearchProperty extends QueryProperty{
	public String getSearchValue();
	public String getFullPropertyFieldPath(String propertyPrefix);
	public ValueType getValueType();
}
