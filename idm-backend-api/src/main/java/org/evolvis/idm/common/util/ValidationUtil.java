package org.evolvis.idm.common.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.evolvis.idm.common.fault.IllegalProperty;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.AbstractBean;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;

public class ValidationUtil {
	private static ValidatorFactory validatorFactory;
	
	private ValidationUtil() {
		// Nothing.
	}
	
	public static ValidatorFactory getValidatorFactory() {
		if (validatorFactory != null)
			return validatorFactory;
		validatorFactory = Validation.buildDefaultValidatorFactory();
		return validatorFactory;
	}

	public static Validator getValidator() {
		return getValidatorFactory().getValidator();
	}
	
	public static <T> void validate(T bean) throws IllegalRequestException {
		try {
			Set<ConstraintViolation<T>> constraintViolations = getValidator().validate(bean);
			
			if (constraintViolations == null || constraintViolations.size() == 0) {
				
				if (bean instanceof AttributeGroup) {
					if ((((AttributeGroup) bean).getAttributes()) != null)
						for (Attribute attribute : ((AttributeGroup) bean).getAttributes())
							validate(attribute);
				}
				else if (bean instanceof ValueSet) {
					if ((((ValueSet) bean).getValues()) != null)
						for (Value value : ((ValueSet) bean).getValues())
							validate(value);
				}
				
				return;
			}
			
			List<IllegalProperty> data = new LinkedList<IllegalProperty>();
			
			for (ConstraintViolation<T> violation : constraintViolations) {
				IllegalProperty illegalProperty = new IllegalProperty();
				illegalProperty.setBeanName(violation.getLeafBean().getClass().getName());
				illegalProperty.setBeanId(((AbstractBean)violation.getLeafBean()).getId());
				illegalProperty.setPropertyName(violation.getPropertyPath().toString());
				illegalProperty.setConstraintType(violation.getConstraintDescriptor().getAnnotation().annotationType().getSimpleName());
				illegalProperty.setInvalidValue(violation.getInvalidValue());
				
				data.add(illegalProperty);
			}
			
			throw new IllegalRequestException(data);
		} catch (ValidationException e) {
			e.printStackTrace();
			throw new IllegalRequestException();
		}
	}
}
