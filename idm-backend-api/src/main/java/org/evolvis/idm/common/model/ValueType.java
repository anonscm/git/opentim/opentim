package org.evolvis.idm.common.model;

public enum ValueType {
	STRING, LONG
}
