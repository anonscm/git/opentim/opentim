package org.evolvis.idm.common.fault;

import java.util.List;

import javax.xml.ws.WebFault;

@WebFault(faultBean = "org.evolvis.idm.common.fault.IllegalRequestExceptionBean")
public class IllegalRequestException extends BackendException {
	private static final long serialVersionUID = -5992446030687246123L;

	public static final String CODE_MISSING_PARAMETERS = "request.missingParameters";
	public static final String CODE_VALIDATION_ERROR = "request.invalidParameters";
	public static final String CODE_INVALID_DUPLICATE = "request.insertDuplicate";
	public static final String CODE_ENTITY_NOT_PERSITENT = "request.referencedDataNotAvailable";
	public static final String CODE_INVALID_PARAMETER_COMBINATION = "request.invalidParameterCombination";
	public static final String CODE_FORBIDDEN_APPROVAL_MODIFICATION = "request.forbiddenApprovalModification";

	public static final String CODE_LOCKED_ATTRIBUTE = "request.dataIsWriteProtected";
	public static final String CODE_LOCKED_VALUESET = CODE_LOCKED_ATTRIBUTE;
	public static final String CODE_INVALID_VALUE_TO_APPROVE = "request.tryApproveNotPersistentValue";
	public static final String CODE_INVALID_CREDENTIALS = "request.invalidCredentials";

	public static final String FORBIDDEN_APPROVAL_MODIFICATION = "The given modification of a persistent approval is not allowed.";
	public static final String ILLEGAL_VALUE_FOR_APPROVAL = "Creating approval for an unpersistent value (set) is not possible.";
	public static final String INVALID_DUPLICATE_MESSAGE_GENERAL = "Entity could not be persisted because unique constraints are violated.";
	public static final String INVALID_DUPLICATE_MESSAGE_DETAIL_PREFIX = "Entity could not be persisted because the following unique constraints are violated: ";
	public static final String INVALID_CREDENTIALS_GENERAL = "The given credentials are not valid.";
	
	public static final String LOCKED_ATTRIBUTE_MESSAGE_GENERAL = "Unable to delete a locked attribute";
	public static final String LOCKED_VALUESET_MESSAGE_GENERAL = "Unable to modify or delete a locked value set";

	// TODO improve invalid null parameter violation message
	public static final IllegalRequestException MISSING_PARAMETERS_EXCEPTION = new IllegalRequestException("Invalid parameter setting", CODE_MISSING_PARAMETERS);
	public static final IllegalRequestException INVALID_DUPLICATE_EXCEPTION = new IllegalRequestException("", CODE_INVALID_DUPLICATE);
	public static final IllegalRequestException NOT_PERSISTENT_ENTITY_REFERENCED_EXCEPTION = new IllegalRequestException("The given entity is not persistent.", CODE_ENTITY_NOT_PERSITENT);
	public static final IllegalRequestException INVALID_DUPLICATE_CLIENT_EXCEPTION = new IllegalRequestException("Could not persist client because a client with this name has already been persisted.", CODE_INVALID_DUPLICATE);
	public static final IllegalRequestException INVALID_PARAMETER_COMBINATION_EXCEPTION = new IllegalRequestException("The combination of the given parameter results in an invalid query", CODE_INVALID_PARAMETER_COMBINATION);
	public static final IllegalRequestException LOCKED_VALUESET_EXCEPTION = new IllegalRequestException(LOCKED_VALUESET_MESSAGE_GENERAL, CODE_LOCKED_VALUESET);
	public static final IllegalRequestException LOCKED_ATTRIBUTE_EXCEPTION = new IllegalRequestException(LOCKED_ATTRIBUTE_MESSAGE_GENERAL, CODE_LOCKED_ATTRIBUTE);
	public static final IllegalRequestException APPROVAL_FOR_UNPERSISTENT_VALUE_EXCEPTION = new IllegalRequestException(ILLEGAL_VALUE_FOR_APPROVAL, CODE_INVALID_VALUE_TO_APPROVE);
	public static final IllegalRequestException FORBIDDEN_APPROVAL_MODIFICATION_EXCEPTION = new IllegalRequestException(FORBIDDEN_APPROVAL_MODIFICATION, CODE_FORBIDDEN_APPROVAL_MODIFICATION);

	public static final IllegalRequestException INVALID_CREDENTIALS_EXCEPTION = new IllegalRequestException(INVALID_CREDENTIALS_GENERAL, CODE_INVALID_CREDENTIALS);

	private List<IllegalProperty> illegalProperties;

	public IllegalRequestException() {
	}

	public IllegalRequestException(List<IllegalProperty> illegalProperties) {
		setIllegalProperties(illegalProperties);
		setErrorCode(CODE_VALIDATION_ERROR);
	}

	public IllegalRequestException(String message) {
		super(message);
	}

	public IllegalRequestException(String message, String errorCode) {
		super(message, errorCode);
	}

	public IllegalRequestException(String message, IllegalRequestExceptionBean faultBean) {
		super(faultBean.getMessage());
		setErrorCode(faultBean.getErrorCode());
		setIllegalProperties(faultBean.getIllegalProperties());
	}

	public List<IllegalProperty> getIllegalProperties() {
		if (illegalProperties == null || illegalProperties.isEmpty())
			return null;
		return illegalProperties;
	}

	public void setIllegalProperties(List<IllegalProperty> illegalProperties) {
		if (illegalProperties == null || illegalProperties.isEmpty())
			this.illegalProperties = null;
		else
			this.illegalProperties = illegalProperties;
	}

	public IllegalRequestExceptionBean getFaultInfo() {
		IllegalRequestExceptionBean faultInfo = new IllegalRequestExceptionBean();
		faultInfo.setMessage(getMessage());
		faultInfo.setErrorCode(getErrorCode());
		faultInfo.setIllegalProperties(getIllegalProperties());
		return faultInfo;
	}
}
