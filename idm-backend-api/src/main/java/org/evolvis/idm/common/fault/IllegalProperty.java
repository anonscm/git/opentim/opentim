package org.evolvis.idm.common.fault;

public class IllegalProperty {
	private String beanName;
	private Long beanId;
	private String propertyName;
	private String constraintType;
	private String constraintValue;
	private Object invalidValue;

	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public Long getBeanId() {
		return beanId;
	}

	public void setBeanId(Long beanId) {
		this.beanId = beanId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getConstraintType() {
		return constraintType;
	}

	public void setConstraintType(String constraintType) {
		this.constraintType = constraintType;
	}

	public String getConstraintValue() {
		return constraintValue;
	}

	public void setConstraintValue(String constraintValue) {
		this.constraintValue = constraintValue;
	}

	public Object getInvalidValue() {
		return invalidValue;
	}

	public void setInvalidValue(Object invalidValue) {
		this.invalidValue = invalidValue;
	}

	@Override
	public String toString() {
		return super.toString() +
				" [beanName=" + beanName +
				"; propertyName=" + propertyName +
				"; constraintType=" + constraintType +
				"; constraintValue=" + constraintValue +
				"; invalidValue=" + invalidValue + "]";
	}
}
