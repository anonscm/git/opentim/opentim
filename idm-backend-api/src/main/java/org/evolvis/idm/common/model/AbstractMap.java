package org.evolvis.idm.common.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@MappedSuperclass
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractMap", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "abstractMap", namespace = "http://idm.evolvis.org")
public abstract class AbstractMap<T extends AbstractMapEntry<?>> extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7395609736544201576L;

	/**
	 * Returns the list of map entries that are hold by this map.
	 * @return a list of map entries.
	 */
	public abstract List<T> getMapEntries();

	/**
	 * Sets the map entries of this map. If there already entries have been set they will be lost.
	 * @param mapEntries the map entries.
	 */
	public abstract void setMapEntries(List<T> mapEntries);

	
	@Transient
	public void clear() {
		getMapEntries().clear();
	}

	@Transient
	public boolean containsKey(Object key) {
		if (this.getMapEntryIndex((String) key) != -1)
			return true;
		return false;
	}

	@Transient
	public boolean containsValue(Object value) {
		for (T mapEntry : getMapEntries()) {
			if (mapEntry.getValue().compareTo((String) value) == 0)
				return true;
		}
		return false;
	}

	@Transient
	public Set<Map.Entry<String, String>> entrySet() {
		return new HashSet<Map.Entry<String, String>>(this.getMapEntries());
	}

	@Transient
	public String get(Object key) {
		if (getMapEntries() != null) {
			for (T mapEntry : getMapEntries()) {
				if (mapEntry.getKey().compareTo((String) key) == 0)
					return mapEntry.getValue() == null ? "" : mapEntry.getValue().trim();
			}
		}
		return null;
	}

	@Transient
	public boolean isEmpty() {
		return getMapEntries().isEmpty();
	}

	@Transient
	public Set<String> keySet() {
		Set<String> returnSet = new HashSet<String>();
		if (!this.isEmpty()) {
			for (T mapEntry : getMapEntries()) {
				returnSet.add(mapEntry.getKey());
			}
		}
		return returnSet;
	}

	@Transient
	public String put(String key, String value) {
		int oldEntryIndex = this.getMapEntryIndex(key);
		if (oldEntryIndex == -1) {
			T mapEntry = this.createMapEntryInstance(key, value);
			this.getMapEntries().add(mapEntry);
			return null;
		} else {
			T mapEntry = this.getMapEntries().get(oldEntryIndex);
			String returnValue = mapEntry.getValue();
			mapEntry.setValue(value);
			return returnValue;
		}
	}

	@Transient
	// TODO ?
	public void putAll(Map<? extends String, ? extends String> map) {
		Iterator<? extends String> iterator = map.keySet().iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
			this.put(key, map.get(key));
		}
	}

	@Transient
	public String remove(Object key) {
		int index = this.getMapEntryIndex((String) key);
		if (index == -1)
			return null;
		T mapEntry = this.getMapEntries().remove(index);
		return mapEntry.getValue();
	}

	@Transient
	public int size() {
		return this.getMapEntries().size();
	}

	@Transient
	public Collection<String> values() {
		Collection<String> returnCollection = new Vector<String>();
		for (T mapEntry : this.getMapEntries()) {
			returnCollection.add(mapEntry.getValue());
		}
		return returnCollection;
	}

	@Transient
	protected int getMapEntryIndex(String key) {
		for (int i = 0; i < getMapEntries().size(); i++) {
			T mapEntry = getMapEntries().get(i);
			if (mapEntry.getKey().equals(key))
				return i;
		}
		return -1;
	}

	/**
	 * Creates and returns a map entry instance with the given key and value.
	 * @param key the key.
	 * @param value the value.
	 * @return a map entry (compatible to that map) with the given key and value.
	 */
	@Transient
	protected abstract T createMapEntryInstance(String key, String value);
	
	@Transient
	public boolean getBoolean(String key) {
		return Boolean.valueOf(get(key));
	}
	
	@Transient
	public int getInt(String key) throws NumberFormatException {
		return Integer.valueOf(get(key));
	}
	
	@Transient
	public long getLong(String key) throws NumberFormatException {
		return Long.valueOf(get(key));
	}

	@Transient
	public String toString() {
		String returnString = super.toString() + " id: " + this.getId() + ", mapEntries:\n";
		for (T entry : this.getMapEntries()) {
			returnString += "\n " + entry.toString();
		}
		return returnString;
	}
}
