package org.evolvis.idm.common.fault;

import javax.xml.ws.WebFault;

@WebFault(faultBean = "org.evolvis.idm.common.fault.AccessDeniedExceptionBean")
public class AccessDeniedException extends BackendException {
	private static final long serialVersionUID = -5992236230687246123L;
	
	public AccessDeniedException() {
	}
	
	public AccessDeniedException(String message) {
		super(message);
	}
	
	public AccessDeniedException(String message, AccessDeniedException faultBean) {
		super(faultBean.getMessage());
	}
}
