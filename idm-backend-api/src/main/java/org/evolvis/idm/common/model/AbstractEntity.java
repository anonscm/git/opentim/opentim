package org.evolvis.idm.common.model;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@MappedSuperclass
@XmlRootElement(name = "AbstractEntity", namespace = "http://idm.evolvis.org")
@EntityListeners(EntityModificationListener.class)
public abstract class AbstractEntity extends AbstractBean {

	@Transient
	private ReferencedEntityContainer referencedEntityContainer;
	
	@Id
	@GeneratedValue
	@XmlAttribute(name = "id")
	protected Long id;
	
	@XmlTransient
	public abstract String getClientName();

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;	
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractEntity other = (AbstractEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void setReferencedEntityContainer(ReferencedEntityContainer referencedEntityContainer ){
		this.referencedEntityContainer =  referencedEntityContainer;
	}
	
	public ReferencedEntityContainer getReferencedEntityContainer(){
		if(this.referencedEntityContainer == null)
			this.setReferencedEntityContainer(new ReferencedEntityContainer());
		return this.referencedEntityContainer;
	}
	
	/**
	 * @return the given string with a no space break after each 25th not breaking character.
	 */
	@Transient
	protected String makeStringBreakable(String string) {
		//return string.replaceAll("([^\\p{Z}]{25})", "$1&shy;"); &shy; does not work in tables for gecko 1.9.2
		return string.replaceAll("([^\\p{Z}]{25})", "$1<br/>");
	}
}
