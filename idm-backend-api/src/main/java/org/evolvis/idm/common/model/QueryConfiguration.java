package org.evolvis.idm.common.model;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class QueryConfiguration {

	private String qlString = null;
	private Map<String, Object> parameterMap = null;
	private QueryDescriptor queryDescriptor = null;
	private String defaultSortOrder = null;
	private List<Class<?>> allowedOrderBeans = null;
	private Map<Class<? extends AbstractBean>, List<String>> allowedSearchProperties = null;
	

	
	public QueryConfiguration(String qlString, Map<String, Object> parameterMap){
		this.setQlString(qlString);
		this.setParameterMap(parameterMap);
	}
	
	public QueryConfiguration(String qlString, Map<String, Object> parameterMap, QueryDescriptor queryDescriptor, String defaultSortOrder, List<Class<?>> allowedOrderBeans){
		this(qlString, parameterMap);
		this.setAllowedOrderBeans(allowedOrderBeans);
		this.setDefaultSortOrder(defaultSortOrder);
		this.setQueryDescriptor(queryDescriptor);
	}

	public void setParameterMap(Map<String, Object> parameterMap) {
		this.parameterMap = parameterMap;
	}


	public Map<String, Object> getParameterMap() {
		return parameterMap;
	}


	public void setQueryDescriptor(QueryDescriptor queryDescriptor) {
		this.queryDescriptor = queryDescriptor;
	}


	public QueryDescriptor getQueryDescriptor() {
		return queryDescriptor;
	}


	public void setDefaultSortOrder(String defaultOrder) {
		this.defaultSortOrder = defaultOrder;
	}


	public String getDefaultSortOrder() {
		return defaultSortOrder;
	}


	public void setAllowedOrderBeans(List<Class<?>> allowedOrderBeans) {
		this.allowedOrderBeans = allowedOrderBeans;
	}


	public List<Class<?>> getAllowedOrderBeans() {
		return allowedOrderBeans;
	}


	public void setQlString(String qlString) {
		this.qlString = qlString;
	}


	public String getQlString() {
		return qlString;
	}

	/**
	 * Returns the set of beans which contains properties that are allowed to be used as search/filter properties within the query of that configuration.
	 * The set does not contain information about the allowed properties of each single bean.
	 * @return A set of beans that are allowed to be used for search/filter configurations for the query.
	 */
	public Set<Class<? extends AbstractBean>> getAllowedSearchBeans() {
		return this.getAllowedSearchProperties().keySet();
	}

	
	public void setAllowedSearchProperties(Map<Class<? extends AbstractBean>, List<String>> allowedSearchProperties) {
		this.allowedSearchProperties = allowedSearchProperties;
	}

	
	public Map<Class<? extends AbstractBean>, List<String>> getAllowedSearchProperties() {
		return allowedSearchProperties;
	}

}
