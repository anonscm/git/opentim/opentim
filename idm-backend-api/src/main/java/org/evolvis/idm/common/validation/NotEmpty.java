package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Size;

@Retention(RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Size(message = "violation.notEmpty", min = 1)
@Constraint(validatedBy = {NotEmpty.Validator.class})
public @interface NotEmpty {
    
    String message() default "";
    
    Class<?>[] payload() default {};
    
    Class<?>[] groups() default {};
    
    static class Validator implements ConstraintValidator<NotEmpty, String> {
        
        public void initialize(NotEmpty constraintAnnotation) {}
        
        public boolean isValid(String value, ConstraintValidatorContext context) {
            return true;
        }
    }
}
