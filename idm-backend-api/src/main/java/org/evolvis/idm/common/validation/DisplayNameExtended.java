/**
 * 
 */
package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Constraint;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Michael Kutz, tarent GmbH
 */
@Retention(RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Constraint(validatedBy = {DisplayNameExtended.Validator.class})
public @interface DisplayNameExtended {
	
	static final Pattern pattern = Pattern.compile(DisplayName.PATTERN);
	
	static final Pattern patternIllegalChars = Pattern.compile(DisplayName.PATTERN_INVERSE_ONECHAR);
	
	String message() default "violation.displayNameReportInvalidChars";
    
    Class<?>[] payload() default {};
    
    Class<?>[] groups() default {};
    
    static class Validator extends ExtendedConstraintValidator<DisplayNameExtended, String> {
    	
        public boolean isValid(String value, ConstraintValidatorContext context) {
        	boolean isValid = true;
        	
        	if (value != null) {
	        	Matcher validMatcher = pattern.matcher(value);
	        	
	    		isValid = validMatcher.matches();
	    		
	    		if (!isValid) {
	    			Matcher invalidMatcher = patternIllegalChars.matcher(value);
	    			
	    			Set<Character> illegalChars = new HashSet<Character>();
	    			while (invalidMatcher.find()) {
	    				illegalChars.add(invalidMatcher.group().charAt(0));
	    			}
	
	    			StringBuilder illegalCharsMessageBuilder = new StringBuilder(illegalChars.size()*4);
	    			for (Character illegalChar : illegalChars) {
	    				illegalCharsMessageBuilder.append("\"");
	    				illegalCharsMessageBuilder.append(illegalChar);
	    				illegalCharsMessageBuilder.append("\", ");
	    			}
	    			
	    			String illegalCharsMessage = illegalCharsMessageBuilder.substring(0, illegalCharsMessageBuilder.length() - 2);
	    			
	    			addAdditionalAttribute("illegalCharsMessage", illegalCharsMessage);
	        		
	        		context.disableDefaultConstraintViolation();
	        		context.buildConstraintViolationWithTemplate(getMessage()).addConstraintViolation();
	    		}
        	}
        	
            return isValid;
        }
    }

}
