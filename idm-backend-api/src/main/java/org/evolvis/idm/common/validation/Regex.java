package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Retention(RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Constraint(validatedBy = {Regex.Validator.class})
public @interface Regex {
    
	String message() default "{violation.regex}";
    
    Class<?>[] payload() default {};
    
    Class<?>[] groups() default {};
    
    static class Validator implements ConstraintValidator<Regex, String> {
        
        public void initialize(Regex constraintAnnotation) {}
        
        public boolean isValid(String value, ConstraintValidatorContext context) {
        	try {
				Pattern.compile(value);
			} catch (PatternSyntaxException e) {
				/*
				 * pattern is malformed and therefore not valid
				 */
				return false;
			}
			/*
			 * no exception therefore the pattern is valid
			 */
            return true;
        }
    }
}
