package org.evolvis.idm.common.model;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class EntityModificationListener{
	
	protected final Log log = LogFactory.getLog(getClass());
	
	@PrePersist
	public void prePersist(AbstractEntity entity){
//		log.debug("prePersist: "+entity.toString());
	}
	
	@PostPersist
	public void postPersist(AbstractEntity entity){
//		log.debug("postPersist: "+entity.toString());
	}
	
	@PreUpdate
	public void preUpdate(AbstractEntity entity){
//		log.debug("preUpdate: "+entity.toString());
	}
	
	@PostUpdate
	public void postUpdate(AbstractEntity entity){
//		log.debug("postUpdate: "+entity.toString());
	}
	
	@PreRemove
	public void preRemove(AbstractEntity entity){
//		log.debug("preRemove: "+entity.toString());
	}
	
	@PostRemove
	public void postRemove(AbstractEntity entity){
//		log.debug("postRemove: "+entity.toString());
	}
}
