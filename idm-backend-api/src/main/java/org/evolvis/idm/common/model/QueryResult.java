package org.evolvis.idm.common.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "queryResult", namespace = "http://idm.evolvis.org")
@XmlType(name = "QueryResult", namespace = "http://idm.evolvis.org")
@XmlAccessorType(XmlAccessType.FIELD)
public class QueryResult<T extends AbstractEntity>  implements Serializable/*, List<T> */{

	private List<T> resultList;
	
	private static final long serialVersionUID = -7315690987971134246L;

	
	@XmlElement(name = "queryDescriptor", type = QueryDescriptor.class)
	private QueryDescriptor queryDescriptor;

	@XmlAttribute(name = "totalSize", required = true)
	private Integer totalSize = new Integer(0);


	public QueryResult() {
	}


	public QueryResult(QueryResult<T> queryResult) {
		this.setTotalSize(queryResult.getTotalSize());
		// this.setQueryDescriptor(queryResult.getQueryDescriptor());
		this.setResultList(queryResult.getResultList());
	}


	/**
	 * Sets the total size of the query result.
	 * @param the size of the query result.
	 */
	public void setTotalSize(Integer totalSize) {
		this.totalSize = totalSize;
	}


	/**
	 * Returns the total size of the query result. The list of this QueryResult instance may only contain a section of the hole query result.
	 * 
	 * @return the total size of the query result.
	 */
	public Integer getTotalSize() {
		if(this.totalSize != 0)
			return this.totalSize;
		else if(this.getResultList() != null) {
				return new Integer(this.getResultList().size());
			 }
		return 0;
	}


	/**
	 * Sets the query descriptor. That should be the instance that is used to build the query in order to retrieve correct information.
	 * @param queryDescriptor 
	 */
	public void setQueryDescriptor(QueryDescriptor queryDescriptor) {
		this.queryDescriptor = queryDescriptor;
	}


	/**
	 * Returns the query descriptor that has been set for that query result. Normally that is that instance that has been used for building
	 * the query.
	 * @return the query descriptor of that query result. <code>null</code> if none has been set before.
	 */
	public QueryDescriptor getQueryDescriptor() {
		return queryDescriptor;
	}

	/**
	 * Sets the result list of the query.
	 * 
	 * @param resultList the result list.
	 */
	public void setResultList(List<T> resultList){
		this.resultList = resultList;
	}

	
	//@XmlElementRef(name = "listelement")
	public List<T> getResultList(){
		if(this.resultList == null)
			this.resultList = new LinkedList<T>();
		return this.resultList;
	}


	/**
	 * Returns the current page that this query result represents. If there was no paging information set for the query
	 * the page number is always one if 
	 * @return
	 */
	public int getPageNumber() {
		if (QueryDescriptor.isPaging(queryDescriptor)) {
			if (this.getQueryDescriptor().getOffSet() % this.getQueryDescriptor().getLimit() == 0) {
				int pageNumber = (queryDescriptor.getOffSet() / queryDescriptor.getLimit()) + 1;
				return pageNumber;
			}
			// TODO may be else case should throw an exception
		}
		return (this.getResultList().size() > 0) ? 1 : 0;
	}


	public QueryDescriptor getDescriptorForPage(int page) {
		if (!QueryDescriptor.isPaging(this.getQueryDescriptor()))
			return this.queryDescriptor;
		// TODO check happens if the given page is greater than the page amount
		this.getQueryDescriptor().setOffSet(this.getOffSetForPage(page));
		return queryDescriptor;
	}


	protected int getOffSetForPage(int page) {
		if (!QueryDescriptor.isPaging(this.getQueryDescriptor()) || page == 0) {
			return 0;
		}
		int returnValue = (page - 1) * this.getQueryDescriptor().getLimit();
		return returnValue;
	}


	public int getPageAmount() {
		if (QueryDescriptor.isPaging(getQueryDescriptor())) {
			int pageAmount = this.getTotalSize() / queryDescriptor.getLimit();
			int remainder = this.getTotalSize() % queryDescriptor.getLimit();
			if (remainder > 0)
				pageAmount++;
			return pageAmount;
		}
		return (this.getResultList().size() > 0) ? 1 : 0;
	}


	/* The following methods are wrapper methods for the list interface */

	//@Override
	public boolean add(T e) {
		return this.getResultList().add(e);
	}


	//@Override
	public void add(int index, T element) {
		this.getResultList().add(index, element);

	}


	//@Override
	public boolean addAll(Collection<? extends T> c) {
		return this.getResultList().addAll(c);
	}


	//@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		return this.getResultList().addAll(index, c);
	}


	//@Override
	public void clear() {
		this.getResultList().clear();

	}


	//@Override
	public boolean contains(Object o) {
		return this.getResultList().contains(o);
	}


	//@Override
	public boolean containsAll(Collection<?> c) {
		return this.getResultList().containsAll(c);
	}


	//@Override
	public T get(int index) {
		return this.getResultList().get(index);
	}


	//@Override
	public int indexOf(Object o) {
		return this.getResultList().indexOf(o);
	}


	//@Override
	public boolean isEmpty() {
		return this.getResultList().isEmpty();
	}


	//@Override
	public Iterator<T> iterator() {
		return this.getResultList().iterator();
	}


	//@Override
	public int lastIndexOf(Object o) {
		return this.getResultList().lastIndexOf(o);
	}


	//@Override
	public ListIterator<T> listIterator() {
		return this.getResultList().listIterator();
	}


	//@Override
	public ListIterator<T> listIterator(int index) {
		return this.getResultList().listIterator(index);
	}


	//@Override
	public boolean remove(Object o) {
		return this.getResultList().remove(o);
	}


	//@Override
	public T remove(int index) {
		return this.getResultList().remove(index);
	}


	//@Override
	public boolean removeAll(Collection<?> c) {
		return this.getResultList().removeAll(c);
	}


	//@Override
	public boolean retainAll(Collection<?> c) {
		return this.getResultList().retainAll(c);
	}


	//@Override
	public T set(int index, T element) {
		return this.getResultList().set(index, element);
	}


	//@Override
	public int size() {
		return this.getResultList().size();
	}


	//@Override
	public List<T> subList(int fromIndex, int toIndex) {
		return this.getResultList().subList(fromIndex, toIndex);
	}


	//@Override
	public Object[] toArray() {
		return this.getResultList().toArray();
	}


	//@Override
	public <A> A[] toArray(A[] a) {
		return this.getResultList().toArray(a);
	}


		


}