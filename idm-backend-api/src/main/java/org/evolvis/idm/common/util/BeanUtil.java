package org.evolvis.idm.common.util;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtilsBean;

public class BeanUtil {
	
	protected static final PropertyUtilsBean propertyUtil = new PropertyUtilsBean();
	
	@SuppressWarnings("unchecked")
	public static <S, D extends S> void copyProperties(S source, D destination) {
		try {			
			Map<String, ?> properties = BeanUtils.describe(source);
			for (String key : properties.keySet()) {
				if (!key.equals("class") && (!key.equals("id") || propertyUtil.getProperty(source, "id") != null))
					BeanUtils.copyProperty(destination, key, propertyUtil.getProperty(source, key));
			}
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
