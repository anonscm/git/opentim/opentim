package org.evolvis.idm.common.service;

import javax.ejb.SessionContext;
import javax.jws.WebMethod;
import javax.persistence.EntityManager;

public interface AbstractJPAService {
	@WebMethod(exclude = true)
	public EntityManager getEntityManager();
	@WebMethod(exclude = true)
	public void setEntityManager(EntityManager[] entityManager);
	@WebMethod(exclude = true)
	public SessionContext getSessionContext();
	@WebMethod(exclude = true)
	public void setSessionContext(SessionContext sessionContext);
	
	@WebMethod(exclude = true)
	public boolean isContainerManagedTransactionDisabled();
	@WebMethod(exclude = true)
	public void setContainerManagedTransactionDisabled(boolean isContainerManagedTransactionDisabled);
	@WebMethod(exclude = true)
	public void beginTransaction();
	@WebMethod(exclude = true)
	public void commitTransaction();
	@WebMethod(exclude = true)
	public void rollbackTransaction();
}
