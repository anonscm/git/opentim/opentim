package org.evolvis.idm.common.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "AbstractManagementService", name = "AbstractManagementServicePT", portName = "AbstractManagementServicePort")
public interface AbstractBaseService {
	@WebMethod
	@WebResult(name = "servertime")
	public abstract Long ping(@WebParam(name = "clienttime") Long clienttime);
	
	@WebMethod
	public abstract void fault(@WebParam(name = "className") String className);
	
	@WebMethod
	@WebResult(name = "version")
	public abstract String getVersion();
}
