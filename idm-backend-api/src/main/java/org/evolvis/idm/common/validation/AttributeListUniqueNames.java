package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.evolvis.idm.identity.privatedata.model.Attribute;

@Retention(RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Constraint(validatedBy = {AttributeListUniqueNames.Validator.class})
public @interface AttributeListUniqueNames {
    
	String message() default "{violation.attributeGroup.duplicateAttribute}";
    
    Class<?>[] payload() default {};
    
    Class<?>[] groups() default {};
    
    static class Validator implements ConstraintValidator<AttributeListUniqueNames, List<Attribute>> {

		@Override
		public void initialize(AttributeListUniqueNames constraintAnnotation) {}

		@Override
		public boolean isValid(List<Attribute> values, ConstraintValidatorContext context) {
			if(values != null){
				Set<String> foundAttributeNames = new HashSet<String>(values.size());
				for (Attribute attribute : values) {
					if (!foundAttributeNames.add(attribute.getName()))
						return false;
				}
			}
			return true;
		}
        
    }
}
