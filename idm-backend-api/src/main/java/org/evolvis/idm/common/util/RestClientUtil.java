package org.evolvis.idm.common.util;

import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;
import org.evolvis.opensso.rest.RestClient;

public class RestClientUtil {
	
	public static RestClient createClient(ClientPropertyMap clientPropertyMap) {
		return RestClient.getInstance().initialize(
					clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_SERVICEURL),
					clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_ADMINLOGIN),
					clientPropertyMap.get(ClientPropertyMap.KEY_OPENSSO_ADMINPASSWORD));
	}
}
