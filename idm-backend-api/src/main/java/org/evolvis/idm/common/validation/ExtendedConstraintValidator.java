/**
 * 
 */
package org.evolvis.idm.common.validation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.validation.ConstraintValidator;

/**
 * @author Jens Neumaier, tarent GmbH
 * @author Michael Kutz, tarent GmbH
 *
 */
public abstract class ExtendedConstraintValidator<A extends Annotation, T> implements ConstraintValidator<A, T> {
	
	Map<String, String> attributes = new HashMap<String, String>(); 
	String message = "";
	protected A constraintAnnotation;
    
    public void initialize(A constraintAnnotation) {
    	this.constraintAnnotation = constraintAnnotation;
    	try {
			Method getMessageMethod = constraintAnnotation.getClass().getMethod("message");
			this.message = (String) getMessageMethod.invoke(constraintAnnotation);
		} catch (Exception e) {}
    }
    
    protected void addAdditionalAttribute(String key, String value) {
    	attributes.put(key, value);
    }
    
    protected void setMessage(String message) {
    	this.message = message;
    }
    
    protected String getMessage() {
    	StringBuffer messageStringBuffer = new StringBuffer(message);
    	for (Entry<String, String> attribute : attributes.entrySet()) {
    		messageStringBuffer.append("//" + attribute.getKey() + "==" + attribute.getValue());
    	}
    	return messageStringBuffer.toString();
    }
}
