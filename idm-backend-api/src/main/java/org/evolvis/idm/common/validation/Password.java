//package org.evolvis.idm.common.validation;
//
//import static java.lang.annotation.RetentionPolicy.RUNTIME;
//
//import java.lang.annotation.ElementType;
//import java.lang.annotation.Retention;
//import java.lang.annotation.Target;
//
//import javax.validation.Constraint;
//import javax.validation.ConstraintValidator;
//import javax.validation.ConstraintValidatorContext;
//import javax.validation.constraints.Pattern;
//
//@Retention(RUNTIME)
//@Target({ElementType.METHOD, ElementType.FIELD})
//@Pattern(message = "violation.password", regexp = "[\\x20-\\x7E]*")
//@Constraint(validatedBy = {Password.Validator.class})
//public @interface Password {
//    
//    String message() default "";
//    
//    Class<?>[] payload() default {};
//    
//    Class<?>[] groups() default {};
//    
//    static class Validator implements ConstraintValidator<Password, String> {
//        
//        public void initialize(Password constraintAnnotation) {}
//        
//        public boolean isValid(String value, ConstraintValidatorContext context) {
//            return true;
//        }
//    }
//}
