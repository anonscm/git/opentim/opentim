package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;

@Retention(RUNTIME)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Constraint(validatedBy = {FieldEquality.Validator.class})
public @interface FieldEquality {
    /**
     * Names of the fields that should be validated for equality.
     */
    String[] value();
    
    Class<?>[] payload() default {};

    /**
     * Default message
     */
    String message() default "violation.fieldEquality";

    Class<?>[] groups() default {};

    /**
     * Validator for FieldEquality
     */
    class Validator implements ConstraintValidator<FieldEquality, Object> {

    	private String message;
        private String[] targetFieldNames;

        public void initialize(FieldEquality constraintAnnotation) {
        	message = constraintAnnotation.message();
            targetFieldNames = constraintAnnotation.value();
        }

        public boolean isValid(Object value, ConstraintValidatorContext context) {

            if (value == null)
                return false;
            
            String firstValue = null;
            
            for (int i = 0; i < targetFieldNames.length; i++) {
            	try {
            		String fieldValue = BeanUtils.getProperty(value, targetFieldNames[i]);
					
            		if (i == 0) {
            			firstValue = fieldValue;
            		}
            		            		
					if ((firstValue == null && fieldValue != null) || (firstValue != null && !firstValue.equals(fieldValue))) {
						context.disableDefaultConstraintViolation();
						context.buildConstraintViolationWithTemplate(message).addNode(targetFieldNames[i]).addConstraintViolation();
						return false;
					}
				} catch (Exception e) {
					return false;
				}
            }
            
            return true;
        }
    }
}
