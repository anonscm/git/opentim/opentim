package org.evolvis.idm.common.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "orderProperty", namespace = "http://idm.evolvis.org")
@XmlType (name = "OrderProperty", namespace = "http://idm.evolvis.org")
public interface OrderProperty extends QueryProperty{

	public boolean isSortOrderAscending();
	
}
