package org.evolvis.idm.common.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.privatedata.model.Approval;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "referencedEntityContainer", namespace = "http://idm.evolvis.org")
public class ReferencedEntityContainer extends AbstractBean {

	private Long id;

	private Map<String, EntityList<? extends AbstractEntity>> entityMap;

	@Override
	public Long getId() {
		return this.id;
	}

	public void setEntityMap(Map<String, EntityList<? extends AbstractEntity>> entityMap) {
		this.entityMap = entityMap;
	}

	public Map<String, EntityList<? extends AbstractEntity>> getEntityMap() {
		if (this.entityMap == null)
			this.entityMap = new HashMap<String, EntityList<? extends AbstractEntity>>();
		return entityMap;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	private <T extends AbstractEntity> void putIdMEntityList(String key, List<T> entityList) {
		EntityList<T> idMEntityList = new EntityList<T>(entityList);
		this.getEntityMap().put(key, idMEntityList);
	}

	@SuppressWarnings("unchecked")
	private <T extends AbstractEntity> List<T> getList(String key) {
		EntityList<T> entityList = (EntityList<T>) this.getEntityMap().get(key);
		if (entityList != null)
			return entityList.getEntityList();
		return null;
	}
	
	private <T extends AbstractEntity> List<T> getList(String key, boolean createListIfNotExists){
		List<T> resultList = this.getList(key);
		if(resultList == null && createListIfNotExists){
			resultList = new LinkedList<T>();
			this.putIdMEntityList(key, resultList);
		}
		return resultList;
	}

	public void setApprovals(List<Approval> approvals) {
		this.putIdMEntityList("approvals", approvals);
	}

	public List<Approval> getApprovals() {
		return this.getList("approvals", true);
	}

	public void setAccounts(List<Account> accounts) {
		this.putIdMEntityList("accounts", accounts);
	}

	public List<Account> getAccounts() {
		return this.getList("accounts", true);
	}

	public void setValues(List<Value> values) {
		this.putIdMEntityList("values", values);
	}

	public void addValue(Value value) {
			this.getValues().add(value);
		
	}

	public List<Value> getValues() {
		return getList("values", true);
	}

	public void setValueSets(List<ValueSet> values) {
		this.putIdMEntityList("valueSets", values);
	}

	public void setAttributeGroups(List<AttributeGroup> attributeGroups){
		this.putIdMEntityList("attributeGroups", attributeGroups);
	}
	
	public void setAttributes(List<Attribute> attributes){
		this.putIdMEntityList("attributes", attributes);
	}
	
	public void addAttribute(Attribute attribute){
		this.getAttributes().add(attribute);
	}
	
	public void addAttributeGroup(AttributeGroup attributeGroup){
		this.getAttributeGroups().add(attributeGroup);
	}
	
	public void addValueSet(ValueSet valueSet) {
		this.getValueSets().add(valueSet);
	}

	public List<ValueSet> getValueSets() {
		return this.getList("valueSets", true);
	}
	
	public List<AttributeGroup> getAttributeGroups() {
		return this.getList("attributeGroups", true);
	}
	
	public List<Attribute> getAttributes(){
		return this.getList("attributes", true);
	}
}
