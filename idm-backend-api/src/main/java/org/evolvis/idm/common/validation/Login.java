package org.evolvis.idm.common.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.Pattern;

@Retention(RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
@Pattern(message = "violation.login", regexp = "[a-zA-Z0-9-]*")
@Constraint(validatedBy = {Login.Validator.class})
public @interface Login {
    
    String message() default "";
    
    Class<?>[] payload() default {};
    
    Class<?>[] groups() default {};
    
    static class Validator implements ConstraintValidator<Login, String> {
        
        public void initialize(Login constraintAnnotation) {}
        
        public boolean isValid(String value, ConstraintValidatorContext context) {
            return true;
        }
    }
}
