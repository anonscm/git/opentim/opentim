package org.evolvis.idm.ws.unsecured;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;

@WebService(serviceName = "PrivateDataRetrievalService", name = "PrivateDataRetrievalServicePT", portName = "PrivateDataRetrievalServicePort", targetNamespace = "http://idm.evolvis.org")
public interface PrivateDataRetrieval {

	@WebMethod
	@WebResult(name = "attributes")
	public abstract List<AttributeGroup> getAttributes(@WebParam(name = "clientName") String clientName) throws BackendException;


	@WebMethod
	@WebResult(name = "attributes")
	public abstract AttributeGroup getAttributeGroupByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeGroupName") String attributeGroupName) throws BackendException;


	@WebMethod
	@WebResult(name = "values")
	public abstract List<ValueSet> getValues(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid) throws BackendException;


	@WebMethod
	@WebResult(name = "valueSet")
	public abstract ValueSet getValuesById(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "id") Long id) throws BackendException;


	@WebMethod
	@WebResult(name = "values")
	public abstract List<ValueSet> getValuesByAttributes(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "attributes") List<AttributeGroup> attributes) throws BackendException;


	@WebMethod
	@WebResult(name = "value")
	public Value getValueWithBinaryAttachmentById(@WebParam(name = "uuid") String uuid, @WebParam(name = "valueId") Long valueId) throws BackendException, IllegalRequestException;

	@WebMethod
	@WebResult(name = "version")
	public abstract String getVersion();

}