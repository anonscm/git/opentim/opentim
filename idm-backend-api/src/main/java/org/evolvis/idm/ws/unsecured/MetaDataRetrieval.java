package org.evolvis.idm.ws.unsecured;

import java.util.Calendar;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.Application;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;

@WebService(serviceName = "MetaDataRetrievalService", name = "MetaDataRetrievalServicePT", portName = "MetaDataRetrievalServicePort", targetNamespace = "http://idm.evolvis.org")
public interface MetaDataRetrieval {

	/**
	 * @see org.evolvis.idm.identity.account.service.AccountManagement#getAccountByUuid(String)
	 */
	@WebMethod
	@WebResult(name = "account")
	public abstract Account getAccountByUUID(
			@WebParam(name = "uuid") String uuid) throws BackendException;

	/**
	 * @see org.evolvis.idm.identity.account.service.AccountManagement#getAccountByUsername(String, String)
	 */
	@WebMethod
	@WebResult(name = "account")
	public abstract Account getAccountByUsername(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "username") String username)
			throws BackendException;

	@WebMethod
	@WebResult(name = "account")
	public abstract Account getAccountByCertificate(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "certificate") String certficate)
			throws BackendException;

	@WebMethod
	@WebResult(name = "certificate")
	public abstract String getCertificateByUUID(
			@WebParam(name = "uuid") String uuid) throws BackendException;

	/**
	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#getGroups(String)
	 */
	@WebMethod
	@WebResult(name = "groups")
	public abstract List<Group> getGroups(
			@WebParam(name = "clientName") String clientName)
			throws BackendException;

	/**
	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#getGroupByName(String, String)
	 */
	@WebMethod
	@WebResult(name = "group")
	public abstract Group getGroupByName(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "groupName") String groupName)
			throws BackendException;

	/**
	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#getGroupsByUser(java.lang.String, String)
	 */
	@WebMethod
	@WebResult(name = "userGroups")
	public abstract List<Group> getGroupsByUser(
			@WebParam(name = "uuid") String uuid) throws BackendException;

	/**
	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#isUserInGroup(String, java.lang.Long)
	 */
	@WebMethod
	@WebResult(name = "userInGroup")
	public abstract boolean isUserInGroup(
			@WebParam(name = "groupName") String groupName,
			@WebParam(name = "uuid") String uuid) throws BackendException;

	/**
	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#getUsersByGroup(java.lang.String, String)
	 */
	@WebMethod
	@WebResult(name = "users")
	public abstract List<Account> getUsersByGroup(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "groupName") String groupName)
			throws BackendException;

	/**
	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#getRoleByName(String, String, String)
	 */
	@WebMethod
	@WebResult(name = "roles")
	public abstract Role getRoleByName(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "applicationName") String applicationName,
			@WebParam(name = "roleName") String roleName)
			throws BackendException;

	/**
	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#getRolesByApplication(String, String)
	 */
	@WebMethod
	@WebResult(name = "roles")
	public abstract List<Role> getRolesByApplication(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "applicationName") String applicationName)
			throws BackendException;

	/**
	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#getRolesByGroup(String, String, String)
	 */
	@WebMethod
	@WebResult(name = "roles")
	public abstract List<Role> getRolesByGroup(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "groupName") String groupName)
			throws BackendException;

	/**
	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#getRolesByUserAndApplication(java.lang.String, java.lang.Long, String)
	 */
	@WebMethod
	@WebResult(name = "roles")
	public abstract List<Role> getRolesByUser(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "applicationName") String applicationName,
			@WebParam(name = "uuid") String uuid) throws BackendException;

	/**
	 * @see org.evolvis.idm.relation.permission.service.GroupAndRoleManagement#isUserInRoleById(java.lang.String, java.lang.Long, String)
	 */
	@WebMethod
	@WebResult(name = "hasUserRole")
	public abstract boolean hasUserRole(
			@WebParam(name = "applicationName") String applicationName,
			@WebParam(name = "uuid") String uuid,
			@WebParam(name = "roleName") String roleName)
			throws BackendException;

	@WebMethod
	@WebResult(name = "users")
	public abstract List<Account> getUsersByRole(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "applicationName") String applicationName,
			@WebParam(name = "roleName") String roleName)
			throws BackendException;

	@WebMethod
	@WebResult(name = "orgUnit")
	public abstract OrgUnit getOrgUnitByName(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "orgUnitName") String orgUnitName)
			throws BackendException;

	@WebMethod
	@WebResult(name = "orgUnits")
	public abstract List<OrgUnit> getOrgUnitsByUser(
			@WebParam(name = "uuid") String uuid) throws BackendException;

	@WebMethod
	@WebResult(name = "orgUnits")
	public abstract List<OrgUnit> getOrgUnitsByMetaDataValue(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "value") String value) throws BackendException;

	@WebMethod
	@WebResult(name = "orgUnits")
	public abstract List<OrgUnit> getOrganisationalUnits(
			@WebParam(name = "clientName") String clientName)
			throws BackendException;

	@WebMethod
	@WebResult(name = "users")
	public abstract List<Account> getUsersByOrgUnit(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "orgUnitName") String orgUnitName)
			throws BackendException;

	@WebMethod
	@WebResult(name = "users")
	public abstract List<Account> getUsersByOrgUnitAndRole(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "applicationName") String applicationName,
			@WebParam(name = "orgUnitName") String orgUnitName,
			@WebParam(name = "roleName") String roleName)
			throws BackendException;

	@WebMethod
	@WebResult(name = "users")
	public abstract List<Account> getDeactivatedUsersSince(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "sinceDate") Calendar sinceDate)
			throws BackendException;

	@WebMethod
	@WebResult(name = "isUserActive")
	public abstract boolean isUserActive(@WebParam(name = "uuid") String uuid)
			throws BackendException;

	@WebMethod
	@WebResult(name = "application")
	public abstract Application getApplicationByName(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "applicationName") String applicationName)
			throws BackendException;

	@WebMethod
	@WebResult(name = "version")
	public abstract String getVersion();

}