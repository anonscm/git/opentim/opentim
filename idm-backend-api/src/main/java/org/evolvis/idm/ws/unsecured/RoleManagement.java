package org.evolvis.idm.ws.unsecured;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.relation.permission.model.Role;

@WebService(serviceName = "RoleManagementService", name = "RoleManagementServicePT", portName = "RoleManagementServicePort", targetNamespace = "http://idm.evolvis.org")
public interface RoleManagement {

	@WebMethod
	public abstract void addGroupToRole(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "applicationName") String applicationName,
			@WebParam(name = "groupName") String groupName,
			@WebParam(name = "roleName") String roleName)
			throws BackendException;

	@WebMethod
	public abstract void addUserToRole(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "applicationName") String applicationName,
			@WebParam(name = "uuid") String uuid,
			@WebParam(name = "roleName") String roleName)
			throws BackendException;

	@WebMethod
	public abstract void removeGroupFromRole(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "applicationName") String applicationName,
			@WebParam(name = "groupName") String groupName,
			@WebParam(name = "roleName") String roleName)
			throws BackendException;

	@WebMethod
	public abstract void deleteRole(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "applicationName") String applicationName,
			@WebParam(name = "roleName") String roleName)
			throws BackendException;

	@WebMethod
	public abstract void removeUserFromRole(
			@WebParam(name = "applicationName") String applicationName,
			@WebParam(name = "uuid") String uuid,
			@WebParam(name = "roleName") String roleName)
			throws BackendException;

	@WebMethod
	@WebResult(name = "role")
	public abstract Role setRole(@WebParam(name = "role") Role role)
			throws BackendException;

	@WebMethod
	@WebResult(name = "version")
	public abstract String getVersion();

}