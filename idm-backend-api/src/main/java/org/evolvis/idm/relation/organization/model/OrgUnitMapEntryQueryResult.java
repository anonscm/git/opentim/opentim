/**
 * 
 */
package org.evolvis.idm.relation.organization.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.QueryResult;

/**
 * @author Yorka Neumann, tarent GmbH
 * 
 */
@XmlRootElement(name = "orgUnitMapEntryQueryResult", namespace = "http://idm.evolvis.org")
@XmlType(name = "OrgUnitMapEntryQueryResult", namespace = "http://idm.evolvis.org")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrgUnitMapEntryQueryResult extends QueryResult<OrgUnitMapEntry> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6374150816433187341L;

	private OrgUnit orgUnit;

	public OrgUnit getOrgUnit() {
		if (this.orgUnit != null) {
			orgUnit.setOrgUnitMap(this.getOrgUnitMap());
			return this.orgUnit;
		} else
			return null;
	}

	public void setOrgUnit(OrgUnit orgUnit) {
		this.orgUnit = orgUnit;
	}

	public OrgUnitMap getOrgUnitMap() {
		OrgUnitMap orgUnitMap = null;
		if (this.orgUnit != null && this.orgUnit.getOrgUnitMap() != null) {
			orgUnitMap = this.orgUnit.getOrgUnitMap();
		} else if (this.getResultList().size() > 0 && this.getResultList().get(0).getMap() != null) {
			orgUnitMap = this.getResultList().get(0).getMap();
		}
		if (orgUnitMap != null) {
			orgUnitMap.setMapEntries(this.getResultList());
		}
		return orgUnitMap;
	}
}
