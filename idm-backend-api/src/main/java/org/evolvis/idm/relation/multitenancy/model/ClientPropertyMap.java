/**
 * 
 */
package org.evolvis.idm.relation.multitenancy.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractMap;
import org.evolvis.idm.common.util.ConfigurationUtil;

/**
 * @author Jens Neumaier, tarent GmbH
 * @author Yorka Neumann, tarent GmbH
 *
 */

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "ClientPropertyMap", uniqueConstraints = @UniqueConstraint(columnNames = { "client_id" }))
@XmlType(name = "ClientPropertyMap", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "clientPropertyMap", namespace = "http://idm.evolvis.org")
public class ClientPropertyMap extends AbstractMap<ClientPropertyMapEntry> {
	
	/*
	 * Internal system data
	 */
	public static final String KEY_INTERNAL_LASTUPDATEUSERS = "internal.lastUpdateUsers";
	
	/*
	 * OpenSSO settings
	 */
	public static final String KEY_OPENSSO_SERVICEURL = "openSso.serviceUrl";
	public static final String KEY_OPENSSO_ADMINLOGIN = "openSso.adminLogin";
	public static final String KEY_OPENSSO_ADMINPASSWORD = "openSso.adminPassword";
	
	/*
	 * cronjobs settings
	 */
	public static final String KEY_SYSTEM_DAILYJOBEXECUTIONTIME = "system.dailyJobExecutionTime"; 
	public static final String KEY_SYSTEM_DEACTIVATEDACCOUNTLIFETIMEINDAYS = "system.deactivatedAccountLifetimeInDays";
	
	/*
	 * registration settings
	 */
	public static final String KEY_REGISTRATION_TERMSREQUIRED_ENABLED = "registration.termsRequired.enabled";
	public static final String KEY_REGISTRATION_TERMSREQUIRED_URL = "registration.termsRequired.url";
	public static final String KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED = "registration.displayNameGeneration.enabled";
	public static final String KEY_REGISTRATION_DISPLAYNAMEGENERATION_PATTERN = "registration.displayNameGeneration.pattern";
	public static final String KEY_REGISTRATION_ALTERNATIVELINK_ENABLED = "registration.alternativeLink.enabled";
	public static final String KEY_REGISTRATION_ALTERNATIVELINK_URL = "registration.alternativeLink.url";
	public static final String KEY_REGISTRATION_REGISTRATIONSUCCESSLINK_ENABLED = "registration.registrationSuccessLink.enabled";
	public static final String KEY_REGISTRATION_REGISTRATIONSUCCESSLINK_URL = "registration.registrationSuccessLink.url";
	public static final String KEY_REGISTRATION_ACTIVATIONSUCCESSLINK_ENABLED = "registration.activationSuccessLink.enabled";
	public static final String KEY_REGISTRATION_ACTIVATIONSUCCESSLINK_URL = "registration.activationSuccessLink.url";
	
	/*
	 * security settings
	 */
	public static final String KEY_SECURITY_PASSWORT_PATTERN = "security.password.pattern";
	public static final String KEY_SECURITY_ALLOWEMAILEXISTENCEHINTS_ENABLED = "security.allowEmailExistenceHints.enabled";
	
	/*
	 * LDAP synchronisation properties
	 */
	public static final String KEY_LDAP_SYNC_ENABLED = "ldapSync.enabled";
	public static final String KEY_LDAP_SYNC_HOSTANDPORT = "ldapSync.hostAndPort";
	public static final String KEY_LDAP_SYNC_USERDN = "ldapSync.userDN";
	public static final String KEY_LDAP_SYNC_PASSWORD = "ldapSync.password";
	public static final String KEY_LDAP_SYNC_BASEDN = "ldapSync.baseDN";
	public static final String KEY_LDAP_SYNC_SEARCHDN = "ldapSync.searchDN";
	public static final String KEY_LDAP_SYNC_SEARCHFILTER = "ldapSync.searchFilter";
	public static final String PLACEHOLDER_LOGINNAME_LDAP_SYNC_SEARCHFILTER = "LOGINNAME";
	
	/*
	 * Search settings
	 */
	public static final String KEY_SEARCH_PAGINGLIMIT = "search.pagingLimit";
	public static final String KEY_SEARCH_FULLPRIVATEDATASEARCH_ENABLED = "search.fullPrivateDataSearch.enabled";
	
	/*
	 * Display settings
	 */
	public static final String KEY_DISPLAY_PRIVATEDATAMANAGEMENT_COLUMNS = "display.privateDataManagement.columns";
	
	/*
	 * Other Settings
	 */
	public static final String KEY_OTHER_ALLOWDELETEUSER_ENABLED = "other.allowDeleteUser.enabled";
	
	private static int DEFAULT_CLIENT_PROPERTIES_SIZE;
	
	/*
	 * Default client properties static initialisation
	 */	
	public static ClientPropertyMap createDefaultClientProperties() {	
		ClientPropertyMap clientPropertyMap = new ClientPropertyMap();
		
		clientPropertyMap.put(KEY_OPENSSO_SERVICEURL, "http://localhost:8080/opensso");
		clientPropertyMap.put(KEY_OPENSSO_ADMINLOGIN, "amAdmin");
		clientPropertyMap.put(KEY_OPENSSO_ADMINPASSWORD, "");
		
		clientPropertyMap.put(KEY_SYSTEM_DAILYJOBEXECUTIONTIME, "02:00");
		clientPropertyMap.put(KEY_SYSTEM_DEACTIVATEDACCOUNTLIFETIMEINDAYS, "30");
		
		clientPropertyMap.put(KEY_REGISTRATION_DISPLAYNAMEGENERATION_ENABLED, "false");
		clientPropertyMap.put(KEY_REGISTRATION_DISPLAYNAMEGENERATION_PATTERN, "%firstname% %lastname%");
		clientPropertyMap.put(KEY_REGISTRATION_ALTERNATIVELINK_ENABLED, "false");
		clientPropertyMap.put(KEY_REGISTRATION_ALTERNATIVELINK_URL, "http://");
		clientPropertyMap.put(KEY_REGISTRATION_REGISTRATIONSUCCESSLINK_ENABLED, "false");
		clientPropertyMap.put(KEY_REGISTRATION_REGISTRATIONSUCCESSLINK_URL, "http://");
		clientPropertyMap.put(KEY_REGISTRATION_ACTIVATIONSUCCESSLINK_ENABLED, "false");
		clientPropertyMap.put(KEY_REGISTRATION_ACTIVATIONSUCCESSLINK_URL, "http://");
		clientPropertyMap.put(KEY_REGISTRATION_TERMSREQUIRED_ENABLED, "false");
		clientPropertyMap.put(KEY_REGISTRATION_TERMSREQUIRED_URL, "http://");

		clientPropertyMap.put(KEY_SECURITY_ALLOWEMAILEXISTENCEHINTS_ENABLED, "false");
		clientPropertyMap.put(KEY_SECURITY_PASSWORT_PATTERN, "^(?=.{8,74})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$");

		clientPropertyMap.put(KEY_LDAP_SYNC_ENABLED, "false");
		clientPropertyMap.put(KEY_LDAP_SYNC_HOSTANDPORT, "windows-ad.qs.tarent.de:389");
		clientPropertyMap.put(KEY_LDAP_SYNC_BASEDN, "dc=qs,dc=tarent,dc=de");
		clientPropertyMap.put(KEY_LDAP_SYNC_USERDN, "CN=Administrator,CN=Users,dc=qs,dc=tarent,dc=de");
		clientPropertyMap.put(KEY_LDAP_SYNC_PASSWORD, "");
		clientPropertyMap.put(KEY_LDAP_SYNC_SEARCHDN, "cn=Users");
		clientPropertyMap.put(KEY_LDAP_SYNC_SEARCHFILTER, "(&(objectClass=person)(cn=LOGINNAME))");
		
		clientPropertyMap.put(KEY_SEARCH_PAGINGLIMIT, "10");
		clientPropertyMap.put(KEY_SEARCH_FULLPRIVATEDATASEARCH_ENABLED, "true");
		
		clientPropertyMap.put(KEY_DISPLAY_PRIVATEDATAMANAGEMENT_COLUMNS, "4");
		
		return clientPropertyMap;
	}
	
	public static int getDefaultlientPropertiesSize() {
		if (DEFAULT_CLIENT_PROPERTIES_SIZE == 0) {
			DEFAULT_CLIENT_PROPERTIES_SIZE = createDefaultClientProperties().size();
		}
		return DEFAULT_CLIENT_PROPERTIES_SIZE;
	}

	@OneToOne(optional = false)
	private Client client;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "map", fetch = FetchType.EAGER)
	@OrderBy("key ASC")
	private List<ClientPropertyMapEntry> mapEntries = new ArrayList<ClientPropertyMapEntry>();
	
	@Override
	protected ClientPropertyMapEntry createMapEntryInstance(String key, String value) {
		return new ClientPropertyMapEntry(key, value, this);
	}

	@Override
	public List<ClientPropertyMapEntry> getMapEntries() {
		if (this.mapEntries == null)
			mapEntries = new LinkedList<ClientPropertyMapEntry>();
		return this.mapEntries;
	}

	@Override
	public void setMapEntries(List<ClientPropertyMapEntry> mapEntries) {
		this.mapEntries = mapEntries;
	}

	@Override
	@Transient
	public String getClientName() {
		if (this.getClient() != null)
			return this.getClient().getName();
		return null;
	}

	
	/**
	 * Sets the client that this property map should be assigned to.
	 * @param client the client.
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * Returns the client that this property map is assigned to.
	 * 
	 * @return the client that this property map is assigned to or <code>null</code> if it is not assigned.
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * Fills clientPropertyMap with missing default entries and return true if values have been added
	 * 
	 * @param clientPropertyMap
	 * @return true if values have been added
	 */
	public static boolean fillMissingDefaults(ClientPropertyMap clientPropertyMap) {
		if (clientPropertyMap.size() == getDefaultlientPropertiesSize())
			return false;
		else {
			boolean mapHasBeenUpdated = false;
			ClientPropertyMap defaults = createDefaultClientProperties();

			for (ClientPropertyMapEntry defaultEntry : defaults.getMapEntries()) {
				if (!clientPropertyMap.containsKey(defaultEntry.getKey())) {
					clientPropertyMap.put(defaultEntry.getKey(), defaultEntry.getValue());
					mapHasBeenUpdated = true;
				}
			}

			return mapHasBeenUpdated;
		}
	}
	
	/**
	 * Puts all key/value pairs of an existing ClientPropertyMap into this instance as a copy.
	 * Leaves existing IDs of map entries already present in this instance unchanged.
	 * 
	 * @param clientPropertyMap
	 */
	@Transient
	public void putAll(ClientPropertyMap clientPropertyMap) {
		for (ClientPropertyMapEntry entry : clientPropertyMap.getMapEntries()) {
			this.put(entry.getKey(), entry.getValue());
		}
	}
	
	@Transient
	public ClientPropertyMap clone() {
		ClientPropertyMap clientPropertyMap = new ClientPropertyMap();
		clientPropertyMap.setClient(this.getClient());
		clientPropertyMap.setId(this.getId());
		// create new map entry list to get a new list reference and new entry references
		List<ClientPropertyMapEntry> entries = new ArrayList<ClientPropertyMapEntry>();
		for (ClientPropertyMapEntry currentEntry : this.getMapEntries()) {
			ClientPropertyMapEntry entryClone = clientPropertyMap.createMapEntryInstance(currentEntry.getKey(), currentEntry.getValue());
			entryClone.setId(currentEntry.getId());
			entries.add(entryClone);
		}
		clientPropertyMap.setMapEntries(entries);
		return clientPropertyMap;
	}
}
