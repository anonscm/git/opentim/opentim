package org.evolvis.idm.relation.permission.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.identity.account.model.Account;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "InternalRoleAssignment", uniqueConstraints = { @UniqueConstraint(columnNames = { "account_id", "type" }) })
@XmlType(name = "InternalRoleAssignment", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "internalRoleAssignment", namespace = "http://idm.evolvis.org")
public class InternalRoleAssignment extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7951180373342847805L;

	@Enumerated
	private InternalRoleType type;

	@ManyToOne
	private Account account;

	@SuppressWarnings("unused")
	@OneToMany(mappedBy = "internalRoleAssignment", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<InternalRoleScope> internalRoleScopes;

	public InternalRoleAssignment(Account account, InternalRoleType type) {
		this.account = account;
		this.type = type;
	}

	public InternalRoleAssignment() {
		// nothing
	}

	public InternalRoleType getType() {
		return type;
	}

	public void setType(InternalRoleType type) {
		this.type = type;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Override
	@Transient
	public String getClientName() {
		if (this.getAccount() != null)
			return this.getAccount().getClientName();
		return null;
	}
}
