package org.evolvis.idm.relation.multitenancy.model;

public interface ClientPropertyFields {
	public static final String FIELD_PATH_NAME = "name";
	public static final String FIELD_PATH_DISPLAYNAME = "displayName";
}
