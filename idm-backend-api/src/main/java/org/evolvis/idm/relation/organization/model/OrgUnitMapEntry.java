package org.evolvis.idm.relation.organization.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractMapEntry;
import org.evolvis.idm.common.util.ConfigurationUtil;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "OrgUnitMapEntry", uniqueConstraints = @UniqueConstraint(columnNames = {"key","map_id"}))
@XmlType(name = "OrgUnitMapEntry", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "orgUnitMapEntry", namespace = "http://idm.evolvis.org")
public class OrgUnitMapEntry extends AbstractMapEntry<OrgUnitMap> {

	private static final long serialVersionUID = 8979677541445063298L;

	
	public OrgUnitMapEntry(){
		//nothing
	}
	
	public OrgUnitMapEntry(String key, String value){
		super(key, value);
	}
	
	public OrgUnitMapEntry(String key, String value, OrgUnitMap map){
		super(key,value, map);
	}

	@Override
	@Size(min = 1, max = 100)
	@Pattern(regexp = "[\\p{L}\\-+#_.:,\"'`´0-9\\(\\)\\[\\]{}\\$&§\\\\/<> ]*")
	public String getKey() {
		return super.getKey();
	}

	@Override
	@Size(max = 100)
	@Pattern(regexp = "[\\p{L}\\-+#_.:,\"'`´0-9\\(\\)\\[\\]{}\\$&§\\\\/<> ]*")
	public String getValue() {
		return super.getValue();
	}
	
}
