package org.evolvis.idm.relation.multitenancy.model;

public interface ApplicationPropertyFields{

	public static final String FIELD_PATH_NAME = "name";
	public static final String FIELD_PATH_CLIENT_PREFIX = "client.";
	public static final String FIELD_PATH_DISPLAYNAME = "displayName";
	
}
