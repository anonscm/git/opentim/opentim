package org.evolvis.idm.relation.permission.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.identity.account.model.Account;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "RoleAssignment", uniqueConstraints = { @UniqueConstraint(columnNames = { "account_id", "role_id", "scope", "group_id" }) })
@XmlType(name = "RoleAssignment", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "roleAssignment", namespace = "http://idm.evolvis.org")
public class RoleAssignment extends UserRole {

	@ManyToOne(optional = true)
	private Account account;

	@ManyToOne(optional = true)
	private Group group;

	@NotNull
	private String scope;

	/* constructors */

	public RoleAssignment() {
		// Do nothing
	}

	public RoleAssignment(Role role, Account account, Group group, String scope) {
		this.setRole(role);
		this.setGroup(group);
		this.setAccount(account);
		this.setScope(scope);
	}

	/* end of constructors */

	/* getter-methods */
	public Account getAccount() {
		return account;
	}

	public Group getGroup() {
		return group;
	}

	public String getScope() {
		return scope;
	}

	/* setter-methods */

	public void setAccount(Account account) {
		this.account = account;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	@Override
	public boolean isDirectAssignment() {
		return account != null;
	}

	@Override
	@Transient
	public String getClientName() {
		if (this.getGroup() != null && this.getGroup().getClientName() != null)
			return this.getGroup().getClientName();
		if (this.getAccount() != null && this.getAccount().getClientName() != null)
			return this.getAccount().getClientName();
		return super.getClientName();
	}
}
