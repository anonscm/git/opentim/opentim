package org.evolvis.idm.relation.organization.model;

public interface OrgUnitSearchableFields {
	public static final String FIELD_PATH_GROUP_PREFIX = "group.";
	public static final String FIELD_PATH_NAME = "name";
	public static final String FIELD_PATH_DISPLAYNAME = "displayName";
}
