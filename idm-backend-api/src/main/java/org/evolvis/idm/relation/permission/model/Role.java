package org.evolvis.idm.relation.permission.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.relation.multitenancy.model.Application;


@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "Role", uniqueConstraints = @UniqueConstraint(columnNames={"name","application_id"}))
@XmlType(name = "Role", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "role", namespace = "http://idm.evolvis.org")
//TODO remove or restore @XmlType(propOrder = {"name","displayName","application"})
public class Role extends AbstractEntity {
	protected static ThreadLocal<Boolean> xmlTransientApplication = new ThreadLocal<Boolean>();
	


	@NotNull
	@Size(min = 1, max = 50, message="violation.role.size")
	@Pattern(regexp = "[a-zA-Z0-9._-]*", message="violation.role.pattern")
	@Column(nullable = false, length = 50)
	private String name;

	@NotNull
	@Size(min = 1, max = 50, message="violation.role.size")
	@Pattern(regexp = DisplayName.PATTERN, message="violation.role.pattern")
	@Column(nullable = false, length = 50)
	private String displayName;
	
	@Transient
	private boolean xmlTransientApplicationFlag = true;
	
	@ManyToOne(optional = false)
	private Application application;
	
	@SuppressWarnings("unused")
	@OneToMany(mappedBy = "role", cascade = CascadeType.REMOVE,fetch = FetchType.LAZY)
	private List<RoleAssignment> roleAssignments;

	public Role() {			
	}

	public Role(String name) {
		this.setName(name);
	}

	public Role(String name, Application application) {
		this(name);
		this.setApplication(application);
	}

	public Role(String name, Application application, String displayName){
		this(name,application);
		this.setDisplayName(displayName);
	}
	
	public Role(Long id, String name, Application application, String displayName){
		this(name, application, displayName);
		this.setId(id);
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Application getApplication() {
		if(xmlTransientApplicationFlag)
			return null;
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
		this.xmlTransientApplicationFlag = false;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

	
	@XmlTransient
	public String getLabel() {
		return displayName + " (" + name + ")";
	}

	@Override
	@Transient
	public String getClientName() {
		if(this.getApplication() != null)
			return this.getApplication().getClientName();
		return null;
	}
}
