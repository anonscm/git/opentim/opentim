package org.evolvis.idm.relation.multitenancy.service;


import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientQueryResult;

@WebService(serviceName = "ClientRetrievalService", name = "ClientRetrievalServicePT", portName = "ClientRetrievalServicePort")
public interface ClientRetrieval extends AbstractBaseService {

	@WebMethod
	@WebResult(name = "clients")
	public ClientQueryResult getClients(@WebParam(name = "queryDescriptor")QueryDescriptor queryDescriptor) throws BackendException, AccessDeniedException, IllegalRequestException;

//	@WebMethod
//	@WebResult(name = "client")
//	public Client getClientById(@WebParam(name = "id") Long id) throws BackendException, AccessDeniedException, IllegalRequestException;
	
	@WebMethod
	@WebResult(name = "client")
	public Client getClientByName(@WebParam(name = "clientName") String clientName) throws BackendException, IllegalRequestException;
}
