package org.evolvis.idm.relation.permission.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "internalRoleAssignment_id", "group_id" }) }, name = ConfigurationUtil.DB_TABLE_PREFIX + "InternalRoleScope")
@XmlType(name = "InternalRoleScope", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "internalRoleScope", namespace = "http://idm.evolvis.org")
public class InternalRoleScope extends AbstractEntity {


	@ManyToOne(optional = false)
	private InternalRoleAssignment internalRoleAssignment;

	@ManyToOne(optional = false)
	private Group group;




	public InternalRoleAssignment getInternalRoleAssignment() {
		return internalRoleAssignment;
	}


	public void setInternalRoleAssignment(InternalRoleAssignment internalRoleAssignment) {
		this.internalRoleAssignment = internalRoleAssignment;
	}


	public Group getGroup() {
		return group;
	}


	public void setGroup(Group group) {
		this.group = group;
	}

	@Override
	@Transient
	public String getClientName() {
		if(this.getGroup() != null && this.getGroup().getClientName() != null)
			return this.getGroup().getClientName();
		if(this.getInternalRoleAssignment() != null)
			return this.getInternalRoleAssignment().getClientName();
		return null;
	}
}
