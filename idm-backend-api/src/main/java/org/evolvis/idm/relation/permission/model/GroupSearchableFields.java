package org.evolvis.idm.relation.permission.model;

public interface GroupSearchableFields {
	public static final String FIELD_PATH_ID = "id";
	public static final String FIELD_PATH_NAME = "name";
	public static final String FIELD_PATH_CLIENT_PREFIX = "client";
	public static final String FIELD_PATH_DISPLAYNAME = "displayName";
}
