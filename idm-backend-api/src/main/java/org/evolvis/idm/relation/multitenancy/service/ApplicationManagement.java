package org.evolvis.idm.relation.multitenancy.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.relation.multitenancy.model.Application;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "ApplicationManagementService", name = "ApplicationManagementServicePT", portName = "ApplicationManagementServicePort")
public interface ApplicationManagement extends AbstractBaseService, ApplicationRetrieval {
	/**
	 * This method save an application for the current client. If the Id
	 * attribute null, the application will be created. Otherwise an update
	 * will be executed. 
	 * 
	 * @param application The application which will be saved.
	 * @return The updated application.
	 */
	@WebMethod
	@WebResult(name = "application")
	public Application setApplication(@WebParam(name = "clientName") String clientName, @WebParam(name = "application") Application application) throws BackendException, IllegalRequestException, AccessDeniedException;

	/**
	 * This method delete an application for the current client.
	 * @param clientName the unique name of the client
	 * @param application The application which will be deleted.
	 * 
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public void deleteApplication(@WebParam(name = "clientName")String clientName, @WebParam(name = "application") Application application) throws BackendException, AccessDeniedException, IllegalRequestException;
}
