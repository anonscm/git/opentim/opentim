package org.evolvis.idm.relation.permission.model;

public interface InternalRoleScopePropertyFields {
	
	public static final String FIELD_PATH_GROUP_PREFIX = "group.";
	public static final String FIELD_PATH_INTERNALROLEASSIGNMENT_PREFIX = "internalRoleManagement.";

}
