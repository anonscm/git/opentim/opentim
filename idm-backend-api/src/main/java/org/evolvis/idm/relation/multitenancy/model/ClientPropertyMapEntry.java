/**
 * 
 */
package org.evolvis.idm.relation.multitenancy.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractMapEntry;
import org.evolvis.idm.common.util.ConfigurationUtil;

/**
 * @author Jens Neumaier, tarent GmbH
 * @author Yorka Neumann, tarent GmbH
 *
 */
@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "ClientPropertyMapEntry", uniqueConstraints = @UniqueConstraint(columnNames = {"key","map_id"}))
@XmlType(name = "ClientPropertyMapEntry", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "clientPropertyMapEntry", namespace = "http://idm.evolvis.org")
public class ClientPropertyMapEntry extends AbstractMapEntry<ClientPropertyMap> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1292570643508032009L;

	
	public ClientPropertyMapEntry(){
		// nothing
	}
	
	public ClientPropertyMapEntry(String key, String value, ClientPropertyMap map){
		super(key, value, map);
	}
}
