package org.evolvis.idm.relation.permission.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.identity.account.model.Account;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "GroupAssignment", uniqueConstraints = @UniqueConstraint(columnNames = { "account_id", "group_id" }))
@XmlType(name = "GroupAssignment", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "groupAssignment", namespace = "http://idm.evolvis.org")
public class GroupAssignment extends AbstractEntity {

	@ManyToOne(optional = false)
	private Group group;

	@ManyToOne(optional = false)
	private Account account;

	public GroupAssignment() {
		// nothing
	}

	public GroupAssignment(Account account, Group group) {
		this.account = account;
		this.group = group;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Override
	@Transient
	public String getClientName() {
		if (this.getGroup() != null && this.getGroup().getClientName() != null)
			return this.getGroup().getClientName();
		if (this.getAccount() != null)
			return this.getAccount().getClientName();
		return null;
	}
}
