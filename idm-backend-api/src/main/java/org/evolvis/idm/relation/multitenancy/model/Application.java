package org.evolvis.idm.relation.multitenancy.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.identity.applicationdata.model.AppMetaData;
import org.evolvis.idm.identity.privatedata.model.Approval;
import org.evolvis.idm.relation.permission.model.Role;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "name", "client_id" }) }, name = ConfigurationUtil.DB_TABLE_PREFIX + "Application")
@XmlType(name = "Application", namespace = "http://idm.evolvis.org"/*, propOrder = { "name", "displayName", "certificationData","client"}*/)
@XmlRootElement(name = "application", namespace = "http://idm.evolvis.org")
public class Application extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = -5973888499280532046L;



	@NotNull
	@Size(min = 1, max = 50, message= "violation.application.size")
	@Pattern(regexp = "[a-zA-Z0-9._-]*", message= "violation.application.pattern")
	@Column(nullable = false, length = 50)
	private String name;

	@NotNull
	@Size(min = 1, max = 50, message= "violation.application.size")
	@Pattern(regexp = DisplayName.PATTERN, message= "violation.application.pattern")
	@Column(nullable = false, length = 50)
	private String displayName;
	private String certificationData;

	@NotNull
	@ManyToOne(optional = false)
	private Client client;

	// used only to cascade on delete
	@SuppressWarnings("unused")
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "application")
	private List<Approval> approvals;

	@SuppressWarnings("unused")
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "application")
	@OrderBy("displayName ASC")
	private List<Role> roles;

	@SuppressWarnings("unused")
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "application")
	private List<AppMetaData> applicationMetaData;

	public Application() {
		// nothing
	}

	public Application(String name, String displayName) {
		setName(name);
		setDisplayName(displayName);
	}

	public Application(String name, String displayName, Client client) {
		this(name, displayName);
		setClient(client);
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	@XmlTransient
	public String getLabel() {
		return displayName + " (" + name + ")";
	}

	public String getCertificationData() {
		return certificationData;
	}

	public void setCertificationData(String certificationData) {
		this.certificationData = certificationData;
	}

	//@XmlTransient
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
	public String getClientName(){
		if(this.getClient() != null)
			return this.getClient().getClientName();
		return null;
	}
}
