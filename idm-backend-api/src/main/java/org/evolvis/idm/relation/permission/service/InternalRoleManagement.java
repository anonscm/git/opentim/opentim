package org.evolvis.idm.relation.permission.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.permission.model.InternalRoleType;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "InternalRoleManagementService", name = "InternalRoleManagementServicePT", portName = "InternalRoleManagementServicePort")
public interface InternalRoleManagement extends InternalRoleScopeManagement {

	/**
	 * assigns a user account to a given internal role
	 * 
	 * @param uuid
	 * @param internalRole
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	public void addUserToInternalRole(@WebParam(name = "uuid") String uuid, @WebParam(name = "internalRole") InternalRoleType internalRole) throws BackendException, IllegalRequestException;


	/**
	 * Returns the internal role type that is assigned to a given user. If the user has been assigned to more than one internal role then the internal role type with the highest level will be returned. If there is no assignment stored the internal role
	 * type of the user is "USER".
	 * 
	 * @param uuid the GUId of the user (account)
	 * @return the (highest level) internal role of the user.
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	@WebMethod
	@WebResult(name = "internalRole")
	public InternalRoleType getInternalRoleForUser(@WebParam(name = "uuid") String uuid) throws IllegalRequestException, BackendException;


	/**
	 * Returns the all internal role types that has been assigned to the user.
	 * 
	 * @param uuid the GUId of the user (account)
	 * @return
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	@WebMethod
	@WebResult(name = "internalRoles")
	public List<InternalRoleType> getInternalRolesOfUser(@WebParam(name = "uuid") String uuid) throws IllegalRequestException, BackendException;


	/**
	 * Returns all user accounts which has been assigned to the given internal role type.
	 * @param clientName the unique name of the client
	 * @param internalRole
	 * 
	 * @return
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	@WebMethod
	@WebResult(name = "accounts")
	public List<Account> getUsersOfInternalRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "internalRole") InternalRoleType internalRole) throws IllegalRequestException, BackendException;


	/**
	 * removes a given user account from a specific internal role
	 * 
	 * @param uuid
	 * @param internalRole
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	public void removeUserFromInternalRole(@WebParam(name = "uuid") String uuid, @WebParam(name = "internalRole") InternalRoleType internalRole) throws BackendException, IllegalRequestException;

}
