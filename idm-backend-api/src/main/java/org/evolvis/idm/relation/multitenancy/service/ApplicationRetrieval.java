package org.evolvis.idm.relation.multitenancy.service;


import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.relation.multitenancy.model.Application;

@WebService(serviceName = "ApplicationRetrievalService", name = "ApplicationRetrievalServicePT", portName = "ApplicationRetrievalServicePort")
public interface ApplicationRetrieval extends AbstractBaseService {
	/**
	 * Returns a list of applications of a certain client. That may be all applications or a sub list of that matches the constraints given by the query descriptor.
	 * @param clientName the unique name of the client.
	 * @param queryDescriptor descriptor that contains information to configure the requested query.
	 * @return a list of applications of a certain client.
	 * @throws IllegalRequestException TODO
	 * @throws BackendException TODO
	 */
	@WebMethod
	@WebResult(name = "applications")
	public QueryResult<Application> getApplications(@WebParam(name = "clientName") String clientName, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, AccessDeniedException, IllegalRequestException;

	/**
	 * Returns the application with the given <code>id</code> within the current client.
	 * @param clientName the unique name of the client.
	 * @param applicationId the id of the application
 	 * @return the application of the given client with the given id or <code>null</code> if no such exists.
	 * @throws IllegalRequestException TODO
	 * @throws BackendException TODO
	 */
	@WebMethod
	@WebResult(name = "application")
	public Application getApplicationById(@WebParam(name = "clientName")String clientName, @WebParam(name = "applicationId") Long applicationId) throws BackendException, AccessDeniedException, IllegalRequestException;
	
	/**
	 * Returns the application of a given client that has the given name.
	 * @param clientName the unique name of the client.
	 * @param applicationName the name of the application.
	 * @return the application of the given client that has the given name or <code>null</code> if no such exists.
	 * @throws BackendException TODO
	 * @throws AccessDeniedException TODO
	 * @throws IllegalRequestException TODO 
	 */
	@WebMethod
	@WebResult(name = "application")
	public Application getApplicationByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName) throws BackendException, AccessDeniedException, IllegalRequestException;

	/**
	 * Returns the application with a given name and is associated to the same client as the given user account.
	 * @param uuid the uuid of the user account.
	 * @param applicationName the name of the application.
	 * @return the application with the given name or <code>null</code> if no such exists.
	 * @throws IllegalRequestException TODO 
	 * @throws BackendException TODO 
	 */
	@WebMethod
	@WebResult(name = "application")
	public Application getApplicationByUuid(@WebParam(name = "uuid") String uuid, @WebParam(name = "applicationName") String applicationName) throws IllegalRequestException, BackendException;
}
