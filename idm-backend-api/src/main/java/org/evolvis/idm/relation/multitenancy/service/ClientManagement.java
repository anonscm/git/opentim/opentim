package org.evolvis.idm.relation.multitenancy.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "ClientManagementService", name = "ClientManagementServicePT", portName = "ClientManagementServicePort")
public interface ClientManagement extends AbstractBaseService, ClientRetrieval {
	@WebMethod
	@WebResult(name = "client")
	public Client setClient(@WebParam(name = "entityManagerId") int entityManagerId, @WebParam(name = "client") Client client) throws BackendException, IllegalRequestException, AccessDeniedException;

	@WebMethod
	public void deleteClient(@WebParam(name = "client") Client client) throws BackendException, AccessDeniedException, IllegalRequestException;
	
	@WebMethod
	@WebResult(name = "clientPropertyMap")
	public ClientPropertyMap setClientPropertyMap(@WebParam(name = "clientName") String clientName, @WebParam(name = "clientPropertyMap") ClientPropertyMap clientPropertyMap) throws BackendException, IllegalRequestException, AccessDeniedException;
	
	@WebMethod
	@WebResult(name = "clientPropertyMap")
	public ClientPropertyMap getClientPropertyMap(@WebParam(name = "clientName") String clientName) throws BackendException, IllegalRequestException, AccessDeniedException;

	@WebMethod(exclude = true)
	public void updateLastChangeUserList(@WebParam(name = "clientName") String clientName) throws BackendException;
	
	@WebMethod
	@WebResult(name = "lastChangeMillis")
	public Long getLastUserListChangeTimeMillis(@WebParam(name = "clientName") String clientName) throws BackendException, IllegalRequestException;
}
