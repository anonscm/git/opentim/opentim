package org.evolvis.idm.relation.organization.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.common.validation.AbstractMapPattern;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.relation.permission.model.Group;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "OrgUnit")
@XmlRootElement(name = "orgUnit", namespace = "http://idm.evolvis.org")
@XmlType(name = "OrgUnit", namespace="http://idm.evolvis.org")
public class OrgUnit extends AbstractEntity {

	@ManyToOne(optional = false)
	private Group group;

	@OneToOne(mappedBy = "orgUnit", cascade = CascadeType.ALL)
	private OrgUnitMap orgUnitMap;

	@NotNull
	@Size(min = 1, max = 50, message = "violation.orgUnit.size")
	@Pattern(regexp = "[a-zA-Z0-9._-]*", message = "violation.orgUnit.pattern")
	@Column(nullable = false, length = 50)
	private String name;

	@NotNull
	@Size(min = 1, max = 50, message = "violation.orgUnit.size")
	@Pattern(regexp = DisplayName.PATTERN, message = "violation.orgUnit.pattern")
	private String displayName;

	public OrgUnit() {
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	@AbstractMapPattern(keyPattern = "[\\p{L}\\-+#_.:,\"'`´0-9\\(\\)\\[\\]{}\\$&§\\\\/<> ]{1,100}",
			valuePattern = "[\\p{L}\\-+#_.:,\"'`´0-9\\(\\)\\[\\]{}\\$&§\\\\/<> ]{0,100}")
	public OrgUnitMap getOrgUnitMap() {
		if (this.orgUnitMap == null) {
			orgUnitMap = new OrgUnitMap();
			orgUnitMap.setOrgUnit(this);
		}
		return orgUnitMap;
	}

	public void setOrgUnitMap(OrgUnitMap orgUnitMap) {
		this.orgUnitMap = orgUnitMap;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@XmlTransient
	public String getLabel() {
		return displayName + " (" + name + ")";
	}

	public String toString() {
		return super.toString() + " id: " + this.getId() + ", name: " + this.getName() + " OrgUnitMap :" + this.getOrgUnitMap().toString();
	}

	@Transient
	public String getClientName() {
		if (this.getGroup() != null) {
			return this.getGroup().getClientName();
		}
		return null;
	}

}
