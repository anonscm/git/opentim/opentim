package org.evolvis.idm.relation.permission.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.organization.model.OrgUnit;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "Group", uniqueConstraints = @UniqueConstraint(columnNames = { "client_id", "name" }))
//TODO remove or restore :@XmlRootElement(name = "group") @XmlType(propOrder = { "name", "displayName", "client", "writeProtected" })
@XmlType(name = "Group", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "group", namespace = "http://idm.evolvis.org")
public class Group extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = -5791993560461046857L;

	@NotNull
	@Size(min = 1, max = 50, message = "violation.group.size")
	@Pattern(regexp = "[a-zA-Z0-9._-]*", message = "violation.group.pattern")
	@Column(nullable = false, length = 50)
	private String name;

	@NotNull
	@ManyToOne(optional = false)
	private Client client;

	@Transient
	private boolean clientFlag = false;

	private boolean writeProtected;

	@NotNull
	@Size(min = 1, max = 50, message = "violation.group.size")
	@Pattern(regexp = DisplayName.PATTERN, message = "violation.group.pattern")
	@Column(nullable = false, length = 50)
	private String displayName;

	@SuppressWarnings("unused")
	@OneToMany(mappedBy = "group", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	private List<GroupAssignment> groupAssignments;

	@SuppressWarnings("unused")
	@OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<RoleAssignment> roleAssignments;

	@SuppressWarnings("unused")
	@OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@OrderBy("displayName ASC")
	private List<OrgUnit> orgUnits;

	@SuppressWarnings("unused")
	@OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private List<InternalRoleScope> internalRoleScopes;


	/* Constructors */
	public Group() {
	}


	public Group(Group group){
		this(group.getName(), group.isWriteProtected(), group.client, group.getDisplayName());
		this.setId(group.getId());
		
	}
	
	public Group(String name, boolean writeProtected, Client client) {
		this.setName(name);
		this.setClient(client);
		this.setWriteProtected(writeProtected);
	}


	public Group(String name, boolean writeProtected, Client client, String description) {
		this(name, writeProtected, client);
		this.setDisplayName(description);
	}

	



	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Client getClient() {
		if (!clientFlag)
			return null;
		return client;
	}


	public void setClient(Client client) {
		this.client = client;
		this.clientFlag = true;
	}


	public boolean isWriteProtected() {
		return writeProtected;
	}


	public void setWriteProtected(boolean writeProtected) {
		this.writeProtected = writeProtected;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public String getDisplayName() {
		return displayName;
	}

	
	@XmlTransient
	public String getLabel() {
		return displayName + " (" + name + ")";
	}

	
	@XmlTransient
	public String getBreakableLabel() {
		return makeStringBreakable(getLabel());
	}


	@Override
	@Transient
	public String getClientName() {
		if(this.getClient() != null)
			return this.getClient().getClientName();
		return null;
	}
}
