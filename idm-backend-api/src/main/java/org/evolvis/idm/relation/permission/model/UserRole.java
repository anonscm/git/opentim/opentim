package org.evolvis.idm.relation.permission.model;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;

@MappedSuperclass
@XmlType(name = "UserRole", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "userRole", namespace = "http://idm.evolvis.org")
public class UserRole extends AbstractEntity {

	@ManyToOne(optional = false)
	private Role role;

	@Transient
	private boolean directAssignment;

	public UserRole() {
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public boolean isDirectAssignment() {
		return directAssignment;
	}

	public void setDirectAssignment(boolean directAssignment) {
		this.directAssignment = directAssignment;
	}

	@Override
	@Transient
	public String getClientName() {
		if (this.getRole() != null)
			return this.getRole().getClientName();
		return null;
	}
}
