package org.evolvis.idm.relation.permission.model;

public class RoleModelConfigurator {
	public static void setXmlTransientApplication(boolean xmlTransientApplication) {		
		Role.xmlTransientApplication.set(xmlTransientApplication);
	}
	
	public static boolean getXmlTransientApplication() {
		return Role.xmlTransientApplication.get() == null ? false : Role.xmlTransientApplication.get();
	}
}
