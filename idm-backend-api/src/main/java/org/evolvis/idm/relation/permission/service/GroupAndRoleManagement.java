package org.evolvis.idm.relation.permission.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.account.model.User;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.Role;
import org.evolvis.idm.relation.permission.model.RoleUser;
import org.evolvis.idm.relation.permission.model.UserRole;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "GroupAndRoleManagementService", name = "GroupAndRoleManagementServicePT", portName = "GroupAndRoleManagementServicePort")
public interface GroupAndRoleManagement extends AbstractBaseService {

	/**
	 * Assigns a given group to a given role.
	 * 
	 * @param clientName the unique name of the client.
	 * @param groupId the id of the group that is to be assigned to the role.
	 * @param roleId the id of the role.
	 * @param scope the scope-variable.
	 * 
	 * @throws BackendException TODO
	 */
	@WebMethod
	public void addGroupToRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException;

	/**
	 * Assigns a given user account to a given group.
	 * 
	 * @param uuid the account that is to be assigned to the group.
	 * @param groupId the group.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public void addUserToGroup(@WebParam(name = "uuid") String uuid, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException;

	/**
	 * Assigns a given user account to a given role.
	 * 
	 * @param uuid the id of the user account that is to be assigned to the role.
	 * @param roleId the role.
	 * @param scope the scope-variable.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public void addUserToRole(@WebParam(name = "uuid") String uuid, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException;

	/**
	 * Deletes a given group from the persistence context.
	 * 
	 * @param clientName the unique name of the client
	 * @param groupId
	 * 
	 * @throws BackendException
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public void deleteGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException;

	/**
	 * Removes the assignment of a given group to a given role.
	 * 
	 * @param clientName the unique name of the client.
	 * @param groupId the id that identifies the specific group.
	 * @param roleId the id that identifies the specific role.
	 * @param scope the scope variable.
	 * 
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public void deleteGroupFromRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException,
			IllegalRequestException;

	/**
	 * Removes a persistent role from the persistent context that can be identified by the given id
	 * 
	 * @param clientName the unique name of the client
	 * @param roleId the id of the role.
	 * 
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public void deleteRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "roleId") Long roleId) throws BackendException, IllegalRequestException;

	/**
	 * Removes an user (account) from the given group.
	 * 
	 * @param uuid the unique attribute uuid of Account to identify the instance that is to be removed from the group.
	 * @param group the group from which the account is to be removed.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public void deleteUserFromGroup(@WebParam(name = "uuid") String uuid, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException;

	/**
	 * Removes an user (account) from the given role belonging to a certain community.
	 * 
	 * @param uuid the unique attribute uuid of Account to identify the instance that is to be removed from the role.
	 * @param roleId the id of the role from which the account is to be removed.
	 * @param scope the community scope.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public void deleteUserFromRole(@WebParam(name = "uuid") String uuid, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException;

	/**
	 * Returns an instance of the client that the given group is assigned to.
	 * 
	 * @param groupId the id of the group.
	 * @return the assigned client.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "clientOfGroup")
	public Client getClientByGroup(@WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException;

	/**
	 * Returns an instance of group that has the given id if it is already persistent.
	 * 
	 * @param clientName the unique name of the client
	 * @param groupId the id of the group that is to be found in the persistent context.
	 * 
	 * @return instance of class Group representing the wanted entity or <code>null</code> if it is not persistent.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "group")
	public Group getGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException;

	/**
	 * Returns a group that exists for a given client with a given group name.
	 * 
	 * @param clientName the unique name of the client
	 * @param groupName the unique name of the group
	 * @return an instance of class Group or <code>null</code> if not exists.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "group")
	public Group getGroupByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupName") String groupName) throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of groups for the current client.
	 * 
	 * @param clientName the unique name of the client
	 * @return a list of all groups of the given client
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "groups")
	public QueryResult<Group> getGroups(@WebParam(name = "clientName") String clientName, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of groups that where associated to a certain role with a certain scope.
	 * 
	 * @param clientName the unique name of the client
	 * @param applicationName the name of the application
	 * @param roleName the name of the application's role
	 * @param scope the scope of the role-group association
	 * @return a list of groups that where assigned to the given role with the given scope.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "groups")
	public List<Group> getGroupsByRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "roleName") String roleName, @WebParam(name = "scope") String scope)
			throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of groups that where associated to a certain role with a certain scope.
	 * 
	 * @param clientName the unique name of the client
	 * @param scope the scope of the role-group association
	 * @param RoleId the id of the role
	 * 
	 * @return a list of groups that where assigned to the given role with the given scope.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "groups")
	public List<Group> getGroupsByRoleId(@WebParam(name = "clientName") String clientName, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of groups where a user is a member. The account of the user is identified by the given uuid string.
	 * 
	 * @param clientName the unique name of the client.
	 * @param uuid the unique account id string to identify the group.
	 * @return a list of accounts that are associated as members to the given group.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "userGroups")
	public List<Group> getGroupsByUser(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException;

	/**
	 * Returns an instance of the role from the persistence context that can be identified with the given id.
	 * 
	 * @param clientName the unique name of the client
	 * @param roleId the id of the role.
	 * 
	 * @return an instance of Role if it is persistent or <code>null</code> otherwise.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public Role getRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "roleId") Long roleId) throws BackendException, IllegalRequestException;

	/**
	 * Returns a role that is associated to a given application of a given client.
	 * 
	 * @param clientName the unique name of the client.
	 * @param applicationName the unique name of the application
	 * @param roleName the unique name of the role
	 * @return an instance of class Role or <code>null</code> if not exists.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "role")
	public Role getRoleByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "roleName") String roleName) throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of roles that are associated to a certain application of a certain client.
	 * 
	 * @param clientName the unique name of the client.
	 * @param applicationName the unique name of the application.
	 * @return a list of Role instances that are associated to the given application.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "applicationRoles")
	public List<Role> getRolesByApplication(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName) throws BackendException, IllegalRequestException;

	/*
	 * methods for getting a list of users in a certain group with option to filter by their existence state
	 */

	/**
	 * Returns a list of roles that are associated to a certain application.
	 * 
	 * @param clientName the unique name of the client.
	 * @param applicationId the id of the application.
	 * @return a list of Role instances that are associated to the given application.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "applicationRoles")
	public List<Role> getRolesByApplicationId(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationId") Long applicationId) throws BackendException, IllegalRequestException;

	/**
	 * Returns an object that contains the list of roles that belongs to a certain client. If paging information are given by a query descriptor, the return object als contains information about the page amount, the current page and the amount of all
	 * roles of the client.
	 * 
	 * @param clientName the unique name of the client.
	 * @param queryDescriptor the query descriptor, which is optional.
	 * @return an object that contains the list of role that belongs to the given client.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "roles")
	public QueryResult<Role> getRolesByClient(@WebParam(name = "clientName") String clientName, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of roles that are associated with a given application
	 * 
	 * @param clientName the unique name of the client
	 * @param queryDescriptor TODO
	 * @param the id of the application for which all associated roles should be identified.
	 * 
	 * @return a list of Role instances that are associated with the application or <code>null</code> if none exists
	 * @throws BackendException
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "roles")
	public QueryResult<Role> getRolesByClientId(@WebParam(name = "clientName") String clientName, @WebParam(name = "clientId") Long clientId, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException,
			IllegalRequestException;

	/**
	 * Returns a list of roles that a given group is assigned to.
	 * 
	 * @param clientName the unique name of the client that is the owner of the group.
	 * @param groupName the name of the group.
	 * @param scope the scope. this parameter is optional. If it is null, all roles of the group will be returned, regardless of which scope.
	 * @return a list of Role instances that the given group is assigned to.
	 * @throws IllegalRequestException TODO
	 * @throws BackendException TODO
	 */
	@WebMethod
	@WebResult(name = "rolesByGroup")
	public List<Role> getRolesByGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupName") String groupName, @WebParam(name = "scope") String scope) throws IllegalRequestException, BackendException;

	/**
	 * Returns a list of roles that a given group is assigned to.
	 * 
	 * @param clientName the unique name of the client.
	 * @param groupId the id of the group.
	 * @param scope the scope. this parameter is optional. If it is null, all roles of the group will be returned, regardless of which scope.
	 * 
	 * @return a list of Role instances that the given group is assigned to.
	 * @throws IllegalRequestException TODO
	 * @throws BackendException TODO
	 */
	@WebMethod
	@WebResult(name = "rolesByGroup")
	public List<Role> getRolesByGroupId(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId, @WebParam(name = "scope") String scope) throws IllegalRequestException, BackendException;

	@WebMethod
	@WebResult(name = "roles")
	public List<Role> getRolesByUser(@WebParam(name = "uuid") String uuid, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of roles which are associated with a certain application and where a certain user is assigned to, belonging to a a certain community context.
	 * 
	 * @param uuid the unique uuid-attribute to identify the account of user.
	 * @param applicationId the id of the application.
	 * @param scope the community scope string.
	 * @return list of Role instances which belong to the given application where the given user is assigned to. <code></code> if there is no role which fulfills the condition.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "roles")
	public List<Role> getRolesByUserAndApplication(@WebParam(name = "uuid") String uuid, @WebParam(name = "applicationId") Long applicationId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of (role) users that has been assigned directly or indirectly to the given role. An instance of RoleUser also contains if the user has been directly assigned to the role with the given scope or indirectly as a member of a group.
	 * 
	 * @param clientName the unique name of the client.
	 * @param roleId the id of the given role.
	 * @param roleScope the scope that has to be a part of the user-role assignment.
	 * 
	 * @return a list of role users.
	 * @throws IllegalRequestException TODO
	 * @throws BackendException TODO
	 */
	@WebMethod
	@WebResult(name = "roleUsers")
	public List<RoleUser> getRoleUsers(@WebParam(name = "clientName") String clientName, @WebParam(name = "roleId") Long roleId, @WebParam(name = "roleScope") String roleScope) throws IllegalRequestException, BackendException;

	@WebMethod
	@WebResult(name = "userAccounts")
	QueryResult<User> getUserAccountsByGroupId(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws IllegalRequestException, BackendException;

	/**
	 * Returns a list of roles of a certain application that the given user has been assigned to.
	 * 
	 * @param uuid the uuid of the user.
	 * @param applicationId the id of the application.
	 * @param scope the scope that should be a part of the user-role assignment.
	 * @return a list of roles of the given application that the given user has been assigned to.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "userRoles")
	public List<UserRole> getUserRoles(@WebParam(name = "uuid") String uuid, @WebParam(name = "applicationId") Long applicationId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of users that are members of a group that has the given name.
	 * 
	 * @param clientName the unique name of the client.
	 * @param groupName the name of the group.
	 * @return the group of the given client that has the given name or <code>null</code> if no such exists.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "usersOfGroup")
	public List<Account> getUsersByGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupName") String groupName) throws BackendException, IllegalRequestException;

	/**
	 * Returns the users of the given group that have also been assigned to the given role with the given scope. This will be the cut of users that are members of both the group and role. If the group has been assigned to the role, all users of the group
	 * will be returned. Otherwise all users of the group will be returned that has been assigned to the role directly or indirectly because of their membership to other groups that have been assigned to that role.
	 * 
	 * @param clientName the unique name of the client.
	 * @param groupId the id of the group.
	 * @param roleId the id of the role.
	 * @param scope the scope of the role assignments.
	 * 
	 * @return the cut of the set of users of the group and the set of the role.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "usersByGroupAndRole")
	public List<Account> getUsersByGroupAndRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException,
			IllegalRequestException;

	/**
	 * Returns a list of users that are members of a group that has the given id.
	 * 
	 * @param clientName the unique name of the client
	 * @param groupId the id of the group.
	 * 
	 * @return a list of all users of the group.
	 * @throws BackendException
	 */
	@WebMethod
	@WebResult(name = "usersOfGroup")
	public List<Account> getUsersByGroupId(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId) throws IllegalRequestException, BackendException;

	/**
	 * Returns all users that are associated with a role with a given role name. Part of that association is the given scope. The role is assigned to an application that has the given name.
	 * 
	 * @param clientName the unique name of the client.
	 * @param applicationName the name of the application.
	 * @param roleName the name of the role.
	 * @param scope the scope that is part of the association.
	 * @return a list of user accounts that are assigned to the given role with the given scope.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "usersInRole")
	public List<Account> getUsersByRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "roleName") String roleName, @WebParam(name = "scope") String scope)
			throws BackendException, IllegalRequestException;

	/**
	 * Returns all users that are associated to a given Role that has the given id with a given (community) scope.
	 * 
	 * @param clientName the unique name of the client.
	 * @param roleId the id of the role.
	 * @param scope the scope that is part of the user-role association.
	 * 
	 * @return a list of user accounts that are assigned to the given role with the given scope.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "usersInRole")
	public List<Account> getUsersByRoleId(@WebParam(name = "clientName") String clientName, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException;

	/**
	 * Returns <code>true</code> if the group is assigned to the given role, <code>false</code>
	 * 
	 * @param clientName the unique name of the client.
	 * @param groupId the id of the group.
	 * @param roleId the id of the role.
	 * @param scope the scope of the group-to-role assignment.
	 * 
	 * @return <code>true</code> if the group is assigned to the given role, <code>false</code>.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public boolean isGroupInRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException;

	/**
	 * Determines if a user is member of a group.
	 * 
	 * @param uuid the unique attribute of the user account.
	 * @param the id of the group
	 * @return <code>true</code> if the given user is member of the given group, <code>false</code> otherwise.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "userInGroup")
	public boolean isUserInGroup(@WebParam(name = "uuid") String uuid, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException;

	/**
	 * Returns <code>true</code> if the user is assigned to the given role, <code>false</code>
	 * 
	 * @param uuid the uuid of the user.
	 * @param roleName the name of the role.
	 * @param applicationName the name of the application that is the owner of the role.
	 * @param scope the scope of the assignment.
	 * @return <code>true</code> if the user is (directly or indirectly) assigned to the given role, <code>false</code> otherwise.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "userInRole")
	boolean isUserInRole(@WebParam(name = "uuid") String uuid, @WebParam(name = "roleName") String roleName, @WebParam(name = "applicationName") String applicationName, @WebParam(name = "scope") String scope) throws BackendException,
			IllegalRequestException;

	/**
	 * Determines if a user is member of a role.
	 * 
	 * @param uuid the unique attribute of the user account.
	 * @param roleId the id of the role
	 * @param scope the community scope
	 * @return <code>true</code> if the given user is member of the given role, <code>false</code> otherwise.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "userInRole")
	public boolean isUserInRoleById(@WebParam(name = "uuid") String uuid, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException, IllegalRequestException;

	/**
	 * Persists a given instance of class Group. If it is already persistent this method updates the persistent version.
	 * 
	 * @param group the group to persist.
	 * @return the group instance after persisting it.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "group")
	public Group setGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "group") Group group) throws BackendException, IllegalRequestException;

	/**
	 * Persists a given instance of Role. If it is already persistent this method updates the persistent version.
	 * 
	 * @param clientName the unique name of the client
	 * @param role the instance of role to persist.
	 * @return
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "role")
	public Role setRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "role") Role role) throws BackendException, IllegalRequestException;

}
