package org.evolvis.idm.relation.permission.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.relation.permission.model.Group;
import org.evolvis.idm.relation.permission.model.InternalRoleAssignment;
import org.evolvis.idm.relation.permission.model.InternalRoleType;

@WebService(serviceName = "InternalRoleScopeManagementService", name = "InternalRoleScopeManagementServicePT", portName = "InternalRoleScopeManagementServicePort")
public interface InternalRoleScopeManagement extends AbstractBaseService {

	/**
	 * Extends the administration scope of a user-account - internal-Role assignment with a given group.
	 * 
	 * @param internalRoleAssigmentId the id of the InternalRoleAssignment instance for which the administration scope is to be extended.
	 * @param groupId the id of the group which is also to be administrated by the user in his specific internal role.
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	@WebMethod(exclude = true)
	public void setInternalRoleScope(@WebParam(name = "internalRoleAssignmentId") Long internalRoleAssigmentId, @WebParam(name = "groupId") Long groupId) throws IllegalRequestException, BackendException;

	/**
	 * Extends the administration scope of user account with a specific internal role with a given group.
	 * 
	 * @param uuid the uuid of the user account.
	 * @param internalRole the internal role of the user account with has to be used to create the role scope.
	 * @param groupId the id of the group.
	 * @throws IllegalRequestException TODO
	 * @throws BackendException TODO
	 */
	@WebMethod
	public void setInternalRoleScope(@WebParam(name = "uuid") String uuid, @WebParam(name = "internalRole") InternalRoleType internalRole, @WebParam(name = "groupId") Long groupId) throws IllegalRequestException, BackendException;

	/**
	 * Removes an internal role scope for a given group and a the given user account with the given internal role.
	 * 
	 * @param uuid the uuid of the user account.
	 * @param internalRole the internal role of the user account that is used for the internal role assignment.
	 * @param groupId the id of the group.
	 * 
	 * @throws IllegalRequestException TODO
	 * @throws BackendException TODO
	 */
	@WebMethod
	public void deleteInternalRoleScope(@WebParam(name = "uuid") String uuid, @WebParam(name = "internalRole") InternalRoleType internalRole, @WebParam(name = "groupId") Long groupId) throws IllegalRequestException, BackendException;

	/**
	 * Returns a list of InternalRoleScope instances that are persistent for the given group.
	 * 
	 * @param clientName the unique name of the client.
	 * @param groupId the id of the group that is used to identify the specific group.
	 * @return a list of InternalRoleScope instances that exist for the given account.
	 * @throws IllegalRequestException TODO
	 * @throws BackendException TODO
	 */
	@WebMethod
	@WebResult(name = "internalRoleAssignments")
	public List<InternalRoleAssignment> getInternalRolesForGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId) throws IllegalRequestException, BackendException;

	/**
	 * Returns a list of InternalRoleScope instances that are persistent for the given account.
	 * 
	 * @param uuid the unique attribute of account that is used to identify the specific account.
	 * @param queryDescriptor TODO
	 * @return a list of InternalRoleScope instances that exist for the given group.
	 * @throws IllegalRequestException TODO
	 * @throws BackendException TODO
	 */
	@WebMethod
	@WebResult(name = "groups")
	public List<Group> getInternalRoleScopesForAccount(@WebParam(name = "uuid") String uuid, @WebParam(name = "internalRole") InternalRoleType internalRole, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws IllegalRequestException, BackendException;

}
