/**
 * 
 */
package org.evolvis.idm.relation.multitenancy.model;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Comparator;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.BeanOrderProperty;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;

/**
 * @author Yorka Neumann
 * 
 */
@XmlRootElement(name = "clientQueryResult", namespace = "http://idm.evolvis.org")
@XmlType(name = "clientQueryResult", namespace = "http://idm.evolvis.org")
public class ClientQueryResult extends QueryResult<Client> implements Comparator<Client> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3162654782813278085L;

	public void adaptResultListOrderAndPaging() throws IllegalRequestException {
		if (this.getQueryDescriptor() != null) {
			for (BeanOrderProperty beanOrderProperty : this.getQueryDescriptor().getOrderProperties()) {
				if (!beanOrderProperty.isValid())
					throw new IllegalRequestException("Invalid order property: " + beanOrderProperty.getFieldPath());
			}
		}
		if (this.getResultList() != null) {
			// sort the result list by default sort order or if available by
			// order properties.
			Collections.sort(this.getResultList(), this);

			if (QueryDescriptor.isPaging(this.getQueryDescriptor())) {
				this.setTotalSize(this.getResultList().size());

				/*
				 * if the offset exceeds the total size - correct the offset to obtain the last page
				 */
				if ((this.getTotalSize() - 1) < this.getQueryDescriptor().getOffSet()) {
					this.setQueryDescriptor(this.getDescriptorForPage(this.getPageAmount()));
				}
				int fromIndex = this.getQueryDescriptor().getOffSet();
				int toIndex = this.getQueryDescriptor().getOffSet() + this.getQueryDescriptor().getLimit();
				if (toIndex > this.getResultList().size() - 1)
					toIndex = this.getResultList().size();
				this.setResultList(this.getResultList().subList(fromIndex, toIndex));
			}
		}
	}

	@Override
	public int compare(Client client0, Client client1) {
		int result = 0;
		if (this.getQueryDescriptor() != null && this.getQueryDescriptor().getOrderProperties() != null && this.getQueryDescriptor().getOrderProperties().size() != 0) {
			if (client0.equals(client1))
				return 0;
			for (BeanOrderProperty orderProperty : this.getQueryDescriptor().getOrderProperties()) {
				try {
					Method beanGetterMethod = client0.getClass().getMethod("get" + orderProperty.getFieldPath().substring(0, 1).toUpperCase() + orderProperty.getFieldPath().substring(1), (Class<?>[]) null);
					String client0Property = (String) beanGetterMethod.invoke(client0, (Object[]) null);
					String client1Property = (String) beanGetterMethod.invoke(client1, (Object[]) null);
					result = client0Property.compareTo(client1Property);
					if (result != 0) {
						if (!orderProperty.isSortOrderAscending())
							result *= (-1);
						return result;
					}

				} catch (NoSuchMethodException e) {
					// ok, try to access field directly
				} catch (Exception e) {
					// TODO may be to do
				}
			}
		} else
			return client0.getDisplayName().compareTo(client1.getDisplayName());
		return 0;
	}

}
