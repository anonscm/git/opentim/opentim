package org.evolvis.idm.relation.organization.model;

public interface OrgUnitMapEntryPropertyFields {
	public static final String FIELD_PATH_ORGUNITMAPENTRY_KEY = "key";
	public static final String FIELD_PATH_ORGUNITMAPENTRY_VALUE = "value";
}
