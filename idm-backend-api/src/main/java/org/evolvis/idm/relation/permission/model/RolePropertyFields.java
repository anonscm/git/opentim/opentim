package org.evolvis.idm.relation.permission.model;

public interface RolePropertyFields {
	
	public static final String FIELD_PATH_NAME = "name";
	public static final String FIELD_PATH_DISPLAYNAME = "displayName";
	public static final String FIELD_PATH_APPLICATION_PREFIX = "application.";
}
