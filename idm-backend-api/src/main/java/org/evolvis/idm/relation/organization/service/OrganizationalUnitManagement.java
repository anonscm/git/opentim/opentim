package org.evolvis.idm.relation.organization.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.organization.model.OrgUnit;
import org.evolvis.idm.relation.organization.model.OrgUnitMap;
import org.evolvis.idm.relation.organization.model.OrgUnitMapEntryQueryResult;
import org.evolvis.idm.relation.permission.model.Group;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "OrganizationalUnitManagementService", name = "OrganizationalUnitManagementServicePT", portName = "OrganizationalManagementServicePort")
public interface OrganizationalUnitManagement extends AbstractBaseService {

	/**
	 * Adds a given value to a given organisational unit that can be addressed by a given key.
	 * 
	 * @param clientName the unique name of the client
	 * @param organisationalUnitId the id of the organisational unit
	 * @param key the key which can be used to address the given value.
	 * @param value the value to be assigned to the organisational unit.
	 * 
	 * @return the org unit value that is replaced be the given value, <code>null</code> if there was no value already stored for the given key.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "removedOrgUnitValue")
	public String addValueToOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "organisationalUnitId") Long organisationalUnitId, @WebParam(name = "key") String key, @WebParam(name = "value") String value) throws BackendException,
			IllegalRequestException;


	/**
	 * Removes a value that is assigned to a given organisational unit and that can be identified by the given key.
	 * 
	 * @param clientName the unique name of the client
	 * @param organisationalUnitId the id of the organisational unit.
	 * @param key the key to identify the value that is to be removed.
	 * 
	 * @return TODO
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "removedOrgUnitValue")
	public String removeValueFromOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "organisationalUnitId") Long organisationalUnitId, @WebParam(name = "key") String key) throws BackendException, IllegalRequestException;


	/**
	 * Returns a value that is assigned to a given organisational unit and that can be identified by the given key.
	 * 
	 * @param clientName the unique name of the client
	 * @param organisationalUnitId the id of the organisational unit.
	 * @param key the key to identify the value that is to be returned.
	 * 
	 * @return the wanted value or <code>null</code> if it does not exist in persistent context.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "metaDataValue")
	public String getValueFromOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "organisationalUnitId") Long organisationalUnitId, @WebParam(name = "key") String key) throws BackendException, IllegalRequestException;


	/**
	 * Returns a java.util.Map that maps a set of key to a set of values that are assigned to the given organisational unit.
	 * 
	 * @param clientName the unique name of the client
	 * @param organisationalUnitId the id of the organisational unit.
	 * @param queryDescriptor TODO
	 * 
	 * @return a map of all keys and values that are assigned to the given organisational unit.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "metaDataMap")
	public OrgUnitMap getOrgUnitMap(@WebParam(name = "clientName") String clientName, @WebParam(name = "organisationalUnitId") Long organisationalUnitId, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException,
			IllegalRequestException;


	/**
	 * Returns the a list of map entries of a map that belongs to a certain org. unit. This method is normally used to get a specific order of the map entries.
	 * 
	 * @param clientName the unique name of the client
	 * @param orgUnitMapId the id of the map
	 * @param queryDescriptor the query descriptor
	 * @return the list of map entries.
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	@WebMethod
	@WebResult(name = "mapEntries")
	public OrgUnitMapEntryQueryResult getOrgUnitMapEntries(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long orgUnitId, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor)
			throws IllegalRequestException, BackendException;


	/**
	 * Returns all persistent organisational units.
	 * 
	 * @param clientName the unique name of the client
	 * @param queryDescriptor TODO
	 * 
	 * @return a list of all persistent organisational units.
	 * @throws BackendException
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	@WebResult(name = "orgUnits")
	public QueryResult<OrgUnit> getOrgUnits(@WebParam(name = "clientName") String clientName, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, IllegalRequestException;


	/**
	 * Returns the instance of the organisational unit that has the given name.
	 * 
	 * @param clientName the unique name of the client
	 * @param organisationalUnitName the name of the organisational unit that is unique in the persistent context.
	 * 
	 * @return the instance of the organisational unit.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "orgUnit")
	public OrgUnit getOrgUnitByName(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitName") String organisationalUnitName) throws BackendException, IllegalRequestException;


	/**
	 * 
	 * @param clientName the unique name of the client
	 * @param organisationalUnitId
	 * @return
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "orgUnit")
	public OrgUnit getOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long organisationalUnitId) throws BackendException, IllegalRequestException;


	/**
	 * Persists a given instance of an organisational unit. If the given organisational unit is already persistent, e.g. the id has been set, the persistent version will be updated by the given one.
	 * 
	 * @param orgUnit the instance of the organisational unit to persist.
	 * @param clientName the unique name of the client
	 * @return
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "orgUnit")
	public OrgUnit setOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnit") OrgUnit orgUnit) throws BackendException, IllegalRequestException;


	/**
	 * Removes the given organisational unit from persistent context.
	 * 
	 * @param orgUnit the instance of the organisational unit that is to remove from persistent context.
	 * @param clientName the unique name of the client
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	public void deleteOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnit") OrgUnit orgUnit) throws BackendException, IllegalRequestException;


	/**
	 * Removes the given organisational unit from persistent context.
	 * 
	 * @param clientName the unique name of the client
	 * @param organisationalUnitId the id of the organisational unit.
	 * 
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	public void deleteOrgUnitById(@WebParam(name = "clientName") String clientName, @WebParam(name = "organisationalUnitId") Long organisationalUnitId) throws BackendException, IllegalRequestException;


	/**
	 * Returns a list of all organisational units that the given user is assigned to.
	 * 
	 * @param clientName the unique name of the client
	 * @param uuid the GUId of the user.
	 * 
	 * @return the list of all organisational units that the given user is assigned to.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "orgUnitsByUser")
	public List<OrgUnit> getOrgUnitsByUser(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException;


	/**
	 * Returns a list of all organisational units that belong to a given group.
	 * 
	 * @param clientName the unique name of the client
	 * @param groupId the id of the group.
	 * 
	 * @return the list of all OrgUnit instances that are assigned to the given group.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "orgUnitsByGroup")
	public List<OrgUnit> getOrgUnitsByGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException;


	/**
	 * Returns a list of organisational units where exists a data value that is equal to the given.
	 * 
	 * @param clientName the unique name of the client
	 * @param value the value that must match to a value of the organisational units to return.
	 * 
	 * @return the list of matching organisational units.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "orgUnitsByValue")
	public List<OrgUnit> getOrgUnitsByValue(@WebParam(name = "clientName") String clientName, @WebParam(name = "value") String value) throws BackendException, IllegalRequestException;


	/**
	 * Returns a list of all users that are assigned by the given organisational unit (e.g. the group of the unit).
	 * 
	 * @param clientName the unique name of the client
	 * @param orgUnitName the unique name of the organisational unit.
	 * 
	 * @return the list of all Account instances that are assigned to the organisational unit (e.g. the group of the unit).
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "accountsOfOrgUnit")
	public List<Account> getUsersByOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitName") String orgUnitName) throws BackendException, IllegalRequestException;


	@WebMethod
	@WebResult(name = "accountsOfOrgUnit")
	public List<Account> getUsersByOrgUnitId(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long orgUnitId) throws BackendException, IllegalRequestException;


	/**
	 * Returns a list of all users that are assigned to both the given organisational unit and the group.
	 * 
	 * @param clientName the unique name of the client
	 * @param organisationalUnitId the id of the organisational unit.
	 * @param groupId the id of the group.
	 * 
	 * @return the list of all Account instances that are assigned to the organisational unit (e.g. the group of the unit).
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "accountsOfOrgUnitAndGroup")
	public List<Account> getUsersByOrgUnitAndGroupById(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long organisationalUnitId, @WebParam(name = "groupId") Long groupId) throws BackendException, IllegalRequestException;


	/**
	 * Returns a list of all users that are assigned to both the given organisational unit and the group.
	 * 
	 * @param clientName the unique name of the client
	 * @param orgUnitName the unique name of the organisational unit.
	 * @param the name of the group.
	 * 
	 * @return the list of all Account instances that are assigned to the organisational unit (e.g. the group of the unit).
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "accountsOfOrgUnitAndGroup")
	public List<Account> getUsersByOrgUnitAndGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "groupName") String groupName, @WebParam(name = "orgUnitName") String orgUnitName) throws BackendException, IllegalRequestException;


	/**
	 * Returns the group that is owning the given org. unit.
	 * 
	 * @param clientName the unique name of the client.
	 * @param orgUnitName the name of the org. unit.
	 * @return
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "groupOfOrgUnit")
	public Group getGroupByOrgUnit(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitName") String orgUnitName) throws BackendException, IllegalRequestException;


	/**
	 * Returns the group that the given organzational unit is assigned to.
	 * 
	 * @param clientName the unique name of the client.
	 * @param orgUnitId the id of the organizational unit.
	 * @return the group of the organizational unit.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "groupOfOrgUnit")
	public Group getGroupByOrgUnitId(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long orgUnitId) throws BackendException, IllegalRequestException;


	/**
	 * Returns a list of users that are both a member of the given organizational unit and are assigned to the given role.
	 * 
	 * @param clientName the unique name of the client.
	 * @param orgUnitId the id of the organizational unit
	 * @param roleId the id of the role
	 * @param scope the scope of the role
	 * @return the list of the users of the given role and org. unit.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "orgUnitAndRoleUsers")
	public List<Account> getUsersByOrgUnitAndRole(@WebParam(name = "clientName") String clientName, @WebParam(name = "orgUnitId") Long orgUnitId, @WebParam(name = "roleId") Long roleId, @WebParam(name = "scope") String scope) throws BackendException,
			IllegalRequestException;
}
