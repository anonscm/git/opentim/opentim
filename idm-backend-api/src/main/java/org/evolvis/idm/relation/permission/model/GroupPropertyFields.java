package org.evolvis.idm.relation.permission.model;

public interface GroupPropertyFields extends GroupSearchableFields {
	public static final String FIELD_PATH_WRITEPROTECTED = "writeProtected";
	
}
