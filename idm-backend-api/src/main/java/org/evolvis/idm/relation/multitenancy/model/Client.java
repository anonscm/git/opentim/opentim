package org.evolvis.idm.relation.multitenancy.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.relation.permission.model.Group;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "Client", uniqueConstraints = { @UniqueConstraint(columnNames = { "name" }) })
@XmlType(name = "Client", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "client", namespace = "http://idm.evolvis.org")
public class Client extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = -7665020239367600509L;

	@NotNull
	@Size(min = 1, max = 50, message = "violation.client.size")
	@Pattern(regexp = "[a-zA-Z0-9._-]*", message = "violation.client.pattern")
	@Column(nullable = false, length = 50)
	private String name;

	@NotNull
	@Size(min = 1, max = 50, message = "violation.client.size")
	@Pattern(regexp = DisplayName.PATTERN, message = "violation.client.pattern")
	@Column(nullable = false, length = 50)
	private String displayName;
	private String certificationData;

	// used only to cascade on delete
	@SuppressWarnings("unused")
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "client")
	@OrderBy("displayName ASC")
	private List<Account> accounts;

	@SuppressWarnings("unused")
	@OneToMany(cascade = { CascadeType.REMOVE }, fetch = FetchType.LAZY, mappedBy = "client")
	@OrderBy("displayName ASC")
	private List<AttributeGroup> attributeGroups;

	@SuppressWarnings("unused")
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "client")
	@OrderBy("displayName ASC")
	private List<Application> applications;

	@SuppressWarnings("unused")
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "client")
	@OrderBy("displayName ASC")
	private List<Group> groups;

	@OneToOne(mappedBy = "client", optional = true, cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	private ClientPropertyMap clientPropertyMap;

	public Client() {
		// nothing
	}

	public Client(String name, String displayName) {
		setName(name);
		setDisplayName(displayName);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@XmlTransient
	public String getLabel() {
		return displayName + " (" + name + ")";
	}

	public String getCertificationData() {
		return certificationData;
	}

	public void setCertificationData(String certificationData) {
		this.certificationData = certificationData;
	}

	@Override
	@Transient
	public String getClientName() {
		return this.getName();
	}

	/**
	 * Assigns a map of properties that have to be assigned to the client.
	 * 
	 * @param clientPropertyMap
	 *            the map of properties.
	 */
	public void setClientPropertyMap(ClientPropertyMap clientPropertyMap) {
		this.clientPropertyMap = clientPropertyMap;
	}

	/**
	 * Returns a map with properties which are assigned to the client.
	 * 
	 * @return the clientPropertyMap a map with properties for that client.
	 */
	@XmlTransient
	public ClientPropertyMap getClientPropertyMap() {
		return clientPropertyMap;
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}	
	
}
