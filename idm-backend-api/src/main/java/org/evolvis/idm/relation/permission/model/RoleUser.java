package org.evolvis.idm.relation.permission.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.identity.account.model.AbstractUserEntity;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "RoleUser")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RoleUser", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "roleUser", namespace = "http://idm.evolvis.org")
public class RoleUser extends AbstractUserEntity {

	@Column(insertable = false)
	private boolean directAssignment;
	
	@Column(insertable = false)
	private Long role_Id;

	@Column(insertable = false)
	private String scope;
	
	public RoleUser(){
		//nothing
	}
	
	public boolean isDirectAssignment() {
		return this.directAssignment;
	}

	public String getScope() {
		return scope;
	}

	public Long getRole_Id() {
		return role_Id;
	}
}
