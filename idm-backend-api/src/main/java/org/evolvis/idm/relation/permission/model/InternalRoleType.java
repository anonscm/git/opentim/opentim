/**
 * This enumeration represents type of the possible internal roles that can to users.
 * Please take care of the order if you want to add another internal role. It is sorted 
 * from the highest level to the lowest. 
 */
package org.evolvis.idm.relation.permission.model;

public enum InternalRoleType {
	SUPER_ADMIN, CLIENT_ADMIN, SECTION_ADMIN, USER
}
