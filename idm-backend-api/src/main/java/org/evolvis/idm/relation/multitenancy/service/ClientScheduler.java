package org.evolvis.idm.relation.multitenancy.service;

import javax.ejb.Local;

@Local
public interface ClientScheduler {

	public void initialize();
	public void initializeTimerservice();
}