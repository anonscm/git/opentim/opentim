package org.evolvis.idm.relation.organization.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractMap;
import org.evolvis.idm.common.util.ConfigurationUtil;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "OrgUnitMap", uniqueConstraints = @UniqueConstraint(columnNames = { "orgUnit_id" }))
@XmlType(name = "OrgUnitMap", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "orgUnitMap", namespace = "http://idm.evolvis.org")
public class OrgUnitMap extends AbstractMap<OrgUnitMapEntry> implements Serializable {

	private static final long serialVersionUID = -7698043448388724832L;

	@ManyToOne(optional = false)
	@XmlIDREF
	private OrgUnit orgUnit;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "map", fetch = FetchType.EAGER)
	@OrderBy("key ASC")
	private List<OrgUnitMapEntry> mapEntries;


	@Override
	protected OrgUnitMapEntry createMapEntryInstance(String key, String value) {
		return new OrgUnitMapEntry(key, value, this);
	}


	@Override
	@Valid
	public List<OrgUnitMapEntry> getMapEntries() {
		if (this.mapEntries == null)
			mapEntries = new LinkedList<OrgUnitMapEntry>();
		return this.mapEntries;
	}


	@Override
	public void setMapEntries(List<OrgUnitMapEntry> mapEntries) {
		this.mapEntries = mapEntries;
	}


	public void setOrgUnit(OrgUnit orgUnit) {
		this.orgUnit = orgUnit;
	}


	public OrgUnit getOrgUnit() {
		return orgUnit;
	}


	@Transient
	public String getClientName() {
		if(this.getOrgUnit() != null)
			return this.getOrgUnit().getClientName();
		return null;
	}

}
