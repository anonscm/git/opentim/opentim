package org.evolvis.idm.identity.privatedata.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.privatedata.model.Approval;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "AttributeRetrievalService", name = "AttributeRetrievalServicePT", portName = "AttributeRetrievalServicePort")
public interface PrivateDataRetrieval extends AbstractPrivateDataBase {

	/**
	 * Returns a list of value sets that are assigned to the user account that has the given uuid.
	 * 
	 * @param uuid the uuid of the user account.
	 * @return the list of value sets that are assigned to the given user account. This may be an empty list.
	 * @throws BackendException
	 * @throws AccessDeniedException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "values")
	public List<ValueSet> getValues(@WebParam(name = "uuid") String uuid) throws BackendException, AccessDeniedException, IllegalRequestException;

	/**
	 * Returns a list of value sets that are assigned to both, one of given the attribute groups and the account that owns the given uuid.
	 * 
	 * @param uuid the uuid of the user account.
	 * @param attributes the list of attribute groups for which the assigned value sets should be returned.
	 * @return list of value sets that are assigned to both, one of given the attribute groups and the account that owns the given uuid. This may be an empty list.
	 * @throws BackendException
	 * @throws AccessDeniedException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "values")
	public List<ValueSet> getValuesByAttributes(@WebParam(name = "uuid") String uuid, @WebParam(name = "attributes") List<AttributeGroup> attributes) throws BackendException, AccessDeniedException, IllegalRequestException;

	/**
	 * Returns a value set that has the given id and belongs to the account with the given uuid.
	 * 
	 * @param uuid the uuid of the account.
	 * @param valueSetId the id of the wanted value set
	 * @return the wanted value set.
	 * @throws BackendException
	 * @throws AccessDeniedException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "attributes")
	public ValueSet getValuesById(@WebParam(name = "uuid") String uuid, @WebParam(name = "valueSetId") Long valueSetId) throws BackendException, AccessDeniedException, IllegalRequestException;

	/**
	 * Returns the complete value instance that has the given id and is assigned to the user account that has the given uuid.
	 * 
	 * @param uuid the uuid of the user.
	 * @param valueId the id of the value.
	 * @return
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "value")
	public Value getValueWithBinaryAttachmentById(@WebParam(name = "uuid") String uuid, @WebParam(name = "valueId") Long valueId) throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of all approvals of the user with the given uuid.
	 * 
	 * @param uuid the uuid of the user.
	 * @return a list of all approvals of the user with the given uuid.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "approvals")
	public List<Approval> getApprovalsByAccount(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of all approvals that are assigned to the given application.
	 * 
	 * @param clientName the unique name of the client.
	 * @param applicationId the id of the application.
	 * @return a list of all approvals that have been assigned to the application with the given id.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "approvals")
	public List<Approval> getApprovalsByApplicationId(@WebParam(name = "clientName") String clientName, @WebParam(name = "applicationId") Long applicationId) throws BackendException, IllegalRequestException;

	/**
	 * Returns a list of all approvals of a user with the given uuid and which have been assigned to the application with the given id.
	 * 
	 * @param uuid the uuid of the user.
	 * @param applicationId the id of the application.
	 * @return a list of all approvals of the user with the given uuid and which have been assigned to the application with the given id.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "approvals")
	public List<Approval> getApprovalsByUuidAndApplicationId(@WebParam(name = "uuid") String uuid, @WebParam(name = "applicationId") Long applicationId) throws BackendException, IllegalRequestException;

}
