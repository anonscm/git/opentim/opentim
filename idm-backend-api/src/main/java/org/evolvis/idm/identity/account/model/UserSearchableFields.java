package org.evolvis.idm.identity.account.model;

public interface UserSearchableFields extends AccountSearchableFields {
	public static final String FIELD_PATH_FIRSTNAME = "firstname";
	public static final String FIELD_PATH_LASTNAME = "lastname";
}
