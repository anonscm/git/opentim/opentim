package org.evolvis.idm.identity.account.model;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.util.ConfigurationUtil;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "User")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
// @XmlType(propOrder = { "firstname", "lastname", "password", "passwordRepeat",
// "localeString", "status", "client", "username", "uuid", "displayName",
// "creationDate", "deactivationDate" })
@XmlType(name = "User", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "user", namespace = "http://idm.evolvis.org")
public class User extends AbstractUserEntity {
	private static final long serialVersionUID = -6985844254412304536L;

	public User() {
		super();
	}

	public User(Account account) {
		super(account);
	}

	public User(Account account, String extendedFlag) {
		super(account, extendedFlag);
	}
}
