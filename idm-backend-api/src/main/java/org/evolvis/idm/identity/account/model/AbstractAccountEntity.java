package org.evolvis.idm.identity.account.model;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.identity.applicationdata.model.AppMetaData;
import org.evolvis.idm.identity.privatedata.model.Approval;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.evolvis.idm.relation.permission.model.GroupAssignment;
import org.evolvis.idm.relation.permission.model.InternalRoleAssignment;
import org.evolvis.idm.relation.permission.model.RoleAssignment;

@MappedSuperclass
@XmlType(name = "AbstractAccountEntity", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "abstractAccountEntity", namespace = "http://idm.evolvis.org")
public class AbstractAccountEntity extends AbstractEntity {

	// TODO repair NotNull constraint
//	@NotNull
//	@Size(min = 6, max = 60, message="violation.account.size")
	@Column(nullable = false, length = 320)
	private String username;
	
	@Size(min=6, max=81, message="violation.account.size")
	@DisplayName
	@Column(nullable = false, length = 100)
	private String displayName;

	// TODO repair NotNull constraint
//	@NotNull
	@Size(min = 1, max = 100, message="violation.account.size")
	@Column(nullable = false, length = 100)
	private String uuid;

	@Transient
	@XmlTransient
	protected boolean clientFlag = false;
	
	//@Null
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	private Client client;
	
	@Temporal(TemporalType.DATE)
	private Calendar creationDate;
	
	@Temporal(TemporalType.DATE)
	private Calendar deactivationDate;
	
	// used only to cascade on delete
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "account")
	protected List<ValueSet> valueSets;
	
	@SuppressWarnings("unused")
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "account")
	private List<Approval> approvals;

	@SuppressWarnings("unused")
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "account")
	private List<GroupAssignment> groupAssignments;

	@SuppressWarnings("unused")
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "account")
	private List<RoleAssignment> roleAssignments;
	
	@SuppressWarnings("unused")
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "account")
	private List<InternalRoleAssignment> internalRoleAssigments;
	
	@SuppressWarnings("unused")
	@OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, mappedBy = "account")
	private List<AppMetaData> appMetaData;
	
	private boolean softDelete = false;


	public String getUuid() {
		return uuid;
	}

	public void setUuid(String accountid) {
		this.uuid = accountid;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) +
				"\tid=" + id +
				"; accountid=" + uuid;				
	}
	
	public Client getClient() {
		if(!clientFlag)
			return null;
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
		this.clientFlag = true;
	}
	
	public boolean isSoftDelete(){
		return this.softDelete;
	}
	
	public void setSoftDelete(boolean value){
		this.softDelete = value;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
		
		// TODO jneuma fix workaround
		if (this.displayName == null)
			setDisplayName(username);
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	public Calendar getCreationDate() {
		return creationDate;
	}

	public void setDeactivationDate(Calendar deactivationDate) {
		this.deactivationDate = deactivationDate;
	}

	public Calendar getDeactivationDate() {
		return deactivationDate;
	}

	@Override
	@Transient
	public String getClientName() {
		if(this.client != null)
			return this.client.getClientName();
		return null;
	}
}