package org.evolvis.idm.identity.privatedata.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.model.ReferencedEntityContainer;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.Application;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "application_id", "account_id", "approvedValueSet_id", "approvedAttributeValue_id" }) }, name = ConfigurationUtil.DB_TABLE_PREFIX + "Approval")
@XmlType(name = "Approval", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "approval", namespace = "http://idm.evolvis.org")
public class Approval extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = 1489269048586433002L;

	@NotNull
	@ManyToOne(optional = false)
	private Application application;

	@NotNull
	@ManyToOne(optional = false)
	@JoinColumn(name = "account_id" ,updatable = false)
	private Account account;

	@ManyToOne
	@JoinColumn(updatable = false)
	private ValueSet approvedValueSet;

	@ManyToOne
	@JoinColumn(updatable = false)
	private Value approvedAttributeValue;

	public Approval() {
	}

	public Approval(Application application) {
		setApplication(application);
	}
	
	public Approval(Application application, Account account){
		this(application);
		this.setAccount(account);
	}



	public Approval(Application application, Account account, Value value){
		this(application, account);
		this.setApprovedAttributeValue(value);
		ReferencedEntityContainer entityContainer = new ReferencedEntityContainer();
		entityContainer.addValue(value);
		this.setReferencedEntityContainer(entityContainer);
	}

	public Approval(Application application, Account account, ValueSet valueSet){
		this(application, account);
		this.setApprovedValueSet(valueSet);
		ReferencedEntityContainer entityContainer = new ReferencedEntityContainer();
		entityContainer.addValueSet(valueSet);
		this.setReferencedEntityContainer(entityContainer);
	}

	public Approval(Approval approval){
		this(approval.getApplication(), approval.getAccount());
		this.setId(approval.getId());
		ReferencedEntityContainer entityContainer = new ReferencedEntityContainer();
		this.setReferencedEntityContainer(entityContainer);
		if(approval.getApprovedAttributeValue() != null){
			this.setApprovedAttributeValue(approval.getApprovedAttributeValue());
			entityContainer.addValue(this.getApprovedAttributeValue());
			this.getApprovedAttributeValue().setApprovals(null);
		}
		else if(approval.getApprovedValueSet() != null){
			this.setApprovedValueSet(approval.getApprovedValueSet());
			entityContainer.addValueSet(approval.getApprovedValueSet());
			this.getApprovedValueSet().setApprovals(null);
		}
	}
	
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@XmlIDREF
	public ValueSet getApprovedValueSet() {
		return approvedValueSet;
	}

	public void setApprovedValueSet(ValueSet approvedValueSet) {
		if(this.getApprovedValueSet() != null)
			this.getReferencedEntityContainer().getValueSets().remove(this.getApprovedValueSet());
		this.getReferencedEntityContainer().getValueSets().add(approvedValueSet);
		this.approvedValueSet = approvedValueSet;
	}

	@XmlIDREF
	public Value getApprovedAttributeValue() {
		return approvedAttributeValue;
	}

	public void setApprovedAttributeValue(Value approvedAttributeValue) {
		if(this.getApprovedAttributeValue() != null)
			this.getReferencedEntityContainer().getValues().remove(this.getApprovedAttributeValue());
		this.getReferencedEntityContainer().getValues().add(approvedAttributeValue);
		this.approvedAttributeValue = approvedAttributeValue;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof Approval) {
			Approval bean = (Approval) obj;
			if (getId() != null && bean.getId() != null && getId().equals(bean.getId()))
				return true;

			// valueSet == valueSet
			// value == value || valueSets == null
			// account == account
			if (getApprovedValueSet() != null
					&& bean.getApprovedValueSet() != null
					&& getApprovedValueSet().equals(bean.getApprovedValueSet())
					&& ((getApprovedAttributeValue() != null && bean.getApprovedAttributeValue() != null && getApprovedAttributeValue().equals(bean.getApprovedAttributeValue())) || (getApprovedAttributeValue() == null && bean.getApprovedAttributeValue() == null))
					&& getAccount() != null && bean.getAccount() != null && getAccount().equals(bean.getAccount()))
				return true;
		}
		return false;
	}

	@Override
	@Transient
	public String getClientName() {
		if(this.getAccount() != null && this.getAccount().getClientName() != null)
			return this.getAccount().getClientName();
		if(this.getApplication() != null && this.getApplication().getClientName() != null)
			return this.getApplication().getClientName();
		if(this.getApprovedValueSet() != null && this.getApprovedValueSet().getClientName() != null)
			return this.getApprovedValueSet().getClientName();
		if(this.getApprovedAttributeValue() != null && this.getApprovedAttributeValue().getClientName() != null)
			return this.getApprovedAttributeValue().getClientName();
		return null;
	}
	
	
}
