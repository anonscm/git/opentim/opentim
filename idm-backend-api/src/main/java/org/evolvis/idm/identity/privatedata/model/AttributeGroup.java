package org.evolvis.idm.identity.privatedata.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.common.validation.AttributeListUniqueNames;
import org.evolvis.idm.common.validation.DisplayName;
import org.evolvis.idm.relation.multitenancy.model.Client;
import org.hibernate.annotations.Cascade;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "name", "client_id" }) }, name = ConfigurationUtil.DB_TABLE_PREFIX + "AttributeGroup")
//TODO remove or restore : @XmlType(propOrder = { "name", "displayName", "displayOrder", "multiple", "attributes" })
@XmlType(name = "AttributeGroup", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "attributeGroup", namespace = "http://idm.evolvis.org")
public class AttributeGroup extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = 4185301173709222808L;


	@NotNull
	@Size(min = 1, max = 50, message="violation.attributegroup.size")
	@Pattern(regexp="[a-zA-Z0-9._-]*", message= "violation.attributegroup.pattern")
	@Column(nullable = false, length = 100)
	private String name;
	
	@NotNull
	@Size(min = 1, max = 50, message="violation.attributegroup.size")
	@Pattern(regexp = DisplayName.PATTERN, message= "violation.attributegroup.pattern")
	@Column(nullable = false, length = 100)
	private String displayName;
	
	private Integer displayOrder;
	
	private boolean multiple;
	
	@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "attributeGroup")
	@OrderBy("displayOrder ASC")
	@AttributeListUniqueNames
	private List<Attribute> attributes;

	
	@ManyToOne(optional = false)
	private Client client;

	// used only to cascade on delete
	@SuppressWarnings("unused")
	@OneToMany(cascade = {CascadeType.MERGE,CascadeType.REFRESH,CascadeType.REMOVE}, fetch = FetchType.LAZY, mappedBy = "attributeGroup")
	private List<ValueSet> valueSets;

	public AttributeGroup() {
		// nothing.
	}

	public AttributeGroup(String name, String displayName) {
		setName(name);
		setDisplayName(displayName);
	}

	public AttributeGroup(String name, String displayName, Integer displayOrder, boolean multiple) {
		setName(name);
		setDisplayName(displayName);
		setDisplayOrder(displayOrder);
		setMultiple(multiple);
	}
	



	public String getName() {
		return name;
	}

	@Transient
	public String getBreakableName() {
		return makeStringBreakable(getName());
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	@XmlTransient
	public String getLabel() {
		return displayName + " (" + name + ")";
	}
	
	@Transient
	public String getBreakableLabel() {
		return makeStringBreakable(getLabel());
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public boolean isMultiple() {
		return multiple;
	}

	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

	public List<Attribute> getAttributes() {
		if(attributes == null)
			attributes = new LinkedList<Attribute>();
		return attributes;
	}

	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}

	public void addAttribute(Attribute attribute) {
		if (attributes == null)
			attributes = new LinkedList<Attribute>();
		if (!attributes.contains(attribute)) {
			attributes.add(attribute);
			attribute.setAttributeGroup(this);
		}
	}

	public Attribute getAttribute(Long id) {
		if (id == null)
			throw new NullPointerException("Id coundn't be null.");
		for (Attribute attribute : getAttributes())
			if (id.equals(attribute.getId()))
				return attribute;
		return null;
	}

	public Attribute getAttribute(String name) {
		if (name == null)
			throw new NullPointerException("Name coundn't be null.");
		for (Attribute attribute : getAttributes())
			if (name.equals(attribute.getName()))
				return attribute;
		return null;
	}

	@XmlTransient
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}


	@Override
	public String toString() {
		return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + "\tid=" + id + "; name=" + name + "; displayName=" + displayName;
	}


	@XmlTransient
	public boolean isWriteProtected(){
		if(this.getAttributes() == null)
			return false;
		for(Attribute attribute : this.getAttributes()){
			if(attribute.isWriteProtected())
				return true;
		}
		return false;
	}

	@Override
	@Transient
	public String getClientName() {
		if(this.getClient() != null)
			return this.getClient().getClientName();
		return null;
	}
	
	@Transient
	public boolean hasWriteProtectedAttributes() {
		for (Attribute attribute : getAttributes()) {
			if (attribute.isWriteProtected())
				return true;
		}
		return false;
	}
	
}
