package org.evolvis.idm.identity.account.model;

import javax.xml.bind.annotation.XmlType;

@XmlType(factoryMethod = "createInstance")
public interface UserPropertyFields extends UserSearchableFields {
	public static final String FIELD_PATH_SOFTDELETE = "softDelete";
	public static final String FIELD_PATH_CREATIONDATE = "creationDate";
	public static final String FIELD_PATH_DEACTIVATIONDATE  = "deactivationDate";
}
