package org.evolvis.idm.identity.account.model;

import java.util.Locale;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.validation.CivilName;
import org.evolvis.idm.common.validation.Email;
import org.evolvis.idm.common.validation.FieldEquality;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.identity.privatedata.model.ValueSet;
import org.evolvis.idm.relation.multitenancy.model.Client;

@MappedSuperclass
@FieldEquality(message = "violation.account.equalPasswords", value = { "password", "passwordRepeat" })
@XmlType(name = "AbstractUserEntity", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "abstractUserEntity", namespace = "http://idm.evolvis.org")
public class AbstractUserEntity extends AbstractAccountEntity {
	
	@Email
	@Size(min = 6, max = 60, message = "violation.account.size")
	@Transient
	private String login;

	@Size(min = 2, max = 40, message = "violation.account.size")
	@CivilName
	private String firstname;

	@Size(min = 2, max = 40, message = "violation.account.size")
	@CivilName(message = "violation.account.pattern")
	private String lastname;

	@Size(min = 8, max = 74, message = "violation.account.size")
	@Transient
	private String password;

	@Transient
	private String passwordRepeat;

	@Transient
	private String localeString;
	
	public AbstractUserEntity(){
		//nothing
	}
	
	public AbstractUserEntity(Account account) {
		setUuid(account.getUuid());
		// TODO find better solution
		// allow client retrieval inside UserManagementImpl
		account.clientFlag = true;
		setClient(account.getClient());
		account.clientFlag = false;
		setUsername(account.getUsername());
		setDisplayName(account.getDisplayName());
		setDeactivationDate(account.getDeactivationDate());
		setCreationDate(account.getCreationDate());
		setId(account.getId());
		setSoftDelete(account.isSoftDelete());
	}

	public AbstractUserEntity(Account account, String extendedFlag){
		this(account);
		boolean foundFirstName = false;
		boolean foundLastName = false;
		Value tempValue = null;
		for(ValueSet valueSet : account.valueSets){
			tempValue = valueSet.getValue("firstname");
			if(tempValue != null){
				this.setFirstname(tempValue.getValue());
				foundFirstName = true;
			}
			tempValue = valueSet.getValue("lastname");
			if(tempValue != null){
				this.setLastname(tempValue.getValue());
				foundLastName = true;
			}
			if(foundFirstName && foundLastName)
				break;
		}
	}
	
	public AbstractUserEntity(final String uuid, final Client client, final String username,
			final String firstname, final String lastname, final Locale locale,
			final boolean softDelete) {
		setUuid(uuid);
		setUsername(username);
		setClient(client);
		setFirstname(firstname);
		setLastname(lastname);
		setLocaleString(locale.toString());
		setSoftDelete(softDelete);
	}
	
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getLocaleString() {
		return localeString;
	}

	public void setLocaleString(String localeString) {
		this.localeString = localeString;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

	@XmlTransient
	public String getLogin() {
		return login;
	}
	
	/**
	 * @return the login with zero width spaces after each 25th character.
	 */
	@Transient
	public String getBreakableLogin() {
		return makeStringBreakable(getLogin());
	}

	@XmlTransient
	public String getLabel() {
		return lastname + ", " + firstname + " (" + getUsername() + ")";
	}

	public void setLogin(String login) {
		this.login = login;

		if (login != null && (this.getUsername() == null || !this.getUsername().equals(login)))
			setUsername(login);
	}
	
	@Override
	public void setUsername(String username) {
		super.setUsername(username);
		
		if (username != null && (this.getLogin() == null || !this.getLogin().equals(username)))
			setLogin(username);
	}

	@XmlTransient
	public String getOpenSSORealm() {
		if (this.getClientName() != null)
			return "/" + this.getClientName();
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((localeString == null) ? 0 : localeString.hashCode());
		result = prime * result + ((getUsername() == null) ? 0 : getUsername().hashCode());
		result = prime * result + ((getDisplayName() == null) ? 0 : getDisplayName().hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((passwordRepeat == null) ? 0 : passwordRepeat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractUserEntity other = (AbstractUserEntity) obj;
		if (getClient() == null) {
			if (other.getClient() != null)
				return false;
		} else if (!getClient().equals(other.getClient()))
			return false;
		if (firstname == null) {
			if (other.getFirstname() != null)
				return false;
		} else if (!firstname.equals(other.getFirstname()))
			return false;
		if (lastname == null) {
			if (other.getLastname() != null)
				return false;
		} else if (!lastname.equals(other.getLastname()))
			return false;
		if (localeString == null) {
			if (other.getLocaleString() != null)
				return false;
		} else if (!localeString.equals(other.getLocaleString()))
			return false;
		if (getUsername() == null) {
			if (other.getUsername() != null)
				return false;
		} else if (!getUsername().equals(other.getUsername()))
			return false;
		if (getUuid() == null) {
			if (other.getUuid() != null)
				return false;
		} else if (!getUuid().equals(other.getUuid()))
			return false;
		if (getDisplayName() == null) {
			if (other.getDisplayName() != null)
				return false;
		} else if (!getDisplayName().equals(other.getDisplayName()))
			return false;
		if (password == null) {
			if (other.getPassword() != null)
				return false;
		} else if (!password.equals(other.getPassword()))
			return false;
		if (passwordRepeat == null) {
			if (other.getPasswordRepeat() != null)
				return false;
		} else if (!passwordRepeat.equals(other.getPasswordRepeat()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id="+getId()+"uuid=" + getUuid() + ", client=" + getClientName() + ", username="
				+ getUsername() + ", firstname=" + firstname + ", lastname=" + lastname + ", locale="
				+ localeString + ", displayName=" + getDisplayName()
				+ ", password=*****, passwordRepeat=*****]";
	}
}
