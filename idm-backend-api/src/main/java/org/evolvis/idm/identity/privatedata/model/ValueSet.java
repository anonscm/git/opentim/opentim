package org.evolvis.idm.identity.privatedata.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.Application;



@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "ValueSet")
@XmlType(name = "ValueSet", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "valueSet", namespace = "http://idm.evolvis.org")
public class ValueSet extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = -3283952311746997428L;

	@Transient
	@NotNull
	private String name;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "valueSet")
	@Valid
	private List<Value> values;

	private Integer sortOrder;

	@ManyToOne(optional = false)
	private AttributeGroup attributeGroup;

	@ManyToOne(optional = false)
	private Account account;

	private boolean isLocked;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "approvedValueSet")
	@JoinTable(name = "Approval", joinColumns = { @JoinColumn(name = "account_id") }, inverseJoinColumns = { @JoinColumn(name = "account_id") })
	private Set<Approval> approvals;

	public ValueSet() {
		// nothing.
	}

	public ValueSet(String name) {
		setName(name);
	}

	public ValueSet(ValueSet valueSet){
		if(valueSet != null){
			this.setName(valueSet.getName());
			this.setValues(valueSet.getValues());
			this.setAccount(valueSet.getAccount());
			this.setApprovals(valueSet.getApprovals());
			this.setAttributeGroup(valueSet.getAttributeGroup());
			if(valueSet.getAttributeGroup() != null)
				this.getReferencedEntityContainer().addAttributeGroup(this.getAttributeGroup());
			this.isLocked = valueSet.isLocked;
			this.setId(valueSet.getId());
			this.setSortOrder(valueSet.getSortOrder());
		}
	}
	
	public ValueSet(Account account, AttributeGroup attributeGroup) {
		setAccount(account);
		setAttributeGroup(attributeGroup);
		for (Attribute attribute : attributeGroup.getAttributes()) {
			addValue(new Value(attribute, ""));
		}
	}
	
	
	public String getName() {
		if(name == null && this.getAttributeGroup() != null){
			this.name = this.getAttributeGroup().getName();
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Value> getValues() {
		if (values == null)
			values = new ArrayList<Value>();
		return values;
	}

	public List<Value> getValuesByAttribute(String attributeName) {
		List<Value> list = new LinkedList<Value>();
		for (Value value : getValues()) {
			if (value.getName().equals(attributeName))
				list.add(value);
		}
		return list;
	}

	public void setValues(List<Value> values) {
		this.values = values;
	}

	public Value getValue(Attribute attribute) {
		if (attribute == null || attribute.getId() == null)
			throw new NullPointerException(
					"Search attribute and attribute id shouldn't be null.");
		for (Value value : getValues())
			if (value.getAttribute() == null)
				throw new NullPointerException("Attribute for value "
						+ value + " is not available here.");
			else if (attribute.getId().equals(value.getAttribute().getId()))
				return value;
		return null;
	}

	public Value getValueById(Long valueId) {
		for (Value value : getValues()) {
			if (value.getId().equals(valueId)) {
				return value;
			}
		}
		return null;
	}

	public Value getValue(String name) {
		if (name == null)
			throw new NullPointerException("Search name shouldn't be null.");
		for (Value value : getValues())
			if (value.getName() == null)
				throw new NullPointerException("Value name for value "
						+ value + " is not available here.");
			else if (name.equals(value.getName()))
				return value;
		return null;
	}

	public void addValue(Value value) {
		if (getValues() == null)
			setValues(new LinkedList<Value>());
		if (!getValues().contains(value)) {
			getValues().add(value);
			value.setValueSet(this);
		}
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	public boolean isLocked() {
		return isLocked;
	}

	/**
	 * Returns <code>true</code> if the value set is locked or one of its values, <code>false</code> otherwise.
	 * @return
	 */
	@XmlTransient
	public boolean isLockedRecursive(){
		if(isLocked())
			return true;
		if(this.getValues() != null){
			for(Value value : this.getValues()){
				if(value.isLocked())
					return true;
			}
		}
		return false;
	}
	
	
	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	@XmlIDREF
	public AttributeGroup getAttributeGroup() {
		return attributeGroup;
	}

	public void setAttributeGroup(AttributeGroup attributeGroup) {
		this.attributeGroup = attributeGroup;
		this.name = attributeGroup.getName();
	}

	@XmlTransient
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Set<Approval> getApprovals() {
		return approvals;
	}

	public void setApprovals(Set<Approval> approvals) {
		this.approvals = approvals;
	}

	public boolean isApproved(Application application) {
		if (getApprovals() != null) {
			for (Approval approval : getApprovals()) {
				if (approval.getApprovedAttributeValue() == null
						&& approval.getApplication().equals(application)) {
					return true;
				}
			}
		}

		boolean approved = false;
		if (getValues() != null) {
			approved = true;
			for (Value value : getValues()) {
				if (!value.isApproved(application)) {
					approved = false;
				}
			}
		}
		return approved;
	}

	public boolean deleteValueById(long valueId){
		for(int i = 0; i < values.size(); i++){
			if(values.get(i).getId() != null && values.get(i).getId() == valueId) {
				values.remove(i);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "@"
				+ Integer.toHexString(hashCode()) + "\tid=" + id + "; name="
				+ name + "; values=" + values;
	}

	@Override
	@Transient
	public String getClientName() {
		if(this.getAccount() != null && this.getAccount().getClientName() != null)
			return this.getAccount().getClientName();
		if(this.getAttributeGroup() != null && this.getAttributeGroup().getClientName() != null)
			return this.getAttributeGroup().getClientName();
		return null;
	}
}
