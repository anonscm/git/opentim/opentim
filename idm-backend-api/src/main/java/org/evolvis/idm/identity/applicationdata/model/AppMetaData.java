package org.evolvis.idm.identity.applicationdata.model;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractMap;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.Application;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "AppMetaData", uniqueConstraints = @UniqueConstraint(columnNames = { "account_id", "application_id" }))
@XmlType(name = "AppMetaData", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "appMetaData", namespace = "http://idm.evolvis.org")
public class AppMetaData extends AbstractMap<AppMetaDataValue> {

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "map", fetch = FetchType.EAGER)
	private List<AppMetaDataValue> mapEntries;


	public List<AppMetaDataValue> getMapEntries() {
		if (this.mapEntries == null)
			mapEntries = new LinkedList<AppMetaDataValue>();
		return this.mapEntries;
	}


	public void setMapEntries(List<AppMetaDataValue> mapEntries) {
		this.mapEntries = mapEntries;
	}

	@ManyToOne(optional = false)
	private Application application;

	@ManyToOne(optional = false)
	private Account account;


	public Application getApplication() {
		return application;
	}


	public void setApplication(Application application) {
		this.application = application;
	}


	public Account getAccount() {
		return account;
	}


	public void setAccount(Account account) {
		this.account = account;
	}


	@Override
	protected AppMetaDataValue createMapEntryInstance(String key, String value) {
		return new AppMetaDataValue(key, value, this);
	}


	@Override
	@Transient
	public String getClientName() {
		if(this.getAccount() != null && this.getAccount().getClientName() != null)
			return this.getAccount().getClientName();
		if(this.getApplication() != null)
			return this.getApplication().getClientName();
		return null;
	}

}
