package org.evolvis.idm.identity.applicationdata.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.identity.applicationdata.model.AppMetaData;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "ApplicationMetaDataManagementService", name = "ApplicationMetaDataManagementServicePT", portName = "ApplicationMetaDataManagementServicePort")
public interface ApplicationMetaDataManagement extends AbstractBaseService {

	/**
	 * Returns the application meta data for a given user account.
	 * 
	 * @param applicationName the name of the application
	 * @param uuid the uuid of the user account.
	 * @return the meta data of an application for a given user account.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO 
	 */
	@WebMethod
	@WebResult(name = "appMetaData")
	public AppMetaData getApplicationMetaData(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException;


	/**
	 * Persists the given application meta data.
	 * 
	 * @param appMetaData the application meta data to persist.
	 * @return the persistent version of the given application meta data.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO 
	 */
	@WebMethod
	@WebResult(name = "appMetaData")
	public AppMetaData setApplicationMetaData(@WebParam(name = "appMetaData") AppMetaData appMetaData) throws BackendException, IllegalRequestException;


	/**
	 * Returns an application meta data value for a given user account which can be accessed with the given key.
	 * 
	 * @param applicationName the name of the application
	 * @param uuid the uuid of the user account.
	 * @param metaDataKey the meta data key to identify the wanted value.
	 * @return the wanted meta data value or <code>null</code> if there is no associated value for the given key.
	 * @throws BackendException TODO 
	 * @throws IllegalRequestException TODO 
	 */
	@WebMethod
	@WebResult(name = "appMetaDataValue")
	public String getApplicationMetaDataValue(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "metaDataKey") String metaDataKey) throws BackendException, IllegalRequestException;


	/**
	 * Assigns a key-value pair to the meta data of the application for a certain user account.
	 * 
	 * @param applicationName the name of the application.
	 * @param uuid the uuid of the user account.
	 * @param metaDataKey the key to localise the associated meta data value.
	 * @param metaDataValue the meta data value.
	 * @return if there is already a meta data value stored with the given key it will be overwritten and returned. Otherwise <code>null</code> will be returned.
	 * @throws BackendException TODO 
	 * @throws IllegalRequestException TODO 
	 */
	@WebMethod
	@WebResult(name = "appMetaDataValue")
	public String setApplicationMetaDataValue(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "metaDataKey") String metaDataKey, @WebParam(name = "metaDataValue") String metaDataValue)
			throws BackendException, IllegalRequestException;


	/**
	 * Removes a key-value pair from the meta data of an application for a certain user account.
	 * @param applicationName the name of the application.
	 * @param uuid the uuid of the user account.
	 * @param metaDataKey the key to access the meta data value.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public void removeApplicationMetaDataValue(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid, @WebParam(name = "metaDataKey") String metaDataKey) throws BackendException, IllegalRequestException;


	/**
	 * Removes the meta data of an application for a certain user account.
	 * @param applicationName the name of the application.
	 * @param uuid the uuid of the user account.
	 * @throws BackendException TODO
	 * @throws IllegalRequestException TODO
	 */
	@WebMethod
	public void deleteApplicationMetaData(@WebParam(name = "applicationName") String applicationName, @WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException;
}
