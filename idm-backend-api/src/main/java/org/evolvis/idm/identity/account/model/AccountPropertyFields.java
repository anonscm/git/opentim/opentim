package org.evolvis.idm.identity.account.model;

import javax.xml.bind.annotation.XmlType;

@XmlType(factoryMethod = "createInstance")
public interface AccountPropertyFields extends AccountSearchableFields {
	public static final String FIELD_PATH_CREATIONDATE = "creationDate";
	public static final String FIELD_PATH_DEACTIVATIONDATE  = "deactivationDate";
}
