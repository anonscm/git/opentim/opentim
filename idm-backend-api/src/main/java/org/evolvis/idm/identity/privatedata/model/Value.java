
package org.evolvis.idm.identity.privatedata.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.common.validation.AttributeConstraints;
import org.evolvis.idm.common.validation.DisplayNameExtended;
import org.evolvis.idm.relation.multitenancy.model.Application;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "Value")
@XmlType(name = "Value", namespace = "http://idm.evolvis.org" /*TODO: remove or restore: ,propOrder = { "name", "sortOrder", "value", "attribute", "valueSet", "approvals", "locked", "byteValue"}*/)
@XmlRootElement(name = "value", namespace = "http://idm.evolvis.org")
@AttributeConstraints
public class Value extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = -7240722070168668837L;


	@Transient
	private String name;
	
	//@NotNull
	@Size(max = 100, message="violation.value.size")
	@DisplayNameExtended
	private String value;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] byteValue;
	
	@Transient
	@XmlTransient
	private boolean byteArrayFlag = false;
	
	private Integer sortOrder;

	private boolean isLocked;
	
	// @NotNull
	@ManyToOne(optional = false)
	private Attribute attribute;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "approvedAttributeValue")
	@JoinTable(name = ConfigurationUtil.DB_TABLE_PREFIX + "Approval", joinColumns = { @JoinColumn(name = "account_id") }, inverseJoinColumns = { @JoinColumn(table = ConfigurationUtil.DB_TABLE_PREFIX + "ValueSet", name = "account_id") })
	@Transient
	private Set<Approval> approvals;

	// @NotNull
	@ManyToOne(optional = false)
	private ValueSet valueSet;

	public Value() {
		// Nothing.
	}
	
	public Value(Value value){
		this.byteArrayFlag = true;
		this.setApprovals(value.getApprovals());
		this.setByteValue(value.byteValue);
		this.setValue(value.getValue());
		this.setName(value.getAttribute().getName());
		this.setValueSet(value.getValueSet());
		this.setLocked(value.isLocked);
		this.setAttribute(value.getAttribute());
		this.setId(value.getId());
	}

	public Value(String name, String value) {
		setName(name);
		setValue(value);
	}

	public Value(Attribute attribute, String value) {
		setAttribute(attribute);
		setValue(value);
	}
	
	public Value(String name, String value, boolean byteArrayFlag){
		this(name, value);
		this.byteArrayFlag = byteArrayFlag;
	}



	public String getName() {
		if(name == null && this.getAttribute() != null){
			this.name = this.getAttribute().getName();
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public byte[] getByteValue() {
		if(byteArrayFlag)
			return byteValue;
		return null;
	}

	public void setByteValue(byte[] byteValue) {
		this.byteArrayFlag = true;
		this.byteValue = byteValue;
	}

	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}

	@XmlIDREF
	@XmlElement(name = "attribute", required = true)
	public Attribute getAttribute() {
		return attribute;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
		this.name = attribute.getName();
	}

	
	@XmlIDREF
	public ValueSet getValueSet() {
		return valueSet;
	}

	public void setValueSet(ValueSet valueSet) {
		this.valueSet = valueSet;
	}

	// TODO jneuma remove transient
	// @XmlTransient
	public Set<Approval> getApprovals() {
		return approvals;
	}

	public void setApprovals(Set<Approval> approvals) {
		this.approvals = approvals;
	}

	public boolean isApproved(Application application) {
		if (getValueSet().getApprovals() != null)
			for (Approval approval : getValueSet().getApprovals())
				if (approval.getApprovedAttributeValue() == null && approval.getApplication().equals(application))
					return true;

		if (getApprovals() != null)
			for (Approval approval : getApprovals())
				if (approval.getApprovedAttributeValue() != null && approval.getApprovedAttributeValue().equals(this) && approval.getApplication().equals(application))
					return true;

		return false;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + "\tid=" + id + "; name=" + name + "; value=" + value + "; attribute=" + attribute;
	}

	@Override
	@Transient
	public String getClientName() {
		if(this.getAttribute() != null && this.getAttribute().getClientName() != null)
			return this.getAttribute().getClientName();
		if(this.getValueSet() != null && this.getValueSet().getClientName() != null)
			return this.getValueSet().getClientName();
		return null;
	}

	/**
	 * Set or removes a lock from this value instance.
	 * @param isLocked the isLocked to set
	 */
	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	/**
	 * Returns <code>true</code> if this value instance is locked, <code>false<code> otherwise.
	 * @return the isLocked
	 */
	public boolean isLocked() {
		return isLocked;
	}
}
