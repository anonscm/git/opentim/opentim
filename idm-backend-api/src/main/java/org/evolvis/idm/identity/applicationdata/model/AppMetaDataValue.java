package org.evolvis.idm.identity.applicationdata.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractMapEntry;
import org.evolvis.idm.common.util.ConfigurationUtil;

@Entity
@Table(name = ConfigurationUtil.DB_TABLE_PREFIX + "AppMetaDataValue", uniqueConstraints = @UniqueConstraint(columnNames = {"key","map_id"}))
@XmlType(name = "AppMetaDataValue", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "appMetaDataValue", namespace = "http://idm.evolvis.org")
public class AppMetaDataValue extends AbstractMapEntry<AppMetaData> {

	
	public AppMetaDataValue(){
		//nothing
	}
	
	public AppMetaDataValue(String key, String value){
		super(key,value);
	}

	public AppMetaDataValue(String key, String value, AppMetaData map){
		super(key,value);
		this.setMap(map);
	}
	
	private static final long serialVersionUID = 3401891370276122925L;

	
	
}
