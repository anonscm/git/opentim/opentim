package org.evolvis.idm.identity.account.model;

public interface AccountSearchableFields {
	public static final String ALL = "all";
	public static final String FIELD_PATH_USERNAME = "username";
	public static final String FIELD_PATH_DISPLAYNAME = "displayName";
	public static final String FIELD_PATH_UUID = "uuid";
	public static final String FIELD_PATH_CLIENT_PREFIX = "client.";
	public static final String FIELD_PATH_SOFTDELETE = "softDelete";
}
