package org.evolvis.idm.identity.account.service;

import java.util.Calendar;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.identity.account.model.Account;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "AccountManagementService", name = "AccountManagementServicePT", portName = "AccountManagementServicePort")
public interface AccountManagement extends AccountRetrieval {

	/**
	 * persists an Account instance or updates the persistent version if it
	 * already exists
	 * 
	 * @param account
	 * @return
	 * @throws BackendException
	 * @throws IllegalRequestException
	 * @throws Exception
	 *             TODO
	 */
	@WebMethod
	@WebResult(name = "account")
	public Account setAccount(@WebParam(name = "clientName") String clientName,
			@WebParam(name = "account") Account account) throws BackendException,
			IllegalRequestException;

	/**
	 * deletes an existing account from the persistent context
	 * 
	 * @param uuid
	 *            the unique attribute to identify the persistent account
	 * @throws BackendException
	 * @throws AccessDeniedException
	 * @throws IllegalRequestException
	 *             TODO
	 */
	@WebMethod
	public void deleteAccount(@WebParam(name = "uuid") String uuid) throws BackendException,
			AccessDeniedException, IllegalRequestException;

	/**
	 * Returns a query result instance with a list of accounts that are assigned
	 * to a given client.
	 * 
	 * @param clientName
	 *            the unique name of the client.
	 * @param queryDescriptor
	 *            optional parameter to describe requested order and paging
	 *            properties.
	 * @return a query result instance of accounts of a certain client.
	 * @throws BackendException
	 * @throws AccessDeniedException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "accounts")
	public QueryResult<Account> getAccounts(@WebParam(name = "clientName") String clientName,
			@WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor)
			throws BackendException, AccessDeniedException, IllegalRequestException;

	/**
	 * Returns <code>true</code> if the given user account is deactivated,
	 * <code>false</code> otherwise.
	 * 
	 * @param uuid
	 *            the uuid of the user account
	 * @return <code>true</code> if the user account is deactivated,
	 *         <code>false</code> otherwise.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	public boolean isDeactived(@WebParam(name = "uuid") String uuid) throws BackendException,
			IllegalRequestException;

	/**
	 * Returns a list of user accounts of a given client that are deactivated
	 * since a given date.
	 * 
	 * @param clientName
	 *            the unique name of the client.
	 * @param sinceDate
	 *            the instance of Calendar that contains the date.
	 * @return a list of deactivated user accounts.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "deactivatedAccounts")
	public List<Account> getDeactivatedAccountsSince(
			@WebParam(name = "clientName") String clientName,
			@WebParam(name = "sinceData") Calendar sinceDate) throws BackendException,
			IllegalRequestException;
}
