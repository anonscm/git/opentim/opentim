package org.evolvis.idm.identity.account.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.model.QueryDescriptor;
import org.evolvis.idm.common.model.QueryResult;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.identity.account.model.User;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "UserManagementService", name = "UserManagementServicePT", portName = "UserManagementServicePort")
public interface UserManagement extends AbstractBaseService {
	
	public static String DELIMITER_USERNAME_CHANGE_SPLIT_ACTIVATIONID_EMAIL = "//";

	/**
	 * Create a user.
	 */
	@WebMethod
	public void createUser(@WebParam(name = "user") User user) throws BackendException, IllegalRequestException;

	/**
	 * Delete a user for the given UUID. Could only called with sufficing permissions.
	 * 
	 * @param UUID of the user
	 */
	@WebMethod
	public void deleteUser(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException;

	@WebMethod
	@WebResult(name = "user")
	public User getUserByUsername(@WebParam(name = "clientName") String clientName, @WebParam(name = "username") String username) throws BackendException, IllegalRequestException;

	/**
	 * Returns the user for a given UUID or null, if not found.
	 */
	@WebMethod
	@WebResult(name = "user")
	public User getUserByUuid(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException;

	/**
	 * Return all users for the sufficing permissions of the caller.
	 * 
	 * @return List of users
	 */
	@WebMethod
	@WebResult(name = "users")
	public QueryResult<User> getUsers(@WebParam(name = "clientName") String clientName, @WebParam(name = "queryDescriptor") QueryDescriptor queryDescriptor) throws BackendException, IllegalRequestException;

	/**
	 * Checks whether a user with a name <code>userName</code> already exists.
	 */
	@WebMethod
	@WebResult(name = "isUserExisting")
	public boolean isUserExisting(@WebParam(name = "clientName") String clientName, @WebParam(name = "username") String username) throws BackendException, IllegalRequestException;

	/**
	 * Register a user. Could only called with sufficing permissions.
	 * 
	 * @return ActivationKey if Double-Opt-In is activated or null
	 */
	@WebMethod
	@WebResult(name = "activationKey")
	public String registerUser(@WebParam(name = "clientName") String clientName, @WebParam(name = "username") String username, @WebParam(name = "password") String password, @WebParam(name = "displayName") String displayName,
			@WebParam(name = "firstname") String firstname, @WebParam(name = "lastname") String lastname, @WebParam(name = "localeString") String localeString) throws BackendException, IllegalRequestException;

	/**
	 * Set the given status for the given UUID of a user
	 * 
	 * @param uuid of the account
	 * @param firstname of the account
	 * @param lastname of the account
	 * @throws BackendException
	 */
	@WebMethod
	public void setUserNames(@WebParam(name = "uuid") String uuid, @WebParam(name = "firstname") String firstname, @WebParam(name = "lastname") String lastname) throws BackendException, IllegalRequestException;

	/**
	 * Set the given status for the given uuid of a user
	 * 
	 * @param uuid of the account
	 * @param status of the account
	 * @throws BackendException
	 */
	@WebMethod
	public void setUserStatus(@WebParam(name = "uuid") String uuid, @WebParam(name = "status") String status) throws BackendException, IllegalRequestException;
	
	/**
	 * Changes the username of a user
	 * 
	 * @throws BackendException
	 */
	@WebMethod
	@WebResult(name = "ssoToken")
	public String changeUsername(@WebParam(name = "uuid") String uuid, @WebParam(name = "newUsername") String newUsername, @WebParam(name = "currentPassword") String currentPassword) throws BackendException, IllegalRequestException;

	/**
	 * Checks if a requested username change is possible and returns an activation key for an email verification link
	 * 
	 * @throws BackendException
	 */
	@WebMethod
	@WebResult(name = "activationKey")
	public String requestUsernameChange(@WebParam(name = "uuid") String uuid, @WebParam(name = "newUsername") String newUsername) throws BackendException, IllegalRequestException;
}
