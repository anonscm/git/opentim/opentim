package org.evolvis.idm.identity.account.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.relation.multitenancy.model.Client;

public interface AccountRetrieval extends AbstractBaseService {

	/**
	 * Returns the instance of Account that has the given uuid.
	 * 
	 * @param uuid the uuid of the user account.
	 * @return an instance of Account that has the given uuid or <code>null</code> if no such account exist.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebResult(name = "account")
	@WebMethod
	public Account getAccountByUuid(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException;


	/**
	 * Returns the instance of Account that has the given username and name of its client.
	 * 
	 * @param clientName the unique name of the client.
	 * @param username the user name.
	 * @return an instance of Account that has the given user name or <code>null</code> if no such account exist.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebResult(name = "account")
	@WebMethod
	public Account getAccountByUsername(@WebParam(name = "clientName") String clientName, @WebParam(name = "username") String username) throws BackendException, IllegalRequestException;


	/**
	 * Returns an instance of Client which the account that has the given uuid is associated to.
	 * 
	 * @param uuid the uuid of the user.
	 * @return an instance of Client which the account that has the given uuid is associated to.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebResult(name = "client")
	@WebMethod
	public Client getClientByUuid(@WebParam(name = "uuid") String uuid) throws BackendException, IllegalRequestException;
}
