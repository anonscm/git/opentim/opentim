package org.evolvis.idm.identity.privatedata.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.account.model.Account;
import org.evolvis.idm.identity.privatedata.model.Approval;
import org.evolvis.idm.identity.privatedata.model.ValueSet;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "PrivateDataManagementService", name = "PrivateDataManagementServicePT", portName = "PrivateDataManagementServicePort")
public interface PrivateDataManagement extends PrivateDataRetrieval {

	@WebMethod
	@WebResult(name = "account")
	public Account getAccount(@WebParam(name = "uuid") String uuid) throws BackendException, AccessDeniedException, IllegalRequestException;

	/**
	 * Persists(insert or update) a given value set that belongs to the user account with the given uuid.
	 * 
	 * @param uuid the uuid of the user account.
	 * @param valueSet the value set that is to persist (insert/update).
	 * @param ignoreLock TODO
	 * @return the given value set with changes created by the persisting process.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 * @throws AccessDeniedException
	 */
	@WebMethod
	@WebResult(name = "values")
	public ValueSet setValues(@WebParam(name = "uuid") String uuid, @WebParam(name = "values") ValueSet valueSet, @WebParam(name = "ignoreLock") boolean ignoreLock) throws BackendException, IllegalRequestException, AccessDeniedException;

	/**
	 * Removes a given value set that belongs to the user account which has the given uuid from the persistent context.
	 * 
	 * @param uuid the uuid of the user account.
	 * @param valueSet the value set to remove.
	 * @param ignoreLock TODO
	 * @throws BackendException
	 * @throws AccessDeniedException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	public void deleteValues(@WebParam(name = "uuid") String uuid, @WebParam(name = "values") ValueSet valueSet, @WebParam(name = "ignoreLock") boolean ignoreLock) throws BackendException, AccessDeniedException, IllegalRequestException;
	
	/**
	 * Persists a new approval or updates an already persistent value.
	 * @param clientName the unique name of the client.
	 * @param approval the approval to persist.
	 * @return the persistent version of the given approval.
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	@WebMethod
	@WebResult(name = "approval")
	public Approval setApproval(@WebParam(name = "clientName") String clientName, @WebParam(name = "approval") Approval approval) throws IllegalRequestException, BackendException;
	
	
	/**
	 * 
	 * @param clientName
	 * @param approvalId
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	@WebMethod
	public void deleteApproval(@WebParam(name = "clientName") String clientName, @WebParam(name = "approvalId") Long approvalId) throws IllegalRequestException, BackendException;
}