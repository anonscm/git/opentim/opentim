package org.evolvis.idm.identity.account.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.relation.multitenancy.model.Client;

// TODO adjust annotations to User class

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "uuid" }), @UniqueConstraint(columnNames = { "client_id", "username" }) }, name = ConfigurationUtil.DB_TABLE_PREFIX + "Account")
//@XmlType(propOrder = { "uuid", "username", "displayName", "client","softDelete", "creationDate", "deactivationDate" })
@XmlType(name = "Account", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "account", namespace = "http://idm.evolvis.org")
public class Account extends AbstractAccountEntity implements Serializable {
	private static final long serialVersionUID = -64430950216668252L;


		
	public Account() {
		// nothing
	}
	
	public Account(String uuid, String username, Client client) {
		this();
		setUuid(uuid);
		setUsername(username);
		setDisplayName(username);
		setClient(client);
	}
	
	public Account(String uuid, String username, String displayName, Client client) {
		this(uuid, username, client);
		setDisplayName(displayName);
	}
	
	public Account(String uuid, String username, String displayName, Client client, boolean softDelete) {
		this(uuid, username, displayName, client);
		setSoftDelete(softDelete);
	}
}
