package org.evolvis.idm.identity.privatedata.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "PrivateDataConfigurationService", name = "PrivateDataConfigurationServicePT", portName = "PrivateDataConfigurationServicePort")
public interface PrivateDataConfiguration extends AbstractPrivateDataBase {
	
	/**
	 * Removes a given attribute from the persistent context.
	 * @param clientName the unique name of the client.
	 * @param attribute the attribute to remove.
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	@WebMethod
	public void deleteAttribute(@WebParam(name = "clientName")String clientName, @WebParam(name = "attribute") Attribute attribute) throws IllegalRequestException, BackendException;


	/**
	 * Removes a given attribute group from the persisting context.
	 * @param clientName the unique name of the client.
	 * @param attributeGroup the attribute group to remove.
	 * @throws BackendException
	 * @throws AccessDeniedException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	public void deleteAttributeGroup(@WebParam(name = "clientName")String clientName, @WebParam(name = "attributeGroup") AttributeGroup attributeGroup) throws BackendException, AccessDeniedException, IllegalRequestException;


	/**
	 * Updates a persistent attribute.
	 * @param clientName the unique name of the client.
	 * @param attribute the attribute to update
	 * @return TODO
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	@WebMethod
	@WebResult(name = "attribute")
	public Attribute setAttribute(@WebParam(name = "clientName") String clientName, @WebParam(name = "attribute") Attribute attribute) throws IllegalRequestException, BackendException;

	/**
	 * Persists or updates a given attribute group.
	 * @param clientName the unique name of the client.
	 * @param attributeGroup the attribute group to persist or update.
	 * @return the given attribute group with changes done by the perstisting process.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 * @throws AccessDeniedException
	 */
	@WebMethod
	@WebResult(name = "attributeGroup")
	public AttributeGroup setAttributeGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeGroup") AttributeGroup attributeGroup) throws BackendException, IllegalRequestException, AccessDeniedException;

	/**
	 * Persists or updates a list of given attribute groups.
	 * @param clientName the unique name of the client.
	 * @param attributeGroups the list of attribute groups to persist or update.
	 * @return the list of given attribute groups with changes done by the persisting process.
	 * @throws BackendException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "attributeGroups")
	public List<AttributeGroup> setAttributeGroups(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeGroups") List<AttributeGroup> attributeGroups) throws BackendException, IllegalRequestException;

}



