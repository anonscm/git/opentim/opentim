package org.evolvis.idm.identity.privatedata.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.identity.privatedata.model.Attribute;
import org.evolvis.idm.identity.privatedata.model.AttributeGroup;

@WebService
public interface AbstractPrivateDataBase extends AbstractBaseService {

	/**
	 * Returns the list of attribute groups that are assigned to client that has the given name.
	 * 
	 * @param clientName the unique name of the client.
	 * @return a list of all attribute groups of the given client.
	 * @throws BackendException
	 * @throws AccessDeniedException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "attributeGroups")
	public List<AttributeGroup> getAttributeGroups(@WebParam(name = "clientName") String clientName) throws BackendException, AccessDeniedException, IllegalRequestException;

	/***
	 * Returns an attribute group that has the given id and is assigned to the given client.
	 * 
	 * @param clientName the unique name of the client.
	 * @param id the id of the attribute group.
	 * @return the wanted attribute group or <code>null</code> if no such exists.
	 * @throws BackendException
	 * @throws AccessDeniedException
	 * @throws IllegalRequestException
	 */
	@WebMethod
	@WebResult(name = "attributeGroup")
	public AttributeGroup getAttributeGroupById(@WebParam(name = "clientName") String clientName, @WebParam(name = "id") Long id) throws BackendException, AccessDeniedException, IllegalRequestException;

	/**
	 * Returns an attribute that has the given id and is indirectly assigned to the given client.
	 * 
	 * @param clientName the unique name of the client.
	 * @param attributeId the id of the attribute that is to be returned.
	 * @return the wanted attribute or <code>null</code> if no such exists.
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	@WebMethod
	@WebResult(name = "attribute")
	public Attribute getAttributeById(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeId") Long attributeId) throws IllegalRequestException, BackendException;

	/**
	 * Returns the attribute group that has the given name and is assigned to the client with the given name.	
	 * @param clientName the unique name of the client.
	 * @param attributeGroupName the name of the attribute group that is to be returned.
	 * @return the attribute group with the given name or <code>null</code> if there is no such persistent attribute group.
	 * @throws IllegalRequestException
	 * @throws BackendException
	 */
	@WebMethod
	@WebResult(name = "attributeGroup")
	AttributeGroup getAttributeGroup(@WebParam(name = "clientName") String clientName, @WebParam(name = "attributeGroupName") String attributeGroupName) throws IllegalRequestException, BackendException;

}
