package org.evolvis.idm.identity.privatedata.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.evolvis.idm.common.model.AbstractEntity;
import org.evolvis.idm.common.util.ConfigurationUtil;
import org.evolvis.idm.common.validation.DisplayName;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "name", "attributeGroup_id","parentattribute_id" }) }, name = ConfigurationUtil.DB_TABLE_PREFIX
		+ "Attribute")
@XmlType(name = "Attribute", namespace = "http://idm.evolvis.org")
@XmlRootElement(name = "attribute", namespace = "http://idm.evolvis.org")
@XmlAccessorType(XmlAccessType.FIELD)
public class Attribute extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = 7533187973395058403L;

	@NotNull
	@Size(min = 1, max = 50, message = "violation.attribute.size")
	@Pattern(regexp = "[a-zA-Z0-9._-]*", message = "violation.attribute.pattern")
	@Column(nullable = false, length = 100)
	private String name;

	@NotNull
	@Size(min = 1, max = 50, message = "violation.attribute.size")
	@Pattern(regexp = DisplayName.PATTERN, message = "violation.attribute.pattern")
	@Column(nullable = false, length = 100)
	private String displayName;

	private Integer displayOrder;

	@Column(nullable = false)
	private boolean multiple;

	@NotNull
	@Column(nullable = false)
	private AttributeType type;

	@Column(nullable = true, length = 256)
	private String additionalTypeConstraint;

	@Column(nullable = true, length = 256)
	private String syncOptions;

	@ManyToOne(optional = true)
	@XmlIDREF
	private AttributeGroup attributeGroup;

	@Column(nullable = false)
	private boolean writeProtected;

	// used only to cascade on delete
	@XmlTransient
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "attribute")
	private List<Value> values;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "parentAttribute")
	@Valid
	private List<Attribute> subAttributes;

	@XmlIDREF
	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	private Attribute parentAttribute;

	public Attribute() {
		// nothing.
	}

	public Attribute(String name, String displayName) {
		setName(name);
		setDisplayName(displayName);
		setType(AttributeType.STRING);
	}

	public Attribute(String name, String displayName, Integer displayOrder, boolean multiple) {
		setName(name);
		setDisplayName(displayName);
		setDisplayOrder(displayOrder);
		setMultiple(multiple);
		setType(AttributeType.STRING);
	}

	public Attribute(String name, String displayName, Integer displayOrder, boolean multiple,
			AttributeType type, boolean writeProtected) {
		this.setName(name);
		this.setDisplayName(displayName);
		this.setDisplayOrder(displayOrder);
		this.setMultiple(multiple);
		this.setType(type);
		this.setWriteProtected(writeProtected);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public boolean isMultiple() {
		return multiple;
	}

	public void setMultiple(boolean multiple) {
		this.multiple = multiple;
	}

	public AttributeType getType() {
		return type;
	}

	public void setType(AttributeType type) {
		this.type = type;
	}

	public String getAdditionalTypeConstraint() {
		return additionalTypeConstraint;
	}

	public void setAdditionalTypeConstraint(String additionalTypeConstraint) {
		this.additionalTypeConstraint = additionalTypeConstraint;
	}

	@NotNull
	public AttributeGroup getAttributeGroup() {
		if (this.attributeGroup == null && this.getParentAttribute() != null)
			return this.getParentAttribute().getAttributeGroup();
		return attributeGroup;
	}

	public void setAttributeGroup(AttributeGroup attributeGroup) {
		this.attributeGroup = attributeGroup;
	}

	@XmlTransient
	public List<Value> getValues() {
		return values;
	}

	public void setValues(List<Value> values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + "\tid=" + id
				+ "; name=" + name + "; displayName=" + displayName;
	}

	public void setWriteProtected(boolean writeProtected) {
		this.writeProtected = writeProtected;
	}

	public boolean isWriteProtected() {
		return writeProtected;
	}

	public String getSyncOptions() {
		return syncOptions;
	}

	public void setSyncOptions(String syncOptions) {
		this.syncOptions = syncOptions;
	}

	@Override
	@Transient
	public String getClientName() {
		if (this.getAttributeGroup() != null)
			return this.getAttributeGroup().getClientName();
		return null;
	}

	/**
	 * @param subAttributes
	 *            the subAttributes to set
	 */
	public void setSubAttributes(List<Attribute> subAttributes) {
		for (Attribute attribute : subAttributes)
			attribute.setParentAttribute(this);
		this.subAttributes = subAttributes;
	}

	/**
	 * @return the subAttributes
	 */
	public List<Attribute> getSubAttributes() {
		return subAttributes;
	}

	/**
	 * @param parentAttribute
	 *            the parentAttribute to set
	 */
	public void setParentAttribute(Attribute parentAttribute) {
		this.parentAttribute = parentAttribute;
	}

	/**
	 * @return the parentAttribute
	 */
	public Attribute getParentAttribute() {
		return parentAttribute;
	}

	public void addSubAttribute(Attribute attribute) {
		if (this.getSubAttributes() == null) {
			this.setSubAttributes(new LinkedList<Attribute>());
		}
		attribute.setParentAttribute(this);
		this.getSubAttributes().add(attribute);
	}

	@Transient
	public String getBreakableName() {
		return makeStringBreakable(getName());
	}

}
