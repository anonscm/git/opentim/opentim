package org.evolvis.idm.identity.privatedata.model;

public enum AttributeType {
	STRING,
	BOOLEAN,
	NUMBER,
	DATE,
	DATETIME,
	BLOB,
	VALIDATABLE,
	SELECTLIST
}
