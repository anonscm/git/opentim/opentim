package org.evolvis.idm.synchronization.service.fault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "SynchronizationException", namespace = "http://idm.evolvis.org")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SynchronizationException", namespace = "http://idm.evolvis.org", propOrder = {
    "errorCode",
    "message"
})
public class SynchronizationExceptionBean {
	private String message;
	private String errorCode;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getErrorCode(){
		return this.errorCode;
	}
	
	public void setErrorCode(String errorCode){
		this.errorCode = errorCode;
	}
}
