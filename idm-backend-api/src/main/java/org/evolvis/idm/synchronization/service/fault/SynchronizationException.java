package org.evolvis.idm.synchronization.service.fault;

import javax.xml.ws.WebFault;

import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.relation.multitenancy.model.ClientPropertyMap;

@WebFault(faultBean = "org.evolvis.idm.synchronization.service.fault.SynchronizationExceptionBean")
public class SynchronizationException extends BackendException {
	
	public static final String CODE_SYNC_HOSTPORT_INVALID = "sychnronization.ldap.invalidHostAndPort";
	public static final String CODE_SYNC_MISSING_LOGIN_PLACEHOLDER = "sychnronization.ldap.missingLoginNameInSearchFilter";
	public static final String CODE_SYNC_PROPERTY_MISSING = "sychnronization.ldap.configurationPropertyMissing";
	
	public static final SynchronizationException LDAP_SYNC_HOSTPORT_INVALID_EXCEPTION = new SynchronizationException(
			"Invalid host and port configuration in property="+ClientPropertyMap.KEY_LDAP_SYNC_HOSTANDPORT+", host and port must be provided as \"HOST:PORT\"",
			CODE_SYNC_HOSTPORT_INVALID);

	public static final SynchronizationException LDAP_SYNC_MISSING_LOGIN_PLACEHOLDER_EXCEPTION = new SynchronizationException(
			"Missing username placeholder \""+ClientPropertyMap.PLACEHOLDER_LOGINNAME_LDAP_SYNC_SEARCHFILTER+"\" in property="+ClientPropertyMap.KEY_LDAP_SYNC_SEARCHFILTER,
			CODE_SYNC_MISSING_LOGIN_PLACEHOLDER);
	
	public static final String MESSAGE_LDAP_SYNC_PROPERTY_MISSING_GENERAL = "LDAP synchronization property is missing";
	
	public static final SynchronizationException LDAP_SYNC_ATTRIBUTE_MISSING_EXCEPTION = new SynchronizationException(
			MESSAGE_LDAP_SYNC_PROPERTY_MISSING_GENERAL,
			CODE_SYNC_PROPERTY_MISSING);
	
	private static final long serialVersionUID = 6195663040410299408L;

	public SynchronizationException() {
	}

	public SynchronizationException(String message) {
		super(message);
	}

	public SynchronizationException(String message, String errorCode) {
		super(message, errorCode);
	}

	public SynchronizationException(String message, SynchronizationExceptionBean faultBean) {
		super(faultBean.getMessage(), faultBean.getErrorCode());
	}
}
