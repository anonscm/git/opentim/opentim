package org.evolvis.idm.synchronization.service;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.evolvis.idm.common.fault.AccessDeniedException;
import org.evolvis.idm.common.fault.BackendException;
import org.evolvis.idm.common.fault.IllegalRequestException;
import org.evolvis.idm.common.service.AbstractBaseService;
import org.evolvis.idm.identity.privatedata.model.Value;
import org.evolvis.idm.synchronization.service.fault.SynchronizationException;

@WebService(targetNamespace = "http://idm.evolvis.org", serviceName = "DirectoryServiceSynchronizationService", name = "DirectoryServiceSynchronizationServicePT", portName = "DirectoryServiceSynchronizationServicePort")
public interface DirectoryServiceSynchronization extends AbstractBaseService {

	/**
	 * Synchronizes private data of an account if synchronization is enabled and configured properly
	 * 
	 * @param uuid
	 * @return list of Value-Objects which have been rejected while synchronizing because of invalid content
	 * 			in the directory service
	 * @throws AccessDeniedException
	 * @throws IllegalRequestException
	 * @throws BackendException
	 * @throws SynchronizationException
	 */
	@WebMethod
	public List<Value> synchroniseAccount(@WebParam(name = "uuid") String uuid)
			throws AccessDeniedException, IllegalRequestException, BackendException, SynchronizationException;

}