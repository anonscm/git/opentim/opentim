package org.evolvis.idm;

import java.io.IOException;

import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


/** servlet for setting properties
 * 
 * @author Martin Pelzer, tarent GmbH
 *
 */
public class PropertiesSetter extends GenericServlet {

	private static final long serialVersionUID = -3930593472690253312L;

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		// TODO nothing
		
	}
	
	public void init(ServletConfig config) throws ServletException {
	    super.init(config);
		
		System.setProperty("javax.xml.soap.SOAPFactory", "org.jboss.ws.core.soap.SOAPFactoryImpl");
		System.setProperty("javax.xml.soap.MessageFactory", "org.jboss.ws.core.soap.MessageFactoryImpl");
		System.setProperty("javax.xml.soap.MetaFactory", "org.jboss.ws.core.soap.SAAJMetaFactoryImpl");
	}
}
